/**=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PVT. LTD                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CADEXDownloadClient.java          
#      Module          :           com.cadex.exchanger.client          
#      Description     :           Applet Code for download from FTP and shared drive of the File server          
#      Project         :           CADExchanger          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  08-Aug-2015                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.cadex.exchanger.client;

import java.applet.Applet;
import java.awt.HeadlessException;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.AccessController;
import java.security.PrivilegedAction;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;

import netscape.javascript.JSObject;

import com.cadex.exchanger.client.ftp.FTPCopyTask;
import com.cadex.exchanger.client.shared.CopyFilesTask;


public class CADEXDownloadClient extends Applet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static int UPLOAD_OPERATION_TYPE = 1;
	public static int DOWNLOAD_OPERATION_TYPE = 2;
	
	final static Charset ENCODING = StandardCharsets.UTF_8;
	
	public CADEXDownloadClient() throws HeadlessException {
		// TODO Auto-generated constructor stub
	}

	
	@Override
	public void init() {
		super.init();
	}
	
	public static void main(String[] args) throws IOException
	{
//		CADEXDownloadClient client = new CADEXDownloadClient();
//		client.uploadFTP("localhost:21", "cadexadmin", "cadexadmin");
//		client.downloadFTP("localhost#21", "cadexadmin", "cadexadmin", "cadex_exchange_08082015_145627866/SampleFiles.zip");
//		System.out.println(result);
		
//		CADEXDownloadClient client = new CADEXDownloadClient();
//		client.uploadSharedDrive("", "", "\\\\PL1USMGT0483NB");
//		client.downloadSharedDrive("", "", "\\\\PL1USMGT0483NB\\OEM_SharedDrive\\supercopier-portable-windows-x86-4.0.1.13.zip");
		
//		File file = new File("C:\\Ashwath\\Teradyne\\output.txt");
//		 
//		// if file doesnt exists, then create it
//		if (!file.exists()) {
//			file.createNewFile();
//		}
//		
//		FileWriter fw = new FileWriter(file.getAbsoluteFile());
//		BufferedWriter bw = new BufferedWriter(fw);
//		
//		try {
//			Path path = Paths.get("C:\\Ashwath\\Teradyne\\BulkDump_MfgPrtMr.dat");
//			
//			
// 
//			
//			
//			
//			
//			int lineNo = 0;
//			int firstLineCharacterCount = 0;
//			int globalcharcount=0;
//			try (Scanner scanner =  new Scanner(path, ENCODING.name())){
//			  while (scanner.hasNextLine()){
//			    //process each line in some way
//				  lineNo++;
//				int charCount = 0;
//			    String scanStr = scanner.nextLine();
//			    int len = scanStr.length();
//			    for (int i = 0; i < len; ++i)
//			    {
//			        char charAt = scanStr.charAt(i);
//
//			        if(charAt=='~')
//			        	{globalcharcount++;charCount++;}
//			    }
//			    
//			    if(lineNo == 1)
//			    {
//			    	firstLineCharacterCount = charCount;
//			    	bw.write("First line has " + firstLineCharacterCount + "\n"); 
//			    }
//			    
//			    if(lineNo>1 && charCount > firstLineCharacterCount-1)
//			    	bw.write(charCount + "-" + lineNo + "\n"); 
//			    
//			    
//			    
//			    //System.out.println(charCount);
//			    
//			  }    
//			  
//			 
//			}
//			
//			
//			System.out.println(globalcharcount);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		finally{
//			 bw.close();
//		}
	
	}
	
	public void doPostProcess(String result, int operationType)
	{
		JSObject js = JSObject.getWindow(this);
		js.call( operationType == UPLOAD_OPERATION_TYPE ? "uploadFinished" : "downloadFinished", new Object[] {result})   ;
	}
	
	public int downloadFTP(final String host, final String user, final String password, final String path )
	{
		AccessController.doPrivileged( new PrivilegedAction<Void>() {

			@Override
			public Void run() {
				
				String port = host.substring( host.indexOf( "#" ) +1);
				final String ftpHost = host.substring( 0 , host.indexOf( "#" ) );
				String downloadDir = getDownloadDir();
				if(downloadDir != null)
				{
					FTPCopyTask copyTask = new FTPCopyTask(CADEXDownloadClient.this);
					copyTask.startOperation(ftpHost, Integer.parseInt( port ), user, password, path, CADEXDownloadClient.DOWNLOAD_OPERATION_TYPE, downloadDir);
				}
				
				return null;
			}
		});
		
		return 0;
	}
	
	public void uploadFTP(final String host, final String user, final String password )
	{
		AccessController.doPrivileged( new PrivilegedAction<Void>() {

			@Override
			public Void run() {
				
				final String port = host.substring( host.indexOf( ":" )+1 );
				final String ftpHost = host.substring( 0 , host.indexOf( ":" ) );
				final String uploadFile = getUploadFile();
				if(uploadFile != null)
				{
					
					SwingUtilities.invokeLater(new Runnable() {
			            public void run() {
			                // create example app window
			            	FTPCopyTask ftpTask = new FTPCopyTask(CADEXDownloadClient.this);
			            	ftpTask.startOperation(ftpHost, Integer.parseInt( port ), user, password, uploadFile,  CADEXDownloadClient.UPLOAD_OPERATION_TYPE, "");
			            }
			        });
					

				}
				
				return null;
			}
		});
		
	}
	
	public void uploadSharedDrive(String user, String password, final String path )
	{
		
		AccessController.doPrivileged( new PrivilegedAction<Void>() {

			@Override
			public Void run() {
				
				final String upFile = getUploadFile();
				
				if(upFile != null)
				{
					SwingUtilities.invokeLater(new Runnable() {
			            public void run() {
			                // create example app window
			            	CopyFilesTask sharedTask = new CopyFilesTask(upFile, path, CADEXDownloadClient.this);
			            	sharedTask.startOperation();
			            }
			        });
				}
				return null;
			}
		});
		
	}
	
	public int downloadSharedDrive(String user, String password, final String path )
	{
		
		AccessController.doPrivileged( new PrivilegedAction<Void>() {

			@Override
			public Void run() {
				
				final String downloadDir = getDownloadDir();
				
				if(downloadDir != null)
				{
					SwingUtilities.invokeLater(new Runnable() {
			            public void run() 
			            {
			            	CopyFilesTask sharedTask = new CopyFilesTask(path, downloadDir, CADEXDownloadClient.this);
			            	sharedTask.startOperation();
			            }
			        });
				}
				return null;
			}
		});
		
		return 0;
	}
	
	private String getDownloadDir()
	{
		File downloadDirectory = null;
		JFileChooser chooser = new JFileChooser();
	    chooser.setCurrentDirectory(new java.io.File("."));
	    chooser.setDialogTitle("Select Directory");
	    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	    
	    chooser.setAcceptAllFileFilterUsed(false);
	    
	    if (chooser.showOpenDialog(new JFrame()) == JFileChooser.APPROVE_OPTION) 
	    { 
	    	downloadDirectory = chooser.getSelectedFile();
	    }
	    else 
	    {
	        System.out.println("No Selection ");
	    }
	    
	    return downloadDirectory != null ? downloadDirectory.getAbsolutePath() : null; 
	}
	
	private String getUploadFile()
	{
		File downloadDirectory = null;
		JFileChooser chooser = new JFileChooser();
	    chooser.setCurrentDirectory(new java.io.File("."));
	    chooser.setDialogTitle("Select Package");
	    chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
	    
	    chooser.setFileFilter( new FileFilter() {
			
			@Override
			public String getDescription() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public boolean accept(File f) {
				// TODO Auto-generated method stub
				return (f.isFile() && f.getName().endsWith(".zip")) || (f.isDirectory());
			}
		} );
	    
	    if (chooser.showOpenDialog(new JFrame()) == JFileChooser.APPROVE_OPTION) 
	    { 
	    	downloadDirectory = chooser.getSelectedFile();
	    }
	    else 
	    {
	        System.out.println("No Selection ");
	    }
	    
	    return downloadDirectory != null ? downloadDirectory.getAbsolutePath() : null; 
	}
	
	
}

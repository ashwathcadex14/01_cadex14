/**=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PVT. LTD                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           FTPCopyTask.java          
#      Module          :           com.cadex.exchanger.client.ftp          
#      Description     :           Asynchronous task for ftp file copy          
#      Project         :           CADExchanger          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  08-Aug-2015                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.cadex.exchanger.client.ftp;

import java.awt.BorderLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ProgressMonitor;
import javax.swing.SwingWorker;

import com.cadex.exchanger.client.CADEXDownloadClient;


public class FTPCopyTask extends JFrame implements ActionListener, PropertyChangeListener {
    /**
	 * 
	 */
	private static final long serialVersionUID = -1380178827524311959L;
    private JButton copyButton;
    private JTextArea console;
    private ProgressMonitor progressMonitor;
    private FTPTask operation;
    CADEXDownloadClient client ;
	private static final int DEFAULT_WIDTH = 700;
    private static final int DEFAULT_HEIGHT = 350;
    
    public FTPCopyTask(CADEXDownloadClient client) {
         //set up the copy files button
        
    	
    	super("Upload Package");
    	
    	copyButton = new JButton("Close");
    	copyButton.addActionListener(this);
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(copyButton);
    	
    	this.client = client;
    	
    	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	this.setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        
        // create example app content pane
        // ProgressMonitorExample constructor does additional GUI setup
        
        // display example app window
        this.setVisible(true);
    	
        // set up the console for display of operation output
        console = new JTextArea(15,60);
        console.setMargin(new Insets(5,5,5,5));
        console.setEditable(false);
        add(new JScrollPane(console), BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.SOUTH);  

    }
    
    public String getResult()
    {
    	return this.operation.result;
    }
    
    public void startOperation(String host, int port, String username, String password,
                String targetFile, int mode, String downloadFile)
    {
        progressMonitor = new ProgressMonitor(FTPCopyTask.this,
                "Operation in progress...",
                "", 0, 100);
		progressMonitor.setProgress(0);
		
		//fix to show the progress bar
		try {
		Thread.sleep(1000);
		} catch (InterruptedException e) {
		// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(new JFrame(), "Problem occurred during thread wait." + e.getMessage() + "\n");
		}
		
		// schedule the copy files operation for execution on a background thread
		operation = new FTPTask(host, port, username, password, targetFile, mode, downloadFile);
		// add ProgressMonitorExample as a listener on CopyFiles;
		// of specific interest is the bound property progress
		operation.addPropertyChangeListener(this);
		operation.execute();
		// we're running our operation; disable copy button
		copyButton.setEnabled(false);
    }
    
    // executes in event dispatch thread
    public void propertyChange(PropertyChangeEvent event) {
        // if the operation is finished or has been canceled by
        // the user, take appropriate action
        if (progressMonitor.isCanceled()) {
            operation.cancel(true);
        } else if (event.getPropertyName().equals("progress")) {            
            // get the % complete from the progress event
            // and set it on the progress monitor
            int progress = ((Integer)event.getNewValue()).intValue();
            progressMonitor.setProgress(progress);            
        }        
    }
    
   public class FTPTask extends SwingWorker<Void, CopyData> {        
        private static final int BUFFER_SIZE = 4096;
        
        private String host;
        private int port;
        private String username;
        private String password;
         
        private String downloadDir;
        //private String destDir;
        private String uploadFile;
        private String result;
        private int taskMode = 0;
        
        FTPTask(String host, int port, String username, String password,
                String targetFile, int mode, String downloadDirectory) {
        	this.host = host;
            this.port = port;
            this.username = username;
            this.password = password;
            this.uploadFile = targetFile;
            this.taskMode = mode;
            this.downloadDir = downloadDirectory;
        }
        
        // perform time-consuming copy task in the worker thread
        @Override
        public Void doInBackground() 
        {
        	if(this.taskMode ==  CADEXDownloadClient.UPLOAD_OPERATION_TYPE)
        		uploadOperation();
        	else
        		downloadOperation();
        	
			return null;
        }
        
        private void uploadOperation()
        {
        	int progress = 0;
            // initialize bound property progress (inherited from SwingWorker)
            setProgress(0);

       	 	 FTPUtility util = new FTPUtility(host, port, username, password);
    		 File file = new File(uploadFile);
    		 FileInputStream inputStream = null;
    	        try {
    	        	console.append( "Connecting to FTP Server\n" );
    	        	
    	            util.connect();
    	            console.append( "Connected to FTP Server\n" );
    	            console.append( "Setting FTP directory\n" );
    	            SimpleDateFormat dFormat = new SimpleDateFormat( "ddMMyyyy_HHmmssSSS" );
    	            String newDir = "cadex_exchange_" + dFormat.format( Calendar.getInstance().getTime() );
    	            util.uploadFile(file, newDir);
    	            console.append( "FTP directory set\n" );
    	           
    	            inputStream = new FileInputStream(file);
    	            byte[] buffer = new byte[BUFFER_SIZE];
    	            int bytesRead = -1;
    	            long totalBytesRead = 0;
    	            long fileSize = file.length();
    	            double fileSizeKb = fileSize / 1024 ;
    	 
    	            console.append( "Uploading File\n" );
    	            
    	            
    	            while ((bytesRead = inputStream.read(buffer)) != -1) {
    	                util.writeFileBytes(buffer, 0, bytesRead);
    	                totalBytesRead += bytesRead;
    	                progress = (int) (totalBytesRead * 100 / fileSize);
    	                double fileSizeReadKb = totalBytesRead / 1024 ;
    	                
    	                CopyData current = new CopyData(progress, uploadFile,
    	                		Math.round(fileSizeKb),
    	                		Math.round(fileSizeReadKb));
    	                
    	                Thread.sleep(10);
    	                
						// set new value on bound property
						// progress and fire property change event
						setProgress(progress);
						
						// publish current progress data for copy task
						publish(current);
    	                
    	            }
    	 
    	            inputStream.close();
    	             
    	            util.finish();
    	        	
    	        	result = newDir + "/" +  file.getName();
    	        } catch (final Exception ex) {
    	        	console.append( "Upload Error \n" + "Problem occurred in uploading file." + ex.getMessage() + "\n");
    	        	 result = "Error";
    	        } finally {
    	        	 
    	            try {
    					util.disconnect();
    					 inputStream.close();
    				} catch (final Exception e) {
    					console.append( "Upload Error \n" + "Unable to disconnect from the server." + e.getMessage()+ "\n");
    					 result = "Error";
    				}
    	           
    	        }
    	        
        }
        
        private void downloadOperation()
        {
        	int progress = 0;
            // initialize bound property progress (inherited from SwingWorker)
            setProgress(0);
        	
        	FTPUtility util = new FTPUtility(host, port, username, password);
   		    FileOutputStream outputStream = null;
   	        try {
   	        	console.append( "Connecting to FTP Server" + "\n");
   	            util.connect();
   	            console.append( "Connected to FTP Server" + "\n");
   	            console.append( "Setting FTP directory" + "\n");
   	            
   	            util.downloadFile(uploadFile);
   	            console.append( "FTP directory set" + "\n");
   	           
   	            SimpleDateFormat dFormat = new SimpleDateFormat( "ddMMyyyy_HHmmssSSS" );
	            String newFile = "cadex_exchange_" + dFormat.format( Calendar.getInstance().getTime() ) + ".zip";
   	            
   	            String ftpFile = downloadDir + "\\" + newFile;
   	            outputStream = new FileOutputStream(ftpFile);
   	            byte[] buffer = new byte[BUFFER_SIZE];
   	            int bytesRead = buffer.length;
   	            long totalBytesRead = 0;
   	            long fileSize = util.getFileSize( uploadFile );
   	            double fileSizeKb = fileSize / 1024 ;
   	 
   	            console.append( "Downloading File" + "\n");
   	            
   	            
   	            while ((bytesRead = util.readFileBytes(buffer, 0, bytesRead)) != -1) {
   	                
   	            	outputStream.write(buffer, 0, bytesRead);
   	                totalBytesRead += bytesRead;
   	                progress = (int) (totalBytesRead * 100 / fileSize);
   	                double fileSizeReadKb = totalBytesRead / 1024 ;
   	                
   	                CopyData current = new CopyData(progress, uploadFile,
	                		Math.round(fileSizeKb),
	                		Math.round(fileSizeReadKb));

					// set new value on bound property
					// progress and fire property change event
					setProgress(progress);
					
					// publish current progress data for copy task
					publish(current);
   	            }
   	 
   	            outputStream.close();
   	             
   	            util.finish();
   	        	
   	           
   	        } catch (final Exception ex) {
   	        	console.append( "Upload Error \n" + "Problem occurred in downloading file." + ex.getMessage()+ "\n");
   	        	result = "Error";
   	        } finally {
   	            try {
   					util.disconnect();
   					outputStream.close();
   				} catch (final Exception e) {
   					console.append( "Upload Error \n" + "Problem occurred in downloading file." + e.getMessage()+ "\n");
   					result = "Error";
   				}
   	            
   	           
   	        }
    	        
        }

        // process copy task progress data in the event dispatch thread
        @Override
        public void process(List<CopyData> data) {
            if(isCancelled()) { return; }
            CopyData update  = new CopyData(0, "", 0, 0);
            for (CopyData d : data) {
                // progress updates may be batched, so get the most recent
                if (d.getKiloBytesCopied() > update.getKiloBytesCopied()) {
                    update = d;
                }
            }
            
            // update the progress monitor's status note with the
            // latest progress data from the copy operation, and
            // additionally append the note to the console
            String progressNote = update.getKiloBytesCopied() + " of " 
                                  + update.getTotalKiloBytes() + " kb copied.";
            String fileNameNote = "Now copying " + update.getFileName();
            
            if (update.getProgress() < 100) {
                progressMonitor.setNote(progressNote + " " + fileNameNote);
                console.append(progressNote + "\n" + fileNameNote + "\n");
            } else {
                progressMonitor.setNote(progressNote);
                console.append(progressNote + "\n");
            }           
        }
        
        /**
		 * @return the result
		 */
		public String getResult() {
			return result;
		}

		// perform final updates in the event dispatch thread
        @Override
        public void done() {
            try {
                // call get() to tell us whether the operation completed or 
                // was canceled; we don't do anything with this result
                @SuppressWarnings("unused")
				Void result = get();
            
                console.append("Copy operation completed. " + this.result + "\n");
                
                
                
            } catch (InterruptedException e) {
            	 result = "Error";
            } catch (CancellationException e) {
                // get() throws CancellationException if background task was canceled
                console.append("Copy operation canceled.\n");
                result = "Error";
            } catch (ExecutionException e) {
                console.append("Exception occurred: " + e.getCause());
                result = "Error";
            }
            // reset the example app
            copyButton.setEnabled(true);
            progressMonitor.setProgress(0);
            progressMonitor.close();
            
            	
            
        }
    }
    
    class CopyData {        
        private int progress;
        private String fileName;
        private long totalKiloBytes;
        private long kiloBytesCopied;
        
        CopyData(int progress, String fileName, long totalKiloBytes, long kiloBytesCopied) {
            this.progress = progress;
            this.fileName = fileName;
            this.totalKiloBytes = totalKiloBytes;
            this.kiloBytesCopied = kiloBytesCopied;
        }

        int getProgress() {
            return progress;
        }
        
        String getFileName() {
            return fileName;
        }

        long getTotalKiloBytes() {
            return totalKiloBytes;
        }

        long getKiloBytesCopied() {
            return kiloBytesCopied;
        }
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		this.dispose();
		client.doPostProcess( operation.getResult(), operation.taskMode );
	}
}

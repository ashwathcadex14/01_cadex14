/**=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PVT. LTD                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           FTPException.java          
#      Module          :           com.cadex.exchanger.client.ftp          
#      Description     :           FTP Exception for ftp related exceptions          
#      Project         :           CADExchanger          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  08-Aug-2015                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.cadex.exchanger.client.ftp;

public class FTPException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = -6733087913434863865L;

	public FTPException(String message) {
        super(message);
    }
}

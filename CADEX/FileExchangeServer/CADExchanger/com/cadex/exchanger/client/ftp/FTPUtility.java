/**=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PVT. LTD                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           FTPUtility.java          
#      Module          :           com.cadex.exchanger.client.ftp          
#      Description     :           Utility to upload or download from File server using FTP          
#      Project         :           CADExchanger          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  08-Aug-2015                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.cadex.exchanger.client.ftp;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
 
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
 
/**
 * A utility class that provides functionality for uploading files to a FTP
 * server.
 *
 * @author www.codejava.net
 *
 */
public class FTPUtility {
 
    private String host;
    private int port;
    private String username;
    private String password;
 
    private FTPClient ftpClient = new FTPClient();
    private int replyCode;
 
    private OutputStream outputStream;
    private InputStream inputStream;
     
    public FTPUtility(String host, int port, String user, String pass) {
        this.host = host;
        this.port = port;
        this.username = user;
        this.password = pass;
    }
 
    /**
     * Connect and login to the server.
     *
     * @throws FTPException
     */
    public void connect() throws FTPException {
        try {
            ftpClient.connect(host, port);
            replyCode = ftpClient.getReplyCode();
            if (!FTPReply.isPositiveCompletion(replyCode)) {
                throw new FTPException("FTP serve refused connection.");
            }
 
            boolean logged = ftpClient.login(username, password);
            if (!logged) {
                // failed to login
                ftpClient.disconnect();
                throw new FTPException("Could not login to the server.");
            }
 
            ftpClient.enterLocalPassiveMode();
 
        } catch (IOException ex) {
            throw new FTPException("I/O error: " + ex.getMessage());
        }
    }
 
    /**
     * Start uploading a file to the server
     * @param uploadFile the file to be uploaded
     * @param destDir destination directory on the server
     * where the file is stored
     * @throws FTPException if client-server communication error occurred
     */
    public void uploadFile(File uploadFile, String destDir) throws FTPException {
        try {
//            boolean success = ftpClient.changeWorkingDirectory(destDir);
//            
//            if (!success) {
//                throw new FTPException("Could not change working directory to "
//                        + destDir + ". The directory may not exist.");
//            }
            
        	boolean createSuccess = ftpClient.makeDirectory( "/" + destDir );
        	if (!createSuccess) {
                throw new FTPException("Could not create the directory " + destDir );
            }
        	
        	
        	boolean success = ftpClient.setFileType(FTP.BINARY_FILE_TYPE);         
            if (!success) {
                throw new FTPException("Could not set binary file type.");
            }
             
            outputStream = ftpClient.storeFileStream(destDir + "/" + uploadFile.getName());
             
        } catch (IOException ex) {
            throw new FTPException("Error uploading file: " + ex.getMessage());
        }
    }
    
    /**
     * Start downloading a file to the server
     * @param downloadFile the file to be uploaded
     * where the file is stored
     * @throws FTPException if client-server communication error occurred
     */
    public void downloadFile(String downloadFile) throws FTPException {
        try {
//            boolean success = ftpClient.changeWorkingDirectory(destDir);
//            
//            if (!success) {
//                throw new FTPException("Could not change working directory to "
//                        + destDir + ". The directory may not exist.");
//            }
             
//        	boolean success = ftpClient.setFileType(FTP.BINARY_FILE_TYPE);         
//            if (!success) {
//                throw new FTPException("Could not set binary file type.");
//            }
             
        	
            inputStream = ftpClient.retrieveFileStream( "/" + downloadFile);
            System.out.println( ftpClient.getReplyString() ) ;
            ftpClient.completePendingCommand();
             
        } catch (IOException ex) {
            throw new FTPException("Error uploading file: " + ex.getMessage());
        }
    }
 
    /**
     * Write an array of bytes to the output stream.
     */
    public void writeFileBytes(byte[] bytes, int offset, int length)
            throws IOException {
        outputStream.write(bytes, offset, length);
    }
    
    /**
     * Get Size of the file
     */
    public long getFileSize(String filePath)
            throws IOException {
    	FTPFile file = ftpClient.mlistFile("/"+filePath);
    	long retSize = file.getSize();
    	if(retSize == -1)
    	{
    		ftpClient.sendCommand("SIZE", filePath);
            String reply = ftpClient.getReplyString();
            String fileSize = reply.substring( reply.indexOf( " " ) + 1, reply.length() - 2 );
            retSize = Long.parseLong( fileSize );
    	}
    	
    	return retSize;
    }
    
    /**
     * Read file
     */
    public int readFileBytes(byte[] bytes, int offset, int length)
            throws IOException {
        return inputStream.read(bytes, offset, length);
    }
     
    /**
     * Complete the upload operation.
     */
    public void finish() throws IOException {
    	if(outputStream != null)
    		outputStream.close();
    	if(inputStream != null)
    		inputStream.close();
        ftpClient.completePendingCommand();
    }
     
    /**
     * Log out and disconnect from the server
     */
    public void disconnect() throws FTPException {
        if (ftpClient.isConnected()) {
            try {
                if (!ftpClient.logout()) {
                    throw new FTPException("Could not log out from the server");
                }
                ftpClient.disconnect();
            } catch (IOException ex) {
                throw new FTPException("Error disconnect from the server: "
                        + ex.getMessage());
            }
        }
    }
}

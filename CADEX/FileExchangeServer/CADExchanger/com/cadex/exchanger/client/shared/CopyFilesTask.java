/**=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PVT. LTD                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CopyFilesTask.java          
#      Module          :           com.cadex.exchanger.client.shared          
#      Description     :           Asynchronous task for shared drive file copy          
#      Project         :           CADExchanger          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  08-Aug-2015                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.cadex.exchanger.client.shared;

import java.awt.BorderLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ProgressMonitor;
import javax.swing.SwingWorker;

import com.cadex.exchanger.client.CADEXDownloadClient;

public class CopyFilesTask extends JFrame implements ActionListener, PropertyChangeListener {
    /**
	 * 
	 */
	private static final long serialVersionUID = -1380178827524311959L;
    private JTextArea console;
    private JButton copyButton;
    private ProgressMonitor progressMonitor;
    private CopyFiles operation;
    private File srcFile;
    private File destDir;
    CADEXDownloadClient client ;
	private static final int DEFAULT_WIDTH = 700;
    private static final int DEFAULT_HEIGHT = 350;
    
    public CopyFilesTask(String srcFile, String destDir, CADEXDownloadClient client) {
        // set up the copy files button
    	super("Upload Package");
    	
    	copyButton = new JButton("Close");
    	copyButton.addActionListener(this);
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(copyButton);
    	
    	this.client = client;
    	
    	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	this.setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        
        // create example app content pane
        // ProgressMonitorExample constructor does additional GUI setup
        
        // display example app window
        this.setVisible(true);
    	
        // set up the console for display of operation output
        console = new JTextArea(15,60);
        console.setMargin(new Insets(5,5,5,5));
        console.setEditable(false);
        add(new JScrollPane(console), BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.SOUTH);  
        
        this.srcFile = new File( srcFile );
        this.destDir = new File( destDir );
    }
    
    public void startOperation() {
        // make sure there are files to copy
        if (srcFile.exists() ) {
            // set up the destination directory
            // create the progress monitor
            progressMonitor = new ProgressMonitor(CopyFilesTask.this,
                                                  "Operation in progress...",
                                                  "", 0, 100);
            progressMonitor.setProgress(0);
            
            //fix to show the progress bar
            try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

            // schedule the copy files operation for execution on a background thread
            operation = new CopyFiles(srcFile, destDir);
            // add ProgressMonitorExample as a listener on CopyFiles;
            // of specific interest is the bound property progress
            operation.addPropertyChangeListener(this);
            operation.execute();
            // we're running our operation; disable copy button
            copyButton.setEnabled(false);
        } else {
            console.append("The sample application needs files to copy."
                           + " Please add some files to the in directory"
                           + " located at the project root.");
        }
    }
    
    // executes in event dispatch thread
    public void propertyChange(PropertyChangeEvent event) {
        // if the operation is finished or has been canceled by
        // the user, take appropriate action
        if (progressMonitor.isCanceled()) {
            operation.cancel(true);
        } else if (event.getPropertyName().equals("progress")) {            
            // get the % complete from the progress event
            // and set it on the progress monitor
            int progress = ((Integer)event.getNewValue()).intValue();
            progressMonitor.setProgress(progress);            
        }        
    }
    
    class CopyFiles extends SwingWorker<Void, CopyData> {        
        private static final int PROGRESS_CHECKPOINT = 10000;
        private File srcFile;
        private File destDir;
        private File srcDir;
        private String result;
        private int taskMode = 0;
        
        CopyFiles(File src, File dest) {
            this.srcFile = src;
            this.destDir = dest;
            this.srcDir = this.srcFile.getParentFile();
            if(srcFile.getAbsolutePath().startsWith( "\\" ))
            {
            	this.taskMode = CADEXDownloadClient.DOWNLOAD_OPERATION_TYPE;
            }
            else
            	this.taskMode =  CADEXDownloadClient.UPLOAD_OPERATION_TYPE;
        }
        
        /**
		 * @return the result
		 */
		public String getResult() {
			return result;
		}
        
        // perform time-consuming copy task in the worker thread
        @Override
        public Void doInBackground() {
            int progress = 0;
            // initialize bound property progress (inherited from SwingWorker)
            setProgress(0);
            // get the files to be copied from the source directory
            File[] files = srcDir.listFiles( new FileFilter() {
				
				@Override
				public boolean accept(File arg0) {
					
					return arg0.getAbsolutePath().equals( srcFile.getAbsolutePath() );
				}
			} );
            
            // determine the scope of the task
            long totalBytes = calcTotalBytes(files);
            long bytesCopied = 0;
            
            File destFile = null;
            
            while (progress < 100 && !isCancelled()) {                 
                // copy the files to the destination directory
                for (File f : files) 
                {
                	if(f.getName().equalsIgnoreCase( this.srcFile.getName() ))
                	{
                		destFile = new File(destDir, f.getName());
                        long previousLen = 0;
                        InputStream in = null;
                        OutputStream out = null;   
                        
                        try 
                        {
                        	in = new FileInputStream(f);    
                        	out = new FileOutputStream(destFile);  
                            byte[] buf = new byte[1024];
                            int counter = 0;
                            int len;
                            
                            while ((len = in.read(buf)) > 0) {
                                out.write(buf, 0, len);
                                counter += len;
                                bytesCopied += (destFile.length() - previousLen);
                                previousLen = destFile.length();
                                if (counter > PROGRESS_CHECKPOINT || bytesCopied == totalBytes) {
                                    // get % complete for the task
                                    progress = (int)((100 * bytesCopied) / totalBytes);
                                    counter = 0;
                                    CopyData current = new CopyData(progress, f.getName(),
                                                                    getTotalKiloBytes(totalBytes),
                                                                    getKiloBytesCopied(bytesCopied));

                                    
                                    // set new value on bound property
                                    // progress and fire property change event
                                    setProgress(progress);
                                    
                                    // publish current progress data for copy task
                                    publish(current);
                                }
                            }
                            
                            result = destFile != null ? destFile.getAbsolutePath() : "Error";
                        } catch (IOException e) {
                        	JOptionPane.showMessageDialog( new JFrame() , e.getMessage());
                            result = "Error";
                            cancel(true);
                        }
                        finally{
                           
                        	try {
								if(in!=null)
									in.close();
								if(out!=null)
									out.close();
							} catch (IOException e) {
								//JOptionPane.showMessageDialog( new JFrame() , e.getMessage());
								result = "Error";
							}
                        }
                	}
                }
            }
            
            return null;
        }

        // process copy task progress data in the event dispatch thread
        @Override
        public void process(List<CopyData> data) {
            if(isCancelled()) { return; }
            CopyData update  = new CopyData(0, "", 0, 0);
            for (CopyData d : data) {
                // progress updates may be batched, so get the most recent
                if (d.getKiloBytesCopied() > update.getKiloBytesCopied()) {
                    update = d;
                }
            }
            
            // update the progress monitor's status note with the
            // latest progress data from the copy operation, and
            // additionally append the note to the console
            String progressNote = update.getKiloBytesCopied() + " of " 
                                  + update.getTotalKiloBytes() + " kb copied.";
            String fileNameNote = "Now copying " + update.getFileName();
            
            if (update.getProgress() < 100) {
                progressMonitor.setNote(progressNote + " " + fileNameNote);
                console.append(progressNote + "\n" + fileNameNote + "\n");
            } else {
                progressMonitor.setNote(progressNote);
                console.append(progressNote + "\n");
            }           
        }
        
        // perform final updates in the event dispatch thread
        @Override
        public void done() {
            try {
                // call get() to tell us whether the operation completed or 
                // was canceled; we don't do anything with this result
                Void result = get();
            
                console.append("Copy operation completed. " + result + "\n");                
            } catch (InterruptedException e) {
            	result = "Error";
            } catch (CancellationException e) {
                // get() throws CancellationException if background task was canceled
            	result = "Error";
                console.append("Copy operation canceled.\n");
            } catch (ExecutionException e) {
            	result = "Error";
                console.append("Exception occurred: " + e.getCause());
            }
            // reset the example app
            progressMonitor.setProgress(0);
            progressMonitor.close();
            copyButton.setEnabled(true);
            
        }

        private long calcTotalBytes(File[] files) {
            long tmpCount = 0;
            for (File f : files) {
                tmpCount += f.length();
            }
            return tmpCount;
        }
        
        private long getTotalKiloBytes(long totalBytes) {
            return Math.round(totalBytes / 1024);
        }

        private long getKiloBytesCopied(long bytesCopied) {
            return Math.round(bytesCopied / 1024);
        }
    }
    
    class CopyData {        
        private int progress;
        private String fileName;
        private long totalKiloBytes;
        private long kiloBytesCopied;
        
        CopyData(int progress, String fileName, long totalKiloBytes, long kiloBytesCopied) {
            this.progress = progress;
            this.fileName = fileName;
            this.totalKiloBytes = totalKiloBytes;
            this.kiloBytesCopied = kiloBytesCopied;
        }

        int getProgress() {
            return progress;
        }
        
        String getFileName() {
            return fileName;
        }

        long getTotalKiloBytes() {
            return totalKiloBytes;
        }

        long getKiloBytesCopied() {
            return kiloBytesCopied;
        }
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		this.dispose();
		client.doPostProcess( operation.getResult(), operation.taskMode );
	}
}

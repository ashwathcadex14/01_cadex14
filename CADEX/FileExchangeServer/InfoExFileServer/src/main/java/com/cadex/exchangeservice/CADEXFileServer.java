/**=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PVT. LTD                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CADEXFileServer.java          
#      Module          :           com.cadex.exchangeservice          
#      Description     :           File Server REST Implementation for upload and download of the Packages.          
#      Project         :           InfoExFileServer          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  08-Aug-2015                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.cadex.exchangeservice;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.naming.InitialContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import com.cadex.exchangeservice.model.ExchangePackage;
import com.cadex.exchangeservice.model.ExchangeSettings;
import com.cadex.exchangeservice.model.ExchangeSettings.FILE_STORAGE;
import com.cadex.exchangeservice.model.PackageModelObjectStore;
import com.cadex.exchangeservice.model.TransferUid;
import com.cadex.exchangeservice.model.TransferUid.TRANSFER_STATUS;
import com.cadex.exchangeservice.ui.InPkgUploadForm;
import com.cadex.exchangeservice.ui.OutPkgUploadForm;
import com.cadex.exchangeservice.utils.FileServerUtils;

/**
 * File Server REST Implementation for upload and download
 * of the Packages.
 * @author Ashwath
 *
 */
@Path("/file-management")
public class CADEXFileServer
{
	private static final Logger logger = Logger
			.getLogger(CADEXFileServer.class.getName());
	
	/**
	 * Uploads incoming package to File Server
	 * @param form
	 * @return response xml
	 */
    @POST
    @Path("/uploadInPkg")
    public Response uploadInPkg(@MultipartForm InPkgUploadForm form)
    {   
    	logger.debug(" ----- >> Entering Function uploadInPkg with parameters ...." + form);
    	String tferUid = form.getTransferUid();
    	logger.debug( "Processing for Transfer UID : " + tferUid );
        String fileName = form.getFileName() == null ? "Unknown" : form.getFileName() ;
        
        String outFilePath ;
        
        String retString = "";
        
        ExchangeSettings exSett = null;
       
        try
        {
    		//PackageModelObjectStore.beginTransaction();
    		
    		TransferUid tfer = PackageModelObjectStore.getTransfer(TransferUid.class, tferUid);
    		
    		//PackageModelObjectStore.commitTransaction();
    		
    		if(tfer != null)
    		{
    			logger.info( "Transfer UID is Available - " + tferUid );
    			if(form.getType() == FILE_STORAGE.HTTP.value())
            	{
            		outFilePath = uploadFile(fileName, form.getFileData());
                    
                    exSett = new ExchangeSettings();
                    exSett.setType( FILE_STORAGE.HTTP );
                    logger.debug( "FILE STORAGE IS HTTP" );
            	}
            	else
            	{
            		outFilePath = form.getFileName();
            		exSett = new ExchangeSettings(form.getFileUrl());
            		logger.debug( "FILE STORAGE IS FTP OR SHARED DRIVE" );
            	}
    			
    			PackageModelObjectStore.beginTransaction();
    			
    			ExchangePackage pkg = new ExchangePackage();
        		pkg.setItemId(tfer.getOutPkgId().getItemId());
        		pkg.setRevId(tfer.getOutPkgId().getRevId());
        		pkg.setSupplierId( tfer.getOutPkgId().getSupplierId() );
        		pkg.setFileName( outFilePath );
        		
        		pkg.setExSettings( exSett );
        		
        		tfer.setInPkgId( pkg );
        		tfer.setPkgStatus( TRANSFER_STATUS.UPLOADED_BY_SUPPLIER );
        		
        		PackageModelObjectStore.merge( tfer );
        		
        		PackageModelObjectStore.commitTransaction();
        		
        		logger.info( "Transfer UID is updated." );
        		
        		retString = "Transfer UID is " +tfer.getId();
    		}
    		else
    			throw new Exception("Specified transfer UID does not exist.");
    		
        }
        catch (Exception e)
        {
        	retString = "Error uploading the file : " + fileName + "\n Error details : \n" + e.getMessage() + "\n" + FileServerUtils.stackTraceToString(e.getCause());
        	logger.error( retString );
        }
        
        logger.debug(" << ----- Leaving Function uploadInPkg ....");
        //Build a response to return
        return Response.status(200)
                .entity(retString).build();
    }
    
    /**
     * Uploads the outgoing package to File Server
     * @param form
     * @return response xml with the transfer uid
     */
    @POST
    @Path("/uploadOutPkg")
    @Consumes("multipart/form-data")
    public Response uploadOutPkg(@MultipartForm OutPkgUploadForm form) {
  
    	logger.debug(" ----- >> Entering Function uploadOutPkg with parameters ...." + form);
    	
        String fileName = form.getFileName() == null ? "Unknown" : form.getFileName() ;
                
        String outFilePath ;
        
        String retString = "";
        
        ExchangeSettings exSett = null;
       
        try
        {
        	
        	if(form.getType() == FILE_STORAGE.HTTP.value())
        	{
        		outFilePath = uploadFile(fileName, form.getFileData());
                
                exSett = new ExchangeSettings();
                exSett.setType( FILE_STORAGE.HTTP );
                logger.info( "Transfer mechanism is HTTP" );
        	}
        	else if(form.getType() == FILE_STORAGE.FTP.value())
        	{
        		outFilePath = form.getFileName();
        		exSett = new ExchangeSettings(form.getFileUrl());
        		logger.info( "Transfer mechanism is FTP" );
        	}
        	else
        	{
        		outFilePath = form.getFileName();
        		exSett = new ExchangeSettings();
        		exSett.setType( FILE_STORAGE.SHARED_DRIVE );
        		logger.info( "Transfer mechanism is SHARED_DRIVE" );
        	}
        	
    		PackageModelObjectStore.beginTransaction();
    		
    		ExchangePackage pkg = new ExchangePackage();
    		pkg.setItemId(form.getItemId());
    		pkg.setRevId(form.getRevId());
    		pkg.setSupplierId( form.getSupplierId() );
    		pkg.setFileName( outFilePath );
    		
    		pkg.setExSettings( exSett );
    		
    		TransferUid newUid = new TransferUid();
    		newUid.setOutPkgId( pkg );
    		
    		PackageModelObjectStore.store( newUid );
    		
    		PackageModelObjectStore.commitTransaction();
    		logger.info( "Transfer UID is created." );
    		
    		retString = "Transfer UID is " +newUid.getId();
        }
        catch (Exception e)
        {
        	retString = "Error uploading the file : " + fileName + "\n" + e.getMessage() + "\n" + FileServerUtils.stackTraceToString(e.getCause());
        	logger.error( retString );
        }
        
        logger.debug(" << ----- Leaving Function uploadOutPkg ....");
        
        //Build a response to return
        return Response.status(200)
                .entity(retString).build();
    }
    
    /**
     * Downloads the exchange package from the File Server
     * @param transferUid - Transfer UID of the package
     * @param inrout - Incoming or outgoing package
     * @param initDload - append the package with response if transfer mode is HTTP
     * @return response with details of the package to initiate download
     */
    @GET
    @Path("/downloadPkg/{inrout}/{tferUid}/{initDload}")
    @Produces(MediaType.APPLICATION_XML)
    public Response downloadExchangePackage(@PathParam("tferUid") String transferUid, @PathParam("inrout") int inrout, @PathParam("initDload") int initDload)
    {
    	logger.debug(" ----- >> Entering Function downloadExchangePackage with parameters ...." + transferUid + inrout + initDload);
    	String retString = "";
    	
    	Response resp ;
    	
        logger.info("Transfer UID requested is : " + transferUid);
         
        //Put some validations here such as invalid file name or missing file name
        if(transferUid == null || transferUid.isEmpty())
        {
            ResponseBuilder response = Response.status(Status.BAD_REQUEST);
            logger.error("Transfer UID requested is invalid " + transferUid);
            return response.build();
        }
        
        try 
        {
			TransferUid tUid = PackageModelObjectStore.getTransfer(TransferUid.class, transferUid);
			
			ExchangePackage ctxtPkg = null ;
			if(tUid != null)
			{
				if(inrout == 1)
					ctxtPkg = tUid.getInPkgId();
				else
					ctxtPkg = tUid.getOutPkgId();
				 
				if(ctxtPkg != null)
				{
					ExchangeSettings exSett = ctxtPkg.getExSettings();
					
					if(exSett.getType() == FILE_STORAGE.HTTP)
					{
						
						if(initDload == 0)
						{
							retString += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
							retString += "<DownloadDetails id=\"cadex\">" ;
							retString += "<Type>HTTP</Type>";
							retString += "</DownloadDetails>";
							resp = Response.status(200)
							        .entity(retString).build();
							logger.info("Downloading initiated for " + transferUid + " via http with initDload as 0");
						}
						else
						{
							File dFile = new File( ctxtPkg.getFileName() );
							ResponseBuilder respBuilder = Response.ok((Object) dFile);
							respBuilder.header("Content-Disposition", "attachment; filename=\""+ dFile.getName() + "\"");
							resp = respBuilder.build();
							logger.info("Downloading initiated for " + transferUid + " via http with initDload as 1");
						}
					}
					else
					{
						retString += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
						retString += "<DownloadDetails id=\"cadex\">" ;
						retString += "<Type>" + (exSett.getType() == FILE_STORAGE.FTP ? "FTP" : "SHARED") + "</Type>";
						retString += "<Host>" + exSett.getHost() + "</Host>"; 
						retString += "<User>" + exSett.getUser() + "</User>";
						retString += "<Pass>" + exSett.getPass() + "</Pass>"; 
						retString += "<Path>" + ctxtPkg.getFileName() + "</Path>";
						retString += "</DownloadDetails>";
						
						resp = Response.status(200)
				        .entity(retString).build();
						logger.info("Download details have been prepared in the response");
					}
					
				}
				else
				{
					throw new Exception( "Package does not exists for the transfer UID");
				}
				
				
				PackageModelObjectStore.beginTransaction();
				
				tUid.setPkgStatus( inrout == 1 ? TRANSFER_STATUS.RECEIVED_FROM_SUPPLIER : TRANSFER_STATUS.DOWNLOADED_BY_SUPPLIER);
				
				PackageModelObjectStore.commitTransaction();
				logger.info("Transfer UID is updated with status");
			}
			else
				throw new Exception("Transfer UID does not exist.");
			
		} catch (Exception e) {
			retString += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
			retString += "<Error>Error occured while downloading the file" + e.getMessage() + "\n" + FileServerUtils.stackTraceToString(e.getCause()) + "</Error>";
			resp = Response.status(200)
		    .entity(retString).build();
			logger.error(retString);
		}

        logger.debug(" << ----- Leaving Function downloadExchangePackage ....");
        
        return resp;
    }
    

    /**
     * Gets the connection details for FTP and shared drive transfer mechanism.
     * @return - response with the connection details
     * @throws Exception
     */
    @GET
    @Path("/connectiondetails")
    @Produces(MediaType.APPLICATION_XML)
    public Response getConnectionDetails() throws Exception
    {
    	logger.debug(" ----- >> Entering Function getConnectionDetails with parameters ...." );
    	String retString = "";

    	InitialContext ctx = new InitialContext();
		String ftpHost = (String) ctx.lookup( "java:comp/env/ftpHost" );
		String ftpUser = (String) ctx.lookup( "java:comp/env/ftpUser" );
		String ftpPass = (String) ctx.lookup( "java:comp/env/ftpPassword" );
		String ftpPort = (String) ctx.lookup( "java:comp/env/ftpPort" );
		String sharedDrive = (String) ctx.lookup( "java:comp/env/defaultSharedDrive" );
    	
		retString += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		retString += "<Connection>";
		retString += "<FTP host=\"" + ftpHost + "\" user=\"" + ftpUser + "\" pass=\"" + ftpPass + "\" port=\"" + ftpPort + "\"/>";
		retString += "<Shared path=\"" + sharedDrive + "\"/>";
		retString += "</Connection>";
    	Response resp = Response.status(200)
    		    .entity(retString).build();
    	logger.debug(" << ----- Leaving Function getConnectionDetails ....");
        return resp;
    }
    
    /**
     * Uploads the file
     * @param fileName
     * @param fileData
     * @return
     * @throws Exception
     */
    private String uploadFile(String fileName, byte[] fileData) throws Exception
    {
    	logger.debug(" ----- >> Entering Function uploadFile with parameters ...." + fileName + fileData);
    	FileOutputStream fos = null;
    	
    	String outFilePath ;
    	
    	try 
    	{
			InitialContext ctx = new InitialContext();
			String path = (String) ctx.lookup( "java:comp/env/fileRepo" );
			
			SimpleDateFormat dFormat = new SimpleDateFormat( "ddMMyyyy_HHmmssSSS" );
			String newDir = "cadex_exchange_" + dFormat.format( Calendar.getInstance().getTime() );
			
			File newDirFile = new File( path + newDir);
			
			newDirFile.mkdir();
			
			logger.info( "New directory " + newDirFile.getAbsolutePath() + " is created." );
			
			String completeFilePath = newDirFile.getAbsolutePath() + "\\" + fileName;
			
			File file = new File(completeFilePath);
			
			if (!file.exists())
			{
			    file.createNewFile();
			}
  
			fos = new FileOutputStream(file);
  
			fos.write(fileData);
			
			outFilePath = file.getAbsolutePath();
		} catch (Exception e)
        {
			logger.error( "Unable to upload file - " + e.getMessage() + "\n" + FileServerUtils.stackTraceToString(e.getCause()) );
        	throw new Exception("Unable to upload file");
        }
        finally{
            try {
            	if(fos != null)
            	{
            		fos.flush();
            		fos.close();
            	}
            	logger.info( "Flushed out all the streams" );
			} catch (IOException e) {
				logger.error( "Unable to upload file - " + e.getMessage() + "\n" + FileServerUtils.stackTraceToString(e.getCause()) );
				throw new Exception("Unable to upload file");
			}
        }
    	logger.debug(" << ----- Leaving Function uploadFile ....");
    	return outFilePath;
    }
     
}

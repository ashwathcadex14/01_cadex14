/**=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PVT. LTD                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           ExchangePackage.java          
#      Module          :           com.cadex.exchangeservice.model        
#      Description     :           Entity implementation class for Entity: Package          
#      Project         :           InfoExFileServer          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  08-Aug-2015                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.cadex.exchangeservice.model;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlRootElement;
/**
 * Entity implementation class for Entity: Package
 *
 */
@Entity
@XmlRootElement
public class ExchangePackage implements Serializable {

	   
	@Id
	@GeneratedValue
	private String Id;
	private String itemId;
	private String revId;
	private String supplierId;
	private String fileName;
	private String parentPath;
	@OneToOne(cascade = ALL)
	private ExchangeSettings exSettings;
	private static final long serialVersionUID = 1L;

	public ExchangePackage() {
		super();
	}   
	public String getId() {
		return this.Id;
	}

	public void setId(String Id) {
		this.Id = Id;
	}   
	public String getItemId() {
		return this.itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}   
	public String getRevId() {
		return this.revId;
	}

	public void setRevId(String revId) {
		this.revId = revId;
	}   
	public String getSupplierId() {
		return this.supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	/**
	 * @return the parentPath
	 */
	public String getParentPath() {
		return parentPath;
	}
	/**
	 * @param parentPath the parentPath to set
	 */
	public void setParentPath(String parentPath) {
		this.parentPath = parentPath;
	}

	/**
	 * @return the exSettings
	 */
	public ExchangeSettings getExSettings() {
		return exSettings;
	}
	/**
	 * @param exSettings the exSettings to set
	 */
	public void setExSettings(ExchangeSettings exSettings) {
		this.exSettings = exSettings;
	}
	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}

/**=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PVT. LTD                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           ExchangeSettings.java          
#      Module          :           com.cadex.exchangeservice.model      
#      Description     :           Entity implementation class for Entity: ExchangeSettings          
#      Project         :           InfoExFileServer          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  08-Aug-2015                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.cadex.exchangeservice.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entity implementation class for Entity: ExchangeSettings
 *
 */
@Entity
@XmlRootElement
public class ExchangeSettings implements Serializable {

	
	@Id
	@GeneratedValue
	private String Id;
	private String host;
	private String user;
	private String pass;
	private FILE_STORAGE type;
	private static final long serialVersionUID = 1L;

	public ExchangeSettings() {
		super();
	}
	
	public ExchangeSettings(String url) throws Exception {
		this();
		String[] splits = url.split( "\\|" );
		if(splits.length == 4)
		{
			host = splits[0];
			user = splits[1];
			pass = splits[2];
			type = splits[3].equals( "1" ) ? FILE_STORAGE.FTP : (splits[3].equals( "2" ) ? FILE_STORAGE.SHARED_DRIVE : FILE_STORAGE.HTTP);
		}
		else
			throw new Exception("The URL specified is not right. Please ensure the url is correct.");
	} 
	
	public String getHost() {
		return this.host;
	}

	public void setHost(String host) {
		this.host = host;
	}   
	public String getUser() {
		return this.user;
	}

	public void setUser(String user) {
		this.user = user;
	}   
	public String getPass() {
		return this.pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}   
	public FILE_STORAGE getType() {
		return this.type;
	}

	public void setType(FILE_STORAGE type) {
		this.type = type;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return Id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		Id = id;
	}
	
//	@Override
//	public String toString() {
//		
//		String retString = "<Settings ";
//		
//		retString += "host=\"" + host + "\" user=\"" + user + "\" pass=\"" + pass + "\" type=\"" + type + "\"/>";
//		
//		return retString;
//	}
   
	public enum FILE_STORAGE
	{
		FTP(1),
		SHARED_DRIVE(2),
		HTTP(3);
		
		private final int value ;
		
		FILE_STORAGE(int value){
			this.value = value;
		}
		
		public int value(){return value;}
	}
}

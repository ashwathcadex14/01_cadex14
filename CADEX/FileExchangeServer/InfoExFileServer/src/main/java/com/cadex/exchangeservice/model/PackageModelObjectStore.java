/**=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PVT. LTD                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           PackageModelObjectStore.java          
#      Module          :           com.cadex.exchangeservice.model         
#      Description     :           Central storage for all the outgoing and incoming packages          
#      Project         :           InfoExFileServer          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  08-Aug-2015                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.cadex.exchangeservice.model;

import java.io.File;
import java.util.HashMap;
import java.util.logging.Logger;

import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 * Central storage for all the outgoing and incoming packages
 * @author Ashwath
 *
 */
public class PackageModelObjectStore {
	
	private static final Logger logger = Logger
			.getLogger(PackageModelObjectStore.class.getName());
	
	private static final String PERSISTENCE_UNIT_NAME = "CADEXFileExchangeService";
	private static final String DB_PATH = "C:\\apps\\Siemens\\01_CADEX14\\TC10\\Development\\Client\\EclipseProject\\workdir";
	private static EntityManagerFactory factory;
	private EntityManager em;
	
	static PackageModelObjectStore docObjectStore ;
	
	private PackageModelObjectStore( String dbName, boolean exists ) throws Exception {
		logger
				.info(" ----- >> Entering Function PackageModelObjectStore with parameters ...."
						+ "dbName - "
						+ dbName
						+ "exists - " + exists);
		setUp(dbName, exists);
		logger.info(" << ----- Leaving Function PackageModelObjectStore ....");
	}
	
	private void setUp( String dbName, boolean exists) throws Exception {
		
		logger.info(" ----- >> Entering Function setUp with parameters ...."
				+ "dbName - " + dbName + "exists - "
				+ exists);
		HashMap<String,String> optMap = new HashMap<String, String>();
		
		//optMap.put( "javax.persistence.jdbc.url" , "jdbc:h2:file:" + dbPath + dbName );
		optMap.put( "eclipselink.ddl-generation" , exists ? "none" : "create-tables" );
		
	    factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME,optMap);
	    em = factory.createEntityManager();
	    
		logger.info(" << ----- Leaving Function setUp ....");
	}
	
	private static void getInstance(String dbName, String dbPath) throws Exception
	{
		logger
				.info(" ----- >> Entering Function getInstance with parameters ...."
						+ "dbName - " + dbName + "dbPath - " + dbPath);
		boolean exists = false;
		
		InitialContext ctx = new InitialContext();
		dbName = (String) ctx.lookup( "java:comp/env/DB_NAME" );
		logger.info( "Changing Database name based on the env entry DB_NAME to " + dbName  );
		dbPath = (String) ctx.lookup( "java:comp/env/DB_PATH" );
		logger.info( "Changing Database name based on the env entry DB_PATH to " + dbPath  );
		
		if( new File(dbPath + "\\" + dbName + ".db").exists())
				 exists = true;
			 
		docObjectStore = new PackageModelObjectStore(dbName, exists);
		refresh();
		logger.info(" << ----- Leaving Function getInstance ....");
		
	}
	
	public static boolean isActive()
	{
		logger
				.info(" ----- >> Entering Function isActive with parameters ....");
		logger.info(" << ----- Leaving Function isActive ....");
		return docObjectStore.em == null || !docObjectStore.em.getTransaction().isActive() ? false : true;
	}
	
	public static void beginTransaction() throws Exception
	{
		logger
				.info(" ----- >> Entering Function beginTransaction with parameters ....");
		getInstance(  PERSISTENCE_UNIT_NAME, DB_PATH);
		
		if(!isActive())
			docObjectStore.em.getTransaction().begin();
		
		logger.info(" << ----- Leaving Function beginTransaction ....");
	}
	
	public static void commitTransaction()
	{
		logger
				.info(" ----- >> Entering Function commitTransaction with parameters ....");
		docObjectStore.em.getTransaction().commit();
		logger.info(" << ----- Leaving Function commitTransaction ....");
	}
	
	public static void store(Object modelObject)
	{
		logger.info(" ----- >> Entering Function store with parameters ...."
				+ modelObject);
		docObjectStore.em.persist( modelObject );
		logger.info(" << ----- Leaving Function store ....");
	}
	
	public static void remove(ExchangePackage modelObject)
	{
		logger.info(" ----- >> Entering Function remove with parameters ...."
				+ modelObject);
		docObjectStore.em.remove( modelObject );
		logger.info(" << ----- Leaving Function remove ....");
	}
	
	@SuppressWarnings("unused")
	private static void close()
	{
		logger.info(" ----- >> Entering Function close with parameters ....");
		docObjectStore.em.close();
		logger.info(" << ----- Leaving Function close ....");
	}
	
	public static ExchangePackage get( Class<ExchangePackage> modelClass, String uid ) throws Exception
	{
		logger.info(" ----- >> Entering Function get with parameters ...."
				+ "modelClass - " + modelClass + "uid - " + uid);
		getInstance(  PERSISTENCE_UNIT_NAME, DB_PATH);
		Query q = docObjectStore.em.createQuery( "Select m from ExchangePackage m where m.ID='" + uid  +  "'" );
		logger.info(" << ----- Leaving Function get ....");
		return q.getResultList() != null && q.getResultList().size() > 0 ? (ExchangePackage) q.getSingleResult() : null ;//(ItemModelObject) em.find( modelClass, ItemModelObject.getPersistenceId( modelClass , uid ) );
	}
	
	public static TransferUid getTransfer( Class<TransferUid> modelClass, String uid ) throws Exception
	{
		logger.info(" ----- >> Entering Function get with parameters ...."
				+ "modelClass - " + modelClass + "uid - " + uid);
		getInstance(  PERSISTENCE_UNIT_NAME, DB_PATH);
		
		TransferUid tfer = docObjectStore.em.find( TransferUid.class , uid);
		
		//Query q = docObjectStore.em.createQuery( "Select m from TransferUid m where m.id='" + uid  +  "'" );
		logger.info(" << ----- Leaving Function get ....");
		//return q.getResultList() != null && q.getResultList().size() > 0 ? (TransferUid) q.getSingleResult() : null ;//(ItemModelObject) em.find( modelClass, ItemModelObject.getPersistenceId( modelClass , uid ) );
		return tfer;
	}
	
	private static void refresh()
	{
		logger.info(" ----- >> Entering Function refresh with parameters ....");
		docObjectStore.em.getEntityManagerFactory().getCache().evictAll();
		logger.info(" << ----- Leaving Function refresh ....");
	}

	public static void merge(TransferUid comp) {
		logger.info(" ----- >> Entering Function merge with parameters ...."
				+ comp);
		docObjectStore.em.merge(comp);
		logger.info(" << ----- Leaving Function merge ....");		
	}
	
	@SuppressWarnings("unused")
	private static void flush()
	{
		logger.info(" ----- >> Entering Function flush with parameters ....");
		docObjectStore.em.flush();
		logger.info(" << ----- Leaving Function flush ....");
	}
	
	@Override
	protected void finalize() throws Throwable {
		logger
				.info(" ----- >> Entering Function finalize with parameters ....");
		em.close();
		super.finalize();
		logger.info(" << ----- Leaving Function finalize ....");
		
	}

	@Override

	public String toString() {
		String strValue = super.toString();
		return strValue;
	}
}

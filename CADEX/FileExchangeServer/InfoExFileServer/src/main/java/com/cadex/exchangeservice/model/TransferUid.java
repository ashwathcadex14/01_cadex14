/**=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PVT. LTD                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           TransferUid.java          
#      Module          :           com.cadex.exchangeservice.model          
#      Description     :           Entity implementation class for Entity: TransferUid          
#      Project         :           InfoExFileServer          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  08-Aug-2015                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.cadex.exchangeservice.model;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entity implementation class for Entity: TransferUid
 *
 */
@Entity
@XmlRootElement
public class TransferUid implements Serializable {

	   
	@Id
	@GeneratedValue
	private String Id;
	@OneToOne(cascade = ALL, fetch = EAGER)
	private ExchangePackage outPkgId;
	@OneToOne(cascade = ALL, fetch = EAGER)
	private ExchangePackage inPkgId;
	private TRANSFER_STATUS pkgStatus;
	private static final long serialVersionUID = 1L;

	public TransferUid() {
		super();
		this.pkgStatus = TRANSFER_STATUS.SENT_TO_SUPPLIER;
	}   
	
	public String getId() {
		return this.Id;
	}
	
	public void setId(String Id) {
		this.Id = Id;
	}
	
	/**
	 * @return the outPkgId
	 */
	public ExchangePackage getOutPkgId() {
		return outPkgId;
	}

	/**
	 * @param outPkgId the outPkgId to set
	 */
	public void setOutPkgId(ExchangePackage outPkgId) {
		this.outPkgId = outPkgId;
	}

	/**
	 * @return the inPkgId
	 */
	public ExchangePackage getInPkgId() {
		return inPkgId;
	}

	/**
	 * @param inPkgId the inPkgId to set
	 */
	public void setInPkgId(ExchangePackage inPkgId) {
		this.inPkgId = inPkgId;
	}

	public TRANSFER_STATUS getPkgStatus() {
		return pkgStatus;
	}
	
	public void setPkgStatus(TRANSFER_STATUS pkgStatus) {
		this.pkgStatus = pkgStatus;
	}
	

	public enum TRANSFER_STATUS
	{
		SENT_TO_SUPPLIER(1),
		DOWNLOADED_BY_SUPPLIER(2),
		UPLOADED_BY_SUPPLIER(3),
		RECEIVED_FROM_SUPPLIER(4);
		
		private final int value ;
		
		TRANSFER_STATUS(int value){
			this.value = value;
		}
		
		public int value(){return value;}
	}
}

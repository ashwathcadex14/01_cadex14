/**=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PVT. LTD                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           AbstractFileDownloadForm.java          
#      Module          :           com.cadex.exchangeservice.ui         
#      Description     :           Abstract Layer for File Download Form          
#      Project         :           InfoExFileServer          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  08-Aug-2015                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.cadex.exchangeservice.ui;

import javax.ws.rs.FormParam;

/**
 * Abstract Layer for File Download Form
 * @author Ashwath
 *
 */
public abstract class AbstractFileDownloadForm {

	private String transferUid ;

	/**
	 * @return the transferUid
	 */
	public String getTransferUid() {
		return transferUid;
	}

	/**
	 * @param transferUid the transferUid to set
	 */
	@FormParam("tferUid")
	public void setTransferUid(String transferUid) {
		this.transferUid = transferUid;
	}
}

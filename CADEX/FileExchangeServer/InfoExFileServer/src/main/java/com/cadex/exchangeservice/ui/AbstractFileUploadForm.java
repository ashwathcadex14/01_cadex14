/**=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PVT. LTD                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           AbstractFileUploadForm.java          
#      Module          :           com.cadex.exchangeservice.ui       
#      Description     :           Abstract Layer for File Upload Form          
#      Project         :           InfoExFileServer          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  08-Aug-2015                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.cadex.exchangeservice.ui;

import javax.ws.rs.FormParam;
import org.jboss.resteasy.annotations.providers.multipart.PartType;

/**
 * Abstract Layer for File Upload Form
 * @author Ashwath
 *
 */
public abstract class AbstractFileUploadForm {
 
    public AbstractFileUploadForm() {
    }
 
    private byte[] fileData;
    private String fileName;
 
    public byte[] getFileData() {
        return fileData;
    }
    
    public String getFileName() {
        return fileName;
    }
 
    @FormParam("fileName")
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
 
    @FormParam("selectedFile")
    @PartType("application/octet-stream")
    public void setFileData(byte[] fileData) {
        this.fileData = fileData;
    }
}

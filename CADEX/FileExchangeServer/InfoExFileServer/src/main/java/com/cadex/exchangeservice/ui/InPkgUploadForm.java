/**=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PVT. LTD                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           InPkgUploadForm.java          
#      Module          :           com.cadex.exchangeservice.ui       
#      Description     :           Incoming package upload form          
#      Project         :           InfoExFileServer          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  08-Aug-2015                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.cadex.exchangeservice.ui;

import javax.ws.rs.FormParam;

/**
 * Incoming package upload form
 * @author Ashwath
 *
 */
public class InPkgUploadForm extends AbstractFileUploadForm {

	private String transferUid ;
	private String fileUrl;
	private int type;
	
	public InPkgUploadForm() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @return the transferUid
	 */
	public String getTransferUid() {
		return transferUid;
	}
	/**
	 * @param transferUid the transferUid to set
	 */
	@FormParam("tferUid")
	public void setTransferUid(String transferUid) {
		this.transferUid = transferUid;
	}
	
	/**
	 * @return the fileUrl
	 */
	public String getFileUrl() {
		return fileUrl;
	}

	/**
	 * @param fileUrl the fileUrl to set
	 */
	@FormParam("fileURL")
	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	@FormParam("uploadType")
	public void setType(int type) {
		this.type = type;
	}
}

/**=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PVT. LTD                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           OutPkgUploadForm.java          
#      Module          :           com.cadex.exchangeservice.ui     
#      Description     :           Outgoing package upload form          
#      Project         :           InfoExFileServer          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  08-Aug-2015                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.cadex.exchangeservice.ui;

import javax.ws.rs.FormParam;

/**
 * Outgoing package upload form
 * @author Ashwath
 *
 */
public class OutPkgUploadForm extends AbstractFileUploadForm{
	
	private String itemId ;
	private String revId ;
	private String supplierId ;
	private String fileUrl;
	private int type;
	
	public OutPkgUploadForm() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the itemId
	 */
	public String getItemId() {
		return itemId;
	}

	/**
	 * @param itemId the itemId to set
	 */
	@FormParam("itemId")
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	/**
	 * @return the revId
	 */
	public String getRevId() {
		return revId;
	}

	/**
	 * @param revId the revId to set
	 */
	@FormParam("revId")
	public void setRevId(String revId) {
		this.revId = revId;
	}

	/**
	 * @return the supplierId
	 */
	public String getSupplierId() {
		return supplierId;
	}

	/**
	 * @param supplierId the supplierId to set
	 */
	@FormParam("supplierId")
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	/**
	 * @return the fileUrl
	 */
	public String getFileUrl() {
		return fileUrl;
	}

	/**
	 * @param fileUrl the fileUrl to set
	 */
	@FormParam("fileURL")
	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	@FormParam("uploadType")
	public void setType(int type) {
		this.type = type;
	}

	
}

/**=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PVT. LTD                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           PkgDownloadForm.java          
#      Module          :           com.cadex.exchangeservice.ui   
#      Description     :           Download Package Form          
#      Project         :           InfoExFileServer          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  08-Aug-2015                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.cadex.exchangeservice.ui;

import javax.ws.rs.FormParam;

/**
 * Download Package Form
 * @author Ashwath
 *
 */
public class PkgDownloadForm extends AbstractFileDownloadForm {

	private int inOrOut ;
	
	public PkgDownloadForm() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the inOrOut
	 */
	public int getInOrOut() {
		return inOrOut;
	}

	/**
	 * @param inOrOut the inOrOut to set
	 */
	@FormParam("inrout")
	public void setInOrOut(int inOrOut) {
		this.inOrOut = inOrOut;
	}

}

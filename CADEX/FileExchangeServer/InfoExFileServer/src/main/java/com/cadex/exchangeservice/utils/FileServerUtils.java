/**=================================================================================================
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PVT. LTD                    
#                  Unpublished - All Rights Reserved                    
#  ================================================================================================= 
#      Filename        :           FileServerUtils.java          
#      Module          :           com.cadex.exchangeservice.utils          
#      Description     :           <DESCRIPTION>          
#      Project         :           InfoExFileServer          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                         Name           Description of Change
#  10-Aug-2015				      Ashwath			Initial Creation
#  $HISTORY$                  
#  =================================================================================================*/
package com.cadex.exchangeservice.utils;

import org.apache.log4j.Logger;

/**
 * @author Ashwath
 *
 */
public class FileServerUtils {
	
	private static final Logger logger = Logger
			.getLogger(FileServerUtils.class.getName());
	
	public static String stackTraceToString(Throwable e) {
	    logger
				.debug(" ----- >> Entering Function stackTraceToString with parameters ...."
						+ e);
		StringBuilder sb = new StringBuilder();
	    for (StackTraceElement element : e.getStackTrace()) {
	        sb.append(element.toString());
	        sb.append("\n");
	    }
	    logger.debug(" << ----- Leaving Function stackTraceToString ....");
		return sb.toString();
	}
}

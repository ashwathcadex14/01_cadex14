<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--/**=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PVT. LTD                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           DownloadPackage.jsp          
#      Module          :           com.cadex.exchangeservice          
#      Description     :           Download Package Form for Web File Server          
#      Project         :           InfoExFileServer          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  08-Aug-2015                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/ -->                    
<html>
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/fileserver.js"></script>
<script src="js/deployjava.js"></script>
<script type="text/javascript">
	
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Upload Outgoing Package</title>
</head>
<body>
	<form id="downForm" action="fileserver/file-management/downloadPkg" method="get" enctype="multipart/form-data">
            <p>
	            Transfer UID : <input type="text" name="tferUid" id="tUid"/>
	        </p>
	        <p>
	            Initiate Download : <input type="hidden" name="initDownload" id="initD"/>
	        </p>
            <p>
                Download Type : <input type="radio" name="inrout" value="1" >Incoming Package
                <input type="radio" name="inrout" value="2" >Outgoing Package
            </p>
            <input value="Download" onclick="javascript:downloadPkg()" type="button" />
    </form>
        <div>
        </div>
           <applet  id="cadexClient" width="676" height="527">
			<param name="jnlp_href" value="http://localhost:8090/InfoExFileServer/js/cadexClient.jnlp"/>
			<param name="image" value="http://localhost:8090/InfoExFileServer/js/images/loadingbar.gif"/>
			<param name="boxborder" value="true"/>
			<param name="centerimage" value="true"/>
			<param name="codebase_lookup" value="false"/>
		  </applet>
</body>
</html>
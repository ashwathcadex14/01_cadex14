<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--/**=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PVT. LTD                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           IncomingPackage.jsp          
#      Module          :           com.cadex.exchangeservice          
#      Description     :           Upload Incoming Package Form for Web File Server          
#      Project         :           InfoExFileServer          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  08-Aug-2015                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/ -->                    
<html>
<Connections id="conns"></Connections>
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/fileserver.js"></script>
<script src="js/deployjava.js"></script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Upload Incoming Package</title>
</head>
<body>  
			<form id="uploadForm" action="fileserver/file-management/uploadInPkg" method="post" enctype="multipart/form-data">
	            
	            <p>
	                Please select the Upload Type
	            </p>
	            
	            <p>
	           	 	Upload Type : 
	                <input type="radio" name="uploadType" value="1" >FTP
	                <input type="radio" name="uploadType" value="2" >Shared Drive
	                <input type="radio" name="uploadType" value="3" >HTTP
	            </p>
	            
	            <p>
	                Transfer UID : <input type="text" name="tferUid" />
	            </p>
            <p id="fileTxt">
                File name : <input type="text" name="fileName" id="fileN"/>
            </p>
	            <p id="fileSelect">
	                Choose the file : <input type="file" name="selectedFile" />
	            </p>
            <p>
                File URL : <input type="text" name="fileURL" id="fileU"/>
            </p>

	            <input id="uploadButton" type="button" value="Upload" onclick="javascript:uploadFile()"/>
	            <input id="commitButton" type="submit" value="Commit" />
            </form>
        <div id="cadexApplet">
          <applet  id="cadexClient" width="676" height="527">
			<param name="jnlp_href" value="http://localhost:8090/InfoExFileServer/js/cadexClient.jnlp"/>
			<param name="image" value="http://localhost:8090/InfoExFileServer/js/images/loadingbar.gif"/>
			<param name="boxborder" value="true"/>
			<param name="centerimage" value="true"/>
			<param name="codebase_lookup" value="false"/>
		  </applet>
        </div>
</body>
</html>
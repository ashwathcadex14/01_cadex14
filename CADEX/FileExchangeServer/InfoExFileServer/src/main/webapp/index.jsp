<!--/**=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PVT. LTD                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           index.jsp          
#      Module          :           com.cadex.exchangeservice          
#      Description     :           Welcome page for Web File Server          
#      Project         :           InfoExFileServer          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  08-Aug-2015                      Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/ -->                    
<html>
    <body>
        <h1>File Exchange Server</h1>
        <ul>Upload
        		<li><a href="/InfoExFileServer/OutgoingPackage.jsp">Outgoing Package</a></li>
        		<li><a href="/InfoExFileServer/IncomingPackage.jsp">Incoming Package</a></li>
        </ul>	
        <ul>Download
        		<li><a href="/InfoExFileServer/DownloadPackage.jsp">Download Package</a></li>	
        </ul>
    </body>
</html>

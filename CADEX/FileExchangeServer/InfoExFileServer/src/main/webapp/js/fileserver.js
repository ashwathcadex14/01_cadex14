/**=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PVT. LTD                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           fileserver.js          
#      Module          :           com.cadex.exchangeservice          
#      Description     :           Javascript for all file upload/download operations from Browser          
#      Project         :           InfoExFileServer          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  08-Aug-2015                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                   
	var conns ;
	var ftpConn;
	var ftpHost ;
	var ftpUser ;
	var ftpPass ;
	var ftpPort ;
	var shConn ;
	var shPath ;
	var formTag = document.getElementById("upForm");

	jQuery(function ($) {
	    $("input:radio[name=uploadType]").click(disp);
	});
	
	$(document).ready(function() {
    $.ajax({
        url: "http://localhost:8090/InfoExFileServer/fileserver/file-management/connectiondetails"
    }).then(function(data) {
    	var ele = document.getElementById("conns");
    	ele.innerHTML = new XMLSerializer().serializeToString(data.documentElement);
    	$("#commitButton").hide();
    	conns = document.getElementById("conns");
    	ftpConn = conns.firstChild.getElementsByTagName("FTP")[0];
    	ftpHost = ftpConn.getAttribute("host");
    	ftpUser = ftpConn.getAttribute("user");
    	ftpPass = ftpConn.getAttribute("pass");
    	ftpPort = ftpConn.getAttribute("port");
    	shConn = conns.firstChild.getElementsByTagName("Shared")[0];
    	shPath = shConn.getAttribute("path");
//    	deployApplet();
    });
});

	function deployApplet()
	{
		var attributes = { id: 'cadexClient',  code:'com.cadex.exchange.client.CADEXDownloadClient.class', archive:'CadExchanger.jar',  width:676, height:527} ;
	    var parameters = {jnlp_href: 'http://localhost:8090/InfoExFileServer/js/cadexClient.jnlp', image: 'http://localhost:8090/InfoExFileServer/js/images/loadingbar.gif' , boxborder: 'true' , centerimage: 'true' , codebase_lookup: 'false'} ;
	    deployJava.runApplet(attributes, parameters, '1.7');
	}
	
	function disp()
	{
		var str = $('input:radio[name=uploadType]:checked').val();
		
		if( str == "1" || str == "2" )
		{
			$('#fileTxt').hide();
			$('#fileSelect').hide();
		}
		else if(str == "3")
		{
			$('#fileTxt').show();
			$('#fileSelect').show();	
		}
	}
	
	function uploadFile()
	{
		
		
		if( formTag != null && !validateUpload())
		{
			alert('Please enter mandatory fields.');
			return;
		}
		
		var str = $('input:radio[name=uploadType]:checked').val();
		
		if( str == "1" )
		{
			cadexClient.uploadFTP(ftpHost + ":" + ftpPort, ftpUser, ftpPass);
		}
		else if(str == "2")
		{
			cadexClient.uploadSharedDrive("", "", shPath);
		}
		else if(str == "3")
		{
			if(formTag == null)
				document.getElementById("upForm").submit();
			else
				document.getElementById("uploadForm").submit();
		}
		else
		{
			alert('Please select a upload type.');
		}
	}
	
	
	function validateUpload()
	{
		var itemId = document.getElementById("item_id").value;
		var revId = document.getElementById("item_revision_id").value;
		var suppId = document.getElementById("supplier_id").value;
		
		if(itemId.length == 0 || revId.length == 0 || suppId.length == 0)
			return false;
		else
			return true;
	}

	function uploadFinished(result)
	{
		if(result == "Error")
			alert("Error occured while upload. Please try again.");
		else
		{
			var str = $('input:radio[name=uploadType]:checked').val();
			
			if( str == "1" )
			{
				document.getElementById("fileU").value = ftpHost + "#" + ftpPort + "|" + ftpUser + "|" + ftpPass + "|" + str;
			}			
			
			document.getElementById("fileN").value = result;
			
			
			if(formTag == null)
				document.getElementById("upForm").submit();
			else
				document.getElementById("uploadForm").submit();
		}
			
	}
	
	function downloadFinished(result)
	{
		if(result == "Error")
			alert("Error occured while upload. Please try again.");
		else
		{
			alert("Download Successful !");
		}
			
	}
	
	function downloadPkg()
	{
		  var url = $("#downForm").attr("action");
		  var newParam = "/" + $('input:radio[name=inrout]:checked').val() + "/" + document.getElementById("tUid").value;
		  url += newParam ;
		  var initUrl = url + "/0";
		  $.ajax({
			  type: "GET",
		        url: initUrl,
		        success: function(data) {
		        	var typearray = data.getElementsByTagName("Type")[0].textContent;
		        	if(typearray == "HTTP")
	        		{
		        		var initUrl1 = url + "/1";
		      		  	$("#downForm").attr("action", initUrl1);
		      		  	$("#downForm")[0].submit();
	        		}
		        	else
	        		{
		        		if( typearray == "FTP" )
		        		{
		        			var ftpHost = data.getElementsByTagName("Host")[0].textContent;
		        			var ftpUser = data.getElementsByTagName("User")[0].textContent;
		        			var ftpPass = data.getElementsByTagName("Pass")[0].textContent;
		        			var ftpPath = data.getElementsByTagName("Path")[0].textContent;
		        			cadexClient.downloadFTP(ftpHost, ftpUser, ftpPass, ftpPath);
		        		}
		        		else if(typearray == "SHARED")
		        		{
		        			var path = data.getElementsByTagName("Path")[0].textContent;
		        			cadexClient.downloadSharedDrive("", "", path);
		        		}
	        		}
		        }
		    });	  
    }
	
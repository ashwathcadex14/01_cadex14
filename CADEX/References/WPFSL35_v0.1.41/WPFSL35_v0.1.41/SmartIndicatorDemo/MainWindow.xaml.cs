﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using SoftArcs.WPFSmartLibrary.SmartUserControls;

namespace SmartIndicatorDemo
{
	/// <summary>
	/// Interaktionslogik für MainWindow.xaml
	/// </summary>
	public partial class MainWindow
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private void RadioButton_Click(object sender, RoutedEventArgs e)
		{
			// ReSharper disable PossibleNullReferenceException
			switch ((sender as RadioButton).Name)
			// ReSharper restore PossibleNullReferenceException
			{
				case "rbNotAvailable":
					this.AvailabilityIndicator.IndicatorState = IndicatorLevel.Low;
					break;
				case "rbNearTermAvailable":
					this.AvailabilityIndicator.IndicatorState = IndicatorLevel.Middle;
					break;
				case "rbInStock":
					this.AvailabilityIndicator.IndicatorState = IndicatorLevel.High;
					break;
			}
		}

		//private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
		//{
		//   this.TestLabel.Background = Brushes.Red;
		//}
	}
}

﻿using SoftArcs.WPFSmartLibrary.MVVMCore;

namespace SmartIndicatorDemo.ViewModels
{
	public class LoginViewModel : ViewModelBase
	{
		public string Password
		{
			get { return GetValue( () => Password ); }
			set { SetValue( () => Password, value ); }
		}
	}
}

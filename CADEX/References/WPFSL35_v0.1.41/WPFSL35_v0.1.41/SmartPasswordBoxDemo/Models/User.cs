﻿// ReSharper disable RedundantUsingDirective
using System;
// ReSharper restore RedundantUsingDirective

namespace SmartPasswordBoxDemo.Models
{
	public class User
	{
		public string Username { get; set; }
		public string Password { get; set; }
	}
}

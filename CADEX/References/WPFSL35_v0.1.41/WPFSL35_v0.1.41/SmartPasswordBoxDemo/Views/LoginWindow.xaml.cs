﻿using SmartPasswordBoxDemo.ViewModels;

namespace SmartPasswordBoxDemo.Views
{
	public partial class LoginWindow
	{
		#region Fields

		public LoginViewModel ViewModel;

		#endregion

		#region Constructor

		public LoginWindow()
		{
			InitializeComponent();

			this.ViewModel = new LoginViewModel();
			this.DataContext = this.ViewModel;
		}

		#endregion
	}
}

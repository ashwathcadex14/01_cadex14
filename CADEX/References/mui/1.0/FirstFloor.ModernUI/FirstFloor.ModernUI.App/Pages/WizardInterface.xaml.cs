﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows.Controls;

namespace FirstFloor.ModernUI.App.Pages
{
    /// <summary>
    /// Interaction logic for WizardInterface.xaml
    /// </summary>
    public partial class WizardInterface : UserControl
    {
        public WizardInterface()
        {
            InitializeComponent();
            //PageCollection pages = new PageCollection();
            //WizardPage page1 = new WizardPage();
            //page1.Source = new Uri("/Content/LayoutBasic.xaml", UriKind.Relative);
            //page1.DisplayName = "Page1";
            //WizardPage page2 = new WizardPage();
            //page2.Source = new Uri("/Pages/LayoutBasic.xaml", UriKind.Relative);
            //page2.DisplayName = "Page2";
            //WizardPage page3 = new WizardPage();
            //page3.Source = new Uri("/Pages/LayoutBasic.xaml", UriKind.Relative);
            //page3.DisplayName = "Page3";
            //pages.Add(page1);
            //pages.Add(page2);
            //pages.Add(page3);
            //this.mainWizard.Pages = pages;
            //this.mainWizard.SelectedSource = new Uri("/Pages/LayoutBasic.xaml", UriKind.Relative);
        }

        private void MainWizard_OnOnPageChanged(object sender, RoutedEventArgs e)
        {
            WizardPageChangedEventArgs args = ((WizardPageChangedEventArgs) e);

            if(args.EventTypeOfWizard == WizardEventType.BackButtonClicked)
                args.StopNavigation = true;
            
        }
    }
}

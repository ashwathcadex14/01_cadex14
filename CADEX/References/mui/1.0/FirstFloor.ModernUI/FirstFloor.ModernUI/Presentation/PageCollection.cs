﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FirstFloor.ModernUI.Presentation
{
    /// <summary>
    /// Represents an observable collection of wizard pages.
    /// </summary>
    public class PageCollection
        : ObservableCollection<WizardPage>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PageCollection"/> class.
        /// </summary>
        public PageCollection()
        {
            CollectionChanged += OnCollectionChanged;
        }

        private void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            SetFirstOrLast();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PageCollection"/> class that contains specified links.
        /// </summary>
        /// <param name="pages">The links that are copied to this collection.</param>
        public PageCollection(IEnumerable<WizardPage> pages) : this()
        {
            if (pages == null)
            {
                throw new ArgumentNullException("pages");
            }
            foreach (var page in pages)
            {
                Add(page);
            }
        }

        private void SetFirstOrLast()
        {
            int idx = 1;
            foreach (WizardPage page in this)
            {
                if (idx == 1)
                    page.IsFirst = true;
                else
                    page.IsFirst = false;

                if (idx == (this.Count))
                    page.IsLast = true;
                else
                    page.IsLast = false;

                page.PageIdx = idx;
                idx++;
            }
        }
    }
}

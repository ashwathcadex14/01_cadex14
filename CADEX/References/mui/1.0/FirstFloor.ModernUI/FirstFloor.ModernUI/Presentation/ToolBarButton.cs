﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace FirstFloor.ModernUI.Presentation
{
    /// <summary>
    /// Tool Bar Button
    /// </summary>
    public class ToolBarButton : DependencyObject
    {
        private PathGeometry _buttonIcon;

        private string _buttonKey;

        private string _buttonTip;

        private string _buttonGeometry;

        private string _buttonCmdName;

        /// <summary>
        /// Command Name to which the click event must be routed
        /// </summary>
        public static DependencyProperty ButtonClickCommandProperty = DependencyProperty.Register("ButtonClickCommand", typeof(ICommand), typeof(ToolBarButton));

        /// <summary>
        /// Button Icon Data
        /// </summary>
        public PathGeometry ButtonIconData
        {
            get
            {
                if (this._buttonIcon == null)
                {
                    PathGeometry pathGeometry = new PathGeometry();
                    pathGeometry.AddGeometry(Geometry.Parse(this._buttonGeometry));
                    this._buttonIcon = pathGeometry;
                }
                return this._buttonIcon;
            }
        }

        /// <summary>
        /// Button Icon Geometry in string format
        /// </summary>
        public string ButtonIconGeometry
        {
            get
            {
                return this._buttonGeometry;
            }
            set
            {
                this._buttonGeometry = value;
            }
        }

        /// <summary>
        /// Unique identifier for the button
        /// </summary>
        public string ButtonKey
        {
            get
            {
                return this._buttonKey;
            }
            set
            {
                this._buttonKey = value;
            }
        }

        /// <summary>
        /// Tooltip for the button
        /// </summary>
        public string ButtonToolTip
        {
            get
            {
                return this._buttonTip;
            }
            set
            {
                this._buttonTip = value;
            }
        }

        /// <summary>
        /// Button Click command
        /// </summary>
        public ICommand ButtonClickCommand
        {
            get
            {
                return (ICommand)base.GetValue(ToolBarButton.ButtonClickCommandProperty);
            }
            set
            {
                base.SetValue(ToolBarButton.ButtonClickCommandProperty, value);
            }
        }

        /// <summary>
        /// Button Command Name
        /// </summary>
        public string ButtonCommandName
        {
            get
            {
                return this._buttonCmdName;
            }
            set
            {
                this._buttonCmdName = value;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="buttonName"></param>
        /// <param name="buttonToolTip"></param>
        /// <param name="iconPath"></param>
        /// <param name="cmdName"></param>
        public ToolBarButton(string buttonName, string buttonToolTip, string iconPath, string cmdName)
        {
            this._buttonKey = buttonName;
            this._buttonGeometry = iconPath;
            this._buttonTip = buttonToolTip;
            this._buttonCmdName = cmdName;
        }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public ToolBarButton()
        {
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace FirstFloor.ModernUI.Presentation
{
    /// <summary>
    /// Collection of Tool Bar Buttons
    /// </summary>
    public class ToolBarButtonCollection : ObservableCollection<ToolBarButton>
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        public ToolBarButtonCollection()
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="toolBarButtons"></param>
        public ToolBarButtonCollection(IEnumerable<ToolBarButton> toolBarButtons)
        {
            if (toolBarButtons == null)
            {
                throw new ArgumentNullException("toolBarButtons");
            }
            foreach (ToolBarButton current in toolBarButtons)
            {
                base.Add(current);
            }
        }
    }
}
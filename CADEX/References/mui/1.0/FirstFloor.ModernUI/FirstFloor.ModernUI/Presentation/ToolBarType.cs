﻿using System;

namespace FirstFloor.ModernUI.Presentation
{
    /// <summary>
    /// Toolbar orientation
    /// </summary>
    public enum ToolBarType
    {
        /// <summary>
        /// Horizontal Toolbar
        /// </summary>
        Horizontal,

        /// <summary>
        /// Vertical Toolbar
        /// </summary>
        Vertical
    }
}

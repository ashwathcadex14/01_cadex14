﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace FirstFloor.ModernUI.Presentation
{
    /// <summary>
    /// Represents a page in the ModernWizard
    /// </summary>
    public class WizardPage : Link
    {
        private WizardPage _nextPage = null;

        private WizardPage _previousPage = null;

        private bool _isFirst = false;

        private bool _isLast = false;

        private int pageIdx = -1;

        private bool _isCompleted = false;

        private Brush _pageColor = new SolidColorBrush(Color.FromRgb(204, 198, 198));

        /// <summary>
        /// Next Page to current page
        /// </summary>
        public WizardPage NextPage
        {
            get
            {
                return _nextPage;
            }
        }

        /// <summary>
        /// Previous Page to current page
        /// </summary>
        public WizardPage PreviousPage
        {
            get
            {
                return _previousPage;
            }
        }

        /// <summary>
        /// Is this page the first page
        /// </summary>
        internal protected bool IsFirst
        {
            set { _isFirst = value; }
        }

        /// <summary>
        /// Is the page first
        /// </summary>
        public bool IsFirstPage
        {
            get{return _isFirst;}
        }

        /// <summary>
        /// Is this page the last page
        /// </summary>
        internal protected bool IsLast
        {
            set { _isLast = value; }
        }

        /// <summary>
        /// Is the page last
        /// </summary>
        public bool IsLastPage
        {
            get { return _isLast; }
        }

        /// <summary>
        /// Sets the PageIdx
        /// </summary>
        internal protected int PageIdx 
        {
            set
            {
                pageIdx = value;
            }
        }

        /// <summary>
        /// Gets the page Index
        /// </summary>
        public int PageNo
        {
            get
            {
                return pageIdx;
            }
        }

        /// <summary>
        /// Denotes whether is page is complete or not
        /// </summary>
        public bool IsPageCompleted
        {
            get { return _isCompleted; }
            set
            {
                _isCompleted = value;
                if (_isCompleted)
                {
                    PageColor = new SolidColorBrush(Color.FromRgb(51, 204, 51));
                }
                //else
                //{
                //    PageColor = new SolidColorBrush(Color.FromRgb(145,145,145));
                //}
                OnPropertyChanged("PageColor");
            }
        }

        /// <summary>
        /// Colour of the page
        /// </summary>
        public Brush PageColor
        {
            get { return _pageColor; }
            set
            {
                _pageColor = value;
                OnPropertyChanged("PageColor");
            }
        }
    }
}

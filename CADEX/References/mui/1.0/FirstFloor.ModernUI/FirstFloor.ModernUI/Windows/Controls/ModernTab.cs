﻿using FirstFloor.ModernUI.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace FirstFloor.ModernUI.Windows.Controls
{
    /// <summary>
    /// Represents a control that contains multiple pages that share the same space on screen.
    /// </summary>
    public class ModernTab
        : Control
    {
        /// <summary>
        /// Identifies the ContentLoader dependency property.
        /// </summary>
        public static readonly DependencyProperty ContentLoaderProperty = DependencyProperty.Register("ContentLoader", typeof(IContentLoader), typeof(ModernTab), new PropertyMetadata(new DefaultContentLoader()));
        /// <summary>
        /// Identifies the Layout dependency property.
        /// </summary>
        public static readonly DependencyProperty LayoutProperty = DependencyProperty.Register("Layout", typeof(TabLayout), typeof(ModernTab), new PropertyMetadata(TabLayout.Tab));
        /// <summary>
        /// Identifies the ListWidth dependency property.
        /// </summary>
        public static readonly DependencyProperty ListWidthProperty = DependencyProperty.Register("ListWidth", typeof(GridLength), typeof(ModernTab), new PropertyMetadata(new GridLength(170)));
        /// <summary>
        /// Identifies the Links dependency property.
        /// </summary>
        public static readonly DependencyProperty LinksProperty = DependencyProperty.Register("Links", typeof(LinkCollection), typeof(ModernTab), new PropertyMetadata(OnLinksChanged));
        /// <summary>
        /// Identifies the SelectedSource dependency property.
        /// </summary>
        public static readonly DependencyProperty SelectedSourceProperty = DependencyProperty.Register("SelectedSource", typeof(Uri), typeof(ModernTab), new PropertyMetadata(OnSelectedSourceChanged));
        /// <summary>
        /// Identifies the ContentColumn dependency property.
        /// </summary>
        public static readonly DependencyProperty ContentColumnProperty = DependencyProperty.Register("ContentColumn", typeof(int), typeof(ModernTab), new PropertyMetadata(2));
        /// <summary>
        /// Identifies the ShowTab dependency property.
        /// </summary>
        public static readonly DependencyProperty ShowTabProperty = DependencyProperty.Register("ShowTab", typeof(Visibility), typeof(ModernTab), new PropertyMetadata(new PropertyChangedCallback(ModernTab.OnShowTabPropertyChanged)));
        /// <summary>
        /// Identifies the ContentColumnSpan dependency property.
        /// </summary>
        public static readonly DependencyProperty ContentColumnSpanProperty = DependencyProperty.Register("ContentColumnSpan", typeof(int), typeof(ModernTab), new PropertyMetadata(2));

        private ModernFrame mainFrame;

        /// <summary>
        /// Occurs when the selected source has changed.
        /// </summary>
        public event EventHandler<SourceEventArgs> SelectedSourceChanged;

        private ListBox linkList;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModernTab"/> control.
        /// </summary>
        public ModernTab()
        {
            this.DefaultStyleKey = typeof(ModernTab);

            // create a default links collection
            SetCurrentValue(LinksProperty, new LinkCollection());
        }

        private static void OnLinksChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ((ModernTab)o).UpdateSelection();
        }

        private static void OnSelectedSourceChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ((ModernTab)o).OnSelectedSourceChanged((Uri)e.OldValue, (Uri)e.NewValue);
        }

        private void OnSelectedSourceChanged(Uri oldValue, Uri newValue)
        {
            UpdateSelection();

            // raise SelectedSourceChanged event
            var handler = this.SelectedSourceChanged;
            if (handler != null) {
                handler(this, new SourceEventArgs(newValue));
            }
        }

        private void UpdateSelection()
        {
            if (this.linkList == null || this.Links == null) {
                return;
            }

            // sync list selection with current source
            this.linkList.SelectedItem = this.Links.FirstOrDefault(l => l.Source == this.SelectedSource);
        }

        /// <summary>
        /// When overridden in a derived class, is invoked whenever application code or internal processes call System.Windows.FrameworkElement.ApplyTemplate().
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            if (this.linkList != null) {
                this.linkList.SelectionChanged -= OnLinkListSelectionChanged;
            }

            this.linkList = GetTemplateChild("LinkList") as ListBox;
            if (this.linkList != null) {
                this.linkList.SelectionChanged += OnLinkListSelectionChanged;
            }
            this.mainFrame = (base.GetTemplateChild("mainFrame") as ModernFrame);
            UpdateSelection();
        }

        private void OnLinkListSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var link = this.linkList.SelectedItem as Link;
            if (link != null && link.Source != this.SelectedSource) {
                SetCurrentValue(SelectedSourceProperty, link.Source);
            }
        }

        private static void OnShowTabPropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ModernTab modernTab = (ModernTab)o;
            modernTab.ContentColumn = (((Visibility)e.NewValue == Visibility.Collapsed) ? 0 : 2);
            modernTab.ContentColumnSpan = (((Visibility)e.NewValue == Visibility.Collapsed) ? 3 : 2);
        }

        /// <summary>
        /// Gets or sets the content loader.
        /// </summary>
        public IContentLoader ContentLoader
        {
            get { return (IContentLoader)GetValue(ContentLoaderProperty); }
            set { SetValue(ContentLoaderProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating how the tab should be rendered.
        /// </summary>
        public TabLayout Layout
        {
            get { return (TabLayout)GetValue(LayoutProperty); }
            set { SetValue(LayoutProperty, value); }
        }

        /// <summary>
        /// Gets or sets the collection of links that define the available content in this tab.
        /// </summary>
        public LinkCollection Links
        {
            get { return (LinkCollection)GetValue(LinksProperty); }
            set { SetValue(LinksProperty, value); }
        }

        /// <summary>
        /// Gets or sets the width of the list when Layout is set to List.
        /// </summary>
        /// <value>
        /// The width of the list.
        /// </value>
        public GridLength ListWidth
        {
            get { return (GridLength)GetValue(ListWidthProperty); }
            set { SetValue(ListWidthProperty, value); }
        }

        /// <summary>
        /// Gets or sets the source URI of the selected link.
        /// </summary>
        /// <value>The source URI of the selected link.</value>
        public Uri SelectedSource
        {
            get { return (Uri)GetValue(SelectedSourceProperty); }
            set { SetValue(SelectedSourceProperty, value); }
        }

        /// <summary>
        /// Show Tab or not
        /// </summary>
        public Visibility ShowTab
        {
            get
            {
                return (Visibility)base.GetValue(ModernTab.ShowTabProperty);
            }
            set
            {
                base.SetValue(ModernTab.ShowTabProperty, value);
            }
        }

        /// <summary>
        /// Content Column
        /// </summary>
        public int ContentColumn
        {
            get
            {
                return (int)base.GetValue(ModernTab.ContentColumnProperty);
            }
            set
            {
                base.SetValue(ModernTab.ContentColumnProperty, value);
            }
        }

        /// <summary>
        /// Content Column Span
        /// </summary>
        public int ContentColumnSpan
        {
            get
            {
                return (int)base.GetValue(ModernTab.ContentColumnSpanProperty);
            }
            set
            {
                base.SetValue(ModernTab.ContentColumnSpanProperty, value);
            }
        }

        /// <summary>
        /// Frame Content
        /// </summary>
        public ContentControl FrameContent
        {
            get
            {
                return (ContentControl)this.mainFrame.Content;
            }
        }
    }
}

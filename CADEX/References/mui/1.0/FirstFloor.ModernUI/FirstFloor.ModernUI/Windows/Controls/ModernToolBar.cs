﻿using FirstFloor.ModernUI.Presentation;
using System;
using System.Windows;
using System.Windows.Controls;

namespace FirstFloor.ModernUI.Windows.Controls
{
    /// <summary>
    /// Modern Tool bar
    /// </summary>
    public class ModernToolBar : UserControl
    {
        /// <summary>
        /// Tool bar orientation
        /// </summary>
        public static DependencyProperty ToolBarOrientationProperty = DependencyProperty.Register("ToolBarOrientation", typeof(ToolBarType), typeof(ModernToolBar), new PropertyMetadata(new PropertyChangedCallback(ModernToolBar.OnToolBarOrientationPropertyChanged)));

        /// <summary>
        /// Toolbar buttons
        /// </summary>
        public static DependencyProperty ToolBarButtonsProperty = DependencyProperty.Register("ToolBarButtons", typeof(ToolBarButtonCollection), typeof(ModernToolBar), new PropertyMetadata(new PropertyChangedCallback(ModernToolBar.OnToolBarButtonsPropertyChanged)));

        /// <summary>
        /// Toolbar Orientation
        /// </summary>
        public ToolBarType ToolBarOrientation
        {
            get
            {
                return (ToolBarType)base.GetValue(ModernToolBar.ToolBarOrientationProperty);
            }
            set
            {
                base.SetValue(ModernToolBar.ToolBarOrientationProperty, value);
            }
        }

        /// <summary>
        /// Toolbar buttons
        /// </summary>
        public ToolBarButtonCollection ToolBarButtons
        {
            get
            {
                return (ToolBarButtonCollection)base.GetValue(ModernToolBar.ToolBarButtonsProperty);
            }
            set
            {
                base.SetValue(ModernToolBar.ToolBarButtonsProperty, value);
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public ModernToolBar()
        {
            base.DefaultStyleKey = typeof(ModernToolBar);
        }

        private static void OnToolBarOrientationPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
        }

        private void toolBarButton_Click(object sender, RoutedEventArgs e)
        {
        }

        private static void OnToolBarButtonsPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
        }
    }
}

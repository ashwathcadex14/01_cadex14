﻿using FirstFloor.ModernUI.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using FirstFloor.ModernUI.Windows.Navigation;

namespace FirstFloor.ModernUI.Windows.Controls
{
    /// <summary>
    /// Represents a control that contains multiple pages that share the same space on screen.
    /// This a improvement from ModernTab to bring about a Wizard interface.
    /// To be continued to enhance the coupling between pages
    /// </summary>
    public class ModernWizard
        : Control
    {
        /// <summary>
        /// Identifies the ContentLoader dependency property.
        /// </summary>
        public static readonly DependencyProperty ContentLoaderProperty = DependencyProperty.Register("ContentLoader", typeof(IContentLoader), typeof(ModernWizard), new PropertyMetadata(new DefaultContentLoader()));
        /// <summary>
        /// Identifies the Links dependency property.
        /// </summary>
        public static readonly DependencyProperty PagesProperty = DependencyProperty.Register("Pages", typeof(PageCollection), typeof(ModernWizard), new PropertyMetadata(OnPagesChanged));
        /// <summary>
        /// Identifies the SelectedSource dependency property.
        /// </summary>
        public static readonly DependencyProperty SelectedSourceProperty = DependencyProperty.Register("SelectedSource", typeof(Uri), typeof(ModernWizard));
        /// <summary>
        /// Identifies the WizardTitle dependency property.
        /// </summary>
        public static readonly DependencyProperty WizardTitleProperty = DependencyProperty.Register("WizardTitle", typeof(string), typeof(ModernWizard));
        /// <summary>
        /// Identifies the SelectedPage dependency property.
        /// </summary>
        public static readonly DependencyProperty SelectedPageProperty = DependencyProperty.Register("SelectedPage",
            typeof (int), typeof (ModernWizard), new PropertyMetadata(OnSelectedPageChanged));
        /// <summary>
        /// Identifies the BackButtonEnabled dependency property.
        /// </summary>
        public static readonly DependencyProperty BackButtonEnabledProperty = DependencyProperty.Register("BackButtonEnabled",
            typeof(bool), typeof(ModernWizard));
        /// <summary>
        /// Identifies the NextButtonEnabled dependency property.
        /// </summary>
        public static readonly DependencyProperty NextButtonEnabledProperty = DependencyProperty.Register("NextButtonEnabled",
            typeof(bool), typeof(ModernWizard));
        /// <summary>
        /// Identifies the FinishButtonEnabled dependency property.
        /// </summary>
        public static readonly DependencyProperty FinishButtonEnabledProperty = DependencyProperty.Register("FinishButtonEnabled",
            typeof(bool), typeof(ModernWizard));

        private ModernFrame mainFrame;
        private Button backButton;
        private Button nextButton;
        private Button finishButton;
        private Button cancelButton;
        private WizardPage activePage;

        /// <summary>
        /// Event for Next or Back click
        /// </summary>
        public static readonly RoutedEvent WizardPageChangedEvent = EventManager.RegisterRoutedEvent("WizardPageChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ModernWizard));

        private ListBox linkList;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModernWizard"/> control.
        /// </summary>
        public ModernWizard()
        {
            this.DefaultStyleKey = typeof(ModernWizard);

            // create a default links collection
            SetCurrentValue(PagesProperty, new PageCollection());
        }

        private static void OnPagesChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ((ModernWizard)o).UpdateSelection();
        }

        private static void OnSelectedPageChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ((ModernWizard)o).OnSelectedPageChanged((int)e.NewValue);
        }

        private void OnSelectedPageChanged(int selectedPage)
        {
            this.activePage = GetActivePage();
            SetActivePageColor();
            this.linkList.SelectedIndex = selectedPage-1;

            if (SelectedPage == 1)
            {
                BackButtonEnabled = false;
                if (Pages.Count > 1)
                    NextButtonEnabled = true;
            }
            else
            {
                BackButtonEnabled = true;
                if (SelectedPage == (Pages.Count))
                {
                    NextButtonEnabled = false;
                    FinishButtonEnabled = true;
                }
                else
                {
                    NextButtonEnabled = true;
                    FinishButtonEnabled = false;
                } 
            }
        }

        private void UpdateSelection()
        {
            if (this.linkList == null || this.Pages == null) {
                return;
            }
            this.SelectedSource = this.Pages.First<WizardPage>().Source;
            this.SelectedPage = this.Pages.First<WizardPage>().PageNo;
        }

        /// <summary>
        /// When overridden in a derived class, is invoked whenever application code or internal processes call System.Windows.FrameworkElement.ApplyTemplate().
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            if (this.linkList != null) {
                this.linkList.SelectionChanged -= OnLinkListSelectionChanged;
            }

            this.linkList = GetTemplateChild("LinkList") as ListBox;
            if (this.linkList != null) {
                this.linkList.SelectionChanged += OnLinkListSelectionChanged;
            }
            this.mainFrame = (base.GetTemplateChild("mainFrame") as ModernFrame);
            this.mainFrame.Navigated += MainFrameOnNavigated;
            this.backButton = (base.GetTemplateChild("backButton") as Button);
            this.backButton.Click += BackButton_Clicked;
            this.nextButton = (base.GetTemplateChild("nextButton") as Button);
            this.nextButton.Click += NextButton_Clicked;
            this.finishButton = (base.GetTemplateChild("finishButton") as Button);
            this.finishButton.Click += FinishButton_Clicked;
            this.cancelButton = (base.GetTemplateChild("cancelButton") as Button);
            this.cancelButton.Click += CancelButton_Clicked;
            UpdateSelection();
        }

        private void MainFrameOnNavigated(object sender, NavigationEventArgs navigationEventArgs)
        {
            object loadedContent = navigationEventArgs.Content;
            ((UserControl) loadedContent).DataContext = this.DataContext;
        }

        private void OnLinkListSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var link = this.linkList.SelectedItem as Link;
            if (link != null && link.Source != this.SelectedSource) {
                SetCurrentValue(SelectedSourceProperty, link.Source);
            }
        }

        /// <summary>
        /// Event triggered when Next or Back Button is clicked
        /// </summary>
        public event RoutedEventHandler WizardPageChanged
        {
            add { AddHandler(WizardPageChangedEvent, value); }
            remove { RemoveHandler(WizardPageChangedEvent, value); }
        }

        /// <summary>
        /// Gets or sets the content loader.
        /// </summary>
        public IContentLoader ContentLoader
        {
            get { return (IContentLoader)GetValue(ContentLoaderProperty); }
            set { SetValue(ContentLoaderProperty, value); }
        }

        /// <summary>
        /// Gets or sets the collection of wizard pages that define the available content in this tab.
        /// </summary>
        public PageCollection Pages
        {
            get { return (PageCollection)GetValue(PagesProperty); }
            set { SetValue(PagesProperty, value); }
        }

        /// <summary>
        /// Gets or sets the source URI of the selected link.
        /// </summary>
        /// <value>The source URI of the selected link.</value>
        public Uri SelectedSource
        {
            get { return (Uri)GetValue(SelectedSourceProperty); }
            set { SetValue(SelectedSourceProperty, value); }
        }

        /// <summary>
        /// Selected Page
        /// </summary>
        public int SelectedPage
        {
            get { return (int)GetValue(SelectedPageProperty); }
            set { SetValue(SelectedPageProperty, value); }
        }

        /// <summary>
        /// Wizard Title
        /// </summary>
        public string WizardTitle
        {
            get { return (string)GetValue(WizardTitleProperty); }
            set { SetValue(WizardTitleProperty, value); }
        }

        /// <summary>
        /// Frame Content
        /// </summary>
        public ContentControl FrameContent
        {
            get
            {
                return (ContentControl)this.mainFrame.Content;
            }
        }

        /// <summary>
        /// Enable or disable Back Button
        /// </summary>
        public bool BackButtonEnabled
        {
            get { return (bool)GetValue(BackButtonEnabledProperty); }
            set { SetValue(BackButtonEnabledProperty, value); }
        }

        /// <summary>
        /// Enable or disable Next Button
        /// </summary>
        public bool NextButtonEnabled
        {
            get { return (bool)GetValue(NextButtonEnabledProperty); }
            set { SetValue(NextButtonEnabledProperty, value); }
        }

        /// <summary>
        /// Enable or disable Finish Button
        /// </summary>
        public bool FinishButtonEnabled
        {
            get { return (bool)GetValue(FinishButtonEnabledProperty); }
            set { SetValue(FinishButtonEnabledProperty, value); }
        }

        private WizardPage GetActivePage()
        {
            foreach (WizardPage page in Pages)
            {
                if (page.PageNo == SelectedPage)
                    return page;
            }

            return null;
        }

        private void SetActivePageColor()
        {
            GetActivePage().PageColor = new SolidColorBrush(Color.FromRgb(0, 102, 255));
        }

        private void SetDefaultPageColor()
        {
            GetActivePage().PageColor = new SolidColorBrush(Color.FromRgb(204,198,198));
        }

        private void BackButton_Clicked(object sender, RoutedEventArgs routedEventArgs)
        {
            WizardPageChangedEventArgs eventArgs = new WizardPageChangedEventArgs(WizardPageChangedEvent, this, SelectedPage,
                    SelectedPage - 1, WizardEventType.BackButtonClicked);
            RaiseEvent(eventArgs);
            if (!eventArgs.StopNavigation)
            {
                GetActivePage().IsPageCompleted = false;
                SetDefaultPageColor();
                if (SelectedPage > 1)
                    SelectedPage = SelectedPage - 1;
                GetActivePage().IsPageCompleted = false;
            }
            else
            {
                Console.WriteLine("Navigation stopped");
            }
        }

        private void NextButton_Clicked(object sender, RoutedEventArgs routedEventArgs)
        {
            WizardPageChangedEventArgs eventArgs = new WizardPageChangedEventArgs(WizardPageChangedEvent, this, SelectedPage,
                SelectedPage + 1, WizardEventType.NextButtonClicked);
            RaiseEvent(eventArgs);
            if (!eventArgs.StopNavigation)
            {
                GetActivePage().IsPageCompleted = true;
                if (SelectedPage <= (Pages.Count - 1))
                    SelectedPage = SelectedPage + 1;
                GetActivePage().IsPageCompleted = false;
            }
            else
            {
                Console.WriteLine("Navigation stopped");
            }
        }

        private void FinishButton_Clicked(object sender, RoutedEventArgs routedEventArgs)
        {

        }

        private void CancelButton_Clicked(object sender, RoutedEventArgs routedEventArgs)
        {

        }
    }

    /// <summary>
    /// Event args to hold the value of the current page and the next page
    /// </summary>
    public class WizardPageChangedEventArgs : RoutedEventArgs
    {
        private int currentPage = -1;
        private int nextPage = -1;
        private WizardEventType eventType;
        private bool stopPageChange = false;
        /// <summary>
        /// Constructor to initialize the event
        /// </summary>
        /// <param name="routedEvent"></param>
        /// <param name="source"></param>
        /// <param name="currentPage"></param>
        /// <param name="nextPage"></param>
        /// <param name="eventType"></param>
        public WizardPageChangedEventArgs(RoutedEvent routedEvent, object source, int currentPage, int nextPage, WizardEventType eventType)
            : base(routedEvent, source)
        {
            this.currentPage = currentPage;
            this.nextPage = nextPage;
            this.eventType = eventType;
        }

        /// <summary>
        /// Denotes the current page the wizard is in
        /// </summary>
        public int CurrentPage
        {
            get
            {
                return currentPage;
            }
        }

        /// <summary>
        /// Denotes the page the wizard is going to move
        /// </summary>
        public int NextPage
        {
            get
            {
                return nextPage;
            }
        }

        /// <summary>
        /// Event Type to be triggerred
        /// </summary>
        public WizardEventType EventTypeOfWizard
        {
            get
            {
                return eventType;
            }
        }

        /// <summary>
        /// Denotes whether to continue or stop navigation
        /// </summary>
        public bool StopNavigation
        {
            get { return stopPageChange; }
            set { stopPageChange = value; }
        }
    }

    /// <summary>
    /// Type of event when wizard page changes. Next or Back
    /// </summary>
    public enum WizardEventType
    {
        /// <summary>
        /// When Next button is clicked
        /// </summary>
        NextButtonClicked,
        /// <summary>
        /// When Back Button is clicked
        /// </summary>
        BackButtonClicked
    }
}

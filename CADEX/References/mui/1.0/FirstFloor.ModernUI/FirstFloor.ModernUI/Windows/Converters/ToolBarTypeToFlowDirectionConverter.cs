﻿using FirstFloor.ModernUI.Presentation;
using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;

namespace FirstFloor.ModernUI.Windows.Converters
{
    /// <summary>
    /// Converts the toolbar type enum to Toolbar's Orientation
    /// </summary>
    public class ToolBarTypeToFlowDirectionConverter : IValueConverter
    {
        /// <summary>
        /// Converts the Toolbar type enum to Toolbar's stack panel orientation
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value as ToolBarType? == ToolBarType.Horizontal)
            {
                return Orientation.Horizontal;
            }
            return Orientation.Vertical;
        }

        /// <summary>
        /// Convert back
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}

﻿using CadExchange.Database;
using CadExchange.Database.Entities;
using CadExchange.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using CadExchange.Common;

namespace CadExchange
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : System.Windows.Application
    {
        private Splash _splash;

        void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            // Process unhandled exception

            CommonUtil.ShowModernMessageBox("Something unexpected happened while running application. See Error log for additional details.\n" + InfoExCommon.Utils.LoggerUtil.FlattenException(e.Exception), "Error", MessageBoxButton.OK);
            InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Exception.Message, e.Exception);

            // Prevent default unhandled exception processing
            e.Handled = true;
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            try
            {
                InfoExCommon.Utils.LoggerUtil.info(GetType(), "Starting the application...");

                base.OnStartup(e);
                // starts splash in separate GUI thread
                StartSplash();// commented for Dev purposes

                //Start the application for first time usage; Optional Loading components 
                // Will be moved to installation. For now assuming the database to be present when starting application.
                StartAppForFirstTimeUse();

                //Stop the splash
                StopSplash();// commented for Dev purposes

                //Launch Login window // Commented for dev purposes
                LoginWindow loginWindow = new LoginWindow();
                loginWindow.Topmost = true;
                loginWindow.Show();
                loginWindow.Topmost = false;

                //CadexAdminWindow adminwindow = new CadexAdminWindow();
                //CadexSession.createCadexSession("infoexadmin");
                //adminwindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                //adminwindow.SourceInitialized += (s, a) => adminwindow.WindowState = WindowState.Maximized;
                //adminwindow.Show();
                //MahWindow newWindow = new MahWindow();
                //newWindow.Show();

                //CadexUserWindow userwindow = new CadexUserWindow();
                //userwindow.Show();
                //CadexSession.createCadexSession("ashwath");
            }
            catch (Exception ex)
            {
                InfoExCommon.Utils.LoggerUtil.error(GetType(), ex.Message, ex);
            }
        }

        private void StartSplash()
        {
            var thread = new Thread(SplashThread);
            thread.SetApartmentState(ApartmentState.STA);
            thread.IsBackground = true;
            thread.Start();
        }

        private void SplashThread()
        {
            _splash = new Splash();
            _splash.Show();
            System.Windows.Threading.Dispatcher.Run();
            _splash = null;
        }

        private void StopSplash()
        {
            if (_splash == null)
            {
                InfoExCommon.Utils.LoggerUtil.error(GetType(), "Not showing splash screen", new InvalidOperationException("Not showing splash screen"));
                //throw new InvalidOperationException("Not showing splash screen");
                return;
            }


            _splash.Dispatcher.BeginInvokeShutdown(DispatcherPriority.Normal);
        }

        private void StartAppForFirstTimeUse()
        {
            #region Make default user for login available
            //try
            //{
            //    Session.createDB();
            //}
            //catch (Exception e)
            //{
            //    InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);throw;
            //}
            #endregion
            //Default delay of 5 seconds
            Thread.Sleep(5000); // commented for Dev purposes
        }
    }
}

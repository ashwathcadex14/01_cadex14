﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows;

namespace CadExchange.CadexControls
{
    public class CadexColumnCollection : ObservableCollection<CadexColumn>
    {
    }

    public class CadexColumn : DependencyObject
    {
        

        public CadexColumn()
        { 
        }

        /// <summary>
        /// Name
        /// </summary>
        public string Name
        {
            get
            {
                return (string)GetValue(NameProperty);
            }
            set
            {
                SetValue(NameProperty, value);
            }
        }

        public static readonly DependencyProperty NameProperty =
        DependencyProperty.Register(
            "Name",
            typeof(string),
            typeof(CadexColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// BindName
        /// </summary>
        public string BindName
        {
            get
            {
                return (string)GetValue(BindNameProperty);
            }
            set
            {
                SetValue(BindNameProperty, value);
            }
        }

        public static readonly DependencyProperty BindNameProperty =
        DependencyProperty.Register(
            "BindName",
            typeof(string),
            typeof(CadexColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// IsCheck
        /// </summary>
        public bool IsCheck
        {
            get
            {
                return (bool)GetValue(IsCheckProperty);
            }
            set
            {
                SetValue(IsCheckProperty, value);
            }
        }

        public static readonly DependencyProperty IsCheckProperty =
        DependencyProperty.Register(
            "IsCheck",
            typeof(bool),
            typeof(CadexColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// IsRef
        /// </summary>
        public bool IsRef
        {
            get
            {
                return (bool)GetValue(IsRefProperty);
            }
            set
            {
                SetValue(IsRefProperty, value);
            }
        }

        public static readonly DependencyProperty IsRefProperty =
        DependencyProperty.Register(
            "IsRef",
            typeof(bool),
            typeof(CadexColumn),
            new PropertyMetadata(null));

        /// <summary>
        /// IsCombo
        /// </summary>
        public bool IsCombo
        {
            get
            {
                return (bool)GetValue(IsComboProperty);
            }
            set
            {
                SetValue(IsComboProperty, value);
            }
        }

        public static readonly DependencyProperty IsComboProperty =
        DependencyProperty.Register(
            "IsCombo",
            typeof(bool),
            typeof(CadexColumn),
            new PropertyMetadata(null));
    }
}

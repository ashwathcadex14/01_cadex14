﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CadExchange.Pages;
using CadExchange.Tree;
using CadExchange.Utils;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Navigation;
using FirstFloor.ModernUI.Presentation;
using CadExchange.Common;
using System.Collections.ObjectModel;
using System.ComponentModel;
using CadExchange.Database.Entities;
using CadExchange.Database;

namespace CadExchange.CadexControls
{
    /// <summary>
    /// Interaction logic for CadexTreeView.xaml
    /// </summary>
    public partial class CadexTreeView : UserControl, IContent, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        

        /// <summary>
        /// SelectedItem
        /// </summary>
        public ICadexTreeNode SelectedItem
        {
            get
            {
                return (ICadexTreeNode)GetValue(SelectedItemProperty);
            }
            set 
            {
                SetValue(SelectedItemProperty, value);
            }

        }

        public static readonly DependencyProperty SelectedItemProperty =
        DependencyProperty.Register(
            "SelectedItem",
            typeof(ICadexTreeNode),
            typeof(CadexTreeView),
            new PropertyMetadata(default(CadexTreeView), OnSelectedItemPropertyChanged));

        private static void OnSelectedItemPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            
            ((UIElement)d).RaiseEvent(new CadexTreeEventArgs(CadexTreeView.SelectionChangedEvent, d));
        }

        void NotifiyPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));

            
        }

        /// <summary>
        /// TreeSource
        /// </summary>
        public ObservableCollection<ICadexTreeNode> TreeSource
        {
            get
            {
                return (ObservableCollection<ICadexTreeNode>)GetValue(TreeSourceProperty);
            }
            set
            {
                SetValue(TreeSourceProperty, value);
            }
        }

        public static readonly DependencyProperty TreeSourceProperty =
        DependencyProperty.Register(
            "TreeSource",
            typeof(ObservableCollection<ICadexTreeNode>),
            typeof(CadexTreeView),
            new PropertyMetadata(null));


        private static GridViewColumn createColumn(string name, string bindName, bool withDisplayMemberBinding, Object source, bool isCheck, bool isRef, bool isCombo)
        {
            GridViewColumn colum1 = new GridViewColumn();
            colum1.Header = name;
            colum1.Width = Double.NaN;
            if (withDisplayMemberBinding)
            {
                Binding col1 = new Binding();
                col1.Path = new PropertyPath(bindName);
                col1.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                colum1.DisplayMemberBinding = col1;
            }

            if (isCheck)
            {
                CheckNoCheckTemplateSelector sel = new CheckNoCheckTemplateSelector();
                sel.CheckTemplate = (DataTemplate)((CadexTreeView)source).Resources["CellTemplate_Check"];
                sel.NoCheckTemplate = (DataTemplate)((CadexTreeView)source).Resources["CellTemplate_NoCheck"];
                sel.PropertyName = bindName;
                colum1.CellTemplateSelector = sel;
            }
            
            if (isRef)
            {
                colum1.CellTemplate = (DataTemplate)((CadexTreeView)source).Resources["CellTemplate_User"];
            }
            
            if (isCombo)
            {
                colum1.CellTemplate = (DataTemplate)((CadexTreeView)source).Resources["CellTemplate_SelectUser"];
            }
            //else
            //{
            //    colum1.CellTemplate = (DataTemplate)((CadexTreeView)source).Resources["CellTemplate_Other"];
            //}

            return colum1;
        }

        /// <summary>
        /// TreeColumns
        /// </summary>
        public CadexColumnCollection TreeColumns
        {
            get
            {
                return (CadexColumnCollection)GetValue(TreeColumnsProperty);
            }
            set
            {
                SetValue(TreeColumnsProperty, value);
            }
        }

        public static readonly DependencyProperty TreeColumnsProperty =
        DependencyProperty.Register(
            "TreeColumns",
            typeof(CadexColumnCollection),
            typeof(CadexTreeView),
            new PropertyMetadata(default(CadexTreeView),OnTreeColumnsPropertyChanged));

        private static void OnTreeColumnsPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CadexTreeView thisView = ((CadexTreeView)d);
            thisView.resetOtherColumns();
            foreach(CadexColumn column in thisView.TreeColumns)
            {
                GridViewColumn newColumn = createColumn(column.Name, column.BindName, column.IsCheck || column.IsRef || column.IsCombo ? false : true, thisView, column.IsCheck,column.IsRef,column.IsCombo);
                //newColumn.CellTemplate = thisView.Resources["CellTemplate_Other"] as DataTemplate;
                thisView.CadexTreeColumns.Add(newColumn);
                
            }
        }

        private void resetOtherColumns()
        {
            CadexTreeColumns.Clear();
            GridViewColumnCollection tempCollection = new GridViewColumnCollection();
            GridViewColumn nameColumn = createColumn("Name", "Name", false,this,false,false,false);
            nameColumn.CellTemplate = this.Resources["CellTemplate_Name"] as DataTemplate;
            tempCollection.Add(nameColumn);
            CadexTreeColumns = tempCollection;
        }

        /// <summary>
        /// CadexTreeColumns
        /// </summary>
        private GridViewColumnCollection CadexTreeColumns
        {
            get
            {
                return (GridViewColumnCollection)GetValue(CadexTreeColumnsProperty);
            }
            set
            {
                SetValue(CadexTreeColumnsProperty, value);
            }
        }

        public static readonly DependencyProperty CadexTreeColumnsProperty =
        DependencyProperty.Register(
            "CadexTreeColumns",
            typeof(GridViewColumnCollection),
            typeof(CadexTreeView),
            new PropertyMetadata(null));

        /// <summary>
        /// UserList
        /// </summary>
        private IList<User> UserList
        {
            get
            {
                return Session.getUsers();
            }
        }

        public CadexTreeView(List<ICadexTreeNode> root)
        {
            
        }

        public CadexTreeView() : base()
        {
            InitializeComponent();
            CadexTreeColumns = new GridViewColumnCollection();
        }

        private void ResizeColumWidth()
        {
            foreach (GridViewColumn column in this.mainTree.Columns)
                CommonUtil.ResizeGridViewColumn(column);
        }

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
        }

        private void mainTree_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var tree = sender as TreeListView;
            this.SelectedItem = ((ICadexTreeNode)tree.SelectedItem);
        }

        public static readonly RoutedEvent SelectionChangedEvent = EventManager.RegisterRoutedEvent("SelectionChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(CadexTreeView));
        public static readonly RoutedEvent DoubleClickedEvent = EventManager.RegisterRoutedEvent("DoubleClicked", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(CadexTreeView));
        public static readonly RoutedEvent ScrollChangedEvent = EventManager.RegisterRoutedEvent("ScrollChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(CadexTreeView));
        public static readonly RoutedEvent TreeExpandedEvent = EventManager.RegisterRoutedEvent("TreeExpanded", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(CadexTreeView));
        public static readonly RoutedEvent TreeCollapsedEvent = EventManager.RegisterRoutedEvent("TreeCollapsed", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(CadexTreeView));


        public event RoutedEventHandler SelectionChanged
        {
            add { AddHandler(SelectionChangedEvent, value); }
            remove { RemoveHandler(SelectionChangedEvent, value); }
        }

        public event RoutedEventHandler DoubleClicked
        {
            add { AddHandler(DoubleClickedEvent, value); }
            remove { RemoveHandler(DoubleClickedEvent, value); }
        }

        public event RoutedEventHandler TreeExpanded
        {
            add { AddHandler(TreeExpandedEvent, value); }
            remove { RemoveHandler(TreeExpandedEvent, value); }
        }

        public event RoutedEventHandler TreeCollapsed
        {
            add { AddHandler(TreeCollapsedEvent, value); }
            remove { RemoveHandler(TreeCollapsedEvent, value); }
        }

        public event RoutedEventHandler ScrollChanged
        {
            add { AddHandler(ScrollChangedEvent, value); }
            remove { RemoveHandler(ScrollChangedEvent, value); }
        }

        public class CadexTreeEventArgs : RoutedEventArgs
        {
            public CadexTreeEventArgs(RoutedEvent routedEvent, object source)
                : base(routedEvent, source)
            { }
        }

        public class CadexTreeScrollEventArgs : CadexTreeEventArgs
        {
            ScrollChangedEventArgs scrollEventArgs;
            public CadexTreeScrollEventArgs(RoutedEvent routedEvent, object source, ScrollChangedEventArgs scrollArgs)
                : base(routedEvent, source)
            {
                scrollEventArgs = scrollArgs;
            }

            public double VerticalOffset
            {
                get
                {
                    return scrollEventArgs.VerticalOffset;
                }
            }

            public double HorizontalOffset
            {
                get
                {
                    return scrollEventArgs.HorizontalOffset;
                }
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Have to use delegate mechanism. Now the dirty way...
            PopulatePackageTree.PackageStructureNode selectedNode = ((PopulatePackageTree.PackageStructureNode)mainTree.SelectedItem);
            ComboBox box = ((ComboBox)sender);
            User selectedUser = ((User)box.SelectedItem);
            if(selectedUser!=null)
            {
                if (selectedNode != null)
                {
                    try
                    {
                        selectedNode.AssignUser(selectedUser.UserId);
                    }
                    catch (Exception err)
                    {
                        box.SelectedItem = null;
                        CommonUtil.ShowModernMessageBox(err.Message, "Assign Error", MessageBoxButton.OK);
                    }
                }
                else
                {
                    box.SelectedItem = null;
                    CommonUtil.ShowModernMessageBox("Please select a node and try again.", "Assign Error", MessageBoxButton.OK);
                }
            }
        }

        private void mainTree_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            RaiseEvent(new CadexTreeEventArgs(CadexTreeView.DoubleClickedEvent, this));
        }

        private void LayoutRoot_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            CadexTreeScrollEventArgs args = new CadexTreeScrollEventArgs(CadexTreeView.ScrollChangedEvent, this, e);
            RaiseEvent(args);
        }

        private void mainTree_TreeExpanded(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new TreeListView.CadexTreeExpandEventArgs( CadexTreeView.TreeExpandedEvent, sender, ((TreeListView.CadexTreeExpandEventArgs)e).Expand ));
        }

        private void mainTree_TreeCollapsed(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new TreeListView.CadexTreeExpandEventArgs(CadexTreeView.TreeCollapsedEvent, sender, ((TreeListView.CadexTreeExpandEventArgs)e).Expand));
        }

        private void columnScroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
           
        }

        private void itemScroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            ScrollViewer columnScroll = (ScrollViewer)this.mainTree.Template.FindName("columnScroll", this.mainTree);
            if (columnScroll != null)
            {
                columnScroll.ScrollToHorizontalOffset(e.HorizontalOffset);
            }
            CadexTreeScrollEventArgs args = new CadexTreeScrollEventArgs(CadexTreeView.ScrollChangedEvent, this, e);
            RaiseEvent(args);
        }

        public void scrollTree(double horizontalOffset, double verticalOffset)
        {
            ScrollViewer itemScroll = (ScrollViewer)this.mainTree.Template.FindName("itemScroll", this.mainTree);
            if (itemScroll != null)
            {
                itemScroll.ScrollToHorizontalOffset(horizontalOffset);
                itemScroll.ScrollToVerticalOffset(verticalOffset);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using CadExchange.Tree;
using CadExchange.Pages;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows;
using CadExchange.Common;


namespace CadExchange.CadexControls
{
    public class CadexTreeViewModel : DependencyObject, INotifyPropertyChanged
    {
        private ObservableCollection<ICadexTreeNode> _treeRoot = new ObservableCollection<ICadexTreeNode>();
        private CadexTreeView thisView;
        private GridViewColumnCollection _columns = new GridViewColumnCollection();

        public GridViewColumnCollection Fields
        {
            get
            {
                return this._columns;
            }
            set
            {
                this._columns = value;
                NotifiyPropertyChanged("Fields");
            }
        }
        

        public ObservableCollection<ICadexTreeNode> treeRoot 
        { 
            get 
            {
                return _treeRoot;
            }
            set
            {
                this._treeRoot = value;
                NotifiyPropertyChanged("treeRoot");
            }
        }

        public CadexTreeViewModel(List<ICadexTreeNode> root, CadexTreeView view)
        {
            root.ToList<ICadexTreeNode>().ForEach(_treeRoot.Add);
            this.thisView = view;
           
        }

        void NotifiyPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}

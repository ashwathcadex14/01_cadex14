﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CadExchange.CadexControls
{
    /// <summary>
    /// Interaction logic for CadexWorkflowButton.xaml
    /// </summary>
    public partial class CadexWorkflowButton : UserControl
    {
        public static int CADEX_START_BUTTON_TYPE = 1;
        public static int CADEX_MIDDLE_BUTTON_TYPE = 2;
        public static int CADEX_END_BUTTON_TYPE = 3;

        /// <summary>
        /// CadexType
        /// </summary>
        public string CadexType
        {
            get
            {
                return (string)GetValue(CadexTypeProperty);
            }
            set
            {
                SetValue(CadexTypeProperty, value);
            }
        }

        public static readonly DependencyProperty CadexTypeProperty =
        DependencyProperty.Register(
            "CadexType",
            typeof(string),
            typeof(CadexWorkflowButton),
            new PropertyMetadata(default(CadexWorkflowButton), OnCadexTypePropertyChanged));

        private static void OnCadexTypePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
           
        }

        /// <summary>
        /// ButtonContent
        /// </summary>
        public string ButtonContent
        {
            get
            {
                return (string)GetValue(ButtonContentProperty);
            }
            set
            {
                SetValue(ButtonContentProperty, value);
            }
        }

        public static readonly DependencyProperty ButtonContentProperty =
        DependencyProperty.Register(
            "ButtonContent",
            typeof(string),
            typeof(CadexWorkflowButton),
            new PropertyMetadata(default(CadexWorkflowButton), OnButtonContentChanged));

        private static void OnButtonContentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

        }

        /// <summary>
        /// DetailedInfo
        /// </summary>
        public string DetailedInfo
        {
            get
            {
                return (string)GetValue(DetailedInfoProperty);
            }
            set
            {
                SetValue(DetailedInfoProperty, value);
            }
        }

        public static readonly DependencyProperty DetailedInfoProperty =
        DependencyProperty.Register(
            "DetailedInfo",
            typeof(string),
            typeof(CadexWorkflowButton),
            new PropertyMetadata(default(CadexWorkflowButton), OnDetailedInfoChanged));

        private static void OnDetailedInfoChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

        }

        /// <summary>
        /// IsActiveStage
        /// </summary>
        public string IsActiveStage
        {
            get
            {
                return (string)GetValue(IsActiveStageProperty);
            }
            set
            {
                SetValue(IsActiveStageProperty, value);
            }
        }

        public static readonly DependencyProperty IsActiveStageProperty =
        DependencyProperty.Register(
            "IsActiveStage",
            typeof(string),
            typeof(CadexWorkflowButton),
            new PropertyMetadata(default(CadexWorkflowButton), OnIsActiveStagePropertyChanged));

        private static void OnIsActiveStagePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

        }

        /// <summary>
        /// UserName
        /// </summary>
        public string UserName
        {
            get
            {
                return (string)GetValue(UserNameProperty);
            }
            set
            {
                SetValue(UserNameProperty, value);
            }
        }

        public static readonly DependencyProperty UserNameProperty =
        DependencyProperty.Register(
            "UserName",
            typeof(string),
            typeof(CadexWorkflowButton),
            new PropertyMetadata(default(CadexWorkflowButton), OnUserNamePropertyChanged));

        private static void OnUserNamePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

        }

        public CadexWorkflowButton()
        {
            InitializeComponent();
        }
    }
}

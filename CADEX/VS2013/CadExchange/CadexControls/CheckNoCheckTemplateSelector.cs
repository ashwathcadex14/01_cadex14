﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;

namespace CadExchange.CadexControls
{
    public class CheckNoCheckTemplateSelector : DataTemplateSelector
    {
        public DataTemplate CheckTemplate { get; set; }
        public DataTemplate NoCheckTemplate { get; set; }
        public string PropertyName { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            bool prop = (bool)item.GetType().GetProperty(PropertyName).GetValue(item);
            if(prop)
                return CheckTemplate;
            else
                return NoCheckTemplate;
        }
    }
}

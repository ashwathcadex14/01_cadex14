﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using FirstFloor.ModernUI.Windows.Controls;
using FirstFloor.ModernUI.Presentation;
using System.Collections.ObjectModel;
using System.Collections;

namespace CadExchange.CadexControls
{
    /// <summary>
    /// Interaction logic for InfoExTile.xaml
    /// </summary>
    public partial class InfoExTile : UserControl
    {
        public InfoExTile()
        {
            InitializeComponent();
        }

        /// <summary>
        /// TileTitle
        /// </summary>
        public string TileTitle
        {
            get
            {
                return (string)GetValue(TileTitleProperty);
            }
            set
            {
                SetValue(TileTitleProperty, value);
            }
        }

        public static readonly DependencyProperty TileTitleProperty =
        DependencyProperty.Register(
            "TileTitle",
            typeof(string),
            typeof(InfoExTile),
            new PropertyMetadata(default(InfoExTile), OnTileTitlePropertyChanged));

        private static void OnTileTitlePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

        }

        /// <summary>
        /// TileImage
        /// </summary>
        public BitmapImage TileImage
        {
            get
            {
                return (BitmapImage)GetValue(TileImageProperty);
            }
            set
            {
                SetValue(TileImageProperty, value);
            }
        }

        public static readonly DependencyProperty TileImageProperty =
        DependencyProperty.Register(
            "TileImage",
            typeof(BitmapImage),
            typeof(InfoExTile),
            new PropertyMetadata(default(InfoExTile), OnTileImagePropertyChanged));

        private static void OnTileImagePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

        }

        /// <summary>
        /// TileColour
        /// </summary>
        public string TileColour
        {
            get
            {
                return (string)GetValue(TileColourProperty);
            }
            set
            {
                SetValue(TileColourProperty, value);
            }
        }

        public static readonly DependencyProperty TileColourProperty =
        DependencyProperty.Register(
            "TileColour",
            typeof(string),
            typeof(InfoExTile),
            new PropertyMetadata(default(InfoExTile), OnTileColourPropertyChanged));

        private static void OnTileColourPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

        }

        /// <summary>
        /// PageNvService
        /// </summary>
        public NavigationService PageNvService
        {
            get
            {
                return (NavigationService)GetValue(PageNvServiceSourceProperty);
            }
            set
            {
                SetValue(PageNvServiceSourceProperty, value);
            }
        }

        public static readonly DependencyProperty PageNvServiceSourceProperty =
        DependencyProperty.Register(
            "PageNvService",
            typeof(NavigationService),
            typeof(InfoExTile),
            new PropertyMetadata(default(InfoExTile), OnPageNvServiceSourcePropertyChanged));

        private static void OnPageNvServiceSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

        }

        /// <summary>
        /// TileContentSource
        /// </summary>
        public string TileContentSource
        {
            get
            {
                return (string)GetValue(TileContentSourceProperty);
            }
            set
            {
                SetValue(TileContentSourceProperty, value);
            }
        }

        public static readonly DependencyProperty TileContentSourceProperty =
        DependencyProperty.Register(
            "TileContentSource",
            typeof(string),
            typeof(InfoExTile),
            new PropertyMetadata(default(InfoExTile), OnTileContentSourcePropertyChanged));

        private static void OnTileContentSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

        }

        private void Tile_Click(object sender, RoutedEventArgs e)
        {
            if(sender is Tile)
            {
                string pageUrl = TileContentSource;
                if(pageUrl!=null && pageUrl.Length > 0)
                {
                    BBCodeBlock bs = new BBCodeBlock();
                    try
                    {
                        App infoEx = (App)Application.Current;
                        ModernWindow wind = (ModernWindow)Application.Current.MainWindow;
                        if (wind.MenuLinkGroups.Count==3)
                        {
                            Collection<ResourceDictionary> allDs = infoEx.Resources.MergedDictionaries;
                            ResourceDictionary linkDictionary = null;
                            foreach (ResourceDictionary resDry in allDs)
                            {
                                if (resDry.Source.ToString().Equals("Content/LinkDictionary.xaml"))
                                {
                                    linkDictionary = resDry;
                                    break;
                                }
                            }

                            if (linkDictionary != null)
                            {
                                IDictionaryEnumerator enumer = linkDictionary.GetEnumerator();
                                while (enumer.MoveNext())
                                {
                                    //if(enumer.Key.Equals("users"))
                                    //{
                                    LinkGroup userLinkGroup = (LinkGroup)enumer.Value;
                                    wind.MenuLinkGroups.Add(userLinkGroup);
                                    //}
                                }
                            } 
                        }
                        //ModernWindow wind = (ModernWindow)Application.Current.MainWindow;
                        //LinkGroup userLinkGroup = new LinkGroup();
                        //userLinkGroup.DisplayName = "Users";
                        //wind.MenuLinkGroups.Add(userLinkGroup);
                        bs.LinkNavigator.Navigate(new Uri(pageUrl, UriKind.Relative), this);
                    }
                    catch (Exception error)
                    {
                        ModernDialog.ShowMessage(error.Message, FirstFloor.ModernUI.Resources.NavigationFailed, MessageBoxButton.OK);
                    }
                }
            }
        }
    }

        
}

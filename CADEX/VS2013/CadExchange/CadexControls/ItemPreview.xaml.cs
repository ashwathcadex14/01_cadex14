﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace CadExchange.CadexControls
{
    /// <summary>
    /// Interaction logic for ItemPreview.xaml
    /// </summary>
    public partial class ItemPreview : UserControl
    {
        public ItemPreview()
        {
            InitializeComponent();
        }

        /// <summary>
        /// PreviewImage
        /// </summary>
        public BitmapImage PreviewImage
        {
            get
            {
                return (BitmapImage)GetValue(PreviewImageProperty);
            }
            set
            {
                SetValue(PreviewImageProperty, value);
            }
        }

        public static readonly DependencyProperty PreviewImageProperty =
        DependencyProperty.Register(
            "PreviewImage",
            typeof(BitmapImage),
            typeof(ItemPreview),
            new PropertyMetadata(default(ItemPreview), OnPreviewImagePropertyChanged));

        private static void OnPreviewImagePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            //Image thisImage = new Image();
            //MemoryStream ms = new MemoryStream();
            //System.Drawing.Bitmap bmp = (System.Drawing.Bitmap)((ItemPreview)d).PreviewImage;
            //bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            //ms.Position = 0;
            //BitmapImage bi = new BitmapImage();
            //bi.BeginInit();
            //bi.StreamSource = ms;
            //bi.EndInit();
            //thisImage.Source = bi;
            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadExchange.Common
{
    /// <summary>
    /// Constatns for the project
    /// </summary>
    public static class CadexConstants
    {
        public const string CADEX_PATH_SEPARTOR                             = "\\";

        public const string CADEX_UNDERSCORE_LITERAL                        = "_";

        public const string DEFAULT_TIME_STAMP                              = "yyyyMMddHHmmssfff";

        public const string CADEX_XML_FILE_PATTERN                          = "*.xml";

        public const string CADEX_CE4_PACKAGE_ID                            = "CE4PackageID";

        public const string CADEX_CE4_PACKAGE_REV_ID                        = "CE4PackageRevID";

        public const string CADEX_FILE_EXTENSON_ZIP                         = ".zip";

        public const string CADEX_FILE_EXTENSON_XML                         = ".xml";

        /// <summary>
        /// Constants for PLMXML
        /// </summary>
        public const string CADEX_PLMXML_USER_INFO_TYPE                     = "PIE_User_Info_Type";

        public const string CADEX_PLMXML_USER_DATA                          = "UserData";

        public const string CADEX_PLMXML_ATTR_TYPE                          = "type";

        public const string CADEX_PLMXML_ATTR_IN_CONTEXT                    = "AttributesInContext";

        public const string CADEX_PLMXML_OCCURRENCENAME                     = "OccurrenceName";

        public const string CADEX_PLMXML_SEQUENCENUMBER                     = "SequenceNumber";

        public const string CADEX_PLMXML_QUANTITY                           = "Quantity";

        public const string CADEX_PLMXML_ASSOCIATED_DATASET                 = "AssociatedDataSet";

        public const string CADEX_DESC_ATTR                                 = "object_desc";

        public const string CADEX_NAME_ATTR                                 = "object_name";

        public const string CADEX_ID_ATTR                                   = "item_id";

        public const string CADEX_REV_ID_ATTR                               = "item_revision_id";

        public const string CADEX_OBJECT_STRING_ATTR                        = "object_string";

        public const string CADEX_TYPE_ATTR                                 = "object_type";

        public const string CADEX_QUANTITY_ATTR                             = "bl_quantity";

        public const string CADEX_SEQ_NO_ATTR                               = "bl_sequence_no";

        public const string CADEX_DATASET_RELATION                          = "relation";

        public const string CADEX_ORG_FILE_NAME_ATTR                        = "original_file_name";

        public const string CADEX_REL_FILE_PATH_ATTR                        = "relative_location";
        //public const Dictionary<string, List<string>> ExternalFileTypes = new Dictionary<string, List<string>>() { { "Document",new List<string>(){"MSWord"} } };
    }
}

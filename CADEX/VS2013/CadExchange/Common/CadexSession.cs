﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using CadExchange.Utils;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using CadExchange.Tree;
using CadExchange.CadexControls;
using CadExchange.Database;
using CadExchange.Tree.dataset;
using System.ComponentModel;
using InfoExCommon.Utils;
using CadExchange.Database.Entities;

namespace CadExchange.Common
{
    public class CadexSession
    {
        #region Create Singleton for CadexSession
        static CadexSession _instance ;
        private string userId;
        public static readonly string userDir = getUserWorkDir();
        public static readonly string dbDir = Properties.Settings.Default.DatabaseDir;
        public static readonly string dbName = Properties.Settings.Default.DatabaseName;
        public static readonly string dbPath = userDir + "\\" + dbDir + "\\" + dbName;
        private Dictionary<int, List<ICadexTreeNode>> pkgs = new Dictionary<int, List<ICadexTreeNode>>();
        private Dictionary<string, string> cadMap = new Dictionary<string, string>();
        private Dictionary<string, string> fileMap = new Dictionary<string, string>();
        private List<string> renderMap = new List<string>();
        private Dictionary<int, ICadexDatasetFileNode> openedFiles = new Dictionary<int, ICadexDatasetFileNode>();
        private List<ICadexDatasetFileNode> filesOpened = new List<ICadexDatasetFileNode>();
        private BackgroundWorker worker;
        private bool enableEncryptyion = false;
        System.Timers.Timer timer = new System.Timers.Timer(1000);

        CadexSession(string id)
        {
         
            enableEncryptyion = Properties.Settings.Default.UseEncryption;
            cadMap.Add("UGMASTER", "NX");
            cadMap.Add("ACADDWG", "ACAD");
            cadMap.Add("SE Assembly", "SE");
            cadMap.Add("SE Part", "SE");
            renderMap.Add("DirectModel");
            renderMap.Add("ACADDXF");
            fileMap.Add("UGMASTER", ".prt");
            fileMap.Add("ACADDWG", ".dwg");
            fileMap.Add("DirectModel", ".jt");
            fileMap.Add("SE Assembly", ".asm");
            fileMap.Add("SE Part", ".par");
            if (File.Exists(dbPath))
            {
                if (Session.getUserByUserId(id)!=null)
                {
                    this.userId = id;
                   
                }
            }
            else
                throw new Exception("The user working directory " + userDir + " cannot be reached. Make sure if the path is accessible.");

            worker = new BackgroundWorker()
            {
                WorkerSupportsCancellation = true,
                WorkerReportsProgress = true
            };
            worker.DoWork += worker_DoWork;
            worker.ProgressChanged += worker_ProgressChanged;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;

            timer.Elapsed += timer_Elapsed;
            
        }
        #endregion

        public static void createCadexSession(string id)
        {
            if (_instance == null)
            {
                try
                {
                    _instance = new CadexSession(id);
                }
                catch (Exception e)
                {
                    InfoExCommon.Utils.LoggerUtil.error(_instance.GetType(), e.Message, e); throw;
                }
            }
        }

        public static string getSessionUser()
        {
            if (_instance != null && _instance.userId.Length > 0)
                return _instance.userId;
            else
                throw new Exception("A session is not established.");
        }

        public static string getSessionDir()
        {
            if (_instance != null && userDir.Length > 0)
                return userDir;
            else
                throw new Exception("A session is not established or the working directory is not set. Please contact your system administrator.");
        }

        public static void addPkg(int pkgId, List<ICadexTreeNode> tree)
        {
            if (_instance.pkgs.ContainsKey(pkgId))
                _instance.pkgs.Remove(pkgId);

            _instance.pkgs.Add(pkgId, tree);
        }

        public static List<ICadexTreeNode> getPkg(int pkgId)
        {
             List<ICadexTreeNode> root = new List<ICadexTreeNode>();
             _instance.pkgs.TryGetValue(pkgId, out root);
             return root;
        }

        public static void clearPkgs()
        {
            _instance.pkgs.Clear();
        }

        public static bool isCad(string type)
        {
            return _instance.cadMap.ContainsKey(type);
        }

        public static bool isRender(string type)
        {
            return _instance.renderMap.Contains(type);
        }

        public static string getCadType(string type)
        {
            string retType;
            _instance.cadMap.TryGetValue(type, out retType);
            return retType;
        }

        public static string getFileType(string type)
        {
            string retType;
            _instance.fileMap.TryGetValue(type, out retType);
            return retType;
        }

        public static Dictionary<int, List<ICadexTreeNode>> getPkgs()
        {
            return _instance.pkgs;
        }

        public static void addOpenedFile(ICadexDatasetFileNode file)
        {
            _instance.filesOpened.Add(file);
            resetTimer();
        }

        public static void remOpenedFile(ICadexDatasetFileNode file)
        {
            _instance.filesOpened.Remove(file);
            resetTimer();
        }

        private static void resetTimer()
        {
            if (_instance.filesOpened.Count > 0)
            {
                LoggerUtil.info(_instance.GetType(), "Starting the timer to keep a watch on files..");
                _instance.timer.Start();
            }
            else
            {
                LoggerUtil.info(_instance.GetType(), "Stopping the timer as there are no files to watch..");
                _instance.timer.Stop();
            }
        }

        private void checkFiles()
        {
            LoggerUtil.info(this.GetType(), "In checkFiles ");
            List<ICadexDatasetFileNode> remFiles = new List<ICadexDatasetFileNode>();
            foreach (ICadexDatasetFileNode file in filesOpened)
            {
                FileInfo fileInfo = new FileInfo(file.ExternalFile);
                bool isLocked = CommonUtil.IsFileLocked(fileInfo);
                if (!isLocked)
                {
                    //file.encryptFile();
                    LoggerUtil.info(this.GetType(), "The file " + file.ExternalFile + " is not locked.");
                    //filesOpened.Remove(file);
                    remFiles.Add(file);
                }
                else
                {
                    LoggerUtil.info(this.GetType(), "The file " + file.ExternalFile + " is still locked.");
                }
            }

            foreach (ICadexDatasetFileNode remFile in remFiles)
            {
                remFile.encryptFile();
                LoggerUtil.info(this.GetType(), "The file " + remFile.ExternalFile + " is removed from the session files.");
                remOpenedFile(remFile);
            }

            LoggerUtil.info(this.GetType(), "Leaving checkFiles");
        }

        void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (!worker.IsBusy)
                worker.RunWorkerAsync();
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker w = (BackgroundWorker)sender;
            LoggerUtil.info( this.GetType(), "Trying to check opened files. Files Opened : " + filesOpened.Count );
            while(filesOpened.Count>0)
            {
                //check if cancellation was requested
                if(w.CancellationPending)
                {
                    //take any necessary action upon cancelling (rollback, etc.)

                    //notify the RunWorkerCompleted event handler
                    //that the operation was cancelled
                    e.Cancel = true; 
                    return;
                }

                //report progress; this method has an overload which can also take
                //custom object (usually representing state) as an argument
                LoggerUtil.info(this.GetType(), "Sending Control to checkFiles..");
                w.ReportProgress(10);

                //do whatever You want the background thread to do...
                checkFiles();

                LoggerUtil.info(this.GetType(), "Got control back from checkFiles..");
                w.ReportProgress(100);

            }
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            LoggerUtil.info(this.GetType(), "Some progress has changed..");
        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                //do something
            }
            else
            {
                //do something else
            }
        }

        public static bool EncryptionEnabled
        {
            get
            {
                if (_instance != null)
                    return _instance.enableEncryptyion;
                else
                    throw new Exception( "A Session has not been established" );
            }
        }

        public static void EnableEncryption(bool enable)
        {
            if (_instance != null)
                _instance.enableEncryptyion = enable;
        }

        /// <summary>
        /// Refreshes the persistent object
        /// TODO: Need to type testing for safety
        /// </summary>
        /// <param name="entity"></param>
        public static void RefreshObject(Object entity) 
        {
            Session.refreshObject(entity);
        }
        private static string getUserWorkDir()
        {
            string userDir;
            string globalUserDir = Properties.Settings.Default.GlobalUserDir;
            string defaultUserDir = Environment.CurrentDirectory + "\\" + Properties.Settings.Default.WorkFolder;
            if (globalUserDir != null && globalUserDir.Trim().Length > 0)
            {
                userDir = globalUserDir;
            }
            else
            {
                userDir = defaultUserDir;
            }
            return userDir;
        }
    }
}

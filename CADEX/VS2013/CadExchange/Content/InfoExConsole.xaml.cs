﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CadExchange.Pages.infoex;
using NavigationEventArgs = FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs;

namespace CadExchange.Content
{
    /// <summary>
    /// Interaction logic for InfoExConsole.xaml
    /// </summary>
    public partial class InfoExConsole : AbstractInfoExPage
    {
        InfoExConsoleModel model = new InfoExConsoleModel();

        public InfoExConsole()
        {
            InitializeComponent();
            this.DataContext = model;
        }

        private void ModelOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (propertyChangedEventArgs.PropertyName.Equals("LogContent"))
            {
                this.mainScroll.ScrollToBottom();
            }
        }

        public override void OnFragmentNavigationInfoEx(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            
        }

        public override void OnFragmentNavigatedToInfoEx(NavigationEventArgs e)
        {
            if (model.checkEvent())
                model.PropertyChanged += ModelOnPropertyChanged;

            model.refreshLog();
        }

        public override string getToolBarResource()
        {
            return "consoleButtons";
        }
    }
}

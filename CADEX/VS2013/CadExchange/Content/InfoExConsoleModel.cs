﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using FirstFloor.ModernUI.Presentation;
using InfoExCommon.Logger;

namespace CadExchange.Content
{
    class InfoExConsoleModel : INotifyPropertyChanged
    {
        private ICommand _refreshCommand;
        public event PropertyChangedEventHandler PropertyChanged;
        private string _logContent;

        public InfoExConsoleModel()
        {
        }

        void NotifyPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public bool checkEvent()
        {
            if (PropertyChanged != null)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Binding for Refresh button command
        /// </summary>
        public ICommand RefreshCommand
        {
            get
            {
                if (_refreshCommand == null)
                {
                    _refreshCommand = new RelayCommand(
                        param => this.refreshLog(),
                        param => true
                    );
                }
                return _refreshCommand;
            }
        }

        /// <summary>
        /// Log Content
        /// </summary>
        public string LogContent
        {
            get
            {
                return _logContent;
            }
        }

        public void refreshLog()
        {
            string logFile = InfoExLogger.GetCurrentLog();
            if (logFile != null)
            {
                string logContent = "Showing log " + "\n\n\n" + logFile;
                _logContent = logContent;
            }
            else
            {
                _logContent = "No Log Content !!!";
            }
            NotifyPropertyChanged("LogContent");
        }
    }
}

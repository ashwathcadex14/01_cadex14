﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CadExchange.Common;

namespace CadExchange.Content
{
    /// <summary>
    /// Interaction logic for SystemSettings.xaml
    /// </summary>
    public partial class SystemSettings : UserControl
    {
        public SystemSettings()
        {
            InitializeComponent();
            enableEncryption.IsChecked = Properties.Settings.Default.UseEncryption;
        }

        private void EnableEncryption_OnChecked(object sender, RoutedEventArgs e)
        {
            CadexSession.EnableEncryption( true );
            Properties.Settings.Default.UseEncryption = true;
        }

        private void EnableEncryption_OnUnchecked(object sender, RoutedEventArgs e)
        {
            CadexSession.EnableEncryption( false );
            Properties.Settings.Default.UseEncryption = false;
        }
    }
}

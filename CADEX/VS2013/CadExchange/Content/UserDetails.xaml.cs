﻿using CadExchange.Database;
using CadExchange.Database.Entities;
using CadExchange.Pages;
using CadExchange.Utils;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CadExchange.Content
{
    /// <summary>
    /// Interaction logic for UserDetails.xaml
    /// </summary>
    public enum UserStatus { Active, Inactive };
    public enum IsUserAdmin { Yes, No };

    public class UserData
    {
        public string UserId { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public IsUserAdmin IsAdministrator { get; set; }
        public UserStatus Status { get; set; }
    }

    public partial class UserDetails : UserControl, IContent
    {
        private UserData SelectedUserEdit = null;
        private User SelectedUser = null;
        private ObservableCollection<UserData> usrdata = null;

        public UserDetails()
        {
            InitializeComponent();
        }

        private ObservableCollection<UserData> GetData(User user)
        {
            if (user != null)
            {
                var users = new ObservableCollection<UserData>();
                users.Add(new UserData { UserId = user.UserId, Password = new String('*', user.Password.Length), FirstName = user.FirstName, LastName = user.LastName, IsAdministrator = user.IsAdministrator == true ? IsUserAdmin.Yes : IsUserAdmin.No, Status = user.Status != 0 ? UserStatus.Active : UserStatus.Inactive });
                return users;
            }
            return null;
        }

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            var fragment = e.Fragment;
            SelectedUser = Session.getUserByUserId(fragment);
            usrdata = GetData(SelectedUser);
            DG1.DataContext = usrdata;
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(SelectedUserEdit != null && SelectedUser != null)
            {
                bool status = Session.Cadex_BeginRequest();
                var existingUser = Session.getObjectById<User>(Convert.ToString(SelectedUser.Id));
                //commit all the changes made for the user
                if(SelectedUserEdit.Password != null && SelectedUserEdit.Password.Length > 0 && SelectedUserEdit.Password != existingUser.Password)
                { existingUser.Password = CommonUtil.EncryptBase(SelectedUserEdit.Password); }
                if(SelectedUserEdit.FirstName != null && SelectedUserEdit.FirstName.Length > 0 && SelectedUserEdit.FirstName != existingUser.FirstName)
                { existingUser.FirstName = SelectedUserEdit.FirstName;  }
                if (SelectedUserEdit.LastName != null && SelectedUserEdit.LastName.Length > 0 && SelectedUserEdit.LastName != existingUser.LastName)
                { existingUser.LastName = SelectedUserEdit.LastName; }
                bool val = SelectedUserEdit.IsAdministrator == IsUserAdmin.Yes ? true : false;
                if (existingUser.IsAdministrator != val) { existingUser.IsAdministrator = val; }
                int ival = SelectedUserEdit.Status == UserStatus.Active ? 1 : 0;
                if (existingUser.Status != ival) { existingUser.Status = ival; }
                Session.persistObject(existingUser);
                if(!status) Session.Cadex_EndRequest(false);
            }
        }

        private void DG1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedUserEdit = DG1.SelectedItem as UserData;
        }

        private void DG1_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            FrameworkElement element_1 = DG1.Columns[1].GetCellContent(e.Row);
            if (element_1.GetType() == typeof(TextBlock))
            {
                var xxPassword = ((TextBlock)element_1).Text;
                SelectedUserEdit.Password = xxPassword;
            }
            FrameworkElement element_2 = DG1.Columns[2].GetCellContent(e.Row);
            if (element_2.GetType() == typeof(TextBlock))
            {
                var xxFirstName = ((TextBlock)element_2).Text;
                SelectedUserEdit.FirstName = xxFirstName;
            }
            FrameworkElement element_3 = DG1.Columns[3].GetCellContent(e.Row);
            if (element_3.GetType() == typeof(TextBlock))
            {
                var xxLastName = ((TextBlock)element_3).Text;
                SelectedUserEdit.LastName = xxLastName;
            }
            FrameworkElement element_4 = DG1.Columns[4].GetCellContent(e.Row);
            if (element_4.GetType() == typeof(ComboBox))
            {
                IsUserAdmin xxIsAdmin = (IsUserAdmin)((ComboBox)element_4).SelectedValue;
                SelectedUserEdit.IsAdministrator = xxIsAdmin == IsUserAdmin.Yes ? IsUserAdmin.Yes : IsUserAdmin.No;
            }
            FrameworkElement element_5 = DG1.Columns[5].GetCellContent(e.Row);
            if (element_5.GetType() == typeof(ComboBox))
            {
                UserStatus xxStatus = ((UserStatus)((ComboBox)element_5).SelectedValue);
                SelectedUserEdit.Status = xxStatus == UserStatus.Active ? UserStatus.Active : UserStatus.Inactive;
            }
        }
    }
}

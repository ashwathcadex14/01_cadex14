﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadExchange.Database.Entities
{
    public class CadexSupplierDataset : ModelObject
    {
        public virtual string Relation { get; set; }
        public virtual CadexSupplierItemRevision ParentRevision { get; set; }
        public virtual PackageLineItem ParentPkgRevision { get; set; }
        public virtual IList<CadexSupplierDatasetFile> Files { get; set; }

        public CadexSupplierDataset()
            : base()
        {
            Files = new List<CadexSupplierDatasetFile>();
        }

        public CadexSupplierDataset(string relation, object rev)
            : this()
        {
            this.Relation = relation;
            
            if(rev is CadexSupplierItemRevision)
            {
                CadexSupplierItemRevision revision = (CadexSupplierItemRevision)rev;
                revision.Datasets.Add(this);
                this.ParentRevision = revision;
            }
            else
            {
                PackageLineItem revision = (PackageLineItem)rev;
                revision.Datasets.Add(this);
                this.ParentPkgRevision = revision;
            }

            this.addOwner();
        }
    }
}

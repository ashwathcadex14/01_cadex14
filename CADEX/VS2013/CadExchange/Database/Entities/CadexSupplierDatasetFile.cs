﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadExchange.Database.Entities
{
    public class CadexSupplierDatasetFile : ModelObject
    {
        public virtual CadexSupplierDataset ParentDataset { get; set; }

        public CadexSupplierDatasetFile() : base()
        { }

        public CadexSupplierDatasetFile(CadexSupplierDataset dset, string filePath) : this()
        {
            dset.Files.Add(this);
            this.ParentDataset = dset;
            this.addOwner();
            this.addAttributeToObject("ImanFile", "absolute_path", filePath);
            this.addAttributeToObject("ImanFile", "original_file_name", System.IO.Path.GetFileName(filePath));
            this.addAttributeToObject("ImanFile", "last_mod_date", Convert.ToString( System.IO.File.GetLastWriteTime(filePath) ));
        }
    }
}

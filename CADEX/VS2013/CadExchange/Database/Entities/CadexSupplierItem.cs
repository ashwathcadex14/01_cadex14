﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CadExchange.Database.Entities
{
    public class CadexSupplierItem : ModelObject
    {
        public virtual string ItemId { get; set; }
        public virtual IList<CadexSupplierItemRevision> Revisions { get; set; }

        public CadexSupplierItem()
            : base()
        {
            Revisions = new List<CadexSupplierItemRevision>();
            
        }

        public CadexSupplierItem(string itemid)
            : this()
        {
            this.ItemId = itemid;
            this.addOwner();
        }
    }
}

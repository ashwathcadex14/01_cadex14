﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadExchange.Database.Entities
{
    public class CadexSupplierItemRevision : ModelObject
    {
        public virtual string RevId { get; set; }
        public virtual CadexSupplierItem ParentItem { get; set; }
        public virtual IList<CadexSupplierDataset> Datasets { get; set; }
        public virtual bool isLatest { get; set; }

        public CadexSupplierItemRevision()
            : base()
        {
            Datasets = new List<CadexSupplierDataset>();
        }

        public CadexSupplierItemRevision(string revId, CadexSupplierItem item)
            : this()
        {
            this.RevId = revId;
            item.Revisions.Add(this);
            this.ParentItem = item;
            this.addOwner();
        }

        public virtual void addDataset(CadexSupplierDataset dset)
        {
            this.Datasets.Add(dset);
        }
    }
}

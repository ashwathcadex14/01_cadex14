﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadExchange.Database.Entities
{
    public class DatasetConfiguration
    {
        public virtual int Id { get; protected set; }
        public virtual string type { get; set; }
        public virtual string reference { get; set; }
        public virtual string ext { get; set; }
        public virtual string format { get; set; }
    }
}

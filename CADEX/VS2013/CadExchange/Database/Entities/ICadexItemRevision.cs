﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadExchange.Database.Entities
{
    public interface ICadexItemRevision
    {
        IList<CadexSupplierDataset> Datasets { get; set; }
    }
}

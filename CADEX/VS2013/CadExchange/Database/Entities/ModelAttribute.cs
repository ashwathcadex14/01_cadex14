﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadExchange.Database.Entities
{
    public class ModelAttribute
    {
        public virtual int Id { get; protected set; }
        public virtual string Element { get; set; }
        public virtual string Name { get; set; }
        public virtual string Value { get; set; }
        public virtual ModelObject ParentObj { get; set; }

        public ModelAttribute()
        { }

        public ModelAttribute(string element, string name, string value, ModelObject obj)
        {
            this.Element = element;
            this.Name = name;
            this.Value = value;
            this.ParentObj = obj;
        }
    }
}

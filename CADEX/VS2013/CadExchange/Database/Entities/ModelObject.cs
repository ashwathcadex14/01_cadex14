﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadExchange.Common;

namespace CadExchange.Database.Entities
{
    public class ModelObject
    {
        public virtual int Id { get; protected set; }
        public virtual IList<ModelAttribute> Attributes { get; set; }

        public ModelObject()
        {
            Attributes = new List<ModelAttribute>();
        }

        public virtual void addAttributeToObject(string element, string name, string value)
        {
            ModelAttribute newAttr = new ModelAttribute(element, name, value, this);
            this.Attributes.Add(newAttr);
        }

        public virtual void addOwner()
        {
            try
            {
                this.addAttributeToObject(getElementFromType(), "owning_user", CadexSession.getSessionUser());
            }
            catch (Exception e)
            {
                //swallowed
            }
        }

        public virtual string getAttribute(string element, string name)
        {
            ModelAttribute retAttr = ((List<ModelAttribute>)this.Attributes.ToList<ModelAttribute>()).Find(attr => attr.Element == element && attr.Name == name);
            return retAttr != null ? retAttr.Value : ""; 
        }

        private string getElementFromType()
        {
            if (this.GetType().Name.Equals("CadexSupplierItem"))
                return "Item";
            else if (this.GetType().Name.Equals("CadexSupplierItemRevision"))
                return "ItemRevision";
            else if (this.GetType().Name.Equals("CadexSupplierDataset"))
                return "Dataset";
            else if (this.GetType().Name.Equals("CadexSupplierDatasetFile"))
                return "ImanFile";
            else if (this.GetType().Name.Equals("PackageStructure"))
                return "Occurrence";
            else
                return "";
        }
    }
}

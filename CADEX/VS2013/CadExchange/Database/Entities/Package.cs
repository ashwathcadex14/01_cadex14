﻿using CadExchange.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CadExchange.Database.Entities
{
    public class Package
    {
        public virtual int Id { get; protected set; }
        public virtual string PackageId { get; set; }
        public virtual string Path { get; set; }
        public virtual IList<PackageStructure> Structures { get; set; }
        public virtual IList<PackageLineItem> LineItems { get; set; }

        public Package()
        {
            Structures = new List<PackageStructure>();
            LineItems = new List<PackageLineItem>();
        }

        public virtual void addStructureToPackage(PackageStructure structureLine)
        {
            Structures.Add(structureLine);
        }

        public virtual void addLineItemToPackage(PackageLineItem structureLine)
        {
            LineItems.Add(structureLine);
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            else
            {
                Package pkg = ((Package)obj);
                return pkg.Id == this.Id;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public virtual string getPlmXMLPath()
        {
            return CadexSession.userDir + Path;
        }
        public virtual string getPackageDir()
        {
            return System.IO.Path.GetDirectoryName(getPlmXMLPath());
        }
    }
}

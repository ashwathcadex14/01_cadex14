﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadExchange.Database.Entities
{
    public class PackageLineItem 
    {
        public virtual int Id { get; protected set; }
        public virtual Package orgPackage { get; set; }
        public virtual string itemId { get; set; }
        public virtual string plmxmlId { get; set; }
        public virtual IList<Workflow> itsWorkflows { get; set; }
        public virtual IList<CadexSupplierDataset> Datasets { get; set; }
        public virtual bool isLocked { get; set; }
        public virtual string LockedBy { get; set; }
        public virtual OwnerType PartOwner { get; set; }

        public PackageLineItem()
        {
            itsWorkflows = new List<Workflow>();
            Datasets = new List<CadexSupplierDataset>();
        }

        public PackageLineItem(CadexSupplierItem item, CadexSupplierItemRevision rev)
        {
            this.itemId = item.ItemId + "|" + rev.RevId;
            this.plmxmlId = Convert.ToString( rev.Id );
            this.PartOwner = OwnerType.Supplier;
        }

        public override bool Equals(object obj)
        {
            PackageLineItem line = ((PackageLineItem)obj);
            return line.itemId.Equals(this.itemId) && (line.orgPackage == null ||(line.orgPackage == this.orgPackage));
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public enum OwnerType
        {
            Supplier=0,
            OEM=1
        }
    }
}

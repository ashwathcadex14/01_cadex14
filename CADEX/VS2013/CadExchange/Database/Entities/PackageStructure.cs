﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadExchange.Database.Entities
{
    public class PackageStructure : ModelObject
    {
        public virtual Package orgPackage { get; set; }
        public virtual string parentId { get; set; }
        public virtual string childId { get; set; }
        public virtual string itemId { get; set; }
        public virtual string childPlmxmlId { get; set; }
        public virtual bool isNew { get; set; }
        public virtual bool isRemoved { get; set; }
        
    }
}

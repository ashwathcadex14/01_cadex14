﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CadExchange.Database.Entities
{
    public class User
    {
        public virtual int Id { get; protected set; }
        public virtual string UserId { get; set; }
        public virtual string Password { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual bool IsAdministrator { get; set; }
        public virtual int Status { get; set; }
    }
}

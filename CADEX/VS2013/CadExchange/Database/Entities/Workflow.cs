﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadExchange.Utils;

namespace CadExchange.Database.Entities
{
    public class Workflow
    {
        public virtual int Id { get; protected set; }
        public virtual PackageLineItem wfItem { get; set; }
        public virtual IList<WorkflowStage> Stages { get; set; }
        public virtual WorkflowState State { get; set; }
        public virtual string LastModifyingUser { get; set; }
        public virtual WorkflowStage CurrentStage { get; set; }

        public Workflow()
        {
        }

        public Workflow(string lasModUser, PackageLineItem line,string userId)
        {
            Stages = new List<WorkflowStage>();
            this.LastModifyingUser = lasModUser;
            this.State = WorkflowState.Assigned;
            this.wfItem = line;
            this.createStage(Session.getUserByUserId(userId), "The Part/Assy is assigned to " + userId, "Assigned"); 
        }

        public virtual void createStage(User respUser, string detailedInfo, string text)
        {
            WorkflowStage newStage = new WorkflowStage();
            newStage.mainWf = this;
            newStage.responsibleUser = respUser;
            newStage.detailedInfo = detailedInfo;
            newStage.text = text;
            newStage.state = this.State;
            this.CurrentStage = newStage;
            this.Stages.Add(newStage);
        }

        public virtual void acknowledgeTask()
        {
            this.State = WorkflowState.Acknowledged;
            User currentUser = Session.getUserByUserId(CadExchange.Common.CadexSession.getSessionUser());
            this.createStage(currentUser, "Acknowledged by " + currentUser.FirstName, "Acknowledged");
            this.State = WorkflowState.InWork;
            this.createStage(currentUser, "In work with " + currentUser.FirstName, "In Work");
        }

        public virtual void sendToReviewTask(string userId)
        {
            this.State = WorkflowState.Review;
            User currentUser = Session.getUserByUserId(userId);
            this.createStage(currentUser, "In Review with " + currentUser.FirstName, "Review");
        }

        public virtual void completeTask()
        {
            this.State = WorkflowState.Completed;
            User workUser = assignedUser();
            this.createStage(workUser, "Completed", "Completed");
        }

        public virtual void rejectTask()
        {
            this.State = WorkflowState.Rejected;
            User currentUser = Session.getUserByUserId(CadExchange.Common.CadexSession.getSessionUser());
            this.createStage(currentUser, "Rejected By " + currentUser.FirstName, "Rejected");
            this.State = WorkflowState.InWork;
            User workUser = assignedUser();
            this.createStage(workUser, "In work with " + workUser.FirstName, "In Work");
        }

        private User assignedUser()
        {
            List<WorkflowStage> tempStages = this.Stages.ToList<WorkflowStage>();
            WorkflowStage stage = tempStages.Find(this.FindAssignedStage);
            return stage.responsibleUser;
        }

        private bool FindAssignedStage(WorkflowStage stage)
        {
            if(stage.state == WorkflowState.Assigned)
                return true;
            else
                return false;
        }
    }


    public enum WorkflowState
    {
        Assigned=1,
        Acknowledged=2,
        InWork=3,
        Review=4,
        Rejected=5,
        Completed=6
    };
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadExchange.Database.Entities
{
    public class WorkflowStage
    {
        public virtual int Id { get; protected set; }
        public virtual User responsibleUser { get; set; }
        public virtual string text { get; set; }
        public virtual string detailedInfo { get; set; }
        public virtual Workflow mainWf { get; set; }
        public virtual DateTime completed { get; set; }
        public virtual WorkflowState state { get; set; }
    }
}

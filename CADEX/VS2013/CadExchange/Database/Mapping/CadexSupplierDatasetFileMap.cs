﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using CadExchange.Database.Entities;

namespace CadExchange.Database.Mappings
{
    public class CadexSupplierDatasetFileMap : SubclassMap<CadexSupplierDatasetFile>
    {
        public CadexSupplierDatasetFileMap()
        {
            KeyColumn("Id");
            References(x => x.ParentDataset).Not.Nullable();
        }
    }
}

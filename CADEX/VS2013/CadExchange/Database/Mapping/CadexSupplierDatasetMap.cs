﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using CadExchange.Database.Entities;

namespace CadExchange.Database.Mappings
{
    public class CadexSupplierDatasetMap : SubclassMap<CadexSupplierDataset>
    {
        public CadexSupplierDatasetMap()
        {
            KeyColumn("Id");
            Map(x => x.Relation).Not.Nullable();
            References(x => x.ParentRevision).Nullable();
            References(x => x.ParentPkgRevision).Nullable();
            HasMany(x => x.Files).KeyColumn("ParentDataset_id").Cascade.All().Inverse();
        }
    }
}

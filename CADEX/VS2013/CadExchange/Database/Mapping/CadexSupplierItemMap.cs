﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using CadExchange.Database.Entities;

namespace CadExchange.Database.Mappings
{
    public class CadexSupplierItemMap : SubclassMap<CadexSupplierItem>
    {
        public CadexSupplierItemMap()
        {
            KeyColumn("Id");
            Map(x => x.ItemId).Unique().Not.Nullable();
            HasMany(x => x.Revisions).KeyColumn("ParentItem_id").Cascade.All().Inverse();
        }
    }
}

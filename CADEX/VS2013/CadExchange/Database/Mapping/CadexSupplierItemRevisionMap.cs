﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using CadExchange.Database.Entities;

namespace CadExchange.Database.Mappings
{
    public class CadexSupplierItemRevisionMap : SubclassMap<CadexSupplierItemRevision>
    {
        public CadexSupplierItemRevisionMap()
        {
            KeyColumn("Id");
            Map(x => x.RevId).Not.Nullable();
            References(x => x.ParentItem).Not.Nullable();
            Map(x => x.isLatest).Default("0");
            HasMany(x => x.Datasets).KeyColumn("ParentRevision_id").Cascade.All().Inverse();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using System.Threading.Tasks;
using CadExchange.Database.Entities;

namespace CadExchange.Database.Mapping
{
    public class DatasetConfigurationMap : ClassMap<DatasetConfiguration>
    {
        public DatasetConfigurationMap()
        {
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.type).Not.Nullable();
            Map(x => x.reference).Nullable();
            Map(x => x.ext).Not.Nullable();
            Map(x => x.format).Nullable();
        }
    }
}

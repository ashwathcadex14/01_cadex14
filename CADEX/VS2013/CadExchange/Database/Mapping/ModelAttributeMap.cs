﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using CadExchange.Database.Entities;

namespace CadExchange.Mappings
{
    public class ModelAttributeMap : ClassMap<ModelAttribute>
    {
        public ModelAttributeMap()
        {
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Element).Not.Nullable();
            Map(x => x.Name).Not.Nullable();
            Map(x => x.Value).Nullable();
            References(x => x.ParentObj).Not.Nullable();
        }
    }
}

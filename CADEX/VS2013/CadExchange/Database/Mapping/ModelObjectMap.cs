﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using CadExchange.Database.Entities;

namespace CadExchange.Database.Mappings
{
    public class ModelObjectMap : ClassMap<ModelObject>
    {
        public ModelObjectMap()
        {
            Id(x => x.Id).GeneratedBy.HiLo("10");
            HasMany(x => x.Attributes).KeyColumn("ParentObj_id").Cascade.All().Inverse();
        }
    }
}

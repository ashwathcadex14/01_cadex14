﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using System.Threading.Tasks;
using CadExchange.Database.Entities;

namespace CadExchange.Database.Mapping
{
    public class PackageLineItemMap : ClassMap<PackageLineItem>
    {
        public PackageLineItemMap()
        {
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.orgPackage).Nullable();
            HasMany(x => x.itsWorkflows).KeyColumn("wfItem_id").Cascade.All().Inverse();
            HasMany(x => x.Datasets).KeyColumn("ParentPkgRevision_id").Cascade.All().Inverse();
            Map(x => x.itemId).Not.Nullable();
            Map(x => x.plmxmlId).Not.Nullable();
            Map(x => x.isLocked).Default("0");
            Map(x => x.LockedBy).Nullable();
            Map(x => x.PartOwner).CustomType<PackageLineItem.OwnerType>();
        }
    }
}

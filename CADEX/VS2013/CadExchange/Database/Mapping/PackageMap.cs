﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using CadExchange.Database.Entities;

namespace CadExchange.Mappings
{
    public class PackageMap : ClassMap<Package>
    {
        public PackageMap()
        {
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.PackageId).Not.Nullable();
            Map(x => x.Path).Not.Nullable();
            HasMany(x => x.Structures).KeyColumn("orgPackage_id").Cascade.All().Inverse();
            HasMany(x => x.LineItems).KeyColumn("orgPackage_id").Cascade.All().Inverse();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using System.Threading.Tasks;
using CadExchange.Database.Entities;

namespace CadExchange.Database.Mapping
{
    public class PackageStructureMap : SubclassMap<PackageStructure>
    {
        public PackageStructureMap()
        {
            KeyColumn("Id");
            References(x => x.orgPackage).Nullable();
            Map(x => x.parentId).Nullable();
            Map(x => x.childId).Not.Nullable();
            Map(x => x.itemId).Not.Nullable();
            Map(x => x.childPlmxmlId).Not.Nullable();
            Map(x => x.isNew).Default("0");
            Map(x => x.isRemoved).Default("0");
        }
    }
}

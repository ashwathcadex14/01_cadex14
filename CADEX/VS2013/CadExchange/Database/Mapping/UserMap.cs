﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using CadExchange.Database.Entities;

namespace CadExchange.Mappings
{
    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.UserId).Length(20).Unique().Not.Nullable();
            Map(x => x.Password).Length(20).Not.Nullable();
            Map(x => x.FirstName).Length(20).Not.Nullable();
            Map(x => x.LastName).Length(20).Not.Nullable();
            Map(x => x.IsAdministrator).Default("0");
            Map(x => x.Status).Default("1");
        }
    }
}

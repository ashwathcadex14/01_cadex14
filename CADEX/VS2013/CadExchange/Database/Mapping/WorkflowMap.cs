﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using System.Threading.Tasks;
using CadExchange.Database.Entities;
using FluentNHibernate;

namespace CadExchange.Database.Mapping
{
    public class WorkflowMap : ClassMap<Workflow>
    {
        public WorkflowMap()
        {
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.wfItem).Not.Nullable();
            HasMany(x => x.Stages).KeyColumn("mainWf_id").Cascade.All().Inverse();
            Map(x => x.State).CustomType<WorkflowState>();
            Map(x => x.LastModifyingUser).Not.Nullable();
            References(x => x.CurrentStage).Nullable();
        }
    }
}

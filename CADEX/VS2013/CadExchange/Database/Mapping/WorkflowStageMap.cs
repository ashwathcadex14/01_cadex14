﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using System.Threading.Tasks;
using CadExchange.Database.Entities;

namespace CadExchange.Database.Mapping
{
    public class WorkflowStageMap : ClassMap<WorkflowStage>
    {
        public WorkflowStageMap()
        {
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.mainWf).Not.Nullable();
            References(x => x.responsibleUser).Not.Nullable();
            Map(x => x.state).CustomType<WorkflowState>();
            Map(x => x.completed).CustomType("Timestamp");
            Map(x => x.detailedInfo).Not.Nullable();
            Map(x => x.text).Not.Nullable();
        }
    }
}

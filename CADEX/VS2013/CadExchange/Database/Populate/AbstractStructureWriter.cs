﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadExchange.Utils;

namespace CadExchange.Database.Populate
{
    public abstract class AbstractStructureWriter<T,R,S,Q> : ICadexStructureWriter<T,R,S,Q>
    {
        private T thisDocument;
        private R thisRoot;
        public AbstractStructureWriter(T document, R rootNode)
        {
            thisDocument = document;
            thisRoot = rootNode;
        }

        public T getDocument()
        {
            return thisDocument;
        }

        public R getRootNode()
        {
            return thisRoot;
        }

        public virtual void startTraversal()
        {
            try
            {
                string rootId = writeHeader();

                traverseChildren(thisRoot, rootId);

                saveDocument();
            }
            catch(Exception e)
            {
                InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);throw;
            }
        }

        public abstract bool checkIfNewOccurrence(R node);
        public abstract string writeNewOccurrence(R node, string parentId);
        public abstract string writeNewItem(R Node);
        public abstract string writeNewItemRevision(R Node);
        public abstract string writeNewDataset(S Node);
        public abstract void modifyDataset(S Node);
        public abstract string writeNewRelation(S Node, string parentId);
        public abstract string writeNewFile(Q Node);
        public abstract bool modifyFile(Q Node);
        public abstract void traverseChildren(R node, string parentId);
        public abstract void saveDocument();
        public abstract string writeHeader();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadExchange.Database.Populate
{
    public interface ICadexStructureWriter<T,R,S,Q>
    {
        string writeNewOccurrence(R Node, string parentId);
        string writeNewItem(R Node);
        string writeNewItemRevision(R Node);
        string writeNewDataset(S Node);
        void modifyDataset(S Node);
        string writeNewRelation(S Node, string parentId);
        string writeNewFile(Q Node);
        bool modifyFile(Q Node);
        void traverseChildren(R node, string parentId);
        void saveDocument();
        string writeHeader();
        T getDocument();
        R getRootNode();
        bool checkIfNewOccurrence(R node);
    }
}

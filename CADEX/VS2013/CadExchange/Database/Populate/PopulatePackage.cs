﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadExchange.plmxml;
using CadExchange.Common;
using CadExchange.Database.Entities;
using CadExchange.Utils;
using System.Windows;
using InfoExPLMXMLSDK.com.infoex.plmxml.types;
using InfoExPLMXMLSDK.com.infoex.plmxml.generic;

namespace CadExchange.Database.Populate
{
    public class PopulatePackage : AbstractplmxmlParser
    {
        #region Declaration
        public Package package { get; protected set; }
        #endregion

        public PopulatePackage(string uri, bool overwrite, string pkgId) : base(uri)
        {
            try
            {
                string plmXmlRelativePath = uri.Replace(CadexSession.userDir, "");
                this.package = new Package { PackageId = pkgId, Path = plmXmlRelativePath } as Package; 

                if (overwrite)
                    removePackage();

                startPopulation();
            }
            catch (Exception e)
            {
                 throw new Exception( "Exception during population of package.", e);
            }
        }

        private void removePackage()
        {
            try
            {
                Session.removeObject(this.package);
            }
            catch (Exception e)
            {
                throw new Exception( "Exception during population of package.", e);
            }
        }

        private void startPopulation()
        {
            try
            {
                Occurrence root = getRootOccurence(getHeader(this.document));
                createAttachLine(root, null);
                getOccurencesRecursive( root );
                Session.persistObject(this.package);

                Tree.PopulateItemTree itemTree = new Tree.PopulateItemTree(this.package.Structures.ToList<PackageStructure>().Find(findLine => findLine.parentId.Length == 0));
                List<Tree.ICadexTreeNode> nodes = itemTree.buildTreeForItem();
                fixFileAttributes(nodes);
            }
            catch (Exception e)
            {
                throw new Exception( "Exception during population of package.", e);
            }
        }

        protected override List<Occurrence> getOccurencesRecursive(Occurrence root)
        {
            try
            {
                Occurrence[] childs = getChildOccurences(root);

                if (childs != null && childs.Length > 0)
                {
                    foreach (Occurrence child in childs)
                    {
                        createAttachLine(child, root);
                        getOccurencesRecursive(child);
                    }
                }
            }
            catch (Exception e)
            {
                 throw new Exception( "Exception during population of package.", e);
            }

            return null;
        }

        private void createAttachLine(Occurrence child, Occurrence parent)
        {
            try
            {
                PackageStructure pkgSt = new PackageStructure();
                pkgSt.childId = child.getId();
                pkgSt.orgPackage = this.package;
                pkgSt.parentId = parent != null ? parent.getId() : "";
                package.addStructureToPackage(pkgSt);
                PackageLineItem newLineItem = new PackageLineItem();
                ProductRevision prodRev = getProductRevision(child);
                string itemId = getProduct(prodRev).getAttributeValueAsString("productId") + "|" + prodRev.getAttributeValueAsString("revision");
                pkgSt.itemId = itemId;
                pkgSt.childPlmxmlId = prodRev.getId();
                newLineItem.itemId = itemId;
                newLineItem.orgPackage = this.package;
                newLineItem.plmxmlId = pkgSt.childId;
                newLineItem.PartOwner = PackageLineItem.OwnerType.OEM;
                if(!package.LineItems.Contains(newLineItem))
                    package.addLineItemToPackage(newLineItem);
            }
            catch (Exception e)
            {
                throw new Exception( "Exception during population of package.", e);
            }
        }

        private void fixFileAttributes(List<Tree.ICadexTreeNode> nodes)
        {
            try
            {
                if(nodes.Count == 1)
                    traverseChildren((Tree.ICadexItemTreeNode)nodes.ElementAt<Tree.ICadexTreeNode>(0));
            }
            catch (Exception e)
            {
                throw new Exception("Exception during fixing of file attributes", e);
            }
        }

        private void traverseChildren(Tree.ICadexItemTreeNode treeNode)
        {
            try
            {
                foreach (Tree.dataset.ICadexDatasetNode dset in treeNode.Datasets)
                {
                    foreach( Tree.dataset.ICadexDatasetFileNode file in dset.ChildrenList)
                    {
                        fixAttribute(file);
                    }
                }

                foreach (Tree.ICadexTreeNode child in treeNode.ChildrenList)
                {
                    traverseChildren((Tree.ICadexItemTreeNode)child);
                }
            }
            catch (Exception e)
            {
                throw new Exception("Exception during population of package.", e);
            }
        }

        private void fixAttribute(Tree.dataset.ICadexDatasetFileNode file)
        {
            try
            {

                string lasMod = file.getAttr("last_mod_date");
                if (lasMod != null && lasMod.Length > 0)
                {                    
                    DateTime actualTime = DateTime.Parse(lasMod);
                    //CommonUtil.setReadOnly(file.ExternalFile, false);
                    CommonUtil.setModified(file.ExternalFile, actualTime);
                    //CommonUtil.setReadOnly(file.ExternalFile, true);
                }                   
            }
            catch (Exception e)
            {
                throw new Exception("Exception during setting of attribute.", e);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadExchange.Tree;
using CadExchange.Tree.dataset;
using System.IO;
using CadExchange.Common;
using CadExchange.Database.Entities;
using CadExchange.Utils;
using InfoExPLMXMLSDK.com.infoex.plmxml.types;
using InfoExPLMXMLSDK.com.infoex.plmxml.generic;

namespace CadExchange.Database.Populate
{
    public class PopulatePlmxml : AbstractStructureWriter<PLMXMLDocument,ICadexItemTreeNode,ICadexDatasetNode,ICadexDatasetFileNode>
    {
        /**
        * This variable points to the instancedRef attribute of the occurrence.
        */
        private static String instanceRefXMLAttr = "instancedRef";

        private ProductView thisProductView = null;
        private InstanceGraph thisWindow = null;
        private View thisView = null;
        private AccessIntent thisSite = null;

        private Dictionary<string, string> newItemMap = new Dictionary<string, string>();

        private string newFilePath = "";
        private string newFileDir = "";

        public PopulatePlmxml(PLMXMLDocument doc, ICadexItemTreeNode rootNode, string newPlmxml, string newPlmxmlFolder)
            : base(doc, rootNode)
        {
            ProductView[] prodViewList = (ProductView[])getDocument().getElementsByClass<ProductView>("ProductView");
            if (prodViewList != null && prodViewList.Length == 1)
                thisProductView = prodViewList[0];

            ProductDef[] prodDefList = (ProductDef[])getDocument().getElementsByClass<ProductDef>("ProductDef");
            if (prodDefList != null && prodDefList.Length == 1)
            {
                InstanceGraph[] prodWindows = (InstanceGraph[])prodDefList[0].getElementsByClass<InstanceGraph>("InstanceGraph");
                if (prodWindows != null && prodWindows.Length == 1)
                    thisWindow = prodWindows[0];
            }

            View[] prodView = (View[])getDocument().getElementsByClass<View>("View");
            if (prodView != null && prodView.Length == 1)
                thisView = prodView[0];

            AccessIntent[] prodSite = (AccessIntent[])getDocument().getElementsByClass<AccessIntent>("AccessIntent");
            if (prodSite != null && prodSite.Length == 1)
                thisSite = prodSite[0];

            this.newFilePath = newPlmxml;
            this.newFileDir = newPlmxmlFolder;
        }

        public override bool checkIfNewOccurrence(ICadexItemTreeNode node)
        {
            return node.OccNode.isNew;
        }

        public override void startTraversal()
        {
            try
            {
                string rootId = writeHeader();

                traverseChildren(getRootNode(), rootId);

                List<PackageStructure> remStructures = Session.getRemovedOccurrences(getRootNode().PkgLineItem.orgPackage);

                foreach(PackageStructure remStruct in remStructures)
                {
                    Occurrence remOcc = (Occurrence)getDocument().resolveId(remStruct.childId);
                    Occurrence parentOcc = (Occurrence)getDocument().resolveId(removeHashFromID(remOcc.getParentURI()));
                    string remOccStr = remOcc.getId();

                    string[] instRefs = remOcc.getInstanceURIs();

                    if(instRefs.Length == 1)
                    {
                        getDocument().deleteElement((IdBase)getDocument().resolveId(removeHashFromID(instRefs[0])));
                    }

                    

                    //thisProductView.deleteOccurrence(remOcc);
                    string refs = parentOcc.occurrenceRefs;
                    string[] refIds = refs.Split(new char[]{' '});
                    //List<object> refsList = PLMXMLHelper.getHandles((Document<DocumentBase>)(object)getDocument(), new List<string>(refIds));

                    for (int i = 0; i < refIds.Length; i++)
                    {
                        if (removeHashFromID(refIds[i]).Equals(remOcc.getId()))
                        {
                            refs = refs.Replace(refIds[i], "").Trim();
                            break;
                        }
                    }

                    parentOcc.occurrenceRefs = refs;
                    
                    getDocument().deleteElement(remOcc);
                    
                }

                saveDocument();
            }
            catch (Exception e)
            {
                InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);throw;
            }
        }

        public override void traverseChildren(ICadexItemTreeNode node,string id)
        {
            try
            {
                string newOccId = "";

                if (checkIfNewOccurrence(node))
                {
                    newOccId = writeNewOccurrence(node, id);
                }
                else
                {
                    newOccId = node.OccNode.childId;
                }

                string itemStr = node.PkgLineItem.itemId;
                if(!this.newItemMap.ContainsKey(itemStr))
                {
                    string revTagId = ((Occurrence) getDocument().resolveId(newOccId)).instancedRef;
                    ProductRevision revTag = (ProductRevision)getDocument().resolveId( removeHashFromID( revTagId ));
                    checkForNewRelations(node, revTag);
                    addToMap(itemStr);
                }
                

                foreach(ICadexItemTreeNode child in node.ChildrenList)
                {
                    traverseChildren(child, newOccId); 
                }
            }
            catch(Exception e)
            {
                InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);throw;
            }
        }

        public override string writeNewOccurrence(ICadexItemTreeNode node, string id)
        {
            string newOccId = "";
            try
            {
                ProductRevision revTag = null;
                if(node.PkgLineItem.PartOwner == Entities.PackageLineItem.OwnerType.OEM)
                {
                    revTag = (ProductRevision)getDocument().resolveId(node.OccNode.childPlmxmlId);
                }
                else
                {
                    string newRevId = writeNewItemRevision(node);
                    revTag = (ProductRevision)getDocument().resolveId(newRevId);
                }

                

                Occurrence newOcc = new Occurrence();
                newOcc.instancedRef = addHashToID(revTag.getId());

                ApplicationRef newOccRef = new ApplicationRef();
                newOccRef.setApplication("Cadex");

                thisProductView.addElement(newOcc);
                if(node.CadexParent != null)
                {
                    Occurrence parent = (Occurrence)getDocument().resolveId(id);
                    newOcc.setParentURI(addHashToID(parent.getId()));
                    parent.addOccurrenceRef(newOcc);
                    ApplicationRef[] parentAppRefs = parent.getApplicationRefs();
                    string parentLabel = "";
                    if (parentAppRefs.Length == 1)
                        parentLabel = parentAppRefs[0].getLabel();

                    newOccRef.setLabel(parentLabel.Substring(0, parentLabel.IndexOf('/')+1) + "NewOccurrence" +  node.OccNode.Id + newOcc.getId());
                }

                string prodInst = writeProductInstance(node, revTag);

                newOcc.addInstanceURI(addHashToID(prodInst));

                newOcc.addApplicationRef(newOccRef);
                
                newOccId = newOcc.getId();
            }
            catch (Exception e)
            {
                InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);throw;
            }
            return newOccId;
        }

        public string writeProductInstance(ICadexItemTreeNode node, ProductRevision prodRev)
        {
            string newpInstanceId = "";

            try
            {
                ProductInstance prodInst = new ProductInstance();
                thisWindow.addElement(prodInst);

                ApplicationRef newProdInstRef = new ApplicationRef();
                newProdInstRef.setVersion("NewProductInstance" + prodInst.getId());
                newProdInstRef.setApplication("Cadex");
                newProdInstRef.setLabel("NewProductInstance1" + prodInst.getId());
                prodInst.addApplicationRef(newProdInstRef);

                string revViewId = writeProductRevView(prodRev, node.ChildrenList.Count > 0 ? true : false);

                prodInst.setPartURI(addHashToID(revViewId));
                prodInst.addAccessURI(addHashToID(thisSite.getId()));

                newpInstanceId = prodInst.getId();
            }
            catch (Exception e)
            {
                InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);throw;
            }
            return newpInstanceId;
        }

        public string writeProductRevView(ProductRevision prodRev, bool isParent)
        {
            string newpRevViewId = "";

            try
            {
                ProductRevisionView revView = new ProductRevisionView();
                thisWindow.addElement(revView);

                ApplicationRef newProdRevViewRef = new ApplicationRef();
                string prodAppVersion = "NewProductRevisionView" + revView.getId();
                newProdRevViewRef.setVersion(prodAppVersion);
                newProdRevViewRef.setApplication("Cadex");
                if (isParent)
                    newProdRevViewRef.setLabel("NewProductRevisionView1" + revView.getId());
                else
                {
                    Product prod = (Product)getDocument().resolveId(removeHashFromID( prodRev.getMasterURI() ));
                    string appName = "",
                           appLabel = "",
                           appVersion = "";
                    getAppRefDetails(prod, out appName, out appLabel, out appVersion);

                    newProdRevViewRef.setLabel(appLabel + "/" + prodAppVersion + "/");
                }

                revView.setRevisionURI(addHashToID(prodRev.getId()));
                revView.addApplicationRef(newProdRevViewRef);
                revView.addAccessURI(addHashToID(thisSite.getId()));

                newpRevViewId = revView.getId();
            }
            catch (Exception e)
            {
                InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);throw;
            }
            return newpRevViewId;
        }

        private string getItemId(ICadexItemTreeNode node)
        {
            string itemStr = node.PkgLineItem.itemId;
            string[] split = itemStr.Split(new Char[] { '|' });
            return split[0];
        }

        private string getRevId(ICadexItemTreeNode node)
        {
            string itemStr = node.PkgLineItem.itemId;
            string[] split = itemStr.Split(new Char[] { '|' });
            return split[1];
        }

        public override string writeNewItemRevision(ICadexItemTreeNode node)
        {
            string newRevId = "";

            try
            {
                string revisionId = ((SupplierItemNode)node).getObject().RevId;
                string itemId = ((SupplierItemNode)node).getObject().ParentItem.ItemId;
                if(newItemMap.ContainsKey(itemId + "|" + revisionId))
                {
                    newItemMap.TryGetValue(itemId + "|" + revisionId, out newRevId);
                }
                else
                {
                    ProductRevision newRev = new ProductRevision();
                    newRev.setName("New Revision");
                    newRev.setSubType("ItemRevision");
                    newRev.setRevision(revisionId);
                    getDocument().addElement(newRev);

                    string productId = writeNewItem(node);
                    newRev.setMasterURI(addHashToID(productId));
                    ApplicationRef newProdRevRef = new ApplicationRef();
                    string prodAppVersion = "NewProductRevision" + node.PkgLineItem.Id + newRev.getId();
                    newProdRevRef.setVersion(prodAppVersion);
                    
                    string appName = "",
                           appLabel = "",
                           appVersion = "";

                    getAppRefDetails((DescriptionBase)getDocument().resolveId(productId), out appName, out appLabel, out appVersion);
                    
                    newProdRevRef.setApplication(appName);
                    newProdRevRef.setLabel(appLabel);
                    newRev.addApplicationRef(newProdRevRef);
                    newRev.addAccessURI(addHashToID(thisSite.getId()));


                    newRevId = newRev.getId();
                }
                
            }
            catch (Exception e)
            {
                InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);throw;
            }
            return newRevId;
        }

        private void checkForNewRelations(ICadexItemTreeNode node, ProductRevision parentId)
        {
            
            foreach(ICadexDatasetNode dSet in node.Datasets)
            {
                if (dSet is SDatasetStructureNode)
                    writeNewRelation(dSet, parentId.getId());
                else
                    modifyDataset(dSet);
            }
            
        }

        public override void modifyDataset(ICadexDatasetNode Node)
        {
            bool datasetModified = false;

            foreach (ICadexDatasetFileNode file in Node.ChildrenList)
            {
                bool isFileModified = modifyFile(file);

                if (isFileModified && !datasetModified)
                    datasetModified = true;
            }

            if (datasetModified)
            {
                string dsetId = ((DatasetStructureNode)Node).getObject().getId();
                DataSet modDset = (DataSet)getDocument().resolveId(dsetId);
                modOrSetAttr("last_mod_date", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"), modDset);
            }
        }

        public override bool modifyFile(ICadexDatasetFileNode Node)
        {
            bool isFileModified = false;

            DateTime fileTime = Utils.CommonUtil.getModified(Node.ExternalFile);
            string lasMod = Node.getAttr("last_mod_date");
            if(lasMod!=null && lasMod.Length>0)
            {
                DateTime orgFileTime = DateTime.Parse(lasMod);

                if (fileTime > orgFileTime)
                    isFileModified = true;
            }
            else
                isFileModified = true;

            if (isFileModified)
            {
                if (Node is DatasetFileNode)
                {
                    string fileId = ((DatasetFileNode)Node).getObject().getId();
                    ExternalFile modFile = (ExternalFile)getDocument().resolveId(fileId);
                    modOrSetAttr("last_mod_date", fileTime.ToString("yyyy-MM-ddTHH:mm:ss"), modFile);
                }
            }

            return isFileModified;
        }

        private void modOrSetAttr(string attrName, string attrValue, AttribOwnerBase baseObject)
        {
            try
            {
                AttributeBase[] userDatas = baseObject.getAttributes("UserData");
                for (int i = 0; i < userDatas.Length; i++)
                {
                    UserDataElement[] userValues = ((UserData)userDatas[i]).Items;

                    for (int j = 0; j < userValues.Length; j++)
                    {
                        if(attrName.Equals(userValues[j].getTitle()))
                            userValues[j].setValue(attrValue);
                    }

                }
            }
            catch (Exception e)
            {
                InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);throw;
            }
        }

        public override string writeNewItem(ICadexItemTreeNode node)
        {
            string newItemId = "";

            try
            {
                string itemId = ((SupplierItemNode)node).getObject().ParentItem.ItemId;

                Product newItem = new Product();
                newItem.setName("New Item");
                newItem.setSubType("Item");
                newItem.setProductId(itemId);

                getDocument().addElement(newItem);

                ApplicationRef newProdRef = new ApplicationRef();
                string prodAppVersion = "NewProduct" + node.PkgLineItem.Id + newItem.getId();
                newProdRef.setVersion(prodAppVersion);
                newProdRef.setApplication("Cadex");
                newProdRef.setLabel(prodAppVersion);

                newItem.addApplicationRef(newProdRef);
                newItem.addAccessURI(addHashToID(thisSite.getId()));

                newItemId = newItem.getId();
            }
            catch (Exception e)
            {
                InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);throw;
            }
            return newItemId;
        }

        /**
      * This function removes the # from the ID value. This is required to
      * resolve the XML element from the XML doc by using the ID.
      *
      * @param idWithHash The ID string with the # value
      * @return idWithoutHash The ID string
      */
     public static string removeHashFromID(string idWithHash) {
         String idWithoutHash = "";

         if (idWithHash != null && idWithHash.Length > 0) {
             idWithoutHash =  idWithHash.Replace("#", "");
         }

         return idWithoutHash;
     }

     /**
      * This function adds the # to the input string. This is needed
      * to add the references to other XML elements in the XML file.
      *
      * @param idWithoutHash The ID string without #
      * @return idWithHash The id with "#" prefixed to it
      */
     public static string addHashToID(string idWithoutHash) {
         string idWithHash = "";

         if (idWithoutHash != null && idWithoutHash.Length > 0) {
             idWithHash =  idWithoutHash.Replace("id", "#id");
         }

         return idWithHash;
     }

     public override void saveDocument()
     {
         getDocument().save(newFilePath);
     }

     public override string writeNewDataset(ICadexDatasetNode node)
     {
         string newDsetId = "";

         try
         {
             DataSet newDset = new DataSet();
             newDset.setName(node.getAttr("object_name"));
             newDset.setType(node.getAttr("object_type"));
             newDset.setVersion(1);
             getDocument().addElement(newDset);
             checkForNewFiles(node, newDset);
             newDset.addAccessURI(addHashToID(thisSite.getId()));

             ApplicationRef newDsetRef = new ApplicationRef();
             string prodAppVersion = "NewDataSet" + ((SDatasetStructureNode)node).getObject().Id + newDset.getId();
             newDsetRef.setVersion(prodAppVersion);
             newDsetRef.setApplication("Cadex");
             newDsetRef.setLabel(prodAppVersion);

             newDset.addApplicationRef(newDsetRef);

             newDsetId = newDset.getId();
         }
         catch (Exception e)
         {
             InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);throw;
         }

         return newDsetId;
     }
    
     private void checkForNewFiles(ICadexDatasetNode node, DataSet dset)
     {
        foreach(ICadexDatasetFileNode file in node.ChildrenList)
        {
            string newFileId = writeNewFile(file);
            dset.addMemberURI(addHashToID(newFileId));
        }
     }

     public override string writeNewRelation(ICadexDatasetNode Node, string parentId)
     {
         string newRelId = "";

         try
         {
             AssociatedDataSet newRel = new AssociatedDataSet();
             newRel.setRole(Node.Relation);
             newRel.setDataSetURI(addHashToID(writeNewDataset(Node)));
             ((AttribOwnerBase)getDocument().resolveId(parentId)).addAttribute(newRel);
             newRelId = newRel.getId();
         }
         catch (Exception e)
         {
             InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);throw;
         }

         return newRelId;
     }
     public override string writeNewFile(ICadexDatasetFileNode Node)
     {
         string newFileId = "";

         try
         {
             ExternalFile newFile = new ExternalFile();

             if (Node is SDatasetFileNode)
             {
                 newFile.setLocationURI(copyNewFiles(Node.ExternalFile, ((ICadexDatasetNode)Node.CadexParent).Parent.PkgLineItem.PartOwner));
                 newFile.setFormat(Path.GetExtension(Node.ExternalFile).Replace(".",""));
             }

             getDocument().addElement(newFile);
             newFile.addAccessURI(addHashToID(thisSite.getId()));

             ApplicationRef newFileRef = new ApplicationRef();
             string prodAppVersion = "NewExternalFile" + ((SDatasetFileNode)Node).getObject().Id + newFile.getId();
             newFileRef.setVersion(prodAppVersion);
             newFileRef.setApplication("Cadex");
             newFileRef.setLabel(prodAppVersion);

             newFile.addApplicationRef(newFileRef);

             newFileId = newFile.getId();
         }
         catch (Exception e)
         {
             InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);throw;
         }

         return newFileId;
     }
     public override string writeHeader()
     {
         return "";
     }

     private void addToMap(string itemString)
     {
         this.newItemMap.Add(itemString,"");
     }

     private void getAppRefDetails(DescriptionBase tag, out string appName, out string appLabel, out string appVersion)
     {
         try
         {
             appName = "";
             appLabel = "";
             appVersion = "";

             ApplicationRef[] allRefs = tag.getApplicationRefs();

             if(allRefs!=null && allRefs.Length > 0)
             {
                 appName = allRefs[0].getApplication();
                 appLabel = allRefs[0].getLabel();
                 appVersion = allRefs[0].getVersion();
             }

         }
         catch (Exception e)
         {
             InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);throw;
         }
     }

     private string copyNewFiles(string srcFile, PackageLineItem.OwnerType type)
     {
         string directoryPath = Path.GetDirectoryName(srcFile);
         string fileName = Path.GetFileName(srcFile);

         if(type == PackageLineItem.OwnerType.OEM)
         {
             return Path.GetFileName(directoryPath) + "/" + fileName;
         }
         else
         {
             string itemDirectory = Path.GetFileName(directoryPath);
             string outputItemDir = newFileDir + CadexConstants.CADEX_PATH_SEPARTOR + itemDirectory;
             
             if (!Directory.Exists(outputItemDir))
                 Directory.CreateDirectory(outputItemDir);

             File.Copy(srcFile, outputItemDir + CadexConstants.CADEX_PATH_SEPARTOR + fileName);

             return Path.GetFileName(newFileDir) + "/" + itemDirectory + "/" + fileName;
         }
         
     }

    }
}

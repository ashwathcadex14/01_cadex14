﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using CadExchange.Database.Entities;
using CadExchange.Utils;
using NHibernate.Context;
using CadExchange.Common;


namespace CadExchange.Database
{
    /// <summary>
    /// A Singleton instance to maintain the database session
    /// </summary>
    public sealed class Session
    {
        #region Create Singelton for Session
        
        ISession currentSession = null;
        static readonly Session _instance = new Session();
        static readonly ISessionFactory sessionFactory = _instance.CreateSessionFactory();
        public static Session Instance { get { return _instance; } }
        private static ISessionFactory SessionFactory { get { return sessionFactory; } }
        Session()
        {
        }
        #endregion

        public static ISession GetCurrentSession()
        {
            return _instance.currentSession;
        }

        private ISessionFactory CreateSessionFactory()
        {
            //if (File.Exists(dbPath))
            //{
            return Fluently.Configure()
                .Database(SQLiteConfiguration.Standard.UsingFile(CadexSession.dbPath))
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<App>())
                .ExposeConfiguration(cfg => cfg.SetProperty("current_session_context_class", "web"))
                .ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(false, true))
                .BuildSessionFactory();
            //}
            //else
            //    throw new Exception("The database " + dbPath + " could not be reached. Make sure if the path is accessible.");

        }

        /// <summary>
        /// Start the database transaction
        /// </summary>
        /// <returns></returns>
        public static bool Cadex_BeginRequest()
        {
            bool sessionThere = false;

            if (_instance.currentSession == null || (_instance.currentSession != null && !_instance.currentSession.IsOpen))
            {
                _instance.currentSession = SessionFactory.OpenSession();
                //CurrentSessionContext.Bind(_instance.currentSession);
                _instance.currentSession.BeginTransaction();
            }
            else
            {
                sessionThere = true;
                //SessionFactory.GetCurrentSession();
            }

            return sessionThere;
        }

        /// <summary>
        /// Commits or rollbacks the transaction
        /// </summary>
        /// <param name="rollback">is rollback</param>
        public static void Cadex_EndRequest(bool rollback)
        {
            //_instance.currentSession = CurrentSessionContext.Unbind(SessionFactory);

            if (_instance.currentSession == null)
                return;

            try
            {
                if (rollback)
                    throw new Exception("Rollback");
                _instance.currentSession.Transaction.Commit();
            }
            catch (Exception e)
            {
                _instance.currentSession.Transaction.Rollback();
            }
            finally
            {
                _instance.currentSession.Close();
                _instance.currentSession.Dispose();
                _instance.currentSession = null;
            }

        }

        // Will be used in the installation script
        public static void createDB()
        {
            //if (!File.Exists(dbPath))
            //{
            using (var session = Session.SessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    // Create the Default Admin user
                    var user = new CadExchange.Database.Entities.User { UserId = "infoexadmin", Password = /*CommonUtil.EncryptBase(*/"a"/*)*/, FirstName = "InfoEx", LastName = "Administrator", IsAdministrator = true, Status = 1 };
                    session.SaveOrUpdate(user);
                    transaction.Commit();
                    transaction.Dispose();
                }
                session.Dispose();
            }
            //}
        }


        
        /// <summary>
        /// Get the user from DB with given user id
        /// </summary>
        /// <param name="value"></param>
        /// <returns>User</returns>
        public static CadExchange.Database.Entities.User getUserByUserId(string value)
        {
            CadExchange.Database.Entities.User usr = null;

            bool status = Cadex_BeginRequest();

            var user =
                _instance.currentSession.QueryOver<CadExchange.Database.Entities.User>()
                    .Where(u => u.UserId == value)
                    .List();
            usr = user.Count > 0 ? user[0] : null;

            if (!status)
                Cadex_EndRequest(false);

            return usr;
        }

        /// <summary>
        /// Get the user from DB with given user id
        /// </summary>
        /// <param name="value"></param>
        /// <returns>IList<User></returns>
        public static IList<User> getUsers()
        {
            IList<User> usr = new List<User>();

            bool status = Cadex_BeginRequest();

            usr = _instance.currentSession.QueryOver<User>().List();

            if (!status)
                Cadex_EndRequest(false);

            return usr;
        }

        /// <summary>
        /// Get the user from DB with given user id
        /// </summary>
        /// <param name="value"></param>
        /// <returns>IList<T></returns>
        public static IList<T> getRecords<T>() where T : class
        {
            IList<T> usr = new List<T>();

            bool status = Cadex_BeginRequest();

            usr = _instance.currentSession.QueryOver<T>().List();

            if (!status)
                Cadex_EndRequest(false);

            return usr;
        }

        /// <summary>
        /// Get the object from DB with given id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>T</returns>
        public static T getObjectById<T>(string id) where T : class
        {
            T obj = null;

            bool status = Cadex_BeginRequest();
            obj = _instance.currentSession.CreateCriteria<T>().Add(NHibernate.Criterion.Restrictions.Eq("Id", Convert.ToInt32(id))).UniqueResult<T>();

            if (!status)
                Cadex_EndRequest(false);
            return obj;
        }

        /// <summary>
        /// Get the root line of a package
        /// </summary>
        /// <param name="pkg">Package</param>
        /// <returns>PackageStructure</returns>
        public static PackageStructure getRootLine(Package pkg)
        {
            PackageStructure root;
            bool status = Cadex_BeginRequest();
            root = _instance.currentSession.CreateCriteria<PackageStructure>().Add(NHibernate.Criterion.Restrictions.Eq("orgPackage", pkg)).Add(NHibernate.Criterion.Restrictions.Eq("parentId", "")).UniqueResult<PackageStructure>();
            if (!status)
                Cadex_EndRequest(false);
            return root;
        }

        /// <summary>
        /// Get the line Item from PackageStructure
        /// </summary>
        /// <param name="node">PackageStructure</param>
        /// <param name="checkPackage">bool</param>
        /// <returns>PackageLineItem</returns>
        [Obsolete("This method will be removed(or made private) from Beta version. Please use Session.getLineFromStructure1")]
        public static PackageLineItem getLineFromStructure(PackageStructure node, bool checkPackage)
        {
            PackageLineItem line;
            bool status = Cadex_BeginRequest();
            if (checkPackage)
                line = _instance.currentSession.CreateCriteria<PackageLineItem>().Add(NHibernate.Criterion.Restrictions.Eq("orgPackage", node.orgPackage)).Add(NHibernate.Criterion.Restrictions.Eq("itemId", node.itemId)).UniqueResult<PackageLineItem>();
            else
                line = _instance.currentSession.CreateCriteria<PackageLineItem>().Add(NHibernate.Criterion.Restrictions.Eq("itemId", node.itemId)).UniqueResult<PackageLineItem>();

            if (!status)
                Cadex_EndRequest(false);
            return line;
        }

        /// <summary>
        /// Get the line Item from PackageStructure
        /// </summary>
        /// <remarks>
        /// This method doesnt need a session and will rely on Package object
        /// to gather line item. However for initial testing we have used Session
        /// to facilitate lazy loading. This can be moved to CadexSession based on testing.
        /// </remarks>
        /// <param name="node">Occurrence</param>
        /// <param name="pkg">Package - make it null in case of supplier part</param>
        /// <returns>PackageLineItem</returns>
        public static PackageLineItem getLineFromStructure1(PackageStructure node, Package pkg)
        {
            PackageLineItem line = null;
            bool status = Cadex_BeginRequest();

            if (node != null && pkg != null)
            {
                IList<PackageLineItem> pkgLineItems = pkg.LineItems;
                if (pkgLineItems != null && pkgLineItems.Count > 0)
                {
                    foreach (PackageLineItem lineItem in pkgLineItems)
                    {
                        if (node.itemId == lineItem.itemId)
                        {
                            line = lineItem;
                            break;
                        }
                    }
                }
            }
            else
                return getLineFromStructure(node, false);

            if (!status)
                Cadex_EndRequest(false);

            return line;
        }

        /// <summary>
        /// Re-queries the package line item
        /// </summary>
        /// <param name="lineItem"></param>
        /// <returns>PackageLineItem</returns>
        public static PackageLineItem refreshPackageLineItem(PackageLineItem lineItem)
        {
            PackageLineItem line = null;
            if (lineItem != null)
            {
                bool status = Cadex_BeginRequest();

                if (lineItem.orgPackage != null)
                    line = _instance.currentSession.CreateCriteria<PackageLineItem>().Add(NHibernate.Criterion.Restrictions.Eq("orgPackage", lineItem.orgPackage)).Add(NHibernate.Criterion.Restrictions.Eq("itemId", lineItem.itemId)).UniqueResult<PackageLineItem>();

                if (!status)
                    Cadex_EndRequest(false);
            }

            return line;
        }

        /// <summary>
        /// Get the structure from line item. Assuming all the structure will have same structure
        /// </summary>
        /// <param name="item">PackageLineItem</param>
        /// <param name="checkPackage">bool</param>
        /// <returns>PackageStructure</returns>
        public static PackageStructure getStructureFromItem(PackageLineItem item, bool checkPackage)
        {
            PackageStructure line;
            bool status = Cadex_BeginRequest();
            if (checkPackage)
                line = _instance.currentSession.CreateCriteria<PackageStructure>().Add(NHibernate.Criterion.Restrictions.Eq("orgPackage", item.orgPackage)).Add(NHibernate.Criterion.Restrictions.Eq("itemId", item.itemId)).List<PackageStructure>()[0];
            else
                line = _instance.currentSession.CreateCriteria<PackageStructure>().Add(NHibernate.Criterion.Restrictions.Eq("itemId", item.itemId)).List<PackageStructure>()[0];

            if (!status)
                Cadex_EndRequest(false);
            return line;
        }

        /// <summary>
        /// Get the child lines for PackageStructure
        /// </summary>
        /// <param name="node">PackageStructure</param>
        /// <param name="checkPackage">bool</param>
        /// <returns>List<PackageStructure></returns>
        [Obsolete("This method will be removed(or made private) from Beta version. Please use Session.getStructureChilds1")]
        public static List<PackageStructure> getStructureChilds(PackageStructure node, bool checkPackage)
        {
            List<PackageStructure> lines = new List<PackageStructure>();
            bool status = Cadex_BeginRequest();

            if (checkPackage)
                lines = (List<PackageStructure>)_instance.currentSession.CreateCriteria<PackageStructure>().Add(NHibernate.Criterion.Expression.Eq("orgPackage", node.orgPackage)).Add(NHibernate.Criterion.Expression.Eq("parentId", node.childId)).Add(NHibernate.Criterion.Expression.Eq("isRemoved", false)).List<PackageStructure>();
            else
                lines = (List<PackageStructure>)_instance.currentSession.CreateCriteria<PackageStructure>().Add(NHibernate.Criterion.Expression.Eq("parentId", node.childId)).Add(NHibernate.Criterion.Expression.Eq("isRemoved", false)).List<PackageStructure>();

            if (!status)
                Cadex_EndRequest(false);
            return lines;
        }

        /// <summary>
        /// Get the child lines for PackageStructure
        /// </summary>
        /// <remarks>
        /// This method doesnt need a session and will rely on Package object
        /// to gather line item. However for initial testing we have used Session
        /// to facilitate lazy loading. This can be moved to CadexSession based on testing.
        /// </remarks>
        /// <param name="node">PackageStructure</param>
        /// <returns>List<PackageStructure></returns>
        public static List<PackageStructure> getStructureChilds1(PackageStructure node)
        {
            List<PackageStructure> lines = new List<PackageStructure>();
            bool status = Cadex_BeginRequest();

            if (node.orgPackage != null)
            {
                IList<PackageStructure> pkgStructures = node.orgPackage.Structures;
                if (pkgStructures != null && pkgStructures.Count > 0)
                {
                    foreach (PackageStructure pkgStructure in pkgStructures)
                    {
                        if (pkgStructure.parentId == node.childId && !pkgStructure.isRemoved)
                        {
                            lines.Add(pkgStructure);
                        }
                    }
                }
            }
            else
                getStructureChilds(node, false);

            if (!status)
                Cadex_EndRequest(false);
            return lines;
        }

        /// <summary>
        /// Get the plmxml id of the Product Revision from item id and revision id
        /// </summary>
        /// <param name="itemid">item_id|item_revision_id</param>
        /// <returns>plmxml id of the Product Revision tag with mentioned item id and revision id</returns>
        public static string getOEMRevisionId(string itemid, Package orgPackage)
        {
            InfoExPLMXMLSDK.com.infoex.plmxml.types.ProductRevision rev = plmxmlUtil.getProductRevision(orgPackage.getPlmXMLPath(), itemid);
            if (rev != null)
            {
                return rev.id;
            }
            else
                throw new Exception("Product Revision " + itemid + " not found.");
        }

        /// <summary>
        /// Get the child lines for PackageStructure with this itemid
        /// </summary>
        /// <param name="node">PackageStructure</param>
        /// <param name="itemId">string</param>
        /// <returns>List<PackageStructure></returns>
        public static List<PackageStructure> getStructureChildWithItem(PackageStructure node, string itemId)
        {
            List<PackageStructure> lines = new List<PackageStructure>();
            bool status = Cadex_BeginRequest();

            lines = (List<PackageStructure>)_instance.currentSession.CreateCriteria<PackageStructure>().Add(NHibernate.Criterion.Expression.Eq("parentId", node.childId)).Add(NHibernate.Criterion.Expression.Eq("itemId", itemId)).List<PackageStructure>();

            if (!status)
                Cadex_EndRequest(false);
            return lines;
        }

        /// <summary>
        /// Get the remvoed OEM lines for Package
        /// </summary>
        /// <param name="pkg">Package</param>
        /// <returns>List<PackageStructure></returns>
        public static List<PackageStructure> getRemovedOccurrences(Package pkg)
        {
            List<PackageStructure> lines = new List<PackageStructure>();
            bool status = Cadex_BeginRequest();

            lines = (List<PackageStructure>)_instance.currentSession.CreateCriteria<PackageStructure>().Add(NHibernate.Criterion.Expression.Eq("orgPackage", pkg)).Add(NHibernate.Criterion.Expression.Eq("isRemoved", true)).List<PackageStructure>();

            if (!status)
                Cadex_EndRequest(false);
            return lines;
        }


        /// <summary>
        /// Get the other lines for PackageStructure Child Item
        /// </summary>
        /// <param name="node">PackageStructure</param>
        /// <param name="excludeInput">bool</param>
        /// <returns>List<PackageLineItem></PackageLineItem></returns>
        public static List<PackageStructure> getOtherOccurrences(PackageStructure node, bool excludeInput)
        {
            List<PackageStructure> lines = new List<PackageStructure>();
            bool status = Cadex_BeginRequest();
            List<PackageStructure> lines1 = (List<PackageStructure>)_instance.currentSession.CreateCriteria<PackageStructure>().Add(NHibernate.Criterion.Expression.Eq("itemId", node.itemId)).List<PackageStructure>();
            lines.AddRange(excludeInput ? lines1.FindAll(findLine => findLine.Id != node.Id) : lines1);
            if (!status)
                Cadex_EndRequest(false);
            return lines;
        }


        /// <summary>
        /// Get the root occurrence of the item
        /// </summary>
        /// <param name="itemId">ItemID</param>
        /// <returns>PackageStructure</returns>
        public static PackageStructure getRootOccurrence(string itemId)
        {
            PackageStructure line = null;
            bool status = Cadex_BeginRequest();
            List<PackageStructure> lines1 = (List<PackageStructure>)_instance.currentSession.CreateCriteria<PackageStructure>().Add(NHibernate.Criterion.Expression.Eq("itemId", itemId)).List<PackageStructure>();
            List<PackageStructure> found = lines1.FindAll(findLine => (findLine.parentId == "" || findLine.parentId == null));
            if (found != null && found.Count == 1)
            {
                line = found[0];
            }

            if (!status)
                Cadex_EndRequest(false);
            return line;
        }

        /// <summary>
        /// Get the config for ext
        /// </summary>
        /// <param name="ext">string</param>
        /// <returns>DatasetConfiguration</returns>
        public static DatasetConfiguration getDatasetConfig(string ext)
        {
            DatasetConfiguration dSetConfig;
            bool status = Cadex_BeginRequest();

            dSetConfig = _instance.currentSession.CreateCriteria<DatasetConfiguration>().Add(NHibernate.Criterion.Expression.Eq("ext", ext.ToLower())).UniqueResult<DatasetConfiguration>();

            if (!status)
                Cadex_EndRequest(false);
            return dSetConfig;
        }

        /// <summary>
        /// Check if the package with same id already exists
        /// </summary>
        /// <param name="packageid"></param>
        /// <returns>boolean</returns>
        public static bool IsPackageExists(string packageid)
        {
            bool bIsExists = false;

            bool status = Cadex_BeginRequest();

            var package = _instance.currentSession.QueryOver<Package>().Where(p => p.PackageId == packageid).List();
            bIsExists = package.Count > 0 ? true : false;

            if (!status)
                Cadex_EndRequest(false);
            return bIsExists;
        }

        /// <summary>
        /// Persists the object in the database
        /// </summary>
        /// <param name="obj"></param>
        public static void persistObject(Object obj)
        {

            bool status = Cadex_BeginRequest();

            _instance.currentSession.SaveOrUpdate(obj);

            if (!status)
                Cadex_EndRequest(false);

        }

        /// <summary>
        /// Removes the object from the database
        /// </summary>
        /// <param name="obj"></param>
        public static void removeObject(Object obj)
        {

            bool status = Cadex_BeginRequest();

            _instance.currentSession.Delete(obj);

            if (!status)
                Cadex_EndRequest(false);
        }

        public static void refreshObject(Object obj)
        {
            bool status = Cadex_BeginRequest();

            _instance.currentSession.Refresh(obj);

            if (!status)
                Cadex_EndRequest(false);
        }

        private class WorkflowAuditListener : NHibernate.Event.IPreInsertEventListener, NHibernate.Event.IPreUpdateEventListener
        {
            public bool OnPreUpdate(NHibernate.Event.PreUpdateEvent e)
            {
                return true;
            }

            public bool OnPreInsert(NHibernate.Event.PreInsertEvent e)
            {
                return true;
            }
        }

    }
}

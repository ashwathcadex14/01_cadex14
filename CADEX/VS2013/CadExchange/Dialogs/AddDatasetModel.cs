﻿using CadExchange.Database;
using CadExchange.Database.Entities;
using FirstFloor.ModernUI.Presentation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using CadExchange.Common;
using NHibernate;
using NHibernate.Criterion;
using System.Windows;
using CadExchange.Utils;
using CadExchange.Tree;

namespace CadExchange.Dialogs
{
    public class AddDatasetModel : NotifyPropertyChanged, IDataErrorInfo
    {
        private string filePath;
        private string objectName;
        private string objectDesc;
        private string objectType;
        private bool showOthers = false;
        private string errorDetails = "Check For Problems";
        private ICommand submitCommand;
        private ICommand checkCommand;
        private ICommand browseCommand = null;
        private Visibility _isBusyIndicator = Visibility.Hidden;
        ICadexItemTreeNode currentNode;
        

        public AddDatasetModel(ICadexItemTreeNode itemNode)
        {
            currentNode = itemNode;
        }

        /// <summary>
        /// Binding for busy indicator
        /// </summary>
        public Visibility BusyIndicator
        {
            get { return this._isBusyIndicator; }
            set
            {
                this._isBusyIndicator = value;
                OnPropertyChanged("BusyIndicator");
            }
        }

        public bool ShowOtherColumns
        {
            get { return this.showOthers; }
            set
            {
                this.showOthers = value;
                OnPropertyChanged("ShowOtherColumns");
            }
        }

        public string FilePath
        {
            get { return this.filePath; }
            set 
            {
                if (this.filePath != value)
                {
                    this.filePath = value;
                    ShowOtherColumns = false;
                    OnPropertyChanged("FilePath");
                }
            }
        }

        public string Name
        {
            get { return this.objectName; }
            set
            {
                if (this.objectName != value)
                {
                    this.objectName = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        public string Description
        {
            get { return this.objectDesc; }
            set
            {
                if (this.objectDesc != value)
                {
                    this.objectDesc = value;
                    OnPropertyChanged("Description");
                }
            }
        }

        public string Type
        {
            get { return this.objectType; }
            set
            {
                if (this.objectType != value)
                {
                    this.objectType = value;
                    OnPropertyChanged("Type");
                }
            }
        }

        public string Error
        {
            get { return null; }
        }

        public string this[string columnName]
        {
            get
            {
                if (columnName == "FilePath")
                {
                    return string.IsNullOrEmpty(this.filePath) ? "Mandatory" : !ShowOtherColumns ? errorDetails : null;
                }
                if (columnName == "Name")
                {
                    return string.IsNullOrEmpty(this.objectName) ? "Mandatory" : null;
                }
                //if (columnName == "Description")
                //{
                //    return string.IsNullOrEmpty(this.objectDesc) ? "Mandatory" : null;
                //}
                return null;
            }
        }

        /// <summary>
        /// Binding for Browse button command
        /// </summary>
        public ICommand BrowseCommand
        {
            get
            {
                if (browseCommand == null)
                {
                    browseCommand = new RelayCommand(
                        param => this.OpenFileDialog(),
                        param => true
                    );
                }
                return browseCommand;
            }
        }

        public ICommand SubmitCommand
        {
            get
            {
                if (submitCommand == null)
                {
                    submitCommand = new RelayCommand(
                        param => this.attachDataset(),
                        param => this.CanSubmit()
                    );
                }
                return submitCommand;
            }
        }

        public ICommand CheckCommand
        {
            get
            {
                if (checkCommand == null)
                {
                    checkCommand = new RelayCommand(
                        param => this.checkId(),
                        param => this.CanCheck()
                    );
                }
                return checkCommand;
            }
        }

        private bool CanSubmit()
        {
            if (this.filePath == null || this.filePath.Length <= 0 || this.objectName == null || this.objectName.Length <= 0 || this.objectDesc == null || this.objectDesc.Length <= 0 || this.objectType == null || this.objectType.Length <= 0)
                return false;
            return true;
        }

        private bool CanCheck()
        {
            if (this.filePath == null || this.filePath.Length <= 0)
                return false;
            return true;
        }

        /// <summary>
        /// Open the File dialog to choose the package
        /// </summary>
        private void OpenFileDialog()
        {
            // Create OpenFileDialog
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension
            dlg.DefaultExt = "*.*";
            dlg.Filter = "All Files|*.*";

            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox
            if (result == true)
            {
                FilePath = dlg.FileName;
            }
        }


        public void attachDataset()
        {
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += (o, ea) =>
            {
                try
                {
                    bool status = Session.Cadex_BeginRequest();

                    CadexSupplierDataset newDset = currentNode.addDatasets("IMAN_specification",this.filePath, this.objectName, this.objectType, this.objectDesc);
                    
                    if(newDset!=null)
                        Session.persistObject(newDset);

                    currentNode.refreshDatasets();

                    if(!status) Session.Cadex_EndRequest(false);

                    
                }
                catch (Exception e)
                {
                    ea.Cancel = true;Session.Cadex_EndRequest(true);
					
                    //ea.Result = e.Message;
                    //CommonUtil.ShowModernMessageBox(e.Message, "Error loading Packages", MessageBoxButton.OK);
                }
            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                if(ea.Cancelled)
                    CommonUtil.ShowModernMessageBox("Unable to add dataset.", "Error", MessageBoxButton.OK);
                else
                    CommonUtil.ShowModernMessageBox("Document added successfully !!", "Add Document", MessageBoxButton.OK);

                FilePath = "";

                //We are not busy
                this.BusyIndicator = Visibility.Hidden;

            };

            //Show we are busy
            this.BusyIndicator = Visibility.Visible;
            worker.RunWorkerAsync();
        }

        public void checkId()
        {
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += (o, ea) =>
            {
                try
                {
                    bool status = Session.Cadex_BeginRequest();

                    DatasetConfiguration config = Session.getDatasetConfig("*" + System.IO.Path.GetExtension(filePath));

                    if (config != null)
                        Type = config.type;
                    else
                        Type = "Image";

                    if(!status) Session.Cadex_EndRequest(false);

                    ShowOtherColumns = true;
                           
                }
                catch (Exception e)
                {
                    ea.Cancel = true;Session.Cadex_EndRequest(true);
                    errorDetails = e.Message;
                    //ea.Result = e.Message;
                    //CommonUtil.ShowModernMessageBox(e.Message, "Error loading Packages", MessageBoxButton.OK);
                }

            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                if (ea.Cancelled)
                {
                    CommonUtil.ShowModernMessageBox(errorDetails, "Add Part Error", MessageBoxButton.OK);
                }

                OnPropertyChanged("FilePath");
                

                //We are not busy
                this.BusyIndicator = Visibility.Hidden;

            };

            //Show we are busy
            this.BusyIndicator = Visibility.Visible;
            worker.RunWorkerAsync();
        }
    }
}

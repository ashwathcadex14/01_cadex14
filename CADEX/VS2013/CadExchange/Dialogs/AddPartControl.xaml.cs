﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CadExchange.Tree;

namespace CadExchange.Dialogs
{
    /// <summary>
    /// Interaction logic for AddPartControl.xaml
    /// </summary>
    public partial class AddPartControl : UserControl
    {
        public AddPartControl(ICadexItemTreeNode itemNode)
        {
            InitializeComponent();
            this.DataContext = new AddPartModel(itemNode);
        }
    }
}

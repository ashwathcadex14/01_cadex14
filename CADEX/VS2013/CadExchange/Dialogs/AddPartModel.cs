﻿using CadExchange.Database;
using CadExchange.Database.Entities;
using FirstFloor.ModernUI.Presentation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using CadExchange.Common;
using NHibernate;
using NHibernate.Criterion;
using System.Windows;
using CadExchange.Utils;
using CadExchange.Tree;

namespace CadExchange.Dialogs
{
    public class AddPartModel : NotifyPropertyChanged, IDataErrorInfo
    {
        private string itemid;
        private string revid;
        private bool showOthers = false;
        private string errorDetails = "Check For Problems";
        private ICommand submitCommand;
        private ICommand checkCommand;
        private Visibility _isBusyIndicator = Visibility.Hidden;
        ICadexItemTreeNode currentNode;
        private PackageLineItem toBad;

        public AddPartModel(ICadexItemTreeNode itemNode)
        {
            currentNode = itemNode;
        }

        /// <summary>
        /// Binding for busy indicator
        /// </summary>
        public Visibility BusyIndicator
        {
            get { return this._isBusyIndicator; }
            set
            {
                this._isBusyIndicator = value;
                OnPropertyChanged("BusyIndicator");
            }
        }

        public bool ShowOtherColumns
        {
            get { return this.showOthers; }
            set
            {
                this.showOthers = value;
                OnPropertyChanged("ShowOtherColumns");
            }
        }

        public string ItemId
        {
            get { return this.itemid; }
            set 
            {
                if(this.itemid != value)
                {
                    this.itemid = value;
                    ShowOtherColumns = false;
                    OnPropertyChanged("ItemId");
                }
            }
        }

        public string RevisionId
        {
            get { return this.revid; }
            set
            {
                if (this.revid != value)
                {
                    this.revid = value;
                    ShowOtherColumns = false;
                    OnPropertyChanged("RevisionId");
                }
            }
        }

        public string Error
        {
            get { return null; }
        }

        public string this[string columnName]
        {
            get
            {
                if (columnName == "ItemId")
                {
                    return string.IsNullOrEmpty(this.itemid) ? "Mandatory" : !ShowOtherColumns ? errorDetails : null;
                }

                if (columnName == "RevisionId")
                {
                    return string.IsNullOrEmpty(this.itemid) ? "Mandatory" : !ShowOtherColumns ? errorDetails : null;
                }

                return null;
            }
        }

        public ICommand SubmitCommand
        {
            get
            {
                if (submitCommand == null)
                {
                    submitCommand = new RelayCommand(
                        param => this.attachPart(),
                        param => this.CanSubmit()
                    );
                }
                return submitCommand;
            }
        }

        public ICommand CheckCommand
        {
            get
            {
                if (checkCommand == null)
                {
                    checkCommand = new RelayCommand(
                        param => this.checkId(),
                        param => this.CanCheck()
                    );
                }
                return checkCommand;
            }
        }

        private bool CanSubmit()
        {
            if (this.itemid == null || this.itemid.Length <= 0 )
                return false;
            return true;
        }

        private bool CanCheck()
        {
            if (this.itemid == null || this.itemid.Length <= 0 || this.revid == null || this.revid.Length <= 0 )
                return false;
            return true;
        }

        public void attachPart()
        {
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += (o, ea) =>
            {
                try
                {
                    bool status = Session.Cadex_BeginRequest();

                    if (toBad != null)
                    {
                        List<PackageStructure> newStructures = currentNode.addPart(toBad);
                        foreach(PackageStructure newStruct in newStructures)
                        {
                            Session.persistObject(newStruct);
                        }
                    }
                    else
                        throw new Exception();
        
                    if(!status) Session.Cadex_EndRequest(false);
                }
                catch (Exception e)
                {
                    ea.Cancel = true;Session.Cadex_EndRequest(true);
					
                    //ea.Result = e.Message;
                    //CommonUtil.ShowModernMessageBox(e.Message, "Error loading Packages", MessageBoxButton.OK);
                }
                finally
                {
                    toBad = null;
                }

            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                if(ea.Cancelled)
                    CommonUtil.ShowModernMessageBox("", "Error New Item", MessageBoxButton.OK);
                else
                    CommonUtil.ShowModernMessageBox("Part added successfully !!", "Add Part", MessageBoxButton.OK);

                ItemId = "";

                //We are not busy
                this.BusyIndicator = Visibility.Hidden;

            };

            //Show we are busy
            this.BusyIndicator = Visibility.Visible;
            worker.RunWorkerAsync();
        }

        public void checkId()
        {
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += (o, ea) =>
            {
                try
                {
                    bool status = Session.Cadex_BeginRequest();
                    List<PackageLineItem> lineItems = (List<PackageLineItem>)Session.GetCurrentSession().CreateCriteria<PackageLineItem>().Add(NHibernate.Criterion.Expression.Eq("itemId", (this.itemid + "|" + this.revid))).List<PackageLineItem>();

                    if (lineItems.Count > 0)
                    {
                        checkPart(lineItems[0]);
                        checkCircularError(lineItems[0],currentNode);
                        errorDetails = "";
                        ShowOtherColumns = true;
                        toBad = lineItems[0];
                    }
                    else
                    {
                        errorDetails = "Item Revision not found";
                    }
                       
                    if(!status) Session.Cadex_EndRequest(false);
                           
                }
                catch (Exception e)
                {
                    ea.Cancel = true;Session.Cadex_EndRequest(true);
                    errorDetails = e.Message;
                    //ea.Result = e.Message;
                    //CommonUtil.ShowModernMessageBox(e.Message, "Error loading Packages", MessageBoxButton.OK);
                }

            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                if (ea.Cancelled)
                {
                    CommonUtil.ShowModernMessageBox(errorDetails, "Add Part Error", MessageBoxButton.OK);
                }

                OnPropertyChanged("ItemId");
                

                //We are not busy
                this.BusyIndicator = Visibility.Hidden;

            };

            //Show we are busy
            this.BusyIndicator = Visibility.Visible;
            worker.RunWorkerAsync();
        }

        private void checkPart(PackageLineItem lineItem)
        {
            //if(lineItem.PartOwner == PackageLineItem.OwnerType.OEM && !(lineItem.orgPackage.Id == currentNode.OwningPackage))
            //        throw new Exception("The Part you are trying to add does not belongs to the same package as that of the selected Item.");
                
            if (lineItem.isLocked && !lineItem.LockedBy.Equals(CadexSession.getSessionUser()))
                    throw new Exception("The Part you are trying to add is locked by " + lineItem.LockedBy);
        }

        private void checkCircularError(PackageLineItem lineItem, ICadexItemTreeNode node)
        {

            PackageLineItem currentLine = node.PkgLineItem;

            if (currentLine.Id == lineItem.Id)
                throw new Exception("Circular structure detected.");
            

            if(node.CadexParent!=null)
            {
                checkCircularError(lineItem, (ICadexItemTreeNode)node.CadexParent);
            }
        }
    }
}

﻿using CadExchange.Database;
using CadExchange.Database.Entities;
using FirstFloor.ModernUI.Presentation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using CadExchange.Common;
using NHibernate;
using NHibernate.Criterion;
using System.Windows;
using CadExchange.Utils;
using CadExchange.Tree;
using CadExchange.Database.Populate;

namespace CadExchange.Dialogs
{
    public class ImportPackageModel : INotifyPropertyChanged
    {
        private string _SelectedPath = null;
        private bool showOthers = false;
        private ICommand browseCommand = null;
        private Visibility _isBusyIndicator = Visibility.Hidden;
        private ICommand importCommand = null;
        public event PropertyChangedEventHandler PropertyChanged;
        private bool IsButtonEnabled = false;
        private ImportPackageControl importPackageControl;

        public ImportPackageModel( ImportPackageControl importPackageControl )
        {
            this.importPackageControl = importPackageControl;
        }

        /// <summary>
        /// Binding for the text box to show selected package
        /// </summary>
        public string SelectedPath
        {
            get { return this._SelectedPath; }
            set
            {
                if (this._SelectedPath != value)
                {
                    this._SelectedPath = value;
                    NotifiyPropertyChanged("SelectedPath");
                }
            }
        }

        void NotifiyPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        /// <summary>
        /// Binding for Import button command
        /// </summary>
        public ICommand ImportCommand
        {
            get
            {
                if (importCommand == null)
                {
                    importCommand = new RelayCommand(
                        param => this.ImportPackage(param),
                        param => true
                    );
                }
                return importCommand;
            }
        }

        /// <summary>
        /// Imports the package
        /// </summary>
        private void ImportPackage(object parameter)
        {
            try
            {
                // new package has been given for import
                // Extract the archive; uniqueness with timestamp

                string zipName = System.IO.Path.GetFileNameWithoutExtension(SelectedPath);

                string extractDirectory = CadexSession.getSessionDir() + CadexConstants.CADEX_PATH_SEPARTOR + zipName +
                                          CadexConstants.CADEX_UNDERSCORE_LITERAL + CommonUtil.GetDefaultTimeStamp();
                

                if(isEncrypted(SelectedPath))
                {
                    using (Ionic.Zip.ZipFile archive = new Ionic.Zip.ZipFile(@SelectedPath))
                    {
                        archive.Password = "INFOEX";
                        archive.Encryption = Ionic.Zip.EncryptionAlgorithm.WinZipAes256; // the default: you might need to select the proper value here
                        archive.StatusMessageTextWriter = Console.Out;

                        archive.ExtractAll(@extractDirectory, Ionic.Zip.ExtractExistingFileAction.Throw);
                    }
                }
                else
                    ZipFile.ExtractToDirectory(SelectedPath, extractDirectory);

                string pkgFilesDir = @extractDirectory + CadexConstants.CADEX_PATH_SEPARTOR + zipName;

                

                // Get the plmxml file from the directory
                string xFile = plmxmlUtil.GetPlmxmlFile(extractDirectory);
                if (xFile == null || !File.Exists(xFile))
                {
                    CommonUtil.ShowModernMessageBox("The package does not have PLMXML file.", "Error", MessageBoxButton.OK);
                }
                else
                {
                    startImport(xFile, parameter, pkgFilesDir);
                }
            }
            catch (Exception ex)
            {
                CommonUtil.ShowModernMessageBox(ex.Message, "Error", MessageBoxButton.OK);
            }
        }

        private bool isEncrypted(string zipPath)
        {
            using (var zip = Ionic.Zip.ZipFile.Read(zipPath))
            {
                // check all entries: 
                foreach (var e in zip)
                {
                    if (e.UsesEncryption)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private void startImport(string xFile, object parameter, string pkgFilesDir)
        {
            string pkgId = plmxmlUtil.GetPackageId(xFile);
            if (pkgId == null)
            {
                pkgId = System.IO.Path.GetFileNameWithoutExtension(xFile);
            }
            bool overwrite = true;
            if (Session.IsPackageExists(pkgId))
            {
                if (MessageBoxResult.Cancel == CommonUtil.ShowModernMessageBox("The package with id " + pkgId + " already exists.Do you want to override it?", "Warning", MessageBoxButton.OKCancel))
                {
                    throw new Exception("Operation Cancelled.");
                }
            }

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += (o, ea) =>
            {
                try
                {
                    bool status = Session.Cadex_BeginRequest();
                    new PopulatePackage(xFile, overwrite, pkgId);
                    if (!status) Session.Cadex_EndRequest(false);

                    if ( Directory.Exists(pkgFilesDir))
                    {
                        foreach (string file in Directory.GetFiles(pkgFilesDir))
                        {
                            if( CadexSession.EncryptionEnabled )
                            {
                                CommonUtil.EncryptFile1(file, file + ".encrypt");
                            }
                            CommonUtil.setReadOnly(file, true);
                            
                        }
                    }

                    //loadAll();
                }
                catch (Exception e)
                {
                    //CommonUtil.ShowModernMessageBox("Error occured while importing the package.", "Error Populating Package details " + e, MessageBoxButton.OK);
                    ea.Cancel = true; Session.Cadex_EndRequest(true);
                }

            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                Window.GetWindow(importPackageControl).Close();  // close import package window
                
               
                //We are not busy
                this.BusyIndicator = Visibility.Hidden;
                this.SelectedPath = "";
                if (ea.Cancelled)
                    CommonUtil.ShowModernMessageBox("Error occured while importing the package.", "Error Populating Package details", MessageBoxButton.OK);
                else
                {
                    CommonUtil.ShowModernMessageBox("Package imported successfully.", "", MessageBoxButton.OK);
                }
            };

            //Show we are busy
            this.BusyIndicator = Visibility.Visible;
            worker.RunWorkerAsync();
        }

        /// <summary>
        /// Binding for busy indicator
        /// </summary>
        public Visibility BusyIndicator
        {
            get { return this._isBusyIndicator; }
            set
            {
                this._isBusyIndicator = value;
                NotifiyPropertyChanged("BusyIndicator");
            }
        }

        public bool ShowOtherColumns
        {
            get { return this.showOthers; }
            set
            {
                this.showOthers = value;
                NotifiyPropertyChanged("ShowOtherColumns");
            }
        }

        /// <summary>
        /// Binding property to Enable/Disable the Import button
        /// </summary>
        public bool IsEnabled
        {
            get { return this.IsButtonEnabled; }
            set
            {
                if (this.IsButtonEnabled != value)
                {
                    this.IsButtonEnabled = value;
                    NotifiyPropertyChanged("IsEnabled");
                }
            }
        }

        /// <summary>
        /// Binding for Browse button command
        /// </summary>
        public ICommand BrowseCommand
        {
            get
            {
                if (browseCommand == null)
                {
                    browseCommand = new RelayCommand(
                        param => this.OpenFileDialog(),
                        param => true
                    );
                }
                return browseCommand;
            }
        }

        /// <summary>
        /// Open the File dialog to choose the package
        /// </summary>
        private void OpenFileDialog()
        {
            // Create OpenFileDialog
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension
            dlg.DefaultExt = CadexConstants.CADEX_FILE_EXTENSON_ZIP;
            dlg.Filter = "Zip|*.zip|All Files|*.*";

            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox
            if (result == true)
            {
                SelectedPath = dlg.FileName;
                this.IsEnabled = true;
            }
        }
    }
}

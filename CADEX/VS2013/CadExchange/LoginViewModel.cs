﻿using CadExchange.Database;
using CadExchange.Database.Entities;
using CadExchange.Utils;
using SoftArcs.WPFSmartLibrary.MVVMCommands;
using SoftArcs.WPFSmartLibrary.MVVMCore;
using SoftArcs.WPFSmartLibrary.SmartUserControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows;
using System.Threading;
using System.ComponentModel;
using CadExchange.Common;

namespace CadExchange
{
    public class LoginViewModel : ViewModelBase
    {
        #region Private Fields
        private bool bAccessible = false;
        private LoginWindow window;
        #endregion

        #region Decide if we can close the login window
        public bool IsAccessible
        {
            get { return bAccessible; }
            set
            {
                if (value != this.bAccessible) { this.bAccessible = value; }
            }
        }
        #endregion

        #region Public Methods
        public LoginViewModel(Object window)
        {
            this.window = (LoginWindow)window;

            if (ViewModelHelper.IsInDesignModeStatic == false)
            {
                this.initializeAllCommands();
            }
        }

        public string UserName
        {
            get { return GetValue(() => UserName); }
            set { SetValue(() => UserName, value); }
        }

        public string Password
        {
            get { return GetValue(() => Password); }
            set { SetValue(() => Password, value); }
        }

        public bool IsAdminPortal
        {
            get { return GetValue(() => IsAdminPortal); }
            set { SetValue(() => IsAdminPortal, value); }
        }
        #endregion

        #region Submit Command Handler

        public ICommand SubmitCommand { get; private set; }

        private void ExecuteSubmit(object commandParameter)
        {
            var accessControlSystem = commandParameter as SmartLoginOverlay;

            BackgroundWorker worker = new BackgroundWorker();
            IList<User> user = new List<User>();
            worker.DoWork += (o, ea) =>
            {
                user = this.validateUser(this.UserName, this.Password);
            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                if (accessControlSystem != null)
                {
                    try
                    {
                        if (this.IsAccessible && user != null)
                        {
                            // Verify if the user is active
                            if (user[0].Status != 1)
                            {
                                accessControlSystem.ShowAccountDeactivatedInfo();
                            }
                            else
                            {
                                // If user is in DB and Admin portal is checked he must have Admin access
                                if (this.IsAdminPortal)
                                {
                                    if (!user[0].IsAdministrator)
                                    {
                                        accessControlSystem.ShowNoAdminRightsInfo();
                                    }
                                    else
                                    {
                                        CadexAdminWindow adminwindow = new CadexAdminWindow();
                                        adminwindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                                        adminwindow.SourceInitialized += (s, a) => adminwindow.WindowState = WindowState.Maximized;
                                        adminwindow.Show();
                                        CadexSession.createCadexSession(user[0].UserId);
                                        this.window.Close();
                                    }
                                }
                                else
                                {
                                    CadexUserWindow userWindow = new CadexUserWindow();
                                    userWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                                    userWindow.SourceInitialized += (s, a) => userWindow.WindowState = WindowState.Maximized;
                                    userWindow.Show();
                                    CadexSession.createCadexSession(user[0].UserId);
                                    this.window.Close();
                                    Thread.Sleep(4000);
                                    this.window.Close();
                                }
                            }
                        }
                        else
                        {
                            accessControlSystem.ShowWrongCredentialsMessage();
                        }

                        accessControlSystem.showHideProgress(false);
                    }
                    catch (Exception e)
                    {
                        CommonUtil.ShowModernMessageBox(e.Message, "Error", MessageBoxButton.OK);
                    }
                }
            };
            

            worker.RunWorkerAsync();

            //this.window.showHideProgress(true);

           
        }

        private bool CanExecuteSubmit(object commandParameter)
        {
            return !string.IsNullOrEmpty(this.Password);
        }

        #endregion

        #region Helper Methods
        private void initializeAllCommands()
        {
            this.SubmitCommand = new ActionCommand(this.ExecuteSubmit, this.CanExecuteSubmit);
        }

        private IList<User> validateUser(string username, string password)
        {
            try
            {
                bool status = Session.Cadex_BeginRequest();
                    // CommonUtil.EncryptBase(password) Removed this from the query as the password is not encrypted and stored in database
                IList<User> user = Session.GetCurrentSession().QueryOver<User>()
                        .Where(u => u.UserId == username).And(u => u.Password == password).List();
                this.IsAccessible = user.Count > 0 ? true : false;
                if(!status) Session.Cadex_EndRequest(false);
                return user;
                
            }
            catch (Exception e)
            {
                throw new Exception(e.InnerException.Message);
            }

        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FirstFloor.ModernUI.Windows.Controls;
using CadExchange.Dialogs;
using System.Reflection;
using System.Diagnostics;
using CadExchange.Pages.infoex;

namespace CadExchange.Pages
{
    /// <summary>
    /// Interaction logic for AboutPage.xaml
    /// </summary>
    public partial class AboutPage : AbstractInfoExPage
    {
        public AboutPage()
        {
            InitializeComponent();
            this.DataContext = new AboutPageModel();
        }

        public override string getToolBarResource()
        {
            return null;
        }
    }

    public class AboutPageModel
    {
        private string majorVersion;
        private string minorVersion;
        private string buildVersion;
        private string buildRevision;
        private string mainAppVersion;

        public AboutPageModel()
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(asm.Location);
            majorVersion = fvi.ProductMajorPart.ToString();
            minorVersion = fvi.ProductMinorPart.ToString();
            buildVersion = fvi.ProductBuildPart.ToString();
            buildRevision = "0";
            mainAppVersion = Properties.Settings.Default.AppVersion;
        }

        public string AppVersion
        {
            get
            {
                return mainAppVersion + " " + majorVersion + "." + minorVersion;
            }
        }

        public string AppBuild
        {
            get
            {
                return "B" + buildVersion + " - " + buildRevision;
            }
        }
    }
}

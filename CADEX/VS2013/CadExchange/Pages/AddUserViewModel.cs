﻿using CadExchange.Database;
using CadExchange.Database.Entities;
using FirstFloor.ModernUI.Presentation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using CadExchange.Common;

namespace CadExchange.Pages
{
    public class AddUserViewModel : NotifyPropertyChanged, IDataErrorInfo
    {
        private string userid;
        private string password;
        private string firstName;
        private string lastName;
        private bool isadminyes;
        private bool isadminno=true;
        private int usrStatusActive=1;
        private int usrStausInactive;
        private ICommand submitCommand;
        private User user=null;

        public string UserId
        {
            get { return this.userid; }
            set 
            {
                if(this.userid != value)
                {
                    this.userid = value;
                    OnPropertyChanged("UserId");
                }
            }
        }

        public string Password
        {
            get { return this.password; }
            set
            {
                if (this.password != value)
                {
                    this.password = value;
                    OnPropertyChanged("Password");
                }
            }
        }

        public string FirstName
        {
            get { return this.firstName; }
            set
            {
                if (this.firstName != value)
                {
                    this.firstName = value;
                    OnPropertyChanged("FirstName");
                }
            }
        }

        public string LastName
        {
            get { return this.lastName; }
            set
            {
                if (this.lastName != value)
                {
                    this.lastName = value;
                    OnPropertyChanged("LastName");
                }
            }
        }

        public bool IsAdminYes
        {
            get { return this.isadminyes; }
            set
            {
                if (this.isadminyes != value)
                {
                    this.isadminyes = value;
                    OnPropertyChanged("IsAdminYes");
                }
            }
        }

        public bool IsAdminNo
        {
            get { return this.isadminno; }
            set
            {
                if (this.isadminno != value)
                {
                    this.isadminno = value;
                    OnPropertyChanged("IsAdminNo");
                }
            }
        }

        public int IsStatusActive
        {
            get { return this.usrStatusActive; }
            set
            {
                if (this.usrStatusActive != value)
                {
                    this.usrStatusActive = value;
                    OnPropertyChanged("IsStatusActive");
                }
            }
        }

        public int IsStatusInActive
        {
            get { return this.usrStausInactive; }
            set
            {
                if (this.usrStausInactive != value)
                {
                    this.usrStausInactive = value;
                    OnPropertyChanged("IsStatusInActive");
                }
            }
        }

        public string Error
        {
            get { return null; }
        }

        public string this[string columnName]
        {
            get
            {
                if (columnName == "UserId")
                {
                    return string.IsNullOrEmpty(this.userid) ? "Mandatory" : null;
                }
                if (columnName == "Password")
                {
                    return string.IsNullOrEmpty(this.password) ? "Mandatory" : null;
                }
                if (columnName == "FirstName")
                {
                    return string.IsNullOrEmpty(this.firstName) ? "Mandatory" : null;
                }
                if (columnName == "LastName")
                {
                    return string.IsNullOrEmpty(this.lastName) ? "Mandatory" : null;
                }
                return null;
            }
        }

        public ICommand SubmitCommand
        {
            get
            {
                if (submitCommand == null)
                {
                    submitCommand = new RelayCommand(
                        param => this.SavetoDB(),
                        param => this.CanSubmit()
                    );
                }
                return submitCommand;
            }
        }

        private bool CanSubmit()
        {
            if (this.userid == null || this.userid.Length <= 0 || this.password == null || this.password.Length <= 0 || this.firstName == null || this.firstName.Length <= 0 || this.lastName == null || this.lastName.Length <= 0)
                return false;
            return true;
        }

        private void SavetoDB()
        {
            bool status = Session.Cadex_BeginRequest();

            if(user != null && user.UserId == this.userid)
            {
                var existingUser = Session.getObjectById<User>(Convert.ToString(user.Id));
                existingUser.UserId = this.userid;
                existingUser.Password = this.password;
                existingUser.FirstName = this.firstName;
                existingUser.LastName = this.lastName;
                existingUser.IsAdministrator = this.IsAdminYes ? true : false;
                existingUser.Status = this.IsStatusActive == 1 ? 1 : 0;
                Session.persistObject(existingUser);
            }
            else
            {
                user = new User { UserId = this.userid, Password = this.password, FirstName = this.firstName, LastName = this.lastName, IsAdministrator = this.isadminyes ? true : false, Status = this.IsStatusActive == 1 ? 1 : 0 };
                Session.GetCurrentSession().SaveOrUpdate(user);
            }
                    
            if(!status) Session.Cadex_EndRequest(false);
        }
    }
}

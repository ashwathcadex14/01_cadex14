﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using CadExchange.Database.Entities;

namespace CadExchange.Pages
{
    /// <summary>
    /// Interaction logic for CadexWorkflowView.xaml
    /// </summary>
    public partial class CadexWorkflowView : UserControl
    {
        public CadexWorkflowView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// WorkflowStages
        /// </summary>
        public ObservableCollection<WorkflowStageUI> WorkflowStages
        {
            get
            {
                return (ObservableCollection<WorkflowStageUI>)GetValue(WorkflowStagesProperty);
            }
            set
            {
                SetValue(WorkflowStagesProperty, value);
            }
        }

        public static readonly DependencyProperty WorkflowStagesProperty =
        DependencyProperty.Register(
            "WorkflowStages",
            typeof(ObservableCollection<WorkflowStageUI>),
            typeof(CadexWorkflowView),
            new PropertyMetadata(null));

        public class WorkflowStageUI : WorkflowStage
        {
            private WorkflowStage _stage;

            public WorkflowStageUI(WorkflowStage stage)
            {
                this._stage = stage;
            }

            public string WfText
            {
                get
                {
                    return this._stage.text;
                }
            }

            public string WfStageType
            {
                get
                {

                    return this._stage.state == WorkflowState.Assigned ? "1" : this._stage.state == WorkflowState.Completed ? "3" : "2";
                }
            }

            public bool IsActive
            {
                get
                {
                    return this._stage.state==this._stage.mainWf.State;
                }
            }

            public string UserName
            {
                get
                {
                    return this._stage.responsibleUser.FirstName;
                }
            }

            public string Info
            {
                get
                {
                    return this._stage.detailedInfo;
                }
            }
        }
    }
}

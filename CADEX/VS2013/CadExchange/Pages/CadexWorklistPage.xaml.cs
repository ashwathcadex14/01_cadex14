﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FirstFloor.ModernUI.Windows;
using CadExchange.Utils;
using CadExchange.Database.Entities;
using CadExchange.Database;
using CadExchange.Pages.infoex;
using NavigationEventArgs = FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs;

namespace CadExchange.Pages
{
    /// <summary>
    /// Interaction logic for CadexWorklistPage.xaml
    /// </summary>
    public partial class CadexWorklistPage : AbstractInfoExPage
    {
        private CadexWorklistPageModel vm;
        public StageLink selLink;
        public static int WORKLIST_ASSIGNED_PAGE = 1;
        public static int WORKLIST_REVIEW_PAGE = 2;
        private string userInfo = "";

        public CadexWorklistPage()
        {
            InitializeComponent();
        }

        public override void OnFragmentNavigationInfoEx(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {

            loadPage(CommonUtil.getFragment(e.Fragment));
        }

        public override void OnFragmentNavigatedToInfoEx(NavigationEventArgs e)
        {

            
            /*string id;
            CommonUtil.getPageArguments(CommonUtil.getFragment(e.Source.ToString())).TryGetValue("Id", out id);
            loadPage(id);*/
            
        }
        void loadPage( string id )
        {
        
          
            selLink = null;
            vm = new CadexWorklistPageModel(mainTab, this, id);
            this.DataContext = vm;
            userInfo = id;
        }
        public override string getToolBarResource()
        {
            int pageId = Convert.ToInt32(userInfo);
            if (pageId == WORKLIST_ASSIGNED_PAGE)
                return "assignPageButtons";
            else
                return "reviewPageButtons";
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {

        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
        }

        private void ackButton_Click(object sender, RoutedEventArgs e)
        {
            if (selLink != null && selLink is StageLink)
            {
                vm.ackItem();
            }
            else
                CadExchange.Utils.CommonUtil.ShowModernMessageBox("Please select a Item", "Acknowledge Error", MessageBoxButton.OK);
        }

        private void ModernTab_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            FrameworkElement link = e.OriginalSource as FrameworkElement;
            if (link != null)
            {
                if (link.DataContext is StageLink)
                    selLink = (StageLink)link.DataContext;
                else
                    selLink = null;
            }
                
        }        
    }
}

﻿using CadExchange.Common;
using CadExchange.plmxml;
using CadExchange.Utils;
using CadExchange.Database.Populate;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using CadExchange.Database.Entities;
using CadExchange.Database;
using CadExchange.Tree;
using System.Windows.Media;
using CadExchange.CadexControls;
using NHibernate;
using CadExchange.Pages.infoex;


namespace CadExchange.Pages
{
    public class CadexWorklistPageModel : AbstractInfoExTabPageModel
    {
        private CadexWorklistPage thisView;
        private bool _isVisControlEnable = true;
        private Visibility _isBusyIndicator = Visibility.Hidden;
        private Uri selectedSource;
        private int pageId;
        private ICommand acknowledgeCommand;
        private ICommand toReviewCommand;
        private ICommand approveCommand;
        private ICommand rejectCommand;
        
        private LinkCollection _myItems = new LinkCollection();

        public Visibility AssignButtons
        {
            get { return pageId == CadexWorklistPage.WORKLIST_ASSIGNED_PAGE ? Visibility.Visible : Visibility.Hidden;}
        }

        public Visibility ReviewButtons
        {
            get { return pageId == CadexWorklistPage.WORKLIST_REVIEW_PAGE ? Visibility.Visible : Visibility.Hidden;}
        }

        public Uri SelectedSource
        {
            get { return this.selectedSource; }
            set
            {
                if (this.selectedSource != value)
                {
                    this.selectedSource = value;
                    NotifyPropertyChanged("SelectedSource");
                }
            }
        }

        public LinkCollection MyItems
        {
            get 
            {
                return this._myItems; 
            }
            set
            {
                this._myItems = value;
                NotifyPropertyChanged("MyItems");
            }
        }

        public CadexWorklistPageModel(ModernTab tab, CadexWorklistPage sView, string id)
            : base(tab, true, true, true, true)
        {
            thisView = sView;
            this.pageId = Convert.ToInt32(id);
            listMyItems();
        }

        private object convertType(WorkflowState state)
        {
            return Convert.ChangeType(state, state.GetTypeCode());
        }

        private string getCondition()
        {
            if(this.pageId == CadexWorklistPage.WORKLIST_ASSIGNED_PAGE)
            {
                return "(state=" + convertType(WorkflowState.Assigned) + " OR state=" + convertType(WorkflowState.InWork) + ")";
            }
            else
                return "(state=" + convertType(WorkflowState.Review) + ")";
        }

        private bool verifyPage(WorkflowStage stage)
        {
            return pageId == CadexWorklistPage.WORKLIST_ASSIGNED_PAGE ? (stage.state == WorkflowState.Assigned || stage.state == WorkflowState.InWork) : (stage.state == WorkflowState.Review);
        }

        /// <summary>
        /// Binding for Acknowledge button command
        /// </summary>
        public ICommand AckCommand
        {
            get
            {
                if (acknowledgeCommand == null)
                {
                    acknowledgeCommand = new RelayCommand(
                        param => this.ackItem(),
                        param => this.CanSubmit()
                    );
                }
                return acknowledgeCommand;
            }
        }

        /// <summary>
        /// Binding for Send To Review button command
        /// </summary>
        public ICommand ToReviewCommand
        {
            get
            {
                if (toReviewCommand == null)
                {
                    toReviewCommand = new RelayCommand(
                        param => this.sendToReviewPreAction(),
                        param => this.CanSubmit()
                    );
                }
                return toReviewCommand;
            }
        }

        /// <summary>
        /// Binding for Approve button command
        /// </summary>
        public ICommand ApproveCommand
        {
            get
            {
                if (approveCommand == null)
                {
                    approveCommand = new RelayCommand(
                        param => this.completeTask(),
                        param => this.CanSubmit()
                    );
                }
                return approveCommand;
            }
        }

        /// <summary>
        /// Binding for Reject button command
        /// </summary>
        public ICommand RejectCommand
        {
            get
            {
                if (rejectCommand == null)
                {
                    rejectCommand = new RelayCommand(
                        param => this.rejectTask(),
                        param => this.CanSubmit()
                    );
                }
                return rejectCommand;
            }
        }

        private bool CanSubmit()
        {
            if (this.SelectedSource != null)
                return true;
            else
                return false;
        }

        private void listMyItems()
        {
            BackgroundWorker worker = new BackgroundWorker();

            LinkCollection tempItems = new LinkCollection();
           
            worker.DoWork += (o, ea) =>
            {
                try
                {
                    bool status = Session.Cadex_BeginRequest();
                    
                    string userId = CadexSession.getSessionUser();
                    User user = Session.getUserByUserId(userId);
                    string condition = getCondition();
                    IQuery query = Session.GetCurrentSession().CreateQuery("FROM WorkflowStage WHERE responsibleUser_id=" + user.Id + " AND " + condition);
                    IList<WorkflowStage> tempStages = query.List<WorkflowStage>();

                    Application.Current.Dispatcher.Invoke((Action)(delegate
                    {
                        _myItems.Clear();
                        foreach (WorkflowStage stage in tempStages)
                        {
                            if (verifyPage(stage) && stage.Id == stage.mainWf.CurrentStage.Id)
                            {
                                StageLink lnk = new StageLink();
                                lnk.StageId = stage.Id;
                                lnk.DisplayName = stage.mainWf.wfItem.itemId.Replace('|','/');
                                lnk.ImageSource = thisView.Resources["itemImage"] as ImageSource;
                                lnk.Source = new Uri("/Pages/ViewObjectPage.xaml#Id#" + Session.getStructureFromItem(stage.mainWf.wfItem, true).Id + "#useLock#" + (pageId == CadexWorklistPage.WORKLIST_ASSIGNED_PAGE ? "true" : "false") + getPageInput() , UriKind.Relative);
                                _myItems.Add(lnk);
                            }
                        }
                    }));

                    if(!status) Session.Cadex_EndRequest(false);
                }
                catch (Exception e)
                {
                    ea.Cancel = true;Session.Cadex_EndRequest(true);
                    //ea.Result = e.Message;
                    //CommonUtil.ShowModernMessageBox(e.Message, "Error loading Packages", MessageBoxButton.OK);
                }

            };
            worker.RunWorkerCompleted += (o, ea) =>
            {


                if(ea.Cancelled)
                    CommonUtil.ShowModernMessageBox("Error loading the assigned Items", "Error", MessageBoxButton.OK);

                //We are not busy
                this.BusyIndicator = Visibility.Hidden;

                //Enable the Ui
                this.IsVisControlEnabled = true;
                
            };
            //Make Ui not reachable
            this.IsVisControlEnabled = false;
            //Show we are busy
            this.BusyIndicator = Visibility.Visible;
            worker.RunWorkerAsync();
        }

        public void ackItem()
        {
            BackgroundWorker worker = new BackgroundWorker();

            WorkflowStage stage;

            worker.DoWork += (o, ea) =>
            {
                try
                {
                    bool status = Session.Cadex_BeginRequest();
                    
                    IQuery query = Session.GetCurrentSession().CreateQuery("FROM WorkflowStage WHERE Id=" + this.thisView.selLink.StageId );
                    stage = query.UniqueResult<WorkflowStage>();
                    if (stage != null && stage.mainWf.State == WorkflowState.Assigned)
                    {
                        Workflow mainWf = stage.mainWf;
                        mainWf.acknowledgeTask();
                    }
                    else
                        throw new Exception("Unable to acknowledge the part. The part is already acknowledged.");

                    if(!status) Session.Cadex_EndRequest(false);

                }
                catch (Exception e)
                {
                    ea.Cancel = true;Session.Cadex_EndRequest(true);
                }

            };
            worker.RunWorkerCompleted += (o, ea) =>
            {


                if(ea.Cancelled)
                    CommonUtil.ShowModernMessageBox("Unable to acknowledge the part. The part is already acknowledged." + ea.Error, "Acknowledge Error", MessageBoxButton.OK);

                //We are not busy
                this.BusyIndicator = Visibility.Hidden;

                //Enable the Ui
                this.IsVisControlEnabled = true;

            };
            //Make Ui not reachable
            this.IsVisControlEnabled = false;
            //Show we are busy
            this.BusyIndicator = Visibility.Visible;
            worker.RunWorkerAsync();
        }

        public void rejectTask()
        {
            BackgroundWorker worker = new BackgroundWorker();

            WorkflowStage stage;

            worker.DoWork += (o, ea) =>
            {
                try
                {
                    bool status = Session.Cadex_BeginRequest();
                    IQuery query = Session.GetCurrentSession().CreateQuery("FROM WorkflowStage WHERE Id=" + this.thisView.selLink.StageId);
                    stage = query.UniqueResult<WorkflowStage>();
                    if (stage != null && stage.mainWf.State == WorkflowState.Review)
                    {
                        Workflow mainWf = stage.mainWf;
                        mainWf.rejectTask();
                    }
                    if(!status) Session.Cadex_EndRequest(false);         


                }
                catch (Exception e)
                {
                    ea.Cancel = true;Session.Cadex_EndRequest(true);
                    //ea.Result = e.Message;
                    //CommonUtil.ShowModernMessageBox(e.Message, "Error loading Packages", MessageBoxButton.OK);
                }

            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                //if(ea.Cancelled)
                //CommonUtil.ShowModernMessageBox(this.searchText, "Error loading Packages", MessageBoxButton.OK);

                //We are not busy
                this.BusyIndicator = Visibility.Hidden;

                //Enable the Ui
                this.IsVisControlEnabled = true;

            };
            //Make Ui not reachable
            this.IsVisControlEnabled = false;
            //Show we are busy
            this.BusyIndicator = Visibility.Visible;
            worker.RunWorkerAsync();
        }

        public void completeTask()
        {
            BackgroundWorker worker = new BackgroundWorker();

            WorkflowStage stage;

            worker.DoWork += (o, ea) =>
            {
                try
                {
                    bool status = Session.Cadex_BeginRequest();
                    IQuery query = Session.GetCurrentSession().CreateQuery("FROM WorkflowStage WHERE Id=" + this.thisView.selLink.StageId);
                    stage = query.UniqueResult<WorkflowStage>();
                    if (stage != null && stage.mainWf.State == WorkflowState.Review)
                    {
                        Workflow mainWf = stage.mainWf;
                        mainWf.completeTask();
                    }
                    if(!status) Session.Cadex_EndRequest(false);       


                }
                catch (Exception e)
                {
                    ea.Cancel = true;Session.Cadex_EndRequest(true);
                    //ea.Result = e.Message;
                    //CommonUtil.ShowModernMessageBox(e.Message, "Error loading Packages", MessageBoxButton.OK);
                }

            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                //if(ea.Cancelled)
                //CommonUtil.ShowModernMessageBox(this.searchText, "Error loading Packages", MessageBoxButton.OK);

                //We are not busy
                this.BusyIndicator = Visibility.Hidden;

                //Enable the Ui
                this.IsVisControlEnabled = true;

            };
            //Make Ui not reachable
            this.IsVisControlEnabled = false;
            //Show we are busy
            this.BusyIndicator = Visibility.Visible;
            worker.RunWorkerAsync();
        }

        public void sendToReview(string userId)
        {
            BackgroundWorker worker = new BackgroundWorker();

            WorkflowStage stage;

            worker.DoWork += (o, ea) =>
            {
                try
                {
                    bool status = Session.Cadex_BeginRequest();
                    IQuery query = Session.GetCurrentSession().CreateQuery("FROM WorkflowStage WHERE Id=" + this.thisView.selLink.StageId);
                    stage = query.UniqueResult<WorkflowStage>();
                    if (stage != null && stage.mainWf.State == WorkflowState.InWork)
                    {
                        Workflow mainWf = stage.mainWf;
                        mainWf.sendToReviewTask(userId);
                    }
                    if(!status) Session.Cadex_EndRequest(false);  

                }
                catch (Exception e)
                {
                    ea.Cancel = true;Session.Cadex_EndRequest(true);
                    //ea.Result = e.Message;
                    //CommonUtil.ShowModernMessageBox(e.Message, "Error loading Packages", MessageBoxButton.OK);
                }

            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                //if(ea.Cancelled)
                //CommonUtil.ShowModernMessageBox(this.searchText, "Error loading Packages", MessageBoxButton.OK);

                //We are not busy
                this.BusyIndicator = Visibility.Hidden;

                //Enable the Ui
                this.IsVisControlEnabled = true;

            };
            //Make Ui not reachable
            this.IsVisControlEnabled = false;
            //Show we are busy
            this.BusyIndicator = Visibility.Visible;
            worker.RunWorkerAsync();
        }

        private void sendToReviewPreAction()
        {
            App.Current.Dispatcher.Invoke((Action)delegate
            {
                var contextMenu = new ContextMenu();
                IList<User> users = Session.getUsers();

                foreach (User u in users)
                {
                    var menu = new UserMenuItem { Header = u.FirstName };
                    menu.UserId = u.UserId;
                    menu.AddHandler(MenuItem.ClickEvent, new RoutedEventHandler(ReviewButton_Click));
                    contextMenu.Items.Add(menu);
                }
                contextMenu.IsOpen = true;
            });
        }

        private void ReviewButton_Click(object sender, RoutedEventArgs e)
        {
            sendToReview(((UserMenuItem)sender).UserId);
        }

        /// <summary>
        /// Binding for busy indicator
        /// </summary>
        public Visibility BusyIndicator
        {
            get { return this._isBusyIndicator; }
            set
            {
                this._isBusyIndicator = value;
                NotifyPropertyChanged("BusyIndicator");
            }
        }

        public class UserMenuItem : MenuItem
        {
            private string userId;

            /// <summary>
            /// Gets or sets the UserId
            /// </summary>
            /// <value>The source.</value>
            public string UserId
            {
                get { return this.userId; }
                set
                {
                    if (this.userId != value)
                    {
                        this.userId = value;
                    }
                }
            }
        }

        /// <summary>
        /// Binding for UserControl Enable/Disable
        /// </summary>
        public bool IsVisControlEnabled
        {
            get { return this._isVisControlEnable; }
            set
            {
                this._isVisControlEnable = value;
                NotifyPropertyChanged("IsVisControlEnabled");
            }
        }

        /// <summary>
        /// Binding for UserControl Enable/Disable
        /// </summary>
        public bool IsRingActive
        {
            get { return !this._isVisControlEnable; }
        }
    }

    public class StageLink : Link
    {
        private int stageId;

        /// <summary>
        /// Gets or sets the StageId
        /// </summary>
        /// <value>The source.</value>
        public int StageId
        {
            get { return this.stageId; }
            set
            {
                if (this.stageId != value)
                {
                    this.stageId = value;
                    OnPropertyChanged("StageId");
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FirstFloor.ModernUI.Windows;
using CadExchange.CadexControls;
using CadExchange.Tree;
using CadExchange.Pages.infoex;
using CadExchange.Utils;
using NavigationEventArgs = FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs;

namespace CadExchange.Pages
{
    
    /// <summary>
    /// Interaction logic for ComparePackage.xaml
    /// </summary>
    public partial class ComparePackage : AbstractInfoExPage
    {
        private ComparePackageModel vm;

        public static readonly DependencyProperty IsManualCompareProperty = DependencyProperty.Register("IsManualCompare",
            typeof (bool), typeof (ComparePackage), new PropertyMetadata(true));

        public ComparePackage()
        {
            InitializeComponent();
        }

        public override string getToolBarResource()
        {
            return "compareButtons";
        }

        public override void OnFragmentNavigatedToInfoEx(NavigationEventArgs e)
        {
            if(IsManualCompare)
                setDefaultModel();
        }

        public bool IsManualCompare
        {
            get { return (bool) GetValue(IsManualCompareProperty); }
            set { SetValue(IsManualCompareProperty, value); }
        }

        private void setDefaultModel()
        {
            if (this.DataContext == null)
            {
                vm = new ComparePackageModel();
                this.DataContext = vm;
            }
            else
            {
                if(this.DataContext is ComparePackageModel)
                {
                    ((ComparePackageModel)this.DataContext).loadPkgs();
                }
            }
        }

        private void leftTree_ScrollChanged(object sender, RoutedEventArgs e)
        {
            scrollTree(rightTree, e);
            //rightTree.LayoutRoot.ScrollToVerticalOffset(((CadexControls.CadexTreeView.CadexTreeScrollEventArgs)e).VerticalOffset);
            //rightTree.LayoutRoot.ScrollToHorizontalOffset(((CadexControls.CadexTreeView.CadexTreeScrollEventArgs)e).HorizontalOffset);
        }

        private void scrollTree(CadexTreeView treeView, RoutedEventArgs e)
        {
            CadexControls.CadexTreeView.CadexTreeScrollEventArgs args = ((CadexControls.CadexTreeView.CadexTreeScrollEventArgs)e);
            treeView.scrollTree(args.HorizontalOffset, args.VerticalOffset);
        }

        private void rightTree_ScrollChanged(object sender, RoutedEventArgs e)
        {
            scrollTree(leftTree, e);
            //leftTree.LayoutRoot.ScrollToVerticalOffset(((CadexControls.CadexTreeView.CadexTreeScrollEventArgs)e).VerticalOffset);
            //leftTree.LayoutRoot.ScrollToHorizontalOffset(((CadexControls.CadexTreeView.CadexTreeScrollEventArgs)e).HorizontalOffset);
        }

        private void rightTree_SelectionChanged(object sender, RoutedEventArgs e)
        {
            DependencyObject d2 = leftTree.mainTree.ItemContainerGenerator.ContainerFromIndex(0);
            PopulateCompareTree.CompareStructureNode rightSelect = (PopulateCompareTree.CompareStructureNode)((CadexTreeView)sender).mainTree.SelectedItem;
            if(rightSelect!=null && rightSelect.SeqInternalNo==1)
                ((TreeListViewItem)d2).IsSelected = true;
            else
                leftTree.mainTree.Select((TreeListViewItem)d2, rightSelect);

            if (rightSelect != null && rightSelect.SideNode != null)
            {
                setRelations(rightSelect);
                setAttributes(rightSelect);
            }
        }

        private void leftTree_SelectionChanged(object sender, RoutedEventArgs e)
        {
            DependencyObject d2 = rightTree.mainTree.ItemContainerGenerator.ContainerFromIndex(0);
            PopulateCompareTree.CompareStructureNode leftSelect = (PopulateCompareTree.CompareStructureNode)((CadexTreeView)sender).mainTree.SelectedItem;
            if (leftSelect != null && leftSelect.SeqInternalNo == 1)
                ((TreeListViewItem)d2).IsSelected = true;
            else
                rightTree.mainTree.Select((TreeListViewItem)d2, leftSelect);



            if (leftSelect != null && leftSelect.SideNode != null)
            {
                setRelations(leftSelect);
                setAttributes(leftSelect);
            }
                
        }

        private void leftTree_TreeExpanded(object sender, RoutedEventArgs e)
        {
            DependencyObject d2 = rightTree.mainTree.ItemContainerGenerator.ContainerFromIndex(0);
            PopulateCompareTree.CompareStructureNode leftSelect = (PopulateCompareTree.CompareStructureNode)((CadexTreeView)sender).mainTree.SelectedItem;
            bool whatToDo = ((TreeListView.CadexTreeExpandEventArgs)e).Expand;
            if (leftSelect != null && leftSelect.SeqInternalNo == 1)
                ((TreeListViewItem)d2).IsExpanded = whatToDo;
            else
                rightTree.mainTree.Expand((TreeListViewItem)d2, leftSelect, whatToDo);
        }

        private void setRelations(PopulateCompareTree.CompareStructureNode selNode)
        {
            if(selNode != null)
                this.relNodeTree.TreeSource = selNode.RelationList;
        }

        private void setAttributes(PopulateCompareTree.CompareStructureNode selNode)
        {
            if (selNode != null)
                this.attrNodeTree.TreeSource = selNode.AttributeList;
        }

        private void rightTree_TreeExpanded(object sender, RoutedEventArgs e)
        {
            DependencyObject d2 = leftTree.mainTree.ItemContainerGenerator.ContainerFromIndex(0);
            PopulateCompareTree.CompareStructureNode rightSelect = (PopulateCompareTree.CompareStructureNode)((CadexTreeView)sender).mainTree.SelectedItem;
            bool whatToDo = ((TreeListView.CadexTreeExpandEventArgs)e).Expand;
            if (rightSelect != null && rightSelect.SeqInternalNo == 1)
                ((TreeListViewItem)d2).IsExpanded = whatToDo;
            else
                leftTree.mainTree.Expand((TreeListViewItem)d2, rightSelect, whatToDo);
        }

        private void leftTree_TreeCollapsed(object sender, RoutedEventArgs e)
        {
            DependencyObject d2 = rightTree.mainTree.ItemContainerGenerator.ContainerFromIndex(0);
            PopulateCompareTree.CompareStructureNode leftSelect = (PopulateCompareTree.CompareStructureNode)((CadexTreeView)sender).mainTree.SelectedItem;
            bool whatToDo = ((TreeListView.CadexTreeExpandEventArgs)e).Expand;
            if (leftSelect != null && leftSelect.SeqInternalNo == 1)
                ((TreeListViewItem)d2).IsExpanded = whatToDo;
            else
                rightTree.mainTree.Expand((TreeListViewItem)d2, leftSelect, whatToDo);
        }

        private void rightTree_TreeCollapsed(object sender, RoutedEventArgs e)
        {
            DependencyObject d2 = leftTree.mainTree.ItemContainerGenerator.ContainerFromIndex(0);
            PopulateCompareTree.CompareStructureNode rightSelect = (PopulateCompareTree.CompareStructureNode)((CadexTreeView)sender).mainTree.SelectedItem;
            bool whatToDo = ((TreeListView.CadexTreeExpandEventArgs)e).Expand;
            if (rightSelect != null && rightSelect.SeqInternalNo == 1)
                ((TreeListViewItem)d2).IsExpanded = whatToDo;
            else
                leftTree.mainTree.Expand((TreeListViewItem)d2, rightSelect, whatToDo);
        }
    }

    
}

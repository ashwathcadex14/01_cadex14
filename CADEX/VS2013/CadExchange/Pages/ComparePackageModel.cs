﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows;
using System.Collections.ObjectModel;
using CadExchange.Tree;
using CompareEngine.Input;
using CadExchange.Pages.compare;
using System.Windows.Controls;
using System.Windows.Input;
using FirstFloor.ModernUI.Presentation;
using CadExchange.Common;
using CadExchange.Pages.infoex;
using CadExchange.Database.Entities;
using CadExchange.Database;
using CadExchange.Database.Populate;
using CadExchange.Utils;
using System.IO.Compression;
using CompareEngine.Compare.Cadex;
using InfoExPLMXMLSDK.com.infoex.plmxml.types;
using InfoExPLMXMLSDK.com.infoex.plmxml.generic;

namespace CadExchange.Pages
{
    public class ComparePackageModel : AbstractInfoExBomPageModel
    {
        private bool _isVisControlEnable = true;
        private Visibility _isBusyIndicator = Visibility.Hidden;
        private ObservableCollection<ICadexTreeNode> _leftTreeNode = new ObservableCollection<ICadexTreeNode>();
        private ObservableCollection<ICadexTreeNode> _rightTreeNode = new ObservableCollection<ICadexTreeNode>();
        private List<ICadexTreeNode> tempLeftItems;
        private List<ICadexTreeNode> tempRightItems;
        private bool _manualCompare = true;
        public CompareType compareType;
        private CompareNode _compareNode = null;
        private bool showPackage;
        private ICommand browseCommand = null;
        private ICommand browseCommand1 = null;
        private ICommand compareCommand = null;
        private ICommand deltaCommand = null;
        private string file1;
        private string file2;
        private string deltaFile;
        ObservableCollection<string> _source = new ObservableCollection<string>();
        string _selectedItem = null;

        public ComparePackageModel()
            : base(false, false)
        {
            tempLeftItems = new List<ICadexTreeNode>();
            tempRightItems = new List<ICadexTreeNode>();
            CompType = CompareType.Package;
            loadPkgs();
        }

        public void loadPkgs()
        {
            try
            {
                bool status = Session.Cadex_BeginRequest();

                string _tempSelItem = this.SelectedItem;

                _source.Clear();

                List<Package> packages = (List<Package>)Session.GetCurrentSession().CreateCriteria(typeof(Package)).List<Package>();

                foreach (Package pkg in packages)
                {
                    _source.Add(pkg.PackageId);
                }

                NotifyPropertyChanged("Source");

                if (_tempSelItem != null && _tempSelItem.Length > 0)
                    SelectedItem = _tempSelItem;

                if (!status) Session.Cadex_EndRequest(false);
            }
            catch (Exception e)
            {
                Session.Cadex_EndRequest(true);
                InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);throw;
            }
            
        }

        /// <summary>
        /// Binding for Browse button command
        /// </summary>
        public ICommand BrowseCommand
        {
            get
            {
                if (browseCommand == null)
                {
                    browseCommand = new RelayCommand(
                        param => this.OpenFileDialog(true),
                        param => true
                    );
                }
                return browseCommand;
            }
        }

        /// <summary>
        /// Binding for Compare button command
        /// </summary>
        public ICommand CompareCommand
        {
            get
            {
                if (compareCommand == null)
                {
                    compareCommand = new RelayCommand(
                        param => this.startCompare(),
                        param => true
                    );
                }
                return compareCommand;
            }
        }

        /// <summary>
        /// Binding for Create Delta button command
        /// </summary>
        public ICommand DeltaCommand
        {
            get
            {
                if (deltaCommand == null)
                {
                    deltaCommand = new RelayCommand(
                        param => this.createDelta(),
                        param => true
                    );
                }
                return deltaCommand;
            }
        }

        public string SelectedItem 
        { 
            get { return _selectedItem; } 
            set 
            { 
                _selectedItem = value;
                NotifyPropertyChanged("SelectedItem");
            }
        }

        public bool IsManualCompare
        {
            get { return _manualCompare; }
            set
            {
                _manualCompare = value;
                NotifyPropertyChanged("IsManualCompare");
                NotifyPropertyChanged("CompareTreeRow");
                NotifyPropertyChanged("CompareTreeRowSpan");
            }
        }

        public int CompareTreeRow
        {
            get { return IsManualCompare ? 1 : 0; }
        }

        public int CompareTreeRowSpan
        {
            get { return IsManualCompare ? TreeSize : TreeSize+1; }
        }

        public ObservableCollection<string> Source { get { return _source; } }

        /// <summary>
        /// Binding for Browse button command
        /// </summary>
        public ICommand BrowseCommand1
        {
            get
            {
                if (browseCommand1 == null)
                {
                    browseCommand1 = new RelayCommand(
                        param => this.OpenFileDialog(false),
                        param => true
                    );
                }
                return browseCommand1;
            }
        }

        /// <summary>
        /// Open the File dialog to choose the package
        /// </summary>
        private void OpenFileDialog(bool isFile1)
        {
            // Create OpenFileDialog
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension
            dlg.DefaultExt = CadexConstants.CADEX_FILE_EXTENSON_XML;
            dlg.Filter = "zip|*.zip";

            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox
            if (result == true)
            {
                if (isFile1)
                    File1Path = dlg.FileName;
                else
                    File2Path = dlg.FileName;
            }
        }

        public CompareType CompType
        {
            get
            {
                return this.compareType;
            }

            set
            {
                this.compareType = value;
                ShowPackage = this.compareType == CompareType.Package ? true : false;
                NotifyPropertyChanged("CompType");
            }
        }

        public string File1Path
        {
            get
            {
                return this.file1;
            }

            set
            {
                this.file1 = value;
                NotifyPropertyChanged("File1Path");
            }

        }

        public string File2Path
        {
            get
            {
                return this.file2;
            }

            set
            {
                this.file2 = value;
                NotifyPropertyChanged("File2Path");
            }

        }

        public bool ShowPackage
        {
            get
            {
                return this.showPackage;
            }

            set
            {
                this.showPackage = value;
                NotifyPropertyChanged("ShowPackage");
            }

        }

        public string DeltaFile
        {
            get { return deltaFile; }
            set { deltaFile = value; }
        }

        public CompareNode CompareRootNode
        {
            get { return _compareNode; }
            set { _compareNode = value; }
        }

        public ObservableCollection<ICadexTreeNode> LeftTreeStructure
        {
            get
            {
                return this._leftTreeNode;
            }
            set
            {
                this._leftTreeNode = value;
                NotifyPropertyChanged("LeftTreeStructure");
            }
        }

        public ObservableCollection<ICadexTreeNode> RightTreeStructure
        {
            get
            {
                return this._rightTreeNode;
            }
            set
            {
                this._rightTreeNode = value;
                NotifyPropertyChanged("RightTreeStructure");
            }
        }

        public void startCompare()
        {
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += (o, ea) =>
            {
                string file1 = "";
                string file2 = "";
                try
                {
                    if (CompType == CompareType.Package || CompType == CompareType.Delta)
                    {
                        bool status = Session.Cadex_BeginRequest();

                        Package package = Session.GetCurrentSession().CreateCriteria(typeof(Package)).Add(NHibernate.Criterion.Restrictions.Eq("PackageId", SelectedItem)).UniqueResult<Package>();

                        List<PackageStructure> structures = (List<PackageStructure>)package.Structures.ToList<PackageStructure>();

                        PackageStructure rootNode = structures.Find(findStruct => findStruct.itemId == package.PackageId && findStruct.parentId == "");

                        PopulateItemTree itemTree = new PopulateItemTree(rootNode);
                        List<ICadexTreeNode> allNodes = itemTree.buildTreeForItem();

                        string exportTemp = System.IO.Path.GetTempPath() + "/cadexCompare";

                        string oldPlmxml = "";
                        string newPlmxml = "";
                        if (CompType == CompareType.Package)
                        {
                            oldPlmxml = package.getPlmXMLPath();
                            newPlmxml = exportTemp + CadexConstants.CADEX_PATH_SEPARTOR + "file2_export.xml";
                        }
                        else
                        {
                            oldPlmxml = exportTemp + CadexConstants.CADEX_PATH_SEPARTOR + "file2_export.xml";
                            newPlmxml = DeltaFile;
                        }

                        if (Directory.Exists(exportTemp))
                            Directory.Delete(exportTemp, true);
                    
                        Directory.CreateDirectory(exportTemp);

                        PLMXMLDocument pkgDoc = plmxmlUtil.readDocument(package.getPlmXMLPath());

                        PopulatePlmxml plmxmlWriter = new PopulatePlmxml(pkgDoc, (ICadexItemTreeNode)allNodes[0], CompType == CompareType.Package ? newPlmxml : oldPlmxml, exportTemp);
                        plmxmlWriter.startTraversal();

                        file1 = oldPlmxml;
                        file2 = newPlmxml;

                        if (!status) Session.Cadex_EndRequest(false);
                    }
                    else
                    {
                        string exportTemp1 = System.IO.Path.GetTempPath() + "/cadexCompare1";
                        string exportFile1 = exportTemp1 + "/file1";
                        string exportFile2 = exportTemp1 + "/file2";
                        if (Directory.Exists(exportTemp1))
                            Directory.Delete(exportTemp1, true);

                        Directory.CreateDirectory(exportTemp1);
                        ZipFile.ExtractToDirectory(File1Path, exportFile1);
                        ZipFile.ExtractToDirectory(File2Path, exportFile2);
                        string xFile1 = plmxmlUtil.GetPlmxmlFile(exportFile1);
                        string xFile2 = plmxmlUtil.GetPlmxmlFile(exportFile2);
                        file1 = xFile1;
                        file2 = xFile2;
                    }


                    CadexCompareEditorInput input = new CadexCompareEditorInput(file1, file2);
                    CompareRootNode = input.compareNode;
                    PopulateCompareTree compTree = new PopulateCompareTree(input.compareNode);
                    ObservableCollection<ICadexTreeNode> nodes = new ObservableCollection<ICadexTreeNode>();
                    tempLeftItems.Clear();
                    tempLeftItems.AddRange( compTree.buildTreeForItem(CompareEngine.Compare.Cadex.CompareNode.LEFT_SIDE) );
                    tempRightItems.Clear();
                    tempRightItems.AddRange(compTree.buildTreeForItem(CompareEngine.Compare.Cadex.CompareNode.RIGHT_SIDE));
                  }
                catch (Exception e)
                {
                    ea.Cancel = true; Session.Cadex_EndRequest(true);
                }
            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                _leftTreeNode.Clear();
                _rightTreeNode.Clear();
                ObservableCollection<ICadexTreeNode> nodes = new ObservableCollection<ICadexTreeNode>();

                if (tempLeftItems.Count == 1)
                {
                    nodes.Add(tempLeftItems[0]);
                    LeftTreeStructure = nodes;
                }

                ObservableCollection<ICadexTreeNode> nodes1 = new ObservableCollection<ICadexTreeNode>();

                if (tempRightItems.Count == 1)
                {
                    nodes1.Add(tempRightItems[0]);
                    RightTreeStructure = nodes1;
                }

                //if(ea.Cancelled)
                //CommonUtil.ShowModernMessageBox(this.searchText, "Error loading Packages", MessageBoxButton.OK);

                //We are not busy
                this.BusyIndicator = Visibility.Hidden;

                //Enable the Ui
                this.IsVisControlEnabled = true;

            };
            //Make Ui not reachable
            this.IsVisControlEnabled = false;
            //Show we are busy
            this.BusyIndicator = Visibility.Visible;
            worker.RunWorkerAsync();
        }

        /// <summary>
        /// Binding for busy indicator
        /// </summary>
        public Visibility BusyIndicator
        {
            get { return this._isBusyIndicator; }
            set
            {
                this._isBusyIndicator = value;
                NotifyPropertyChanged("BusyIndicator");
            }
        }

        /// <summary>
        /// Binding for UserControl Enable/Disable
        /// </summary>
        public bool IsVisControlEnabled
        {
            get { return this._isVisControlEnable; }
            set
            {
                this._isVisControlEnable = value;
                NotifyPropertyChanged("IsVisControlEnabled");
            }
        }

        /// <summary>
        /// Binding for UserControl Enable/Disable
        /// </summary>
        public bool IsRingActive
        {
            get { return !this._isVisControlEnable; }
        }

        public void createDelta()
        {

        }
    }
}

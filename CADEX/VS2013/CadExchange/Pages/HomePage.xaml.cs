﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CadExchange.Pages.infoex;

namespace CadExchange.Pages
{
    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    public partial class HomePage : AbstractInfoExPage
    {
        NavigationService navService;
        
        public HomePage()
        {
            InitializeComponent();
            navService = NavigationService.GetNavigationService(this);
        }

        public override string getToolBarResource()
        {
            return null;
        }

        public NavigationService NavService
        {
            get
            {
                return navService;
            }
        }
    }
}

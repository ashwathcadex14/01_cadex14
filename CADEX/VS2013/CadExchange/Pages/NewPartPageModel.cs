﻿using CadExchange.Database;
using CadExchange.Database.Entities;
using FirstFloor.ModernUI.Presentation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using CadExchange.Common;
using NHibernate;
using NHibernate.Criterion;
using System.Windows;
using CadExchange.Utils;

namespace CadExchange.Pages
{
    public class NewPartPageModel : NotifyPropertyChanged, IDataErrorInfo
    {
        private string itemid;
        private string objectName;
        private string objectDesc;
        private string revId;
        private bool showOthers = false;
        private ICommand submitCommand;
        private ICommand checkCommand;
        private Visibility _isBusyIndicator = Visibility.Hidden;

        /// <summary>
        /// Binding for busy indicator
        /// </summary>
        public Visibility BusyIndicator
        {
            get { return this._isBusyIndicator; }
            set
            {
                this._isBusyIndicator = value;
                OnPropertyChanged("BusyIndicator");
            }
        }

        public bool ShowOtherColumns
        {
            get { return this.showOthers; }
            set
            {
                this.showOthers = value;
                if (this.showOthers)
                    OnPropertyChanged("ItemId");
                OnPropertyChanged("ShowOtherColumns");
            }
        }

        public string ItemId
        {
            get { return this.itemid; }
            set 
            {
                if (this.itemid != value)
                {
                    this.itemid = value;
                    ShowOtherColumns = false;
                    OnPropertyChanged("ItemId");
                }
            }
        }

        public string RevisionId
        {
            get { return this.revId; }
            set
            {
                if (this.revId != value)
                {
                    this.revId = value;
                    OnPropertyChanged("RevisionId");
                }
            }
        }

        public string Name
        {
            get { return this.objectName; }
            set
            {
                if (this.objectName != value)
                {
                    this.objectName = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        public string Description
        {
            get { return this.objectDesc; }
            set
            {
                if (this.objectDesc != value)
                {
                    this.objectDesc = value;
                    OnPropertyChanged("Description");
                }
            }
        }

        public string Error
        {
            get { return null; }
        }

        public string this[string columnName]
        {
            get
            {
                if (columnName == "ItemId")
                {
                    return string.IsNullOrEmpty(this.itemid) ? "Mandatory" : !ShowOtherColumns ? "Item Id not Available" : null;
                }
                if (columnName == "RevisionId")
                {
                    return string.IsNullOrEmpty(this.revId) ? "Mandatory" : null;
                }
                if (columnName == "Name")
                {
                    return string.IsNullOrEmpty(this.objectName) ? "Mandatory" : null;
                }
                //if (columnName == "Description")
                //{
                //    return string.IsNullOrEmpty(this.objectDesc) ? "Mandatory" : null;
                //}
                return null;
            }
        }

        public ICommand SubmitCommand
        {
            get
            {
                if (submitCommand == null)
                {
                    submitCommand = new RelayCommand(
                        param => this.createNewItem(),
                        param => this.CanSubmit()
                    );
                }
                return submitCommand;
            }
        }

        public ICommand CheckCommand
        {
            get
            {
                if (checkCommand == null)
                {
                    checkCommand = new RelayCommand(
                        param => this.checkId(),
                        param => this.CanCheck()
                    );
                }
                return checkCommand;
            }
        }

        private bool CanSubmit()
        {
            if (this.itemid == null || this.itemid.Length <= 0 || this.revId == null || this.revId.Length <= 0 || this.objectName == null || this.objectName.Length <= 0 || this.objectDesc == null || this.objectDesc.Length <= 0)
                return false;
            return true;
        }

        private bool CanCheck()
        {
            if (this.itemid == null || this.itemid.Length <= 0 )
                return false;
            return true;
        }

        public void createNewItem()
        {
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += (o, ea) =>
            {
                try
                {
                    bool status = Session.Cadex_BeginRequest();
                    CadexSupplierItem newItem = new CadexSupplierItem(this.itemid);
                    CadexSupplierItemRevision newRev = new CadexSupplierItemRevision(this.revId, newItem);
                    newRev.isLatest = true;
                    newRev.addAttributeToObject("ItemRevision", "object_name", this.objectName);
                    newRev.addAttributeToObject("ItemRevision", "object_desc", this.objectDesc);
                    newRev.addAttributeToObject("ItemRevision", "object_string", newRev.ParentItem.ItemId + "/" + newRev.RevId + ";" + this.objectName);
                    Session.persistObject(newItem);
                    PackageLineItem newLine = new PackageLineItem(newItem,newRev);
                    Session.persistObject(newLine);
                    PackageStructure newStruct = new PackageStructure();
                    newStruct.itemId = newItem.ItemId + "|" + newRev.RevId;
                    newStruct.parentId = "";
                    newStruct.childPlmxmlId = Convert.ToString( newRev.Id );
                    newStruct.childId = "occ" + "_" + DateTime.Now.ToString("ddMMyyyy_HHmmssfff");
                    Session.persistObject(newStruct);
                    if(!status) Session.Cadex_EndRequest(false);
                }
                catch (Exception e)
                {
                    ea.Cancel = true;Session.Cadex_EndRequest(true);
                    //ea.Result = e.Message;
                    //CommonUtil.ShowModernMessageBox(e.Message, "Error loading Packages", MessageBoxButton.OK);
                }

            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                if(ea.Cancelled)
                    CommonUtil.ShowModernMessageBox("", "Error New Item", MessageBoxButton.OK);
                else
                    CommonUtil.ShowModernMessageBox("Part Created Successfully !!", "New Part", MessageBoxButton.OK);

                ItemId = "";

                //We are not busy
                this.BusyIndicator = Visibility.Hidden;

            };

            //Show we are busy
            this.BusyIndicator = Visibility.Visible;
            worker.RunWorkerAsync();
        }

        public void checkId()
        {
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += (o, ea) =>
            {
                try
                {
                    bool status = Session.Cadex_BeginRequest();

                    List<PackageLineItem> lineItems = (List<PackageLineItem>)Session.GetCurrentSession().CreateCriteria<PackageLineItem>().Add(NHibernate.Criterion.Expression.Eq("itemId", this.itemid)).List<PackageLineItem>();

                    if (lineItems.Count == 0)
                    {
                        ShowOtherColumns = true;

                    }
                    else
                        ItemId = "";

                   if(!status) Session.Cadex_EndRequest(false);
                         
                }
                catch (Exception e)
                {
                    ea.Cancel = true;Session.Cadex_EndRequest(true);
                    //ea.Result = e.Message;
                    //CommonUtil.ShowModernMessageBox(e.Message, "Error loading Packages", MessageBoxButton.OK);
                }

            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                if (ea.Cancelled)
                    CommonUtil.ShowModernMessageBox("", "Error New Item", MessageBoxButton.OK);

                //We are not busy
                this.BusyIndicator = Visibility.Hidden;

            };

            //Show we are busy
            this.BusyIndicator = Visibility.Visible;
            worker.RunWorkerAsync();
        }
    }
}

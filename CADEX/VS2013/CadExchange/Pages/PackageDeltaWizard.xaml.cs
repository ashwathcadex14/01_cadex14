﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CadExchange.Pages.delta;
using CadExchange.Pages.infoex;
using CadExchange.Utils;
using FirstFloor.ModernUI.Windows.Controls;
using FragmentNavigationEventArgs = FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs;
using NavigationEventArgs = FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs;

namespace CadExchange.Pages
{
    /// <summary>
    /// Interaction logic for PackageDeltaWizard.xaml
    /// </summary>
    public partial class PackageDeltaWizard : AbstractInfoExPage
    {
        private ImportDeltaModel deltaModel = null;
        public PackageDeltaWizard()
        {
            InitializeComponent();
            deltaModel = new ImportDeltaModel();
            deltaModel.BusyIndicator = Visibility.Hidden;
            this.DataContext = deltaModel;
        }

        public override void OnFragmentNavigationInfoEx(FragmentNavigationEventArgs e)
        {
            
        }

        public override void OnFragmentNavigatedToInfoEx(NavigationEventArgs e)
        {
            
        }

        private void ModernWizard_OnWizardPageChanged(object sender, RoutedEventArgs e)
        {
            WizardPageChangedEventArgs eventArgs = (WizardPageChangedEventArgs)e;
            if (eventArgs.CurrentPage == 1 && eventArgs.EventTypeOfWizard == WizardEventType.NextButtonClicked)
            {
                bool isValid = deltaModel.ImportPackage();
                if (!isValid)
                {
                    eventArgs.StopNavigation = true;
                    CommonUtil.ShowModernMessageBox(
                        "The package is not doesn't exist or the delta package is invalid.", "Error",
                        MessageBoxButton.OK);
                }
            }
            else if (eventArgs.CurrentPage == 2 && eventArgs.EventTypeOfWizard == WizardEventType.NextButtonClicked)
            {
                deltaModel.generateDelta();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FirstFloor.ModernUI.Windows;
using System.Collections.ObjectModel;
using CadExchange.Tree;
using CadExchange.Common;
using CadExchange.Database.Entities;
using CadExchange.Pages;
using CadExchange.CadexControls;

namespace CadExchange.Pages
{
    /// <summary>
    /// Interaction logic for PackageOverviewPage.xaml
    /// </summary>
    public partial class PackageOverviewPage : UserControl, IContent
    {
        public PackageOverviewPage()
        {
            InitializeComponent();
        }

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            var fragment = e.Fragment;

            ObservableCollection<ICadexTreeNode> nodes = new ObservableCollection<ICadexTreeNode>();

            string pkgId;
            Utils.CommonUtil.getPageArguments(fragment).TryGetValue("Id", out pkgId);

            int id = Convert.ToInt32(pkgId);

            if (CadexSession.getPkg(id).Count == 1)
            {
                nodes.Add(CadexSession.getPkg(id)[0]);
                itemTree.TreeSource = nodes;
            }
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {

        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
        }

        private void itemTree_SelectionChanged(object sender, RoutedEventArgs e)
        {
            if (((CadexTreeView)sender).SelectedItem is PopulatePackageTree.PackageStructureNode)
            {
                PopulatePackageTree.PackageStructureNode selNode = (PopulatePackageTree.PackageStructureNode)((CadexTreeView)sender).SelectedItem;
                selNode.refreshWfInfo();
                ObservableCollection<CadexWorkflowView.WorkflowStageUI> stages = new ObservableCollection<CadexWorkflowView.WorkflowStageUI>();

                if (selNode.stages != null && selNode.stages.Count > 0)
                {
                    foreach (WorkflowStage stage in selNode.stages)
                    {
                        CadexWorkflowView.WorkflowStageUI newStage = new CadexWorkflowView.WorkflowStageUI(stage);
                        stages.Add(newStage);
                    }
                }

                this.wfView.WorkflowStages = stages;
            }
            else
                this.wfView.WorkflowStages = new ObservableCollection<CadexWorkflowView.WorkflowStageUI>();
        }
    }
}

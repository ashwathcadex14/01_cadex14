﻿using CadExchange.Common;
using CadExchange.plmxml;
using CadExchange.Utils;
using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using CadExchange.CadexControls;
using FirstFloor.ModernUI.Windows.Controls;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Presentation;
using CadExchange.Pages.infoex;
using NavigationEventArgs = FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs;

namespace CadExchange.Pages
{
    /// <summary>
    /// Interaction logic for PackageView.xaml
    /// </summary>
    public partial class PackageView : AbstractInfoExPage
    {
        private PackageViewModel vm;
        /// <summary>
        /// Init import package page
        /// </summary>
        public PackageView()
        {
            InitializeComponent();
        }

        public override void OnFragmentNavigationInfoEx(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            if(this.DataContext == null)
            {
                string pageMode;
                CommonUtil.getPageArguments(e.Fragment).TryGetValue("mode", out pageMode);
                vm = new PackageViewModel(packageTab, this, pageMode);
                this.DataContext = vm;
            }
        }

        public override void OnFragmentNavigatedToInfoEx(NavigationEventArgs e)
        {
            
        }

        public override string getToolBarResource()
        {
            return "toolBarButtons";
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
        }
    }
}

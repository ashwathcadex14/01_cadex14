﻿using CadExchange.Common;
using CadExchange.plmxml;
using CadExchange.Utils;
using CadExchange.Database.Populate;
using FirstFloor.ModernUI.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using CadExchange.Database.Entities;
using CadExchange.Database;
using CadExchange.Tree;
using System.Windows.Media;
using CadExchange.CadexControls;
using CadExchange.Dialogs;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using CadExchange.Pages.infoex;
using InfoExCommon.Utils;
using InfoExPLMXMLSDK.com.infoex.plmxml.types;
using InfoExPLMXMLSDK.com.infoex.plmxml.generic;

namespace CadExchange.Pages
{
    public class PackageViewModel : AbstractInfoExTabPageModel
    {
        private bool IsButtonEnabled = false;
        private System.Windows.Visibility IsPackageDetailsVisibile = System.Windows.Visibility.Hidden;
        private ICommand browseCommand = null;
        private ICommand downloadCommand = null;
        private ICommand importCommand = null;
        private ICommand exportCommand = null;
        private ICommand removeCommand = null;
        private ICommand refreshCommand = null;
        private string _SelectedPath = null;
        private bool _isVisControlEnable = true;
        private Visibility _isBusyIndicator = Visibility.Hidden;
        private LinkCollection _packageCollection;
        private Uri selectedSource;
        private List<Package> packages;
        private PackageView thisView;
        public static int IMPORT_PACKAGE_MODE = 0;
        public static int EXPORT_PACKAGE_MODE = 1;
        private int _pageMode = -1;
        private static string IMPORT_TITLE = "IMPORT PACKAGE";
        private static string IMPORT_INFORMATION = "Please specify the path of the package to import";
        private static string EXPORT_TITLE = "EXPORT PACKAGE";
        private static string EXPORT_INFORMATION = "Please select the package to export";
        

        public PackageViewModel(ModernTab tab, PackageView impView, string mode)
            : base(tab, true, false, false, false)
        {
            _packageCollection = new LinkCollection();
            thisView = impView;
            _pageMode = Convert.ToInt32(mode);
            //if(_pageMode == IMPORT_PACKAGE_MODE)
            //{
            //    impView.importForm.Visibility = Visibility.Visible;
            //    impView.exportForm.Visibility = Visibility.Hidden;
            //}
            //else
            //{
            //    impView.importForm.Visibility = Visibility.Hidden;
            //    impView.exportForm.Visibility = Visibility.Visible;
            //}
                

            loadPackages();
        }

        public string PageTitle
        {
            get 
            { 
                return _pageMode == IMPORT_PACKAGE_MODE ? IMPORT_TITLE : EXPORT_TITLE; 
            }
        }

        public bool ImportModeOn
        {
            get
            {
                return _pageMode == IMPORT_PACKAGE_MODE ? true : false;
            }
        }

        public string PageInformation
        {
            get
            {
                return _pageMode == IMPORT_PACKAGE_MODE ? IMPORT_INFORMATION : EXPORT_INFORMATION;
            }
        }

        public LinkCollection PackageCollection
        { 
            get 
            {
                return _packageCollection; 
            }
        }

        public Uri SelectedSource
        {
            get { return this.selectedSource; }
            set
            {
                if (this.selectedSource != value)
                {
                    this.selectedSource = value;
                    NotifyPropertyChanged("SelectedSource");
                }
            }
        }

        /// <summary>
        /// Binding property to Enable/Disable the Import button
        /// </summary>
        public bool IsEnabled
        {
            get { return this.IsButtonEnabled; }
            set
            {
                if (this.IsButtonEnabled != value)
                {
                    this.IsButtonEnabled = value;
                    NotifyPropertyChanged("IsEnabled");
                }
            }
        }

        /// <summary>
        /// Binding property to hide/unhide the Pakage details section 
        /// </summary>
        public System.Windows.Visibility IsPackageDetailVisible
        {
            get { return this.IsPackageDetailsVisibile; }
            set
            {
                if (this.IsPackageDetailsVisibile != value)
                {
                    this.IsPackageDetailsVisibile = value;
                    NotifyPropertyChanged("IsPackageDetailVisible");
                }
            }
        }

        /// <summary>
        /// Binding for Browse button command
        /// </summary>
        public ICommand BrowseCommand
        {
            get
            {
                if (browseCommand == null)
                {
                    browseCommand = new RelayCommand(
                        param => this.OpenFileDialog(),
                        param => true
                    );
                }
                return browseCommand;
            }
        }

        /// <summary>
        /// Binding for Refresh button command
        /// </summary>
        public ICommand RefreshCommand
        {
            get
            {
                if (refreshCommand == null)
                {
                    refreshCommand = new RelayCommand(
                        param => this.loadPackages(),
                        param => true
                    );
                }
                return refreshCommand;
            }
        }

        /// <summary>
        /// Binding for Remove button command
        /// </summary>
        public ICommand RemoveCommand
        {
            get
            {
                if (removeCommand == null)
                {
                    removeCommand = new RelayCommand(
                        param => this.removePackage(),
                        param => this.CanSubmit()
                    );
                }
                return removeCommand;
            }
        }

        /// <summary>
        /// Binding for Download button command
        /// </summary>
        public ICommand DownloadCommand
        {
            get
            {
                if (downloadCommand == null)
                {
                    downloadCommand = new RelayCommand(
                        param => this.SaveFileDialog(),
                        param => this.CanSubmit()
                    );
                }
                return downloadCommand;
            }
        }

        /// <summary>
        /// Binding for Import button command
        /// </summary>
        public ICommand ImportCommand
        {
            get
            {
                if (importCommand == null)
                {
                    importCommand = new RelayCommand(
                        param => this.ImportPackage(param),
                        param => true
                    );
                }
                return importCommand;
            }
        }

        /// <summary>
        /// Binding for Export button command
        /// </summary>
        public ICommand ExportCommand
        {
            get
            {
                if (exportCommand == null)
                {
                    exportCommand = new RelayCommand(
                        param => this.startExport(param),
                        param => this.CanSubmit()
                    );
                }
                return exportCommand;
            }
        }

        /// <summary>
        /// Binding for the text box to show selected package
        /// </summary>
        public string SelectedPath
        {
            get { return this._SelectedPath; }
            set
            {
                if (this._SelectedPath != value)
                {
                    this._SelectedPath = value;
                    NotifyPropertyChanged("SelectedPath");
                }
            }
        }

        /// <summary>
        /// Binding for busy indicator
        /// </summary>
        public Visibility BusyIndicator
        {
            get { return this._isBusyIndicator; }
            set
            {
                this._isBusyIndicator = value;
                NotifyPropertyChanged("BusyIndicator");
            }
        }

        /// <summary>
        /// Binding for UserControl Enable/Disable
        /// </summary>
        public bool IsVisControlEnabled
        {
            get { return this._isVisControlEnable; }
            set
            {
                this._isVisControlEnable = value;
                NotifyPropertyChanged("IsVisControlEnabled");
            }
        }

        /// <summary>
        /// Open the File dialog to choose the destination
        /// </summary>
        private void SaveFileDialog()
        {
            // Create OpenFileDialog
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();

            // Set filter for file extension and default file extension
            dlg.DefaultExt = CadexConstants.CADEX_FILE_EXTENSON_ZIP;
            dlg.Filter = "Zip|*.zip|All Files|*.*";

            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox
            if (result == true)
            {
                SelectedPath = dlg.FileName;
                startDownload("");
            }
        }

        /// <summary>
        /// Open the File dialog to choose the package
        /// </summary>
        private void OpenFileDialog()
        {
            // Create OpenFileDialog
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension
            dlg.DefaultExt = CadexConstants.CADEX_FILE_EXTENSON_ZIP;
            dlg.Filter = "Zip|*.zip|All Files|*.*";

            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox
            if (result == true)
            {
                SelectedPath = dlg.FileName;
                this.IsEnabled = true;
            }
        }

        /// <summary>
        /// Imports the package
        /// </summary>
        private void ImportPackage(object parameter)
        {
            var wnd = new ModernWindow
            {
                Style = (Style)App.Current.Resources["BlankWindow"],
                Title = "Import Package",
                IsTitleVisible = true,
                Content = new ImportPackageControl(),
                Height = 100,
                Width = 600.159
            };

            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = wnd.Width;
            double windowHeight = wnd.Height;
            wnd.Left = (screenWidth / 2) - (windowWidth / 2);
            wnd.Top = (screenHeight / 2) - (windowHeight / 2);
            wnd.Closed += wnd_Closed;
            BusyIndicator = Visibility.Visible;
            wnd.Show();
        }

        void wnd_Closed(object sender, EventArgs e)
        {
            loadAll();
        }

        public void removePackage()
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += (o, ea) =>
            {
                try
                {
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        MessageBoxResult res = CommonUtil.ShowModernMessageBox("Are you sure ?", "Remove", MessageBoxButton.OKCancel);
                        if (res == MessageBoxResult.OK)
                        {
                            Uri uri = SelectedSource;
                            int hashIndex = uri.OriginalString.IndexOf("#");
                            string packageId = uri.OriginalString.Substring(hashIndex + 1, uri.OriginalString.Length - hashIndex - 1);
                            string pkgId;
                            CommonUtil.getPageArguments(packageId).TryGetValue("Id", out pkgId);

                            bool status = Session.Cadex_BeginRequest();

                            Package remPkg = Session.getObjectById<Package>(pkgId);

                            if (remPkg != null)
                            {
                                string remPkgPath = CommonUtil.GetPLMXMLFilesDir(remPkg.getPackageDir());
                                
                                Session.removeObject(remPkg);
                                if (Directory.Exists(remPkgPath))
                                {
                                    foreach (string file in Directory.GetFiles(remPkgPath))
                                    {
                                        CommonUtil.setReadOnly(file, false);
                                        try
                                        {
                                            CommonUtil.DecryptFile1(file + ".encrypt", file);
                                        }
                                        catch (Exception e)
                                        {
                                           LoggerUtil.fatal( GetType(), "Unable to decrypt before deleting the file " + file, e );
                                        }
                                    }
                                }
                                if (Directory.Exists(remPkg.getPackageDir()))
                                    Directory.Delete(remPkg.getPackageDir(), true);
                            }
                                

                            if (!status) Session.Cadex_EndRequest(false);

                            loadAll();
                        }
                    });
                }
                catch (Exception e)
                {
                    ea.Cancel = true;Session.Cadex_EndRequest(true);
                    //ea.Result = e.Message;
                    //CommonUtil.ShowModernMessageBox(e.Message, "Error loading Packages", MessageBoxButton.OK);
                }

            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                //if(ea.Cancelled)
                //CommonUtil.ShowModernMessageBox("", "Error loading Packages", MessageBoxButton.OK);

                //We are not busy
                this.BusyIndicator = Visibility.Hidden;

                //Enable the Ui
                this.IsVisControlEnabled = true;

                //Arrange the columns
                //ResizeColumWidth(parameter as TreeListView);

                //Make the package details controls visible
                this.IsPackageDetailVisible = Visibility.Visible;
                CommonUtil.ShowModernMessageBox("Package removed successfully.", "", MessageBoxButton.OK);

                
            };
            //Make Ui not reachable
            this.IsVisControlEnabled = false;
            //Show we are busy
            this.BusyIndicator = Visibility.Visible;
            worker.RunWorkerAsync();
        }

        private void loadPackages()
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += (o, ea) =>
            {
                try
                {
                    bool status = Session.Cadex_BeginRequest();

                    loadAll();

                    if(!status) Session.Cadex_EndRequest(false); 
                }
                catch (Exception e)
                {
                    ea.Cancel = true;Session.Cadex_EndRequest(true);
                    //ea.Result = e.Message;
                    //CommonUtil.ShowModernMessageBox(e.Message, "Error loading Packages", MessageBoxButton.OK);
                }

            };
            worker.RunWorkerCompleted += (o, ea) =>
            {

                //if(ea.Cancelled)
                //CommonUtil.ShowModernMessageBox("", "Error loading Packages", MessageBoxButton.OK);

                //We are not busy
                this.BusyIndicator = Visibility.Hidden;

                //Enable the Ui
                this.IsVisControlEnabled = true;

                //Arrange the columns
                //ResizeColumWidth(parameter as TreeListView);

                //Make the package details controls visible
                this.IsPackageDetailVisible = Visibility.Visible;
            };
            //Make Ui not reachable
            this.IsVisControlEnabled = false;
            //Show we are busy
            this.BusyIndicator = Visibility.Visible;
            worker.RunWorkerAsync();
        }

        private void startImport(string xFile, object parameter)
        {
            string pkgId = plmxmlUtil.GetPackageId(xFile);
            if(pkgId == null)
            {
                pkgId = System.IO.Path.GetFileNameWithoutExtension(xFile);
            }
            bool overwrite = true;
            if (Session.IsPackageExists(pkgId))
            {
                if (MessageBoxResult.Cancel == CommonUtil.ShowModernMessageBox("The package with id " + pkgId + " already exists.Do you want to override it?", "Warning", MessageBoxButton.OKCancel))
                {
                    throw new Exception("Operation Cancelled.");
                }
            }

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += (o, ea) =>
            {
                try 
	            {
                    bool status = Session.Cadex_BeginRequest();
		            new PopulatePackage(xFile, overwrite, pkgId);
                    if (!status) Session.Cadex_EndRequest(false);

                    loadAll();
	            }
	            catch (Exception e)
	            {
                    CommonUtil.ShowModernMessageBox("Error occured while importing the package.", "Error Populating Package details " + e, MessageBoxButton.OK);
                    ea.Cancel = true; Session.Cadex_EndRequest(true);
	            }
               
            };
            worker.RunWorkerCompleted += (o, ea) =>
            {

                if(ea.Cancelled)
                    CommonUtil.ShowModernMessageBox("Error occured while importing the package." , "Error Populating Package details", MessageBoxButton.OK);
                //We are not busy
                this.BusyIndicator = Visibility.Hidden;

                //Enable the Ui
                this.IsVisControlEnabled = true;

                //Arrange the columns
                //ResizeColumWidth(parameter as TreeListView);



                //Make the package details controls visible
                this.IsPackageDetailVisible = Visibility.Visible;
                
                
            };
            //Make Ui not reachable
            this.IsVisControlEnabled = false;
            //Show we are busy
            this.BusyIndicator = Visibility.Visible;
            worker.RunWorkerAsync();
        }

        private void startDownload(object parameter)
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += (o, ea) =>
            {
                try
                {
                    string frag = selectedSource.ToString();
                    string fragment = frag.Substring(frag.IndexOf('#') + 1);
                    string pkgId;
                    CommonUtil.getPageArguments(fragment).TryGetValue("Id", out pkgId);

                    bool status = Session.Cadex_BeginRequest();

                    Package package = Session.getObjectById<Package>(pkgId);


                    string rootDirectory = Path.GetDirectoryName(package.getPlmXMLPath());

                    string pathDir = Path.GetFileNameWithoutExtension(package.getPlmXMLPath());
                    
                    string newZip = rootDirectory + CadexConstants.CADEX_PATH_SEPARTOR + pathDir + "_export.zip";

                    File.Copy(newZip, SelectedPath);

                    //CommonUtil.ZipUp(newZip, newFileFolder);

                    if (!status) Session.Cadex_EndRequest(false);
                }
                catch (Exception e)
                {
                    ea.Cancel = true; Session.Cadex_EndRequest(true);
                }

            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                //We are not busy
                this.BusyIndicator = Visibility.Hidden;

                //Enable the Ui
                this.IsVisControlEnabled = true;

                //Make the package details controls visible
                this.IsPackageDetailVisible = Visibility.Visible;


            };
            //Make Ui not reachable
            this.IsVisControlEnabled = false;
            //Show we are busy
            this.BusyIndicator = Visibility.Visible;
            worker.RunWorkerAsync();
        }

        private void startExport(object parameter)
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += (o, ea) =>
            {
                try
                {
                    string frag = selectedSource.ToString();
                    string fragment = frag.Substring(frag.IndexOf('#') + 1);
                    string pkgId;
                    CommonUtil.getPageArguments(fragment).TryGetValue("Id", out pkgId);

                    bool status = Session.Cadex_BeginRequest();

                    Package package = Session.getObjectById<Package>(pkgId);

                    List<PackageStructure> structures = (List<PackageStructure>)package.Structures.ToList<PackageStructure>();

                    PackageStructure rootNode = structures.Find(findStruct => findStruct.itemId == package.PackageId && findStruct.parentId == "");

                    PopulateItemTree itemTree = new PopulateItemTree(rootNode);
                    List<ICadexTreeNode> allNodes = itemTree.buildTreeForItem();

                    string rootDirectory = Path.GetDirectoryName(package.getPlmXMLPath());
                    string exportTemp = rootDirectory + CadexConstants.CADEX_PATH_SEPARTOR + "Export_" + DateTime.Now.ToString("ddMMyyyy_HHmmssfff");
                    string pathDir = Path.GetFileNameWithoutExtension(package.getPlmXMLPath());
                    string newFileFolder = exportTemp + CadexConstants.CADEX_PATH_SEPARTOR + pathDir;
                    string newZip = rootDirectory + CadexConstants.CADEX_PATH_SEPARTOR + pathDir + "_export.zip";
                    string oldPlmxml = exportTemp + CadexConstants.CADEX_PATH_SEPARTOR + Path.GetFileName(package.getPlmXMLPath());
                    string newPlmxml = exportTemp + CadexConstants.CADEX_PATH_SEPARTOR + pathDir + "_export.xml";
                    Directory.CreateDirectory(exportTemp);
                    Directory.CreateDirectory(newFileFolder);

                    File.Copy(package.getPlmXMLPath(), oldPlmxml);


                    string pkgNewDir = rootDirectory + CadexConstants.CADEX_PATH_SEPARTOR + pathDir;

                    if(Directory.Exists(pkgNewDir))
                        CommonUtil.DirectoryCopy(pkgNewDir, newFileFolder, true);

                    PLMXMLDocument pkgDoc = plmxmlUtil.readDocument(package.getPlmXMLPath());

                    PopulatePlmxml plmxmlWriter = new PopulatePlmxml(pkgDoc, (ICadexItemTreeNode)allNodes[0], newPlmxml, newFileFolder);
                    plmxmlWriter.startTraversal();

                    File.Delete(oldPlmxml);
                    File.Move(newPlmxml,oldPlmxml);
                    

                    ZipFile.CreateFromDirectory(exportTemp, newZip);

                    //CommonUtil.ZipUp(newZip, newFileFolder);
                    
                    if (!status) Session.Cadex_EndRequest(false);
                }
                catch (Exception e)
                {
                   ea.Cancel = true;Session.Cadex_EndRequest(true);
                }
                
            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                //We are not busy
                this.BusyIndicator = Visibility.Hidden;

                //Enable the Ui
                this.IsVisControlEnabled = true;

                //Make the package details controls visible
                this.IsPackageDetailVisible = Visibility.Visible;

                CommonUtil.ShowModernMessageBox("Package exported successfully.", "", MessageBoxButton.OK);


            };
            //Make Ui not reachable
            this.IsVisControlEnabled = false;
            //Show we are busy
            this.BusyIndicator = Visibility.Visible;
            worker.RunWorkerAsync();
        }

        private bool CanSubmit()
        {
            if (this.SelectedSource != null )
                return true;
            else
                return false;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void loadAll()
        {
            try
            {
                bool status = Session.Cadex_BeginRequest();

                packages = (List<Package>)Session.GetCurrentSession().CreateCriteria(typeof(Package)).List<Package>();

                CadexSession.clearPkgs();

                for (int u = 0; u < packages.Count; u++)
                {
                    PopulatePackageTree treePop = new PopulatePackageTree(packages[u]);
                    CadexSession.addPkg(packages[u].Id, treePop.buildTreeForPkg());
                }

                Application.Current.Dispatcher.Invoke((Action)(delegate
                {
                    refreshList();
                }));

                if (!status) Session.Cadex_EndRequest(false);
            }
            catch (Exception e)
            {
                Session.Cadex_EndRequest(true);
                InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);throw;
            }
        }

        private void refreshList()
        {
            try
            {
                this._packageCollection.Clear();

                foreach (KeyValuePair<int, List<ICadexTreeNode>> entry in CadexSession.getPkgs())
                {
                    Link lnk = new Link();
                    lnk.DisplayName = Session.getObjectById<Package>(Convert.ToString(entry.Key)).PackageId;
                    lnk.Source = new Uri("/Pages/PackageOverviewPage.xaml#Id#" + entry.Key, UriKind.Relative);
                    lnk.ImageSource = this.thisView.Resources["packageListImage"] as ImageSource;

                    this._packageCollection.Add(lnk);
                    NotifyPropertyChanged("PackageCollection");
                }

                //thisView.DataContext = this;
                this.SelectedSource = null;
                LinkCollection links = thisView.packageTab.Links;
                if (links != null && links.Count > 0)
                {
                    this.SelectedSource = links.ElementAt<Link>(0).Source;
                }
            }
            catch (Exception e)
            {
                InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);throw;
            }
            finally
            {
                BusyIndicator = Visibility.Hidden;
            }
        }
    }
}

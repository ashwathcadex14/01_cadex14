﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CadExchange.Pages.infoex;

namespace CadExchange.Pages
{
    /// <summary>
    /// Interaction logic for SearchObjectPage.xaml
    /// </summary>
    public partial class SearchPage : AbstractInfoExPage
    {
        public SearchPage()
        {
            InitializeComponent();
            this.DataContext = new SearchPageModel(mainTab, this);
        }

        public override void OnFragmentNavigationInfoEx(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            
        }

        public override string getToolBarResource()
        {
            return "searchButtons";
        }

        private void copyButton_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}

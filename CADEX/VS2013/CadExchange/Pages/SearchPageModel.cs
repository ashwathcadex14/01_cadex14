﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using FirstFloor.ModernUI.Presentation;
using CadExchange.Common;
using CadExchange.Database;
using System.Windows;
using System.Windows.Input;
using System.Threading;
using CadExchange.Utils;
using System.Windows.Threading;
using CadExchange.Database.Entities;
using NHibernate;
using System.Windows.Media;
using CadExchange.Pages.infoex;
using FirstFloor.ModernUI.Windows.Controls;

namespace CadExchange.Pages
{
    public class SearchPageModel : AbstractInfoExTabPageModel
    {
        
        private SearchPage thisView;
        private bool _isVisControlEnable = true;
        private Visibility _isBusyIndicator = Visibility.Hidden;
        private ICommand searchCommand = null;
        private ICommand searchMyPartsCommand = null;
        private Uri selectedSource;
        private string searchText = "";
        IList<PackageStructure> items;
        Dictionary<string, bool> itemBook = new Dictionary<string, bool>();
        private LinkCollection _searchResults = new LinkCollection();

        public Uri SelectedSource
        {
            get { return this.selectedSource; }
            set
            {
                if (this.selectedSource != value)
                {
                    this.selectedSource = value;
                    NotifyPropertyChanged("SelectedSource");
                }
            }
        }

        public LinkCollection SearchResults
        {
            get 
            {
                return this._searchResults; 
            }
            set
            {
                this._searchResults = value;
                NotifyPropertyChanged("SearchResults");
            }
        }

        public string SearchText
        {
            get { return this.searchText; }
            set
            {
                if (!this.searchText.Equals( value )) 
                {
                    this.searchText = value;
                    NotifyPropertyChanged("SearchText");
                }
            }
        }

        public SearchPageModel(ModernTab tab, SearchPage sView)
            : base(tab, true,true, true, true)
        {
            thisView = sView;
        }

        /// <summary>
        /// Binding for Search button command
        /// </summary>
        public ICommand SearchCommand
        {
            get
            {
                if (searchCommand == null)
                {
                    searchCommand = new RelayCommand(
                        param => this.searchItems(false),
                        param => true
                    );
                }
                return searchCommand;
            }
        }

        /// <summary>
        /// Binding for Search button command
        /// </summary>
        public ICommand SearchMineCommand
        {
            get
            {
                if (searchMyPartsCommand == null)
                {
                    searchMyPartsCommand = new RelayCommand(
                        param => this.searchItems(true),
                        param => true
                    );
                }
                return searchMyPartsCommand;
            }
        }

        private void searchItems(bool searchUserParts)
        {
            BackgroundWorker worker = new BackgroundWorker();
           
            worker.DoWork += (o, ea) =>
            {
                try
                {
                    bool status = Session.Cadex_BeginRequest();

                        items = new List<PackageStructure>();
                        itemBook.Clear();
                        if (searchUserParts)
                        {
                            string queryStr = "From PackageStructure AS line,ModelAttribute AS attr where attr.ParentObj.Id = line.childPlmxmlId AND attr.Element = 'ItemRevision' AND attr.Name = 'owning_user' AND attr.Value ='" + CadexSession.getSessionUser() + "'";

                            
                            IQuery query = Session.GetCurrentSession().CreateQuery(queryStr);
                            List<object> itemObjs = (List<object>)query.List<object>();

                            foreach (object item in itemObjs)
                            {
                                object[] objArray = (object[])item;
                                if (objArray.Length == 2)
                                {
                                    if (objArray[0] is PackageStructure)
                                        items.Add((PackageStructure)objArray[0]);
                                }
                            }
                        }
                        else
                        {
                            string searchId = searchText;
                            string searchOperator = " LIKE ";
                            string queryStr = "";
                            if (searchId.Contains('*'))
                            {
                                searchId.Replace('*', '%');
                            }
                            
                            queryStr = "FROM PackageStructure WHERE itemId" + searchOperator + "'" + searchText + "%'";
                            IQuery query = Session.GetCurrentSession().CreateQuery(queryStr);
                            items = (List<PackageStructure>)query.List<PackageStructure>();
                            foreach (PackageStructure occ in items)
                            {
                                string childId = occ.childPlmxmlId;
                                if (childId.Contains("id"))
                                    addToBook(childId, false);
                                else
                                    addToBook(childId, true);
                            }
                        }
                        if(!status) Session.Cadex_EndRequest(false);
                }
                catch (Exception e)
                {
                    ea.Cancel = true;Session.Cadex_EndRequest(true);
                    //ea.Result = e.Message;
                    //CommonUtil.ShowModernMessageBox(e.Message, "Error loading Packages", MessageBoxButton.OK);
                }

            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                if(ea.Cancelled)
                    CommonUtil.ShowModernMessageBox(this.searchText, "Search Error", MessageBoxButton.OK);
                else
                {
                    this._searchResults.Clear();
                    foreach (PackageStructure lineItem in items)
                    {
                        Link lnk = new Link();
                        bool allowEdit = false;
                        this.itemBook.TryGetValue(lineItem.childPlmxmlId, out allowEdit);
                        lnk.DisplayName = lineItem.itemId.Replace('|','/');
                        lnk.ImageSource = this.thisView.Resources["itemImage"] as ImageSource;
                        lnk.Source = new Uri("/Pages/ViewObjectPage.xaml#Id#" + lineItem.Id + (allowEdit ? "#useLock#true" : "") + getPageInput(), UriKind.Relative);
                        this._searchResults.Add(lnk);
                    }
                }

                //We are not busy
                this.BusyIndicator = Visibility.Hidden;

                //Enable the Ui
                this.IsVisControlEnabled = true;
                
            };
            //Make Ui not reachable
            this.IsVisControlEnabled = false;
            //Show we are busy
            this.BusyIndicator = Visibility.Visible;
            worker.RunWorkerAsync();
        }

        private void addToBook(string childId, bool allowEdit)
        {
            if (!itemBook.ContainsKey(childId))
                this.itemBook.Add(childId, allowEdit);
        }

        /// <summary>
        /// Binding for busy indicator
        /// </summary>
        public Visibility BusyIndicator
        {
            get { return this._isBusyIndicator; }
            set
            {
                this._isBusyIndicator = value;
                NotifyPropertyChanged("BusyIndicator");
            }
        }

        /// <summary>
        /// Binding for UserControl Enable/Disable
        /// </summary>
        public bool IsVisControlEnabled
        {
            get { return this._isVisControlEnable; }
            set
            {
                this._isVisControlEnable = value;
                NotifyPropertyChanged("IsVisControlEnabled");
            }
        }

        /// <summary>
        /// Binding for UserControl Enable/Disable
        /// </summary>
        public bool IsRingActive
        {
            get { return !this._isVisControlEnable; }
        }
    }
}

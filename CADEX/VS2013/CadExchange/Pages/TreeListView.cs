using System;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;
using CadExchange.CadexControls;
using CadExchange.Tree;

namespace CadExchange.Pages
{
    public class TreeListView : TreeView
    {
        public static readonly RoutedEvent TreeExpandedEvent = EventManager.RegisterRoutedEvent("TreeExpanded", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(TreeListView));
        public static readonly RoutedEvent TreeCollapsedEvent = EventManager.RegisterRoutedEvent("TreeCollapsed", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(TreeListView));

        protected override DependencyObject GetContainerForItemOverride()
        {
            return new TreeListViewItem();
        }

        public event RoutedEventHandler TreeExpanded
        {
            add { AddHandler(TreeExpandedEvent, value); }
            remove { RemoveHandler(TreeExpandedEvent, value); }
        }

        public event RoutedEventHandler TreeCollapsed
        {
            add { AddHandler(TreeCollapsedEvent, value); }
            remove { RemoveHandler(TreeCollapsedEvent, value); }
        }

        public TreeListView()
        {
            this.AddHandler(TreeListViewItem.ExpandedEvent, new RoutedEventHandler(OnItemExpanded));
            this.AddHandler(TreeListViewItem.CollapsedEvent, new RoutedEventHandler(OnItemCollapsed));
        }

        private void OnItemExpanded(object sender, RoutedEventArgs e)
        {
            TreeListViewItem expandedItem = (TreeListViewItem)e.OriginalSource;
            expandedItem.IsSelected = true;
            RaiseEvent(new CadexTreeExpandEventArgs(TreeListView.TreeExpandedEvent, expandedItem,true));
        }

        private void OnItemCollapsed(object sender, RoutedEventArgs e)
        {
            TreeListViewItem collapsedItem = (TreeListViewItem)e.OriginalSource;
            collapsedItem.IsSelected = true;
            RaiseEvent(new CadexTreeExpandEventArgs(TreeListView.TreeExpandedEvent, collapsedItem,false));
        }

        public class CadexTreeExpandEventArgs : RoutedEventArgs
        {
            bool expand = false;
            public CadexTreeExpandEventArgs(RoutedEvent routedEvent, object source, bool expandCollapse)
                : base(routedEvent, source)
            {
                expand = expandCollapse;
            }

            public bool Expand
            {
                get
                {
                    return expand;
                }
            }
        }

        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is TreeListViewItem;
        }

        #region Public Properties

        public GridViewColumnCollection Columns
        {
            get
            {
                return (GridViewColumnCollection)GetValue(ColumnsProperty);
            }
            set
            {
                SetValue(ColumnsProperty, value);
            }
        }

        public static readonly DependencyProperty ColumnsProperty =
        DependencyProperty.Register(
            "Columns",
            typeof(GridViewColumnCollection),
            typeof(TreeListView),
            new PropertyMetadata(default(TreeListView), OnColumnsPropertyChanged));


        private static void OnColumnsPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            // AutocompleteTextBox source = d as AutocompleteTextBox;
            // Do something...
        }

        public bool Select(TreeViewItem item, object select) // recursive function to set item selection in treeview
        {
            if (item == null)
                return false;

            TreeViewItem child = item.ItemContainerGenerator.ContainerFromItem(select) as TreeViewItem;
            if (child != null)
            {
                child.IsSelected = true;
                return true;
            }
            foreach (object c in item.Items)
            {
                bool result = Select(item.ItemContainerGenerator.ContainerFromItem(c) as TreeViewItem, select);
                if (result == true)
                    return true;
            }
            return false;
        }


        public bool Expand(TreeViewItem item, object select, bool expand) // recursive function to set item selection in treeview
        {
            if (item == null)
                return false;

            TreeViewItem child = item.ItemContainerGenerator.ContainerFromItem(select) as TreeViewItem;
            if (child != null)
            {
                child.IsExpanded = expand;
                //return true;
            }
            foreach (object c in item.Items)
            {
                bool result = Expand(item.ItemContainerGenerator.ContainerFromItem(c) as TreeViewItem, select,expand);
                if (result == true)
                    return true;
            }
            return false;
        }

        #endregion
    }

    public class TreeListViewItem : TreeViewItem
    {

        /// <summary>
        /// Item's hierarchy in the tree
        /// </summary>
        public int Level
        {
            get
            {
                if (_level == -1)
                {
                    TreeListViewItem parent = ItemsControl.ItemsControlFromItemContainer(this) as TreeListViewItem;
                    _level = (parent != null) ? parent.Level + 1 : 0;
                }
                return _level;
            }
        }

        public new bool IsExpanded
        {
            get
            {
                return ((ICadexTreeNode)this.DataContext).IsExpanded;
            }
            set
            {
                base.IsExpanded = value;
                ((ICadexTreeNode)this.DataContext).IsExpanded = value;
            }
        }

        protected override DependencyObject GetContainerForItemOverride()
        {
            return new TreeListViewItem();
        }

        

        /// <summary>
        /// Recursively search for an item in this subtree.
        /// </summary>
        /// <param name="container">
        /// The parent ItemsControl. This can be a TreeView or a TreeViewItem.
        /// </param>
        /// <param name="item">
        /// The item to search for.
        /// </param>
        /// <returns>
        /// The TreeViewItem that contains the specified item.
        /// </returns>
        private TreeViewItem GetTreeViewItem(ItemsControl container, object item)
        {
            if (container != null)
            {
                if (container.DataContext == item)
                {
                    return container as TreeViewItem;
                }

                // Expand the current container
                if (container is TreeViewItem && !((TreeViewItem)container).IsExpanded)
                {
                    container.SetValue(TreeViewItem.IsExpandedProperty, true);
                }

                // Try to generate the ItemsPresenter and the ItemsPanel.
                // by calling ApplyTemplate.  Note that in the
                // virtualizing case even if the item is marked
                // expanded we still need to do this step in order to
                // regenerate the visuals because they may have been virtualized away.

                container.ApplyTemplate();
                ItemsPresenter itemsPresenter =
                    (ItemsPresenter)container.Template.FindName("ItemsHost", container);
                if (itemsPresenter != null)
                {
                    itemsPresenter.ApplyTemplate();
                }
                else
                {
                    // The Tree template has not named the ItemsPresenter,
                    // so walk the descendents and find the child.
                    itemsPresenter = FindVisualChild<ItemsPresenter>(container);
                    if (itemsPresenter == null)
                    {
                        container.UpdateLayout();

                        itemsPresenter = FindVisualChild<ItemsPresenter>(container);
                    }
                }

                Panel itemsHostPanel = (Panel)VisualTreeHelper.GetChild(itemsPresenter, 0);


                // Ensure that the generator for this panel has been created.
                UIElementCollection children = itemsHostPanel.Children;

                MyVirtualizingStackPanel virtualizingPanel =
                    itemsHostPanel as MyVirtualizingStackPanel;

                for (int i = 0, count = container.Items.Count; i < count; i++)
                {
                    TreeViewItem subContainer;
                    if (virtualizingPanel != null)
                    {
                        // Bring the item into view so
                        // that the container will be generated.
                        virtualizingPanel.BringIntoView(i);

                        subContainer =
                            (TreeViewItem)container.ItemContainerGenerator.
                            ContainerFromIndex(i);
                    }
                    else
                    {
                        subContainer =
                            (TreeViewItem)container.ItemContainerGenerator.
                            ContainerFromIndex(i);

                        // Bring the item into view to maintain the
                        // same behavior as with a virtualizing panel.
                        subContainer.BringIntoView();
                    }

                    if (subContainer != null)
                    {
                        // Search the next level for the object.
                        TreeViewItem resultContainer = GetTreeViewItem(subContainer, item);
                        if (resultContainer != null)
                        {
                            return resultContainer;
                        }
                        else
                        {
                            // The object is not under this TreeViewItem
                            // so collapse it.
                            subContainer.IsExpanded = false;
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Search for an element of a certain type in the visual tree.
        /// </summary>
        /// <typeparam name="T">The type of element to find.</typeparam>
        /// <param name="visual">The parent element.</param>
        /// <returns></returns>
        private T FindVisualChild<T>(Visual visual) where T : Visual
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(visual); i++)
            {
                Visual child = (Visual)VisualTreeHelper.GetChild(visual, i);
                if (child != null)
                {
                    T correctlyTyped = child as T;
                    if (correctlyTyped != null)
                    {
                        return correctlyTyped;
                    }

                    T descendent = FindVisualChild<T>(child);
                    if (descendent != null)
                    {
                        return descendent;
                    }
                }
            }

            return null;
        }

        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is TreeListViewItem;
        }

        private int _level = -1;

       
    }

}

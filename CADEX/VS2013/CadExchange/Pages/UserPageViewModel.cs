﻿using CadExchange.Content;
using CadExchange.Database;
using CadExchange.Database.Entities;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Navigation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace CadExchange.Pages
{
    public class UserPageViewModel : NotifyPropertyChanged
    {
        private LinkCollection _UserCollection;
        private Uri selectedSource;
        private static IList<User> users;
        private UserPage thisView;

        public LinkCollection UserCollection
        { get { return _UserCollection; } }

        public Uri SelectedSource
        {
            get { return this.selectedSource; }
            set
            {
                if (this.selectedSource != value)
                {
                    this.selectedSource = value;
                    OnPropertyChanged("SelectedSource");
                }
            }
        }

        public UserPageViewModel(UserPage view)
        {
            _UserCollection = new LinkCollection();
            thisView = view;
            getAllUsers();
        }

        private void getAllUsers()
        {
            bool status = Session.Cadex_BeginRequest();
            users = Session.GetCurrentSession().CreateCriteria(typeof(User)).List<User>();
            for (int u = 0; u < users.Count; u++)
            {
                Link lnk = new Link();
                lnk.DisplayName = users[u].UserId;
                lnk.Source = new Uri("/Content/UserDetails.xaml#"+users[u].UserId, UriKind.Relative);
                lnk.ImageSource = this.thisView.Resources["userImage"] as ImageSource;
                this._UserCollection.Add(lnk);
            }

            if(!status) Session.Cadex_EndRequest(false);
        }
    }
}

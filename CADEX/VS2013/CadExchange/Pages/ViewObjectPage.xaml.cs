﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FirstFloor.ModernUI.Windows;
using CadExchange.Tree;
using System.Collections.ObjectModel;
using System.Windows.Xps.Packaging;
using FirstFloor.ModernUI.Windows.Controls;
using CadExchange.Dialogs;
using CadExchange.Tree.dataset;
using CadExchange.Pages.infoex;
using System.IO;
using CadExchange.Common;
using CadExchange.Utils;
using InfoExPLMXMLSDK.com.infoex.plmxml.types;

namespace CadExchange.Pages
{
    /// <summary>
    /// Interaction logic for ViewObjectPage.xaml
    /// </summary>
    public partial class ViewObjectPage : AbstractInfoExBomPage
    {
        private ViewObjectPageModel dC;
        public ViewObjectPage()
        {
            InitializeComponent();
        }

        public override void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            Dictionary<string, string> pageArgs = Utils.CommonUtil.getPageArguments(e.Fragment);
            string Id ;
            pageArgs.TryGetValue("Id", out Id);
            string useLock;
            pageArgs.TryGetValue("useLock", out useLock);
            string showRel;
            pageArgs.TryGetValue("ShowRelations", out showRel);
            string showProps;
            pageArgs.TryGetValue("ShowProperties", out showProps);
            string showPreview;
            pageArgs.TryGetValue("ShowPreview", out showPreview);
            if( useLock!=null && useLock.Equals("true"))
            {
                this.lockButton.Visibility = Visibility.Visible;
                this.unlockButton.Visibility = Visibility.Visible;
                this.addPartButton.Visibility = Visibility.Visible;
                this.addDocument.Visibility = Visibility.Visible;
                this.remPartButton.Visibility = Visibility.Visible;
            }
            else
            {
                this.lockButton.Visibility = Visibility.Hidden;
                this.unlockButton.Visibility = Visibility.Hidden;
                this.addPartButton.Visibility = Visibility.Hidden;
                this.addDocument.Visibility = Visibility.Hidden;
                this.remPartButton.Visibility = Visibility.Hidden;
            }
            hidePreview();
            dC = new ViewObjectPageModel(this, Id, showRel.Equals("true") ? true : false,
                        showProps.Equals("true") ? true : false, showPreview.Equals("true") ? true : false);
            this.DataContext = dC;
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
        }

        private void itemTree_SelectionChanged(object sender, RoutedEventArgs e)
        {
            ObservableCollection<ICadexTreeNode> datasetNodes = new ObservableCollection<ICadexTreeNode>();

            ICadexItemTreeNode node = ((ICadexItemTreeNode)itemTree.SelectedItem);
           
            if(node!=null)
            {
                foreach (ICadexDatasetNode dsetNode in node.Datasets)
                {
                    datasetNodes.Add(dsetNode);
                }

                //string cadFile = node.GetCadFile;
                //System.Drawing.Image image = System.Drawing.Image.FromFile(cadFile);
                //System.Drawing.Image thumbNail = image.GetThumbnailImage(120, 120, () => false, IntPtr.Zero);
                //thumbNail.Save(System.IO.Path.ChangeExtension(cadFile, "thumb"));
                if (node.ThumbnailImage != null)
                {
                    MemoryStream ms = new MemoryStream();
                    System.Drawing.Bitmap bmp = (System.Drawing.Bitmap)node.ThumbnailImage;
                    bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                    ms.Position = 0;
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    previewImage.PreviewImage = bi;
                }
                else
                    previewImage.PreviewImage = null;

            }

            this.datasetTree.TreeSource = datasetNodes;
        }

        private void hideButton_Click(object sender, RoutedEventArgs e)
        {
            hidePreview();
        }

        private void showButton_Click(object sender, RoutedEventArgs e)
        {
            showPreview();
        }

        private void showPreview()
        {
            //this.documentView.Visibility = Visibility.Visible;
            //Grid.SetRowSpan(this.itemTree, 3);
            //Grid.SetColumnSpan(this.itemTree, 1);
            //Grid.SetColumn(this.datasetTitle, 1);
            //Grid.SetColumn(this.propertiesTitle, 2);
            //Grid.SetColumn(this.datasetTree, 1);
            //Grid.SetColumn(this.propertiesTree, 2);
        }

        private void hidePreview()
        {
            //this.documentView.Visibility = Visibility.Hidden;
            //Grid.SetRowSpan(this.itemTree, 1);
            //Grid.SetColumnSpan(this.itemTree, 2);
            //Grid.SetColumn(this.datasetTitle, 0);
            //Grid.SetColumn(this.propertiesTitle, 1);
            //Grid.SetColumn(this.datasetTree, 0);
            //Grid.SetColumn(this.propertiesTree, 1);
        }

        private void addDocument_Click(object sender, RoutedEventArgs e)
        {
            ICadexTreeNode selNode = ((CadexControls.CadexTreeView)itemTree).SelectedItem;

            if (selNode is ICadexItemTreeNode )
            {
                ICadexItemTreeNode node = ((ICadexItemTreeNode)selNode);

                if(node.ReadOnly)
                {
                    Utils.CommonUtil.ShowModernMessageBox("The object is read only. Documents cannot be added for read only objects.", "Add Error", MessageBoxButton.OK);
                }
                else if (node.Locked && checkLock(node.LockedBy))
                {
                    var wnd = new ModernWindow
                    {
                        Style = (Style)App.Current.Resources["BlankWindow"],
                        Title = "Add Document",
                        IsTitleVisible = true,
                        Content = new AddDatasetControl(node),
                        Height = 350.402,
                        Width = 558.159
                    };

                    double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
                    double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
                    double windowWidth = wnd.Width;
                    double windowHeight = wnd.Height;
                    wnd.Left = (screenWidth / 2) - (windowWidth / 2);
                    wnd.Top = (screenHeight / 2) - (windowHeight / 2);
                    wnd.Closed += wnd_Closed;
                    dC.BusyIndicator = Visibility.Visible;
                    wnd.Show();
                }
                else if(!node.Locked)
                    Utils.CommonUtil.ShowModernMessageBox("The object is not locked and document cannot be added. Please acquire lock and try again.", "Add Error", MessageBoxButton.OK);
                else
                    Utils.CommonUtil.ShowModernMessageBox("The object is locked by "+ node.LockedBy + " and document cannot be added.", "Add Error", MessageBoxButton.OK);
            }
        }

        private void datasetTree_SelectionChanged(object sender, RoutedEventArgs e)
        {
            ICadexTreeNode selNode = ((CadexControls.CadexTreeView)sender).SelectedItem;

            if (selNode is ICadexDatasetFileNode)
            {
                //XpsDocument doc = ((ICadexDatasetFileNode)selNode).XpsDoc;
                //if (doc != null)
                //    this.documentView.Document = doc.GetFixedDocumentSequence();
            }
        }

        private void showJtButtom_Click(object sender, RoutedEventArgs e)
        {
            string noFileMsg = "No data to show JT.";
            ICadexTreeNode selNode = ((CadexControls.CadexTreeView)itemTree).SelectedItem;

            if (selNode != null)
            {
                Console.WriteLine(selNode.ChildrenList);
                //  testing starts here
                if (selNode.ChildrenList.Count != 0)
                {
                    try
                    {
                        createParentJtFile(selNode);
                    }
                    catch (Exception )
                    {
                        Utils.CommonUtil.ShowModernMessageBox("JT view failed.", "Message", MessageBoxButton.OK);
                    }
                }
                else
                {
                    if (selNode is ICadexItemTreeNode)
                    {
                        ICadexItemTreeNode node = (ICadexItemTreeNode)selNode;
                        string renderFile = node.GetRenderFile;
                        if (renderFile != null)
                        {
                            System.Diagnostics.Process.Start(renderFile);
                        }
                        else
                            Utils.CommonUtil.ShowModernMessageBox(noFileMsg, "Message", MessageBoxButton.OK);
                    }
                    else
                        Utils.CommonUtil.ShowModernMessageBox(noFileMsg, "Message", MessageBoxButton.OK);

                }
            }
            else
            {
                Utils.CommonUtil.ShowModernMessageBox("Action needs BOM line to be selected.", "Message", MessageBoxButton.OK);

            }
            // ends here
            ////if (selNode is ICadexItemTreeNode && ((ICadexItemTreeNode)selNode).HasRendering)
            ////{
            ////    ICadexItemTreeNode node = ((ICadexItemTreeNode)selNode);
              
            ////    if (!node.Locked || (node.Locked && checkLock(node.LockedBy)))
            ////    {
            ////        string openPath = ((ICadexItemTreeNode)selNode).GetRenderFile;
            ////        if (openPath.Length > 0 && System.IO.File.Exists(openPath))
            ////            System.Diagnostics.Process.Start(@openPath);
            ////    }
            ////    else
            ////        Utils.CommonUtil.ShowModernMessageBox("The object is locked. It cannot be opened.", "Open Error", MessageBoxButton.OK);
            ////}
            
        }
        private void createParentJtFile( ICadexTreeNode node )
        {
            JTinfo jtInfo = new JTinfo(node,"STRING","JT_PROP_MEASUREMENT_UNITS","Millimeters");
            jtInfo.createJTfileAndView();
            Console.Write(jtInfo);      
        }
       
        private void datasetTree_DoubleClicked(object sender, RoutedEventArgs e)
        {
            try
            {
                ICadexTreeNode selNode = ((CadexControls.CadexTreeView)sender).SelectedItem;
                if (selNode is ICadexDatasetFileNode)
                {
                    ICadexDatasetNode node = ((ICadexDatasetNode)selNode.CadexParent);
                    if (!node.Locked || (node.Locked && checkLock(node.LockedBy)))
                    {
                        //bool isEnc = CommonUtil.isFileEncrypted(((ICadexDatasetFileNode) selNode).ExternalFile);
                        
                        if(CadexSession.EncryptionEnabled)
                            ((ICadexDatasetFileNode)selNode).decryptFile();

                        string openPath = ((ICadexDatasetFileNode)selNode).ExternalFile;
                        if (openPath != null && openPath.Length > 0 && System.IO.File.Exists(openPath))
                        {
                            System.Diagnostics.Process newProcess = System.Diagnostics.Process.Start(@openPath);
                            //newProcess.Exited += NewProcessOnExited;
                            //newProcess.WaitForExit();
                         //   CadexSession.addOpenedFile(((ICadexDatasetFileNode)selNode));
                        }
                    }
                    else
                        Utils.CommonUtil.ShowModernMessageBox("The object is locked. It cannot be opened.", "Open Error", MessageBoxButton.OK);
                }
            }
            catch (Exception exp)
            {
                Utils.CommonUtil.ShowModernMessageBox(exp.Message, "Open Error", MessageBoxButton.OK);
            }
        }

        private void NewProcessOnExited(object sender, EventArgs eventArgs)
        {
            
        }

        private void OpenCadButton_Click(object sender, RoutedEventArgs e)
        {
            ICadexTreeNode selNode = ((CadexControls.CadexTreeView)itemTree).SelectedItem;

            if (selNode is ICadexItemTreeNode && ((ICadexItemTreeNode)selNode).HasCad)
            {
                ICadexItemTreeNode node = ((ICadexItemTreeNode)selNode);
                if (!node.Locked || ( node.Locked && checkLock(node.LockedBy)))
                {
                    string openPath = ((ICadexItemTreeNode)selNode).GetCadFile;
                    if (openPath.Length > 0 && System.IO.File.Exists(openPath))
                        System.Diagnostics.Process.Start(@openPath);
                }
                else
                    Utils.CommonUtil.ShowModernMessageBox("The object is locked. It cannot be opened.", "Open Error", MessageBoxButton.OK);
            }
                
        }

        private void lockButton_Click(object sender, RoutedEventArgs e)
        {
            dC.lockObjects(true);
            itemTree.mainTree.Items.Refresh();
            //DependencyObject d2 = itemTree.mainTree.ItemContainerGenerator.ContainerFromIndex(0);
            //((TreeListViewItem)d2).IsExpanded = true;
            //itemTree.mainTree.Expand((TreeListViewItem)d2, ((TreeListViewItem)d2).DataContext, true);
        }

        private void unlockButton_Click(object sender, RoutedEventArgs e)
        {
            dC.lockObjects(false);
            itemTree.mainTree.Items.Refresh();
            //DependencyObject d2 = itemTree.mainTree.ItemContainerGenerator.ContainerFromIndex(0);
            //((TreeListViewItem)d2).IsExpanded = true;
            //itemTree.mainTree.Expand((TreeListViewItem)d2, ((TreeListViewItem)d2).DataContext, true);
        }

        private bool checkLock(string lockedBy)
        {
            return lockedBy != null && lockedBy.Length > 0 ? Common.CadexSession.getSessionUser().Equals(lockedBy) ? true : false : true;
        }

        private void addPartButton_Click(object sender, RoutedEventArgs e)
        {
            ICadexItemTreeNode selNode = (ICadexItemTreeNode)this.itemTree.SelectedItem;

            if(selNode == null)
            {
                Utils.CommonUtil.ShowModernMessageBox("Please select a part", "Add Part Error", MessageBoxButton.OK);
            }
            else if( selNode.Locked && !selNode.LockedBy.Equals(CadExchange.Common.CadexSession.getSessionUser()))
            {
                Utils.CommonUtil.ShowModernMessageBox("The selected object is not locked by you. Cannot add part.", "Add Part Error", MessageBoxButton.OK);
            }
            else if( !selNode.Locked )
            {
                Utils.CommonUtil.ShowModernMessageBox("The selected object is not locked edit. Cannot add part.", "Add Part Error", MessageBoxButton.OK);
            }
            else if( selNode.ReadOnly )
            {
                Utils.CommonUtil.ShowModernMessageBox("Selected part is read only. Parts can only be added to the modfiable Parts.", "Add Part Error", MessageBoxButton.OK);
            }
            else
            {
                var wnd = new ModernWindow
                {
                    Style = (Style)App.Current.Resources["BlankWindow"],
                    Title = "Add Part",
                    IsTitleVisible = true,
                    Content = new AddPartControl(selNode),
                    MinHeight = 100,
                    MinWidth = 400,
                    Height = 280,
                    Width = 400.826
                };

                double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
                double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
                double windowWidth = wnd.Width;
                double windowHeight = wnd.Height;
                wnd.Left = (screenWidth / 2) - (windowWidth / 2);
                wnd.Top = (screenHeight / 2) - (windowHeight / 2);
                wnd.Closed += wnd_Closed;
                dC.BusyIndicator = Visibility.Visible;
                wnd.Show();
            }
        }

        void wnd_Closed(object sender, EventArgs e)
        {
            dC.loadItemDetails();
        }

        private void remPartButton_Click(object sender, RoutedEventArgs e)
        {
            ICadexItemTreeNode selNode = (ICadexItemTreeNode)this.itemTree.SelectedItem;

            if (selNode == null)
            {
                Utils.CommonUtil.ShowModernMessageBox("Please select a part", "Remove Part Error", MessageBoxButton.OK);
            }
            else if(((ICadexItemTreeNode)selNode.CadexParent)==null)
            {
                Utils.CommonUtil.ShowModernMessageBox("You cannot remove the root item.", "Remove Part Error", MessageBoxButton.OK);
            }
            else if (((ICadexItemTreeNode)selNode.CadexParent).Locked && !((ICadexItemTreeNode)selNode.CadexParent).LockedBy.Equals(CadExchange.Common.CadexSession.getSessionUser()))
            {
                Utils.CommonUtil.ShowModernMessageBox("The parent object is not locked by you. Cannot remove selected part.", "Remove Part Error", MessageBoxButton.OK);
            }
            else if (!((ICadexItemTreeNode)selNode.CadexParent).Locked)
            {
                Utils.CommonUtil.ShowModernMessageBox("The parent object is not locked edit. Cannot remove part.", "Remove Part Error", MessageBoxButton.OK);
            }
            else if (((ICadexItemTreeNode)selNode.CadexParent).ReadOnly)
            {
                Utils.CommonUtil.ShowModernMessageBox("Parent part is read only. Parts can only be removed from modfiable Parts.", "Remove Part Error", MessageBoxButton.OK);
            }
            else
            {
                dC.removePart(((ICadexItemTreeNode)selNode.CadexParent), selNode);
            }
        }
    }
}

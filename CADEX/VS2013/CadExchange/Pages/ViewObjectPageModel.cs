﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using FirstFloor.ModernUI.Presentation;
using CadExchange.Common;
using CadExchange.Database;
using System.Windows;
using System.Windows.Input;
using System.Threading;
using CadExchange.Utils;
using System.Windows.Threading;
using CadExchange.Database.Entities;
using NHibernate;
using System.Windows.Media;
using CadExchange.Tree;
using System.Windows.Controls;
using System.Windows.Data;
using CadExchange.Pages.infoex;

namespace CadExchange.Pages
{
    public class ViewObjectPageModel : AbstractInfoExBomPreviewPageModel
    {
        
        private ViewObjectPage thisView;
        private bool _isVisControlEnable = true;
        private Visibility _isBusyIndicator = Visibility.Hidden;
        private Uri selectedSource;
        private string itemId;
        private ObservableCollection<ICadexTreeNode> _mainTreeNode = new ObservableCollection<ICadexTreeNode>();
        private List<ICadexTreeNode> tempItems;
        private Dictionary<string, string> mainTreeColumns = new Dictionary<string, string>();
        private Dictionary<string, string> dsetTreeColumns = new Dictionary<string, string>();

        public Uri SelectedSource
        {
            get { return this.selectedSource; }
            set
            {
                if (this.selectedSource != value)
                {
                    this.selectedSource = value;
                    NotifyPropertyChanged("SelectedSource");
                }
            }
        }

        public ObservableCollection<ICadexTreeNode> ItemTreeStructure
        {
            get
            {
                return this._mainTreeNode;
            }
            set
            {
                this._mainTreeNode = value;
                NotifyPropertyChanged("ItemTreeStructure");
            }
        }

        public ViewObjectPageModel(ViewObjectPage sView, string itemId, bool showRel, bool showProps, bool showPreview)
            : base(showRel, showProps, showPreview)
        {
            thisView = sView;
            this.itemId = itemId;
            tempItems = new List<ICadexTreeNode>();
            mainTreeColumns.Add("Description", "Description");
            dsetTreeColumns.Add("Type", "Type");
            loadItemDetails();
        }

        public void loadItemDetails()
        {
            BackgroundWorker worker = new BackgroundWorker();
           
            worker.DoWork += (o, ea) =>
            {
                try
                {
                    bool status = Session.Cadex_BeginRequest();
                    PackageStructure itemNode = Session.getObjectById<PackageStructure>(Convert.ToString(itemId));

                    if (itemNode != null)
                    {
                        PopulateItemTree itemPopulate = new PopulateItemTree(itemNode);
                        tempItems.Clear();
                        tempItems.AddRange(itemPopulate.buildTreeForItem());
                    }

                    if(!status) Session.Cadex_EndRequest(false);
                }
                catch (Exception e)
                {
                    ea.Cancel = true;Session.Cadex_EndRequest(true);
					
                    //ea.Result = e.Message;
                    //CommonUtil.ShowModernMessageBox(e.Message, "Error loading Packages", MessageBoxButton.OK);
                }

            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                _mainTreeNode.Clear();
                ObservableCollection<ICadexTreeNode> nodes = new ObservableCollection<ICadexTreeNode>();
                
                if (tempItems.Count == 1)
                {
                    nodes.Add(tempItems[0]);
                    ItemTreeStructure = nodes;
                }
                    
                //if(ea.Cancelled)
                //CommonUtil.ShowModernMessageBox(this.searchText, "Error loading Packages", MessageBoxButton.OK);

                //We are not busy
                this.BusyIndicator = Visibility.Hidden;

                //Enable the Ui
                this.IsVisControlEnabled = true;
                
            };
            //Make Ui not reachable
            this.IsVisControlEnabled = false;
            //Show we are busy
            this.BusyIndicator = Visibility.Visible;
            worker.RunWorkerAsync();
        }

        public void lockObjects(bool lockNode)
        {
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += (o, ea) =>
            {
                try
                {
                    List<ICadexItemTreeNode> checkOutNodes = new List<ICadexItemTreeNode>();

                    bool status = Session.Cadex_BeginRequest();


                    ((ICadexItemTreeNode)this._mainTreeNode[0]).lockItem(lockNode, ref checkOutNodes);

                    foreach (ICadexItemTreeNode node in checkOutNodes)
                    {
                        node.lockDatasets(lockNode);
                    }

                    if(!status) Session.Cadex_EndRequest(false);
                }
                catch (Exception e)
                {
                    ea.Cancel = true;Session.Cadex_EndRequest(true);
                    //ea.Result = e.Message;
                    //CommonUtil.ShowModernMessageBox(e.Message, "Error loading Packages", MessageBoxButton.OK);
                }

            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                if (ea.Cancelled)
                    CommonUtil.ShowModernMessageBox("Unable to " + (lockNode ? "lock" : "unlock") + " the objects.", (lockNode ? "Lock" : "Unlock") + " Error", MessageBoxButton.OK);

                //loadItemDetails();

                //We are not busy
                this.BusyIndicator = Visibility.Hidden;

                //Enable the Ui
                this.IsVisControlEnabled = true;

            };
            //Make Ui not reachable
            this.IsVisControlEnabled = false;
            //Show we are busy
            this.BusyIndicator = Visibility.Visible;
            worker.RunWorkerAsync();
        }

        public void removePart(ICadexItemTreeNode parent, ICadexItemTreeNode child)
        {
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += (o, ea) =>
            {
                try
                {
                    bool status = Session.Cadex_BeginRequest();

                    List<PackageStructure> newStructures = parent.remPart(child.OccNode);

                    foreach (PackageStructure newStruct in newStructures)
                    {
                        if (parent.PkgLineItem.PartOwner == PackageLineItem.OwnerType.OEM && child.PkgLineItem.PartOwner == PackageLineItem.OwnerType.OEM)
                        {
                            newStruct.isRemoved = true;
                            Session.persistObject(newStruct);
                        }
                        else
                            Session.removeObject(newStruct);
                    }

                    if (!status) Session.Cadex_EndRequest(false);

                    Application.Current.Dispatcher.Invoke((Action)(delegate
                    {
                        parent.ChildrenList.Remove(child);
                    }));

                    
                }
                catch (Exception e)
                {
                    ea.Cancel = true; Session.Cadex_EndRequest(true);

                    //ea.Result = e.Message;
                    //CommonUtil.ShowModernMessageBox(e.Message, "Error loading Packages", MessageBoxButton.OK);
                }

            };
            worker.RunWorkerCompleted += (o, ea) =>
            {
                if (ea.Cancelled)
                    CommonUtil.ShowModernMessageBox("Error occurred in removing the part.", "Remove Part", MessageBoxButton.OK);
                else
                    CommonUtil.ShowModernMessageBox("Part removed successfully !!", "Remove Part", MessageBoxButton.OK);


                //We are not busy
                this.BusyIndicator = Visibility.Hidden;

            };

            //Show we are busy
            this.BusyIndicator = Visibility.Visible;
            worker.RunWorkerAsync();
        }

        /// <summary>
        /// Binding for busy indicator
        /// </summary>
        public Visibility BusyIndicator
        {
            get { return this._isBusyIndicator; }
            set
            {
                this._isBusyIndicator = value;
                NotifyPropertyChanged("BusyIndicator");
            }
        }

        /// <summary>
        /// Binding for UserControl Enable/Disable
        /// </summary>
        public bool IsVisControlEnabled
        {
            get { return this._isVisControlEnable; }
            set
            {
                this._isVisControlEnable = value;
                NotifyPropertyChanged("IsVisControlEnabled");
            }
        }

        /// <summary>
        /// Binding for UserControl Enable/Disable
        /// </summary>
        public bool IsRingActive
        {
            get { return !this._isVisControlEnable; }
        }

        /// <summary>
        /// Tree Column Span
        /// </summary>
        public int TreeColumnSpan
        {
            get 
            { 
                return ShowPreview == Visibility.Collapsed ? 3 : 1; 
            }
        }

        /// <summary>
        /// Tree Row Span
        /// </summary>
        public int TreeRowSpan
        {
            get 
            { 
                return ShowRelations == Visibility.Visible ? 1 : 3;
            }
        }

        public override void NotifyPropertyChanged(string property)
        {
            base.NotifyPropertyChanged(property);
            if (property.Equals("ShowPreview") || property.Equals("ShowProperties") || property.Equals("ShowRelations"))
            {
                NotifyPropertyChanged("TreeColumnSpan");
                NotifyPropertyChanged("TreeRowSpan");
            }
        }
    }
}

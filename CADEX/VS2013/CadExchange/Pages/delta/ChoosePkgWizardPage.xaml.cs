﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CadExchange.Pages.infoex;
using FirstFloor.ModernUI.Windows;
using FragmentNavigationEventArgs = FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs;
using NavigationEventArgs = FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs;

namespace CadExchange.Pages.delta
{
    /// <summary>
    /// Interaction logic for ChoosePkgWizardPage.xaml
    /// </summary>
    public partial class ChoosePkgWizardPage : AbstractInfoExPage
    {
        public ChoosePkgWizardPage()
        {
            InitializeComponent();
        }

        public override void OnFragmentNavigatedToInfoEx(NavigationEventArgs e)
        {
            //PackageLoc.Text = "";
        }

        public override string getToolBarResource()
        {
            return null;
        }
    }
}

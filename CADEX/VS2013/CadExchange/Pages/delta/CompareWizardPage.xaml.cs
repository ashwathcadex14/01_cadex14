﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CadExchange.Pages.compare;
using CadExchange.Pages.infoex;
using FirstFloor.ModernUI.Windows;
using FragmentNavigationEventArgs = FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs;
using NavigatingCancelEventArgs = FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs;
using NavigationEventArgs = FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs;

namespace CadExchange.Pages.delta
{
    /// <summary>
    /// Interaction logic for CompareWizardPage.xaml
    /// </summary>
    public partial class CompareWizardPage : UserControl, IContent
    {
        public CompareWizardPage()
        {
            InitializeComponent();
        }

        public void OnFragmentNavigation(FragmentNavigationEventArgs e)
        {
        }

        public void OnNavigatedFrom(NavigationEventArgs e)
        {
            
        }

        public void OnNavigatedTo(NavigationEventArgs e)
        {
            ImportDeltaModel deltaModel = ((ImportDeltaModel)this.DataContext);
            if (deltaModel != null)
            {
                ComparePackageModel pkgModel = new ComparePackageModel();
                pkgModel.IsManualCompare = false;
                pkgModel.DeltaFile = deltaModel.DeltaFile;
                pkgModel.SelectedItem = deltaModel.PackageId;
                pkgModel.CompType = CompareType.Delta;
                deltaModel.CompareModel = pkgModel;
                comparePkgCtrl.DataContext = pkgModel;
                pkgModel.startCompare();
                comparePkgCtrl.OnNavigatedTo(e);
            }
        }

        public void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            
        }
    }
}

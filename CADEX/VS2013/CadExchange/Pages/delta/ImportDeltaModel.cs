﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using CadExchange.Common;
using CadExchange.Database;
using CadExchange.Database.Populate;
using CadExchange.Utils;
using CompareEngine.Compare.Cadex;
using CompareEngine.Delta;
using FirstFloor.ModernUI.Presentation;
using InfoExPLMXMLSDK.com.infoex.plmxml.types;
using InfoExPLMXMLSDK.com.infoex.plmxml.generic;

namespace CadExchange.Pages.delta
{
    public class ImportDeltaModel : NotifyPropertyChanged
    {
        private string _SelectedPath = "";
        private string packageId = "";
        private ICommand browseCommand = null;
        private string deltaFile;
        private CompareNode compareNode = null;
        private Visibility _isBusyIndicator = Visibility.Hidden;
        private PLMXMLDeltaDocument deltaDocument = null;
        private ComparePackageModel compareModel = null;

        public ImportDeltaModel()
        {
            PLMXMLDelta deltaDoc = new PLMXMLDelta();
            deltaDoc.author = "CADEX Delta Import";
            deltaDoc.date = DateTime.Now;
            deltaDoc.language = "en_US";
            deltaDocument = new PLMXMLDeltaDocument(deltaDoc);
        }

        /// <summary>
        /// Binding for the text box to show selected package
        /// </summary>
        public string SelectedPath
        {
            get { return this._SelectedPath; }
            set
            {
                if (this._SelectedPath != value)
                {
                    this._SelectedPath = value;
                    OnPropertyChanged("SelectedPath");
                }
            }
        }

        /// <summary>
        /// Package Id for which delta has to be compared
        /// </summary>
        public string PackageId
        {
            get { return this.packageId; }
            set
            {
                this.packageId = value;
                OnPropertyChanged("PackageId");
            }
        }

        public string DeltaFile
        {
            get { return deltaFile; }
            set { deltaFile = value; }
        }

        public CompareNode CompareRootNode
        {
            get { return compareModel != null ? compareModel.CompareRootNode : null; }
        }

        public ComparePackageModel CompareModel
        {
            get { return compareModel; }
            set { compareModel = value; }
        }

        public PLMXMLDeltaDocument DeltaDocument
        {
            get { return deltaDocument; }
        }

        /// <summary>
        /// Binding for busy indicator
        /// </summary>
        public Visibility BusyIndicator
        {
            get { return this._isBusyIndicator; }
            set
            {
                this._isBusyIndicator = value;
                OnPropertyChanged("BusyIndicator");
            }
        }

        /// <summary>
        /// Binding for Browse button command
        /// </summary>
        public ICommand BrowseCommand
        {
            get
            {
                if (browseCommand == null)
                {
                    browseCommand = new RelayCommand(
                        param => this.OpenFileDialog(),
                        param => true
                    );
                }
                return browseCommand;
            }
        }

        /// <summary>
        /// Open the File dialog to choose the package
        /// </summary>
        private void OpenFileDialog()
        {
            // Create OpenFileDialog
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension
            dlg.DefaultExt = CadexConstants.CADEX_FILE_EXTENSON_ZIP;
            dlg.Filter = "Zip|*.zip|All Files|*.*";

            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox
            if (result == true)
            {
                SelectedPath = dlg.FileName;
            }
        }

        public void generateDelta()
        {
            PlmxmlDeltaGenerator deltaGen = new PlmxmlDeltaGenerator(DeltaDocument.getRoot(), CompareRootNode);
            deltaGen.traverseStructure();
            DeltaDocument.save("C:\\sampleDeltaFile.xml");
        }

        /// <summary>
        /// Imports the package
        /// </summary>
        public bool ImportPackage()
        {
            bool isValidDelta = false;

            try
            {
                // new package has been given for import
                // Extract the archive; uniqueness with timestamp

                string zipName = System.IO.Path.GetFileNameWithoutExtension(SelectedPath);

                string extractDirectory = CadexSession.getSessionDir() + CadexConstants.CADEX_PATH_SEPARTOR + zipName + "_" + "delta" + "_" +
                                          CadexConstants.CADEX_UNDERSCORE_LITERAL + CommonUtil.GetDefaultTimeStamp();


                if (isEncrypted(SelectedPath))
                {
                    using (Ionic.Zip.ZipFile archive = new Ionic.Zip.ZipFile(@SelectedPath))
                    {
                        archive.Password = "INFOEX";
                        archive.Encryption = Ionic.Zip.EncryptionAlgorithm.WinZipAes256; // the default: you might need to select the proper value here
                        archive.StatusMessageTextWriter = Console.Out;

                        archive.ExtractAll(@extractDirectory, Ionic.Zip.ExtractExistingFileAction.Throw);
                    }
                }
                else
                    ZipFile.ExtractToDirectory(SelectedPath, extractDirectory);

                // Get the plmxml file from the directory
                string xFile = plmxmlUtil.GetPlmxmlFile(extractDirectory);
                if (xFile == null || !File.Exists(xFile))
                {
                    CommonUtil.ShowModernMessageBox("The package does not have PLMXML file.", "Error", MessageBoxButton.OK);
                }
                else
                {
                    string pkgId = plmxmlUtil.GetPackageId(xFile);
                    if (Session.IsPackageExists(pkgId))
                    {
                        isValidDelta = true;
                        PackageId = pkgId;
                        deltaFile = xFile;
                    }
                }
            }
            catch (Exception ex)
            {
                isValidDelta = false;
                CommonUtil.ShowModernMessageBox(ex.Message, "Error", MessageBoxButton.OK);
            }

            return isValidDelta;
        }

        private bool isEncrypted(string zipPath)
        {
            using (var zip = Ionic.Zip.ZipFile.Read(zipPath))
            {
                // check all entries: 
                foreach (var e in zip)
                {
                    if (e.UsesEncryption)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}

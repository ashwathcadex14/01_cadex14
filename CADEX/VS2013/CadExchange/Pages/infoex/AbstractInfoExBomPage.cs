﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using FirstFloor.ModernUI.Windows;
using System.Windows;

namespace CadExchange.Pages.infoex
{
    /// <summary>
    /// Abstract Layer for the Frame pages in the Tab Interface
    /// </summary>
    public partial class AbstractInfoExBomPage : UserControl, IContent
    {
        /// <summary>
        /// Show Relations
        /// </summary>
        public Visibility ShowRelations
        {
            set
            {
                if (this.DataContext != null && this.DataContext is IInfoExBomPreviewPageModel)
                {
                    ((IInfoExBomPreviewPageModel)this.DataContext).ShowRelations = value;
                }
            }
        }

        /// <summary>
        /// Show Preview
        /// </summary>
        public Visibility ShowPreview
        {
            set
            {
                if (this.DataContext != null && this.DataContext is IInfoExBomPreviewPageModel)
                {
                    ((IInfoExBomPreviewPageModel)this.DataContext).ShowPreview = value;
                }
            }
        }

        /// <summary>
        /// Show Properties
        /// </summary>
        public Visibility ShowProps
        {
            set
            {
                if (this.DataContext != null && this.DataContext is IInfoExBomPreviewPageModel)
                {
                    ((IInfoExBomPreviewPageModel)this.DataContext).ShowProperties = value;
                }
            }
        }

        public virtual void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        { 
        }

        public virtual void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
        }

        public virtual void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
        }

        public virtual void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
        }
    }
}

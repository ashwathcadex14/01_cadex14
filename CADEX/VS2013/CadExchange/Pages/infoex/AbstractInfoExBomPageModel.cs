﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using FirstFloor.ModernUI.Presentation;
using System.ComponentModel;

namespace CadExchange.Pages.infoex
{
    /// <summary>
    /// Abstract View Model for pages which will implement a BOM View
    /// </summary>
    public abstract class AbstractInfoExBomPageModel : INotifyPropertyChanged, IInfoExBomPageModel
    {
        private ICommand showRelationCommand = null;
        private ICommand showPropertiesCommand = null;
        private Visibility _showRelations = Visibility.Collapsed;
        private Visibility _showProperties = Visibility.Collapsed;
        
        /// <summary>
        /// Property Changed event handler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="showRel">Show Relations</param>
        /// <param name="showProps">Show Properties</param>
        public AbstractInfoExBomPageModel(bool showRel, bool showProps)
        {
            ShowRelations = showRel ? Visibility.Visible : Visibility.Collapsed;
            ShowProperties = showProps ? Visibility.Visible : Visibility.Collapsed;
        }

        /// <summary>
        /// Binding for Show Relations button command
        /// </summary>
        public ICommand ShowRelationsCommand
        {
            get
            {
                if (showRelationCommand == null)
                {
                    showRelationCommand = new RelayCommand(
                        param => this.showRelations(),
                        param => true
                    );
                }
                return showRelationCommand;
            }
        }

        /// <summary>
        /// Binding for Show Properties button command
        /// </summary>
        public ICommand ShowPropertiesCommand
        {
            get
            {
                if (showPropertiesCommand == null)
                {
                    showPropertiesCommand = new RelayCommand(
                        param => this.showProperties(),
                        param => true
                    );
                }
                return showPropertiesCommand;
            }
        }

        /// <summary>
        /// Binding for Show Relations
        /// </summary>
        public virtual Visibility ShowRelations
        {
            get { return this._showRelations; }
            set
            {
                this._showRelations = value;
                NotifyPropertyChanged("ShowRelations");
                NotifyPropertyChanged("TreeSize");
            }
        }

        /// <summary>
        /// Binding for Show Properties
        /// </summary>
        public virtual Visibility ShowProperties
        {
            get { return this._showProperties; }
            set
            {
                this._showProperties = value;
                NotifyPropertyChanged("ShowProperties");
                NotifyPropertyChanged("TreeSize");
            }
        }

        /// <summary>
        /// Main Tree Size
        /// </summary>
        public int TreeSize
        {
            get { return ShowProperties == Visibility.Visible || ShowRelations == Visibility.Visible ? 1 : 2; }
        }

        /// <summary>
        /// Others can override provided the base method is called.
        /// <remarks>This must be enhanced to provide a method hook for user action
        /// rather than allowing them override it directly</remarks>
        /// </summary>
        public virtual void showRelations()
        {
            if (this.ShowRelations == Visibility.Collapsed)
                this.ShowRelations = Visibility.Visible;
            else
                this.ShowRelations = Visibility.Collapsed;

        }

        /// <summary>
        /// Others can override provided the base method is called.
        /// <remarks>This must be enhanced to provide a method hook for user action
        /// rather than allowing them override it directly</remarks>
        /// </summary>
        public virtual void showProperties()
        {
            if (this.ShowProperties == Visibility.Collapsed)
                this.ShowProperties = Visibility.Visible;
            else
                this.ShowProperties = Visibility.Collapsed;
        }

        public virtual void NotifyPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }
}

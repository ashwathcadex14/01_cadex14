﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using FirstFloor.ModernUI.Presentation;

namespace CadExchange.Pages.infoex
{
    /// <summary>
    /// Abstract Layer for the pages which need preview show/hide functionality
    /// </summary>
    public abstract class AbstractInfoExBomPreviewPageModel : AbstractInfoExBomPageModel, IInfoExBomPreviewPageModel
    {
        private ICommand showPreviewCommand = null;
        private Visibility _showPreview = Visibility.Visible;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="showRel">Show Relations</param>
        /// <param name="showProps">Show Properties</param>
        /// <param name="showPreview">Show Preview</param>
        public AbstractInfoExBomPreviewPageModel(bool showRel, bool showProps, bool showPreview) : base(showRel, showProps)
        {
            ShowPreview = showPreview ? Visibility.Visible : Visibility.Collapsed;
        }
        /// <summary>
        /// Binding for Show Relations button command
        /// </summary>
        public ICommand ShowPreviewCommand
        {
            get
            {
                if (showPreviewCommand == null)
                {
                    showPreviewCommand = new RelayCommand(
                        param => this.showPreview(),
                        param => true
                    );
                }
                return showPreviewCommand;
            }
        }

        /// <summary>
        /// Others can override provided the base method is called.
        /// <remarks>This must be enhanced to provide a method hook for user action
        /// rather than allowing them override it directly.</remarks>
        /// </summary>
        public virtual void showPreview()
        {
            if (this.ShowPreview == Visibility.Collapsed)
                this.ShowPreview = Visibility.Visible;
            else
                this.ShowPreview = Visibility.Collapsed;
        }

        /// <summary>
        /// To get the page input which will be passed to the view page
        /// </summary>
        /// <returns></returns>
        public string getPageInput()
        {
            string showRel = ShowRelations == Visibility.Collapsed ? "#ShowRelations#false" : "#ShowRelations#true";
            string showProps = ShowProperties == Visibility.Collapsed ? "#ShowProperties#false" : "#ShowProperties#true";
            string showPreview = ShowPreview == Visibility.Collapsed ? "#ShowPreview#false" : "#ShowPreview#true";
            return showRel + showProps + showPreview;
        }

        /// <summary>
        /// Binding for Show Preview
        /// </summary>
        public virtual Visibility ShowPreview
        {
            get { return this._showPreview; }
            set
            {
                this._showPreview = value;
                NotifyPropertyChanged("ShowPreview");
                NotifyPropertyChanged("TreeSize");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using FirstFloor.ModernUI.Presentation;
using InfoExCommon.Utils;

namespace CadExchange.Pages.infoex
{
    /// <summary>
    /// Abstract Page which wants to supply the navigation toolbar button.
    /// All Main Content pages must inherit this class to supply the buttons to the main
    /// window.
    /// </summary>
    public partial class AbstractInfoExPage : UserControl, IContent
    {
        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            try
            {
                OnFragmentNavigationInfoEx(e);

            }
            catch (Exception er)
            {
                LoggerUtil.fatal(this.GetType(), "Error occurred in calling OnFragmentNavigationInfoEx(). This is likey to be a programming error.", er);
            }
            addToolBarButtons();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        [System.Obsolete("Method will removed in the next versions.")]
        public virtual void OnFragmentNavigationInfoEx(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {

        }

        public virtual void OnFragmentNavigatedToInfoEx(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {

        }

        public virtual void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            //throw new NotImplementedException();
        }

        public virtual void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            try
            {
                OnFragmentNavigatedToInfoEx(e);
            }
            catch (Exception er)
            {
                LoggerUtil.fatal(this.GetType(), "Error occurred in calling OnFragmentNavigatedToInfoEx(). This is likey to be a programming error.", er);
            }
            addToolBarButtons();
        }
        public virtual void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            //throw new NotImplementedException();
        }
        public virtual string getToolBarResource()
        {
            throw new NotImplementedException();
        }
        private void addToolBarButtons()
        {
            try
            {
                string toolBarButtonRes = getToolBarResource();
                if (toolBarButtonRes != null && toolBarButtonRes.Length > 0 && this.FindResource(toolBarButtonRes) != null)
                {
                    ToolBarButtonCollection navButtons = (ToolBarButtonCollection)this.FindResource(toolBarButtonRes);
                    foreach (ToolBarButton button in navButtons)
                    {
                        string cmdName = button.ButtonCommandName;
                        if(cmdName!=null && cmdName.Length>0)
                        {
                            PropertyInfo info = this.DataContext.GetType().GetProperty(cmdName);
                            if(info!=null && info.GetValue(this.DataContext, null) != null)
                            {
                                button.ButtonClickCommand = ((ICommand)info.GetValue(this.DataContext, null));
                            }
                        }
                    }
                    ((ModernWindow)Application.Current.MainWindow).NavigationToolBarButtons = navButtons;
                }
                else
                    throw new Exception("Resource not specified or the resource name " + toolBarButtonRes + " not found in the your xaml.");
            }
            catch (Exception e)
            {
                InfoExCommon.Utils.LoggerUtil.error(this.GetType(), "Problem occurred while setting the toolbar for the page. Resetting the toolbar to blank.", e);
                ((ModernWindow)Application.Current.MainWindow).NavigationToolBarButtons = new ToolBarButtonCollection();
            }

        }
    }
}

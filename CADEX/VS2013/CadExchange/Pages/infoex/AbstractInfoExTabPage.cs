﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Reflection;
using FirstFloor.ModernUI.Windows.Controls;

namespace CadExchange.Pages.infoex
{
    /// <summary>
    /// Abstract Tab Page which can propogate the changes in the tab to its child controls
    /// </summary>
    public partial class AbstractInfoExTabPage : UserControl
    {
        /// <summary>
        /// Get the tab control
        /// </summary>
        /// <returns></returns>
        public ModernTab getTabControl()
        {
            return null;
        }

        /// <summary>
        /// Helper method to propogate the new value to its child tabs
        /// </summary>
        /// <param name="propName"></param>
        /// <param name="propValue"></param>
        public virtual void propogateToTab(string propName, string propValue)
        {
            try
            {
                if (getTabControl() != null)
                {
                    object dContext = getTabControl().FrameContent.DataContext;
                    if (dContext != null)
                    {
                        PropertyInfo propInfo = dContext.GetType().GetProperty(propName);
                        if (propInfo != null)
                        {
                            propInfo.SetValue(dContext, propValue);
                        }
                        else
                            throw new Exception("The specified property " + propName + " does not exist in the DataContext of the child tabs.");
                    }
                    else
                        throw new Exception("The tab content does not have a data context. Unable to propogate.");
                }
                else
                    throw new Exception("The tab control is null.");
            }
            catch(Exception)
            {
                throw;
            }
        }
    }
}

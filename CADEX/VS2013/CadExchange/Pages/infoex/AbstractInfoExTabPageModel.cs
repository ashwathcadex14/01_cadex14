﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows.Controls;
using System.Reflection;

namespace CadExchange.Pages.infoex
{
    /// <summary>
    /// Abstract Layer for the Pages which have Tabs inside them
    /// </summary>
    public abstract class AbstractInfoExTabPageModel : AbstractInfoExBomPreviewPageModel
    {
        private ICommand showTabCommand = null;
        private Visibility _showTab = Visibility.Visible;
        private ModernTab _mainTab = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="showTab">Show Tab</param>
        /// <param name="showRel">Show Relations</param>
        /// <param name="showProps">Show Properties</param>
        /// <param name="showPreview">Show Preview</param>
        public AbstractInfoExTabPageModel(ModernTab mainTab, bool showTab, bool showRel, bool showProps, bool showPreview)
                : base(showRel, showProps, showPreview)
        {
            ShowTab = showTab ? Visibility.Visible : Visibility.Collapsed;
            _mainTab = mainTab;
        }
        /// <summary>
        /// Binding for Show Relations button command
        /// </summary>
        public ICommand ShowTabCommand
        {
            get
            {
                if (showTabCommand == null)
                {
                    showTabCommand = new RelayCommand(
                        param => this.showTab(),
                        param => true
                    );
                }
                return showTabCommand;
            }
        }

        /// <summary>
        /// Binding for Show Tab
        /// </summary>
        public Visibility ShowTab
        {
            get { return this._showTab; }
            set
            {
                this._showTab = value;
                NotifyPropertyChanged("ShowTab");
            }
        }

        private void showTab()
        {
            if (this.ShowTab == Visibility.Collapsed)
                this.ShowTab = Visibility.Visible;
            else
                this.ShowTab = Visibility.Collapsed;
        }

        public override void showPreview()
        {
            base.showPreview();
            propogateToTab<Visibility>("ShowPreview", ShowPreview);
        }

        public override void showProperties()
        {
 	        base.showProperties();
            propogateToTab<Visibility>("ShowProperties", ShowProperties);
        }

        public override void showRelations()
        {
            base.showRelations();
            propogateToTab<Visibility>("ShowRelations", ShowRelations);
        }

        /// <summary>
        /// Get the tab control
        /// </summary>
        /// <returns></returns>
        public ModernTab getTabControl()
        {
            return _mainTab;
        }

        /// <summary>
        /// Helper method to propogate the new value to its child tabs
        /// </summary>
        /// <param name="propName"></param>
        /// <param name="propValue"></param>
        public virtual void propogateToTab<T>(string propName, T propValue)
        {
            try
            {
                if (getTabControl() != null)
                {
                    object dContext = getTabControl().FrameContent.DataContext;
                    if (dContext != null)
                    {
                        PropertyInfo propInfo = dContext.GetType().GetProperty(propName);
                        if (propInfo != null)
                        {
                            propInfo.SetValue(dContext, propValue);
                        }
                        else
                            throw new Exception("The specified property " + propName + " does not exist in the DataContext of the child tabs.");
                    }
                    else
                        throw new Exception("The tab content does not have a data context. Unable to propogate.");
                }
                else
                    throw new Exception("The tab control is null.");

                modifyLinks();
            }
            catch(Exception e)
            {
                InfoExCommon.Utils.LoggerUtil.error(GetType(), "Error occurred while propogating " + propName, e);
            }
        }

        /// <summary>
        /// Gets the tab links from the tab control
        /// </summary>
        /// <returns></returns>
        public LinkCollection getTabLinks()
        {
            if (getTabControl() != null)
            {
                return getTabControl().Links;
            }
            else
                InfoExCommon.Utils.LoggerUtil.error(GetType(), "Error occurred while getting the tab links.", new Exception("Error occurred while getting the tab links."));
           
            return null;
        }

        ///// <summary>
        ///// Link Source for all the links
        ///// </summary>
        ///// <remarks>Cannot be this way. Each link can have a different source!!!</remarks>
        ///// <returns></returns>
        //public abstract string getLinkSource();

        /// <summary>
        /// Modify the links to accomodate to the property
        /// </summary>
        public virtual void modifyLinks()
        {
            foreach(Link link in getTabLinks())
            {
                string linkSource = Utils.CommonUtil.getLink(link.Source.ToString());
                string linkFragment = Utils.CommonUtil.getFragment(link.Source.ToString());
                Dictionary<string, string> pageArgs = Utils.CommonUtil.getPageArguments(linkFragment);
                string Id;
                pageArgs.TryGetValue("Id", out Id);
                string useLock;
                pageArgs.TryGetValue("useLock", out useLock);
                link.Source = new Uri(linkSource + "#Id#" + Id + (useLock != null ? "#useLock#" + useLock : "") + getPageInput(), UriKind.Relative);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;

namespace CadExchange.Pages.infoex
{
    /// <summary>
    /// Interface for View Models which have Relations, Properties in their views
    /// </summary>
    public interface IInfoExBomPageModel
    {
        /// <summary>
        /// Show Relations Property
        /// </summary>
        Visibility ShowRelations { get; set; }

        /// <summary>
        /// Show Properties Property
        /// </summary>
        Visibility ShowProperties { get; set; }
    }
}

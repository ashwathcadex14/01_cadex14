﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CadExchange.Pages.infoex
{
    /// <summary>
    /// Interface for View Models which have Preview in their views
    /// </summary>
    public interface IInfoExBomPreviewPageModel : IInfoExBomPageModel
    {
        /// <summary>
        /// Show Preview Property
        /// </summary>
        Visibility ShowPreview { get; set; }
    }
}

﻿using CadExchange.Common;
using CadExchange.Utils;
using InfoExPLMXMLSDK.com.infoex.plmxml.types;
using InfoExPLMXMLSDK.com.infoex.plmxml.generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadExchange.plmxml
{
    public abstract class AbstractplmxmlParser : IPlmxmlParser
    {
        protected PLMXMLDocument document = null;
        protected DocumentHeader docHeader = null;
        protected string RootObjectId = null;
        protected Occurrence rootOccurence = null;

        /// <summary>
        /// Default Constructor
        /// </summary>
        public AbstractplmxmlParser()
        {
        }

        /// <summary>
        /// Constructor with file uri to parse the given input PLMXML
        /// </summary>
        /// <param name="uri"></param>
        public AbstractplmxmlParser(string uri)
        {
            document = plmxmlUtil.readDocument(uri);
            initialize();
        }

        /// <summary>
        /// Constructor with document to load the given input PLMXML
        /// </summary>
        /// <param name="document"></param>
        public AbstractplmxmlParser(PLMXMLDocument document)
        {
            this.document = document;
            initialize();
        }

        /// <summary>
        /// Initialize the loader
        /// </summary>
        protected void initialize()
        {
            this.docHeader = getHeader(document);
            this.RootObjectId = getRootObjectId(document);
            this.rootOccurence = getRootOccurence(docHeader);
        }

        // Implement method
        protected abstract List<Occurrence> getOccurencesRecursive(Occurrence root);

        /// <summary>
        /// Gets the Document Header object
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        public DocumentHeader getHeader(PLMXMLDocument document)
        {
            return (DocumentHeader)document.getElementsByClass<DocumentHeader>("DocumentHeader")[0];
        }

        /// <summary>
        /// Gets the Root Object's Item ID
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        public virtual string getRootObjectId(PLMXMLDocument document)
        {
            AttributeBase[] usrDataObjList = (AttributeBase[])document.getElementsByClass<AttributeBase>(CadexConstants.CADEX_PLMXML_USER_DATA);
            foreach (AttributeBase objectB in usrDataObjList)
            {
                UserData data = (UserData)objectB;
                if (CadexConstants.CADEX_PLMXML_USER_INFO_TYPE == data.type)
                {
                    UserDataElement[] values = data.Items;
                    foreach (UserDataElement value in values)
                    {
                        if (CadexConstants.CADEX_CE4_PACKAGE_ID == value.getTitle())
                        {
                            return value.getValue();
                        }
                    }
                    break;
                }
            }

            return null;
        }

        /// <summary>
        /// Get the Root object occurence
        /// </summary>
        /// <param name="header"></param>
        /// <returns></returns>
        public virtual Occurrence getRootOccurence(DocumentHeader header)
        {
            object[] productViews = header.getDocument().getElementsByClass<ProductView>("ProductView");
            if (productViews != null && productViews.Length > 0)
            {
                string rootOcc = ((ProductView) productViews[0]).rootRefs;
                return (Occurrence)header.getDocument().resolveId(rootOcc);
            }
            return null;
        }

        /// <summary>
        /// Get all the child occurences
        /// </summary>
        /// <param name="occurence"></param>
        /// <returns></returns>
        public virtual Occurrence[] getChildOccurences(Occurrence occurence)
        {
            return occurence.resolveOccurrenceRefs().Cast<Occurrence>().ToArray();
        }

        /// <summary>
        /// Gets the ProductRevision from the given Occurence
        /// </summary>
        /// <param name="occurence"></param>
        /// <returns></returns>
        public virtual ProductRevision getProductRevision(Occurrence occurence)
        {
            return (ProductRevision)occurence.getInstancedRef();
        }

        /// <summary>
        /// Gets the Product from the given ProductRevision
        /// </summary>
        /// <param name="revision"></param>
        /// <returns></returns>
        public virtual Product getProduct(ProductRevision revision)
        {
            return (Product)revision.getDocument().resolveId(revision.getMasterURI());
        }

        /// <summary>
        /// Gets the name of the Product from ProductRevision
        /// </summary>
        /// <param name="revision"></param>
        /// <returns></returns>
        public string getProductName(ProductRevision revision)
        {
            return revision.name;
        }

        /// <summary>
        /// Gets the name of the Product from Product
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public string getProductName(Product product)
        {
            return product.name;
        }

        /// <summary>
        /// Gets the occurence name from the Product Revision
        /// </summary>
        /// <param name="occurence"></param>
        /// <returns></returns>
        public string getOccurenceName(Occurrence occurence)
        {
            return this.getProductRevision(occurence).name;
        }

        /// <summary>
        /// Gets the Product from the Occurrence
        /// </summary>
        /// <param name="occurence"></param>
        /// <returns></returns>
        public Product getProduct(Occurrence occurence)
        {
            ProductRevision rev =  this.getProductRevision(occurence);
            return this.getProduct(rev);
        }

        /// <summary>
        /// Gets the Revision Description from ProductRevision tag
        /// </summary>
        /// <param name="revision"></param>
        /// <returns></returns>
        public string getProductRevisionDescription(ProductRevision revision)
        {
            return revision.getDescription();
        }

        /// <summary>
        /// Gets the Revision Description from the Occurrence
        /// </summary>
        /// <param name="occurence"></param>
        /// <returns></returns>
        public string getProductRevisionDescription(Occurrence occurence)
        {
            ProductRevision revision = this.getProductRevision(occurence);
            return revision.getDescription();
        }

        /// <summary>
        /// Gets the UserData for the given occurence
        /// </summary>
        /// <param name="occurence"></param>
        /// <returns></returns>
        public UserData[] getUserData(Occurrence occurence)
        {
            return occurence.getAttributes(CadexConstants.CADEX_PLMXML_USER_DATA).Cast<UserData>().ToArray();
        }

        /// <summary>
        /// Gets the UserData for the given occurence of specific type
        /// </summary>
        /// <param name="occurence"></param>
        /// <returns></returns>
        public virtual UserData getUserData(Occurrence occurence,string type)
        {
            UserData[] data = getUserData(occurence);
            foreach (UserData usrdata in data)
            {
                if (type == usrdata.type)
                    return usrdata;
            }
            return null;
        }


        public DataSet[] getDataset(Occurrence occurrence)
        {
            List<DataSet> datasets = new List<DataSet>();
            AttributeBase[] baseObj = getProductRevision(occurrence).getAttributes(CadexConstants.CADEX_PLMXML_ASSOCIATED_DATASET);
            if (baseObj.Length > 0)
            {
                AssociatedDataSet[] datasetsRef = baseObj.Cast<AssociatedDataSet>().ToArray();
                for(int i = 0; i < datasetsRef.Length; i++)
                {
                    IdBase objBase = (IdBase)datasetsRef[i].getDocument().resolveId(datasetsRef[i].dataSetRef);
                    if (objBase != null)
                    {
                        datasets.Add((DataSet)objBase);
                    }
                }
            }
            return datasets.ToArray();
        }
    }
}

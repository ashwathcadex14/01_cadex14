﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InfoExPLMXMLSDK.com.infoex.plmxml.types;
using InfoExPLMXMLSDK.com.infoex.plmxml.generic;

namespace CadExchange.plmxml
{
    interface IPlmxmlParser
    {
        //Get the header object
        DocumentHeader getHeader(PLMXMLDocument document);

        //Get the Root Object Id for the document
        string getRootObjectId(PLMXMLDocument document);

        //Get the root occurence
        Occurrence getRootOccurence(DocumentHeader header);

        //Get all the occurences
        Occurrence[] getChildOccurences(Occurrence occurence);

        //Get the product revision from occurence
        ProductRevision getProductRevision(Occurrence occurence);

        //Get the product revision from ProductRevision
        Product getProduct(ProductRevision revsion);

        //Get the product revision from Occurrence
        Product getProduct(Occurrence occurence);

        //Get the name of the product
        string getProductName(ProductRevision revision);

        //Get the name of the product
        string getProductName(Product product);

        //Get the Description of the ProductRevision
        string getProductRevisionDescription(ProductRevision revision);

        //Get the Description of the Occurence
        string getProductRevisionDescription(Occurrence occurence);

        //Get the User data of the Occurence
        UserData[] getUserData(Occurrence occurence);

        DataSet[] getDataset(Occurrence occurrence);
    }
}

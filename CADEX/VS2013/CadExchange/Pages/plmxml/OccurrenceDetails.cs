﻿using CadExchange.Common;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadExchange.plmxml
{
    public class OccurrenceDetails : AbstractplmxmlParser
    {
        #region Declaration
        public string OccurrenceName { get; set; }

        public string OccurrenceDescription { get; set; }

        public string OccurrenceQuantity { get; set; }

        public string OccurrenceSeqNumber { get; set; }

        public ObservableCollection<OccurrenceDetails> occTreeList { get; set; }

        public ObservableCollection<DatasetProperties> datasets { get; set; }
        #endregion

        // Constructor
        public OccurrenceDetails(string uri) : base(uri)
        {
            occTreeList = new ObservableCollection<OccurrenceDetails>();
        }

        // Constructor
        public OccurrenceDetails()
        {
            occTreeList = new ObservableCollection<OccurrenceDetails>();
        }

        // abstract method
        protected override List<Occurrence> getOccurencesRecursive(Occurrence root)
        {
            return null;
        }

        private string getUserDataValue(UserData data,string title)
        {
            UserValue[] values = data.getUserValues();
            foreach(UserValue value in values)
            {
                if (title == value.getTitle())
                    return value.getValue();
            }
            return null;
        }

        public static OccurrenceDetails CreateOccurenceTree(string uri, Occurrence occs, bool isRoot)
        {
            OccurrenceDetails occ;
            if (isRoot) {
                occ = new OccurrenceDetails(uri);
                occs = occ.rootOccurence;
            }
            else {
                occ = new OccurrenceDetails();
            }

            // Used for TreeView Display name
            occ.OccurrenceName = occ.getOccurenceName(occs);
            occ.OccurrenceDescription = occ.getProductRevisionDescription(occs);
            //Get the additional display properties
            UserData data = occ.getUserData(occs, CadexConstants.CADEX_PLMXML_ATTR_IN_CONTEXT);
            occ.OccurrenceQuantity = occ.getUserDataValue(data, CadexConstants.CADEX_PLMXML_QUANTITY);
            occ.OccurrenceSeqNumber = occ.getUserDataValue(data, CadexConstants.CADEX_PLMXML_SEQUENCENUMBER);
            //Get all the associated datasets
            var datasetRefs = occ.getDataset(occs);
            if (datasetRefs != null && datasetRefs.Length > 0)
            {
                occ.datasets = new ObservableCollection<DatasetProperties>();
                occ.datasets.Add(new DatasetProperties { Name = datasetRefs[0].getName(), Type = datasetRefs[0].getType(), Version = datasetRefs[0].getVersion() });
            }

            // Recursive to build hierarchial data
            var childs = occ.getChildOccurences(occs);
            if (childs != null && childs.Length > 0)
            {
                foreach (var item in occ.getChildOccurences(occs))
                    occ.occTreeList.Add(CreateOccurenceTree(uri, item, false));
            }

            return occ;
        }
    }

    public class DatasetProperties
    {
        public string Name {get; set;}
        public int Version { get; set; }
        public string Type { get; set; }
    }
}

﻿using CadExchange.Common;
using CadExchange.Database;
using CadExchange.Database.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Media;

namespace CadExchange.Tree
{
    public abstract class AbstractCadexTreeNode<T> : ICadexTreeNode
    {
        public Dictionary<string, string> attrMap = new Dictionary<string, string>();
        public T NodeObject; // Need to be casted based on needs
        private ObservableCollection<ICadexTreeNode> _children = new ObservableCollection<ICadexTreeNode>();
        private ICadexTreeNode _parent;
        private bool _isExpanded = false;

        public string getAttr(string attrName)
        {
            string attrValue = "";
            this.attrMap.TryGetValue(attrName, out attrValue);
            return attrValue;
        }

        public AbstractCadexTreeNode(T nodeObj)
        {
            this.NodeObject = nodeObj;
            //Moved to as needed by implementors. Implementors can call this after their respective constructors
            //this.setProperties();
            //foreach (ICadexTreeNode node in this.setChildren())
            //{
            //    _children.Add(node);
            //}
        }

        public T getObject()
        {
             return this.NodeObject;
        }

        public abstract void setProperties();
        public abstract List<ICadexTreeNode> setChildren();


        public void addChild(ICadexTreeNode child)
        {
           this._children.Add(child);
        }

        public virtual ObservableCollection<ICadexTreeNode> ChildrenList
        { 
            get
            {
                return this._children;
            }
        }

        public ICadexTreeNode CadexParent
        {
            get
            {
                return this._parent;
            }

            set
            {
                this._parent = value;
            }
        }

        public virtual bool IsExpanded
        {
            get
            {
                return this._isExpanded;
            }

            set
            {
                this._isExpanded = value;
            }
        }
        
        public string Description
        {
            get
            {
                try
                {
                    return getAttr(CadexConstants.CADEX_DESC_ATTR);
                }
                catch (Exception e)
                {
                    throw new Exception("Exception while getting description", e);
                }
            }
        }

        public virtual string Name
        {
            get
            {
                try
                {
                    return getAttr(CadexConstants.CADEX_OBJECT_STRING_ATTR);
                }
                catch (Exception e)
                {
                    throw new Exception("Exception while getting Name", e);
                }
            }
        }

        public string SequenceNumber
        {
            get
            {
                try
                {
                    return getAttr(CadexConstants.CADEX_SEQ_NO_ATTR);
                }
                catch (Exception e)
                {
                    throw new Exception("Exception while getting Sequence Number", e);
                }
            }
        }

        public string Quantity
        {
            get
            {
                try
                {
                    return getAttr(CadexConstants.CADEX_QUANTITY_ATTR);
                }
                catch (Exception e)
                {
                    throw new Exception("Exception while getting Quantity", e);
                }
            }
        }

        public abstract ImageSource ImageSource { get; }
        public PackageStructure createNewNode(PackageStructure occ, PackageLineItem addThis )
        {
           
            string revPlmxmlId = null;
            if (addThis.PartOwner == PackageLineItem.OwnerType.OEM)
            {
              revPlmxmlId = Session.getOEMRevisionId(addThis.itemId, occ.orgPackage);
            }
            else
            {
                revPlmxmlId = addThis.plmxmlId;
            }
            PackageStructure newNode = new PackageStructure();
            newNode.isNew = true;
            newNode.orgPackage = occ.orgPackage;
            newNode.parentId = occ.childId;
            newNode.itemId = addThis.itemId;
            newNode.childId = "occ_" + DateTime.Now.ToString("ddMMyyyy_HHmmssfff");
            newNode.childPlmxmlId = revPlmxmlId;
            return newNode;
        }
    }
}

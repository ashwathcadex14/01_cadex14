﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InfoExPLMXMLSDK.com.infoex.plmxml.types;
using System.Windows.Xps.Packaging;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using CadExchange.Common;
using CadExchange.Utils;
using CadExchange.Tree.dataset;

namespace CadExchange.Tree
{
    public class DatasetFileNode : AbstractCadexDatasetFileNode<ExternalFile>
    {
        public DatasetFileNode(ExternalFile file, string parentdir, DatasetStructureNode parent)
            : base(file,parentdir,parent)
        {

        }

        public override string ExternalFile
        {
            get
            {
                return this.parentDir + "\\" + System.Uri.UnescapeDataString(this.getAttr(CadexConstants.CADEX_REL_FILE_PATH_ATTR));
            }
        }

        /// <summary>
        /// Gets or sets the source image.
        /// </summary>
        /// <value>The source.</value>
        public override ImageSource ImageSource
        {
            get
            {
                return new BitmapImage(new Uri("/InfoExchange;component/images/1413579964_12.File-32.png", UriKind.Relative));
            }
        }

        public override void setProperties()
        {
            try
            {
                string locationRef = this.getObject().getAttributeValueAsString("locationRef");
                string orgFileName = "";
                if (locationRef.Contains("\\"))
                    orgFileName = locationRef.Substring(locationRef.IndexOf("\\") + 1, locationRef.Length - locationRef.IndexOf("\\") - 1);
                orgFileName = System.Uri.UnescapeDataString(orgFileName);
                this.attrMap.Add(CadexConstants.CADEX_OBJECT_STRING_ATTR, orgFileName);
                this.attrMap.Add(CadexConstants.CADEX_ORG_FILE_NAME_ATTR, orgFileName);
                this.attrMap.Add(CadexConstants.CADEX_REL_FILE_PATH_ATTR, locationRef);


                foreach (KeyValuePair<string, string> pair in plmxmlUtil.addUserData(this.getObject()))
                {
                    this.attrMap.Add(pair.Key, pair.Value);
                }
            }
            catch (Exception e)
            {
                throw new Exception("Exception during setting properties.", e);
            }
        }

        public override List<ICadexTreeNode> setChildren()
        {
            List<ICadexTreeNode> files = new List<ICadexTreeNode>();

            try
            {

            }
            catch (Exception e)
            {
                throw new Exception("Exception during setting children.", e);
            }

            return files;
        }

        
    }
}

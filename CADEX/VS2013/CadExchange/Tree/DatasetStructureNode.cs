﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InfoExPLMXMLSDK.com.infoex.plmxml.types;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using CadExchange.Common;
using CadExchange.Utils;
using CadExchange.Tree.dataset;

namespace CadExchange.Tree
{
    public class DatasetStructureNode : AbstractCadexDatasetNode<DataSet>
    {
        /// <summary>
        /// Gets or sets the source image.
        /// </summary>
        /// <value>The source.</value>
        public override ImageSource ImageSource
        {
            get
            {
                return new BitmapImage(new Uri("/InfoExchange;component/images/Dataset_1.png", UriKind.Relative));
            }
        }

        public DatasetStructureNode(DataSet dset, string rel, string parentdir, ItemStructureNode parent)
            : base(dset,rel,parentdir,parent)
        {
        }

        public override void setProperties()
        {
            try
            {
                this.attrMap.Add(CadexConstants.CADEX_OBJECT_STRING_ATTR, this.getObject().getAttributeValueAsString("name"));
                this.attrMap.Add(CadexConstants.CADEX_TYPE_ATTR, this.getObject().getAttributeValueAsString("type"));

                foreach (KeyValuePair<string, string> pair in plmxmlUtil.addUserData(this.getObject()))
                {
                    this.attrMap.Add(pair.Key, pair.Value);
                }
            }
            catch (Exception e)
            {
                throw new Exception("Exception during setting properties.", e);
            }
        }

        public override List<ICadexTreeNode> setChildren()
        {
            List<ICadexTreeNode> files = new List<ICadexTreeNode>();

            try
            {
                String[] baseObj = this.getObject().memberRefs;

                if (baseObj != null && baseObj.Length > 0)
                {
                    for (int i = 0; i < baseObj.Length; i++)
                    {
                        ExternalFile file = (ExternalFile) this.getObject().getDocument().resolveId(baseObj[i]);
                        DatasetFileNode fileNode = new DatasetFileNode(file, parentDir, this);
                        if (fileNode.IsPrimary)
                            _primaryFile = fileNode;
                        files.Add(fileNode);
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("Exception during setting children.", e);
            }

            return files;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadExchange.Database.Entities;
using System.Drawing;

namespace CadExchange.Tree
{
    public interface ICadexItemTreeNode : ICadexTreeNode
    {
        bool Locked { get; }
        string LockedBy { get; }
        string GetCadFile
        {
            get;
        }

        string GetRenderFile
        {
            get;
        }

        List<ICadexTreeNode> Datasets
        {
            get;
        }

        void lockItem(bool lockObj, ref List<ICadexItemTreeNode> checkOutNodes);
        void lockDatasets(bool lockObj);
        List<PackageStructure> addPart(PackageLineItem addThis); //throws Exception
        List<PackageStructure> remPart(PackageStructure remThis); //throws Exception
        //void encryptFile();
        //void decryptFile();
        CadexSupplierDataset addDatasets(string relation, string path, string name, string type, string desc );
        CadexSupplierDataset remDatasets(DatasetStructureNode dSet);

        PackageLineItem PkgLineItem
        {
            get;
        }

        PackageStructure OccNode
        {
            get;
        }

        int OwningPackage
        {
            get;
        }

        bool HasCad
        {
            get;
        }

        string CadType
        {
            get;
        }

        bool HasRendering
        {
            get;
        }

        bool ReadOnly
        {
            get;
        }

        Image ThumbnailImage
        {
            get;
        }

        void refreshDatasets();
       
    }
}   

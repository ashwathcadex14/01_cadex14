﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadExchange.Tree
{
    interface ICadexJTinfoNode
    {
          string GetRenderFile { get;  }
          string ItemId { get; }
          string Matrix { get; }
          string ParentDir { get; }
    }
}

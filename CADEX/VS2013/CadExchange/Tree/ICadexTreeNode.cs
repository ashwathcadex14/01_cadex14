﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows.Media;


namespace CadExchange.Tree
{
    public interface ICadexTreeNode
    {
        ObservableCollection<ICadexTreeNode> ChildrenList
        { get; }
        ICadexTreeNode CadexParent { get; set; }
        string Name { get; }
        string Description { get; }
        string Quantity { get; }
        string SequenceNumber { get; }
        string getAttr(string attrName);
        ImageSource ImageSource {get;}
        bool IsExpanded { get; set; }
    }
}

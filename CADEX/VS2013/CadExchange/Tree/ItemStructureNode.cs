﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using InfoExPLMXMLSDK.com.infoex.plmxml.types;
using InfoExPLMXMLSDK.com.infoex.plmxml.generic;
using CadExchange.Database.Entities;
using CadExchange.Common;
using CadExchange.Utils;
using NHibernate;
using CadExchange.Database;
using System.IO;
using System.Drawing;
using CadExchange.Tree.dataset;

namespace CadExchange.Tree
{
    public class ItemStructureNode : AbstractCadexTreeNode<Occurrence>, ICadexItemTreeNode, ICadexJTinfoNode
    {
        private PackageLineItem thisItem;
        private PackageStructure thisOcc;
        public PLMXMLDocument pkgPlmxmlDoc;
        private List<ICadexTreeNode> datasets = new List<ICadexTreeNode>();
        private ProductRevision revision;
        private Product item;
        public string parentDir;
        private bool _hasCad = false;
        private bool _hasRender = false;
        private string _cadType = "";
        private Image thumbnailImage;

        public Image ThumbnailImage
        {
            get
            {
                return thumbnailImage;
            }
        }

        public List<ICadexTreeNode> Datasets
        {
            get
            {
                return datasets;
            }
        }

        public int OwningPackage
        {
            get
            {
                return thisItem.orgPackage == null ? -1 : thisItem.orgPackage.Id;
            }
        }

        public PackageLineItem PkgLineItem
        {
            get
            {
                return thisItem;
            }
        }
       

        public Product Item
        {
            get { return item; }
            private set { item = value; }
        }
        
      
        /// <summary>
        /// Gets or sets the source image.
        /// </summary>
        /// <value>The source.</value>
        public override ImageSource ImageSource
        {
            get
            {
                return new BitmapImage(new Uri("/InfoExchange;component/images/Item.png", UriKind.Relative));
            }
        }

        public void lockDatasets(bool lockObj)
        {
            // As the next loop handles these, not required at the moment.
            //if (!ReadOnly)
            //{
            //    if (HasCad)
            //    {
            //        CommonUtil.setReadOnly(GetCadFile, !lockObj);
            //    }

            //    if (HasRendering)
            //    {
            //        CommonUtil.setReadOnly(GetRenderFile, !lockObj);
            //    }
            //}

            foreach (ICadexDatasetNode dSet in datasets)
            {
                dSet.lockObj(lockObj);
            }
        }

        public void lockItem(bool lockObj, ref List<ICadexItemTreeNode> checkOutNodes)
        {
            string objStr = this.getAttr(CadexConstants.CADEX_OBJECT_STRING_ATTR);

            //Not so strict now
            //if (Locked == lockObj)
            //{
            //    throw new Exception(lockObj ? "The object " + objStr + " is already locked." : "The object " + objStr + " is already unlocked.");
            //}
            //else
            //{
                var lines = Session.GetCurrentSession().QueryOver<PackageLineItem>().Where(p => p.itemId == (this.getAttr(CadexConstants.CADEX_ID_ATTR) + "|" + this.getAttr(CadexConstants.CADEX_REV_ID_ATTR))).List();
                foreach (PackageLineItem line in lines)
                {
                    if (line.isLocked != lockObj)
                    {
                        line.isLocked = lockObj;
                        line.LockedBy = lockObj ? Session.getUserByUserId(CadexSession.getSessionUser()).UserId : "";
                        Session.persistObject(line);
                        thisItem = line;
                        checkOutNodes.Add(this);
                    }
                    //Not so strict now
                    //else
                    //    throw new Exception(lockObj ? "The object " + objStr + " is already locked." : "The object " + objStr + " is already unlocked.");
                }
            //}

            foreach (ICadexItemTreeNode child in ChildrenList)
            {
                try
                {
                    child.lockItem(lockObj, ref checkOutNodes);
                }
                catch (Exception e)
                { InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);throw; }

            }
        }

        public List<PackageStructure> addPart(PackageLineItem addThis)
        {
            List<PackageStructure> newLines = new List<PackageStructure>();
            try
            {
                PackageStructure newNode = createNewNode(thisOcc, addThis);
                newLines.Add(newNode);

                List<PackageStructure> otherOccs = Session.getOtherOccurrences(thisOcc,true);

                foreach(PackageStructure otherOcc in otherOccs)
                {
                    newLines.Add(createNewNode(otherOcc, addThis));
                }
            }
            catch (Exception e)
            {
                throw new Exception("Exception while adding part.", e);
            }
            return newLines;
        }

        public List<PackageStructure> remPart(PackageStructure remThis)
        {
            List<PackageStructure> newLines = new List<PackageStructure>();
            try
            {
                List<PackageStructure> otherOccs = Session.getOtherOccurrences(thisOcc, false);

                foreach (PackageStructure otherOcc in otherOccs)
                {
                    newLines.AddRange(Session.getStructureChildWithItem(otherOcc, remThis.itemId));
                }
            }
            catch (Exception e)
            {
                throw new Exception("Exception while removing part.", e);
            }
            return newLines;
        }

        public CadexSupplierDataset addDatasets(string relation, string path, string name, string type, string desc)
        {
            CadexSupplierDataset newDset = null;

            try
            {
                PackageLineItem line = Session.getObjectById<PackageLineItem>(Convert.ToString(this.PkgLineItem.Id));
                newDset = new CadexSupplierDataset(relation, line);
                newDset.addAttributeToObject("Dataset", "object_name", name);
                newDset.addAttributeToObject("Dataset", "object_desc", desc);
                newDset.addAttributeToObject("Dataset", "object_type", type);
                newDset.addAttributeToObject("Dataset", "object_string", name);
                string folderName = Path.GetFileNameWithoutExtension(line.orgPackage.getPlmXMLPath());
                string packageRoot = Path.GetDirectoryName(line.orgPackage.getPlmXMLPath());
                string oemDsetDir = packageRoot + CadexConstants.CADEX_PATH_SEPARTOR + folderName;

                if (!Directory.Exists(oemDsetDir))
                    Directory.CreateDirectory(oemDsetDir);

                string newFile = oemDsetDir + CadexConstants.CADEX_PATH_SEPARTOR + Path.GetFileName(path);
                if (File.Exists(newFile))
                    throw new Exception("A file with the same name is already available in the item directory. Please choose a file with different name.");
                File.Copy(path, newFile);
                CadexSupplierDatasetFile newDsetFile = new CadexSupplierDatasetFile(newDset, newFile);
                Session.persistObject(newDset);
                thisItem = line;
            }
            catch (Exception e)
            {
                throw new Exception("Exception while adding dataset.", e);
            }

            return newDset;
        }
        public CadexSupplierDataset remDatasets(DatasetStructureNode dSet)
        {
            CadexSupplierDataset newDset = null;

            try
            {
               
            }
            catch (Exception e)
            {
                throw new Exception("Exception while removing dataset.", e);
            }

            return newDset;
        }

       

        public ItemStructureNode(PLMXMLDocument doc, Occurrence occ, PackageStructure structNode, PackageLineItem lineItem, ICadexItemTreeNode parentNode)
            : this(occ)
        {
            this.pkgPlmxmlDoc = doc;
            this.thisOcc = structNode;
            //this.thisItem = Session.getLineFromStructure(this.thisOcc, false);
            this.thisItem = lineItem;
            this.parentDir = this.thisItem.orgPackage.getPackageDir();
            this.CadexParent = parentNode;
            setItemRev();
            this.setProperties();
            foreach (ICadexTreeNode node in this.setChildren())
            {
                this.addChild(node);
            }
            refreshDatasets();
            createThumbnail();
        }

        private void createThumbnail()
        {
           thumbnailImage = CommonUtil.createThumbnail(this);
        }

        public void refreshDatasets()
        {
            try
            {
                foreach (ICadexTreeNode node in this.setDatasets())
                {
                    this.datasets.Add(node);
                }
            }
            catch (Exception e)
            {
                InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);throw;
            }
        }

        public ItemStructureNode(Occurrence nodeObj)
            : base(nodeObj)
        {

        }

        public PackageStructure OccNode
        {
            get
            {
                return thisOcc;
            }
        }

        public bool ReadOnly
        {
            get
            {
                string prop = getAttr("checked_out");
                return this.item.getAttributeValueAsString("subType").Equals("CE4_ExPkg") ? false : (prop != null && prop.Equals("Y")) ? false : true;
            }
        }

        public bool HasCad
        {
            get
            {
                return _hasCad;
            }
        }

        public string CadType
        {
            get
            {
                return _cadType;
            }
        }

        public bool HasRendering
        {
            get
            {
                return _hasRender;
            }
        }

        public bool Locked
        {
            get
            {
                return thisItem.isLocked;
            }
        }

        public string LockedBy
        {
            get
            {
                return thisItem.LockedBy;
            }
        }

        public string GetCadFile
        {
            get
            {
                ICadexDatasetNode dset = ((ICadexDatasetNode)this.datasets.Find(this.FindCadDataset));
                return dset!=null ? dset.PrimaryFile : null;
            }
        }

        public string GetRenderFile 
        {
            get 
            {
                string render = getRendering();
                string newJT = CommonUtil.GetPLMXMLFilesDir(thisOcc.orgPackage.getPackageDir()) + "\\" + ItemId + "_" + this.getAttr(CadexConstants.CADEX_REV_ID_ATTR) + "__MODEL.jt";
                if (File.Exists(newJT))
                {
                    if (render != null && render.Length > 0 && File.Exists(render))
                        File.Copy(newJT, render, true);
                }
                return render;
            }
        }

        private string getRendering()
        {
            ICadexTreeNode renderObj = this.datasets.Find(this.FindRenderDataset);
            if (renderObj != null)
            {
                return ((ICadexDatasetNode)renderObj).PrimaryFile;
            }
            else
            {
                return null;
            }
        }

        public string ProjectType
        {
            get
            {
                string attr ;
                this.attrMap.TryGetValue("project_type", out attr);
                return attr;
            }
        }

        public string Project
        {
            get
            {
                string attr;
                this.attrMap.TryGetValue("project", out attr);
                return attr;
            }
        }

        public string Plant
        {
            get
            {
                string attr;
                this.attrMap.TryGetValue("plant", out attr);
                return attr;
            }
        }

        public string Area
        {
            get
            {
                string attr;
                this.attrMap.TryGetValue("area", out attr);
                return attr;
            }
        }

        public string Equipment
        {
            get
            {
                string attr;
                this.attrMap.TryGetValue("equipment", out attr);
                return attr;
            }
        }

        public string SubGroup
        {
            get
            {
                string attr;
                this.attrMap.TryGetValue("sub_group", out attr);
                return attr;
            }
        }

        public string SupplyGroupCode
        {
            get
            {
                string attr;
                this.attrMap.TryGetValue("supply_group_code", out attr);
                return attr;
            }
        }

        public string PartNumber
        {
            get
            {
                string attr;
                this.attrMap.TryGetValue("part_number", out attr);
                return attr;
            }
        }

        public override void setProperties()
        {
            try
            {
                
                if (item != null && revision != null)
                {
                    this.attrMap.Add(CadexConstants.CADEX_ID_ATTR, this.item.getAttributeValueAsString("productId"));
                    string subType = this.item.getAttributeValueAsString("subType");
                    this.attrMap.Add("subType", subType);
                    //string objString = subType.Equals("Item") ? this.item.getAttributeValueAsString("name") : this.item.getAttributeValueAsString("productId") + "/" + this.revision.getAttributeValueAsString("revision") + ";" + this.item.getAttributeValueAsString("name");
                    string objString = false ? this.item.getAttributeValueAsString("name") : this.item.getAttributeValueAsString("productId") + "/" + this.revision.getAttributeValueAsString("revision") + ";" + this.item.getAttributeValueAsString("name");
                    this.attrMap.Add(CadexConstants.CADEX_OBJECT_STRING_ATTR, objString);
                    string objDesc = this.item.getDescription();
                    //if (objDesc.Length == 0)
                        objDesc = this.revision.getDescription();
                    this.attrMap.Add(CadexConstants.CADEX_DESC_ATTR, objDesc);
                    
                    this.attrMap.Add(CadexConstants.CADEX_REV_ID_ATTR, this.revision.getAttributeValueAsString("revision"));
                    
                    string tempDesc;
                    this.attrMap.TryGetValue(CadexConstants.CADEX_DESC_ATTR, out tempDesc);

                    if (tempDesc != null && tempDesc.Length > 0)
                    {
                        char[] delimiters = { '.' };
                        string[] allCodes = tempDesc.Split(delimiters);
                        if(allCodes.Length == 8)
                        {
                            this.attrMap.Add("project_type", allCodes[0]);
                            this.attrMap.Add("project", allCodes[1]);
                            this.attrMap.Add("plant", allCodes[2]);
                            this.attrMap.Add("area", allCodes[3]);
                            this.attrMap.Add("equipment", allCodes[4]);
                            this.attrMap.Add("sub_group", allCodes[5]);
                            this.attrMap.Add("supply_group_code", allCodes[6]);
                            this.attrMap.Add("part_number", allCodes[7]);
                        }
                    }


                    if (!thisOcc.isNew)
                    {
                        foreach (KeyValuePair<string, string> pair in plmxmlUtil.addUserData(this.getObject()))
                        {
                            this.attrMap.Add(pair.Key, pair.Value);
                        }
                    }
                    else
                    {
                        populateModelAttrs(thisOcc);
                    }

                    foreach (KeyValuePair<string, string> pair in plmxmlUtil.addUserData(this.revision))
                    {
                        if (this.attrMap.ContainsKey(pair.Key))
                            this.attrMap.Remove(pair.Key);
                        this.attrMap.Add(pair.Key, pair.Value);
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("Exception during setting properties.", e);
            }
        }

        private void populateModelAttrs(ModelObject obj)
        {
            try
            {
                foreach (ModelAttribute attr in obj.Attributes)
                {
                    this.attrMap.Add(attr.Name, attr.Value);
                }
            }
            catch (Exception e)
            {
                throw new Exception("Exception during populating Model properties for " + obj.GetType(), e);
            }
        }

        public override List<ICadexTreeNode> setChildren()
        {
            List<ICadexTreeNode> allChilds = new List<ICadexTreeNode>();

            try
            {
                List<PackageStructure> allChildLines = Session.getStructureChilds1(thisOcc);

                foreach (PackageStructure line in allChildLines)
                {
                    PackageLineItem lineItem = Session.getLineFromStructure1(line, line.orgPackage);
                    
                    if (lineItem.PartOwner == PackageLineItem.OwnerType.OEM)
                    {
                        Occurrence occ = null;
                        if(!line.isNew)
                             occ = (Occurrence)pkgPlmxmlDoc.resolveId(line.childId);
                        ItemStructureNode newNode = new ItemStructureNode(pkgPlmxmlDoc, occ, line, lineItem, this);
                        allChilds.Add(newNode);
                    }
                    else
                    {
                        CadexSupplierItemRevision childItemRev = Session.getObjectById<CadexSupplierItemRevision>(line.childPlmxmlId);
                        SupplierItemNode newNode = new SupplierItemNode(pkgPlmxmlDoc, childItemRev, line, this);
                        allChilds.Add(newNode);
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("Exception during setting children.", e);
            }

            return allChilds;
        }

        private bool FindCadDataset(ICadexTreeNode line)
        {
            string cadType = CadexSession.getCadType(line.getAttr(CadexConstants.CADEX_TYPE_ATTR));
            if (cadType != null && cadType.Equals(this.CadType))
                return true;
            else
                return false;
        }

        private bool FindRenderDataset(ICadexTreeNode line)
        {
            if (CadexSession.isRender(line.getAttr(CadexConstants.CADEX_TYPE_ATTR)))
                return true;
            else
                return false;
        }

        private List<ICadexTreeNode> setDatasets()
        {
            List<ICadexTreeNode> allDatasets = new List<ICadexTreeNode>();

            try
            {
                
                AttributeBase[] assDsets = revision.getAttributes(CadexConstants.CADEX_PLMXML_ASSOCIATED_DATASET);

                if (assDsets != null && assDsets.Length > 0)
                {
                    for (int i = 0; i < assDsets.Length; i++)
                    {
                        AssociatedDataSet assDset = (AssociatedDataSet)assDsets[i];
                        DataSet objBase = assDset.GetDataSet();
                        if (objBase != null)
                        {
                            DatasetStructureNode dsetNode = new DatasetStructureNode((DataSet)objBase, assDset.getAttributeValueAsString("role"), parentDir, this);
                            if (!_hasRender)
                                _hasRender = CadexSession.isRender(dsetNode.getAttr("object_type"));

                            if (!_hasCad)
                            {
                                string objType = dsetNode.getAttr("object_type");
                                _hasCad = CadexSession.isCad(objType);
                                _cadType = CadexSession.getCadType(objType);
                            }

                            allDatasets.Add(dsetNode);
                        }
                    }
                }

                foreach(CadexSupplierDataset sDset in thisItem.Datasets)
                {
                    SDatasetStructureNode dsetNode = new SDatasetStructureNode(sDset, sDset.Relation, "" , this);
                    if (!_hasRender)
                        _hasRender = CadexSession.isRender(dsetNode.getAttr("object_type"));

                    if (!_hasCad)
                    {
                        string objType = dsetNode.getAttr("object_type");
                        _hasCad = CadexSession.isCad(objType);
                        _cadType = CadexSession.getCadType(objType);
                    }
                    allDatasets.Add(dsetNode);
                }

            }
            catch (Exception e)
            {
                throw new Exception("Exception during setting children.", e);
            }

            return allDatasets;
        }

        private void setItemRev()
        {
            this.revision = (ProductRevision)pkgPlmxmlDoc.resolveId(thisOcc.childPlmxmlId);
            if (this.revision != null)
                this.item = (Product)revision.getMasterObject();
        }



        public string ItemId
        {
            get { return Item.productId; }
        }

        public string Matrix
        {
            get
            {
                Occurrence occ = getObject();
                String Instanceref = occ.instanceRefs[0];
                ProductInstance ProdInst = (ProductInstance)occ.getDocument().resolveId(Instanceref);
                return ProdInst.Transform.Text[0];
            }
        }


        public string ParentDir
        {
            get { return parentDir; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadExchange.Database.Entities;


using CadExchange.Utils;
using CadExchange.Common;
using System.Collections.ObjectModel;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.IO;
using System.Windows.Xps.Packaging;
using CompareEngine.Compare.Cadex;
using CompareEngine.Model.Interfaces;
using CompareEngine.Model.Plmxml;


namespace CadExchange.Tree
{
    public class PopulateCompareTree
    {
        private CompareNode lineItem;
        

        public PopulateCompareTree(CompareNode item)
        {
            this.lineItem = item;
        }

        public List<ICadexTreeNode> buildTreeForItem(int side)
        {
            List<ICadexTreeNode> retNodes = new List<ICadexTreeNode>();

            try
            {
                retNodes.Add(new CompareStructureNode(this.lineItem, side));
            }
            catch (Exception e)
            {
                throw new Exception("Error while building tree for Item", e);
            }

            return retNodes;
        }

        public class CompareAttributeNode : AbstractCadexTreeNode<AttributeNode>
        {
            public CompareAttributeNode(AttributeNode attr) : base(attr)
            {

            }

            public override List<ICadexTreeNode> setChildren()
            {
                return new List<ICadexTreeNode>();
            }

            public GradientBrush CellColour
            {
                get
                {
                    if (!LeftValue.Equals(RightValue))
                    {
                        return CommonUtil.getModifiedColour();
                    }
                    else
                        return null;
                }
            }

            public override void setProperties()
            {
                if(getObject()!=null)
                {
                    this.attrMap.Add("object_string", getObject().getAttrName());
                }
            }

            public override ImageSource ImageSource
            {
                get 
                {
                    return null;
                }
            }

            public string LeftValue
            {
                get
                {
                    return getObject().getAttrValueL();
                }
            }

            public string RightValue
            {
                get
                {
                    return getObject().getAttrValueR();
                }
            }
        }

        public class CompareStructureNode : AbstractCadexTreeNode<CompareNode>
        {
            private ICadexComparable node;
            private int side;
            private int seqNo;
            private int nodeType;

            /// <summary>
            /// Gets or sets the source image.
            /// </summary>
            /// <value>The source.</value>
            public override ImageSource ImageSource
            {
                get 
                {
                    if (NodeObject.getDelta() == CompareEngine.Delta.DELTA_INFO.NO_CHANGE && this.nodeType == CompareNode.COMPARE_LINE_NODE)
                    {
                        return isModified(CompareNode.COMPARE_LINE_NODE) ? CommonUtil.getCompareNodeImage(CompareEngine.Delta.DELTA_INFO.MODIFIED) : CommonUtil.getCompareNodeImage(CompareEngine.Delta.DELTA_INFO.NO_CHANGE);
                    }
                    else
                        return CommonUtil.getCompareNodeImage(this.NodeObject.getDelta());
                }
            }

            public string CmpStatus
            {
                get
                {
                    if (NodeObject.getDelta() == CompareEngine.Delta.DELTA_INFO.NO_CHANGE && this.nodeType == CompareNode.COMPARE_LINE_NODE)
                    {
                        //return isModified(CompareNode.COMPARE_LINE_NODE) ? NodeObject.getDelta().ToString() : getDetailedInfo();
                        return getDetailedInfo();
                    }
                    else
                        return NodeObject.getDelta().ToString();
                }
            }

            private string getDetailedInfo()
            {
               if(this.nodeType == CompareNode.COMPARE_LINE_NODE)
                {
                    List<CompareNode> childDetails = this.NodeObject.getChildDetailNodes();
                    if(childDetails != null && childDetails.Count == 1)
                    {
                        List<CompareNode> childNodes = childDetails[0].getChildNodes();
                        if(childNodes != null && childNodes.Count == 1)
                        {
                            List<CompareNode> dsetNodes = childNodes[0].getChildNodes();
                            if(dsetNodes != null && dsetNodes.Count > 0)
                            {
                                foreach(CompareNode dsetNode in dsetNodes)
                                {
                                    if (dsetNode.getDelta() != CompareEngine.Delta.DELTA_INFO.NO_CHANGE)
                                    {
                                        return "Datasets Added/Removed/Modified";
                                    }
                                }
                            }
                        }
                    }
                }

                return "";
            }

            /// <summary>
            /// Gets the cell coulour.
            /// </summary>
            /// <value>The source.</value>
            public GradientBrush CellColour
            {
                get
                {
                    //switch (this.NodeObject.getDelta())
                    //{
                    //    case CompareEngine.Delta.DELTA_INFO.NEW:
                    //        return this.side == CompareNode.RIGHT_SIDE || this.nodeType == CompareNode.COMPARE_OTHER_NODE ? CommonUtil.getNewColour() : null;
                    //    case CompareEngine.Delta.DELTA_INFO.REMOVED:
                    //        return this.side == CompareNode.LEFT_SIDE || this.nodeType == CompareNode.COMPARE_OTHER_NODE ? CommonUtil.getRemovedColour() : null;
                    //    case CompareEngine.Delta.DELTA_INFO.MODIFIED:
                    //        return this.side == CompareNode.RIGHT_SIDE || this.nodeType == CompareNode.COMPARE_OTHER_NODE ? CommonUtil.getModifiedColour() : null;
                    //    default:
                    //        return null;
                    //}
                    return null;
                }
            }

            public override ObservableCollection<ICadexTreeNode> ChildrenList
            {
                get
                {
                    ObservableCollection<ICadexTreeNode> allChilds = base.ChildrenList;
                    ObservableCollection<ICadexTreeNode> lineChilds = new ObservableCollection<ICadexTreeNode>();
                    if (this.nodeType == CompareNode.COMPARE_LINE_NODE)
                    {
                        foreach (ICadexTreeNode childNode in allChilds)
                        {
                            if (((CompareStructureNode)childNode).nodeType == CompareNode.COMPARE_LINE_NODE)
                            {
                                lineChilds.Add(childNode);
                            }
                                
                        }
                    }
                    else
                    {
                        lineChilds = allChilds;
                    }

                    return lineChilds;
                }
            }

            public ObservableCollection<ICadexTreeNode> RelationList
            {
                get
                {
                    ObservableCollection<ICadexTreeNode> allChilds = base.ChildrenList;
                    ObservableCollection<ICadexTreeNode> lineChilds = new ObservableCollection<ICadexTreeNode>();

                    if (this.nodeType == CompareNode.COMPARE_LINE_NODE)
                    {
                        foreach (ICadexTreeNode childNode in allChilds)
                        {
                            if (((CompareStructureNode)childNode).nodeType == CompareNode.COMPARE_OTHER_NODE)
                            {
                                lineChilds.Add(childNode);
                            }
                                
                        }
                    }

                    return lineChilds;
                }
            }

            public ObservableCollection<ICadexTreeNode> AttributeList
            {
                get
                {
                    ObservableCollection<ICadexTreeNode> lineAttrs = new ObservableCollection<ICadexTreeNode>();
                    List<AttributeNode> attrNodes = this.getObject().getAttrNodes();
                    
                    foreach(AttributeNode attrNode in attrNodes)
                    {
                        CompareAttributeNode lineAttr = new CompareAttributeNode(attrNode);
                        lineAttr.setProperties();
                        lineAttrs.Add(lineAttr);
                    }
                    
                    return lineAttrs;
                }
            }

            public int SeqInternalNo
            {
                get
                {
                    return this.seqNo;
                }

            }

            public ICadexComparable SideNode
            {
                get
                {
                    return node;
                }
            }

            public CompareStructureNode(CompareNode nodeObj, int inSide)
                : base(nodeObj)
            {
               this.side = inSide;
               this.node = inSide == CompareNode.LEFT_SIDE ? nodeObj.leftNode : nodeObj.rightNode;
               this.seqNo = nodeObj.seqNo;
               this.nodeType = nodeObj.getNodeType();
               this.setProperties();
               foreach (ICadexTreeNode node in this.setChildren())
               {
                   this.addChild(node);
               }
               this.IsExpanded = true;
            }

            public override void setProperties()
            {
                try
                {
                    if(node!=null)
                    {
                        this.attrMap.Add("object_string", ((ICadexGenericObject)node).getAttr("object_string"));
                    }
                }
                catch (Exception e)
                {
                    throw new Exception("Exception during setting properties.", e);
                }
            }

            public override List<ICadexTreeNode> setChildren()
            {
                List<ICadexTreeNode> allChilds = new List<ICadexTreeNode>();

                try
                {
                    foreach (CompareNode line in this.getObject().getChildNodes())
                    {
                        CompareStructureNode newNode = new CompareStructureNode(line,side);
                        allChilds.Add(newNode);
                    }

                    if (nodeType == CompareNode.COMPARE_LINE_NODE)
                    {
                        foreach (CompareNode line in this.getObject().getChildDetailNodes())
                        {
                            CompareStructureNode newNode = new CompareStructureNode(line, side);
                            allChilds.Add(newNode);
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new Exception("Exception during setting children.", e);
                }

                return allChilds;
            }

            public override bool Equals(object obj)
            {
                if(obj is CompareStructureNode)
                    return this.seqNo == ((CompareStructureNode)obj).seqNo;
                return false;
            }

            public override int GetHashCode()
            {
                return this.seqNo.GetHashCode();
            }

            private bool isModified(int nodeType)
            {
                foreach (CompareStructureNode node in (nodeType == CompareNode.COMPARE_LINE_NODE ? RelationList : ChildrenList))
                {
                    if(node.getObject().getDelta() != CompareEngine.Delta.DELTA_INFO.NO_CHANGE)
                    {
                        return true;
                    }
                    else
                    {
                        return node.isModified(CompareNode.COMPARE_OTHER_NODE);
                    }
                }

                return false;
            }
        }
    }
}

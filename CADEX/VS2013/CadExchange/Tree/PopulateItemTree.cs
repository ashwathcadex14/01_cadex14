﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadExchange.Database.Entities;
using InfoExPLMXMLSDK.com.infoex.plmxml.types;
using InfoExPLMXMLSDK.com.infoex.plmxml.generic;
using CadExchange.Utils;
using CadExchange.Common;
using System.Collections.ObjectModel;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.IO;
using System.Windows.Xps.Packaging;
using CadExchange.Database;
using NHibernate;

namespace CadExchange.Tree
{
    public class PopulateItemTree
    {
        private PackageStructure lineNode;
        private PackageLineItem lineItem;
        public PLMXMLDocument pkgPlmxmlDoc;
        public string parentDir;

        public PopulateItemTree(PackageStructure item)
        {
            this.lineNode = item;
            this.lineItem = Session.getLineFromStructure1(this.lineNode, this.lineNode.orgPackage);

            if (lineItem.PartOwner == PackageLineItem.OwnerType.OEM)
            {
                pkgPlmxmlDoc = plmxmlUtil.readDocument(this.lineItem.orgPackage.getPlmXMLPath());
            }
        }

        public List<ICadexTreeNode> buildTreeForItem()
        {
            List<ICadexTreeNode> retNodes = new List<ICadexTreeNode>();

            try
            {
                if (lineItem.PartOwner == PackageLineItem.OwnerType.OEM)
                {
                    Occurrence occ = (Occurrence)pkgPlmxmlDoc.resolveId(this.lineNode.childId);
                    ItemStructureNode rootNode = new ItemStructureNode(pkgPlmxmlDoc, occ, lineNode, lineItem, null);
                    retNodes.Add(rootNode);
                }
                else
                {
                    CadexSupplierItemRevision item = Session.getObjectById<CadexSupplierItemRevision>(this.lineNode.childPlmxmlId);
                    SupplierItemNode newNode = new SupplierItemNode(pkgPlmxmlDoc, item, lineNode, null);
                    retNodes.Add(newNode);
                }
                
            }
            catch (Exception e)
            {
                throw new Exception("Error while building tree for Item", e);
            }

            return retNodes;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadExchange.Database.Entities;
using InfoExPLMXMLSDK.com.infoex.plmxml.types;
using InfoExPLMXMLSDK.com.infoex.plmxml.generic;
using CadExchange.Utils;
using CadExchange.Common;
using System.Collections.ObjectModel;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using CadExchange.Database;
using System.ComponentModel;

namespace CadExchange.Tree
{
    public class PopulatePackageTree
    {
        private Package thisPkg;
        public PLMXMLDocument pkgPlmxmlDoc;

        
        public PopulatePackageTree(Package pkg)
        {
            this.thisPkg = pkg;
            pkgPlmxmlDoc = plmxmlUtil.readDocument(pkg.getPlmXMLPath());
        }

        public List<ICadexTreeNode> buildTreeForPkg()
        {
            List<ICadexTreeNode> retNodes = new List<ICadexTreeNode>();

            try
            {
                Console.WriteLine(DateTime.Now.ToString());
                PackageStructure rootLine = Session.getRootLine(thisPkg);
                PackageLineItem lineItem = Session.getLineFromStructure1(rootLine, rootLine.orgPackage);
                //Console.WriteLine(rootLine);
                Occurrence occ = (Occurrence)pkgPlmxmlDoc.resolveId(rootLine.childId);
                //Console.WriteLine(pkgPlmxmlDoc);
                PackageStructureNode rootNode = new PackageStructureNode(pkgPlmxmlDoc, lineItem, rootLine, occ, null);
                retNodes.Add(rootNode);
                Console.WriteLine(DateTime.Now.ToString());
            }
            catch (Exception e)
            {
                throw new Exception("Error while building tree for Package", e);
            }

            return retNodes;
        }

        //Currently only revision attributes are populated in both OEM and supplier part cases.

        public class PackageStructureNode : AbstractCadexTreeNode<Occurrence>, INotifyPropertyChanged
        {
            private PackageLineItem lineItem;
            private PackageStructure occStruct;
            public PLMXMLDocument pkgPlmxmlDoc;
            private ProductRevision revision;
            private Product item;
            private CadexSupplierItem suppItem;
            private CadexSupplierItemRevision suppRev;
            private string _userName;
            public List<WorkflowStage> stages = new List<WorkflowStage>();

            public event PropertyChangedEventHandler PropertyChanged;

            void NotifiyPropertyChanged(string property)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(property));
            }

            public IList<CadExchange.Database.Entities.User> UserList
            {
                get
                {
                    return (IList<CadExchange.Database.Entities.User>) Session.getUsers();
                }
            }

            public override ImageSource ImageSource
            {
                get 
                {
                    return lineItem.PartOwner == PackageLineItem.OwnerType.OEM ? new BitmapImage(new Uri("/InfoExchange;component/images/Item.png", UriKind.Relative)) : new BitmapImage(new Uri("/InfoExchange;component/images/1416422795_suppliers.png", UriKind.Relative)); 
                }
            }

            public bool ReadOnly
            {
                get
                {
                    string prop = getAttr("checked_out");
                    return lineItem.PartOwner == PackageLineItem.OwnerType.OEM ? ( this.item.getAttributeValueAsString("subType").Equals("CE4_ExPkg") ? false : (prop != null && prop.Equals("Y"))) ? false : true : false;
                }
            }

            public bool HasCad
            {
                get
                {
                    string prop = getAttr("CE4_exportNode");
                    return prop!=null && prop.Equals("true");
                }
            }

            public string UserName
            {
                get
                {
                    return _userName;
                }

                set
                {
                    _userName = value;
                    NotifiyPropertyChanged("UserName");
                }
            }

            public PackageStructureNode(PLMXMLDocument doc, PackageLineItem line, PackageStructure structNode, Occurrence occ, ICadexTreeNode parent)
                : this(occ)
            {
                this.pkgPlmxmlDoc = doc;
                this.CadexParent = parent;
                this.lineItem = line;
                this.occStruct = structNode;
                this.setProperties();
                refreshWfInfo();
                foreach (ICadexTreeNode node in this.setChildren())
                {
                    this.addChild(node);
                }
                
            }

            public PackageStructureNode(Occurrence nodeObj)
                : base(nodeObj)
            {
                
            }



            public override void setProperties()
            {
                try
                {
                    if(lineItem.PartOwner == PackageLineItem.OwnerType.OEM)
                    {
                        populateAttrsForOEM();
                    }
                    else
                    {
                        populateAttrsForSupplier();
                    }
                }
                catch (Exception e)
                {
                    throw new Exception("Exception during setting properties.", e);
                }
            }

            private void populateAttrsForSupplier()
            {
                try
                {
                    if (suppItem == null || suppRev == null)
                    { setItemRev(); }
                    if (suppItem != null && suppRev != null)
                    {
                        //populateModelAttrs(this.suppItem);
                        populateModelAttrs(this.suppRev);
                    }
                    
                    populateModelAttrs(this.occStruct);
                }
                catch (Exception e)
                {
                    throw new Exception("Exception during setting properties for supplier.", e);
                }
            }

            private void populateAttrsForOEM()
            {
                try
                {
                    if (item == null || revision == null)
                    { setItemRev(); }
                    if (item != null && revision != null)
                    {
                        string objString = this.item.getAttributeValueAsString("productId") + "/" + this.revision.getAttributeValueAsString("revision") + ";" + this.item.getAttributeValueAsString("name");
                        this.attrMap.Add(CadexConstants.CADEX_OBJECT_STRING_ATTR, objString);
                        string objDesc = this.item.getDescription();
                        if (objDesc.Length == 0)
                            objDesc = this.revision.getDescription();
                        this.attrMap.Add(CadexConstants.CADEX_DESC_ATTR, objDesc);
                        foreach (KeyValuePair<string, string> pair in plmxmlUtil.addUserData(this.revision))
                        {
                            if (this.attrMap.ContainsKey(pair.Key))
                                this.attrMap.Remove(pair.Key);
                            this.attrMap.Add(pair.Key, pair.Value);
                        }
                    }

                    if (!this.occStruct.isNew)
                    {
                        foreach (KeyValuePair<string, string> pair in plmxmlUtil.addUserData(this.getObject()))
                        {
                            this.attrMap.Add(pair.Key, pair.Value);
                        }
                    }
                    else
                    {
                        populateModelAttrs(this.occStruct);
                    }
                }
                catch (Exception e)
                {
                    throw new Exception("Exception during populating properties for OEM part.", e);
                }
            }

            private void populateModelAttrs(ModelObject obj)
            {
                try
                {
                    foreach (ModelAttribute attr in obj.Attributes)
                    {
                        this.attrMap.Add(attr.Name, attr.Value);
                    }
                }
                catch (Exception e)
                {
                    throw new Exception("Exception during populating Model properties for " + obj.GetType(), e);
                }
            }

            public void refreshWfInfo()
            {
                try
                {
                    if (this.lineItem != null)
                    {
                        if (this.lineItem.itsWorkflows.Count == 1)
                        {
                            setStages( this.lineItem.itsWorkflows[0] );
                        }
                        else
                        {
                            if(CadexParent!=null && ((PackageStructureNode)CadexParent).lineItem!=null)
                            {
                                if (((PackageStructureNode)CadexParent).lineItem.itsWorkflows.Count == 1)
                                {
                                    setStages(((PackageStructureNode)CadexParent).lineItem.itsWorkflows[0]);
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new Exception("Exception while refreshing workflow information.", e);
                }
            }

            private void setStages(Workflow wf)
            {
                try
                {
                    stages.Clear();

                    if (wf.Stages.Count > 0)
                    {
                        foreach (WorkflowStage stage in wf.Stages)
                        {
                            stages.Add(stage);
                        }

                        UserName = stages.Find(this.FindAssignStage).responsibleUser.FirstName;
                    }
                }
                catch (Exception e)
                {
                    throw new Exception("Exception while setting workflow stages.", e);
                }
            }

            private void setItemRev()
            {
                if(lineItem.PartOwner == PackageLineItem.OwnerType.OEM)
                {
                    this.revision = (ProductRevision)pkgPlmxmlDoc.resolveId(this.occStruct.childPlmxmlId);
                    if (this.revision != null)
                        this.item = (Product)revision.getMasterObject();
                }
                else
                {
                    this.suppRev = Session.getObjectById<CadexSupplierItemRevision>(this.occStruct.childPlmxmlId);
                    if (this.suppRev != null)
                        this.suppItem = suppRev.ParentItem;
                }
            }

            public override List<ICadexTreeNode> setChildren()
            {
                List<ICadexTreeNode> allChilds = new List<ICadexTreeNode>();

                try
                {
                    List<PackageStructure> allChildLines = Session.getStructureChilds1(this.occStruct);

                    foreach (PackageStructure line in allChildLines)
                    {
                        PackageLineItem lineItem = Session.getLineFromStructure1(line,line.orgPackage);
                        
                        if( lineItem.PartOwner == PackageLineItem.OwnerType.OEM)
                        {
                            Occurrence occ = null;
                            if (!line.isNew)
                                occ = (Occurrence)pkgPlmxmlDoc.resolveId(line.childId);
                            PackageStructureNode newNode = new PackageStructureNode(pkgPlmxmlDoc, lineItem, line, occ, this);
                            allChilds.Add(newNode);
                        }
                        else
                        {
                            Occurrence occ = null;
                            PackageStructureNode newNode = new PackageStructureNode(pkgPlmxmlDoc, lineItem, line, occ, this);
                            allChilds.Add(newNode);
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new Exception("Exception during setting children.", e);
                }

                return allChilds;
            }

            private bool FindAssignStage(WorkflowStage stage)
            {
                if (stage.state == WorkflowState.Assigned)
                    return true;
                else
                    return false;
            }

            public void AssignUser(string userId)
            {
                try
                {
                    if (this.UserName!=null && this.UserName.Length > 0)
                        throw new Exception("The Object is already assigned to a user.");

                    validateAssign();

                    bool status = Session.Cadex_BeginRequest();
                    PackageLineItem editLine = Session.getObjectById<PackageLineItem>(Convert.ToString(this.lineItem.Id));
                    Workflow itsWorkflow = new Workflow(CadexSession.getSessionUser(), editLine, userId);
                    editLine.itsWorkflows.Add(itsWorkflow);
                    this.lineItem = editLine;
                    refreshWfInfo();
                    if(!status) Session.Cadex_EndRequest(false);
                }
                catch (Exception e)
                {
                    InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);throw;
                }
            }

            public void validateAssign()
            {
                try
                {
                    validateParent();
                    validateChilds(this);
                }
                catch (Exception e)
                {
                    InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);throw;
                }
            }

            // One Level Validation
            private void validateParent()
            {
                try
                {
                    PackageStructureNode parent = ((PackageStructureNode)CadexParent);
                    if (parent!=null && parent.UserName!=null && parent.UserName.Length > 0)
                        throw new Exception("The parent item is already assigned to a user.");
                }
                catch (Exception e)
                {
                    InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);throw;
                }
            }

            // Recursive Validation
            private void validateChilds(PackageStructureNode thisNode)
            {
                try
                {
                    foreach (PackageStructureNode node in thisNode.ChildrenList)
                    {
                        if (node.UserName != null && node.UserName.Length > 0)
                            throw new Exception("One Of its child is assigned to a user already.");

                        validateChilds(node);
                    }

                }
                catch (Exception e)
                {
                    InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);throw;
                }
            }

        }


    }
}

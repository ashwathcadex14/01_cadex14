﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Xps.Packaging;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using CadExchange.Common;
using CadExchange.Utils;
using CadExchange.Database.Entities;
using CadExchange.Tree.dataset;

namespace CadExchange.Tree
{
    public class SDatasetFileNode : AbstractCadexDatasetFileNode<CadexSupplierDatasetFile>
    {

        /// <summary>
        /// Gets or sets the source image.
        /// </summary>
        /// <value>The source.</value>
        public override ImageSource ImageSource
        {
            get
            {
                return new BitmapImage(new Uri("/InfoExchange;component/images/1413579964_12.File-32.png", UriKind.Relative));
            }
        }

        /// <summary>
        /// Gets or sets the source image.
        /// </summary>
        /// <value>The source.</value>
        public override string ExternalFile
        {
            get
            {
                return this.getAttr("absolute_path");
            }
        }

        public SDatasetFileNode(CadexSupplierDatasetFile file, string parentdir, SDatasetStructureNode parent)
            : base(file,parentdir,parent)
        {
            
        }

        public override void setProperties()
        {
            try
            {
                foreach (ModelAttribute attr in this.getObject().Attributes)
                {
                    this.attrMap.Add(attr.Name, attr.Value);
                }
            }
            catch (Exception e)
            {
                throw new Exception("Exception during setting properties.", e);
            }
        }

        public override List<ICadexTreeNode> setChildren()
        {
            List<ICadexTreeNode> files = new List<ICadexTreeNode>();

            try
            {

            }
            catch (Exception e)
            {
                throw new Exception("Exception during setting children.", e);
            }

            return files;
        }

        public override string Name
        {
            get
            {
                return this.getAttr(CadexConstants.CADEX_ORG_FILE_NAME_ATTR);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using CadExchange.Common;
using CadExchange.Utils;
using CadExchange.Database.Entities;
using CadExchange.Tree.dataset;

namespace CadExchange.Tree
{
    public class SDatasetStructureNode : AbstractCadexDatasetNode<CadexSupplierDataset>
    {
        /// <summary>
        /// Gets or sets the source image.
        /// </summary>
        /// <value>The source.</value>
        public override ImageSource ImageSource
        {
            get
            {
                return new BitmapImage(new Uri("/InfoExchange;component/images/Dataset_1.png", UriKind.Relative));
            }
        }

        public SDatasetStructureNode(CadexSupplierDataset dset, string rel, string parentdir, ICadexItemTreeNode parent)
            : base(dset,rel,parentdir,parent)
        {
            
        }

        public override void setProperties()
        {
            try
            {
                foreach (ModelAttribute attr in this.getObject().Attributes)
                {
                    this.attrMap.Add(attr.Name, attr.Value);
                }
            }
            catch (Exception e)
            {
                throw new Exception("Exception during setting properties.", e);
            }
        }

        public override List<ICadexTreeNode> setChildren()
        {
            List<ICadexTreeNode> files = new List<ICadexTreeNode>();

            try
            {
                foreach(CadexSupplierDatasetFile file in this.getObject().Files)
                {
                    SDatasetFileNode fileNode = new SDatasetFileNode(file, parentDir, this);
                    if (fileNode.IsPrimary)
                        _primaryFile = fileNode;
                    files.Add(fileNode);
                }
            }
            catch (Exception e)
            {
                throw new Exception("Exception during setting children.", e);
            }

            return files;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadExchange.Database.Entities;
using CadExchange.Tree;
using CadExchange.Utils;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using CadExchange.Common;
using InfoExPLMXMLSDK.com.infoex.plmxml.types;
using InfoExPLMXMLSDK.com.infoex.plmxml.generic;
using NHibernate;
using CadExchange.Database;
using CadExchange.Tree.dataset;
using System.IO;
using System.Drawing;

namespace CadExchange.Tree
{
    public class SupplierItemNode : AbstractCadexTreeNode<CadexSupplierItemRevision>, ICadexItemTreeNode, ICadexJTinfoNode
    {
        private PackageLineItem thisItem ;
        private PackageStructure thisOcc;
        public List<ICadexTreeNode> datasets = new List<ICadexTreeNode>();
        private CadexSupplierItem item;
        public string parentDir;
        public PLMXMLDocument pkgPlmxmlDoc;
        private bool _hasCad = false;
        private bool _hasRender = false;
        private string _cadType = "";

        private Image thumbnailImage;

        public Image ThumbnailImage
        {
            get
            {
                return thumbnailImage;
            }
        }

        public List<ICadexTreeNode> Datasets
        {
            get
            {
                return datasets;
            }
        }

        public int OwningPackage
        {
            get
            {
                return thisItem.Id;
            }
        }

        public PackageLineItem PkgLineItem
        {
            get
            {
                return thisItem;
            }
        }

        /// <summary>
        /// Gets or sets the source image.
        /// </summary>
        /// <value>The source.</value>
        public override ImageSource ImageSource
        {
            get
            {
                return new BitmapImage(new Uri("/InfoExchange;component/images/1416422795_suppliers.png", UriKind.Relative));
            }
        }

        public void lockDatasets(bool lockObj)
        {
            if (!ReadOnly)
            {
                if (HasCad)
                {
                    CommonUtil.setReadOnly(GetCadFile, !lockObj);
                }

                if (HasRendering)
                {
                    CommonUtil.setReadOnly(GetRenderFile, !lockObj);
                }
            }

            foreach (ICadexDatasetNode dSet in datasets)
            {
                dSet.lockObj(lockObj);
            }
        }

        public void lockItem(bool lockObj, ref List<ICadexItemTreeNode> checkOutNodes)
        {
            string objStr = this.getAttr(CadexConstants.CADEX_OBJECT_STRING_ATTR);

            //Not so strict now
            //if (Locked == lockObj)
            //{
            //    throw new Exception(lockObj ? "The object " + objStr + " is already locked." : "The object " + objStr + " is already unlocked.");
            //}
            //else
            //{
                var lines = Session.GetCurrentSession().QueryOver<PackageLineItem>().Where(p => p.itemId == (this.getAttr(CadexConstants.CADEX_ID_ATTR) + "|" + this.getAttr(CadexConstants.CADEX_REV_ID_ATTR))).List();
                
                foreach (PackageLineItem line in lines)
                {
                    if (line.isLocked != lockObj)
                    {
                        line.isLocked = lockObj;
                        line.LockedBy = lockObj ? ((CadExchange.Database.Entities.User)Session.getUserByUserId(CadexSession.getSessionUser())).UserId : "";
                        thisItem = line;
                        checkOutNodes.Add(this);
                    }
                    //Not so strict now
                    //else
                    //    throw new Exception(lockObj ? "The object " + objStr + " is already locked." : "The object " + objStr + " is already unlocked.");
                }
            //}

            foreach (ICadexItemTreeNode child in ChildrenList)
            {
                try
                {
                    child.lockItem(lockObj, ref checkOutNodes);
                }
                catch (Exception e)
                { InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);throw; }

            }
        }

        public CadexSupplierDataset addDatasets(string relation, string path, string name, string type, string desc)
        {
            CadexSupplierDataset newDset = null;

            try
            {
                PackageLineItem line = Session.getObjectById<PackageLineItem>(Convert.ToString(this.PkgLineItem.Id));
                newDset = new CadexSupplierDataset(relation, line);
                newDset.addAttributeToObject("Dataset", "object_name", name);
                newDset.addAttributeToObject("Dataset", "object_desc", desc);
                newDset.addAttributeToObject("Dataset", "object_type", type);
                newDset.addAttributeToObject("Dataset", "object_string", name);
                string itemDirectory = CadexSession.getSessionDir() + CadexConstants.CADEX_PATH_SEPARTOR + "local" + CadexConstants.CADEX_PATH_SEPARTOR + thisItem.itemId.Replace('|', '_');

                if (!Directory.Exists(itemDirectory))
                    Directory.CreateDirectory(itemDirectory);

                string newFile = itemDirectory + CadexConstants.CADEX_PATH_SEPARTOR + Path.GetFileName(path);

                if (File.Exists(newFile))
                    throw new Exception("A file with the same name is already available in the item directory. Please choose a file with different name.");
                File.Copy(path, newFile);
                CadexSupplierDatasetFile newDsetFile = new CadexSupplierDatasetFile(newDset, newFile);
                Session.persistObject(newDset);
                thisItem = line;
            }
            catch (Exception e)
            {
                throw new Exception("Exception while adding dataset.", e);
            }

            return newDset;
        }
        public CadexSupplierDataset remDatasets(DatasetStructureNode dSet)
        {
            CadexSupplierDataset newDset = null;

            try
            {

            }
            catch (Exception e)
            {
                throw new Exception("Exception while removing dataset.", e);
            }

            return newDset;
        }

        public SupplierItemNode(PLMXMLDocument doc, CadexSupplierItemRevision occ, PackageStructure structNode, ICadexTreeNode parentNode)
            : this(occ)
        {
            this.pkgPlmxmlDoc = doc;
            this.thisOcc = structNode;
            this.thisItem = Session.getLineFromStructure1(this.thisOcc, null);
            this.parentDir = CadexSession.getSessionDir() + CadexConstants.CADEX_PATH_SEPARTOR + "local";
            this.CadexParent = parentNode;
            setItemRev();
            this.setProperties();
            foreach (ICadexTreeNode node in this.setChildren())
            {
                this.addChild(node);
            }
            refreshDatasets();
        }

        public void refreshDatasets()
        {
            try
            {
                foreach (ICadexTreeNode node in this.setDatasets())
                {
                    this.datasets.Add(node);
                }
            }
            catch (Exception e)
            {
                InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);throw;
            }
        }

        public SupplierItemNode(CadexSupplierItemRevision nodeObj)
            : base(nodeObj)
        {

        }

        public List<PackageStructure> remPart(PackageStructure remThis)
        {
            List<PackageStructure> newLines = new List<PackageStructure>();
            try
            {
                List<PackageStructure> otherOccs = Session.getOtherOccurrences(thisOcc, false);

                foreach (PackageStructure otherOcc in otherOccs)
                {
                    newLines.AddRange(Session.getStructureChildWithItem(otherOcc,remThis.itemId));
                }
            }
            catch (Exception e)
            {
                throw new Exception("Exception while removing part.", e);
            }
            return newLines;
        }

        public PackageStructure OccNode
        {
            get
            {
                return thisOcc;
            }
        }

        public bool ReadOnly
        {
            get
            {
                return false;
            }
        }

        public bool HasCad
        {
            get
            {
                return _hasCad;
            }
        }

        public string CadType
        {
            get
            {
                return _cadType;
            }
        }

        public bool HasRendering
        {
            get
            {
                return _hasRender;
            }
        }

        public bool Locked
        {
            get
            {
                return thisItem.isLocked ;
            }
        }

        public string LockedBy
        {
            get
            {
                return thisItem.LockedBy;
            }
        }

        public string GetCadFile
        {
            get
            {
                return ((ICadexDatasetNode)this.datasets.Find(this.FindCadDataset)).PrimaryFile;
            }
        }

        public string GetRenderFile
        {
            get
            {
                ICadexTreeNode renderObj = this.datasets.Find(this.FindRenderDataset);
                if (renderObj != null)
                {
                    return ((ICadexDatasetNode)renderObj).PrimaryFile;
                }
                else
                {
                    return null;
                }

            }
        }

        public override void setProperties()
        {
            try
            {
                
                if (item != null)
                {
                    string objString = this.item.ItemId + "/" + this.getObject().RevId + ";" + this.item.getAttribute("Item","object_name");
                    this.attrMap.Add(CadexConstants.CADEX_OBJECT_STRING_ATTR, objString);
                    string objDesc = this.item.getAttribute("Item", "object_desc");
                    this.attrMap.Add(CadexConstants.CADEX_DESC_ATTR, objDesc);
                    this.attrMap.Add(CadexConstants.CADEX_ID_ATTR, this.item.ItemId);
                    this.attrMap.Add(CadexConstants.CADEX_REV_ID_ATTR, this.getObject().RevId);
                    this.attrMap.Add(CadexConstants.CADEX_TYPE_ATTR, this.item.getAttribute("Item", "object_type"));
                }
            }
            catch (Exception e)
            {
                throw new Exception("Exception during setting properties.", e);
            }
        }

        public List<PackageStructure> addPart(PackageLineItem addThis)
        {
            List<PackageStructure> newLines = new List<PackageStructure>();
            try
            {
                PackageStructure newNode = createNewNode(thisOcc, addThis);
                newLines.Add(newNode);


                if(addThis.PartOwner == PackageLineItem.OwnerType.Supplier)
                {
                    //List<PackageStructure> otherOccs = Session.getOtherOccurrences(thisOcc, true);

                    //foreach (PackageStructure otherOcc in otherOccs)
                    //{
                    //    newLines.Add(createNewNode(otherOcc, addThis));
                    //}
                }
                
            }
            catch (Exception e)
            {
                throw new Exception("Exception while adding part.", e);
            }
            return newLines;
        }

       

        public override List<ICadexTreeNode> setChildren()
        {
            List<ICadexTreeNode> allChilds = new List<ICadexTreeNode>();

            try
            {
                List<PackageStructure> allChildLines = Session.getStructureChilds1(thisOcc);

                foreach (PackageStructure line in allChildLines)
                {
                    PackageLineItem lineItem = Session.getLineFromStructure1(line, line.orgPackage);
                    

                    if (lineItem.PartOwner == PackageLineItem.OwnerType.OEM)
                    {
                        if(pkgPlmxmlDoc==null)
                            pkgPlmxmlDoc = plmxmlUtil.readDocument(line.orgPackage.getPlmXMLPath());

                        Occurrence occ = null;
                        if (!line.isNew)
                            occ = (Occurrence)pkgPlmxmlDoc.resolveId(line.childId);
                        ItemStructureNode newNode = new ItemStructureNode(pkgPlmxmlDoc, occ, line, lineItem, this);
                        allChilds.Add(newNode);
                    }
                    else
                    {
                        CadexSupplierItemRevision childItemRev = Session.getObjectById<CadexSupplierItemRevision>(line.childPlmxmlId);
                        SupplierItemNode newNode = new SupplierItemNode(pkgPlmxmlDoc, childItemRev, line, this);
                        allChilds.Add(newNode);
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("Exception during setting children.", e);
            }

            return allChilds;
        }

        private bool FindCadDataset(ICadexTreeNode line)
        {
            string cadType = CadexSession.getCadType(line.getAttr(CadexConstants.CADEX_TYPE_ATTR));
            if ( cadType !=null && cadType.Equals(this.CadType))
                return true;
            else
                return false;
        }

        private bool FindRenderDataset(ICadexTreeNode line)
        {
            if (CadexSession.isRender(line.getAttr(CadexConstants.CADEX_TYPE_ATTR)))
                return true;
            else
                return false;
        }

        private List<ICadexTreeNode> setDatasets()
        {
            List<ICadexTreeNode> allDatasets = new List<ICadexTreeNode>();

            try
            {
                foreach (CadexSupplierDataset sDset in thisItem.Datasets)
                {
                    SDatasetStructureNode dsetNode = new SDatasetStructureNode(sDset, sDset.Relation, "", this);
                    if (!_hasRender)
                        _hasRender = CadexSession.isRender(dsetNode.getAttr("object_type"));

                    if (!_hasCad)
                    {
                        string objType = dsetNode.getAttr("object_type");
                        _hasCad = CadexSession.isCad(objType);
                        _cadType = CadexSession.getCadType(objType);
                    }
                    allDatasets.Add(dsetNode);
                }
            }
            catch (Exception e)
            {
                throw new Exception("Exception during setting children.", e);
            }

            return allDatasets;
        }

        private void setItemRev()
        {
            item = this.getObject().ParentItem;
        }
        public string ItemId
        {
            get { return item.ItemId; }
        }

        public string Matrix
        {
            get
            {
                return ""; 
            }
        }


        public string ParentDir
        {
            get
            {
                return parentDir; 
            }
        }
    }
}

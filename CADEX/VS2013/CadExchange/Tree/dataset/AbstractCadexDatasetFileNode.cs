﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Xps.Packaging;
using CadExchange.Common;
using CadExchange.Utils;

namespace CadExchange.Tree.dataset
{
    public abstract class AbstractCadexDatasetFileNode<T> : AbstractCadexTreeNode<T>, ICadexDatasetFileNode
    {
        protected string parentDir;
        private XpsDocument xpsDoc;
        protected bool _isPrimary = false;

        public bool IsPrimary
        {
            get
            {
                return _isPrimary;
            }
        }

        public XpsDocument XpsDoc
        {
            get
            {
                return xpsDoc;
            }
        }

        /// <summary>
        /// Gets or sets the source image.
        /// </summary>
        /// <value>The source.</value>
        public abstract string ExternalFile
        {
            get;
        }

        public AbstractCadexDatasetFileNode(T file, string parentdir, ICadexDatasetNode parent)
            : this(file)
        {

            this.parentDir = parentdir;
            this.CadexParent = parent;
            this.setProperties();
            string orgFileName = this.getAttr(CadexConstants.CADEX_ORG_FILE_NAME_ATTR);
            try
            {
                xpsDoc = CommonUtil.generateXpsFile(ExternalFile);
            }
            catch(Exception e)
            {
                Console.WriteLine("Cannot create Xps document");
            }
            string fileExt = CadexSession.getFileType(this.CadexParent.getAttr(CadexConstants.CADEX_TYPE_ATTR));
            if (fileExt != null && fileExt.Length > 0 && orgFileName.EndsWith(fileExt))
            {
                _isPrimary = true;
            }
            
        }

        public AbstractCadexDatasetFileNode(T nodeObj)
            : base(nodeObj)
        {

        }

        public string Type
        {
            get
            {
                return this.getAttr(CadexConstants.CADEX_TYPE_ATTR);
            }
        }

        public string Relation
        {
            get
            {
                return this.getAttr(CadexConstants.CADEX_DATASET_RELATION);
            }
        }

        public bool IsEncrypted
        {
            get
            {
                return File.Exists(ExternalFile + ".encrypt") ? true : false;
            }
        }

        public virtual void decryptFile()
        {
            try
            {
                bool done = CommonUtil.DecryptFile1(ExternalFile + ".encrypt", ExternalFile);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual void encryptFile()
        {
            try
            {
                bool done = CommonUtil.EncryptFile1(ExternalFile, ExternalFile + ".encrypt");
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}

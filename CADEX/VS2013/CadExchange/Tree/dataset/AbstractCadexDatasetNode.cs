﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadExchange.Common;
using CadExchange.Utils;

namespace CadExchange.Tree.dataset
{
    public abstract class AbstractCadexDatasetNode<T> : AbstractCadexTreeNode<T> , ICadexDatasetNode
    {
        public string parentDir;
        protected ICadexDatasetFileNode _primaryFile;
        private ICadexItemTreeNode _parent;

        public bool Locked
        {
            get
            {
                return this._parent != null ? this._parent.Locked : false;
            }
        }

        public ICadexItemTreeNode Parent
        {
            get
            {
                return this._parent;
            }
        }

        public string LockedBy
        {
            get
            {
                return this._parent != null ? this._parent.LockedBy : "";
            }
        }

        public bool ReadOnly
        {
            get
            {
                string prop = getAttr("checked_out");
                return (prop != null && prop.Equals("Y")) ? false : true;
            }
        }

        public string PrimaryFile
        {
            get
            {
                if (_primaryFile != null && _primaryFile.ExternalFile.Length > 0)
                    return _primaryFile.ExternalFile;
                else
                    return "";
            }
        }

        public AbstractCadexDatasetNode(T dset, string rel, string parentdir, ICadexItemTreeNode parent)
            : this(dset)
        {
            this._parent = parent;
            this.setProperties();
            this.attrMap.Add(CadexConstants.CADEX_DATASET_RELATION, rel);
            this.parentDir = parentdir;
            foreach (ICadexTreeNode node in this.setChildren())
            {
                this.ChildrenList.Add(node);
            }
        }

        public AbstractCadexDatasetNode(T nodeObj)
            : base(nodeObj)
        {

        }

        public void lockObj(bool lockObj)
        {
            if (!ReadOnly)
            {
                foreach (ICadexDatasetFileNode fileNode in ChildrenList)
                {
                    DateTime currentFileTime = CommonUtil.getModified(fileNode.ExternalFile);

                    CommonUtil.setReadOnly(fileNode.ExternalFile, !lockObj);
                    string lasMod = fileNode.getAttr("last_mod_date");
                    if (lasMod != null && lasMod.Length > 0 && lockObj )
                    {
                        DateTime actualTime = DateTime.Parse(lasMod);
                        if(currentFileTime.Equals(actualTime))
                            CommonUtil.setModified(fileNode.ExternalFile, actualTime);
                    }                   
                }
            }
        }

        public string Type
        {
            get
            {
                return this.getAttr(CadexConstants.CADEX_TYPE_ATTR);
            }
        }

        public string Relation
        {
            get
            {
                return this.getAttr(CadexConstants.CADEX_DATASET_RELATION);
            }
        }
    }
}

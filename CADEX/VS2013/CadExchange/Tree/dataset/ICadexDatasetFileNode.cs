﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Xps.Packaging;

namespace CadExchange.Tree.dataset
{
    public interface ICadexDatasetFileNode : ICadexTreeNode
    {
        bool IsPrimary { get; }
        string Type { get; }
        string Relation { get; }
        string ExternalFile { get; }
        XpsDocument XpsDoc { get; }
        void encryptFile();
        void decryptFile();
    }
}

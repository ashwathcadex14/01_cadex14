﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadExchange.Tree.dataset
{
    public interface ICadexDatasetNode : ICadexTreeNode
    {
        bool Locked { get; }
        string LockedBy { get; }
        bool ReadOnly { get; }
        string PrimaryFile { get; }
        void lockObj(bool lockObj);
        string Type { get; }
        string Relation { get; }
        ICadexItemTreeNode Parent { get; }
    }
}

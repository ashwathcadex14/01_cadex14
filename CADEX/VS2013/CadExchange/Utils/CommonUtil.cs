﻿using CadExchange.Common;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using CadExchange.Tree;
using CadExchange.Database.Entities;
using System.Windows.Xps.Packaging;
using System.Runtime.InteropServices;
using System.IO.Packaging;
using Ionic.Zip;
using System.Security;
using System.Security.Cryptography;
using InfoExCommon.Utils;

namespace CadExchange.Utils
{
    public static class CommonUtil
    {
        /// <summary>
        /// Get the DataGridRow from the Grid
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="index"></param>
        /// <returns>DataGridRow</returns>
        public static DataGridRow GetDataGridRow(this DataGrid grid, int index)
        {
            DataGridRow row = (DataGridRow)grid.ItemContainerGenerator.ContainerFromIndex(index);
            if (row == null)
            {
                // May be virtualized, bring into view and try again.
                grid.UpdateLayout();
                grid.ScrollIntoView(grid.Items[index]);
                row = (DataGridRow)grid.ItemContainerGenerator.ContainerFromIndex(index);
            }
            return row;
        }

        /// <summary>
        /// To get the time stamp with the default time format
        /// </summary>
        /// <returns>DateTime</returns>
        public static string GetDefaultTimeStamp()
        {
            return DateTime.Now.ToString(CadexConstants.DEFAULT_TIME_STAMP);
        }

        /// <summary>
        /// To get the current time stamp in the fiven format
        /// </summary>
        /// <param name="timeFormat"></param>
        /// <returns>DateTime</returns>
        public static string GetTimeStamp(string timeFormat)
        {
            return DateTime.Now.ToString(timeFormat);
        }

        /// <summary>
        /// To check if the folder exists
        /// </summary>
        /// <param name="DirectoryName"></param>
        /// <returns>boolean</returns>
        public static bool IsFolderExists(string DirectoryName)
        {
            return Directory.Exists(DirectoryName);
        }

        public static System.Drawing.Image createThumbnail(ICadexItemTreeNode itemNode)
        {
            string cadFile = itemNode.GetCadFile;
            //System.Drawing.Image image = System.Drawing.Image.FromFile(cadFile);
            //System.Drawing.Image thumbNail = image.GetThumbnailImage(120, 120, () => false, IntPtr.Zero);
            //thumbNail.Save(System.IO.Path.ChangeExtension(cadFile, "thumb"));
            //FileStream fs = new FileStream(cadFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            //System.Drawing.Image retImage;
            //using (MemoryStream ms = new MemoryStream(fs.))
            //{
            //    retImage = System.Drawing.Image.FromStream(ms);
            //}
            int THUMB_SIZE = 512;
            System.Drawing.Bitmap thumbnail = null;

            if (cadFile != null && cadFile.Length > 0 && File.Exists(cadFile))
            {
                thumbnail = WindowsThumbnailProvider.GetThumbnail(
               cadFile, THUMB_SIZE, THUMB_SIZE, ThumbnailOptions.None);
            }

            return thumbnail;
        }

        /// <summary>
        /// Make the file read only or not
        /// </summary>
        /// <param name="fileName">string</param>
        /// <param name="set">bool</param>
        /// <returns>void</returns>
        public static void setReadOnly(string fileName, bool set)
        {
            var attr = File.GetAttributes(fileName);
            if(!set)
                attr = attr & ~FileAttributes.ReadOnly;
            else
                attr = attr | FileAttributes.ReadOnly;

            File.SetAttributes(fileName, attr);
        }

        /// <summary>
        /// Set tha last Write Time attribute of a file
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="dateModified"></param>
        /// <returns>void</returns>
        public static void setModified(string fileName, DateTime dateModified)
        {
            File.SetLastWriteTime(fileName, dateModified);
        }

        /// <summary>
        /// Get tha last Write Time attribute of a file
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="dateModified"></param>
        /// <returns>DateTime</returns>
        public static DateTime getModified(string fileName)
        {
            DateTime dateModified = File.GetLastWriteTime(fileName);
            return dateModified;
        }

        /// <summary>
        /// To Get the File from the specified folder whic matches the given pattern
        /// </summary>
        /// <param name="DirectoryName"></param>
        /// <param name="FilePattern"></param>
        /// <returns>It returns list of all files which matches the pattern from the given directory</returns>
        public static string[] GetFileFromFolder(string DirectoryName, string FilePattern)
        {
            string[] files = Directory.GetFiles(DirectoryName, FilePattern);
            return files.Length > 0 ? files : null;
        }

        /// <summary>
        /// To Get the File from the specified folder
        /// </summary>
        /// <param name="DirectoryName"></param>
        /// <returns>It returns list of all files from the given directory</returns>
        public static string[] GetFileFromFolder(string DirectoryName)
        {
            string[] files = Directory.GetFiles(DirectoryName);
            return files.Length > 0 ? files : null;
        }

        /// <summary>
        /// To encrypt the input string
        /// </summary>
        /// <param name="basestring"></param>
        /// <returns>It returns encrypted code</returns>
        public static string EncryptBase(string basestring)
        {
            byte[] strBytes = System.Text.Encoding.Unicode.GetBytes(basestring);
            string encryptString = Convert.ToBase64String(strBytes);
            return encryptString;
        }

        /// <summary>
        /// Shows the modern message dialog
        /// </summary>
        /// <param name="message"></param>
        /// <param name="title"></param>
        /// <param name="btn"></param>
        /// <returns>MessageBox result</returns>
        public static MessageBoxResult ShowModernMessageBox(string message,string title,MessageBoxButton btn)
        {
            var result = ModernDialog.ShowMessage(message, title, btn);
            return result;
        }

        /// <summary>
        /// Resizes the GridviewColumn size to support Auto
        /// </summary>
        /// <param name="column"></param>
        public static void ResizeGridViewColumn(GridViewColumn column)
        {
            if (double.IsNaN(column.Width))
            {
                column.Width = column.ActualWidth;
            }

            column.Width = double.NaN;
        }

        public static XpsDocument generateXpsFile(string orgFileName)
        {
            XpsDocument retDoc = null;

            try
            {
                string newXPSDocumentName = String.Concat(System.IO.Path.GetDirectoryName(orgFileName), "\\",
                              System.IO.Path.GetFileNameWithoutExtension(orgFileName), ".xps");

                if(File.Exists(newXPSDocumentName))
                {
                    retDoc = new XpsDocument(newXPSDocumentName, FileAccess.Read);
                }
                else if (orgFileName.EndsWith(".doc") || orgFileName.EndsWith(".docx"))
                {
                    retDoc = (XpsDocument)ConvertWordDocToXPSDoc(orgFileName, newXPSDocumentName);
                }
                else if (orgFileName.EndsWith(".xls") || orgFileName.EndsWith(".xlsx"))
                {
                    retDoc = (XpsDocument)ConvertExcelToXPSDoc(orgFileName, newXPSDocumentName);
                }
            }
            catch (Exception exp)
            {
                throw new Exception("Problem occured while converting xps document", exp);
            }
            

            return retDoc;
        }

        public static BitmapImage getCompareNodeImage(CompareEngine.Delta.DELTA_INFO deltaInfo)
        {
            switch (deltaInfo)
            {
                case CompareEngine.Delta.DELTA_INFO.NEW:
                    return new BitmapImage(new Uri("/InfoExchange;component/images/Item_New.png", UriKind.Relative));
                case CompareEngine.Delta.DELTA_INFO.REMOVED:
                    return new BitmapImage(new Uri("/InfoExchange;component/images/Item_Deleted.png", UriKind.Relative));
                case CompareEngine.Delta.DELTA_INFO.MODIFIED:
                    return new BitmapImage(new Uri("/InfoExchange;component/images/Item_Modified.png", UriKind.Relative));
                default:
                    return new BitmapImage(new Uri("/InfoExchange;component/images/Item.png", UriKind.Relative));
            }
        }

        public static LinearGradientBrush getNewColour()
        {
            Color startColor = (Color)ColorConverter.ConvertFromString("#6df40c");
            Color endColor = (Color)ColorConverter.ConvertFromString("#07c661");

            LinearGradientBrush newGrBrush = new LinearGradientBrush(startColor, endColor, 240);

            return newGrBrush;
        }

        public static LinearGradientBrush getRemovedColour()
        {
            Color startColor = (Color)ColorConverter.ConvertFromString("#febbbb");
            Color endColor = (Color)ColorConverter.ConvertFromString("#ff5c5c");

            LinearGradientBrush newGrBrush = new LinearGradientBrush(startColor, endColor, 240);

            return newGrBrush;
        }

        public static LinearGradientBrush getModifiedColour()
        {
            Color startColor = (Color)ColorConverter.ConvertFromString("#FFCC00");
            Color endColor = (Color)ColorConverter.ConvertFromString("#FFCC00");

            LinearGradientBrush newGrBrush = new LinearGradientBrush(startColor, endColor, 240);

            return newGrBrush;
        }

        /// <summary>
        /// This method takes a Word document full path and new XPS document full path and name
        /// and returns the new XpsDocument
        /// </summary>
        /// <param name="wordDocName"></param>
        /// <param name="xpsDocName"></param>
        /// <returns></returns>
        private static XpsDocument ConvertPptToXPSDoc(string pptName, string xpsDocName)
        {
            // Create a WordApplication and add Document to it
            Microsoft.Office.Interop.PowerPoint.Application
                wordApplication = new Microsoft.Office.Interop.PowerPoint.Application();
            //wordApplication.Presentations.Add(pptName);


            Microsoft.Office.Interop.PowerPoint.Presentation doc = wordApplication.ActivePresentation;
            // You must ensure you have Microsoft.Office.Interop.Word.Dll version 12.
            // Version 11 or previous versions do not have WdSaveFormat.wdFormatXPS option
            try
            {
                //doc.SaveAs(xpsDocName, Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsXPS);
                wordApplication.Quit();

                XpsDocument xpsDoc = new XpsDocument(xpsDocName, System.IO.FileAccess.Read);
                return xpsDoc;
            }
            catch (Exception e)
            {
                InfoExCommon.Utils.LoggerUtil.error(typeof(CommonUtil), e.Message, e); throw;
            }
        }

        /// <summary>
        /// This method takes a Word document full path and new XPS document full path and name
        /// and returns the new XpsDocument
        /// </summary>
        /// <param name="wordDocName"></param>
        /// <param name="xpsDocName"></param>
        /// <returns></returns>
        private static XpsDocument ConvertWordDocToXPSDoc(string wordDocName, string xpsDocName)
        {
            // Create a WordApplication and add Document to it
            Microsoft.Office.Interop.Word.Application
                wordApplication = new Microsoft.Office.Interop.Word.Application();
            wordApplication.Documents.Add(wordDocName);


            Microsoft.Office.Interop.Word.Document doc = wordApplication.ActiveDocument;
            // You must ensure you have Microsoft.Office.Interop.Word.Dll version 12.
            // Version 11 or previous versions do not have WdSaveFormat.wdFormatXPS option
            try
            {
                doc.SaveAs(xpsDocName, Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatXPS);
                wordApplication.Quit();

                XpsDocument xpsDoc = new XpsDocument(xpsDocName, System.IO.FileAccess.Read);
                return xpsDoc;
            }
            catch (Exception e)
            {
                InfoExCommon.Utils.LoggerUtil.error(typeof(CommonUtil), e.Message, e); throw;
            }
        }

        public static Dictionary<string, string> getPageArguments(string fragment)
        {
            Dictionary<string, string> pageArgs = new Dictionary<string, string>();
            try
            {
                string[] split = fragment.Split(new Char[]{'#'});
                
                for(int i = 0 ; i < split.Length ; i++)
                {
                    pageArgs.Add(split[i], split[++i]);
                }
            }
            catch (Exception e)
            {
                InfoExCommon.Utils.LoggerUtil.error(typeof(CommonUtil), e.Message, e); throw;
            }

            return pageArgs;
        }

        /// <summary>
        /// Gets the fragment from the uri
        /// </summary>
        /// <param name="uri">String of URI</param>
        /// <returns>Fragment containing the link arguments</returns>
        public static string getFragment(string uri)
        {
            int ashIndex = uri.IndexOf("#");
            if (ashIndex >= 0)
                return uri.Substring(ashIndex+1);
            else
                return null;
        }

        /// <summary>
        /// Gets the main page link from the uri
        /// </summary>
        /// <param name="uri">String of URI</param>
        /// <returns>xaml link</returns>
        public static string getLink(string uri)
        {
            int ashIndex = uri.IndexOf("#");
            if (ashIndex >= 0)
                return uri.Substring(0, ashIndex);
            else
                return uri;
        }

        private static XpsDocument ConvertExcelToXPSDoc(string excelName, string xpsDocName)
        {
            try
            {
                var excelApp = new Microsoft.Office.Interop.Excel.Application();
                excelApp.DisplayAlerts = false;
                excelApp.Visible = false;
                Microsoft.Office.Interop.Excel.Workbook excelWorkbook = excelApp.Workbooks.Open(excelName);
                excelWorkbook.ExportAsFixedFormat(Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypeXPS,
                Filename: xpsDocName,
                OpenAfterPublish: false);
                excelWorkbook.Close(false, null, null);
                excelApp.Quit();
                Marshal.ReleaseComObject(excelApp);
                excelApp = null;
                XpsDocument xpsPackage = new XpsDocument(xpsDocName, FileAccess.Read, CompressionOption.NotCompressed);
                return xpsPackage;
            }
            catch(Exception e)
            {
                InfoExCommon.Utils.LoggerUtil.error(typeof(CommonUtil), e.Message, e); throw;
            }
        }

        public static void ZipUp(string targetZip, string directory)
        {
            using (var zip = new ZipFile(targetZip))
            {
                zip.AddDirectory(directory, System.IO.Path.GetFileName(directory));
                zip.Save(targetZip);
            }
        }

        public static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            DirectoryInfo[] dirs = dir.GetDirectories();

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            // If the destination directory doesn't exist, create it. 
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            // If copying subdirectories, copy them and their contents to new location. 
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }

        ///<summary>
        /// Steve Lydford - 12/05/2008.
        ///
        /// Encrypts a file using Rijndael algorithm.
        ///</summary>
        ///<param name="inputFile"></param>
        ///<param name="outputFile"></param>
        [Obsolete("This method is deprecated and will be removed from next versions. Please Use EncryptFile1")]
        public static bool EncryptFile(string inputFile, string outputFile)
        {
            bool isEncrypted = false;
            FileStream fsIn = null;
            FileStream fsCrypt = null;
            try
            {
                string password = @"infoex2015"; // Your Key Here
                UnicodeEncoding UE = new UnicodeEncoding();
                byte[] key = UE.GetBytes(password);

                string cryptFile = outputFile;
                fsCrypt = new FileStream(cryptFile, FileMode.Create);

                RijndaelManaged RMCrypto = new RijndaelManaged();

                CryptoStream cs = new CryptoStream(fsCrypt,
                    RMCrypto.CreateEncryptor(key, key),
                    CryptoStreamMode.Write);

                fsIn = new FileStream(inputFile, FileMode.Open);

                int data;
                while ((data = fsIn.ReadByte()) != -1)
                    cs.WriteByte((byte) data);


                fsIn.Close();
                cs.Close();
                fsCrypt.Close();
                isEncrypted = true;
                System.IO.File.Copy(outputFile, inputFile, true);
                System.IO.File.Delete(outputFile);
            }
            catch (Exception e)
            {
                LoggerUtil.fatal(typeof (CommonUtil), "Unable to encrypt the file " + inputFile, e);
                isEncrypted = false;
                throw;
            }
            finally
            {
                if (fsIn != null)
                    fsIn.Close();
                if (fsCrypt != null)
                    fsCrypt.Close();
            }

            return isEncrypted;
        }
        ///<summary>
        /// Steve Lydford - 12/05/2008.
        ///
        /// Decrypts a file using Rijndael algorithm.
        ///</summary>
        ///
        ///<param name="inputFile"></param>
        ///<param name="outputFile"></param>
        [Obsolete("This method is deprecated and will be removed from next versions. Please Use DecryptFile1")]
        public static bool DecryptFile(string inputFile, string outputFile)
        {
            bool isDecrypted = false;
            FileStream fsOut = null;
            FileStream fsCrypt = null;
            try
            {
                string password = @"infoex2015"; // Your Key Here

                UnicodeEncoding UE = new UnicodeEncoding();
                byte[] key = UE.GetBytes(password);

                fsCrypt = new FileStream(inputFile, FileMode.Open);

                RijndaelManaged RMCrypto = new RijndaelManaged();

                CryptoStream cs = new CryptoStream(fsCrypt,
                    RMCrypto.CreateDecryptor(key, key),
                    CryptoStreamMode.Read);

                fsOut = new FileStream(outputFile, FileMode.Create);

                int data;
                while ((data = cs.ReadByte()) != -1)
                    fsOut.WriteByte((byte)data);

                fsOut.Close();
                cs.Close();
                fsCrypt.Close();
                isDecrypted = true;
                //System.IO.File.Copy(outputFile, inputFile, true);
                System.IO.File.Delete(inputFile);
            }
            catch(Exception e)
            {
                LoggerUtil.fatal(typeof(CommonUtil), "Unable to decrypt the file " + inputFile, e);
                isDecrypted = false;
                throw;
            }
            finally
            {
                if(fsOut!=null)
                    fsOut.Close();
                if (fsCrypt != null)
                    fsCrypt.Close();
            }

            return isDecrypted;
        }

        /// <summary>
        /// Checks if file is locked by other process
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        /// <summary>
        /// Checks if a file is encrypted
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static bool isFileEncrypted(string filePath)
        {
            FileInfo fi = new FileInfo(filePath);
            if (fi.Attributes.HasFlag(FileAttributes.Encrypted))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the plmxml files directory in the package
        /// </summary>
        /// <param name="pkgPath"></param>
        /// <returns></returns>
        public static string GetPLMXMLFilesDir(string pkgPath)
        {
            string[] dirs = Directory.GetDirectories(pkgPath);
            if (dirs != null && dirs.Length == 1)
                return dirs[0];

            return null;
        }

        /// <summary>
        /// Decrypts a file
        /// </summary>
        /// <param name="fileIn"></param>
        /// <param name="fileOut"></param>
        /// <returns></returns>
        public static bool DecryptFile1(string fileIn,
                string fileOut)
        {
            bool isDecrypted = false;
            FileStream fsIn = null;
            FileStream fsOut = null;
            CryptoStream cs = null;
            string Password = @"infoex2015";
            try
            {
                // First we are going to open the file streams 
                fsIn = new FileStream(fileIn,
                            FileMode.Open, FileAccess.Read);
                fsOut = new FileStream(fileOut,
                            FileMode.OpenOrCreate, FileAccess.Write);

                // Then we are going to derive a Key and an IV from
                // the Password and create an algorithm 
                PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                    new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 
                0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76});
                Rijndael alg = Rijndael.Create();

                alg.Key = pdb.GetBytes(32);
                alg.IV = pdb.GetBytes(16);

                // Now create a crypto stream through which we are going
                // to be pumping data. 
                // Our fileOut is going to be receiving the Decrypted bytes. 
                cs = new CryptoStream(fsOut,
                    alg.CreateDecryptor(), CryptoStreamMode.Write);

                // Now will will initialize a buffer and will be 
                // processing the input file in chunks. 
                // This is done to avoid reading the whole file (which can be
                // huge) into memory. 
                int bufferLen = 4096;
                byte[] buffer = new byte[bufferLen];
                int bytesRead;

                do
                {
                    // read a chunk of data from the input file 
                    bytesRead = fsIn.Read(buffer, 0, bufferLen);

                    // Decrypt it 
                    cs.Write(buffer, 0, bytesRead);

                } while (bytesRead != 0);

                //// close everything 
                //cs.Close(); // this will also close the unrelying fsOut stream 
                //fsIn.Close();
                
            }
            catch (Exception e)
            {
                LoggerUtil.fatal(typeof(CommonUtil), "Unable to decrypt the file " + fileIn, e);
                isDecrypted = false;
                throw;
            }
            finally
            {
                try
                {
                    if (fsIn != null)
                        fsIn.Close();
                    if (fsOut != null)
                        fsOut.Close();
                    //if (cs != null)
                    //    cs.Close();
                }
                catch (Exception exp1)
                {
                    LoggerUtil.fatal(typeof(CommonUtil), "Unable to close one of the streams after decryption ", exp1);
                    //Swallowing and reporting it to log alone
                    //throw;
                }
            }

            try
            {
                //System.IO.File.Copy(fileOut, fileIn, true);
                System.IO.File.Delete(fileIn);
                isDecrypted = true;
            }
            catch (Exception exp)
            {
                LoggerUtil.fatal(typeof(CommonUtil), "Unable to copy the decrypted file " + fileOut, exp);
                isDecrypted = false;
                throw;
            }

            return isDecrypted;
        }

        /// <summary>
        /// Encrypt the file
        /// </summary>
        /// <param name="fileIn"></param>
        /// <param name="fileOut"></param>
        public static bool EncryptFile1(string fileIn,
                string fileOut)
        {
            bool isEncrypted = false;
            FileStream fsIn = null;
            FileStream fsOut = null;
            CryptoStream cs = null;
            string Password = @"infoex2015";
            try
            {
                // First we are going to open the file streams 
                fsIn = new FileStream(fileIn,
                    FileMode.Open, FileAccess.Read);
                fsOut = new FileStream(fileOut,
                    FileMode.OpenOrCreate, FileAccess.Write);

                // Then we are going to derive a Key and an IV from the
                // Password and create an algorithm 
                PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                    new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 
                0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76});

                Rijndael alg = Rijndael.Create();
                alg.Key = pdb.GetBytes(32);
                alg.IV = pdb.GetBytes(16);

                // Now create a crypto stream through which we are going
                // to be pumping data. 
                // Our fileOut is going to be receiving the encrypted bytes. 
                cs = new CryptoStream(fsOut,
                    alg.CreateEncryptor(), CryptoStreamMode.Write);

                // Now will will initialize a buffer and will be processing
                // the input file in chunks. 
                // This is done to avoid reading the whole file (which can
                // be huge) into memory. 
                int bufferLen = 4096;
                byte[] buffer = new byte[bufferLen];
                int bytesRead;

                do
                {
                    // read a chunk of data from the input file 
                    bytesRead = fsIn.Read(buffer, 0, bufferLen);

                    // encrypt it 
                    cs.Write(buffer, 0, bytesRead);
                } while (bytesRead != 0);

                // close everything 

                // this will also close the unrelying fsOut stream
                //cs.Close();
                //fsIn.Close();
                
                
            }
            catch (Exception e)
            {
                LoggerUtil.fatal(typeof(CommonUtil), "Unable to encrypt the file " + fileIn, e);
                isEncrypted = false;
                throw;
            }
            finally
            {
                try
                {
                    if (fsIn != null)
                        fsIn.Close();
                    if (fsOut != null)
                        fsOut.Close();
                    //if (cs != null)
                    //    cs.Close();
                }
                catch (Exception exp1)
                {
                    LoggerUtil.fatal(typeof(CommonUtil), "Unable to close one of the streams after encryption ", exp1);
                    //Swallowing and reporting it to log alone
                    //throw;
                }
            }

            try
            {
                //System.IO.File.Copy(fileOut, fileIn, true);
                System.IO.File.Delete(fileIn);
                isEncrypted = true;
            }
            catch (Exception exp)
            {
                LoggerUtil.fatal(typeof(CommonUtil), "Unable to copy the encrypted file " + fileOut, exp);
                isEncrypted = false;
                throw;
            }

            return isEncrypted;
        } 
    }
}

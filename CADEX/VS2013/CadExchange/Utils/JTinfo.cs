﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadExchange.Tree;
using InfoExPLMXMLSDK.com.infoex.plmxml.types;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows;
using CadExchange.Database;
using CadExchange.Common;

namespace CadExchange.Utils
{
    class JTinfo
    {

        public JTinfo(ICadexTreeNode node, string type,string key, string value )
        {
            ObservableCollection<SingleNodeJTInfo> rootNodeChildInfo = new ObservableCollection<SingleNodeJTInfo>();
            captureChildRecursively(node, rootNodeChildInfo, 0);
            this.childNodeInfo = rootNodeChildInfo;
            this.type = type;
            this.key = key;
            this.value = value;
        }
        private string Type;
        private string Key;
        private string Value;
        private ObservableCollection<SingleNodeJTInfo> ChildNodeInfo;
        private int maxLevel;
        private string parentId;
        private string packageDir;
        private string ajtFilePath;
        private string jtFilepath;
        private string ajtFileName;
        private string jtFileName;

        public string JtFileName
        {
            get { return jtFileName; }
            set { jtFileName = value; }
        }
        
        
        public string AjtFileName
        {
            get { return ajtFileName; }
            set { ajtFileName = value; }
        }
        


        public string JtFilePath
        {
            get { return jtFilepath; }
            set { jtFilepath = value; }
        }

        public string AjtFilePath
        {
            get { return ajtFilePath; }
            set { ajtFilePath = value; }
        }

     
        

        public int MaxLevel        {
            get { return maxLevel; }
            set { maxLevel = value; }
        }
        

        public string type
        {
            get { return Type; }
            set { Type = value; }
        }



        public string key
        {
            get { return Key; }
            set { Key = value; }
        }
      

        public string value
        {
            get { return Value; }
            set { Value = value; }
        }

        public ObservableCollection<SingleNodeJTInfo> childNodeInfo
        {
            get { return ChildNodeInfo; }
            set { ChildNodeInfo = value; }
        }

        public string ParentId
        {
            get { return parentId; }
            set { parentId = value; }
        }
        public string PackageDir
        {
            get { return packageDir; }
            set { packageDir = value; }
        }
        
        private void captureChildRecursively(ICadexTreeNode node, ObservableCollection<SingleNodeJTInfo> childNodeInfo, int level)
        {
           SingleNodeJTInfo singleJTinfo = getSingleJtInfo(node,level);
          
            childNodeInfo.Add(singleJTinfo);
            if (node.ChildrenList.Count > 0)
            {
                level = level + 1;
                maxLevel = level;
                ObservableCollection<SingleNodeJTInfo> newChildNodeInfo = new ObservableCollection<SingleNodeJTInfo>();
                foreach (ICadexTreeNode ChildNode in node.ChildrenList)
                {
                    captureChildRecursively(ChildNode, newChildNodeInfo, level);
                    singleJTinfo.childNodeInfo = newChildNodeInfo;
                    
                }
            }
        }
        private SingleNodeJTInfo getSingleJtInfo(ICadexTreeNode node, int level)
        {
            Console.Write( node.GetType() );
            SingleNodeJTInfo info = null;
            if (node is ICadexJTinfoNode)
            {
                info = new SingleNodeJTInfo();

                ICadexJTinfoNode inode = (ICadexJTinfoNode)node;
                string itemId = inode.ItemId;
                string jtFile = null;
                string matrix = null;
                if (level != 0)
                {
                    jtFile = inode.GetRenderFile;
                    
                    matrix = inode.Matrix;
                    info.jtFilePath = jtFile;
                    info.matrix = matrix.Split(' ');
                    info.matrix[12] = Convert.ToString(Convert.ToDouble(info.matrix[12]) * 1000);
                    info.matrix[13] = Convert.ToString(Convert.ToDouble(info.matrix[13]) * 1000);
                    info.matrix[14] = Convert.ToString(Convert.ToDouble(info.matrix[14]) * 1000);
                }
                if( level == 0 )
                {
                    this.ParentId = itemId; ;
                    this.PackageDir = System.IO.Directory.GetDirectories(inode.ParentDir)[0] ; // gets the first folder of the directory
                    AjtFilePath = this.PackageDir + "\\" + this.parentId + ".ajt";
                    JtFilePath = this.PackageDir + "\\" + this.parentId + ".jt";
                    AjtFileName = this.parentId + ".ajt";
                    JtFileName = this.parentId + ".jt"; 
                }
                info.level = level;
                info.isPRT = node.ChildrenList.Count == 0 && jtFile != null; // if jtfile not present in prt, consider as asm
                info.itemId = itemId;
               

            }

            return info;
        }
        string nl = Environment.NewLine;

     public   void createJTfileAndView()
        {
            StringBuilder content = new StringBuilder();
            content.Append("#	DirectModel	ASCII	file	-	version	1" + nl + nl );
            writeRecursively( this.childNodeInfo, content );
            System.IO.File.WriteAllText( ajtFilePath, content.ToString() );


          

            // Use ProcessStartInfo class.
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = false;
            startInfo.UseShellExecute = false;
            startInfo.WorkingDirectory = this.PackageDir;
            startInfo.FileName = CadexSession.userDir + @"\"+Properties.Settings.Default.DatabaseDir+@"\JT_utility\asciitojt.exe";
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.Arguments = ".\\" + AjtFileName + " " + JtFileName;

            try
            {
                // Start the process with the info we specified.
                // Call WaitForExit and then the using-statement will close.
                using (Process exeProcess = Process.Start(startInfo))
                {
                    exeProcess.WaitForExit();
                }
                System.Diagnostics.Process.Start( JtFilePath );

            }
            catch( Exception e )
            {
                Utils.CommonUtil.ShowModernMessageBox(  e.Message, "Message", MessageBoxButton.OK);
                // Log error.
            }


        }
        void writeRecursively(ObservableCollection<SingleNodeJTInfo> nodeCollection, StringBuilder content)
        {
            foreach (SingleNodeJTInfo node in nodeCollection)
            {
                content.Append(node.level + "\t");
                content.Append((node.isPRT ? "PRT" : "ASM")  + "\t");
                content.Append( "\"" + node.itemId + "\"" + nl);
                if (node.level == 0)
                {
                    content.Append("ATTR_H" + "\t");
                    content.Append("Type=" + "\"" + this.type + "\"" + "\t");
                    content.Append("Key=" + "\"" + this.key + "\"" + "\t");
                    content.Append("Value=" + "\"" + this.value + "\"" + nl);
                }
                else
                {
                    if (node.isPRT)
                    {
                        string filename = node.jtFilePath.Split('\\')[node.jtFilePath.Split('\\').Length-1];
                        content.Append("File" + "\t" + "\".\\" + filename + "\"" + nl);
                    }
                    string[] mat = node.matrix;
                    content.Append("MATRIX [" + mat[0] + " " + mat[1] + " " + mat[2] + " " + mat[3] + "]" + nl);
                    content.Append("[" + mat[4] + " " + mat[5] + " " + mat[6] + " " + mat[7] + "]" + nl);
                    content.Append("[" + mat[8] + " " + mat[9] + " " + mat[10] + " " + mat[11] + "]" + nl);
                    content.Append("[" + mat[12] + " " + mat[13] + " " + mat[14] + " " + mat[15] + "]" + nl);
                    
                }
                content.Append(nl + nl);

                if ( node.childNodeInfo != null && node.childNodeInfo.Count != 0)
                {
                    writeRecursively(node.childNodeInfo, content);
                }
            }
        }
        void openJTfile()
        {

        }
    }

    class SingleNodeJTInfo
    {
        public string itemId { get; set; }
        public string[] matrix { get; set; }
        public string jtFilePath { get; set; }
        public  bool isPRT { get; set; }
        public int level { get; set; }         
        public ObservableCollection<SingleNodeJTInfo> childNodeInfo { get; set; }
    }

}

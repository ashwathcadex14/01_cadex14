﻿using CadExchange.Common;
using InfoExPLMXMLSDK.com.infoex.plmxml.types;
using InfoExPLMXMLSDK.com.infoex.plmxml.generic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadExchange.Tree;

namespace CadExchange.Utils
{
    public static class plmxmlUtil
    {
        /// <summary>
        /// Load the plmxml
        /// </summary>
        /// <param name="xmlFileUri"></param>
        /// <returns>Document</returns>
        public static PLMXMLDocument readDocument(string xmlFileUri) {
           return PLMXMLParser.readDocument(xmlFileUri);
	    }

        /// <summary>
        /// Get the xml file from the Extracted directory
        /// </summary>
        /// <param name="DirectoryName"></param>
        /// <returns>Full path with file name</returns>
        public static string GetPlmxmlFile(string DirectoryName)
        {
            if (CommonUtil.IsFolderExists(DirectoryName))
            {
                string[] files = CommonUtil.GetFileFromFolder(DirectoryName, CadexConstants.CADEX_XML_FILE_PATTERN);
                if(files != null)
                { return files[0]; }
            }
            return null;
        }

        /// <summary>
        /// Get the product revision tag with the specified the item id and revision id
        /// </summary>
        /// <param name="itemid">item_id|item_revision_id</param>
        /// <returns>ProductRevision</returns>
        public static ProductRevision getProductRevision(string plmxmlFile, string itemid)
        {
            if (plmxmlFile != null)
            {
                PLMXMLDocument doc = readDocument(plmxmlFile);
                if (doc != null)
                {
                    ProductRevision[] allProdRevs = doc.getElementsByClass<ProductRevision>("ProductRevision");
                    if (allProdRevs != null && allProdRevs.Length > 0)
                    {
                        foreach (ProductRevision prodRev in allProdRevs)
                        {
                            string revid = prodRev.revision;
                            Product revItem = prodRev.getMasterObject();
                            if (revItem != null)
                            {
                                string item_id = revItem.productId;
                                if (item_id != null)
                                {
                                    if ((item_id + "|" + revid).Equals(itemid))
                                        return prodRev;
                                    else
                                        continue;
                                }
                                else
                                    throw new Exception("Item id is null for the product " + revItem.id);
                            }
                            else
                                throw new Exception("Item for revision is null in the document" + prodRev.id);
                        }
                    }
                    else
                        throw new Exception("Unable to get the product revisions from file " + plmxmlFile);
                }
                else
                    throw new Exception("Unable to read plmxml document " + plmxmlFile);
            }
            else
                throw new Exception("Plmxml file " + plmxmlFile + " is not present or null.");

            return null;
        }

        /// <summary>
        /// Get the package id from the plmxml
        /// </summary>
        /// <param name="DirectoryName"></param>
        /// <returns>Package id</returns>
        public static string GetPackageId(string plmxmlFile)
        {
            string packageId = null;
            if (plmxmlFile != null) {
                PLMXMLDocument doc = readDocument(plmxmlFile);
                if(doc != null) {
                    AttributeBase[] usrDataObjList = (AttributeBase[])doc.getElementsByClass<AttributeBase>(CadexConstants.CADEX_PLMXML_USER_DATA);
                    foreach (AttributeBase objectB in usrDataObjList)
                    {
                        UserData data = (UserData)objectB;
                        if (CadexConstants.CADEX_PLMXML_USER_INFO_TYPE == data.getAttributeValueAsString(CadexConstants.CADEX_PLMXML_ATTR_TYPE))
                        {
                            UserDataElement[] values =  data.Items;
                            foreach (UserDataElement value in values)
                            {
                                if(CadexConstants.CADEX_CE4_PACKAGE_ID == value.getTitle()) {
                                    packageId = value.getValue();
                                }
                                if (CadexConstants.CADEX_CE4_PACKAGE_REV_ID == value.getTitle())
                                {
                                    packageId += "|" + value.getValue();
                                }
                            }
                            break;
                        }
                    }
                    
                }
            }
            return packageId;
        }

        public static Dictionary<string, string> addUserData( AttribOwnerBase tag )
	    {
            Dictionary<string, string> attrMap = new Dictionary<string, string>();
            AttributeBase[] userDatas = tag.getAttributes("UserData");
		    for (int i = 0; i < userDatas.Length; i++) 
		    {
			    UserDataElement[] userValues = ((UserData)userDatas[i]).Items;
			
			    for (int j = 0; j < userValues.Length; j++) {
                    attrMap.Add(userValues[j].getTitle(), userValues[j].getValue());
			    }
			
		    }

            return attrMap;
	    }
    }
}

BEGIN TRANSACTION;
CREATE TABLE hibernate_unique_key ( next_hi INT );
INSERT INTO `hibernate_unique_key` VALUES(6);
CREATE TABLE "WorkflowStage" (Id  integer primary key autoincrement, state INT, completed DATETIME, detailedInfo TEXT not null, text TEXT not null, mainWf_id INT not null, responsibleUser_id INT not null, constraint FK27A5984B28E0781 foreign key (mainWf_id) references "Workflow", constraint FK27A5984BEE615A42 foreign key (responsibleUser_id) references "User");
INSERT INTO `WorkflowStage` VALUES(1,1,'0001-01-01 00:00:00','The Part/Assy is assigned to infoexadmin','Assigned',1,1);
INSERT INTO `WorkflowStage` VALUES(2,2,'0001-01-01 00:00:00','Acknowledged by InfoEx','Acknowledged',1,1);
INSERT INTO `WorkflowStage` VALUES(3,3,'0001-01-01 00:00:00','In work with InfoEx','In Work',1,1);
CREATE TABLE "Workflow" (Id  integer primary key autoincrement, State INT, LastModifyingUser TEXT not null, wfItem_id INT not null, CurrentStage_id INT, constraint FKB2223E2BDE778699 foreign key (wfItem_id) references "PackageLineItem", constraint FKB2223E2BF8FB852E foreign key (CurrentStage_id) references "WorkflowStage");
INSERT INTO `Workflow` VALUES(1,3,'infoexadmin',1,3);
CREATE TABLE "User" (Id  integer primary key autoincrement, UserId TEXT not null unique, Password TEXT not null, FirstName TEXT not null, LastName TEXT not null, IsAdministrator BOOL default 0 , Status INT default 1 );
INSERT INTO `User` VALUES(1,'infoexadmin','a','InfoEx','Administrator',1,1);
CREATE TABLE "PackageStructure" (Id INT not null, parentId TEXT, childId TEXT not null, itemId TEXT not null, childPlmxmlId TEXT not null, isNew BOOL default 0 , isRemoved BOOL default 0 , orgPackage_id INT, primary key (Id), constraint FKE8958D777BB1F8D foreign key (Id) references "ModelObject", constraint FKE8958D7A58516B0 foreign key (orgPackage_id) references "Package");
INSERT INTO `PackageStructure` VALUES(11,'','id6','DIST0000000102|000','id167',0,0,1);
INSERT INTO `PackageStructure` VALUES(12,'id6','id9','MD124JMC-VOL2-Inventor|000','id12',0,0,1);
INSERT INTO `PackageStructure` VALUES(13,'id6','id32','MD124JMC-VOL1-Inventor|000','id35',0,0,1);
INSERT INTO `PackageStructure` VALUES(14,'id6','id49','MD124JMC-VOL3-Inventor|000','id52',0,0,1);
INSERT INTO `PackageStructure` VALUES(15,'id6','id66','MD124JMC-VOL1-ACAD|000','id69',0,0,1);
INSERT INTO `PackageStructure` VALUES(16,'id6','id83','MD124JMC-VOL2-ACAD|000','id86',0,0,1);
INSERT INTO `PackageStructure` VALUES(17,'id6','id100','MD1ANAXC-VOL1-ACAD|000','id103',0,0,1);
INSERT INTO `PackageStructure` VALUES(18,'id6','id117','MD1ANAXC-VOL2-ACAD|000','id120',0,0,1);
INSERT INTO `PackageStructure` VALUES(19,'id6','id134','MD1ANAXC-VOL1-DOC|000','id137',0,0,1);
INSERT INTO `PackageStructure` VALUES(20,'id6','id151','MD1ANAXC-VOL2-DOC|000','id154',0,0,1);
INSERT INTO `PackageStructure` VALUES(23,'','occ_17082015_115642049','SPart001|000',22,0,0,'');
INSERT INTO `PackageStructure` VALUES(33,'id6','occ_17082015_120328131','SPart001|000',22,1,0,1);
INSERT INTO `PackageStructure` VALUES(46,'','occ_17082015_175330213','SDE0000081863|000',45,0,0,'');
INSERT INTO `PackageStructure` VALUES(49,'','occ_17082015_175415109','SDE0000081862|000',48,0,0,'');
INSERT INTO `PackageStructure` VALUES(52,'','occ_17082015_175450756','DE0000081861|000',51,0,0,'');
INSERT INTO `PackageStructure` VALUES(55,'','occ_17082015_175516826','DE0000081860|000',54,0,0,'');
INSERT INTO `PackageStructure` VALUES(56,'occ_17082015_175330213','occ_17082015_182603358','SDE0000081862|000',48,1,0,'');
INSERT INTO `PackageStructure` VALUES(59,'','occ_17082015_182828254','SDE0000081860|000',58,0,0,'');
INSERT INTO `PackageStructure` VALUES(62,'','occ_17082015_182839661','SDE0000081861|000',61,0,0,'');
INSERT INTO `PackageStructure` VALUES(63,'occ_17082015_175330213','occ_17082015_182923721','SDE0000081860|000',58,1,0,'');
INSERT INTO `PackageStructure` VALUES(64,'occ_17082015_175330213','occ_17082015_182930596','SDE0000081861|000',61,1,0,'');
CREATE TABLE "PackageLineItem" (Id  integer primary key autoincrement, itemId TEXT not null, plmxmlId TEXT not null, isLocked BOOL default 0 , LockedBy TEXT, PartOwner INT, orgPackage_id INT, constraint FKC7EC445A58516B0 foreign key (orgPackage_id) references "Package");
INSERT INTO `PackageLineItem` VALUES(1,'DIST0000000102|000','id6',1,'infoexadmin',1,1);
INSERT INTO `PackageLineItem` VALUES(2,'MD124JMC-VOL2-Inventor|000','id9',1,'infoexadmin',1,1);
INSERT INTO `PackageLineItem` VALUES(3,'MD124JMC-VOL1-Inventor|000','id32',1,'infoexadmin',1,1);
INSERT INTO `PackageLineItem` VALUES(4,'MD124JMC-VOL3-Inventor|000','id49',1,'infoexadmin',1,1);
INSERT INTO `PackageLineItem` VALUES(5,'MD124JMC-VOL1-ACAD|000','id66',1,'infoexadmin',1,1);
INSERT INTO `PackageLineItem` VALUES(6,'MD124JMC-VOL2-ACAD|000','id83',1,'infoexadmin',1,1);
INSERT INTO `PackageLineItem` VALUES(7,'MD1ANAXC-VOL1-ACAD|000','id100',1,'infoexadmin',1,1);
INSERT INTO `PackageLineItem` VALUES(8,'MD1ANAXC-VOL2-ACAD|000','id117',1,'infoexadmin',1,1);
INSERT INTO `PackageLineItem` VALUES(9,'MD1ANAXC-VOL1-DOC|000','id134',1,'infoexadmin',1,1);
INSERT INTO `PackageLineItem` VALUES(10,'MD1ANAXC-VOL2-DOC|000','id151',1,'infoexadmin',1,1);
INSERT INTO `PackageLineItem` VALUES(11,'SPart001|000',22,1,'infoexadmin',0,'');
INSERT INTO `PackageLineItem` VALUES(12,'SDE0000081863|000',45,1,'infoexadmin',0,'');
INSERT INTO `PackageLineItem` VALUES(13,'SDE0000081862|000',48,0,'',0,'');
INSERT INTO `PackageLineItem` VALUES(14,'DE0000081861|000',51,0,'',0,'');
INSERT INTO `PackageLineItem` VALUES(15,'DE0000081860|000',54,0,'',0,'');
INSERT INTO `PackageLineItem` VALUES(16,'SDE0000081860|000',58,0,'',0,'');
INSERT INTO `PackageLineItem` VALUES(17,'SDE0000081861|000',61,0,'',0,'');
CREATE TABLE "Package" (Id  integer primary key autoincrement, PackageId TEXT not null, Path TEXT not null);
INSERT INTO `Package` VALUES(1,'DIST0000000102|000','C:\apps\Siemens\01_CADEX14\CADEX\VS2013\CadExchange\bin\Debug\workdir\Cadex_ExPkg_DIST0000000102_000_20150817115244715\Cadex_ExPkg_DIST0000000102_000.xml');
CREATE TABLE "ModelObject" (Id INT not null, primary key (Id));
INSERT INTO `ModelObject` VALUES(11);
INSERT INTO `ModelObject` VALUES(12);
INSERT INTO `ModelObject` VALUES(13);
INSERT INTO `ModelObject` VALUES(14);
INSERT INTO `ModelObject` VALUES(15);
INSERT INTO `ModelObject` VALUES(16);
INSERT INTO `ModelObject` VALUES(17);
INSERT INTO `ModelObject` VALUES(18);
INSERT INTO `ModelObject` VALUES(19);
INSERT INTO `ModelObject` VALUES(20);
INSERT INTO `ModelObject` VALUES(21);
INSERT INTO `ModelObject` VALUES(22);
INSERT INTO `ModelObject` VALUES(23);
INSERT INTO `ModelObject` VALUES(33);
INSERT INTO `ModelObject` VALUES(44);
INSERT INTO `ModelObject` VALUES(45);
INSERT INTO `ModelObject` VALUES(46);
INSERT INTO `ModelObject` VALUES(47);
INSERT INTO `ModelObject` VALUES(48);
INSERT INTO `ModelObject` VALUES(49);
INSERT INTO `ModelObject` VALUES(50);
INSERT INTO `ModelObject` VALUES(51);
INSERT INTO `ModelObject` VALUES(52);
INSERT INTO `ModelObject` VALUES(53);
INSERT INTO `ModelObject` VALUES(54);
INSERT INTO `ModelObject` VALUES(55);
INSERT INTO `ModelObject` VALUES(56);
INSERT INTO `ModelObject` VALUES(57);
INSERT INTO `ModelObject` VALUES(58);
INSERT INTO `ModelObject` VALUES(59);
INSERT INTO `ModelObject` VALUES(60);
INSERT INTO `ModelObject` VALUES(61);
INSERT INTO `ModelObject` VALUES(62);
INSERT INTO `ModelObject` VALUES(63);
INSERT INTO `ModelObject` VALUES(64);
CREATE TABLE "ModelAttribute" (Id  integer primary key autoincrement, Element TEXT not null, Name TEXT not null, Value TEXT, ParentObj_id INT not null, constraint FK70730EFF37A94D9D foreign key (ParentObj_id) references "ModelObject");
INSERT INTO `ModelAttribute` VALUES(1,'Item','owning_user','infoexadmin',21);
INSERT INTO `ModelAttribute` VALUES(2,'ItemRevision','owning_user','infoexadmin',22);
INSERT INTO `ModelAttribute` VALUES(3,'ItemRevision','object_name','Supplier Part',22);
INSERT INTO `ModelAttribute` VALUES(4,'ItemRevision','object_desc','New Supplier Part',22);
INSERT INTO `ModelAttribute` VALUES(5,'ItemRevision','object_string','SPart001/000;Supplier Part',22);
INSERT INTO `ModelAttribute` VALUES(6,'Item','owning_user','infoexadmin',44);
INSERT INTO `ModelAttribute` VALUES(7,'ItemRevision','owning_user','infoexadmin',45);
INSERT INTO `ModelAttribute` VALUES(8,'ItemRevision','object_name','S81863',45);
INSERT INTO `ModelAttribute` VALUES(9,'ItemRevision','object_desc','S81863',45);
INSERT INTO `ModelAttribute` VALUES(10,'ItemRevision','object_string','SDE0000081863/000;S81863',45);
INSERT INTO `ModelAttribute` VALUES(11,'Item','owning_user','infoexadmin',47);
INSERT INTO `ModelAttribute` VALUES(12,'ItemRevision','owning_user','infoexadmin',48);
INSERT INTO `ModelAttribute` VALUES(13,'ItemRevision','object_name','S81862',48);
INSERT INTO `ModelAttribute` VALUES(14,'ItemRevision','object_desc','S81862',48);
INSERT INTO `ModelAttribute` VALUES(15,'ItemRevision','object_string','SDE0000081862/000;S81862',48);
INSERT INTO `ModelAttribute` VALUES(16,'Item','owning_user','infoexadmin',50);
INSERT INTO `ModelAttribute` VALUES(17,'ItemRevision','owning_user','infoexadmin',51);
INSERT INTO `ModelAttribute` VALUES(18,'ItemRevision','object_name','S81861',51);
INSERT INTO `ModelAttribute` VALUES(19,'ItemRevision','object_desc','S81861',51);
INSERT INTO `ModelAttribute` VALUES(20,'ItemRevision','object_string','DE0000081861/000;S81861',51);
INSERT INTO `ModelAttribute` VALUES(21,'Item','owning_user','infoexadmin',53);
INSERT INTO `ModelAttribute` VALUES(22,'ItemRevision','owning_user','infoexadmin',54);
INSERT INTO `ModelAttribute` VALUES(23,'ItemRevision','object_name','S81860',54);
INSERT INTO `ModelAttribute` VALUES(24,'ItemRevision','object_desc','S81860',54);
INSERT INTO `ModelAttribute` VALUES(25,'ItemRevision','object_string','DE0000081860/000;S81860',54);
INSERT INTO `ModelAttribute` VALUES(26,'Item','owning_user','infoexadmin',57);
INSERT INTO `ModelAttribute` VALUES(27,'ItemRevision','owning_user','infoexadmin',58);
INSERT INTO `ModelAttribute` VALUES(28,'ItemRevision','object_name','S81860',58);
INSERT INTO `ModelAttribute` VALUES(29,'ItemRevision','object_desc','S81860',58);
INSERT INTO `ModelAttribute` VALUES(30,'ItemRevision','object_string','SDE0000081860/000;S81860',58);
INSERT INTO `ModelAttribute` VALUES(31,'Item','owning_user','infoexadmin',60);
INSERT INTO `ModelAttribute` VALUES(32,'ItemRevision','owning_user','infoexadmin',61);
INSERT INTO `ModelAttribute` VALUES(33,'ItemRevision','object_name','S81861',61);
INSERT INTO `ModelAttribute` VALUES(34,'ItemRevision','object_desc','S81861',61);
INSERT INTO `ModelAttribute` VALUES(35,'ItemRevision','object_string','SDE0000081861/000;S81861',61);
CREATE TABLE "DatasetConfiguration" (Id  integer primary key autoincrement, type TEXT not null, reference TEXT, ext TEXT not null, format TEXT);
INSERT INTO `DatasetConfiguration` VALUES(1,'Bitmap','Image','*.bmp','BINARY');
INSERT INTO `DatasetConfiguration` VALUES(2,'GIF','GIF_Reference','*.gif','BINARY');
INSERT INTO `DatasetConfiguration` VALUES(3,'Image','Image','*.*','BINARY');
INSERT INTO `DatasetConfiguration` VALUES(4,'JPEG','JPEG_Reference','*.jpg','BINARY');
INSERT INTO `DatasetConfiguration` VALUES(5,'MSExcel','excel','*.xls','BINARY');
INSERT INTO `DatasetConfiguration` VALUES(6,'MSExcelTemplate','template','*.xlt','BINARY');
INSERT INTO `DatasetConfiguration` VALUES(7,'MSExcelTemplateX','template','*.xltx','BINARY');
INSERT INTO `DatasetConfiguration` VALUES(8,'MSExcelX','excel','*.xlsx','BINARY');
INSERT INTO `DatasetConfiguration` VALUES(9,'MSPowerPoint','powerpoint','*.ppt','BINARY');
INSERT INTO `DatasetConfiguration` VALUES(10,'MSPowerPointX','powerpoint','*.pptx','BINARY');
INSERT INTO `DatasetConfiguration` VALUES(11,'MSWord','word','*.doc','BINARY');
INSERT INTO `DatasetConfiguration` VALUES(12,'MSWordX','word','*.docx','BINARY');
INSERT INTO `DatasetConfiguration` VALUES(13,'PDF','PDF_Reference','*.pdf','BINARY');
INSERT INTO `DatasetConfiguration` VALUES(14,'Text','Text','*.txt','TEXT');
INSERT INTO `DatasetConfiguration` VALUES(15,'Zip','ZIPFILE','*.zip','BINARY');
INSERT INTO `DatasetConfiguration` VALUES(16,'ACADDWG','DWG','*.dwg','BINARY');
INSERT INTO `DatasetConfiguration` VALUES(17,'DirectModel','JTPART','*.jt','BINARY');
INSERT INTO `DatasetConfiguration` VALUES(18,'SE Assembly','asm','*.asm','BINARY');
INSERT INTO `DatasetConfiguration` VALUES(19,'SE Part','par','*.par','BINARY');
CREATE TABLE "CadexSupplierItemRevision" (Id INT not null, RevId TEXT not null, isLatest BOOL default 0 , ParentItem_id INT not null, primary key (Id), constraint FKF004039177BB1F8D foreign key (Id) references "ModelObject", constraint FKF00403912C94B74F foreign key (ParentItem_id) references "CadexSupplierItem");
INSERT INTO `CadexSupplierItemRevision` VALUES(22,000,1,21);
INSERT INTO `CadexSupplierItemRevision` VALUES(45,000,1,44);
INSERT INTO `CadexSupplierItemRevision` VALUES(48,000,1,47);
INSERT INTO `CadexSupplierItemRevision` VALUES(51,000,1,50);
INSERT INTO `CadexSupplierItemRevision` VALUES(54,000,1,53);
INSERT INTO `CadexSupplierItemRevision` VALUES(58,000,1,57);
INSERT INTO `CadexSupplierItemRevision` VALUES(61,000,1,60);
CREATE TABLE "CadexSupplierItem" (Id INT not null, ItemId TEXT not null unique, primary key (Id), constraint FK4F4FA5A077BB1F8D foreign key (Id) references "ModelObject");
INSERT INTO `CadexSupplierItem` VALUES(21,'SPart001');
INSERT INTO `CadexSupplierItem` VALUES(44,'SDE0000081863');
INSERT INTO `CadexSupplierItem` VALUES(47,'SDE0000081862');
INSERT INTO `CadexSupplierItem` VALUES(50,'DE0000081861');
INSERT INTO `CadexSupplierItem` VALUES(53,'DE0000081860');
INSERT INTO `CadexSupplierItem` VALUES(57,'SDE0000081860');
INSERT INTO `CadexSupplierItem` VALUES(60,'SDE0000081861');
CREATE TABLE "CadexSupplierDatasetFile" (Id INT not null, ParentDataset_id INT not null, primary key (Id), constraint FK8137E95B77BB1F8D foreign key (Id) references "ModelObject", constraint FK8137E95BA592DD67 foreign key (ParentDataset_id) references "CadexSupplierDataset");
CREATE TABLE "CadexSupplierDataset" (Id INT not null, Relation TEXT not null, ParentRevision_id INT, ParentPkgRevision_id INT, primary key (Id), constraint FK84F0704D77BB1F8D foreign key (Id) references "ModelObject", constraint FK84F0704D14364C82 foreign key (ParentRevision_id) references "CadexSupplierItemRevision", constraint FK84F0704D4F06F5D0 foreign key (ParentPkgRevision_id) references "PackageLineItem");
;
;
;
;
;
;
;
;
COMMIT;

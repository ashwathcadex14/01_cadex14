﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompareEngine.Model.Interfaces;

namespace CompareEngine.Compare.Cadex
{
   public class AttributeNode
	{
		String attrName = "" ;
		String attrValueL = "" ;
		String attrValueR = "" ;
		String attrValueA = "" ;
		int attrType ;
		public const int BOM_ATTR_TYPE = 1;
		public const int ITEM_ATTR_TYPE = 2;
		public const int REV_ATTR_TYPE = 3;
        public bool isSame = false;
        //List<AttributeNode<T> childNodes = new List<AttributeNode<T>>();
		
		private AttributeNode(String attrName, CompareNode obj, int type) : this(attrName,obj.getLeftNode(),obj.getRightNode(),obj.getAncestorNode()){
			
			this.attrType = type;			
            //setAttributes(obj);
		}
		
        //private void setAttributes(CompareNode obj)
        //{
        //    switch(this.attrType)
        //    {
        //        case AttributeNode.BOM_ATTR_TYPE:
        //            //setAttribute( obj.getLeftNode() , obj.getRightNode(), obj.getAncestorNode());
        //            break;
        //        case ITEM_ATTR_TYPE:
        //        {
        //            //setAttribute( getLineItem(obj.getLeftNode()) , getLineItem(obj.getRightNode()), getLineItem(obj.getAncestorNode()));
        //            break;
        //        }
        //        case REV_ATTR_TYPE:
        //        {
        //            //setAttribute( getLineRev(obj.getLeftNode()) , getLineRev(obj.getRightNode()), getLineRev(obj.getAncestorNode()));
        //            break;
        //        }
        //        default:
        //            break;
        //    }
        //}

        //private void setAttribute(ICadexComparable iCadexGenericObject, ICadexComparable iCadexGenericObject2, ICadexComparable iCadexGenericObject3)
        //{
        //    HashSet<String> globNames = new HashSet<String>(); 
			
        //    if(iCadexGenericObject != null)
        //        globNames.UnionWith(((ICadexGenericObject) iCadexGenericObject).getAttrNames());
        //    if(iCadexGenericObject2 != null)
        //        globNames.UnionWith(((ICadexGenericObject)iCadexGenericObject2).getAttrNames());
        //    if(iCadexGenericObject3 != null)
        //        globNames.UnionWith(((ICadexGenericObject)iCadexGenericObject3).getAttrNames());
			
        //    foreach(String globString in globNames)
        //    {
        //        this.childNodes.Add( new AttributeNode(globString, iCadexGenericObject, iCadexGenericObject2,iCadexGenericObject3) );
        //    }
        //}
		
		private AttributeNode(String attrName) {
			this.attrName = attrName;
		}

        public AttributeNode(string attrName, string leftValue, string rightValue, string ancestorValue) : this(attrName)
        {
            if (leftValue.Equals(rightValue))
                isSame = true;

            attrValueL = leftValue;
            attrValueR = rightValue;
            attrValueA = ancestorValue;
        }

        private AttributeNode(String attrName, ICadexComparable iCadexGenericObject, ICadexComparable iCadexGenericObject2, ICadexComparable iCadexGenericObject3)
            : this(attrName)
        {
			
			this.attrValueL = iCadexGenericObject != null ? ((ICadexGenericObject) iCadexGenericObject).getAttr(attrName) : "" ;
			this.attrValueR = iCadexGenericObject2 != null ? ((ICadexGenericObject) iCadexGenericObject2).getAttr(attrName) : "" ;
			this.attrValueA = iCadexGenericObject3 != null ? ((ICadexGenericObject) iCadexGenericObject3).getAttr(attrName) : "" ;
		}

		public String getAttrName() {
			return attrName;
		}

		public String getAttrValueL() {
			return attrValueL;
		}

		public String getAttrValueR() {
			return attrValueR;
		}

		public String getAttrValueA() {
			return attrValueA;
		}
		
		
		public override bool Equals(Object arg0) {
			if( arg0 != null && arg0 is AttributeNode )
				return ((AttributeNode)arg0).getAttrName().Equals( this.attrName ); 
			else
				return false ;
		}

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
		
        //private ICadexGenericObject getLineItem(ICadexGenericLineObject line)
        //{
        //    if( line != null && line.getLineRev() != null )
        //    {
        //        return (ICadexGenericObject) line.getLineRev().getRevItem();
        //    }
        //    else
        //        return null;	
        //}
		
        //private ICadexGenericObject getLineRev(ICadexGenericLineObject line)
        //{
        //    if( line != null )
        //    {
        //        return (ICadexGenericObject) line.getLineRev();
        //    }
        //    else
        //        return null;	
        //}

        //public List<AttributeNode> getChildNodes() {
        //    return childNodes;
        //}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompareEngine.Model.Interfaces;

namespace CompareEngine.Compare.Cadex
{
    class CadexDifferencer : Differencer
    {
			int nodeTraverse = 0 ;
			CompareNode currentNode ;
			CompareNode parentNode ;
			Dictionary<String, CompareNode> compareNodes = new Dictionary<String, CompareNode>();
            private CompareNode compareNode = null;
            private int seqNo = 0;

            public CompareNode getCompareNode()
            {
                return compareNode;
            }

			protected override Object visit(Object parent, int description, Object ancestor, Object left, Object right) {
				MyDiffNode diff = new MyDiffNode((IDiffContainer) parent, description, (ITypedElement) ancestor, (ITypedElement)left, (ITypedElement)right);
				String nodeUid = getNodeUid(ancestor, left, right);
				
				foreach(KeyValuePair<String, CompareNode> entry in compareNodes)
				{
					if( entry.Key.StartsWith( nodeUid ) && ((entry.Value.leftNode ==null) || (entry.Value.rightNode ==null)))
					{
						entry.Value.setDifNode(diff);
					}
				}
				
				return diff;
			}
			
			
			protected override bool contentsEqual(Object input1, Object input2) {
				bool compareResult = base.contentsEqual(input1, input2);
				return compareResult;
			}
			
			
			protected override Object[] getChildren(Object input) 
			{
				Object[] objs = base.getChildren(input);

				if(nodeTraverse==0)
				{
					currentNode = new CompareNode((ICadexComparable) input, null, null,++seqNo);  
					
					if(compareNode == null)
						compareNode = this.currentNode;
					
					nodeTraverse++;
				}
				else if(nodeTraverse==1)
				{
					if(currentNode != null)
                        currentNode.setRightNode((ICadexComparable)input);
					
					nodeTraverse++;
				}
				else
				{
					if(currentNode != null)
                        currentNode.setLeftNode((ICadexComparable)input);

                    currentNode.setAttributes();

					if(parentNode != null && !currentNode.nodeUid.Equals( parentNode.nodeUid ))	
					{
						parentNode.addChildNode( currentNode );
					}

                    
					
					if(currentNode.childAvailable && currentNode.leftNode != null && currentNode.rightNode != null)
						parentNode = currentNode;
					else
						revertParent();
					
					
					compareNodes.Add( currentNode.nodeUid + "|" + currentNode.seqNo , currentNode);
					
					if(currentNode.leftNode == null || currentNode.rightNode == null )
					{
						createDifferenceCompNodes();
					}

                    currentNode.performDeepCompare();
					nodeTraverse-=2; 
				}
				
				return objs;
			}

            private void revertParent()
            {
                if (parentNode != null)
                {
                    ICadexComparable ancestorNode = parentNode
                            .getAncestorNode();
                    ICadexComparable leftNode = parentNode.getLeftNode();
                    ICadexComparable rightNode = parentNode
                            .getRightNode();
                    int leftSize = leftNode != null ? leftNode.getChildrenCount() : 0;
                    int rightSize = rightNode != null ? rightNode.getChildrenCount() : 0;
                    int ancestorSize = ancestorNode != null ? ancestorNode.getChildrenCount() : 0;
                    int allSize = leftSize > rightSize ? leftSize > ancestorSize ? leftSize
                            : ancestorSize > rightSize ? ancestorSize
                                    : rightSize
                            : rightSize > ancestorSize ? rightSize
                                    : ancestorSize;
                    if ((parentNode.getChildCount() - parentNode.deletions + parentNode.additions) == allSize)
                    {
                        parentNode = parentNode.getParentCompNode();
                        revertParent();
                    }
                }
            }
			
			private void createDifferenceCompNodes()
			{
				if(currentNode.leftNode != null)
					addCompareChild(currentNode, CompareNode.LEFT_SIDE);
				else
                    addCompareChild(currentNode, CompareNode.RIGHT_SIDE);
			}
			
			private void addCompareChild(CompareNode curNode, int side)
			{
				Object[] lines = side == CompareNode.LEFT_SIDE ? curNode.getLeftNode().getChildren() : curNode.getRightNode().getChildren();
				
				if(lines!=null)
				{
					foreach(Object line in  lines)
					{
                        CompareNode newNode = new CompareNode(null, side == CompareNode.LEFT_SIDE ? (ICadexComparable)line : null, side == CompareNode.RIGHT_SIDE ? (ICadexComparable)line : null, ++seqNo);
						newNode.setNodeType();
						newNode.performDeepCompare();
						curNode.addChildNode( newNode );
						compareNodes.Add( newNode.getNodeUid() + "|" + newNode.getSeqNo() , newNode);
						addCompareChild(newNode, side);
					}
				}
			}
			
			private String getNodeUid(Object ancestor, Object left, Object right)
			{
				String retUid = "" ;
				
				if( ancestor != null && ancestor is ITypedElement  )
					retUid = ((ITypedElement)ancestor).getName();
				
				if( retUid.Length == 0 && left != null && left is ITypedElement  )
					retUid = ((ITypedElement)left).getName();
				
				if( retUid.Length == 0 && right != null && right is ITypedElement  )
					retUid = ((ITypedElement)right).getName();
				
				return retUid;
			}
			
    }
}

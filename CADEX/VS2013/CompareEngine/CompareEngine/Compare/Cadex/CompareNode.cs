﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompareEngine.Model.Interfaces;
using CompareEngine.Delta;

namespace CompareEngine.Compare.Cadex
{
    public class CompareNode
    {
        private ICadexComparable ancestorNode;
        public ICadexComparable leftNode;
        public ICadexComparable rightNode;
        private MyDiffNode difNode;
        private bool isDifferent;
        public String nodeUid = "";
        public int seqNo = 0;
        public int additions = 0;
        public int deletions = 0;
        private int nodeType = 0;
        public static int COMPARE_LINE_NODE = 1;
        public static int COMPARE_OTHER_NODE = 2;
        private CompareNode parentCompNode = null;
        private List<CompareNode> childNodes = new List<CompareNode>();
        private List<AttributeNode> attrNodes = new List<AttributeNode>();
        private List<DatasetNode> datasetNodes = new List<DatasetNode>();
        public bool childAvailable = false;
        public static int LEFT_SIDE = 1;
        public static int RIGHT_SIDE = 2;

        public CompareNode(ICadexComparable ancestorNode, ICadexComparable leftNode, ICadexComparable rightNode, int seqno)
        {
			this.leftNode = leftNode;
			this.rightNode = rightNode;
			this.setAncestorNode(ancestorNode);
            this.seqNo = seqno;
			
			if(this.ancestorNode != null)
				this.nodeUid = this.ancestorNode.ToString();
		}

        public MyDiffNode getDifNode()
        {
            return difNode;
        }

        public void performDeepCompare()
        {

            if (leftNode == null && rightNode != null)
            {
                rightNode.setChangeInfo(DELTA_INFO.NEW);
            }
            else if (leftNode != null && rightNode == null)
            {
                leftNode.setChangeInfo(DELTA_INFO.REMOVED);
            }
            else
            {
                rightNode.setChangeInfo(leftNode.doDeepCompare(rightNode) ? DELTA_INFO.MODIFIED : DELTA_INFO.NO_CHANGE);
            }
        }

        public DELTA_INFO getDelta()
        {
            if (leftNode == null && rightNode != null)
            {
                return rightNode.changeInfo();
            }
            else if (leftNode != null && rightNode == null)
            {
                return leftNode.changeInfo();
            }
            else
            {
                return rightNode.changeInfo();
            }
        }

        public void setDifNode(MyDiffNode difNode)
        {
            this.difNode = difNode;
            if (this.difNode != null)
                this.isDifferent = true;
        }

        public ICadexComparable getLeftNode()
        {
            return leftNode;
        }

        public ICadexComparable getRightNode()
        {
            return rightNode;
        }

        public List<CompareNode> getChildNodes()
        {
            if(this.nodeType == COMPARE_LINE_NODE)
		    {
			    List<CompareNode> childNodes1 = new List<CompareNode>();
			    foreach(CompareNode node in this.childNodes)
			    {
				    if(node.nodeType == COMPARE_LINE_NODE)
					    childNodes1.Add( node );
			    }
			    return childNodes1;
		    }
		    else
			    return childNodes;
        }

        public List<CompareNode> getChildDetailNodes() {

		    if(this.nodeType == COMPARE_LINE_NODE)
		    {
			    List<CompareNode> childNodes1 = new List<CompareNode>();
			    foreach(CompareNode node in this.childNodes)
			    {
				    if(node.nodeType == COMPARE_OTHER_NODE)
					    childNodes1.Add( node );
			    }
			    return childNodes1;
		    }
		    else
			    return null;
		
	    }

        public void shiftHierarchy()
	    {
		    CompareNode itemNode = null;
		    List<CompareNode> detailChilds = (List<CompareNode>) this.getChildDetailNodes();
		    if(detailChilds.Count == 1)
		    {
			    foreach(CompareNode revChild in detailChilds[0].childNodes )
			    {
				    if(revChild.getNode().getType().Equals( "Item" ))
				    {
					    itemNode = revChild;
					    break;
				    }
			    }
			
			    if(itemNode!=null)
			    {
				    detailChilds[0].childNodes.Remove( itemNode );
				    itemNode.childNodes.Add( detailChilds[0] );
				    this.childNodes.Remove( detailChilds[0] );
				    this.childNodes.Add( itemNode );
			    }
		    }
	    }

        public ICadexComparable getAncestorNode()
        {
            return ancestorNode;
        }

        public int getChildCount()
        {
            return childNodes.Count;
        }

        public void addChildNode(CompareNode childNode)
        {
            this.childNodes.Add(childNode);

            if (childNode.leftNode == null)
            {
                this.additions++;
            }
            else if (childNode.rightNode == null)
            {
                this.deletions++;
            }

            childNode.setParentCompNode(this);
        }

        //		@Override
        //		public String toString() {
        //			return ancestorNode + " <--> " + leftNode + " <--> " + rightNode ;
        //		}

        public void setLeftNode(ICadexComparable leftNode)
        {
            this.leftNode = leftNode;
            this.setChildAvailable(this.leftNode);

            if (this.leftNode != null)
            {
                if (this.nodeUid.Length == 0)
                {
                    this.nodeUid = this.leftNode.ToString();
                }

                setNodeType();
            }


        }

        private void populateDatasets(ICadexGenericLineObject leftNode2, int side)
        {
            ICadexGenericRevision rev = leftNode2.getLineRev();
            if (rev != null)
            {
                DatasetNode node = new DatasetNode(side == LEFT_SIDE ? "LEFT" : side == RIGHT_SIDE ? "RIGHT" : "ANCESTOR");
                node.setDsets(rev.getAttaches());
                this.datasetNodes.Add(node);
            }
        }

        public void setRightNode(ICadexComparable rightNode)
        {
            this.rightNode = rightNode;
            this.setChildAvailable(this.rightNode);

            if (this.rightNode != null)
            {
                if (this.nodeUid.Length == 0)
                {
                    this.nodeUid = this.rightNode.ToString();
                }

                setNodeType();
            }
        }

        public bool isChildAvailable()
        {
            return childAvailable;
        }

        public void setChildAvailable(Object input)
        {

            Object[] objs = this.getChildren(input);

            if (!this.childAvailable)
                this.childAvailable = objs != null && objs.Length > 0 ? true : false;
        }

        public CompareNode getParentCompNode()
        {
            return parentCompNode;
        }

        private void setParentCompNode(CompareNode parentCompNode)
        {
            this.parentCompNode = parentCompNode;
        }

        protected Object[] getChildren(Object input) {
			if (input is IStructureComparator)
				return ((IStructureComparator)input).getChildren();
			return null;
		}

        public void setAncestorNode(ICadexComparable ancestorNode2)
        {
            this.ancestorNode = ancestorNode2;
            this.setChildAvailable(this.ancestorNode);
        }

        public void setAttributes()
        {
            HashSet<string> leftAttrs = new HashSet<string>();
            HashSet<string> rightAttrs = new HashSet<string>();

            if (leftNode != null)
                leftAttrs = leftNode.getAttrNames();

            if (rightNode != null)
                rightAttrs = rightNode.getAttrNames();

            leftAttrs.UnionWith(rightAttrs);

            foreach (string entry in leftAttrs)
		    {
                String oldAttr = leftNode != null ? ((ICadexGenericObject)leftNode).getAttr(entry) : "NA";
                String newAttr = rightNode != null ? ((ICadexGenericObject)rightNode).getAttr(entry) : "NA";
                addAttrNode(entry, oldAttr, newAttr, "");
		    }

            //AttributeNode bomAttr = new AttributeNode("BOM", this, 1);
            //AttributeNode itemAttr = new AttributeNode("Item", this, 2);
            //AttributeNode revAttr = new AttributeNode("Revision", this, 3);
            //this.attrNodes.Add(bomAttr);
            //this.attrNodes.Add(itemAttr);
            //this.attrNodes.Add(revAttr);
        }

        public void addAttrNode(string attrName, string leftValue, string rightValue, string ancestorValue)
        {
            AttributeNode node = new AttributeNode(attrName, leftValue, rightValue, ancestorValue);
            attrNodes.Add(node);
        }

        public List<AttributeNode> getAttrNodes()
        {
            return attrNodes;
        }

        public List<DatasetNode> getDatasetNodes()
        {
            return datasetNodes;
        }

        public bool isDiff()
        {
            return isDifferent;
        }

        public int getSeqNo()
        {
            return seqNo;
        }

        public int getNodeType()
        {
            return nodeType;
        }

        public void setNodeType() 
	    {
		    if(nodeType == 0)
            {
                nodeType = ( (this.leftNode != null ? this.leftNode : this.rightNode) is ICadexGenericLineObject) ? CompareNode.COMPARE_LINE_NODE : CompareNode.COMPARE_OTHER_NODE ;
            }
			    
	    }

        public ICadexComparable getNode()
        {
            if (leftNode == null)
                return rightNode;
            else if (rightNode == null)
                return leftNode;
            else
                return rightNode;
        }

        public String getNodeUid()
        {
            return nodeUid;
        }
    }
}

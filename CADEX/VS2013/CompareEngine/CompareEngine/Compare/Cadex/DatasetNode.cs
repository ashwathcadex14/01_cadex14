﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompareEngine.Model.Interfaces;

namespace CompareEngine.Compare.Cadex
{
    public class DatasetNode
    {
        String sideName = "";
        List<ICadexGenericDataset> dsets = new List<ICadexGenericDataset>();
        public DatasetNode(String name)
        {
            this.sideName = name;
        }
        public void setDsets(List<ICadexGenericDataset> dsets)
        {
            if (dsets != null)
                this.dsets.AddRange(dsets);
        }
        public List<ICadexGenericDataset> getDsets()
        {
            return dsets;
        }
        public String getSideName()
        {
            return sideName;
        }
    }
}

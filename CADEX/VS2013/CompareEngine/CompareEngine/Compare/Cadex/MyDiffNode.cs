﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompareEngine.Compare.Cadex
{
    public class MyDiffNode : DiffNode
    {
		private bool fDirty= false;
		private ITypedElement fLastId;
		private String fLastName;
		
		
		public MyDiffNode(IDiffContainer parent, int description, ITypedElement ancestor, ITypedElement left, ITypedElement right) : base(parent, description, ancestor, left, right) {
			
		}
	
		void clearDirty() {
			fDirty= false;
		}
		public override String getName() {
			if (fLastName == null)
				fLastName= base.getName();
			if (fDirty)
				return '<' + fLastName + '>';
			return fLastName;
		}

        public override ITypedElement getId()
        {
            ITypedElement id = base.getId();
            if (id == null)
                return fLastId;
            fLastId = id;
            return id;
        }
    }
}

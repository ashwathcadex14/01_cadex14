﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace CompareEngine.Compare
{
    public abstract class DiffContainer : DiffElement, IDiffContainer
    {
        private static IDiffElement[] fgEmptyArray = new IDiffElement[0];
        private List<IDiffElement> fChildren;

        public DiffContainer(IDiffContainer parent, int kind) : base(parent,kind)
        {
            
        }

        public IDiffElement findChild(String name)
        {
            Object[] children = getChildren();
            for (int i = 0; i < children.Length; i++)
            {
                IDiffElement child = (IDiffElement)children[i];
                if (name.Equals(child.getName()))
                    return child;
            }
            return null;
        }

        public abstract override String getName();

        public void add(IDiffElement diff)
        {
            if (this.fChildren == null)
                this.fChildren = new List<IDiffElement>();
            this.fChildren.Add(diff);
            diff.setParent(this);
        }

        public void removeToRoot(IDiffElement child)
        {
            if (this.fChildren != null)
            {
                this.fChildren.Remove(child);
                child.setParent(null);
                if (this.fChildren.Count == 0)
                {
                    IDiffContainer p = getParent();
                    if (p != null)
                        p.removeToRoot(this);
                }
            }
        }

        public void remove(IDiffElement child)
        {
            if (this.fChildren != null)
            {
                this.fChildren.Remove(child);
                child.setParent(null);
            }
        }

        public bool hasChildren()
        {
            return (this.fChildren != null) && (this.fChildren.Count > 0);
        }

        public IDiffElement[] getChildren()
        {

            if (this.fChildren != null)
                return (IDiffElement[])this.fChildren.ToArray();
            return null;
        }
    }
}

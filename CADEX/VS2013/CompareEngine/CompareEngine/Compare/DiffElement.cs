﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace CompareEngine.Compare
{
    public abstract class DiffElement : IDiffElement
    {
        private int fKind;
        private IDiffContainer fParent;

        public DiffElement(IDiffContainer parent, int kind)
        {
            this.fParent = parent;
            this.fKind = kind;
            if (parent != null)
                parent.add(this);
        }

        public Image getImage()
        {
            return null;
        }

        public abstract String getName();

        public String getType()
        {
            return "???";
        }

        public void setKind(int kind)
        {
            this.fKind = kind;
        }

        public int getKind()
        {
            return this.fKind;
        }

        public IDiffContainer getParent()
        {
            return this.fParent;
        }

        public void setParent(IDiffContainer parent)
        {
            this.fParent = parent;
        }
    }
}

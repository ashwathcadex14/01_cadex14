﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Drawing;

namespace CompareEngine.Compare
{
    public class DiffNode : DiffContainer, ICompareInput
    {
        private ITypedElement fAncestor;
        private ITypedElement fLeft;
        private ITypedElement fRight;
        private bool fDontExpand;
        private ObservableCollection<ICompareInputChangeListener> fListener;
        private bool fSwapSides;

        public DiffNode(IDiffContainer parent, int kind, ITypedElement ancestor, ITypedElement left, ITypedElement right) : base(parent, kind)
        {
            
            this.fAncestor = ancestor;
            this.fLeft = left;
            this.fRight = right;
        }

        public DiffNode(ITypedElement left, ITypedElement right) : this(null, 3, null, left, right)
        {
            
        }

        public DiffNode(int kind, ITypedElement ancestor, ITypedElement left, ITypedElement right) : this(null, kind, ancestor, left, right)
        {
            
        }

        public DiffNode(int kind) : base(null, kind)
        {
            
        }

        public DiffNode(IDiffContainer parent, int kind) : base(parent, kind)
        {
            
        }

        public void addCompareInputChangeListener(ICompareInputChangeListener listener)
        {
            if (this.fListener == null)
                this.fListener = new ObservableCollection<ICompareInputChangeListener>();
            this.fListener.Add(listener);
        }

        public void removeCompareInputChangeListener(ICompareInputChangeListener listener)
        {
            if (this.fListener != null)
            {
                this.fListener.Remove(listener);
                if (this.fListener.Count==0)
                    this.fListener = null;
            }
        }

        protected void fireChange()
        {
            if (this.fListener != null)
            {
                Object[] listeners = this.fListener.ToArray<ICompareInputChangeListener>();
                for (int i = 0; i < listeners.Length; i++)
                    ((ICompareInputChangeListener)listeners[i]).compareInputChanged(this);
            }
        }

        public bool dontExpand()
        {
            return this.fDontExpand;
        }

        public void setDontExpand(bool dontExpand)
        {
            this.fDontExpand = dontExpand;
        }

        public virtual ITypedElement getId()
        {
            if (this.fAncestor != null)
                return this.fAncestor;
            if (this.fRight != null)
                return this.fRight;
            return this.fLeft;
        }

        public override String getName()
        {
            String right = null;
            if (this.fRight != null)
            {
                right = this.fRight.getName();
            }
            String left = null;
            if (this.fLeft != null)
            {
                left = this.fLeft.getName();
            }
            if ((right == null) && (left == null))
            {
                if (this.fAncestor != null)
                    return this.fAncestor.getName();
                return "<no name>";
            }

            if (right == null)
                return left;
            if (left == null)
            {
                return right;
            }
            if (right.Equals(left))
                return right;
            String s2;
            String s1;
            
            if (this.fSwapSides)
            {
                s1 = left;
                s2 = right;
            }
            else
            {
                s1 = right;
                s2 = left;
            }

            String fmt = s1 + " / " + s2;
            return fmt;
        }

        void swapSides(bool swap)
        {
            this.fSwapSides = swap;
        }

        public Image getImage()
        {
            ITypedElement id = getId();
            if (id != null)
                return id.getImage();
            return null;
        }

        public String getType()
        {
            ITypedElement id = getId();
            if (id != null)
                return id.getType();
            return "???";
        }

        public void setAncestor(ITypedElement ancestor)
        {
            this.fAncestor = ancestor;
        }

        public ITypedElement getAncestor()
        {
            return this.fAncestor;
        }

        public void setLeft(ITypedElement left)
        {
            this.fLeft = left;
        }

        public ITypedElement getLeft()
        {
            return this.fLeft;
        }

        public void setRight(ITypedElement right)
        {
            this.fRight = right;
        }

        public ITypedElement getRight()
        {
            return this.fRight;
        }

        public void copy(bool leftToRight)
          {
            IDiffContainer pa = getParent();
            if ((pa is ICompareInput)) {
              ICompareInput parent = (ICompareInput)pa;
              Object dstParent = leftToRight ? parent.getRight() : parent.getLeft();

              if ((dstParent is IEditableContent)) {
                ITypedElement dst = leftToRight ? getRight() : getLeft();
                ITypedElement src = leftToRight ? getLeft() : getRight();
                dst = ((IEditableContent)dstParent).replace(dst, src);
                if (leftToRight)
                  setRight(dst);
                else {
                  setLeft(dst);
                }

                fireChange();
              }
            }
          }

        public int hashCode()
        {
            String[] path = getPath(this, 0);
            int hashCode = 1;
            for (int i = 0; i < path.Length; i++)
            {
                String s = path[i];
                hashCode = 31 * hashCode + (s != null ? s.GetHashCode() : 0);
            }
            return hashCode;
        }

        public bool equals(Object other)
        {
            if ((other != null) && (GetType() == other.GetType()))
            {
                String[] path1 = getPath(this, 0);
                String[] path2 = getPath((DiffNode)other, 0);
                if (path1.Length != path2.Length)
                    return false;
                for (int i = 0; i < path1.Length; i++)
                    if (!path1[i].Equals(path2[i]))
                        return false;
                return true;
            }
            return base.Equals(other);
        }

        private static String[] getPath(ITypedElement el, int level) {
    String[] path = null;
    if ((el is IDiffContainer)) {
      IDiffContainer parent = ((IDiffContainer)el).getParent();
      if (parent != null)
        path = getPath(parent, level + 1);
    }
    if (path == null)
      path = new String[level + 1];
    path[(path.Length - 1 - level)] = el.getName();
    return path;
  }
    }
}

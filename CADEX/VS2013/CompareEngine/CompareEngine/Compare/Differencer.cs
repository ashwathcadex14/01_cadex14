﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CompareEngine.Compare
{
    class Differencer
    {
          public static  int NO_CHANGE = 0;
          public static  int ADDITION = 1;
          public static  int DELETION = 2;
          public static  int CHANGE = 3;
          public static  int CHANGE_TYPE_MASK = 3;
          public static  int LEFT = 4;
          public static  int RIGHT = 8;
          public static  int CONFLICTING = 12;
          public static  int DIRECTION_MASK = 12;
          public static  int PSEUDO_CONFLICT = 16;

          public Object findDifferences(bool threeWay, Object data, Object ancestor, Object left, Object right)
          {
            Node root = new Node();

            int code = traverse(threeWay, root, threeWay ? ancestor : null, left, right);

            if (code != 0) {
              List<Node> l = root.fChildren;
              if (l.Count > 0) {
                  Node first = (Node)l.ElementAt<Node>(0);
                return first.visit(this, data, 0);
              }
            }
            return null;
          }

          private int traverse(bool threeWay, Node parent, Object ancestor, Object left, Object right)
          {
            Object[] ancestorChildren = getChildren(ancestor);
            Object[] rightChildren = getChildren(right);
            Object[] leftChildren = getChildren(left);

            int code = 0;

            Node node = new Node(parent, ancestor, left, right);

            bool content = true;

            if (((threeWay) && (ancestorChildren != null)) || ((!threeWay) && 
              (rightChildren != null) && (leftChildren != null)))
            {
                HashSet<Object> allSet = new HashSet<Object>();
              Dictionary<Object,Object> ancestorSet = null;
              Dictionary<Object, Object> rightSet = null;
              Dictionary<Object, Object> leftSet = null;

              if (ancestorChildren != null) {
                  ancestorSet = new Dictionary<Object, Object>();
                for (int i = 0; i < ancestorChildren.Length; i++) {
                  Object ancestorChild = ancestorChildren[i];
                  ancestorSet.Add(ancestorChild, ancestorChild);
                  allSet.Add(ancestorChild);
                }
              }

              if (rightChildren != null) {
                  rightSet = new Dictionary<Object, Object>();
                for (int i = 0; i < rightChildren.Length; i++) {
                  Object rightChild = rightChildren[i];
                  rightSet.Add(rightChild, rightChild);
                  allSet.Add(rightChild);
                }
              }

              if (leftChildren != null) {
                  leftSet = new Dictionary<Object, Object>();
                for (int i = 0; i < leftChildren.Length; i++) {
                  Object leftChild = leftChildren[i];
                  leftSet.Add(leftChild, leftChild);
                  allSet.Add(leftChild);
                }
              }

              foreach( Object keyChild in allSet) {
                

                //if (pm != null)
                //{
                //  if (pm.isCanceled()) {
                //    throw new OperationCanceledException();
                //  }
                //  updateProgress(pm, keyChild);
                //}
                
                  Object ancestorChild = null ;
                  if(ancestorSet != null)
                      ancestorSet.TryGetValue(keyChild, out ancestorChild);
                  Object leftChild = null;
                  if (leftSet != null)
                      leftSet.TryGetValue(keyChild, out leftChild);
                  Object rightChild = null;
                  if (rightSet != null)
                      rightSet.TryGetValue(keyChild, out rightChild);
               

                int c = traverse(threeWay, node, ancestorChild, leftChild, rightChild);

                if ((c & 0x3) != 0) {
                  code |= 3;
                  code |= c & 0xC;
                  content = false;
                }
              }
            }

            if (content) {
              code = compare(threeWay, ancestor, left, right);
            }
            node.fCode = code;

            return code;
          }

          protected virtual Object visit(Object data, int result, Object ancestor, Object left, Object right)
          {
            return new DiffNode((IDiffContainer)data, result, (ITypedElement)ancestor, (ITypedElement)left, (ITypedElement)right);
          }

          private int compare(bool threeway, Object ancestor, Object left, Object right)
          {
            int description = 0;

            if (threeway) {
              if (ancestor == null) {
                if (left == null) {
                  if (right == null) {
                      
                  }
                  else {
                    description = 9;
                  }
                }
                else if (right == null) {
                  description = 5;
                } else {
                  description = 13;
                  if (contentsEqual(left, right)) {
                    description |= 16;
                  }
                }
              }
              else if (left == null) {
                if (right == null) {
                  description = 30;
                }
                else if (contentsEqual(ancestor, right))
                  description = 6;
                else {
                  description = 15;
                }
              }
              else if (right == null) {
                if (contentsEqual(ancestor, left))
                  description = 10;
                else
                  description = 15;
              } else {
                bool ay = contentsEqual(ancestor, left);
                bool am = contentsEqual(ancestor, right);

                if ((!ay) || (!am))
                {
                  if ((ay) && (!am)) {
                    description = 11;
                  } else if ((!ay) && (am)) {
                    description = 7;
                  } else {
                    description = 15;
                    if (contentsEqual(left, right)) {
                      description |= 16;
                    }
                  }
                }
              }
            }
            else if (left == null) {
              if (right == null) {
                  throw new Exception("right is null");
              }
              else {
                description = 1;
              }
            }
            else if (right == null) {
              description = 2;
            }
            else if (!contentsEqual(left, right)) {
              description = 3;
            }

            return description;
          }

          protected virtual bool contentsEqual(Object input1, Object input2)
          {
            if (input1 == input2) {
              return true;
            }
            Stream is1 = getStream(input1);
            Stream is2 = getStream(input2);

            if ((is1 == null) && (is2 == null))
              return true;
            try
            {
              if ((is1 == null) || (is2 == null))
                return false;

              int c1;
              int c2;
              do 
              { 
                  c1 = is1.ReadByte();
                  c2 = is2.ReadByte();
                  if ((c1 == -1) && (c2 == -1))
                    return true; 
              }
              while (c1 == c2);
            }
            catch (IOException localIOException5)
            {
              if (is1 != null)
                try {
                  is1.Close();
                }
                catch (IOException localIOException6)
                {
                }
              if (is2 != null)
                try {
                  is2.Close();
                }
                catch (IOException localIOException7)
                {
                }
            }
            finally
            {
              if (is1 != null)
                try {
                    is1.Close();
                }
                catch (IOException localIOException8)
                {
                }
              if (is2 != null)
                try {
                    is2.Close();
                }
                catch (IOException localIOException9)
                {
                }
            }
            return false;
          }

          private Stream getStream(Object o)
          {
            if ((o is IStreamContentAccessor))
              try {
                return ((IStreamContentAccessor)o).getContents();
              }
              catch (Exception localCoreException)
              {
              }
            return null;
          }

          protected virtual Object[] getChildren(Object input)
          {
            if ((input is IStructureComparator))
              return ((IStructureComparator)input).getChildren();
            return null;
          }

          protected void updateProgress(IProgressMonitor progressMonitor, Object node)
          {
            if ((node is ITypedElement)) {
              String name = ((ITypedElement)node).getName();
              //String fmt = Utilities.getString("Differencer.progressFormat");
              //String msg = MessageFormat.format(fmt, new String[] { name });
              //progressMonitor.subTask(msg);
            }
          }

          class Node
          {
            public List<Node> fChildren;
            public int fCode;
            public Object fAncestor;
            Object fLeft;
            Object fRight;

            public Node()
            {
            }

            public Node(Node parent, Object ancestor, Object left, Object right)
            {
              parent.add(this);
              this.fAncestor = ancestor;
              this.fLeft = left;
              this.fRight = right;
            }
            void add(Node child) {
              if (this.fChildren == null)
                this.fChildren = new List<Node>();
              this.fChildren.Add(child);
            }
            public Object visit(Differencer d, Object parent, int level) {
              if (this.fCode == 0) {
                return null;
              }
              Object data = d.visit(parent, this.fCode, this.fAncestor, this.fLeft, this.fRight);
              if (this.fChildren != null) {
                
                foreach( Node n in this.fChildren) {
                  n.visit(d, data, level + 1);
                }
              }
              return data;
            }
          }


            }
}

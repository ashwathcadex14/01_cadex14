﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace CompareEngine.Compare
{
    public interface ICompareInput
    {
          String getName();

          Image getImage();

          int getKind();

          ITypedElement getAncestor();

          ITypedElement getLeft();

          ITypedElement getRight();

          void addCompareInputChangeListener(ICompareInputChangeListener paramICompareInputChangeListener);

          void removeCompareInputChangeListener(ICompareInputChangeListener paramICompareInputChangeListener);

          void copy(bool paramBoolean);
    }
}

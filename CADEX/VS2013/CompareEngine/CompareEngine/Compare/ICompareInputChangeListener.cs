﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CompareEngine.Compare
{
    public interface ICompareInputChangeListener
    {
        void compareInputChanged(ICompareInput paramICompareInput);
    }
}

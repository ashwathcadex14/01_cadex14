﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompareEngine.Compare
{
    public interface IDiffContainer : IDiffElement
    {
         bool hasChildren();

         IDiffElement[] getChildren();

         void add(IDiffElement paramIDiffElement);

         void removeToRoot(IDiffElement paramIDiffElement);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompareEngine.Compare
{
    public interface IDiffElement : ITypedElement
    {
         int getKind();
    
         IDiffContainer getParent();

         void setParent(IDiffContainer paramIDiffContainer);
    }
}

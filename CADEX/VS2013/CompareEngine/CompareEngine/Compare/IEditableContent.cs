﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompareEngine.Compare
{
    interface IEditableContent
    {
         bool isEditable();

         void setContent(byte[] paramArrayOfByte);

         ITypedElement replace(ITypedElement paramITypedElement1, ITypedElement paramITypedElement2);
    }
}

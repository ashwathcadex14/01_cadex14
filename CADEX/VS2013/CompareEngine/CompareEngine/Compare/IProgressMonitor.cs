﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompareEngine.Compare
{
    interface IProgressMonitor
    {
          //public static  int UNKNOWN = -1;

           void beginTask(String paramString, int paramInt);

           void done();

           void internalWorked(double paramDouble);

           bool isCanceled();

           void setCanceled(bool parambool);

           void setTaskName(String paramString);

           void subTask(String paramString);

           void worked(int paramInt);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompareEngine.Compare
{
    public interface IStructureComparator
    {
         Object[] getChildren();

         bool Equals(object paramObject);

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace CompareEngine.Compare
{
    public interface ITypedElement
    {
      //public static String FOLDER_TYPE = "FOLDER";
      //public static String TEXT_TYPE = "txt";
      //public static String UNKNOWN_TYPE = "???";

       String getName();

       Image getImage();

       String getType();
    }
}

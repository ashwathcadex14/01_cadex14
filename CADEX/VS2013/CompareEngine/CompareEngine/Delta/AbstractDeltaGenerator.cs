using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompareEngine.Model.Interfaces;
using CompareEngine.Compare.Cadex;
using InfoExPLMXMLSDK.com.infoex.plmxml.types;

namespace CompareEngine.Delta
{

public abstract class AbstractDeltaGenerator<T> : ICadexDeltaGenerator where T: DocumentBase {

	private T thisDeltaDoc ;
	CompareNode thisCmpNode = null ;
	HashSet<String> visitedNodes = new HashSet<String>();
	
	public AbstractDeltaGenerator(T deltaDoc, CompareNode cmpNode) {
		thisDeltaDoc = deltaDoc;
		thisCmpNode = cmpNode;
	}
	
	public CompareNode getThisCmpNode() {
		return thisCmpNode;
	}

	public T getDocument()
	{
		return thisDeltaDoc;
	}

	
	public void traverseStructure()
	{
		try
		{
			traverseChildren(thisCmpNode);	
		}
		catch(Exception e)
		{
            InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);
            throw;
		}
	}
	
	protected bool isVisited(String id) {
		return visitedNodes.Contains( id );
	}

    public abstract void writeDelta(CompareNode node);
	
	
	public void traverseChildren(CompareNode node)
	{
		try
		{
			writeDelta(node);
			
			List<CompareNode> childs = node.getChildNodes();
			
			if( childs !=null && childs.Count > 0)
			{
				foreach(CompareNode cmpNode in childs)
				{
					traverseChildren(cmpNode);
				}
			}
		}
		catch(Exception e)
		{
            InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);
            throw;
		}
	}
}

    public enum DELTA_INFO
    {
        NO_CHANGE = 0,
        NEW_PRODUCT = 1,
        NEW_PRODUCT_REVISION = 2,
        NEW_DATASET = 3,
        DATASET_REMOVED = 4,
        NEW_OCCURRENCE = 5,
        OCCURRENCE_REMOVED = 6,
        OCCURRENCE_MODIFIED = 7,
        PRODUCT_MODIFIED = 8,
        PRODUCT_REVISION_MODIFIED = 9,
        DATASET_MODIFIED = 10,
        NEW_FILE = 11,
        FILE_MODIFIED = 12,
        FILE_REMOVED = 13,
        NEW = 14,
        MODIFIED = 15,
        REMOVED = 16
    }
}

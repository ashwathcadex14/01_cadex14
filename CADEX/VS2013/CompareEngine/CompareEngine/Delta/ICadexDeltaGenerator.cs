using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompareEngine.Model.Interfaces;
using CompareEngine.Compare.Cadex;

namespace CompareEngine.Delta
{
    public interface ICadexDeltaGenerator
    {

        void traverseStructure();

        void traverseChildren(CompareNode node);

        void writeDelta(CompareNode node);


    }
}

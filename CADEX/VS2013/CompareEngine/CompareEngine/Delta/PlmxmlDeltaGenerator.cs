using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompareEngine.Model.Interfaces;
using CompareEngine.Compare.Cadex;
using InfoExCommon.Utils;
using CompareEngine.Model.Plmxml;
using InfoExPLMXMLSDK.com.infoex.plmxml.types;

namespace CompareEngine.Delta
{

public class PlmxmlDeltaGenerator : AbstractDeltaGenerator<PLMXMLDelta>
{
    DeltaAdd addDelta = null;
	DeltaModify modDelta = null;
	
	public PlmxmlDeltaGenerator(PLMXMLDelta deltaDoc, CompareNode node) : 
		base(deltaDoc, node)
    {
		// TODO Auto-generated constructor stub
	}

	public override void writeDelta(CompareNode node)
	{
		HashSet<CompareNode> addNodes = new HashSet<CompareNode>();
		HashSet<CompareNode> modNodes = new HashSet<CompareNode>();
		HashSet<CompareNode> delNodes = new HashSet<CompareNode>();
		node.shiftHierarchy();
		addDelta = new DeltaAdd();
		modDelta = new DeltaModify();
	    
		writeDeltaForLine(node, addNodes, modNodes, delNodes);
		if(addDelta.getElements() != null && addDelta.getElements().Count() > 0)
			getDocument().addAdd(addDelta);
		if(modDelta.getElements() != null && modDelta.getElements().Count() > 0)
			getDocument().addModify(modDelta);
	}

	private void writeDeltaForLine(CompareNode node, HashSet<CompareNode> addNodes, HashSet<CompareNode> modNodes, HashSet<CompareNode> delNodes)
	{	
		writeDeltaForOccurrence(node);
		
		if(node.getChildDetailNodes().Count == 1)
		{
			doDeltaForChilds(node.getChildDetailNodes()[0]);
		}
	}
	
	private void doDeltaForChilds(CompareNode node)
	{
		ICadexComparable cNode = node.getNode();
		
		if(cNode.getType().Equals( "Item" ))
		{
			writeDeltaForItem(node);
		}
		else if(cNode.getType().Equals( "ItemRevision" ))
		{
			writeDeltaForRevision(node);
		}
		else if(cNode.getType().Equals( "Dataset" ))
		{
			writeDeltaForDataset(node);
		}
		else if(cNode.getType().Equals( "ImanFile" ))
		{
			writeDeltaForFile(node);
		}
		
		foreach(CompareNode detailNode in node.getChildNodes())
		{
			doDeltaForChilds(detailNode);
		}
	}

	
	private void writeDeltaForOccurrence(CompareNode node)
	{
		Occurrence occ = ((PlmxmlOccurence)node.getNode().getObject()).getObj();
		
		Occurrence newOcc = occ;
		//newOcc.setOwner( occ );
		
		LoggerUtil.info(this.GetType(), "Preparing delta for occ " + occ.getId());
		
		if(node.getDelta() == DELTA_INFO.NEW)
		{
			if(node.getParentCompNode() == null)
			{
				addDelta.addElement( newOcc );
                writeDeltaForOccObjects(newOcc, addDelta);
			}
			else
			{
				if(node.getParentCompNode().getDelta() == DELTA_INFO.NEW)
				{
					addDelta.addElement( newOcc );
                    writeDeltaForOccObjects(newOcc, addDelta);
				}
				else
				{
					ICadexComparable rightParent = node.getParentCompNode().getNode();
					ExternalReference newRef = new ExternalReference();
					newRef.type = rightParent.getType();					
					ApplicationRef newAppRef = new ApplicationRef();
					newAppRef.setApplication( ((IPlmxmlGenericObject)rightParent).getAppRefName() );
					//newAppRef.setVersion( ((IPlmxmlGenericObject)rightParent).getAppRefVersion() );
					newAppRef.setLabel( ((IPlmxmlGenericObject)rightParent).getAppRefLabel() );
					newRef.addApplicationRef( newAppRef );
					getDocument().addElement(newRef);
					DeltaModify modNode = new DeltaModify();
				    modNode.attributeName = "occurrenceRefs";
					modNode.op = DeltaModifyOp.add ;
					modNode.targetRef = "#" + newRef.getId();
					modNode.addValueRef( "#" + occ.getId() );
					getDocument().addModify(modNode);
					addDelta.addElement( newOcc );
                    writeDeltaForOccObjects(newOcc, addDelta);
				}
			}
		}
		else if( node.getDelta() == DELTA_INFO.MODIFIED )
		{
			modDelta.addElement( newOcc );
            writeDeltaForOccObjects(newOcc, addDelta);
		}
		else if( node.getDelta() == DELTA_INFO.REMOVED )
		{
			ICadexComparable leftNode = node.getLeftNode();
			ExternalReference newRef = new ExternalReference();
			newRef.type = leftNode.getType();					
			ApplicationRef newAppRef = new ApplicationRef();
			newAppRef.setApplication( ((IPlmxmlGenericObject)leftNode).getAppRefName() );
			//newAppRef.setVersion( ((IPlmxmlGenericObject)rightParent).getAppRefVersion() );
			newAppRef.setLabel( ((IPlmxmlGenericObject)leftNode).getAppRefLabel() );
			newRef.addApplicationRef( newAppRef );
			getDocument().addElement(newRef);
			DeltaDelete delNode = new DeltaDelete();
			delNode.addTargetURI( "#" + newRef.getId() );
			getDocument().addDelete(delNode);
		}
	}

    private void writeDeltaForOccObjects(Occurrence occ, DeltaAdd addDelta)
    {
        List<IdBase> prodInstances = occ.getProductInstances();
        if(prodInstances!=null && prodInstances.Count>0)
        {
            foreach(IdBase prodInst in prodInstances)
            {
                if(prodInst is ProductInstance)
                {
                    addDelta.addElement(prodInst);
                    IdBase prodRevView = ((ProductInstance)prodInst).getProductRevisionView();
                    if(prodRevView!=null && prodRevView is ProductRevisionView)
                    {
                        ((ProductRevisionView)prodRevView).viewRef = null;
                        addDelta.addElement(prodRevView);
                    }
                }
            }
        }
    }
	
	private void writeDeltaForRevision(CompareNode node)
	{
		ProductRevision occ = ((PlmxmlRevision)node.getNode().getObject()).getObj();
		ProductRevision newOcc = (ProductRevision) occ;
		
		LoggerUtil.info(this.GetType(), "Preparing delta for ItemRevision " + node.getNodeUid());
	
		if(node.getDelta() == DELTA_INFO.NEW)
		{
			if(node.getParentCompNode() == null)
			{
				addDelta.addElement( newOcc );
			}
			else
			{
				if(node.getParentCompNode().getDelta() == DELTA_INFO.NEW)
				{
					addDelta.addElement( newOcc );
				}
				else
				{
					ICadexComparable rightParent = node.getParentCompNode().getNode();
					ExternalReference newRef = new ExternalReference();
					newRef.type = rightParent.getType();					
					ApplicationRef newAppRef = new ApplicationRef();
					newAppRef.setApplication( ((IPlmxmlGenericObject)rightParent).getAppRefName() );
					newAppRef.setVersion( ((IPlmxmlGenericObject)rightParent).getAppRefVersion() );
					newAppRef.setLabel( ((IPlmxmlGenericObject)rightParent).getAppRefLabel() );
					newRef.addApplicationRef( newAppRef );
					getDocument().addElement(newRef);
//					DeltaModify modNode = new DeltaModify();
//					modNode.setAttributeName( "occurrenceRefs" );
//					modNode.setOp( eDeltaModifyOpType.ADD );
//					modNode.setTargetURI( "#" + newRef.getId() );
//					modNode.addValueURI( "#" + occ.getId() );
//					getDocument().addElement(modNode);
					addDelta.addElement( newOcc );
				}
			}
		}
		else if( node.getDelta() == DELTA_INFO.MODIFIED )
		{
			modDelta.addElement( newOcc );
		}
		else if( node.getDelta() == DELTA_INFO.REMOVED )
		{
			ICadexComparable leftNode = node.getLeftNode();
			ExternalReference newRef = new ExternalReference();
			newRef.type =  leftNode.getType();					
			ApplicationRef newAppRef = new ApplicationRef();
			newAppRef.setApplication( ((IPlmxmlGenericObject)leftNode).getAppRefName() );
			newAppRef.setVersion( ((IPlmxmlGenericObject)leftNode).getAppRefVersion() );
			newAppRef.setLabel( ((IPlmxmlGenericObject)leftNode).getAppRefLabel() );
			newRef.addApplicationRef( newAppRef );
			getDocument().addElement(newRef);
			DeltaDelete delNode = new DeltaDelete();
			delNode.addTargetURI( "#" + newRef.getId() );
			getDocument().addDelete(delNode);
		}
	}
	
	private void writeDeltaForItem(CompareNode node)
	{
		
		Product occ = ((PlmxmlItem)node.getNode().getObject()).getObj();
		Product newOcc = (Product) occ;
		
		LoggerUtil.info(this.GetType(), "Preparing delta for Item " + node.getNodeUid());
		
		if(node.getDelta() == DELTA_INFO.NEW)
		{
			addDelta.addElement( newOcc );
		}
		else if( node.getDelta() == DELTA_INFO.MODIFIED )
		{
			modDelta.addElement( newOcc );
		}
		else if( node.getDelta() == DELTA_INFO.REMOVED )
		{
			ICadexComparable leftNode = node.getNode();
			ExternalReference newRef = new ExternalReference();
			newRef.type = ( leftNode.getType());					
			ApplicationRef newAppRef = new ApplicationRef();
			newAppRef.setApplication( ((IPlmxmlGenericObject)leftNode).getAppRefName() );
			newAppRef.setVersion( ((IPlmxmlGenericObject)leftNode).getAppRefVersion() );
			newAppRef.setLabel( ((IPlmxmlGenericObject)leftNode).getAppRefLabel() );
			newRef.addApplicationRef( newAppRef );
			getDocument().addElement(newRef);
			DeltaDelete delNode = new DeltaDelete();
			delNode.addTargetURI( "#" + newRef.getId() );
			getDocument().addDelete(delNode);
		}
		
	}
	
	private void writeDeltaForDataset(CompareNode node)
	{
		DataSet occ = ((PlmxmlDataset)node.getNode().getObject()).getObj();
		DataSet newOcc = (DataSet) occ;
		AssociatedDataSet relNode = ((PlmxmlDataset)node.getNode()).getRelNode();
		AssociatedDataSet newRelNode = (AssociatedDataSet) relNode;
		
		LoggerUtil.info(this.GetType(), "Preparing delta for Dataset " + node.getNodeUid());
		
		if(node.getDelta() == DELTA_INFO.NEW)
		{
			if(node.getParentCompNode() == null)
			{
				addDelta.addElement( newOcc );
			}
			else
			{
				if(node.getParentCompNode().getDelta() == DELTA_INFO.NEW)
				{
					addDelta.addElement( newOcc );
				}
				else
				{
					ICadexComparable rightParent = node.getParentCompNode().getNode();
					ExternalReference newRef = new ExternalReference();
					newRef.type = ( rightParent.getType());					
					ApplicationRef newAppRef = new ApplicationRef();
					newAppRef.setApplication( ((IPlmxmlGenericObject)rightParent).getAppRefName() );
					newAppRef.setVersion( ((IPlmxmlGenericObject)rightParent).getAppRefVersion() );
					newAppRef.setLabel( ((IPlmxmlGenericObject)rightParent).getAppRefLabel() );
					newRef.addApplicationRef( newAppRef );
					getDocument().addElement(newRef);
					DeltaModify modNode = new DeltaModify();
					modNode.attributeName = ( "associatedDataSetRefs" );
					modNode.op = ( DeltaModifyOp.add );
					modNode.targetRef = ( "#" + newRef.getId() );
					modNode.addValueRef( "#" + relNode.getId() );
					getDocument().addModify(modNode);
					addDelta.addElement( newOcc );
					addDelta.addElement( newRelNode );
				}
			}
		}
		else if( node.getDelta() == DELTA_INFO.MODIFIED )
		{
			modDelta.addElement( newOcc );
		}
		else if( node.getDelta() == DELTA_INFO.REMOVED )
		{
			ExternalReference newRef = new ExternalReference();
			newRef.type = ( "AssociatedDataSet" );					
			
			ApplicationRef[] appRefs =  relNode.getApplicationRefs();
			
			if(appRefs.Count() == 1)
			{
				newRef.addApplicationRef( appRefs[0] );
			}
			
			getDocument().addElement(newRef);
			DeltaDelete delNode = new DeltaDelete();
			delNode.addTargetURI( "#" + newRef.getId() );
			getDocument().addDelete(delNode);
		}
	}
	
	private void writeDeltaForFile(CompareNode node)
	{
		ExternalFile occ = ((PlmxmlFile)node.getNode().getObject()).getObj();
		ExternalFile newOcc = (ExternalFile) occ;

        LoggerUtil.info(this.GetType(), "Preparing delta for ImanFile " + node.getNodeUid());
		
		if(node.getDelta() == DELTA_INFO.NEW)
		{
			if(node.getParentCompNode() == null)
			{
				addDelta.addElement( newOcc );
			}
			else
			{
				if(node.getParentCompNode().getDelta() == DELTA_INFO.NEW)
				{
					addDelta.addElement( newOcc );
				}
				else
				{
					ICadexComparable rightParent = node.getParentCompNode().getNode();
					ExternalReference newRef = new ExternalReference();
					newRef.type = ( rightParent.getType());					
					ApplicationRef newAppRef = new ApplicationRef();
					newAppRef.setApplication( ((IPlmxmlGenericObject)rightParent).getAppRefName() );
					newAppRef.setVersion( ((IPlmxmlGenericObject)rightParent).getAppRefVersion() );
					newAppRef.setLabel( ((IPlmxmlGenericObject)rightParent).getAppRefLabel() );
					newRef.addApplicationRef( newAppRef );
					getDocument().addElement(newRef);
					DeltaModify modNode = new DeltaModify();
					modNode.attributeName = ( "memberRefs" );
					modNode.op = ( DeltaModifyOp.add );
					modNode.targetRef = ( "#" + newRef.getId() );
					modNode.addValueRef( "#" + occ.getId() );
					getDocument().addModify(modNode);
					addDelta.addElement( newOcc );
				}
			}
		}
		else if( node.getDelta() == DELTA_INFO.MODIFIED )
		{
			modDelta.addElement( newOcc );
		}
		else if( node.getDelta() == DELTA_INFO.REMOVED )
		{
			ICadexComparable leftNode = node.getNode();
			ExternalReference newRef = new ExternalReference();
			newRef.type  = ( leftNode.getType());					
			ApplicationRef newAppRef = new ApplicationRef();
			newAppRef.setApplication( ((IPlmxmlGenericObject)leftNode).getAppRefName() );
			newAppRef.setVersion( ((IPlmxmlGenericObject)leftNode).getAppRefVersion() );
			newAppRef.setLabel( ((IPlmxmlGenericObject)leftNode).getAppRefLabel() );
			newRef.addApplicationRef( newAppRef );
			getDocument().addElement(newRef);
			DeltaDelete delNode = new DeltaDelete();
			delNode.addTargetURI( "#" + newRef.getId() );
			getDocument().addDelete(delNode);
		}
		
	}
    
}
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompareEngine.Model.Plmxml;
using CompareEngine.Compare;
using CompareEngine.Compare.Cadex;
using InfoExPLMXMLSDK.com.infoex.plmxml.types;
using InfoExPLMXMLSDK.com.infoex.plmxml.generic;

namespace CompareEngine.Input
{

    public class CadexCompareEditorInput
    {

        private PlmxmlOccurence root1;
        private PlmxmlOccurence root2;
        private DiffNode diffNode;

        public CompareNode compareNode
        {
            get
            {
                return d.getCompareNode();
            }
        }
		CadexDifferencer d = new CadexDifferencer();

        public DiffNode getDiffNode()
        {
            return diffNode;
        }

        public CadexCompareEditorInput(String file1, String file2)
        {

            root1 = initializeFromInputPLMXML(file1);
            root2 = initializeFromInputPLMXML(file2);
            prepareInput();
        }

        public PlmxmlOccurence initializeFromInputPLMXML(String inputPLMXMLPath)
        {
            String caeDeckStructId = null;
            ProductView[] prodViewList = null;
            PLMXMLDocument m_xmlDoc = readDocument(inputPLMXMLPath);
            ProductView mainProdcutView;

            prodViewList = (ProductView[])m_xmlDoc.getElementsByClass<ProductView>("ProductView");

            mainProdcutView = (ProductView)prodViewList[0];

            caeDeckStructId = mainProdcutView.getAttributeValueAsString("rootRefs");

            Occurrence m_caeDeckOcc = (Occurrence)m_xmlDoc.resolveId(caeDeckStructId);

            return new PlmxmlOccurence(m_caeDeckOcc);
        }

        public static PLMXMLDocument readDocument(String xmlFileUri)
        {
            return PLMXMLParser.readDocument(xmlFileUri);
        }

        public PlmxmlOccurence getRoot1()
        {
            return root1;
        }

        public PlmxmlOccurence getRoot2()
        {
            return root2;
        }

        protected void prepareInput()
        {
            
            diffNode = (DiffNode)d.findDifferences(false, null, null, root1, root2);

            if (compareNode != null)
                traverseStructure();

        }

        public void traverseStructure() 
	    {
		    try
		    {
			    traverseChildren(compareNode);	
		    }
		    catch(Exception e)
		    {
			    //InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);
                throw;
		    }
	    }
	
	    public void traverseChildren(CompareNode node)
	    {
		    try
		    {
			    node.shiftHierarchy();
			
			    List<CompareNode> childs = node.getChildNodes();
			
			    if( childs !=null && childs.Count > 0)
			    {
				    foreach(CompareNode cmpNode in childs)
				    {
					    traverseChildren(cmpNode);
				    }
			    }
		    }
		    catch(Exception e)
		    {
			    //InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);
                throw;
		    }
	    }

    }
}

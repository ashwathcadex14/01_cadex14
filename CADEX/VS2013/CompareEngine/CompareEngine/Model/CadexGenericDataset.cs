using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompareEngine.Model.Interfaces;

namespace CompareEngine.Model
{
    public abstract class CadexGenericDataset<T> : CadexGenericObject<T>, ICadexGenericDataset
    {

        List<ICadexGenericFile> datasetFiles = new List<ICadexGenericFile>();
        String relationName = "";

        public CadexGenericDataset(T obj, String relation)
            : base(obj)
        {
            this.relationName = relation;
            setFiles();
        }

        public abstract List<ICadexGenericFile> populateFiles();


        public List<ICadexGenericFile> getFiles()
        {
            return datasetFiles;
        }

        private void setFiles()
        {
            List<ICadexGenericFile> files = populateFiles();
            if (files != null)
                this.datasetFiles.AddRange(files);
        }

        public String getRelation()
        {
            // TODO Auto-generated method stub
            return this.relationName;
        }

    }
}

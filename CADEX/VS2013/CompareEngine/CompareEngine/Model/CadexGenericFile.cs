using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompareEngine.Model.Interfaces;

namespace CompareEngine.Model
{

    public abstract class CadexGenericFile<T> : CadexGenericObject<T>,
            ICadexGenericFile
    {

        public CadexGenericFile(T obj)
            : base(obj)
        {

        }

        public abstract override void setProperties();

        public abstract string getOriginalFileName();

    }
}

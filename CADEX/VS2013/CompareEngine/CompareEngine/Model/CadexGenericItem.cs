using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompareEngine.Model.Interfaces;

namespace CompareEngine.Model
{

    public abstract class CadexGenericItem<T> : CadexGenericObject<T>, ICadexGenericItem
    {

        public CadexGenericItem(T item)
            : base(item)
        {

        }

    }
}
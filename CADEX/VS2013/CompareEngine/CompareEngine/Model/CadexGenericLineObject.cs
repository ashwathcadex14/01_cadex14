using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompareEngine.Model.Interfaces;
using CompareEngine.Compare;
using System.Drawing;

namespace CompareEngine.Model
{


    public abstract class CadexGenericLineObject<T> : CadexGenericObject<T>, IStructureComparator, ITypedElement, ICadexGenericLineObject
    {

        private ICadexGenericRevision lineItem;
        private List<ICadexGenericLineObject> children = new List<ICadexGenericLineObject>();

        public CadexGenericLineObject(T lineObj)
            : base(lineObj)
        {

            setLineRev();
            setChildren();
        }

        public abstract ICadexGenericRevision getItemRevision();
        public abstract List<ICadexGenericLineObject> getChildLines();

        public ICadexGenericRevision getLineRev()
        {
            return lineItem;
        }

        public void setLineRev()
        {
            this.lineItem = getItemRevision();
        }

        public List<ICadexGenericLineObject> returnChildren()
        {
            return children;
        }

        private void setChildren()
        {
            this.children.AddRange(getChildLines());
        }

        public abstract override bool Equals(object paramObject);

        public abstract override int GetHashCode();
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompareEngine.Model.Interfaces;
using CompareEngine.Delta;
using System.Globalization;
using System.Drawing;
using CompareEngine.Compare;
using CompareEngine.Compare.Cadex;

namespace CompareEngine.Model
{

    public abstract class CadexGenericObject<T> : ICadexGenericObject
    {

        private T obj;
        private Dictionary<String, String> attrMap = new Dictionary<String, String>();
        private DELTA_INFO dChangeInfo = DELTA_INFO.NO_CHANGE;
        Dictionary<string, AttributeNode> attrNodes = new Dictionary<string, AttributeNode>();

        public CadexGenericObject(T obj)
        {
            this.obj = obj;
            setProperties();
        }

        public abstract void setProperties();

        public abstract String getType();
        public abstract string getUid();
        public abstract string getTagName();
        public abstract bool isNew();
        public abstract int getChildrenCount();
        public abstract void generateDelta();
        public abstract Object[] getChildren();
        public abstract Image getImage();

        public T getObj() 
        {
            return obj;
        }

        Object ICadexGenericObject.getObj()
        {
            return getObj();
        }

        public String getAttr(String attrName)
        {
            String attrValue;
            attrMap.TryGetValue(attrName, out attrValue);
            return attrValue;
        }

        public void addAttr(String attrName, String attrValue)
        {
            this.attrMap.Add(attrName, attrValue);
        }

        public HashSet<String> getAttrNames()
        {
            HashSet<String> tempSet = new HashSet<String>();
            foreach (String item in this.attrMap.Keys)
                tempSet.Add(item);

            return tempSet;
        }

        
	    public DELTA_INFO changeInfo() {
		    // TODO Auto-generated method stub
		    return dChangeInfo;
	    }
	
	
	    public void setChangeInfo(DELTA_INFO delta) {
            this.dChangeInfo = delta;
	    }
	
	
	    public String getObjectString() {
		    return getAttr( "object_string" );
	    }
	
        	
	    public override bool Equals(Object other) {
		    if (other is ITypedElement)
			    return getName().Equals(((ITypedElement) other).getName());
		    return base.Equals(other);
	    }
	
        public abstract String getName();

        public override int GetHashCode()
        {
            return getName().GetHashCode();
        }

        public override string ToString()
        {
            return getName();
        }

        public Dictionary<string, AttributeNode> getAttrNodes()
        {
            return attrNodes;
        }

        public void addAttrNode(string attrName, string leftValue, string rightValue, string ancestorValue)
        {
            AttributeNode node = new AttributeNode(attrName, leftValue, rightValue, ancestorValue);
            attrNodes.Add(attrName, node);
        }
	
	    public virtual bool doDeepCompare(ICadexComparable oldObject) {
		
		    bool isSame = true;

            HashSet<string> thisAttrs = this.getAttrNames();
            HashSet<string> oldObjAttrs = oldObject.getAttrNames();

            thisAttrs.UnionWith(oldObjAttrs);

            foreach (string entry in thisAttrs)
		    {
                String oldAttr = getAttr(entry);
                String newAttr = oldObject.getObject().getAttr(entry);
                if(entry.Equals( "last_mod_date" ))
			    {
				    try 
				    {
                        DateTime oldDate = DateTime.ParseExact(oldAttr, "yyyy-MM-dd'T'HH:mm:ss", CultureInfo.InvariantCulture);
                        DateTime newDate = DateTime.ParseExact(newAttr, "yyyy-MM-dd'T'HH:mm:ss", CultureInfo.InvariantCulture);

					
					    if(newDate.CompareTo(oldDate) >0)
						    isSame = false;
					
				    } catch (Exception e) {
					    isSame = false;
				    }
			    }
		    }
		
		    return !isSame;
	    }
        	
	    public ICadexGenericObject getObject() {
		    // TODO Auto-generated method stub
		    return this;
	    }

	
	    public string getDeltaInfo() {
		    // TODO Auto-generated method stub
		    return Enum.GetName(typeof(DELTA_INFO), this.changeInfo());
	    }
    }
}

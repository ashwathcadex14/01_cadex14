using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompareEngine.Model.Interfaces;

namespace CompareEngine.Model
{

    public abstract class CadexGenericRevision<T> : CadexGenericObject<T>, ICadexGenericRevision
    {

        private ICadexGenericItem revItem;
        private List<ICadexGenericDataset> attaches = new List<ICadexGenericDataset>();

        public CadexGenericRevision(T obj)
            : base(obj)
        {

            setRevItem();
            setAttaches();
        }

        public abstract List<ICadexGenericDataset> getAttachments();
        public abstract ICadexGenericItem getItem();

        public List<ICadexGenericDataset> getAttaches()
        {
            return attaches;
        }

        public void setAttaches()
        {
            this.attaches.AddRange(getAttachments());
        }

        public ICadexGenericItem getRevItem()
        {
            return revItem;
        }

        private void setRevItem()
        {
            this.revItem = getItem();
        }



    }
}

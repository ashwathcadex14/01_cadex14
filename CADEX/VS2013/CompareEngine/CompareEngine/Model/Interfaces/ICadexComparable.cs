using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompareEngine.Compare;
using CompareEngine.Delta;
using System.Collections.Generic;
using CompareEngine.Compare.Cadex;

namespace CompareEngine.Model.Interfaces
{

    public interface ICadexComparable : IStructureComparator, ITypedElement
    {
        ICadexGenericObject getObject();

        int getChildrenCount();

        string getObjectString();

        string getDeltaInfo();

        DELTA_INFO changeInfo();

        void generateDelta();

        void setChangeInfo(DELTA_INFO delta);

        bool doDeepCompare(ICadexComparable oldObject);

        Dictionary<string, AttributeNode> getAttrNodes();

        HashSet<String> getAttrNames();
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompareEngine.Model.Interfaces
{

    public interface ICadexGenericDataset : ICadexGenericObject
    {
        List<ICadexGenericFile> getFiles();
        string getType();
        string getRelation();

    }
}

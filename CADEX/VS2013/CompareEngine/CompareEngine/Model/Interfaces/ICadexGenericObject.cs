using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompareEngine.Model.Interfaces
{

     public interface ICadexGenericObject : ICadexComparable
    {

         Object getObj();

         string getAttr(String attrName);

         void addAttr(String attrName, String attrValue);

         new HashSet<String> getAttrNames();

         string getUid();

         string getTagName();

         bool isNew();
    }
}
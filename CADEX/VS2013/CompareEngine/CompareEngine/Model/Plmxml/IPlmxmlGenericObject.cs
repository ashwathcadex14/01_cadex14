using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompareEngine.Model.Interfaces;
using CompareEngine.Compare;
using InfoExPLMXMLSDK.com.infoex.plmxml.types;

namespace CompareEngine.Model.Plmxml
{
    public interface IPlmxmlGenericObject
    {

         String getAppRefLabel();
         String getAppRefVersion();
         String getAppRefName();
         void setAppRefLabel(String a);
         void setAppRefVersion(String a);
         void setAppRefName(String a);
         DescriptionBase getDescObj();

    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompareEngine.Model.Interfaces;
using InfoExPLMXMLSDK.com.infoex.plmxml.types;
using System.Drawing;

namespace CompareEngine.Model.Plmxml
{

    public class PlmxmlDataset : CadexGenericDataset<DataSet>, IPlmxmlGenericObject
    {
        protected String applicationName = "";
        protected String appRefLabel = "";
        protected String appRefVersion = "";
        protected AssociatedDataSet relNode = null;

        public PlmxmlDataset(DataSet obj, String relation)
            : base(obj, relation)
        {
            PlmxmlUtil.populateAppRef(this);
        }


        public override void setProperties()
        {
            this.addAttr("object_name", this.getObj().getAttributeValueAsString("name"));
            this.addAttr("object_type", this.getObj().getAttributeValueAsString("type"));
            this.addAttr("version", this.getObj().getAttributeValueAsString("version").ToString());
            PlmxmlUtil.addUserData(this, this.getObj());
            this.addAttr("object_string", this.getAttr("object_name"));
        }




        public override List<ICadexGenericFile> populateFiles()
        {

            List<ICadexGenericFile> files = new List<ICadexGenericFile>();
            String[] baseObj = this.getObj().memberRefs;

            if (baseObj.Length > 0)
            {
                for (int i = 0; i < baseObj.Length; i++)
                {
                    ExternalFile file = (ExternalFile) this.getObj().getDocument().resolveId(baseObj[i]);
                    files.Add(new PlmxmlFile(file));
                }
            }

            return files;

        }


        public override String getType()
        {
            return "Dataset";
        }


        public override bool doDeepCompare(ICadexComparable obj)
        {
		// TODO Auto-generated method stub
		    return base.doDeepCompare(obj);
	    }


        public override bool isNew()
        {
		// TODO Auto-generated method stub
		return !applicationName.Equals( PlmxmlUtil.TEAMCENTER_APP_NAME ) ? true : false;
	}


        public override String getUid()
        {
		// TODO Auto-generated method stub
		return getAppRefLabel();
	}


        public override String getTagName()
        {
		// TODO Auto-generated method stub
		return PlmxmlUtil.PLMXML_DATASET;
	}

	
	public String getAppRefLabel() {
		// TODO Auto-generated method stub
		return appRefLabel;
	}

	
	public String getAppRefVersion() {
		// TODO Auto-generated method stub
		return appRefVersion;
	}

	
	public String getAppRefName() {
		// TODO Auto-generated method stub
		return applicationName;
	}

	
	public void setAppRefLabel(String a) {
		this.appRefLabel = a;
		
	}

	
	public void setAppRefVersion(String a) {
		this.appRefVersion = a;
	}

	
	public void setAppRefName(String a) {
		this.applicationName = a;
	}

	
	public DescriptionBase getDescObj() {
		// TODO Auto-generated method stub
		return getObj();
	}


    public override Image getImage()
    {
		// TODO Auto-generated method stub
		return null;
	}


    public override String getName()
    {
		// TODO Auto-generated method stub
		return getUid();
	}


    public override Object[] getChildren() 
	{
		List<ICadexComparable> allChilds = new List<ICadexComparable>();
		
		foreach(ICadexGenericFile line in this.getFiles())
		{
			allChilds.Add( line );
		}
		
		return allChilds.ToArray();
	}


    public override int getChildrenCount()
    {
		Object[] objs = this.getChildren();
		return objs != null ? objs.Length : 0 ;
	}



    public override void generateDelta()
    {
		// TODO Auto-generated method stub
		
	}

	public AssociatedDataSet getRelNode() {
		return relNode;
	}

	public void setRelNode(AssociatedDataSet relNode) {
		this.relNode = relNode;
	}

    }
}

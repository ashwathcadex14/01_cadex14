using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InfoExPLMXMLSDK.com.infoex.plmxml.types;
using CompareEngine.Model.Interfaces;
using System.Drawing;

namespace CompareEngine.Model.Plmxml
{

    public class PlmxmlFile : CadexGenericFile<ExternalFile>, IPlmxmlGenericObject
    {
        protected String applicationName = "";
        protected String appRefLabel = "";
        protected String appRefVersion = "";

        public PlmxmlFile(ExternalFile obj)
            : base(obj)
        {
            PlmxmlUtil.populateAppRef(this);
            // TODO Auto-generated constructor stub
        }


        public override void setProperties()
        {
            this.addAttr("format", (String)this.getObj().getAttributeValueAsString("format"));
            String locationRef = (String)this.getObj().getAttributeValueAsString("locationRef");
            String orgFileName = "";
            if (locationRef.Contains("/"))
                orgFileName = locationRef.Substring(locationRef.IndexOf("/") + 1, locationRef.Length - locationRef.IndexOf("/") - 1);
            else
                orgFileName = locationRef.Substring(locationRef.IndexOf("\\") + 1, locationRef.Length - locationRef.IndexOf("\\") - 1);

            this.addAttr("location", locationRef);
            this.addAttr("original_file_name", orgFileName);
            this.addAttr("object_string", orgFileName);
            PlmxmlUtil.addUserData(this, this.getObj());
        }


        public override String getOriginalFileName()
        {
            return this.getAttr("original_file_name");
        }


        public override bool doDeepCompare(ICadexComparable obj)
        {
		    // TODO Auto-generated method stub
		    return base.doDeepCompare(obj);
	    }


        public override bool isNew()
        {
		    // TODO Auto-generated method stub
		    return !applicationName.Equals( PlmxmlUtil.TEAMCENTER_APP_NAME ) ? true : false;
	    }


        public override String getUid()
        {
		    // TODO Auto-generated method stub
		    return getAppRefLabel();
	    }


        public override String getTagName()
        {
		    // TODO Auto-generated method stub
		    return PlmxmlUtil.PLMXML_FILE;
	    }

	    
	    public String getAppRefLabel() {
		    // TODO Auto-generated method stub
		    return appRefLabel;
	    }

	    
	    public String getAppRefVersion() {
		    // TODO Auto-generated method stub
		    return appRefVersion;
	    }

	    
	    public String getAppRefName() {
		    // TODO Auto-generated method stub
		    return applicationName;
	    }

	    
	    public void setAppRefLabel(String a) {
		    this.appRefLabel = a;
		
	    }

	    
	    public void setAppRefVersion(String a) {
		    this.appRefVersion = a;
	    }

	    
	    public void setAppRefName(String a) {
		    this.applicationName = a;
	    }

	    
	    public DescriptionBase getDescObj() {
		    // TODO Auto-generated method stub
		    return getObj();
	    }


        public override Image getImage()
        {
		    // TODO Auto-generated method stub
		    return null;
	    }


        public override String getName()
        {
		    // TODO Auto-generated method stub
		    return getUid();
	    }


        public override String getType()
        {
		    // TODO Auto-generated method stub
		    return "ImanFile";
	    }


        public override Object[] getChildren()
        {
		    // TODO Auto-generated method stub
		    return null;
	    }


        public override int getChildrenCount()
        {
		    Object[] objs = this.getChildren();
		    return objs != null ? objs.Length : 0 ;
	    }


        public override void generateDelta()
        {
		    // TODO Auto-generated method stub
		
	    }

    }
}

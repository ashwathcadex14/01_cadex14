using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InfoExPLMXMLSDK.com.infoex.plmxml.types;
using CompareEngine.Model.Interfaces;
using System.Drawing;

namespace CompareEngine.Model.Plmxml
{

    public class PlmxmlItem : CadexGenericItem<Product>, IPlmxmlGenericObject
    {
        protected String applicationName = "";
        protected String appRefLabel = "";
        protected String appRefVersion = "";

        public PlmxmlItem(Product item)
            : base(item)
        {
            PlmxmlUtil.populateAppRef(this);
            addObjString();
        }


        public override void setProperties()
        {
            this.addAttr("item_id", (String)this.getObj().getAttributeValueAsString("productId"));
            this.addAttr("object_name", (String)this.getObj().getAttributeValueAsString("name"));
        }


        public override bool doDeepCompare(ICadexComparable obj)
        {
		    // TODO Auto-generated method stub
		    return base.doDeepCompare(obj);
	    }
	
	    private void addObjString()
	    {
		    String objName = this.getAttr( "object_name" ) ;
		    addAttr("object_string", getAttr( "item_id" )+ ";" + objName);
	    }


        public override bool isNew()
        {
		    // TODO Auto-generated method stub
		    return !applicationName.Equals( PlmxmlUtil.TEAMCENTER_APP_NAME ) ? true : false;
	    }


        public override String getUid()
        {
		    // TODO Auto-generated method stub
		    return getAppRefLabel();
	    }


        public override String getTagName()
        {
		    // TODO Auto-generated method stub
		    return PlmxmlUtil.PLMXML_PRODUCT;
	    }

	
	    public String getAppRefLabel() {
		    // TODO Auto-generated method stub
		    return appRefLabel;
	    }

	
	    public String getAppRefVersion() {
		    // TODO Auto-generated method stub
		    return appRefVersion;
	    }

	
	    public String getAppRefName() {
		    // TODO Auto-generated method stub
		    return applicationName;
	    }

	
	    public void setAppRefLabel(String a) {
		    this.appRefLabel = a;
		
	    }

	
	    public void setAppRefVersion(String a) {
		    this.appRefVersion = a;
	    }

	
	    public void setAppRefName(String a) {
		    this.applicationName = a;
	    }

	
	    public DescriptionBase getDescObj() {
		    // TODO Auto-generated method stub
		    return getObj();
	    }


        public override Image getImage()
        {
		    // TODO Auto-generated method stub
		    return null;
	    }


        public override String getName()
        {
		    // TODO Auto-generated method stub
		    return getUid();
	    }


        public override String getType()
        {
		    // TODO Auto-generated method stub
		    return "Item";
	    }


        public override Object[] getChildren()
        {
		    // TODO Auto-generated method stub
		    return null;
	    }


        public override int getChildrenCount()
        {
		    Object[] objs = this.getChildren();
		    return objs != null ? objs.Length : 0 ;
	    }



        public override void generateDelta()
        {
		    // TODO Auto-generated method stub
		
	    }

    }
}

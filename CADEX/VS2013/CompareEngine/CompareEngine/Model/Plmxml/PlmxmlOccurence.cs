using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompareEngine.Model.Interfaces;
using CompareEngine.Compare;
using InfoExPLMXMLSDK.com.infoex.plmxml.types;
using System.Drawing;

namespace CompareEngine.Model.Plmxml
{

    public class PlmxmlOccurence : CadexGenericLineObject<Occurrence>, IPlmxmlGenericObject
    {
        protected String applicationName = "";
        protected String appRefLabel = "";
        protected String appRefVersion = "";

        public PlmxmlOccurence(Occurrence lineObj)
            : base(lineObj)
        {
            PlmxmlUtil.populateAppRef(this);
            addObjString();
            // TODO Auto-generated constructor stub
        }


        public override void setProperties()
        {
            PlmxmlUtil.addUserData(this, this.getObj());
        }


        public override List<ICadexGenericLineObject> getChildLines()
        {

            List<ICadexGenericLineObject> childList = new List<ICadexGenericLineObject>();

            //IdBase[] objs = this.getObj().resolveOccurrenceRefs();

            List<IdBase> objs = this.getObj().GetHandles("occurrenceRefs");

            foreach (IdBase obj in objs)
            {
                childList.Add((ICadexGenericLineObject)new PlmxmlOccurence((Occurrence)obj));
            }

            return childList;
        }


        public override ICadexGenericRevision getItemRevision()
        {

            ProductRevision rev = null;

            rev = (ProductRevision)this.getObj().getInstancedRef();

            return (ICadexGenericRevision)new PlmxmlRevision(rev);
        }


        public override Object[] getChildren()
        {
            List<ICadexComparable> allChilds = new List<ICadexComparable>();
		
		    foreach(ICadexGenericLineObject line in this.returnChildren())
		    {
			    allChilds.Add( line );
		    }
		
            //try {
            //    allChilds.Add( getLineRev() );
            //} catch (Exception e) {
            //    // Needs Proper Handling
            //    //InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);
            //    throw;
            //}
		
		    return allChilds.ToArray();
        }



        public override bool Equals(object other)
        {
            if (other is ITypedElement)
                return getName().Equals(((ITypedElement)other).getName());
            return false;
        }



        public override int GetHashCode()
        {
            return getName().GetHashCode();
        }


        public override String ToString()
        {
            return getName();
        }


        public override String getName()
        {
		    return getUid();
	    }

        private void addObjString()
	    {
		    ICadexGenericObject lineRev = ((ICadexGenericObject) getLineRev());
		    if(lineRev!=null)
		    {
			    String revId = lineRev.getAttr( "item_revision_id" ) ;
			    String objName = lineRev.getAttr( "object_name" ) ;
			    String itemId = ((ICadexGenericObject) getLineRev().getRevItem()).getAttr( "item_id" ) ;
			    addAttr("object_string", itemId + "/" + revId + ";" + objName);
		    }
	    }


        public override Image getImage()
        {
		    // TODO Auto-generated method stub
		    return null;
	    }


        public override String getType()
        {
		    // TODO Auto-generated method stub
		    return "Occurrence";
	    }


        public override bool doDeepCompare(ICadexComparable obj)
        {
		    // TODO Auto-generated method stub
		    return base.doDeepCompare(obj);
	    }


        public override bool isNew()
        {
		    // TODO Auto-generated method stub
		    return !applicationName.Equals( PlmxmlUtil.TEAMCENTER_APP_NAME ) ? true : false;
	    }


        public override String getUid()
        {
		    // TODO Auto-generated method stub
		    return getAppRefLabel();
	    }


        public override String getTagName()
        {
		    // TODO Auto-generated method stub
		    return PlmxmlUtil.PLMXML_OCCURRENCE;
	    }

	
	    public String getAppRefLabel() {
		    // TODO Auto-generated method stub
		    return appRefLabel;
	    }

	
	    public String getAppRefVersion() {
		    // TODO Auto-generated method stub
		    return appRefVersion;
	    }

	
	    public String getAppRefName() {
		    // TODO Auto-generated method stub
		    return applicationName;
	    }

	
	    public void setAppRefLabel(String a) {
		    this.appRefLabel = a;
		
	    }

	
	    public void setAppRefVersion(String a) {
		    this.appRefVersion = a;
	    }

	
	    public void setAppRefName(String a) {
		    this.applicationName = a;
	    }

	
	    public DescriptionBase getDescObj() {
		    // TODO Auto-generated method stub
		    return getObj();
	    }


        public override int getChildrenCount()
        {
		    Object[] objs = this.getChildren();
		    return objs != null ? objs.Length : 0 ;
	    }



        public override void generateDelta()
        {
		    // TODO Auto-generated method stub
		
	    }
    }
}

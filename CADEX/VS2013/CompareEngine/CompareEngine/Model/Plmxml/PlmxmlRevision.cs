using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompareEngine.Model.Interfaces;
using CompareEngine.Compare;
using InfoExPLMXMLSDK.com.infoex.plmxml.types;
using InfoExPLMXMLSDK.com.infoex.plmxml.generic;
using System.Drawing;


namespace CompareEngine.Model.Plmxml
{

    public class PlmxmlRevision : CadexGenericRevision<ProductRevision>, IPlmxmlGenericObject
    {

        protected String applicationName = "";
        protected String appRefLabel = "";
        protected String appRefVersion = "";

        public PlmxmlRevision(ProductRevision obj)
            : base(obj)
        {
            PlmxmlUtil.populateAppRef(this);
            addObjString();
        }


        public override List<ICadexGenericDataset> getAttachments()
        {
            return getDatasets();
        }


        public override void setProperties()
        {
            this.addAttr("item_revision_id", this.getObj().getAttributeValueAsString("revision"));
            this.addAttr("object_name", this.getObj().getAttributeValueAsString("name"));
        }

        private void addObjString()
        {
            ICadexGenericObject lineItem = ((ICadexGenericObject)getRevItem());
            if (lineItem != null)
            {
                String revId = this.getAttr("item_revision_id");
                String objName = lineItem.getAttr("object_name");
                String itemId = lineItem.getAttr("item_id");
                addAttr("object_string", itemId + "/" + revId + ";" + objName);
            }
        }

        public override ICadexGenericItem getItem()
        {
            return (ICadexGenericItem)new PlmxmlItem((Product) this.getObj().getDocument().resolveId(PLMXMLHelper.removeHashFromID(this.getObj().masterRef)));
        }

        public List<ICadexGenericDataset> getDatasets()
        {
            List<ICadexGenericDataset> datasets = new List<ICadexGenericDataset>();
            AttributeBase[] baseObj = this.getObj().getAttributes("AssociatedDataSet");

            if (baseObj.Length > 0)
            {
                for (int i = 0; i < baseObj.Length; i++)
                {
                    AssociatedDataSet assDset = (AssociatedDataSet)baseObj[i];
                    DataSet objBase = (DataSet)assDset.getDocument().resolveId(assDset.dataSetRef);
                    if (objBase != null)
                    {
                        datasets.Add(new PlmxmlDataset((DataSet)objBase, assDset.getAttributeValueAsString("role")));
                    }
                }
            }

            return datasets;
        }


        public override bool doDeepCompare(ICadexComparable obj)
        {
		    // TODO Auto-generated method stub
		    return base.doDeepCompare(obj);
	    }


        public override bool isNew()
        {
		    // TODO Auto-generated method stub
		    return !applicationName.Equals( PlmxmlUtil.TEAMCENTER_APP_NAME ) ? true : false;
	    }


        public override String getUid()
        {
		    // TODO Auto-generated method stub
		    return getAppRefVersion();
	    }


        public override String getTagName()
        {
		    // TODO Auto-generated method stub
		    return PlmxmlUtil.PLMXML_PRODUCT_REVISION;
	    }

	    
	    public String getAppRefLabel() {
		    // TODO Auto-generated method stub
		    return appRefLabel;
	    }

	    
	    public String getAppRefVersion() {
		    // TODO Auto-generated method stub
		    return appRefVersion;
	    }

	    
	    public String getAppRefName() {
		    // TODO Auto-generated method stub
		    return applicationName;
	    }

	    
	    public void setAppRefLabel(String a) {
		    this.appRefLabel = a;
		
	    }

	    
	    public void setAppRefVersion(String a) {
		    this.appRefVersion = a;
	    }

	    
	    public void setAppRefName(String a) {
		    this.applicationName = a;
	    }

	    
	    public DescriptionBase getDescObj() {
		    // TODO Auto-generated method stub
		    return getObj();
	    }


        public override Object[] getChildren()
        {
		    List<ICadexComparable> allChilds = new List<ICadexComparable>();
		
		    foreach(ICadexGenericDataset line in this.getAttaches())
		    {
			    allChilds.Add( line );
		    }
		
		    try {
			    allChilds.Add( getRevItem() );
		    } catch (Exception e) {
			    // Needs Proper Handling
                //InfoExCommon.Utils.LoggerUtil.error(GetType(), e.Message, e);
                throw;
		    }
		
		    return allChilds.ToArray();
	    }


        public override Image getImage()
        {
		    // TODO Auto-generated method stub
		    return null;
	    }


        public override String getName()
        {
		    // TODO Auto-generated method stub
		    return getUid();
	    }


        public override String getType()
        {
		    // TODO Auto-generated method stub
		    return "ItemRevision";
	    }


        public override int getChildrenCount()
        {
		    Object[] objs = this.getChildren();
		    return objs != null ? objs.Length : 0 ;
	    }


        public override void generateDelta()
        {
		    // TODO Auto-generated method stub
		
	    }

    }
}

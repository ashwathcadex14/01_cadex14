using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompareEngine.Model.Interfaces;
using InfoExPLMXMLSDK.com.infoex.plmxml.types;

namespace CompareEngine.Model.Plmxml
{
    public class PlmxmlUtil
    {
        public static String TEAMCENTER_APP_NAME = "Teamcenter";
        public static String THIRD_PARTY_APP = "Cadex";
        public static String PLMXML_PRODUCT_REVISION = "ProductRevision";
        public static String PLMXML_PRODUCT = "Product";
        public static String PLMXML_DATASET = "DataSet";
        public static String PLMXML_FILE = "ExternalFile";
        public static String PLMXML_OCCURRENCE = "Occurrence";
        public static bool CONSIDER_MISSING_ATTRIBUTES_AS_CHANGE = false;

        public static void addUserData(ICadexGenericObject obj, AttribOwnerBase tag)
        {
            AttributeBase[] userDatas = tag.getAttributes("UserData");
            for (int i = 0; i < userDatas.Length; i++)
            {
                UserDataElement[] userValues = ((UserData)userDatas[i]).Items;

                for (int j = 0; j < userValues.Length; j++)
                {
                    obj.addAttr(userValues[j].getTitle(), userValues[j].getValue());
                }

            }
        }

        public static void populateAppRef(IPlmxmlGenericObject plmxmlObject)
	    {
		    ApplicationRef[] appRef =  plmxmlObject.getDescObj().getApplicationRefs();
		
		    if(appRef.Length == 1)
		    {
			    plmxmlObject.setAppRefLabel(appRef[0].getLabel());
			    plmxmlObject.setAppRefVersion(appRef[0].getVersion());
			    plmxmlObject.setAppRefName(appRef[0].getApplication());
		    }
	    }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace InfoExCommon.Logger
{    
    public interface ILogger
    {        
        void LogException(Exception exception, string methodName, int lineNo, string fileName);
        void LogError(string message, string methodName, int lineNo, string fileName);
        void LogWarningMessage(string message, string methodName, int lineNo, string fileName);
        void LogInfoMessage(string message, string methodName, int lineNo, string fileName);
        void LogDebugMessage(string message, string methodName, int lineNo, string fileName);
    }
}
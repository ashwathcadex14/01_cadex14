﻿using System;
using System.Collections.Generic;
using System.Linq;using System.Text;
using System.Threading.Tasks;
using log4net;
using System.Globalization;
using System.IO;
using log4net.Repository;
using log4net.Appender;
using InfoExCommon.Appender;

namespace InfoExCommon.Logger
{    
    public class InfoExLogger : ILogger
    {
        private static ILog log = null;
        private static LogWatcher logWatcher = null;
        static InfoExLogger() 
        {            
            var log4NetConfigDirectory = 
                AppDomain.CurrentDomain.RelativeSearchPath ?? AppDomain.CurrentDomain.BaseDirectory;
            var log4NetConfigFilePath = Path.Combine(log4NetConfigDirectory, "log4net.config");
            log4net.Config.XmlConfigurator.ConfigureAndWatch(new FileInfo(log4NetConfigFilePath));
            log = LogManager.GetLogger(typeof(InfoExLogger));
            log4net.GlobalContext.Properties["host"] = Environment.MachineName;
            logWatcher = new LogWatcher();
        }       
        
        public InfoExLogger(Type logClass)
        {           
            log = LogManager.GetLogger(logClass);
        }        
        
        #region ILogger Members
        public void LogException(Exception exception, string methodName, int lineNo, string fileName)
        {           
            if (log.IsErrorEnabled)
                log.Error(string.Format(CultureInfo.InvariantCulture, "{0}(), File: {2}, Line:{1} ===> {3}", methodName, lineNo, fileName, exception.Message), exception);
        }        
        
        public void LogError(string message, string methodName, int lineNo, string fileName)
        {            
            if (log.IsErrorEnabled)
                log.Error(string.Format(CultureInfo.InvariantCulture, "{0}(), File: {2}, Line:{1} ===> {3}", methodName, lineNo, fileName, message)); 
        }       
        public void LogWarningMessage(string message, string methodName, int lineNo, string fileName)
        {            
            if (log.IsWarnEnabled)
                log.Warn(string.Format(CultureInfo.InvariantCulture, "{0}(), File: {2}, Line:{1} ===> {3}", methodName, lineNo, fileName, message));
        }       
        public void LogInfoMessage(string message, string methodName, int lineNo, string fileName)
        {            
            if (log.IsInfoEnabled)
                log.Info(string.Format(CultureInfo.InvariantCulture, "{0}(), File: {2}, Line:{1} ===> {3}", methodName, lineNo, fileName, message));
        }        
        public void LogDebugMessage(string message, string methodName, int lineNo, string fileName)
        {             
            if (log.IsDebugEnabled)
                log.Debug(string.Format(CultureInfo.InvariantCulture, "{0}(), File: {2}, Line:{1} ===> {3}", methodName, lineNo, fileName, message));
        }

        public static string GetCurrentLog()
        {
            return logWatcher.LogContent;
        }
        #endregion    
    }
}
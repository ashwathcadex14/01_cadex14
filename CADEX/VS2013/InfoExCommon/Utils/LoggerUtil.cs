﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InfoExCommon.Logger;

namespace InfoExCommon.Utils
{
    public class LoggerUtil
    {
        private static void log(Type type, LogType logType, String message, Exception e, string memberName, string sourceFilePath, int sourceLineNumber)
        {
            ILogger logger = new InfoExLogger(type);
            if (logType.Equals(LogType.error) && e != null)
                logger.LogException(e, memberName, sourceLineNumber, sourceFilePath);

            if (logType.Equals(LogType.error) && e == null)
                logger.LogError(message, memberName, sourceLineNumber, sourceFilePath);

            if (logType.Equals(LogType.warn))
                logger.LogWarningMessage(message, memberName, sourceLineNumber, sourceFilePath);
            if (logType.Equals(LogType.info))
                logger.LogInfoMessage(message, memberName, sourceLineNumber, sourceFilePath);
            if (logType.Equals(LogType.debug))
                logger.LogDebugMessage(message, memberName, sourceLineNumber, sourceFilePath);
        }

        public static void error(Type type, String message, Exception e,
            [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
        {
            log(type, LogType.error, message, e, memberName, sourceFilePath, sourceLineNumber);
        }

        public static void info(Type type, String message,
            [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
        {
            log(type, LogType.info, message, null, memberName, sourceFilePath, sourceLineNumber);
        }
        public static void warn(Type type, String message,
            [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
        {
            log(type, LogType.warn, message, null, memberName, sourceFilePath, sourceLineNumber);
        }
        public static void debug(Type type, String message, Exception e,
            [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
        {
            log(type, LogType.debug, message, e, memberName, sourceFilePath, sourceLineNumber);
        }
        public static void trace(Type type, String message, Exception e,
            [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
        {
            log(type, LogType.trace, message, e, memberName, sourceFilePath, sourceLineNumber);
        }
        public static void fatal(Type type, String message, Exception e,
            [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
        {
            log(type, LogType.fatal, message, e, memberName, sourceFilePath, sourceLineNumber);
        }
        public static string FlattenException(Exception exception)
        {
            var stringBuilder = new StringBuilder();
            while (exception != null)
            {
                stringBuilder.AppendLine(exception.Message);
                stringBuilder.AppendLine(exception.StackTrace);

                exception = exception.InnerException;
            }
            return stringBuilder.ToString();
        }
        public enum LogType
        {
            error,
            info,
            warn,
            debug,
            trace,
            fatal
        }
    }
}

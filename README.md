INFO EXCHANGE
=============
Copyright (c) 2015 BAVIS TECHNOLOGIES PVT. LTD
==============================================
Unpublished - All Rights Reserved                      
==================================

OVERVIEW
--------
The solution aims in providing a comprehensive mechanism of exchanging the Teamcenter Data with a Supplier and ensure a
seamless OEM Supplier collaborative design process.

VERSION
-------
Stable - ALPHA 3.0
Unstable - ALPHA 4.1

DOWNLOADS
---------
All the versions of the solution is available in the following location
https://gitlab.com/ashwathcadex14/InfoExchange.git

UPDATE
------
Current Active branch is Alpha4.0. It is going through a series of testing as it 
as undergone a critical code migration from Original PLMXML SDK to a custom 
PLMXML SDK. Alpha4.0 will be merged with master after declared stable.

CONFIGURATION
-------------
1. Install the BMIDE package for the TC10 in a Teamcenter Corporate Server.
2. Copy the tool to any machine to run it.

Well that's vague isn't ? We will bring in a clear documentation about the installation soon. 

SYSTEM REQUIREMENTS
-------------------
1. A Teamcenter 10.1 corporate server in a Windows Machine. 
2. A Windows client machine with .NET Framework 4.5
3. Java Runtime Environment 1.7+

MODULES
-------
1. Teamcenter Configurations for OEM activities and package export. 
2. A .NET tool for Supplier PDM operations.
3. File Exchange server for package transfer mechanism between OEM and Supplier.
4. A RCP based Comparator tool for comparing PLMXMLs.
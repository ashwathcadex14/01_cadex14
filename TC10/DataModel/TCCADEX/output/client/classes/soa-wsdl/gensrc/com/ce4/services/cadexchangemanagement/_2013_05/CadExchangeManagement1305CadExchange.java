/**
 * CadExchangeManagement1305CadExchange.java
 *
 * This file was auto-generated from WSDL using the
 * Teamcenter Services Ant tool Wsdl2SEI 
 *
 * Copyright (c) 2004 UGS. All Rights Reserved
 * This software and related documentation are proprietary to UGS.
 *
 */
package com.ce4.services.cadexchangemanagement._2013_05;

/**
 * 
 */
public interface CadExchangeManagement1305CadExchange extends java.rmi.Remote
{
    /**
     * .
     */
    public com.teamcenter.schemas.soa._2006_03.base.ServiceData createExportPackage ( com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange.CreateExportPackageInput in0 )
             throws java.rmi.RemoteException, com.teamcenter.schemas.soa._2006_03.exceptions.InternalServerException, com.teamcenter.schemas.soa._2006_03.exceptions.InvalidUserException;

    /**
     * .
     */
    public com.teamcenter.schemas.soa._2006_03.base.ServiceData importExchangePackage ( com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange.ImportExchangePackageInput in0 )
             throws java.rmi.RemoteException, com.teamcenter.schemas.soa._2006_03.exceptions.InternalServerException, com.teamcenter.schemas.soa._2006_03.exceptions.InvalidUserException;

    /**
     * .
     */
    public com.teamcenter.schemas.soa._2006_03.base.ServiceData setExpChkOut ( com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange.SetExpChkOutInput in0 )
             throws java.rmi.RemoteException, com.teamcenter.schemas.soa._2006_03.exceptions.InternalServerException, com.teamcenter.schemas.soa._2006_03.exceptions.InvalidUserException;


}

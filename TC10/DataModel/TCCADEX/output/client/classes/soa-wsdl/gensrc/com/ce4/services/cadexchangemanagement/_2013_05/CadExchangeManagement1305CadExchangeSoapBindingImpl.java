/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

   Auto-generated source from service interface.
                 DO NOT EDIT

 ==================================================
*/

package com.ce4.services.cadexchangemanagement._2013_05;

import com.teamcenter.mld.logging.Log;
import com.teamcenter.soa.ptier.SoapInjector;


/**
 * 
 */
public class CadExchangeManagement1305CadExchangeSoapBindingImpl implements com.ce4.services.cadexchangemanagement._2013_05.CadExchangeManagement1305CadExchange
{

    private static Log log = new Log(CadExchangeManagement1305CadExchangeSoapBindingImpl.class, true);
    private static final String servicePort = "CadExchangeManagement-2013-05-CadExchange";
    
    
    /**
     * .    
     */
    public com.teamcenter.schemas.soa._2006_03.base.ServiceData importExchangePackage ( com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange.ImportExchangePackageInput in0 )
        throws java.rmi.RemoteException, com.teamcenter.schemas.soa._2006_03.exceptions.InternalServerException, com.teamcenter.schemas.soa._2006_03.exceptions.InvalidUserException
    {
         log.debug("CadExchangeManagement1305CadExchangeSoapBindingImpl.importExchangePackage()");
 
         return (com.teamcenter.schemas.soa._2006_03.base.ServiceData)SoapInjector.sendRequestToPTier( in0, servicePort, "importExchangePackage" );
             
     }
    
    /**
     * .    
     */
    public com.teamcenter.schemas.soa._2006_03.base.ServiceData createExportPackage ( com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange.CreateExportPackageInput in0 )
        throws java.rmi.RemoteException, com.teamcenter.schemas.soa._2006_03.exceptions.InternalServerException, com.teamcenter.schemas.soa._2006_03.exceptions.InvalidUserException
    {
         log.debug("CadExchangeManagement1305CadExchangeSoapBindingImpl.createExportPackage()");
 
         return (com.teamcenter.schemas.soa._2006_03.base.ServiceData)SoapInjector.sendRequestToPTier( in0, servicePort, "createExportPackage" );
             
     }
    
    /**
     * .    
     */
    public com.teamcenter.schemas.soa._2006_03.base.ServiceData setExpChkOut ( com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange.SetExpChkOutInput in0 )
        throws java.rmi.RemoteException, com.teamcenter.schemas.soa._2006_03.exceptions.InternalServerException, com.teamcenter.schemas.soa._2006_03.exceptions.InvalidUserException
    {
         log.debug("CadExchangeManagement1305CadExchangeSoapBindingImpl.setExpChkOut()");
 
         return (com.teamcenter.schemas.soa._2006_03.base.ServiceData)SoapInjector.sendRequestToPTier( in0, servicePort, "setExpChkOut" );
             
     }
    

}
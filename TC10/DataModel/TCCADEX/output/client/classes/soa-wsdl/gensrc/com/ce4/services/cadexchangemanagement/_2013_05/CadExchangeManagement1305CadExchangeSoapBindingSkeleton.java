/**
 * CadExchangeManagement1305CadExchangeSoapBindingSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ce4.services.cadexchangemanagement._2013_05;

public class CadExchangeManagement1305CadExchangeSoapBindingSkeleton implements com.ce4.services.cadexchangemanagement._2013_05.CadExchangeManagement1305CadExchange, org.apache.axis.wsdl.Skeleton {
    private com.ce4.services.cadexchangemanagement._2013_05.CadExchangeManagement1305CadExchange impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ce4.com/Schemas/CadExchangeManagement/2013-05/CadExchange", "CreateExportPackageInput"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ce4.com/Schemas/CadExchangeManagement/2013-05/CadExchange", "CreateExportPackageInput"), com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange.CreateExportPackageInput.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("createExportPackage", _params, new javax.xml.namespace.QName("http://teamcenter.com/Schemas/Soa/2006-03/Base", "ServiceData"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://teamcenter.com/Schemas/Soa/2006-03/Base", "ServiceData"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "createExportPackage"));
        _oper.setSoapAction("createExportPackage");
        _myOperationsList.add(_oper);
        if (_myOperations.get("createExportPackage") == null) {
            _myOperations.put("createExportPackage", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("createExportPackage")).add(_oper);
        _fault = new org.apache.axis.description.FaultDesc();
        _fault.setName("InternalServerFaultError");
        _fault.setQName(new javax.xml.namespace.QName("http://teamcenter.com/Schemas/Soa/2006-03/Exceptions", "InternalServerFault"));
        _fault.setClassName("com.teamcenter.schemas.soa._2006_03.exceptions.InternalServerException");
        _fault.setXmlType(new javax.xml.namespace.QName("http://teamcenter.com/Schemas/Soa/2006-03/Exceptions", "InternalServerException"));
        _oper.addFault(_fault);
        _fault = new org.apache.axis.description.FaultDesc();
        _fault.setName("InvalidUserFaultError");
        _fault.setQName(new javax.xml.namespace.QName("http://teamcenter.com/Schemas/Soa/2006-03/Exceptions", "InvalidUserFault"));
        _fault.setClassName("com.teamcenter.schemas.soa._2006_03.exceptions.InvalidUserException");
        _fault.setXmlType(new javax.xml.namespace.QName("http://teamcenter.com/Schemas/Soa/2006-03/Exceptions", "InvalidUserException"));
        _oper.addFault(_fault);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ce4.com/Schemas/CadExchangeManagement/2013-05/CadExchange", "ImportExchangePackageInput"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ce4.com/Schemas/CadExchangeManagement/2013-05/CadExchange", "ImportExchangePackageInput"), com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange.ImportExchangePackageInput.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("importExchangePackage", _params, new javax.xml.namespace.QName("http://teamcenter.com/Schemas/Soa/2006-03/Base", "ServiceData"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://teamcenter.com/Schemas/Soa/2006-03/Base", "ServiceData"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "importExchangePackage"));
        _oper.setSoapAction("importExchangePackage");
        _myOperationsList.add(_oper);
        if (_myOperations.get("importExchangePackage") == null) {
            _myOperations.put("importExchangePackage", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("importExchangePackage")).add(_oper);
        _fault = new org.apache.axis.description.FaultDesc();
        _fault.setName("InternalServerFaultError");
        _fault.setQName(new javax.xml.namespace.QName("http://teamcenter.com/Schemas/Soa/2006-03/Exceptions", "InternalServerFault"));
        _fault.setClassName("com.teamcenter.schemas.soa._2006_03.exceptions.InternalServerException");
        _fault.setXmlType(new javax.xml.namespace.QName("http://teamcenter.com/Schemas/Soa/2006-03/Exceptions", "InternalServerException"));
        _oper.addFault(_fault);
        _fault = new org.apache.axis.description.FaultDesc();
        _fault.setName("InvalidUserFaultError");
        _fault.setQName(new javax.xml.namespace.QName("http://teamcenter.com/Schemas/Soa/2006-03/Exceptions", "InvalidUserFault"));
        _fault.setClassName("com.teamcenter.schemas.soa._2006_03.exceptions.InvalidUserException");
        _fault.setXmlType(new javax.xml.namespace.QName("http://teamcenter.com/Schemas/Soa/2006-03/Exceptions", "InvalidUserException"));
        _oper.addFault(_fault);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ce4.com/Schemas/CadExchangeManagement/2013-05/CadExchange", "SetExpChkOutInput"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ce4.com/Schemas/CadExchangeManagement/2013-05/CadExchange", "SetExpChkOutInput"), com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange.SetExpChkOutInput.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("setExpChkOut", _params, new javax.xml.namespace.QName("http://teamcenter.com/Schemas/Soa/2006-03/Base", "ServiceData"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://teamcenter.com/Schemas/Soa/2006-03/Base", "ServiceData"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "setExpChkOut"));
        _oper.setSoapAction("setExpChkOut");
        _myOperationsList.add(_oper);
        if (_myOperations.get("setExpChkOut") == null) {
            _myOperations.put("setExpChkOut", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("setExpChkOut")).add(_oper);
        _fault = new org.apache.axis.description.FaultDesc();
        _fault.setName("InternalServerFaultError");
        _fault.setQName(new javax.xml.namespace.QName("http://teamcenter.com/Schemas/Soa/2006-03/Exceptions", "InternalServerFault"));
        _fault.setClassName("com.teamcenter.schemas.soa._2006_03.exceptions.InternalServerException");
        _fault.setXmlType(new javax.xml.namespace.QName("http://teamcenter.com/Schemas/Soa/2006-03/Exceptions", "InternalServerException"));
        _oper.addFault(_fault);
        _fault = new org.apache.axis.description.FaultDesc();
        _fault.setName("InvalidUserFaultError");
        _fault.setQName(new javax.xml.namespace.QName("http://teamcenter.com/Schemas/Soa/2006-03/Exceptions", "InvalidUserFault"));
        _fault.setClassName("com.teamcenter.schemas.soa._2006_03.exceptions.InvalidUserException");
        _fault.setXmlType(new javax.xml.namespace.QName("http://teamcenter.com/Schemas/Soa/2006-03/Exceptions", "InvalidUserException"));
        _oper.addFault(_fault);
    }

    public CadExchangeManagement1305CadExchangeSoapBindingSkeleton() {
        this.impl = new com.ce4.services.cadexchangemanagement._2013_05.CadExchangeManagement1305CadExchangeSoapBindingImpl();
    }

    public CadExchangeManagement1305CadExchangeSoapBindingSkeleton(com.ce4.services.cadexchangemanagement._2013_05.CadExchangeManagement1305CadExchange impl) {
        this.impl = impl;
    }
    public com.teamcenter.schemas.soa._2006_03.base.ServiceData createExportPackage(com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange.CreateExportPackageInput in0) throws java.rmi.RemoteException, com.teamcenter.schemas.soa._2006_03.exceptions.InternalServerException, com.teamcenter.schemas.soa._2006_03.exceptions.InvalidUserException
    {
        com.teamcenter.schemas.soa._2006_03.base.ServiceData ret = impl.createExportPackage(in0);
        return ret;
    }

    public com.teamcenter.schemas.soa._2006_03.base.ServiceData importExchangePackage(com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange.ImportExchangePackageInput in0) throws java.rmi.RemoteException, com.teamcenter.schemas.soa._2006_03.exceptions.InternalServerException, com.teamcenter.schemas.soa._2006_03.exceptions.InvalidUserException
    {
        com.teamcenter.schemas.soa._2006_03.base.ServiceData ret = impl.importExchangePackage(in0);
        return ret;
    }

    public com.teamcenter.schemas.soa._2006_03.base.ServiceData setExpChkOut(com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange.SetExpChkOutInput in0) throws java.rmi.RemoteException, com.teamcenter.schemas.soa._2006_03.exceptions.InternalServerException, com.teamcenter.schemas.soa._2006_03.exceptions.InvalidUserException
    {
        com.teamcenter.schemas.soa._2006_03.base.ServiceData ret = impl.setExpChkOut(in0);
        return ret;
    }

}

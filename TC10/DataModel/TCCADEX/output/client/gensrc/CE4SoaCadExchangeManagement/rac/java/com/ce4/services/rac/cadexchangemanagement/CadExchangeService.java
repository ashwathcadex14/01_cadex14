/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

   Auto-generated source from service interface.
                 DO NOT EDIT

 ==================================================
*/

package com.ce4.services.rac.cadexchangemanagement;

import com.teamcenter.soa.SoaConstants;
import com.teamcenter.soa.client.Connection;
import com.teamcenter.rac.kernel.TCSession;

/**
 * Service for Cad Exchange operations of TCCADEX
 * <br>
 * <br>
 * <br>
 * <b>Library Reference:</b>
 * <ul>
 * <li type="disc">CE4SoaCadExchangeManagementRac.jar
 * </li>
 * <li type="disc">CE4SoaCadExchangeManagementTypes.jar (runtime dependency)
 * </li>
 * </ul>
 */

public abstract class CadExchangeService
  implements     com.ce4.services.rac.cadexchangemanagement._2013_05.CadExchange
{

    /**
     * 
     * @param session 
     * @return A instance of the service stub for the given TCSession
     */
    public static CadExchangeService getService( TCSession session )
    {

        Connection connection = session.getSoaConnection();
        if(connection.getBinding().equalsIgnoreCase( SoaConstants.REST ))
        {
            return new CadExchangeRestBindingStub( connection );
        }

        throw new IllegalArgumentException("The "+connection.getBinding()+" binding is not supported.");
    }

}

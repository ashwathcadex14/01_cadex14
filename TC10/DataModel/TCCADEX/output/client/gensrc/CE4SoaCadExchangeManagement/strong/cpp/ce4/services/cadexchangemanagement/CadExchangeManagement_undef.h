/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

   Auto-generated source from service interface.
                 DO NOT EDIT

 ==================================================
*/
#include <common/library_indicators.h>


#if !defined(EXPORTLIBRARY)
#   error EXPORTLIBRARY is not defined
#endif

#undef EXPORTLIBRARY

#if !defined(IPLIB)
#   error IPLIB is not defined
#endif

#undef CE4SOACADEXCHANGEMANAGEMENTSTRONG_API
#undef CE4SOACADEXCHANGEMANAGEMENTSTRONGEXPORT
#undef CE4SOACADEXCHANGEMANAGEMENTSTRONGGLOBAL
#undef CE4SOACADEXCHANGEMANAGEMENTSTRONGPRIVATE


/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

   Auto-generated source from service interface.
                 DO NOT EDIT

 ==================================================
*/


#include <ce4/services/cadexchangemanagement/CadexchangeRestBindingStub.hxx>


#include <teamcenter/soa/common/xml/SaxToNodeParser.hxx>
#include <teamcenter/soa/common/xml/XmlUtils.hxx>
#include <teamcenter/soa/client/internal/SessionManager.hxx>
#include <teamcenter/soa/client/internal/ModelManagerImpl.hxx>
#include <teamcenter/soa/client/RuntimeException.hxx>
#include <teamcenter/soa/internal/common/Monitor.hxx>
#include <teamcenter/soa/internal/common/PolicyMarshaller.hxx>

Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub::CadexchangeRestBindingStub( Teamcenter::Soa::Client::Sender* restSender, Teamcenter::Soa::Client::ModelManagerImpl* modelManager )
    : m_restSender( restSender ), m_modelManager( modelManager )
{
}




void Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub::localToWire( const Ce4::Services::Cadexchangemanagement::_2013_05::Cadexchange ::CreateExportPkgInput& localIn, Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPkgInput>& wireOut )
{
    Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject> exPkgRevision_WO( new Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject() ); //MODEL_OBJECT_ELEMENT_TO_WIRE
    if(localIn.exPkgRevision.isNull())
        exPkgRevision_WO->setUid( MODELOBJECT_NULLID );
    else
        exPkgRevision_WO->setUid(localIn.exPkgRevision->getUid());
    wireOut->setExPkgRevision( exPkgRevision_WO );
    Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject> exPkgSessionTM_WO( new Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject() ); //MODEL_OBJECT_ELEMENT_TO_WIRE
    if(localIn.exPkgSessionTM.isNull())
        exPkgSessionTM_WO->setUid( MODELOBJECT_NULLID );
    else
        exPkgSessionTM_WO->setUid(localIn.exPkgSessionTM->getUid());
    wireOut->setExPkgSessionTM( exPkgSessionTM_WO );

}

void Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub::localToWire( const Ce4::Services::Cadexchangemanagement::_2013_05::Cadexchange ::DatasetExportCheckOutInfo& localIn, Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::DatasetExportCheckOutInfo>& wireOut )
{
    Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject> dsetObject_WO( new Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject() ); //MODEL_OBJECT_ELEMENT_TO_WIRE
    if(localIn.dsetObject.isNull())
        dsetObject_WO->setUid( MODELOBJECT_NULLID );
    else
        dsetObject_WO->setUid(localIn.dsetObject->getUid());
    wireOut->setDsetObject( dsetObject_WO );
    wireOut->setIsExport( localIn.isExport ); //BASIC_ELEMENT_TO_WIRE
    wireOut->setIsCheckOut( localIn.isCheckOut ); //BASIC_ELEMENT_TO_WIRE
    Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject> bomLine_WO( new Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject() ); //MODEL_OBJECT_ELEMENT_TO_WIRE
    if(localIn.bomLine.isNull())
        bomLine_WO->setUid( MODELOBJECT_NULLID );
    else
        bomLine_WO->setUid(localIn.bomLine->getUid());
    wireOut->setBomLine( bomLine_WO );
    Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject> bomWindow_WO( new Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject() ); //MODEL_OBJECT_ELEMENT_TO_WIRE
    if(localIn.bomWindow.isNull())
        bomWindow_WO->setUid( MODELOBJECT_NULLID );
    else
        bomWindow_WO->setUid(localIn.bomWindow->getUid());
    wireOut->setBomWindow( bomWindow_WO );

}

void Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub::localToWire( const Ce4::Services::Cadexchangemanagement::_2013_05::Cadexchange ::ImportPackageInput& localIn, Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportPackageInput>& wireOut )
{
    Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject> packageRev_WO( new Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject() ); //MODEL_OBJECT_ELEMENT_TO_WIRE
    if(localIn.packageRev.isNull())
        packageRev_WO->setUid( MODELOBJECT_NULLID );
    else
        packageRev_WO->setUid(localIn.packageRev->getUid());
    wireOut->setPackageRev( packageRev_WO );
    Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject> importTM_WO( new Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject() ); //MODEL_OBJECT_ELEMENT_TO_WIRE
    if(localIn.importTM.isNull())
        importTM_WO->setUid( MODELOBJECT_NULLID );
    else
        importTM_WO->setUid(localIn.importTM->getUid());
    wireOut->setImportTM( importTM_WO );

}


Teamcenter::Soa::Client::ServiceData Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub::createExportPackage( const Ce4::Services::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPkgInput&  exPkgSessionInput )
{
    Teamcenter::Soa::Client::RequestId requestId( m_restSender );
    Teamcenter::Soa::Internal::Common::Monitor::markStart("Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub::createExportPackage");

    Teamcenter::Soa::Internal::Common::Monitor::markStart("Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub::createExportPackage.localToWire");
    Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPackageInput> wireIn( new Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPackageInput() );
    Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPkgInput> exPkgSessionInput_WO( new Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPkgInput() ); //STRUCT_ELEMENT_TO_WIRE
    localToWire( exPkgSessionInput, exPkgSessionInput_WO );
    wireIn->setExPkgSessionInput( exPkgSessionInput_WO );

    Teamcenter::Soa::Internal::Common::Monitor::markEnd("Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub::createExportPackage.localToWire");

    Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ServiceData> wireOut;
    wireOut = new Teamcenter::Schemas::Soa::_2006_03::Base::ServiceData();
    {
        Teamcenter::Soa::Common::Xml::XmlStream xmlStreamOut;
        Teamcenter::Soa::Common::Xml::SAXToNodeParser parser;
        
        wireIn->outputXML( xmlStreamOut );   // Serialize the request document
        m_restSender->invoke( "CadExchangeManagement-2013-05-CadExchange", "createExportPackage", xmlStreamOut.str(), parser ); 
        wireOut->parse( parser.getRoot() );  // Parse the return document to the XSD bindings
    } // Free the request XML (xmlStreamOut) and returned DOM Object (parser) from the stack 
  
    Teamcenter::Soa::Internal::Common::Monitor::markStart("Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub::createExportPackage.wireToLocal");                      
    Teamcenter::Soa::Client::ServiceData out = m_modelManager->loadServiceData( wireOut ); // SERVICEDATA_RETURN

    Teamcenter::Soa::Internal::Common::Monitor::markEnd("Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub::createExportPackage.wireToLocal");
    
    Teamcenter::Soa::Internal::Common::Monitor::markEnd("Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub::createExportPackage");
    return out;
}


Teamcenter::Soa::Client::ServiceData Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub::importExchangePackage( const Ce4::Services::Cadexchangemanagement::_2013_05::Cadexchange::ImportPackageInput&  importPkgInput )
{
    Teamcenter::Soa::Client::RequestId requestId( m_restSender );
    Teamcenter::Soa::Internal::Common::Monitor::markStart("Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub::importExchangePackage");

    Teamcenter::Soa::Internal::Common::Monitor::markStart("Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub::importExchangePackage.localToWire");
    Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportExchangePackageInput> wireIn( new Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportExchangePackageInput() );
    Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportPackageInput> importPkgInput_WO( new Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportPackageInput() ); //STRUCT_ELEMENT_TO_WIRE
    localToWire( importPkgInput, importPkgInput_WO );
    wireIn->setImportPkgInput( importPkgInput_WO );

    Teamcenter::Soa::Internal::Common::Monitor::markEnd("Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub::importExchangePackage.localToWire");

    Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ServiceData> wireOut;
    wireOut = new Teamcenter::Schemas::Soa::_2006_03::Base::ServiceData();
    {
        Teamcenter::Soa::Common::Xml::XmlStream xmlStreamOut;
        Teamcenter::Soa::Common::Xml::SAXToNodeParser parser;
        
        wireIn->outputXML( xmlStreamOut );   // Serialize the request document
        m_restSender->invoke( "CadExchangeManagement-2013-05-CadExchange", "importExchangePackage", xmlStreamOut.str(), parser ); 
        wireOut->parse( parser.getRoot() );  // Parse the return document to the XSD bindings
    } // Free the request XML (xmlStreamOut) and returned DOM Object (parser) from the stack 
  
    Teamcenter::Soa::Internal::Common::Monitor::markStart("Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub::importExchangePackage.wireToLocal");                      
    Teamcenter::Soa::Client::ServiceData out = m_modelManager->loadServiceData( wireOut ); // SERVICEDATA_RETURN

    Teamcenter::Soa::Internal::Common::Monitor::markEnd("Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub::importExchangePackage.wireToLocal");
    
    Teamcenter::Soa::Internal::Common::Monitor::markEnd("Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub::importExchangePackage");
    return out;
}


Teamcenter::Soa::Client::ServiceData Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub::setExpChkOut( const std::vector< Ce4::Services::Cadexchangemanagement::_2013_05::Cadexchange::DatasetExportCheckOutInfo >& datasetExportChkOutInfo )
{
    Teamcenter::Soa::Client::RequestId requestId( m_restSender );
    Teamcenter::Soa::Internal::Common::Monitor::markStart("Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub::setExpChkOut");

    Teamcenter::Soa::Internal::Common::Monitor::markStart("Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub::setExpChkOut.localToWire");
    Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::SetExpChkOutInput> wireIn( new Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::SetExpChkOutInput() );
    for( size_t index = 0; index < datasetExportChkOutInfo.size(); ++index ) // STRUCT_VECTOR_ELEMENT_TO_WIRE
    {
        Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::DatasetExportCheckOutInfo> datasetExportChkOutInfo_WO( new Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::DatasetExportCheckOutInfo() );
        localToWire( datasetExportChkOutInfo[index], datasetExportChkOutInfo_WO );
        wireIn->addDatasetExportChkOutInfo( datasetExportChkOutInfo_WO );
    }

    Teamcenter::Soa::Internal::Common::Monitor::markEnd("Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub::setExpChkOut.localToWire");

    Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ServiceData> wireOut;
    wireOut = new Teamcenter::Schemas::Soa::_2006_03::Base::ServiceData();
    {
        Teamcenter::Soa::Common::Xml::XmlStream xmlStreamOut;
        Teamcenter::Soa::Common::Xml::SAXToNodeParser parser;
        
        wireIn->outputXML( xmlStreamOut );   // Serialize the request document
        m_restSender->invoke( "CadExchangeManagement-2013-05-CadExchange", "setExpChkOut", xmlStreamOut.str(), parser ); 
        wireOut->parse( parser.getRoot() );  // Parse the return document to the XSD bindings
    } // Free the request XML (xmlStreamOut) and returned DOM Object (parser) from the stack 
  
    Teamcenter::Soa::Internal::Common::Monitor::markStart("Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub::setExpChkOut.wireToLocal");                      
    Teamcenter::Soa::Client::ServiceData out = m_modelManager->loadServiceData( wireOut ); // SERVICEDATA_RETURN

    Teamcenter::Soa::Internal::Common::Monitor::markEnd("Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub::setExpChkOut.wireToLocal");
    
    Teamcenter::Soa::Internal::Common::Monitor::markEnd("Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub::setExpChkOut");
    return out;
}



using Teamcenter::Soa::Client::SessionManager;
using Teamcenter::Soa::Client::ServiceStub;
using Teamcenter::Soa::Client::ModelManagerImpl;

Ce4::Services::Cadexchangemanagement::CadexchangeService* Ce4::Services::Cadexchangemanagement::CadexchangeService::getService( Teamcenter::Soa::Client::Connection* conn )
{
    SessionManager* smanager = SessionManager::getSessionManager( conn );

    ServiceStub* stub = smanager->getService( "Ce4::Services::Cadexchangemanagement::CadexchangeService" );

    if( stub == 0 )
    {
        stub = new Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub( smanager->getSender(), (ModelManagerImpl*)conn->getModelManager() );
        smanager->registerService( "Ce4::Services::Cadexchangemanagement::CadexchangeService", stub );
    }

    Ce4::Services::Cadexchangemanagement::CadexchangeService* ret = dynamic_cast< Ce4::Services::Cadexchangemanagement::CadexchangeService* >( stub );

    if( ret == 0 )
    {
        throw Teamcenter::Soa::Client::RuntimeException(  Teamcenter::Soa::Client::RuntimeException::InternalError, "Could not cast service to correct type" );
    }

    return ret;        
}


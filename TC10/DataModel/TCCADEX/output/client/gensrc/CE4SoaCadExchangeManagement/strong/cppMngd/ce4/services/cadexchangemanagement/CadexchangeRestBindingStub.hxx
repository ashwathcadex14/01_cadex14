/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

   Auto-generated source from service interface.
                 DO NOT EDIT

 ==================================================
*/

#ifndef CE4_SERVICES_CADEXCHANGEMANAGEMENT_CADEXCHANGERESTBINDINGSTUB_HXX
#define CE4_SERVICES_CADEXCHANGEMANAGEMENT_CADEXCHANGERESTBINDINGSTUB_HXX


#include <new> // for size_t
#include <teamcenter/soa/common/MemoryManager.hxx>
#include <teamcenter/soa/client/internal/ModelManagerImpl.hxx>
#include <teamcenter/soa/client/internal/Sender.hxx>
#include <teamcenter/soa/client/internal/ServiceStub.hxx>

#include <ce4/services/cadexchangemanagement/CadexchangeService.hxx>
#include <ce4/schemas/cadexchangemanagement/_2013_05/cadexchange/CreateExportPkgInput.hxx>
#include <ce4/schemas/cadexchangemanagement/_2013_05/cadexchange/DatasetExportCheckOutInfo.hxx>
#include <ce4/schemas/cadexchangemanagement/_2013_05/cadexchange/ImportPackageInput.hxx>
#include <teamcenter/schemas/soa/_2006_03/base/ServiceData.hxx>
#include <teamcenter/soa/client/model/BOMLine.hxx>
#include <teamcenter/soa/client/model/BOMWindow.hxx>
#include <teamcenter/soa/client/model/Dataset.hxx>
#include <teamcenter/soa/client/model/ItemRevision.hxx>
#include <teamcenter/soa/client/model/TransferMode.hxx>
#include <ce4/schemas/cadexchangemanagement/_2013_05/cadexchange/CreateExportPackageInput.hxx>
#include <ce4/schemas/cadexchangemanagement/_2013_05/cadexchange/CreateExportPkgInput.hxx>
#include <ce4/schemas/cadexchangemanagement/_2013_05/cadexchange/DatasetExportCheckOutInfo.hxx>
#include <ce4/schemas/cadexchangemanagement/_2013_05/cadexchange/ImportExchangePackageInput.hxx>
#include <ce4/schemas/cadexchangemanagement/_2013_05/cadexchange/ImportPackageInput.hxx>
#include <ce4/schemas/cadexchangemanagement/_2013_05/cadexchange/SetExpChkOutInput.hxx>


#pragma warning ( push )
#pragma warning ( disable : 4996  )

#include <ce4/services/cadexchangemanagement/CadExchangeManagement_exports.h>
namespace Ce4
{
    namespace Services
    {
        namespace Cadexchangemanagement
        {
            class CadexchangeRestBindingStub;
        }
    }
}


class CE4SOACADEXCHANGEMANAGEMENTSTRONG_API Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub : public Ce4::Services::Cadexchangemanagement::CadexchangeService, public Teamcenter::Soa::Client::ServiceStub
{
public:
    virtual Teamcenter::Soa::Client::ServiceData createExportPackage ( const Ce4::Services::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPkgInput&  exPkgSessionInput );
    virtual Teamcenter::Soa::Client::ServiceData importExchangePackage ( const Ce4::Services::Cadexchangemanagement::_2013_05::Cadexchange::ImportPackageInput&  importPkgInput );
    virtual Teamcenter::Soa::Client::ServiceData setExpChkOut ( const std::vector< Ce4::Services::Cadexchangemanagement::_2013_05::Cadexchange::DatasetExportCheckOutInfo >& datasetExportChkOutInfo );
    static void localToWire( const Ce4::Services::Cadexchangemanagement::_2013_05::Cadexchange ::CreateExportPkgInput& localIn, Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPkgInput>& wireOut );
    static void localToWire( const Ce4::Services::Cadexchangemanagement::_2013_05::Cadexchange ::DatasetExportCheckOutInfo& localIn, Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::DatasetExportCheckOutInfo>& wireOut );
    static void localToWire( const Ce4::Services::Cadexchangemanagement::_2013_05::Cadexchange ::ImportPackageInput& localIn, Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportPackageInput>& wireOut );




    CadexchangeRestBindingStub( Teamcenter::Soa::Client::Sender* restSender, Teamcenter::Soa::Client::ModelManagerImpl* modelManager );
    
    SOA_CLASS_NEW_OPERATORS_WITH_IMPL("Ce4::Services::Cadexchangemanagement::CadexchangeRestBindingStub")

private:
    Teamcenter::Soa::Client::Sender*              m_restSender;
    Teamcenter::Soa::Client::ModelManagerImpl*    m_modelManager;
};

#pragma warning ( pop )

#include <ce4/services/cadexchangemanagement/CadExchangeManagement_undef.h>
#endif


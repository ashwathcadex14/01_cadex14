/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

   Auto-generated source from service interface.
                 DO NOT EDIT

 ==================================================
*/

#ifndef CE4_SERVICES_CADEXCHANGEMANAGEMENT_CADEXCHANGESERVICE_HXX
#define CE4_SERVICES_CADEXCHANGEMANAGEMENT_CADEXCHANGESERVICE_HXX

#include <ce4/services/cadexchangemanagement/_2013_05/Cadexchange.hxx>



#include <teamcenter/soa/client/Connection.hxx>
#include <new> // for size_t
#include <teamcenter/soa/common/MemoryManager.hxx>

#pragma warning ( push )
#pragma warning ( disable : 4996  )

#include <ce4/services/cadexchangemanagement/CadExchangeManagement_exports.h>

namespace Ce4
{
    namespace Services
    {
        namespace Cadexchangemanagement
        {
            class CadexchangeService;

/**
 * Service for Cad Exchange operations of TCCADEX
 * <br>
 * <br>
 * <br>
 * <b>Library Reference:</b>
 * <ul>
 * <li type="disc">libce4soacadexchangemanagementstrongmngd.dll
 * </li>
 * <li type="disc">libce4soacadexchangemanagementtypes.dll (runtime dependency)
 * </li>
 * </ul>
 */

class CE4SOACADEXCHANGEMANAGEMENTSTRONG_API CadexchangeService
    : public Ce4::Services::Cadexchangemanagement::_2013_05::Cadexchange
{
public:
    static CadexchangeService* getService( Teamcenter::Soa::Client::Connection* );


    /**
     * .
     *
     * @param exPkgSessionInput
     *        Input for export package session
     *
     * @return
     *
     */
    virtual Teamcenter::Soa::Client::ServiceData createExportPackage ( const Ce4::Services::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPkgInput&  exPkgSessionInput ) = 0;

    /**
     * .
     *
     * @param importPkgInput
     *        Input details for importing the exchange package from supplier
     *
     * @return
     *
     */
    virtual Teamcenter::Soa::Client::ServiceData importExchangePackage ( const Ce4::Services::Cadexchangemanagement::_2013_05::Cadexchange::ImportPackageInput&  importPkgInput ) = 0;

    /**
     * .
     *
     * @param datasetExportChkOutInfo
     *        vector of Dataset Export Check Out info
     *
     * @return
     *
     */
    virtual Teamcenter::Soa::Client::ServiceData setExpChkOut ( const std::vector< Ce4::Services::Cadexchangemanagement::_2013_05::Cadexchange::DatasetExportCheckOutInfo >& datasetExportChkOutInfo ) = 0;


    SOA_CLASS_NEW_OPERATORS_WITH_IMPL("CadexchangeService")

};
        }
    }
}


#pragma warning ( pop )

#include <ce4/services/cadexchangemanagement/CadExchangeManagement_undef.h>
#endif


/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

   Auto-generated source from service interface.
                 DO NOT EDIT

 ==================================================
*/

#ifndef CE4_SERVICES_CADEXCHANGEMANAGEMENT__2013_05_CADEXCHANGE_HXX
#define CE4_SERVICES_CADEXCHANGEMANAGEMENT__2013_05_CADEXCHANGE_HXX

#include <teamcenter/soa/client/model/BOMLine.hxx>
#include <teamcenter/soa/client/model/BOMWindow.hxx>
#include <teamcenter/soa/client/model/Dataset.hxx>
#include <teamcenter/soa/client/model/ItemRevision.hxx>
#include <teamcenter/soa/client/model/TransferMode.hxx>


#include <teamcenter/soa/client/ModelObject.hxx>
#include <teamcenter/soa/client/ServiceData.hxx>
#include <teamcenter/soa/client/PartialErrors.hxx>
#include <teamcenter/soa/client/Preferences.hxx>

#include <ce4/services/cadexchangemanagement/CadExchangeManagement_exports.h>

namespace Ce4
{
    namespace Services
    {
        namespace Cadexchangemanagement
        {
            namespace _2013_05
            {
                class Cadexchange;

class CE4SOACADEXCHANGEMANAGEMENTSTRONG_API Cadexchange
{
public:

    struct CreateExportPkgInput;
    struct DatasetExportCheckOutInfo;
    struct ImportPackageInput;

    struct CreateExportPkgInput
    {
        /**
         * Exchange Package Revision
         */
        Teamcenter::Soa::Common::AutoPtr<Teamcenter::Soa::Client::Model::ItemRevision>  exPkgRevision;
        /**
         * Transfer Mode to be used during export
         */
        Teamcenter::Soa::Common::AutoPtr<Teamcenter::Soa::Client::Model::TransferMode>  exPkgSessionTM;
    };

    struct DatasetExportCheckOutInfo
    {
        /**
         * Dataset for setting Exported, CheckOut in Supplier CAD Exchange
         */
        Teamcenter::Soa::Common::AutoPtr<Teamcenter::Soa::Client::Model::Dataset>  dsetObject;
        /**
         * Need to export or not
         */
        bool isExport;
        /**
         * Need to check out or not
         */
        bool isCheckOut;
        /**
         * Line to which dataset is associated
         */
        Teamcenter::Soa::Common::AutoPtr<Teamcenter::Soa::Client::Model::BOMLine>  bomLine;
        /**
         * Window of the current package
         */
        Teamcenter::Soa::Common::AutoPtr<Teamcenter::Soa::Client::Model::BOMWindow>  bomWindow;
    };

    struct ImportPackageInput
    {
        /**
         * Package Revision to import the exchange package
         */
        Teamcenter::Soa::Common::AutoPtr<Teamcenter::Soa::Client::Model::ItemRevision>  packageRev;
        /**
         * Transfer mode to import the exchange package
         */
        Teamcenter::Soa::Common::AutoPtr<Teamcenter::Soa::Client::Model::TransferMode>  importTM;
    };




    /**
     * .
     *
     * @param exPkgSessionInput
     *        Input for export package session
     *
     * @return
     *
     */
    virtual Teamcenter::Soa::Client::ServiceData createExportPackage ( const Ce4::Services::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPkgInput&  exPkgSessionInput ) = 0;

    /**
     * .
     *
     * @param importPkgInput
     *        Input details for importing the exchange package from supplier
     *
     * @return
     *
     */
    virtual Teamcenter::Soa::Client::ServiceData importExchangePackage ( const Ce4::Services::Cadexchangemanagement::_2013_05::Cadexchange::ImportPackageInput&  importPkgInput ) = 0;

    /**
     * .
     *
     * @param datasetExportChkOutInfo
     *        vector of Dataset Export Check Out info
     *
     * @return
     *
     */
    virtual Teamcenter::Soa::Client::ServiceData setExpChkOut ( const std::vector< Ce4::Services::Cadexchangemanagement::_2013_05::Cadexchange::DatasetExportCheckOutInfo >& datasetExportChkOutInfo ) = 0;


protected:
    virtual ~Cadexchange() {}
};
            }
        }
    }
}

#include <ce4/services/cadexchangemanagement/CadExchangeManagement_undef.h>
#endif


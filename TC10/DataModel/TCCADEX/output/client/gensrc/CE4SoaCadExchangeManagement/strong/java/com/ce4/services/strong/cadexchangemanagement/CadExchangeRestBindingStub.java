/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

   Auto-generated source from service interface.
                 DO NOT EDIT

 ==================================================
*/
 
package com.ce4.services.strong.cadexchangemanagement;

import java.util.List;


import com.teamcenter.soa.client.Connection;
import com.teamcenter.soa.internal.client.Sender;
import com.teamcenter.soa.internal.client.model.PopulateModel;

 /**
  * @unpublished
  */
@SuppressWarnings("unchecked")
public class CadExchangeRestBindingStub extends CadExchangeService
{
    private Sender              restSender;
    private PopulateModel       modelManager;
    private Connection          localConnection;
    
    /**
     * Constructor
     * @param connection
     * @unpublished
     */
    public CadExchangeRestBindingStub( Connection connection )
    {
        this.localConnection = connection;
        this.restSender  = connection.getSender();
        this.modelManager= (PopulateModel)this.localConnection.getModelManager();
        com.teamcenter.soa.client.model.StrongObjectFactory.init();
    }

    static com.teamcenter.schemas.soa._2006_03.base.ObjectFactory base_Factory = new 
           com.teamcenter.schemas.soa._2006_03.base.ObjectFactory();

    // each child interface has its own factory and methods for calling


    static com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange.ObjectFactory CadExchange_201305Factory = new 
           com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange.ObjectFactory();
    static final String CADEXCHANGE_201305_PORT_NAME          = "CadExchangeManagement-2013-05-CadExchange";
    static final String CADEXCHANGE_201305_CONTEXT_PATH = "com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange:com.teamcenter.schemas.soa._2006_03.base";

    /**
     * @param local
     * @return wire
     * @unpublished 
     */
    public  static com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange.CreateExportPkgInput toWire( com.ce4.services.strong.cadexchangemanagement._2013_05.CadExchange.CreateExportPkgInput local ) 
    {
 		com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange.CreateExportPkgInput wire = null;
     	wire = CadExchange_201305Factory.createCreateExportPkgInput();

        com.teamcenter.schemas.soa._2006_03.base.ModelObject exPkgRevisionWireIn = null; //WIRE_CREATE
        exPkgRevisionWireIn = base_Factory.createModelObject();
        if(local.exPkgRevision == null)  // MODEL_OBJECT_ELEMENT_TO_WIRE
             exPkgRevisionWireIn.setUid( com.teamcenter.soa.client.model.ModelObject.NULL_ID );
        else
             exPkgRevisionWireIn.setUid(local.exPkgRevision.getUid());
        wire.setExPkgRevision( exPkgRevisionWireIn );
        com.teamcenter.schemas.soa._2006_03.base.ModelObject exPkgSessionTMWireIn = null; //WIRE_CREATE
        exPkgSessionTMWireIn = base_Factory.createModelObject();
        if(local.exPkgSessionTM == null)  // MODEL_OBJECT_ELEMENT_TO_WIRE
             exPkgSessionTMWireIn.setUid( com.teamcenter.soa.client.model.ModelObject.NULL_ID );
        else
             exPkgSessionTMWireIn.setUid(local.exPkgSessionTM.getUid());
        wire.setExPkgSessionTM( exPkgSessionTMWireIn );

		return wire;
	}
    /**
     * @param wire
     * @param modelManager
     * @return local
     * @unpublished 
     */
    public  static com.ce4.services.strong.cadexchangemanagement._2013_05.CadExchange.CreateExportPkgInput toLocal( com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange.CreateExportPkgInput  wire , com.teamcenter.soa.internal.client.model.PopulateModel modelManager ) 
 		
    {
  		com.ce4.services.strong.cadexchangemanagement._2013_05.CadExchange.CreateExportPkgInput local = new com.ce4.services.strong.cadexchangemanagement._2013_05.CadExchange.CreateExportPkgInput();

        local.exPkgRevision = (com.teamcenter.soa.client.model.strong.ItemRevision)modelManager.loadObjectData( wire.getExPkgRevision()); // POMREF_ELEMENT_TO_LOCAL
        local.exPkgSessionTM = (com.teamcenter.soa.client.model.strong.TransferMode)modelManager.loadObjectData( wire.getExPkgSessionTM()); // POMREF_ELEMENT_TO_LOCAL


		return local;
	}

    /**
     * @param local
     * @return wire
     * @unpublished 
     */
    public  static com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange.DatasetExportCheckOutInfo toWire( com.ce4.services.strong.cadexchangemanagement._2013_05.CadExchange.DatasetExportCheckOutInfo local ) 
    {
 		com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange.DatasetExportCheckOutInfo wire = null;
     	wire = CadExchange_201305Factory.createDatasetExportCheckOutInfo();

        com.teamcenter.schemas.soa._2006_03.base.ModelObject dsetObjectWireIn = null; //WIRE_CREATE
        dsetObjectWireIn = base_Factory.createModelObject();
        if(local.dsetObject == null)  // MODEL_OBJECT_ELEMENT_TO_WIRE
             dsetObjectWireIn.setUid( com.teamcenter.soa.client.model.ModelObject.NULL_ID );
        else
             dsetObjectWireIn.setUid(local.dsetObject.getUid());
        wire.setDsetObject( dsetObjectWireIn );
        wire.setIsExport( local.isExport ); //BASIC_ELEMENT_TO_WIRE
        wire.setIsCheckOut( local.isCheckOut ); //BASIC_ELEMENT_TO_WIRE
        com.teamcenter.schemas.soa._2006_03.base.ModelObject bomLineWireIn = null; //WIRE_CREATE
        bomLineWireIn = base_Factory.createModelObject();
        if(local.bomLine == null)  // MODEL_OBJECT_ELEMENT_TO_WIRE
             bomLineWireIn.setUid( com.teamcenter.soa.client.model.ModelObject.NULL_ID );
        else
             bomLineWireIn.setUid(local.bomLine.getUid());
        wire.setBomLine( bomLineWireIn );
        com.teamcenter.schemas.soa._2006_03.base.ModelObject bomWindowWireIn = null; //WIRE_CREATE
        bomWindowWireIn = base_Factory.createModelObject();
        if(local.bomWindow == null)  // MODEL_OBJECT_ELEMENT_TO_WIRE
             bomWindowWireIn.setUid( com.teamcenter.soa.client.model.ModelObject.NULL_ID );
        else
             bomWindowWireIn.setUid(local.bomWindow.getUid());
        wire.setBomWindow( bomWindowWireIn );

		return wire;
	}
    /**
     * @param wire
     * @param modelManager
     * @return local
     * @unpublished 
     */
    public  static com.ce4.services.strong.cadexchangemanagement._2013_05.CadExchange.DatasetExportCheckOutInfo toLocal( com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange.DatasetExportCheckOutInfo  wire , com.teamcenter.soa.internal.client.model.PopulateModel modelManager ) 
 		
    {
  		com.ce4.services.strong.cadexchangemanagement._2013_05.CadExchange.DatasetExportCheckOutInfo local = new com.ce4.services.strong.cadexchangemanagement._2013_05.CadExchange.DatasetExportCheckOutInfo();

        local.dsetObject = (com.teamcenter.soa.client.model.strong.Dataset)modelManager.loadObjectData( wire.getDsetObject()); // POMREF_ELEMENT_TO_LOCAL
        local.isExport = wire.isIsExport(); //BOOLEAN_ELEMENT_TO_LOCAL
        local.isCheckOut = wire.isIsCheckOut(); //BOOLEAN_ELEMENT_TO_LOCAL
        local.bomLine = (com.teamcenter.soa.client.model.strong.BOMLine)modelManager.loadObjectData( wire.getBomLine()); // POMREF_ELEMENT_TO_LOCAL
        local.bomWindow = (com.teamcenter.soa.client.model.strong.BOMWindow)modelManager.loadObjectData( wire.getBomWindow()); // POMREF_ELEMENT_TO_LOCAL


		return local;
	}

    /**
     * @param local
     * @return wire
     * @unpublished 
     */
    public  static com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange.ImportPackageInput toWire( com.ce4.services.strong.cadexchangemanagement._2013_05.CadExchange.ImportPackageInput local ) 
    {
 		com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange.ImportPackageInput wire = null;
     	wire = CadExchange_201305Factory.createImportPackageInput();

        com.teamcenter.schemas.soa._2006_03.base.ModelObject packageRevWireIn = null; //WIRE_CREATE
        packageRevWireIn = base_Factory.createModelObject();
        if(local.packageRev == null)  // MODEL_OBJECT_ELEMENT_TO_WIRE
             packageRevWireIn.setUid( com.teamcenter.soa.client.model.ModelObject.NULL_ID );
        else
             packageRevWireIn.setUid(local.packageRev.getUid());
        wire.setPackageRev( packageRevWireIn );
        com.teamcenter.schemas.soa._2006_03.base.ModelObject importTMWireIn = null; //WIRE_CREATE
        importTMWireIn = base_Factory.createModelObject();
        if(local.importTM == null)  // MODEL_OBJECT_ELEMENT_TO_WIRE
             importTMWireIn.setUid( com.teamcenter.soa.client.model.ModelObject.NULL_ID );
        else
             importTMWireIn.setUid(local.importTM.getUid());
        wire.setImportTM( importTMWireIn );

		return wire;
	}
    /**
     * @param wire
     * @param modelManager
     * @return local
     * @unpublished 
     */
    public  static com.ce4.services.strong.cadexchangemanagement._2013_05.CadExchange.ImportPackageInput toLocal( com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange.ImportPackageInput  wire , com.teamcenter.soa.internal.client.model.PopulateModel modelManager ) 
 		
    {
  		com.ce4.services.strong.cadexchangemanagement._2013_05.CadExchange.ImportPackageInput local = new com.ce4.services.strong.cadexchangemanagement._2013_05.CadExchange.ImportPackageInput();

        local.packageRev = (com.teamcenter.soa.client.model.strong.ItemRevision)modelManager.loadObjectData( wire.getPackageRev()); // POMREF_ELEMENT_TO_LOCAL
        local.importTM = (com.teamcenter.soa.client.model.strong.TransferMode)modelManager.loadObjectData( wire.getImportTM()); // POMREF_ELEMENT_TO_LOCAL


		return local;
	}
	
    @Override 
    public com.teamcenter.soa.client.model.ServiceData createExportPackage( com.ce4.services.strong.cadexchangemanagement._2013_05.CadExchange.CreateExportPkgInput exPkgSessionInput ) 
    
    {
       try
       {
        restSender.pushRequestId();
        com.teamcenter.soa.internal.common.Monitor.markStart("com.ce4.services.strong.cadexchangemanagement.CadExchangeRestBindingStub.createExportPackage");
        com.teamcenter.soa.internal.common.Monitor.markStart("com.ce4.services.strong.cadexchangemanagement.CadExchangeRestBindingStub.createExportPackage.localToWire");
		com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange.CreateExportPackageInput wireIn = null;
        wireIn = CadExchange_201305Factory.createCreateExportPackageInput();
        wireIn.setExPkgSessionInput( toWire( exPkgSessionInput )); //STRUCT_ELEMENT_TO_WIRE

        com.teamcenter.soa.internal.common.Monitor.markEnd  ("com.ce4.services.strong.cadexchangemanagement.CadExchangeRestBindingStub.createExportPackage.localToWire");

        Object outObj = restSender.invoke2( CADEXCHANGE_201305_PORT_NAME, "createExportPackage", wireIn, 
                        CADEXCHANGE_201305_CONTEXT_PATH, CADEXCHANGE_201305_CONTEXT_PATH);
		modelManager.lockModel();
		

        com.teamcenter.soa.internal.common.Monitor.markStart("com.ce4.services.strong.cadexchangemanagement.CadExchangeRestBindingStub.createExportPackage.wireToLocal");

        com.teamcenter.schemas.soa._2006_03.base.ServiceData wireOut = 
       (com.teamcenter.schemas.soa._2006_03.base.ServiceData)outObj;
        com.teamcenter.soa.client.model.ServiceData localOut;
        
        localOut =  modelManager.loadServiceData( wireOut ); // SERVICEDATA_RETURN

        if(!localConnection.getOption(Connection.OPT_CACHE_MODEL_OBJECTS).equals( "true" ))
      	{
        	localConnection.getClientDataModel().removeAllObjects(); 
        }
        com.teamcenter.soa.internal.common.Monitor.markEnd  ("com.ce4.services.strong.cadexchangemanagement.CadExchangeRestBindingStub.createExportPackage.wireToLocal");
        com.teamcenter.soa.internal.common.Monitor.markEnd  ("com.ce4.services.strong.cadexchangemanagement.CadExchangeRestBindingStub.createExportPackage");
         return localOut;
       }
       finally
       {
        restSender.popRequestId();
        modelManager.unlockModel();
       }
    }
	
    @Override 
    public com.teamcenter.soa.client.model.ServiceData importExchangePackage( com.ce4.services.strong.cadexchangemanagement._2013_05.CadExchange.ImportPackageInput importPkgInput ) 
    
    {
       try
       {
        restSender.pushRequestId();
        com.teamcenter.soa.internal.common.Monitor.markStart("com.ce4.services.strong.cadexchangemanagement.CadExchangeRestBindingStub.importExchangePackage");
        com.teamcenter.soa.internal.common.Monitor.markStart("com.ce4.services.strong.cadexchangemanagement.CadExchangeRestBindingStub.importExchangePackage.localToWire");
		com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange.ImportExchangePackageInput wireIn = null;
        wireIn = CadExchange_201305Factory.createImportExchangePackageInput();
        wireIn.setImportPkgInput( toWire( importPkgInput )); //STRUCT_ELEMENT_TO_WIRE

        com.teamcenter.soa.internal.common.Monitor.markEnd  ("com.ce4.services.strong.cadexchangemanagement.CadExchangeRestBindingStub.importExchangePackage.localToWire");

        Object outObj = restSender.invoke2( CADEXCHANGE_201305_PORT_NAME, "importExchangePackage", wireIn, 
                        CADEXCHANGE_201305_CONTEXT_PATH, CADEXCHANGE_201305_CONTEXT_PATH);
		modelManager.lockModel();
		

        com.teamcenter.soa.internal.common.Monitor.markStart("com.ce4.services.strong.cadexchangemanagement.CadExchangeRestBindingStub.importExchangePackage.wireToLocal");

        com.teamcenter.schemas.soa._2006_03.base.ServiceData wireOut = 
       (com.teamcenter.schemas.soa._2006_03.base.ServiceData)outObj;
        com.teamcenter.soa.client.model.ServiceData localOut;
        
        localOut =  modelManager.loadServiceData( wireOut ); // SERVICEDATA_RETURN

        if(!localConnection.getOption(Connection.OPT_CACHE_MODEL_OBJECTS).equals( "true" ))
      	{
        	localConnection.getClientDataModel().removeAllObjects(); 
        }
        com.teamcenter.soa.internal.common.Monitor.markEnd  ("com.ce4.services.strong.cadexchangemanagement.CadExchangeRestBindingStub.importExchangePackage.wireToLocal");
        com.teamcenter.soa.internal.common.Monitor.markEnd  ("com.ce4.services.strong.cadexchangemanagement.CadExchangeRestBindingStub.importExchangePackage");
         return localOut;
       }
       finally
       {
        restSender.popRequestId();
        modelManager.unlockModel();
       }
    }
	
    @Override 
    public com.teamcenter.soa.client.model.ServiceData setExpChkOut( com.ce4.services.strong.cadexchangemanagement._2013_05.CadExchange.DatasetExportCheckOutInfo[] datasetExportChkOutInfo ) 
    
    {
       try
       {
        restSender.pushRequestId();
        com.teamcenter.soa.internal.common.Monitor.markStart("com.ce4.services.strong.cadexchangemanagement.CadExchangeRestBindingStub.setExpChkOut");
        com.teamcenter.soa.internal.common.Monitor.markStart("com.ce4.services.strong.cadexchangemanagement.CadExchangeRestBindingStub.setExpChkOut.localToWire");
		com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange.SetExpChkOutInput wireIn = null;
        wireIn = CadExchange_201305Factory.createSetExpChkOutInput();
         List wireInDatasetExportChkOutInfo = wireIn.getDatasetExportChkOutInfo();// VECTOR_TO_WIRE 
         for (int i = 0; i < datasetExportChkOutInfo.length; i ++ )
         {
                     wireInDatasetExportChkOutInfo.add(toWire(datasetExportChkOutInfo[i])); // STRUCT_IN_VECTOR_TO_WIRE 

         }

        com.teamcenter.soa.internal.common.Monitor.markEnd  ("com.ce4.services.strong.cadexchangemanagement.CadExchangeRestBindingStub.setExpChkOut.localToWire");

        Object outObj = restSender.invoke2( CADEXCHANGE_201305_PORT_NAME, "setExpChkOut", wireIn, 
                        CADEXCHANGE_201305_CONTEXT_PATH, CADEXCHANGE_201305_CONTEXT_PATH);
		modelManager.lockModel();
		

        com.teamcenter.soa.internal.common.Monitor.markStart("com.ce4.services.strong.cadexchangemanagement.CadExchangeRestBindingStub.setExpChkOut.wireToLocal");

        com.teamcenter.schemas.soa._2006_03.base.ServiceData wireOut = 
       (com.teamcenter.schemas.soa._2006_03.base.ServiceData)outObj;
        com.teamcenter.soa.client.model.ServiceData localOut;
        
        localOut =  modelManager.loadServiceData( wireOut ); // SERVICEDATA_RETURN

        if(!localConnection.getOption(Connection.OPT_CACHE_MODEL_OBJECTS).equals( "true" ))
      	{
        	localConnection.getClientDataModel().removeAllObjects(); 
        }
        com.teamcenter.soa.internal.common.Monitor.markEnd  ("com.ce4.services.strong.cadexchangemanagement.CadExchangeRestBindingStub.setExpChkOut.wireToLocal");
        com.teamcenter.soa.internal.common.Monitor.markEnd  ("com.ce4.services.strong.cadexchangemanagement.CadExchangeRestBindingStub.setExpChkOut");
         return localOut;
       }
       finally
       {
        restSender.popRequestId();
        modelManager.unlockModel();
       }
    }


}

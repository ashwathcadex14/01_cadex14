/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

   Auto-generated source from service interface.
                 DO NOT EDIT

 ==================================================
*/


package com.ce4.services.strong.cadexchangemanagement._2013_05;

/**
 *
 */
 @SuppressWarnings("unchecked")
public interface CadExchange
{

    public class CreateExportPkgInput
    {
        /**
         * Exchange Package Revision
         */
        public com.teamcenter.soa.client.model.strong.ItemRevision exPkgRevision = null;
        /**
         * Transfer Mode to be used during export
         */
        public com.teamcenter.soa.client.model.strong.TransferMode exPkgSessionTM = null;
    }


    public class DatasetExportCheckOutInfo
    {
        /**
         * Dataset for setting Exported, CheckOut in Supplier CAD Exchange
         */
        public com.teamcenter.soa.client.model.strong.Dataset dsetObject = null;
        /**
         * Need to export or not
         */
        public boolean isExport;
        /**
         * Need to check out or not
         */
        public boolean isCheckOut;
        /**
         * Line to which dataset is associated
         */
        public com.teamcenter.soa.client.model.strong.BOMLine bomLine = null;
        /**
         * Window of the current package
         */
        public com.teamcenter.soa.client.model.strong.BOMWindow bomWindow = null;
    }


    public class ImportPackageInput
    {
        /**
         * Package Revision to import the exchange package
         */
        public com.teamcenter.soa.client.model.strong.ItemRevision packageRev = null;
        /**
         * Transfer mode to import the exchange package
         */
        public com.teamcenter.soa.client.model.strong.TransferMode importTM = null;
    }



    

    /**
     * .
     *
     * @param exPkgSessionInput
     *        Input for export package session
     *
     * @return
     *
     */
    public com.teamcenter.soa.client.model.ServiceData createExportPackage ( com.ce4.services.strong.cadexchangemanagement._2013_05.CadExchange.CreateExportPkgInput exPkgSessionInput );


    /**
     * .
     *
     * @param importPkgInput
     *        Input details for importing the exchange package from supplier
     *
     * @return
     *
     */
    public com.teamcenter.soa.client.model.ServiceData importExchangePackage ( com.ce4.services.strong.cadexchangemanagement._2013_05.CadExchange.ImportPackageInput importPkgInput );


    /**
     * .
     *
     * @param datasetExportChkOutInfo
     *        vector of Dataset Export Check Out info
     *
     * @return
     *
     */
    public com.teamcenter.soa.client.model.ServiceData setExpChkOut ( com.ce4.services.strong.cadexchangemanagement._2013_05.CadExchange.DatasetExportCheckOutInfo[] datasetExportChkOutInfo );



}

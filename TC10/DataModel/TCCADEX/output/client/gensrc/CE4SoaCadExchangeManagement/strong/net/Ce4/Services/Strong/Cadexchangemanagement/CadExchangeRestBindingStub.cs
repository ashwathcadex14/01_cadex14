/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

   Auto-generated source from service interface.
                 DO NOT EDIT

 ==================================================
*/


using System;

using Ce4.Services.Strong.Cadexchangemanagement._2013_05.CadExchange;

using Teamcenter.Soa.Client; 
using Teamcenter.Soa.Internal.Client; 
using Teamcenter.Soa.Internal.Client.Model; 

#pragma warning disable 1591

namespace Ce4.Services.Strong.Cadexchangemanagement
{
    public class CadExchangeRestBindingStub : CadExchangeService
    {
        private Sender              restSender;
        private PopulateModel       modelManager;
        private Connection          localConnection;
    
        public CadExchangeRestBindingStub( Connection connection )
        {
            this.localConnection = connection;
            this.restSender  = connection.Sender;
            this.modelManager= (PopulateModel)connection.ModelManager;
            Teamcenter.Soa.Client.Model.StrongObjectFactory.Init();
        }


    static readonly string CADEXCHANGE_201305_PORT_NAME          = "CadExchangeManagement-2013-05-CadExchange";

    public  static Ce4.Schemas.Cadexchangemanagement._2013_05.Cadexchange.CreateExportPkgInput toWire( Ce4.Services.Strong.Cadexchangemanagement._2013_05.CadExchange.CreateExportPkgInput local ) 
    {
 		Ce4.Schemas.Cadexchangemanagement._2013_05.Cadexchange.CreateExportPkgInput wire = 
 		   new Ce4.Schemas.Cadexchangemanagement._2013_05.Cadexchange.CreateExportPkgInput();


        Teamcenter.Schemas.Soa._2006_03.Base.ModelObject exPkgRevisionWireIn =  //WIRE_CREATE
          new Teamcenter.Schemas.Soa._2006_03.Base.ModelObject();
        if(local.ExPkgRevision == null)  // MODEL_OBJECT_ELEMENT_TO_WIRE
             exPkgRevisionWireIn.setUid( Teamcenter.Soa.Client.Model.NullModelObject.NULL_ID );
        else
             exPkgRevisionWireIn.setUid(local.ExPkgRevision.Uid);
        wire.setExPkgRevision( exPkgRevisionWireIn );
        Teamcenter.Schemas.Soa._2006_03.Base.ModelObject exPkgSessionTMWireIn =  //WIRE_CREATE
          new Teamcenter.Schemas.Soa._2006_03.Base.ModelObject();
        if(local.ExPkgSessionTM == null)  // MODEL_OBJECT_ELEMENT_TO_WIRE
             exPkgSessionTMWireIn.setUid( Teamcenter.Soa.Client.Model.NullModelObject.NULL_ID );
        else
             exPkgSessionTMWireIn.setUid(local.ExPkgSessionTM.Uid);
        wire.setExPkgSessionTM( exPkgSessionTMWireIn );

		return wire;
	}
    public  static Ce4.Services.Strong.Cadexchangemanagement._2013_05.CadExchange.CreateExportPkgInput toLocal( Ce4.Schemas.Cadexchangemanagement._2013_05.Cadexchange.CreateExportPkgInput  wire , Teamcenter.Soa.Internal.Client.Model.PopulateModel modelManager ) 
    {
  		Ce4.Services.Strong.Cadexchangemanagement._2013_05.CadExchange.CreateExportPkgInput local = new Ce4.Services.Strong.Cadexchangemanagement._2013_05.CadExchange.CreateExportPkgInput();

        local.ExPkgRevision =  (Teamcenter.Soa.Client.Model.Strong.ItemRevision)modelManager.LoadObjectData( wire.getExPkgRevision()); // POMREF_ELEMENT_TO_LOCAL
        local.ExPkgSessionTM =  (Teamcenter.Soa.Client.Model.Strong.TransferMode)modelManager.LoadObjectData( wire.getExPkgSessionTM()); // POMREF_ELEMENT_TO_LOCAL


		return local;
	}

    public  static Ce4.Schemas.Cadexchangemanagement._2013_05.Cadexchange.DatasetExportCheckOutInfo toWire( Ce4.Services.Strong.Cadexchangemanagement._2013_05.CadExchange.DatasetExportCheckOutInfo local ) 
    {
 		Ce4.Schemas.Cadexchangemanagement._2013_05.Cadexchange.DatasetExportCheckOutInfo wire = 
 		   new Ce4.Schemas.Cadexchangemanagement._2013_05.Cadexchange.DatasetExportCheckOutInfo();


        Teamcenter.Schemas.Soa._2006_03.Base.ModelObject dsetObjectWireIn =  //WIRE_CREATE
          new Teamcenter.Schemas.Soa._2006_03.Base.ModelObject();
        if(local.DsetObject == null)  // MODEL_OBJECT_ELEMENT_TO_WIRE
             dsetObjectWireIn.setUid( Teamcenter.Soa.Client.Model.NullModelObject.NULL_ID );
        else
             dsetObjectWireIn.setUid(local.DsetObject.Uid);
        wire.setDsetObject( dsetObjectWireIn );
        wire.setIsExport( local.IsExport ); //BASIC_ELEMENT_TO_WIRE
        wire.setIsCheckOut( local.IsCheckOut ); //BASIC_ELEMENT_TO_WIRE
        Teamcenter.Schemas.Soa._2006_03.Base.ModelObject bomLineWireIn =  //WIRE_CREATE
          new Teamcenter.Schemas.Soa._2006_03.Base.ModelObject();
        if(local.BomLine == null)  // MODEL_OBJECT_ELEMENT_TO_WIRE
             bomLineWireIn.setUid( Teamcenter.Soa.Client.Model.NullModelObject.NULL_ID );
        else
             bomLineWireIn.setUid(local.BomLine.Uid);
        wire.setBomLine( bomLineWireIn );
        Teamcenter.Schemas.Soa._2006_03.Base.ModelObject bomWindowWireIn =  //WIRE_CREATE
          new Teamcenter.Schemas.Soa._2006_03.Base.ModelObject();
        if(local.BomWindow == null)  // MODEL_OBJECT_ELEMENT_TO_WIRE
             bomWindowWireIn.setUid( Teamcenter.Soa.Client.Model.NullModelObject.NULL_ID );
        else
             bomWindowWireIn.setUid(local.BomWindow.Uid);
        wire.setBomWindow( bomWindowWireIn );

		return wire;
	}
    public  static Ce4.Services.Strong.Cadexchangemanagement._2013_05.CadExchange.DatasetExportCheckOutInfo toLocal( Ce4.Schemas.Cadexchangemanagement._2013_05.Cadexchange.DatasetExportCheckOutInfo  wire , Teamcenter.Soa.Internal.Client.Model.PopulateModel modelManager ) 
    {
  		Ce4.Services.Strong.Cadexchangemanagement._2013_05.CadExchange.DatasetExportCheckOutInfo local = new Ce4.Services.Strong.Cadexchangemanagement._2013_05.CadExchange.DatasetExportCheckOutInfo();

        local.DsetObject =  (Teamcenter.Soa.Client.Model.Strong.Dataset)modelManager.LoadObjectData( wire.getDsetObject()); // POMREF_ELEMENT_TO_LOCAL
        local.IsExport = wire.IsExport; //BOOLEAN_ELEMENT_TO_LOCAL
        local.IsCheckOut = wire.IsCheckOut; //BOOLEAN_ELEMENT_TO_LOCAL
        local.BomLine =  (Teamcenter.Soa.Client.Model.Strong.BOMLine)modelManager.LoadObjectData( wire.getBomLine()); // POMREF_ELEMENT_TO_LOCAL
        local.BomWindow =  (Teamcenter.Soa.Client.Model.Strong.BOMWindow)modelManager.LoadObjectData( wire.getBomWindow()); // POMREF_ELEMENT_TO_LOCAL


		return local;
	}

    public  static Ce4.Schemas.Cadexchangemanagement._2013_05.Cadexchange.ImportPackageInput toWire( Ce4.Services.Strong.Cadexchangemanagement._2013_05.CadExchange.ImportPackageInput local ) 
    {
 		Ce4.Schemas.Cadexchangemanagement._2013_05.Cadexchange.ImportPackageInput wire = 
 		   new Ce4.Schemas.Cadexchangemanagement._2013_05.Cadexchange.ImportPackageInput();


        Teamcenter.Schemas.Soa._2006_03.Base.ModelObject packageRevWireIn =  //WIRE_CREATE
          new Teamcenter.Schemas.Soa._2006_03.Base.ModelObject();
        if(local.PackageRev == null)  // MODEL_OBJECT_ELEMENT_TO_WIRE
             packageRevWireIn.setUid( Teamcenter.Soa.Client.Model.NullModelObject.NULL_ID );
        else
             packageRevWireIn.setUid(local.PackageRev.Uid);
        wire.setPackageRev( packageRevWireIn );
        Teamcenter.Schemas.Soa._2006_03.Base.ModelObject importTMWireIn =  //WIRE_CREATE
          new Teamcenter.Schemas.Soa._2006_03.Base.ModelObject();
        if(local.ImportTM == null)  // MODEL_OBJECT_ELEMENT_TO_WIRE
             importTMWireIn.setUid( Teamcenter.Soa.Client.Model.NullModelObject.NULL_ID );
        else
             importTMWireIn.setUid(local.ImportTM.Uid);
        wire.setImportTM( importTMWireIn );

		return wire;
	}
    public  static Ce4.Services.Strong.Cadexchangemanagement._2013_05.CadExchange.ImportPackageInput toLocal( Ce4.Schemas.Cadexchangemanagement._2013_05.Cadexchange.ImportPackageInput  wire , Teamcenter.Soa.Internal.Client.Model.PopulateModel modelManager ) 
    {
  		Ce4.Services.Strong.Cadexchangemanagement._2013_05.CadExchange.ImportPackageInput local = new Ce4.Services.Strong.Cadexchangemanagement._2013_05.CadExchange.ImportPackageInput();

        local.PackageRev =  (Teamcenter.Soa.Client.Model.Strong.ItemRevision)modelManager.LoadObjectData( wire.getPackageRev()); // POMREF_ELEMENT_TO_LOCAL
        local.ImportTM =  (Teamcenter.Soa.Client.Model.Strong.TransferMode)modelManager.LoadObjectData( wire.getImportTM()); // POMREF_ELEMENT_TO_LOCAL


		return local;
	}
    
    public override Teamcenter.Soa.Client.Model.ServiceData CreateExportPackage( Ce4.Services.Strong.Cadexchangemanagement._2013_05.CadExchange.CreateExportPkgInput ExPkgSessionInput ) 
    {
       try
       {
        restSender.PushRequestId();
        Ce4.Schemas.Cadexchangemanagement._2013_05.Cadexchange.CreateExportPackageInput wireIn = 
          new Ce4.Schemas.Cadexchangemanagement._2013_05.Cadexchange.CreateExportPackageInput();
 
        wireIn.setExPkgSessionInput( toWire( ExPkgSessionInput )); //STRUCT_ELEMENT_TO_WIRE


        System.Type wireOut_Type = typeof(Teamcenter.Schemas.Soa._2006_03.Base.ServiceData);
        System.Type[] exceptionTypeList = null;

 
        object outObj = restSender.Invoke( CADEXCHANGE_201305_PORT_NAME, "CreateExportPackage", wireIn, 
                        wireOut_Type, exceptionTypeList);
        modelManager.LockModel();



        Teamcenter.Schemas.Soa._2006_03.Base.ServiceData wireOut = 
          (Teamcenter.Schemas.Soa._2006_03.Base.ServiceData)outObj;
        Teamcenter.Soa.Client.Model.ServiceData localOut;

        localOut = modelManager.LoadServiceData( wireOut ); // SERVICEDATA_RETURN

        if (!localConnection.GetOption(Connection.OPT_CACHE_MODEL_OBJECTS).Equals("true"))
        {
            localConnection.ModelManager.RemoveAllObjectsFromStore();
       }

        return localOut;
       }
       finally
       {
        restSender.PopRequestId();
        modelManager.UnlockModel();
       }
    }
    
    public override Teamcenter.Soa.Client.Model.ServiceData ImportExchangePackage( Ce4.Services.Strong.Cadexchangemanagement._2013_05.CadExchange.ImportPackageInput ImportPkgInput ) 
    {
       try
       {
        restSender.PushRequestId();
        Ce4.Schemas.Cadexchangemanagement._2013_05.Cadexchange.ImportExchangePackageInput wireIn = 
          new Ce4.Schemas.Cadexchangemanagement._2013_05.Cadexchange.ImportExchangePackageInput();
 
        wireIn.setImportPkgInput( toWire( ImportPkgInput )); //STRUCT_ELEMENT_TO_WIRE


        System.Type wireOut_Type = typeof(Teamcenter.Schemas.Soa._2006_03.Base.ServiceData);
        System.Type[] exceptionTypeList = null;

 
        object outObj = restSender.Invoke( CADEXCHANGE_201305_PORT_NAME, "ImportExchangePackage", wireIn, 
                        wireOut_Type, exceptionTypeList);
        modelManager.LockModel();



        Teamcenter.Schemas.Soa._2006_03.Base.ServiceData wireOut = 
          (Teamcenter.Schemas.Soa._2006_03.Base.ServiceData)outObj;
        Teamcenter.Soa.Client.Model.ServiceData localOut;

        localOut = modelManager.LoadServiceData( wireOut ); // SERVICEDATA_RETURN

        if (!localConnection.GetOption(Connection.OPT_CACHE_MODEL_OBJECTS).Equals("true"))
        {
            localConnection.ModelManager.RemoveAllObjectsFromStore();
       }

        return localOut;
       }
       finally
       {
        restSender.PopRequestId();
        modelManager.UnlockModel();
       }
    }
    
    public override Teamcenter.Soa.Client.Model.ServiceData SetExpChkOut( Ce4.Services.Strong.Cadexchangemanagement._2013_05.CadExchange.DatasetExportCheckOutInfo[] DatasetExportChkOutInfo ) 
    {
       try
       {
        restSender.PushRequestId();
        Ce4.Schemas.Cadexchangemanagement._2013_05.Cadexchange.SetExpChkOutInput wireIn = 
          new Ce4.Schemas.Cadexchangemanagement._2013_05.Cadexchange.SetExpChkOutInput();
 
         System.Collections.ArrayList wireInDatasetExportChkOutInfo = new System.Collections.ArrayList() ;// VECTOR_TO_WIRE 
         for (int i = 0; i < DatasetExportChkOutInfo.Length; i ++ )
         {
                     wireInDatasetExportChkOutInfo.Add(toWire(DatasetExportChkOutInfo[i])); // STRUCT_IN_VECTOR_TO_WIRE 

         }
         wireIn.setDatasetExportChkOutInfo(wireInDatasetExportChkOutInfo);// PROP_SET


        System.Type wireOut_Type = typeof(Teamcenter.Schemas.Soa._2006_03.Base.ServiceData);
        System.Type[] exceptionTypeList = null;

 
        object outObj = restSender.Invoke( CADEXCHANGE_201305_PORT_NAME, "SetExpChkOut", wireIn, 
                        wireOut_Type, exceptionTypeList);
        modelManager.LockModel();



        Teamcenter.Schemas.Soa._2006_03.Base.ServiceData wireOut = 
          (Teamcenter.Schemas.Soa._2006_03.Base.ServiceData)outObj;
        Teamcenter.Soa.Client.Model.ServiceData localOut;

        localOut = modelManager.LoadServiceData( wireOut ); // SERVICEDATA_RETURN

        if (!localConnection.GetOption(Connection.OPT_CACHE_MODEL_OBJECTS).Equals("true"))
        {
            localConnection.ModelManager.RemoveAllObjectsFromStore();
       }

        return localOut;
       }
       finally
       {
        restSender.PopRequestId();
        modelManager.UnlockModel();
       }
    }


    }
}

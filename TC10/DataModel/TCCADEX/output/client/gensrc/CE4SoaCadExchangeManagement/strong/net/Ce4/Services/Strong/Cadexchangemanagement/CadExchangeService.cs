/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

   Auto-generated source from service interface.
                 DO NOT EDIT

 ==================================================
*/

using System;
using Teamcenter.Soa; 
using Teamcenter.Soa.Client; 

using Ce4.Services.Strong.Cadexchangemanagement._2013_05.CadExchange;


namespace Ce4.Services.Strong.Cadexchangemanagement
{

    /// <summary>
    /// Service for Cad Exchange operations of TCCADEX
    /// <br />
    /// <br />
    /// <br />
    /// <b>Library Reference:</b>
    /// <ul>
    /// <li type="disc">CE4SoaCadExchangeManagementStrong.dll
    /// </li>
    /// <li type="disc">CE4SoaCadExchangeManagementTypes.dll (runtime dependency)
    /// </li>
    /// </ul>
    /// </summary>

    public abstract class CadExchangeService :     Ce4.Services.Strong.Cadexchangemanagement._2013_05.CadExchange.CadExchange
    {
        ///<summary>Get service from the connection object</summary>
        ///<param name="connection">Connection class object</param>
        ///<returns>Returns the service object to call the correct service operation</returns>
        public static CadExchangeService getService( Connection connection )
        {
            if(connection.Binding.ToUpper().Equals( SoaConstants.REST.ToUpper() ))
            {
                return new CadExchangeRestBindingStub( connection );
            }
            else
                throw new ArgumentOutOfRangeException("connection", "The " + connection.Binding + " binding is not supported.");
        }
        
        
        
        /// <summary>.</summary>
        ///
        /// <param name="ExPkgSessionInput">
        ///             Input for export package session
        /// </param>
        ///
        /// <returns>
        /// </returns>
        ///
        public virtual Teamcenter.Soa.Client.Model.ServiceData CreateExportPackage( Ce4.Services.Strong.Cadexchangemanagement._2013_05.CadExchange.CreateExportPkgInput ExPkgSessionInput ) 
        {  throw new NotImplementedException();  }


        /// <summary>.</summary>
        ///
        /// <param name="ImportPkgInput">
        ///             Input details for importing the exchange package from supplier
        /// </param>
        ///
        /// <returns>
        /// </returns>
        ///
        public virtual Teamcenter.Soa.Client.Model.ServiceData ImportExchangePackage( Ce4.Services.Strong.Cadexchangemanagement._2013_05.CadExchange.ImportPackageInput ImportPkgInput ) 
        {  throw new NotImplementedException();  }


        /// <summary>.</summary>
        ///
        /// <param name="DatasetExportChkOutInfo">
        ///             vector of Dataset Export Check Out info
        /// </param>
        ///
        /// <returns>
        /// </returns>
        ///
        public virtual Teamcenter.Soa.Client.Model.ServiceData SetExpChkOut( Ce4.Services.Strong.Cadexchangemanagement._2013_05.CadExchange.DatasetExportCheckOutInfo[] DatasetExportChkOutInfo ) 
        {  throw new NotImplementedException();  }


        
    }
}

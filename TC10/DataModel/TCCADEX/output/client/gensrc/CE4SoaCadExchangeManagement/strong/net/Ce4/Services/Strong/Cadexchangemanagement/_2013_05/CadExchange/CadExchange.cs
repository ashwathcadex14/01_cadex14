/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

   Auto-generated source from service interface.
                 DO NOT EDIT

 ==================================================
*/

using System;

namespace Ce4.Services.Strong.Cadexchangemanagement._2013_05.CadExchange
{
    public class CreateExportPkgInput
    {

        /// <summary>Exchange Package Revision</summary>
        public Teamcenter.Soa.Client.Model.Strong.ItemRevision ExPkgRevision = null;

        /// <summary>Transfer Mode to be used during export</summary>
        public Teamcenter.Soa.Client.Model.Strong.TransferMode ExPkgSessionTM = null;
    }


    public class DatasetExportCheckOutInfo
    {

        /// <summary>Dataset for setting Exported, CheckOut in Supplier CAD Exchange</summary>
        public Teamcenter.Soa.Client.Model.Strong.Dataset DsetObject = null;

        /// <summary>Need to export or not</summary>
        public bool IsExport;

        /// <summary>Need to check out or not</summary>
        public bool IsCheckOut;

        /// <summary>Line to which dataset is associated</summary>
        public Teamcenter.Soa.Client.Model.Strong.BOMLine BomLine = null;

        /// <summary>Window of the current package</summary>
        public Teamcenter.Soa.Client.Model.Strong.BOMWindow BomWindow = null;
    }


    public class ImportPackageInput
    {

        /// <summary>Package Revision to import the exchange package</summary>
        public Teamcenter.Soa.Client.Model.Strong.ItemRevision PackageRev = null;

        /// <summary>Transfer mode to import the exchange package</summary>
        public Teamcenter.Soa.Client.Model.Strong.TransferMode ImportTM = null;
    }




    ///<summary>Interface CadExchange</summary>
    public interface CadExchange
    {

    
     /// <summary>.</summary>
     ///
     /// <param name="ExPkgSessionInput">
     ///             Input for export package session
     /// </param>
     ///
     /// <returns>
     /// </returns>
     ///
     Teamcenter.Soa.Client.Model.ServiceData CreateExportPackage ( Ce4.Services.Strong.Cadexchangemanagement._2013_05.CadExchange.CreateExportPkgInput ExPkgSessionInput );


     /// <summary>.</summary>
     ///
     /// <param name="ImportPkgInput">
     ///             Input details for importing the exchange package from supplier
     /// </param>
     ///
     /// <returns>
     /// </returns>
     ///
     Teamcenter.Soa.Client.Model.ServiceData ImportExchangePackage ( Ce4.Services.Strong.Cadexchangemanagement._2013_05.CadExchange.ImportPackageInput ImportPkgInput );


     /// <summary>.</summary>
     ///
     /// <param name="DatasetExportChkOutInfo">
     ///             vector of Dataset Export Check Out info
     /// </param>
     ///
     /// <returns>
     /// </returns>
     ///
     Teamcenter.Soa.Client.Model.ServiceData SetExpChkOut ( Ce4.Services.Strong.Cadexchangemanagement._2013_05.CadExchange.DatasetExportCheckOutInfo[] DatasetExportChkOutInfo );



    }
}
/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
@<COPYRIGHT>@

 ==================================================

   Auto-generated source from service interface.
                 DO NOT EDIT

 ==================================================
*/

package com.ce4.services.strong.cadexchangemanagement;


import com.teamcenter.soa.client.Connection;
import com.teamcenter.soa.client.model.ModelManager;
import com.teamcenter.utest.SoaSession;

import junit.framework.TestCase;

public class CadExchangeTest extends TestCase
{
    private Connection        connection;
    private ModelManager      manager;
    private CadExchangeService   service;
    

    public CadExchangeTest( String name )
    {
        super( name );
    }

    protected void setUp( ) throws Exception
    {
        super.setUp( );

        connection  = SoaSession.getConnection();
        manager     = connection.getModelManager();       
        service     = CadExchangeService.getService(connection);

    }
        
    
    public void testCreateExportPackage()
    {
        // TODO write test code, then remove fail()
        // service.createExportPackage(  )
        fail("This test has not been implemented yet");
    }

    public void testImportExchangePackage()
    {
        // TODO write test code, then remove fail()
        // service.importExchangePackage(  )
        fail("This test has not been implemented yet");
    }

    public void testSetExpChkOut()
    {
        // TODO write test code, then remove fail()
        // service.setExpChkOut(  )
        fail("This test has not been implemented yet");
    }


}


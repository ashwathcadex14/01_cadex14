/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

  Auto-generated source from Teamcenter Data Model.
                 DO NOT EDIT

 ==================================================
*/

#ifndef TEAMCENTER_SOA_CLIENT_MODEL_CE4_EXPKGBASEMASTERS_HXX
#define TEAMCENTER_SOA_CLIENT_MODEL_CE4_EXPKGBASEMASTERS_HXX

#include <new>
#include <teamcenter/soa/common/MemoryManager.hxx>
#include <teamcenter/soa/common/DateTime.hxx>
#include <teamcenter/soa/client/ModelObject.hxx>

#include <teamcenter/soa/client/model/ItemMaster.hxx>

#include <teamcenter/soa/client/model/Tccadex_exports.h>

namespace Teamcenter
{
    namespace Soa
    {
        namespace Client
        {
            namespace Model
            {


class TCSOATCCADEXMODEL_API CE4_ExPkgBaseMasterS : public Teamcenter::Soa::Client::Model::ItemMaster
{
public:


   SOA_CLASS_NEW_OPERATORS_WITH_IMPL("CE4_ExPkgBaseMasterS")

   virtual ~CE4_ExPkgBaseMasterS();
};
            }
        }
    }
}
#include <teamcenter/soa/client/model/Tccadex_undef.h>
#endif

/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

  Auto-generated source from Teamcenter Data Model.
                 DO NOT EDIT

 ==================================================
*/

#ifndef TEAMCENTER_SOA_CLIENT_MODEL_CE4_EXPKGBASEREVISION_HXX
#define TEAMCENTER_SOA_CLIENT_MODEL_CE4_EXPKGBASEREVISION_HXX

#include <new>
#include <teamcenter/soa/common/MemoryManager.hxx>
#include <teamcenter/soa/common/DateTime.hxx>
#include <teamcenter/soa/client/ModelObject.hxx>

#include <teamcenter/soa/client/model/ItemRevision.hxx>

#include <teamcenter/soa/client/model/Tccadex_exports.h>

namespace Teamcenter
{
    namespace Soa
    {
        namespace Client
        {
            namespace Model
            {
                class CE4_PackageStatus;


class TCSOATCCADEXMODEL_API CE4_ExPkgBaseRevision : public Teamcenter::Soa::Client::Model::ItemRevision
{
public:
    const Teamcenter::Soa::Client::StringVector& get_ce4_checkedOutNodes();
    const Teamcenter::Soa::Client::StringVector& get_ce4_renderingNodes();
    const std::string& get_ce4_ep_serial_no();
    const Teamcenter::Soa::Client::StringVector& get_ce4_nativeNodes();
    const Teamcenter::Soa::Client::StringVector& get_ce4_doNotExportNodes();
    const std::string& get_ce4_status();
    const Teamcenter::Soa::Client::StringVector& get_ce4_siteCheckedOutNodes();
    const Teamcenter::Soa::Client::StringVector& get_ce4_epConfiguration();
    bool get_ce4_isUpdated();
    const std::string& get_ce4_revisionRule();
    size_t count_ce4_pkgHistory();
    CE4_PackageStatus* get_ce4_pkgHistory_at( size_t inx );
    const std::string& get_ce4_statusRemarks();


   SOA_CLASS_NEW_OPERATORS_WITH_IMPL("CE4_ExPkgBaseRevision")

   virtual ~CE4_ExPkgBaseRevision();
};
            }
        }
    }
}
#include <teamcenter/soa/client/model/Tccadex_undef.h>
#endif

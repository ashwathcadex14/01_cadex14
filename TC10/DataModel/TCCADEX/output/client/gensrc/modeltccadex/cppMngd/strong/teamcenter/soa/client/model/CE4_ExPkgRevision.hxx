/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

  Auto-generated source from Teamcenter Data Model.
                 DO NOT EDIT

 ==================================================
*/

#ifndef TEAMCENTER_SOA_CLIENT_MODEL_CE4_EXPKGREVISION_HXX
#define TEAMCENTER_SOA_CLIENT_MODEL_CE4_EXPKGREVISION_HXX

#include <new>
#include <teamcenter/soa/common/MemoryManager.hxx>
#include <teamcenter/soa/common/DateTime.hxx>
#include <teamcenter/soa/client/ModelObject.hxx>

#include <teamcenter/soa/client/model/CE4_ExPkgBaseRevision.hxx>

#include <teamcenter/soa/client/model/Tccadex_exports.h>

namespace Teamcenter
{
    namespace Soa
    {
        namespace Client
        {
            namespace Model
            {


class TCSOATCCADEXMODEL_API CE4_ExPkgRevision : public Teamcenter::Soa::Client::Model::CE4_ExPkgBaseRevision
{
public:
    const Teamcenter::Soa::Client::ModelObjectVector& get_CE4_ExPkgIncomingPackage();
    const Teamcenter::Soa::Client::ModelObjectVector& get_CE4_ExPkgOutgoingPackage();
    const Teamcenter::Soa::Client::ModelObjectVector& get_CE4_ExPkgToRecipients();
    const Teamcenter::Soa::Client::ModelObjectVector& get_CE4_ExPkgToReferences();
    const Teamcenter::Soa::Client::ModelObjectVector& get_CE4_ExPkgToTargets();
    const Teamcenter::Soa::Client::ModelObjectVector& get_CE4_ExPkgXML();


   SOA_CLASS_NEW_OPERATORS_WITH_IMPL("CE4_ExPkgRevision")

   virtual ~CE4_ExPkgRevision();
};
            }
        }
    }
}
#include <teamcenter/soa/client/model/Tccadex_undef.h>
#endif

/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

  Auto-generated source from Teamcenter Data Model.
                 DO NOT EDIT

 ==================================================
*/

#ifndef TEAMCENTER_SOA_CLIENT_MODEL_CE4_PACKAGESTATUS_HXX
#define TEAMCENTER_SOA_CLIENT_MODEL_CE4_PACKAGESTATUS_HXX

#include <new>
#include <teamcenter/soa/common/MemoryManager.hxx>
#include <teamcenter/soa/common/DateTime.hxx>
#include <teamcenter/soa/client/ModelObject.hxx>

#include <teamcenter/soa/client/model/WorkspaceObject.hxx>

#include <teamcenter/soa/client/model/Tccadex_exports.h>

namespace Teamcenter
{
    namespace Soa
    {
        namespace Client
        {
            namespace Model
            {


class TCSOATCCADEXMODEL_API CE4_PackageStatus : public Teamcenter::Soa::Client::Model::WorkspaceObject
{
public:
    const std::string& get_ce4_exPkgStatus();


   SOA_CLASS_NEW_OPERATORS_WITH_IMPL("CE4_PackageStatus")

   virtual ~CE4_PackageStatus();
};
            }
        }
    }
}
#include <teamcenter/soa/client/model/Tccadex_undef.h>
#endif

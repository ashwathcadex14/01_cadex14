/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

  Auto-generated source from Teamcenter Data Model.
                 DO NOT EDIT

 ==================================================
*/

#include <teamcenter/soa/client/internal/ModelManagerImpl.hxx>
#include <teamcenter/soa/client/ModelObjectFactory.hxx>
#include <teamcenter/soa/client/Property.hxx>
#include <teamcenter/soa/client/RuntimeException.hxx>
#include <teamcenter/soa/client/model/TccadexObjectFactory.hxx>


// Type/Classes implemented in this CXX file
#include <teamcenter/soa/client/model/CE4_ExPkgBaseRevisionMaster.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgTargetsRevision.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgRefsMasterS.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgRevMasterS.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkg.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgBase.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgIncomingPackage.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgTemplateToPkg.hxx>
#include <teamcenter/soa/client/model/CE4_checkOutableObject.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgBaseRevision.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgTempMaster.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgRefsMaster.hxx>
#include <teamcenter/soa/client/model/CE4_Supplier.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgToReferences.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgTargetsMaster.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgMaster.hxx>
#include <teamcenter/soa/client/model/CE4_PackageStatus.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgRefsRevision.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgTemp.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgTempRevMasterS.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgRefsRevisionMaster.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgTargetsRevMasterS.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgRevision.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgTempRevision.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgTargetsRevisionMaster.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgTempRevisionMaster.hxx>
#include <teamcenter/soa/client/model/CE4_exportableObject.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgBaseMasterS.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgXML.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgTargets.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgRefs.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgMasterS.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgOutgoingPackage.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgToRecipients.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgTempMasterS.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgTargetsMasterS.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgToTargets.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgBaseMaster.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgRefsRevMasterS.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgBaseRevMasterS.hxx>
#include <teamcenter/soa/client/model/CE4_ExPkgRevisionMaster.hxx>

// Referenced Type/Classes



using namespace std;
using namespace Teamcenter::Soa::Client;
using namespace Teamcenter::Soa::Client::Model;





Teamcenter::Soa::Client::Model::CE4_ExPkgBaseMaster::~CE4_ExPkgBaseMaster()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgMaster::~CE4_ExPkgMaster()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgTempMaster::~CE4_ExPkgTempMaster()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgRefsMaster::~CE4_ExPkgRefsMaster()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgTargetsMaster::~CE4_ExPkgTargetsMaster()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgBaseRevisionMaster::~CE4_ExPkgBaseRevisionMaster()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgRevisionMaster::~CE4_ExPkgRevisionMaster()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgTempRevisionMaster::~CE4_ExPkgTempRevisionMaster()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgRefsRevisionMaster::~CE4_ExPkgRefsRevisionMaster()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgTargetsRevisionMaster::~CE4_ExPkgTargetsRevisionMaster()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgBase::~CE4_ExPkgBase()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkg::~CE4_ExPkg()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgTemp::~CE4_ExPkgTemp()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgRefs::~CE4_ExPkgRefs()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgTargets::~CE4_ExPkgTargets()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgBaseRevision::~CE4_ExPkgBaseRevision()
{
}

const Teamcenter::Soa::Client::StringVector& Teamcenter::Soa::Client::Model::CE4_ExPkgBaseRevision::get_ce4_checkedOutNodes()
{
    return getProperty("ce4_checkedOutNodes")->getStringArrayValue();
}

const Teamcenter::Soa::Client::StringVector& Teamcenter::Soa::Client::Model::CE4_ExPkgBaseRevision::get_ce4_renderingNodes()
{
    return getProperty("ce4_renderingNodes")->getStringArrayValue();
}

const std::string& Teamcenter::Soa::Client::Model::CE4_ExPkgBaseRevision::get_ce4_ep_serial_no()
{
    return getProperty("ce4_ep_serial_no")->getStringValue();
}

const Teamcenter::Soa::Client::StringVector& Teamcenter::Soa::Client::Model::CE4_ExPkgBaseRevision::get_ce4_nativeNodes()
{
    return getProperty("ce4_nativeNodes")->getStringArrayValue();
}

const Teamcenter::Soa::Client::StringVector& Teamcenter::Soa::Client::Model::CE4_ExPkgBaseRevision::get_ce4_doNotExportNodes()
{
    return getProperty("ce4_doNotExportNodes")->getStringArrayValue();
}

const std::string& Teamcenter::Soa::Client::Model::CE4_ExPkgBaseRevision::get_ce4_status()
{
    return getProperty("ce4_status")->getStringValue();
}

const Teamcenter::Soa::Client::StringVector& Teamcenter::Soa::Client::Model::CE4_ExPkgBaseRevision::get_ce4_siteCheckedOutNodes()
{
    return getProperty("ce4_siteCheckedOutNodes")->getStringArrayValue();
}

const Teamcenter::Soa::Client::StringVector& Teamcenter::Soa::Client::Model::CE4_ExPkgBaseRevision::get_ce4_epConfiguration()
{
    return getProperty("ce4_epConfiguration")->getStringArrayValue();
}

bool Teamcenter::Soa::Client::Model::CE4_ExPkgBaseRevision::get_ce4_isUpdated()
{
    return getProperty("ce4_isUpdated")->getBoolValue();
}

const std::string& Teamcenter::Soa::Client::Model::CE4_ExPkgBaseRevision::get_ce4_revisionRule()
{
    return getProperty("ce4_revisionRule")->getStringValue();
}

size_t Teamcenter::Soa::Client::Model::CE4_ExPkgBaseRevision::count_ce4_pkgHistory()
{
    return getProperty("ce4_pkgHistory")->getModelObjectArrayValue().size();
}
Teamcenter::Soa::Common::AutoPtr<CE4_PackageStatus>  Teamcenter::Soa::Client::Model::CE4_ExPkgBaseRevision::get_ce4_pkgHistory_at( size_t inx )
{
    Teamcenter::Soa::Common::AutoPtr<Teamcenter::Soa::Client::ModelObject> temp = getProperty("ce4_pkgHistory")->getModelObjectArrayValue()[inx];
    Teamcenter::Soa::Common::AutoPtr<CE4_PackageStatus> ret = temp.dyn_cast< CE4_PackageStatus >();
    if ( ret.isNull()  && !temp.isNull())
    {
        throw Teamcenter::Soa::Client::RuntimeException( Teamcenter::Soa::Client::RuntimeException::TypeMismatch, "Wrong type of object stored");
    }
    return ret;
}

const std::string& Teamcenter::Soa::Client::Model::CE4_ExPkgBaseRevision::get_ce4_statusRemarks()
{
    return getProperty("ce4_statusRemarks")->getStringValue();
}

Teamcenter::Soa::Client::Model::CE4_ExPkgRevision::~CE4_ExPkgRevision()
{
}

const Teamcenter::Soa::Client::ModelObjectVector& Teamcenter::Soa::Client::Model::CE4_ExPkgRevision::get_CE4_ExPkgIncomingPackage()
{
    return getProperty("CE4_ExPkgIncomingPackage")->getModelObjectArrayValue();
}

const Teamcenter::Soa::Client::ModelObjectVector& Teamcenter::Soa::Client::Model::CE4_ExPkgRevision::get_CE4_ExPkgOutgoingPackage()
{
    return getProperty("CE4_ExPkgOutgoingPackage")->getModelObjectArrayValue();
}

const Teamcenter::Soa::Client::ModelObjectVector& Teamcenter::Soa::Client::Model::CE4_ExPkgRevision::get_CE4_ExPkgToRecipients()
{
    return getProperty("CE4_ExPkgToRecipients")->getModelObjectArrayValue();
}

const Teamcenter::Soa::Client::ModelObjectVector& Teamcenter::Soa::Client::Model::CE4_ExPkgRevision::get_CE4_ExPkgToReferences()
{
    return getProperty("CE4_ExPkgToReferences")->getModelObjectArrayValue();
}

const Teamcenter::Soa::Client::ModelObjectVector& Teamcenter::Soa::Client::Model::CE4_ExPkgRevision::get_CE4_ExPkgToTargets()
{
    return getProperty("CE4_ExPkgToTargets")->getModelObjectArrayValue();
}

const Teamcenter::Soa::Client::ModelObjectVector& Teamcenter::Soa::Client::Model::CE4_ExPkgRevision::get_CE4_ExPkgXML()
{
    return getProperty("CE4_ExPkgXML")->getModelObjectArrayValue();
}

Teamcenter::Soa::Client::Model::CE4_ExPkgTempRevision::~CE4_ExPkgTempRevision()
{
}

const Teamcenter::Soa::Client::ModelObjectVector& Teamcenter::Soa::Client::Model::CE4_ExPkgTempRevision::get_CE4_ExPkgToRecipients()
{
    return getProperty("CE4_ExPkgToRecipients")->getModelObjectArrayValue();
}

const Teamcenter::Soa::Client::ModelObjectVector& Teamcenter::Soa::Client::Model::CE4_ExPkgTempRevision::get_CE4_ExPkgTemplateToPkg()
{
    return getProperty("CE4_ExPkgTemplateToPkg")->getModelObjectArrayValue();
}

Teamcenter::Soa::Client::Model::CE4_ExPkgRefsRevision::~CE4_ExPkgRefsRevision()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgTargetsRevision::~CE4_ExPkgTargetsRevision()
{
}

Teamcenter::Soa::Client::Model::CE4_PackageStatus::~CE4_PackageStatus()
{
}

const std::string& Teamcenter::Soa::Client::Model::CE4_PackageStatus::get_ce4_exPkgStatus()
{
    return getProperty("ce4_exPkgStatus")->getStringValue();
}

Teamcenter::Soa::Client::Model::CE4_Supplier::~CE4_Supplier()
{
}

const std::string& Teamcenter::Soa::Client::Model::CE4_Supplier::get_ce4_supplierId()
{
    return getProperty("ce4_supplierId")->getStringValue();
}

const std::string& Teamcenter::Soa::Client::Model::CE4_Supplier::get_ce4_supplierLocation()
{
    return getProperty("ce4_supplierLocation")->getStringValue();
}

Teamcenter::Soa::Client::Model::CE4_ExPkgBaseMasterS::~CE4_ExPkgBaseMasterS()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgMasterS::~CE4_ExPkgMasterS()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgTempMasterS::~CE4_ExPkgTempMasterS()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgRefsMasterS::~CE4_ExPkgRefsMasterS()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgTargetsMasterS::~CE4_ExPkgTargetsMasterS()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgBaseRevMasterS::~CE4_ExPkgBaseRevMasterS()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgRevMasterS::~CE4_ExPkgRevMasterS()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgTempRevMasterS::~CE4_ExPkgTempRevMasterS()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgRefsRevMasterS::~CE4_ExPkgRefsRevMasterS()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgTargetsRevMasterS::~CE4_ExPkgTargetsRevMasterS()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgIncomingPackage::~CE4_ExPkgIncomingPackage()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgOutgoingPackage::~CE4_ExPkgOutgoingPackage()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgTemplateToPkg::~CE4_ExPkgTemplateToPkg()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgToRecipients::~CE4_ExPkgToRecipients()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgToReferences::~CE4_ExPkgToReferences()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgToTargets::~CE4_ExPkgToTargets()
{
}

Teamcenter::Soa::Client::Model::CE4_ExPkgXML::~CE4_ExPkgXML()
{
}

Teamcenter::Soa::Client::Model::CE4_checkOutableObject::~CE4_checkOutableObject()
{
}

Teamcenter::Soa::Client::Model::CE4_exportableObject::~CE4_exportableObject()
{
}

const std::string& Teamcenter::Soa::Client::Model::CE4_exportableObject::get_ce4_actualRelation()
{
    return getProperty("ce4_actualRelation")->getStringValue();
}



void TccadexObjectFactory::init0( map< std::string, ModelObjectFactory* >& factoryMap )
{
    factoryMap[ "CE4_ExPkgBaseRevisionMaster" ] = new ModelObjectTypeFactory< CE4_ExPkgBaseRevisionMaster >();
    factoryMap[ "CE4_ExPkgTargetsRevision" ] = new ModelObjectTypeFactory< CE4_ExPkgTargetsRevision >();
    factoryMap[ "CE4_ExPkgRefsMasterS" ] = new ModelObjectTypeFactory< CE4_ExPkgRefsMasterS >();
    factoryMap[ "CE4_ExPkgRevMasterS" ] = new ModelObjectTypeFactory< CE4_ExPkgRevMasterS >();
    factoryMap[ "CE4_ExPkg" ] = new ModelObjectTypeFactory< CE4_ExPkg >();
    factoryMap[ "CE4_ExPkgBase" ] = new ModelObjectTypeFactory< CE4_ExPkgBase >();
    factoryMap[ "CE4_ExPkgIncomingPackage" ] = new ModelObjectTypeFactory< CE4_ExPkgIncomingPackage >();
    factoryMap[ "CE4_ExPkgTemplateToPkg" ] = new ModelObjectTypeFactory< CE4_ExPkgTemplateToPkg >();
    factoryMap[ "CE4_checkOutableObject" ] = new ModelObjectTypeFactory< CE4_checkOutableObject >();
    factoryMap[ "CE4_ExPkgBaseRevision" ] = new ModelObjectTypeFactory< CE4_ExPkgBaseRevision >();
    factoryMap[ "CE4_ExPkgTempMaster" ] = new ModelObjectTypeFactory< CE4_ExPkgTempMaster >();
    factoryMap[ "CE4_ExPkgRefsMaster" ] = new ModelObjectTypeFactory< CE4_ExPkgRefsMaster >();
    factoryMap[ "CE4_Supplier" ] = new ModelObjectTypeFactory< CE4_Supplier >();
    factoryMap[ "CE4_ExPkgToReferences" ] = new ModelObjectTypeFactory< CE4_ExPkgToReferences >();
    factoryMap[ "CE4_ExPkgTargetsMaster" ] = new ModelObjectTypeFactory< CE4_ExPkgTargetsMaster >();
    factoryMap[ "CE4_ExPkgMaster" ] = new ModelObjectTypeFactory< CE4_ExPkgMaster >();
    factoryMap[ "CE4_PackageStatus" ] = new ModelObjectTypeFactory< CE4_PackageStatus >();
    factoryMap[ "CE4_ExPkgRefsRevision" ] = new ModelObjectTypeFactory< CE4_ExPkgRefsRevision >();
    factoryMap[ "CE4_ExPkgTemp" ] = new ModelObjectTypeFactory< CE4_ExPkgTemp >();
    factoryMap[ "CE4_ExPkgTempRevMasterS" ] = new ModelObjectTypeFactory< CE4_ExPkgTempRevMasterS >();
    factoryMap[ "CE4_ExPkgRefsRevisionMaster" ] = new ModelObjectTypeFactory< CE4_ExPkgRefsRevisionMaster >();
    factoryMap[ "CE4_ExPkgTargetsRevMasterS" ] = new ModelObjectTypeFactory< CE4_ExPkgTargetsRevMasterS >();
    factoryMap[ "CE4_ExPkgRevision" ] = new ModelObjectTypeFactory< CE4_ExPkgRevision >();
    factoryMap[ "CE4_ExPkgTempRevision" ] = new ModelObjectTypeFactory< CE4_ExPkgTempRevision >();
    factoryMap[ "CE4_ExPkgTargetsRevisionMaster" ] = new ModelObjectTypeFactory< CE4_ExPkgTargetsRevisionMaster >();
    factoryMap[ "CE4_ExPkgTempRevisionMaster" ] = new ModelObjectTypeFactory< CE4_ExPkgTempRevisionMaster >();
    factoryMap[ "CE4_exportableObject" ] = new ModelObjectTypeFactory< CE4_exportableObject >();
    factoryMap[ "CE4_ExPkgBaseMasterS" ] = new ModelObjectTypeFactory< CE4_ExPkgBaseMasterS >();
    factoryMap[ "CE4_ExPkgXML" ] = new ModelObjectTypeFactory< CE4_ExPkgXML >();
    factoryMap[ "CE4_ExPkgTargets" ] = new ModelObjectTypeFactory< CE4_ExPkgTargets >();
    factoryMap[ "CE4_ExPkgRefs" ] = new ModelObjectTypeFactory< CE4_ExPkgRefs >();
    factoryMap[ "CE4_ExPkgMasterS" ] = new ModelObjectTypeFactory< CE4_ExPkgMasterS >();
    factoryMap[ "CE4_ExPkgOutgoingPackage" ] = new ModelObjectTypeFactory< CE4_ExPkgOutgoingPackage >();
    factoryMap[ "CE4_ExPkgToRecipients" ] = new ModelObjectTypeFactory< CE4_ExPkgToRecipients >();
    factoryMap[ "CE4_ExPkgTempMasterS" ] = new ModelObjectTypeFactory< CE4_ExPkgTempMasterS >();
    factoryMap[ "CE4_ExPkgTargetsMasterS" ] = new ModelObjectTypeFactory< CE4_ExPkgTargetsMasterS >();
    factoryMap[ "CE4_ExPkgToTargets" ] = new ModelObjectTypeFactory< CE4_ExPkgToTargets >();
    factoryMap[ "CE4_ExPkgBaseMaster" ] = new ModelObjectTypeFactory< CE4_ExPkgBaseMaster >();
    factoryMap[ "CE4_ExPkgRefsRevMasterS" ] = new ModelObjectTypeFactory< CE4_ExPkgRefsRevMasterS >();
    factoryMap[ "CE4_ExPkgBaseRevMasterS" ] = new ModelObjectTypeFactory< CE4_ExPkgBaseRevMasterS >();
    factoryMap[ "CE4_ExPkgRevisionMaster" ] = new ModelObjectTypeFactory< CE4_ExPkgRevisionMaster >();

}

/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

  Auto-generated source from Teamcenter Data Model.
                 DO NOT EDIT

 ==================================================
*/

package com.teamcenter.soa.client.model;

import java.lang.reflect.Constructor;
import java.util.HashMap;

import com.teamcenter.soa.client.model.ModelObjectFactory;
import com.teamcenter.soa.client.model.Type;


/**
 * Generated class to construct the various client model classes
 * that represent the TcTypes and TcClasses in the server.
 * This subclass specifically adds objects for extension model Tccadex.
 */

public class StrongObjectFactoryTccadex extends StrongObjectFactory implements ModelObjectFactory
{
    static boolean inited = false;

    public static synchronized void init()
    {
        if ( !inited )
        {
            StrongObjectFactory.init();

            Class[] parameterTypes = {Type.class, String.class};

            try
            {
                objConstructorMap.put("CE4_ExPkgBaseMaster", com.teamcenter.soa.client.model.strong.CE4_ExPkgBaseMaster.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgMaster", com.teamcenter.soa.client.model.strong.CE4_ExPkgMaster.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgTempMaster", com.teamcenter.soa.client.model.strong.CE4_ExPkgTempMaster.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgRefsMaster", com.teamcenter.soa.client.model.strong.CE4_ExPkgRefsMaster.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgTargetsMaster", com.teamcenter.soa.client.model.strong.CE4_ExPkgTargetsMaster.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgBaseRevisionMaster", com.teamcenter.soa.client.model.strong.CE4_ExPkgBaseRevisionMaster.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgRevisionMaster", com.teamcenter.soa.client.model.strong.CE4_ExPkgRevisionMaster.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgTempRevisionMaster", com.teamcenter.soa.client.model.strong.CE4_ExPkgTempRevisionMaster.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgRefsRevisionMaster", com.teamcenter.soa.client.model.strong.CE4_ExPkgRefsRevisionMaster.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgTargetsRevisionMaster", com.teamcenter.soa.client.model.strong.CE4_ExPkgTargetsRevisionMaster.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgBase", com.teamcenter.soa.client.model.strong.CE4_ExPkgBase.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkg", com.teamcenter.soa.client.model.strong.CE4_ExPkg.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgTemp", com.teamcenter.soa.client.model.strong.CE4_ExPkgTemp.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgRefs", com.teamcenter.soa.client.model.strong.CE4_ExPkgRefs.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgTargets", com.teamcenter.soa.client.model.strong.CE4_ExPkgTargets.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgBaseRevision", com.teamcenter.soa.client.model.strong.CE4_ExPkgBaseRevision.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgRevision", com.teamcenter.soa.client.model.strong.CE4_ExPkgRevision.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgTempRevision", com.teamcenter.soa.client.model.strong.CE4_ExPkgTempRevision.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgRefsRevision", com.teamcenter.soa.client.model.strong.CE4_ExPkgRefsRevision.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgTargetsRevision", com.teamcenter.soa.client.model.strong.CE4_ExPkgTargetsRevision.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_PackageStatus", com.teamcenter.soa.client.model.strong.CE4_PackageStatus.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_Supplier", com.teamcenter.soa.client.model.strong.CE4_Supplier.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgBaseMasterS", com.teamcenter.soa.client.model.strong.CE4_ExPkgBaseMasterS.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgMasterS", com.teamcenter.soa.client.model.strong.CE4_ExPkgMasterS.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgTempMasterS", com.teamcenter.soa.client.model.strong.CE4_ExPkgTempMasterS.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgRefsMasterS", com.teamcenter.soa.client.model.strong.CE4_ExPkgRefsMasterS.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgTargetsMasterS", com.teamcenter.soa.client.model.strong.CE4_ExPkgTargetsMasterS.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgBaseRevMasterS", com.teamcenter.soa.client.model.strong.CE4_ExPkgBaseRevMasterS.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgRevMasterS", com.teamcenter.soa.client.model.strong.CE4_ExPkgRevMasterS.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgTempRevMasterS", com.teamcenter.soa.client.model.strong.CE4_ExPkgTempRevMasterS.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgRefsRevMasterS", com.teamcenter.soa.client.model.strong.CE4_ExPkgRefsRevMasterS.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgTargetsRevMasterS", com.teamcenter.soa.client.model.strong.CE4_ExPkgTargetsRevMasterS.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgIncomingPackage", com.teamcenter.soa.client.model.strong.CE4_ExPkgIncomingPackage.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgOutgoingPackage", com.teamcenter.soa.client.model.strong.CE4_ExPkgOutgoingPackage.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgTemplateToPkg", com.teamcenter.soa.client.model.strong.CE4_ExPkgTemplateToPkg.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgToRecipients", com.teamcenter.soa.client.model.strong.CE4_ExPkgToRecipients.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgToReferences", com.teamcenter.soa.client.model.strong.CE4_ExPkgToReferences.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgToTargets", com.teamcenter.soa.client.model.strong.CE4_ExPkgToTargets.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_ExPkgXML", com.teamcenter.soa.client.model.strong.CE4_ExPkgXML.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_checkOutableObject", com.teamcenter.soa.client.model.strong.CE4_checkOutableObject.class.getConstructor(parameterTypes));
                objConstructorMap.put("CE4_exportableObject", com.teamcenter.soa.client.model.strong.CE4_exportableObject.class.getConstructor(parameterTypes));

            }
            catch( NoSuchMethodException  e)
            {
                throw new IllegalArgumentException( e.getMessage() );
            }
            catch( SecurityException  e)
            {
                throw new IllegalArgumentException( e.getMessage() );
            }  

            inited = true;
        }
        
    }
    
}

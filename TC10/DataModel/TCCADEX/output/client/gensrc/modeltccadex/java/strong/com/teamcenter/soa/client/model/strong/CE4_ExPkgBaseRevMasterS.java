/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

  Auto-generated source from Teamcenter Data Model.
                 DO NOT EDIT

 ==================================================
*/

package com.teamcenter.soa.client.model.strong;

import java.util.List;
import com.teamcenter.soa.exceptions.NotLoadedException;

import com.teamcenter.soa.client.model.strong.ItemVersionMaster;


/**
 * Generated class to represent the TcType/TcClass CE4_ExPkgBaseRevMasterS
 */

public class CE4_ExPkgBaseRevMasterS extends ItemVersionMaster
{

    public CE4_ExPkgBaseRevMasterS( com.teamcenter.soa.client.model.Type type, String uid )
    {
        super( type, uid );
    }


}


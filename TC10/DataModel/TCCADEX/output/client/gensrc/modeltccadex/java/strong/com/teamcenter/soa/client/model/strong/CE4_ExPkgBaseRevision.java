/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

  Auto-generated source from Teamcenter Data Model.
                 DO NOT EDIT

 ==================================================
*/

package com.teamcenter.soa.client.model.strong;

import java.util.List;
import com.teamcenter.soa.exceptions.NotLoadedException;

import com.teamcenter.soa.client.model.strong.ItemRevision;

import com.teamcenter.soa.client.model.strong.CE4_PackageStatus;

/**
 * Generated class to represent the TcType/TcClass CE4_ExPkgBaseRevision
 */

public class CE4_ExPkgBaseRevision extends ItemRevision
{

    public CE4_ExPkgBaseRevision( com.teamcenter.soa.client.model.Type type, String uid )
    {
        super( type, uid );
    }

    public String[] get_ce4_checkedOutNodes()
    throws NotLoadedException
    {
        return getPropertyObject("ce4_checkedOutNodes").getStringArrayValue();
    }

    public String[] get_ce4_renderingNodes()
    throws NotLoadedException
    {
        return getPropertyObject("ce4_renderingNodes").getStringArrayValue();
    }

    public String get_ce4_ep_serial_no()
    throws NotLoadedException
    {
        return getPropertyObject("ce4_ep_serial_no").getStringValue();
    }

    public String[] get_ce4_nativeNodes()
    throws NotLoadedException
    {
        return getPropertyObject("ce4_nativeNodes").getStringArrayValue();
    }

    public String[] get_ce4_doNotExportNodes()
    throws NotLoadedException
    {
        return getPropertyObject("ce4_doNotExportNodes").getStringArrayValue();
    }

    public String get_ce4_status()
    throws NotLoadedException
    {
        return getPropertyObject("ce4_status").getStringValue();
    }

    public String[] get_ce4_siteCheckedOutNodes()
    throws NotLoadedException
    {
        return getPropertyObject("ce4_siteCheckedOutNodes").getStringArrayValue();
    }

    public String[] get_ce4_epConfiguration()
    throws NotLoadedException
    {
        return getPropertyObject("ce4_epConfiguration").getStringArrayValue();
    }

    public boolean get_ce4_isUpdated()
    throws NotLoadedException
    {
        return getPropertyObject("ce4_isUpdated").getBoolValue();
    }

    public String get_ce4_revisionRule()
    throws NotLoadedException
    {
        return getPropertyObject("ce4_revisionRule").getStringValue();
    }

    public CE4_PackageStatus[] get_ce4_pkgHistory()
    throws NotLoadedException
    {
        List list = getPropertyObject("ce4_pkgHistory").getModelObjectListValue();
        return (CE4_PackageStatus[]) list.toArray(new CE4_PackageStatus[list.size()]);
    }

    public String get_ce4_statusRemarks()
    throws NotLoadedException
    {
        return getPropertyObject("ce4_statusRemarks").getStringValue();
    }


}


/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

  Auto-generated source from Teamcenter Data Model.
                 DO NOT EDIT

 ==================================================
*/

package com.teamcenter.soa.client.model.strong;

import java.util.List;
import com.teamcenter.soa.exceptions.NotLoadedException;

import com.teamcenter.soa.client.model.strong.Item;


/**
 * Generated class to represent the TcType/TcClass CE4_ExPkgRefs
 */

public class CE4_ExPkgRefs extends Item
{

    public CE4_ExPkgRefs( com.teamcenter.soa.client.model.Type type, String uid )
    {
        super( type, uid );
    }


}


/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

  Auto-generated source from Teamcenter Data Model.
                 DO NOT EDIT

 ==================================================
*/

package com.teamcenter.soa.client.model.strong;

import java.util.List;
import com.teamcenter.soa.exceptions.NotLoadedException;

import com.teamcenter.soa.client.model.strong.ItemRevision;


/**
 * Generated class to represent the TcType/TcClass CE4_ExPkgRefsRevision
 */

public class CE4_ExPkgRefsRevision extends ItemRevision
{

    public CE4_ExPkgRefsRevision( com.teamcenter.soa.client.model.Type type, String uid )
    {
        super( type, uid );
    }


}


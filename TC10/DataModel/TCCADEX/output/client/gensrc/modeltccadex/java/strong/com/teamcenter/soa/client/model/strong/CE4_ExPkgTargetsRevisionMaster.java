/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

  Auto-generated source from Teamcenter Data Model.
                 DO NOT EDIT

 ==================================================
*/

package com.teamcenter.soa.client.model.strong;

import java.util.List;
import com.teamcenter.soa.exceptions.NotLoadedException;

import com.teamcenter.soa.client.model.strong.ItemRevision_Master;


/**
 * Generated class to represent the TcType/TcClass CE4_ExPkgTargetsRevisionMaster
 */

public class CE4_ExPkgTargetsRevisionMaster extends ItemRevision_Master
{

    public CE4_ExPkgTargetsRevisionMaster( com.teamcenter.soa.client.model.Type type, String uid )
    {
        super( type, uid );
    }


}


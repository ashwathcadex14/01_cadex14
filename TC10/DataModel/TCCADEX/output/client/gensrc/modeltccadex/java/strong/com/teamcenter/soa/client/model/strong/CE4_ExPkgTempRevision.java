/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

  Auto-generated source from Teamcenter Data Model.
                 DO NOT EDIT

 ==================================================
*/

package com.teamcenter.soa.client.model.strong;

import java.util.List;
import com.teamcenter.soa.exceptions.NotLoadedException;

import com.teamcenter.soa.client.model.strong.CE4_ExPkgBaseRevision;

import com.teamcenter.soa.client.model.ModelObject;

/**
 * Generated class to represent the TcType/TcClass CE4_ExPkgTempRevision
 */

public class CE4_ExPkgTempRevision extends CE4_ExPkgBaseRevision
{

    public CE4_ExPkgTempRevision( com.teamcenter.soa.client.model.Type type, String uid )
    {
        super( type, uid );
    }

    public ModelObject[] get_CE4_ExPkgToRecipients()
    throws NotLoadedException
    {
        return getPropertyObject("CE4_ExPkgToRecipients").getModelObjectArrayValue();
    }

    public ModelObject[] get_CE4_ExPkgTemplateToPkg()
    throws NotLoadedException
    {
        return getPropertyObject("CE4_ExPkgTemplateToPkg").getModelObjectArrayValue();
    }


}


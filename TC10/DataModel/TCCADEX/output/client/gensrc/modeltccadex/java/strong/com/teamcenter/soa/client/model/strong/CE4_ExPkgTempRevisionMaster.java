/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

  Auto-generated source from Teamcenter Data Model.
                 DO NOT EDIT

 ==================================================
*/

package com.teamcenter.soa.client.model.strong;

import java.util.List;
import com.teamcenter.soa.exceptions.NotLoadedException;

import com.teamcenter.soa.client.model.strong.CE4_ExPkgBaseRevisionMaster;


/**
 * Generated class to represent the TcType/TcClass CE4_ExPkgTempRevisionMaster
 */

public class CE4_ExPkgTempRevisionMaster extends CE4_ExPkgBaseRevisionMaster
{

    public CE4_ExPkgTempRevisionMaster( com.teamcenter.soa.client.model.Type type, String uid )
    {
        super( type, uid );
    }


}


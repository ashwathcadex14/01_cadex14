/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

  Auto-generated source from Teamcenter Data Model.
                 DO NOT EDIT

 ==================================================
*/

package com.teamcenter.soa.client.model.strong;

import java.util.List;
import com.teamcenter.soa.exceptions.NotLoadedException;

import com.teamcenter.soa.client.model.strong.WorkspaceObject;


/**
 * Generated class to represent the TcType/TcClass CE4_Supplier
 */

public class CE4_Supplier extends WorkspaceObject
{

    public CE4_Supplier( com.teamcenter.soa.client.model.Type type, String uid )
    {
        super( type, uid );
    }

    public String get_ce4_supplierId()
    throws NotLoadedException
    {
        return getPropertyObject("ce4_supplierId").getStringValue();
    }

    public String get_ce4_supplierLocation()
    throws NotLoadedException
    {
        return getPropertyObject("ce4_supplierLocation").getStringValue();
    }


}


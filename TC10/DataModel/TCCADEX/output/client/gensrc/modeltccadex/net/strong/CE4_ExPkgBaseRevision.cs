/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

  Auto-generated source from Teamcenter Data Model.
                 DO NOT EDIT

 ==================================================
*/

using System;
using Teamcenter.Soa.Exceptions;

using Teamcenter.Soa.Client.Model.Strong;


namespace Teamcenter.Soa.Client.Model.Strong
{

    /**
     * Generated class to represent the TcType/TcClass CE4_ExPkgBaseRevision
     */

    public class CE4_ExPkgBaseRevision : ItemRevision
    {
        public CE4_ExPkgBaseRevision( Teamcenter.Soa.Client.Model.SoaType type, string uid ) :  base( type, uid )
        {
        }

        public string[] Ce4_checkedOutNodes
        {
            get 
            { 
                return GetProperty("ce4_checkedOutNodes").StringArrayValue; 
            }
        }

        public string[] Ce4_renderingNodes
        {
            get 
            { 
                return GetProperty("ce4_renderingNodes").StringArrayValue; 
            }
        }

        public string Ce4_ep_serial_no
        {
            get 
            { 
                return GetProperty("ce4_ep_serial_no").StringValue; 
            }
        }

        public string[] Ce4_nativeNodes
        {
            get 
            { 
                return GetProperty("ce4_nativeNodes").StringArrayValue; 
            }
        }

        public string[] Ce4_doNotExportNodes
        {
            get 
            { 
                return GetProperty("ce4_doNotExportNodes").StringArrayValue; 
            }
        }

        public string Ce4_status
        {
            get 
            { 
                return GetProperty("ce4_status").StringValue; 
            }
        }

        public string[] Ce4_siteCheckedOutNodes
        {
            get 
            { 
                return GetProperty("ce4_siteCheckedOutNodes").StringArrayValue; 
            }
        }

        public string[] Ce4_epConfiguration
        {
            get 
            { 
                return GetProperty("ce4_epConfiguration").StringArrayValue; 
            }
        }

        public bool Ce4_isUpdated
        {
            get 
            { 
                return GetProperty("ce4_isUpdated").BoolValue; 
            }
        }

        public string Ce4_revisionRule
        {
            get 
            { 
                return GetProperty("ce4_revisionRule").StringValue; 
            }
        }

        public CE4_PackageStatus[] Ce4_pkgHistory
        {
            get
            {
                System.Collections.IList list = GetProperty("ce4_pkgHistory").ModelObjectListValue;
                CE4_PackageStatus[] results = new CE4_PackageStatus[list.Count];
                list.CopyTo(results, 0);
                return results;
            }
        }

        public string Ce4_statusRemarks
        {
            get 
            { 
                return GetProperty("ce4_statusRemarks").StringValue; 
            }
        }


    }
}
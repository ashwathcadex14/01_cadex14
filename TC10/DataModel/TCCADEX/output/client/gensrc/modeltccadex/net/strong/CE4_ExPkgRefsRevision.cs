/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

  Auto-generated source from Teamcenter Data Model.
                 DO NOT EDIT

 ==================================================
*/

using System;
using Teamcenter.Soa.Exceptions;

using Teamcenter.Soa.Client.Model.Strong;


namespace Teamcenter.Soa.Client.Model.Strong
{

    /**
     * Generated class to represent the TcType/TcClass CE4_ExPkgRefsRevision
     */

    public class CE4_ExPkgRefsRevision : ItemRevision
    {
        public CE4_ExPkgRefsRevision( Teamcenter.Soa.Client.Model.SoaType type, string uid ) :  base( type, uid )
        {
        }


    }
}
/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

  Auto-generated source from Teamcenter Data Model.
                 DO NOT EDIT

 ==================================================
*/

using System;
using Teamcenter.Soa.Exceptions;

using Teamcenter.Soa.Client.Model.Strong;
using Teamcenter.Soa.Client.Model;


namespace Teamcenter.Soa.Client.Model.Strong
{

    /**
     * Generated class to represent the TcType/TcClass CE4_ExPkgRevision
     */

    public class CE4_ExPkgRevision : CE4_ExPkgBaseRevision
    {
        public CE4_ExPkgRevision( Teamcenter.Soa.Client.Model.SoaType type, string uid ) :  base( type, uid )
        {
        }

        public ModelObject[] CE4_ExPkgIncomingPackage
        {
            get 
            { 
                return GetProperty("CE4_ExPkgIncomingPackage").ModelObjectArrayValue; 
            }
        }

        public ModelObject[] CE4_ExPkgOutgoingPackage
        {
            get 
            { 
                return GetProperty("CE4_ExPkgOutgoingPackage").ModelObjectArrayValue; 
            }
        }

        public ModelObject[] CE4_ExPkgToRecipients
        {
            get 
            { 
                return GetProperty("CE4_ExPkgToRecipients").ModelObjectArrayValue; 
            }
        }

        public ModelObject[] CE4_ExPkgToReferences
        {
            get 
            { 
                return GetProperty("CE4_ExPkgToReferences").ModelObjectArrayValue; 
            }
        }

        public ModelObject[] CE4_ExPkgToTargets
        {
            get 
            { 
                return GetProperty("CE4_ExPkgToTargets").ModelObjectArrayValue; 
            }
        }

        public ModelObject[] CE4_ExPkgXML
        {
            get 
            { 
                return GetProperty("CE4_ExPkgXML").ModelObjectArrayValue; 
            }
        }


    }
}
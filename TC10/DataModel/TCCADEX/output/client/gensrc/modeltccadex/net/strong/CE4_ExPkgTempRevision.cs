/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

  Auto-generated source from Teamcenter Data Model.
                 DO NOT EDIT

 ==================================================
*/

using System;
using Teamcenter.Soa.Exceptions;

using Teamcenter.Soa.Client.Model.Strong;
using Teamcenter.Soa.Client.Model;


namespace Teamcenter.Soa.Client.Model.Strong
{

    /**
     * Generated class to represent the TcType/TcClass CE4_ExPkgTempRevision
     */

    public class CE4_ExPkgTempRevision : CE4_ExPkgBaseRevision
    {
        public CE4_ExPkgTempRevision( Teamcenter.Soa.Client.Model.SoaType type, string uid ) :  base( type, uid )
        {
        }

        public ModelObject[] CE4_ExPkgToRecipients
        {
            get 
            { 
                return GetProperty("CE4_ExPkgToRecipients").ModelObjectArrayValue; 
            }
        }

        public ModelObject[] CE4_ExPkgTemplateToPkg
        {
            get 
            { 
                return GetProperty("CE4_ExPkgTemplateToPkg").ModelObjectArrayValue; 
            }
        }


    }
}
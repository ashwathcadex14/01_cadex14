/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

  Auto-generated source from Teamcenter Data Model.
                 DO NOT EDIT

 ==================================================
*/

using System;
using Teamcenter.Soa.Exceptions;

using Teamcenter.Soa.Client.Model.Strong;


namespace Teamcenter.Soa.Client.Model.Strong
{

    /**
     * Generated class to represent the TcType/TcClass CE4_Supplier
     */

    public class CE4_Supplier : WorkspaceObject
    {
        public CE4_Supplier( Teamcenter.Soa.Client.Model.SoaType type, string uid ) :  base( type, uid )
        {
        }

        public string Ce4_supplierId
        {
            get 
            { 
                return GetProperty("ce4_supplierId").StringValue; 
            }
        }

        public string Ce4_supplierLocation
        {
            get 
            { 
                return GetProperty("ce4_supplierLocation").StringValue; 
            }
        }


    }
}
/* 
 @<COPYRIGHT>@
 ==================================================
 Copyright 2012
 Siemens Product Lifecycle Management Software Inc.
 All Rights Reserved.
 ==================================================
 @<COPYRIGHT>@

 ==================================================

  Auto-generated source from Teamcenter Data Model.
                 DO NOT EDIT

 ==================================================
*/

using System;
using System.Collections;
using System.Reflection;

using Teamcenter.Soa.Client.Model.Strong;
using Teamcenter.Soa.Internal.Client.Model;


namespace Teamcenter.Soa.Client.Model
{

    /**
     * Generated class to construct the various client model classes
     * that represent the TcTypes and TcClasses in the server.
     * This subclass specifically adds objects for extension model Tccadex.
     */

    public class StrongObjectFactoryTccadex : StrongObjectFactory
    {
        static bool inited = false;
        private static readonly object singletonLock = new object();

        public static void Init()
        {
            lock (singletonLock)
            {
                if ( !inited )
                {
                    StrongObjectFactory.Init();
    
                    Type[] types = { typeof(Teamcenter.Soa.Client.Model.SoaType), typeof(string) };
    
                     objConstructorMap["CE4_ExPkgBaseMaster"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgBaseMaster ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgMaster"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgMaster ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgTempMaster"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgTempMaster ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgRefsMaster"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgRefsMaster ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgTargetsMaster"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgTargetsMaster ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgBaseRevisionMaster"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgBaseRevisionMaster ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgRevisionMaster"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgRevisionMaster ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgTempRevisionMaster"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgTempRevisionMaster ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgRefsRevisionMaster"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgRefsRevisionMaster ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgTargetsRevisionMaster"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgTargetsRevisionMaster ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgBase"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgBase ).GetConstructor( types);
                objConstructorMap["CE4_ExPkg"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkg ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgTemp"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgTemp ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgRefs"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgRefs ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgTargets"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgTargets ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgBaseRevision"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgBaseRevision ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgRevision"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgRevision ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgTempRevision"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgTempRevision ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgRefsRevision"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgRefsRevision ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgTargetsRevision"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgTargetsRevision ).GetConstructor( types);
                objConstructorMap["CE4_PackageStatus"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_PackageStatus ).GetConstructor( types);
                objConstructorMap["CE4_Supplier"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_Supplier ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgBaseMasterS"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgBaseMasterS ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgMasterS"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgMasterS ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgTempMasterS"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgTempMasterS ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgRefsMasterS"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgRefsMasterS ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgTargetsMasterS"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgTargetsMasterS ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgBaseRevMasterS"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgBaseRevMasterS ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgRevMasterS"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgRevMasterS ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgTempRevMasterS"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgTempRevMasterS ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgRefsRevMasterS"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgRefsRevMasterS ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgTargetsRevMasterS"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgTargetsRevMasterS ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgIncomingPackage"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgIncomingPackage ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgOutgoingPackage"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgOutgoingPackage ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgTemplateToPkg"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgTemplateToPkg ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgToRecipients"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgToRecipients ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgToReferences"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgToReferences ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgToTargets"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgToTargets ).GetConstructor( types);
                objConstructorMap["CE4_ExPkgXML"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_ExPkgXML ).GetConstructor( types);
                objConstructorMap["CE4_checkOutableObject"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_checkOutableObject ).GetConstructor( types);
                objConstructorMap["CE4_exportableObject"] = typeof(  Teamcenter.Soa.Client.Model.Strong.CE4_exportableObject ).GetConstructor( types);

     
                    inited = true;
                }
            }
        }
    }
}

@echo off

SET TC_ROOT=C:\apps\Teamcenter\TC10\tcroot

del /Q %TC_ROOT%\install\install\modules\feature_tccadexrac.xml
del /Q %TC_ROOT%\portal\plugins\CADEXStructureCompare*.jar
del /Q %TC_ROOT%\portal\plugins\com.teamcenter.rac.tccadex*.jar
del /Q %TC_ROOT%\portal\plugins\InfoExPlmxmlSDK*.jar
del /Q %TC_ROOT%\portal\plugins\CE4SoaCadExchangeManagementRac.jar
del /Q %TC_ROOT%\portal\plugins\CE4SoaCadExchangeManagementTypes.jar
del /Q %TC_ROOT%\portal\plugins\org.eclipse.debug.core_3.7.100.v20120521-2012.jar
del /Q %TC_ROOT%\portal\plugins\org.eclipse.equinox.frameworkadmin.equinox_1.0.400.v20120913-155709.jar
del /Q %TC_ROOT%\portal\plugins\org.eclipse.equinox.frameworkadmin_2.0.100.v20120913-155515.jar
del /Q %TC_ROOT%\portal\plugins\org.eclipse.ui.editors_3.8.0.v20120523-1540.jar
del /Q %TC_ROOT%\portal\plugins\org.eclipse.ui.ide_3.8.2.v20121106-170106.jar
del /Q %TC_ROOT%\portal\plugins\org.eclipse.ui.intro_3.4.200.v20120521-2344.jar

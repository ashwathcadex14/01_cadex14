/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CadExchange1305IiopSkeleton.cxx          
#      Module          :           libce4tccadex_v1.output.server.gensrc.CE4SoaCadExchangeManagement          
#      Description     :           Auto Generated SOA File          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
/* 


   Auto-generated source from service interface.
                 DO NOT EDIT


*/


#include <unidefs.h>
#if defined(SUN)
#    include <unistd.h>
#endif

#include<CadExchange1305IiopSkeleton.hxx>


#include <mld/journal/journal.h>
#include <teamcenter/soa/internal/common/Monitor.hxx>
#include <teamcenter/soa/internal/common/PolicyMarshaller.hxx>
#include <teamcenter/soa/internal/server/PolicyManager.hxx>
#include <teamcenter/soa/internal/server/PerServicePropertyPolicy.hxx>


using namespace std;
using namespace Teamcenter::Soa::Server;
using namespace Teamcenter::Soa::Internal::Server;
using namespace Teamcenter::Soa::Internal::Common;
using namespace Teamcenter::Soa::Common::Xml;
using namespace Teamcenter::Soa::Common::Exceptions;


namespace Teamcenter
{
    namespace Services
    {
    
        namespace CadExchangeManagement
        {
             namespace _2013_05
             {




    CadExchangeIiopSkeleton::CadExchangeIiopSkeleton()
    {
        m_serviceName = "CadExchange";
    
       _svcMap[ "createExportPackage" ]   = createExportPackage;
       _svcMap[ "importExchangePackage" ]   = importExchangePackage;
       _svcMap[ "setExpChkOut" ]   = setExpChkOut;

    }

    CadExchangeIiopSkeleton::~CadExchangeIiopSkeleton()
    {
    	// If the implementing class did not implement the ServicePolicy
    	// then delete it, since it was allocated here in the skeleton
	 	Teamcenter::Soa::Server::ServicePolicy* sp = dynamic_cast<Teamcenter::Soa::Server::ServicePolicy *>(_service);
    	if(sp == NULL)
    	{
    		delete _servicePolicy;
    	}
        delete _service;
    }



     void CadExchangeIiopSkeleton::initialize()
    {
   	// If the impl class has not implemented the ServicePolicy interface
    	// Create an instance of the default ServicePolicy
	 	_servicePolicy = dynamic_cast<Teamcenter::Soa::Server::ServicePolicy *>(_service);
    	if(_servicePolicy == NULL)
    	{
    		_servicePolicy = new Teamcenter::Soa::Server::ServicePolicy;
    	}
    }


 CE4::Soa::CadExchangeManagement::_2013_05::CadExchange::CreateExportPkgInput   CadExchangeIiopSkeleton::toLocal( Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPkgInput> wire )
{
     CE4::Soa::CadExchangeManagement::_2013_05::CadExchange::CreateExportPkgInput local;
     local.exPkgRevision = Utils::uidToTag( wire->getExPkgRevision()->getUid()); //TAG_GET_ASSIGNMENT2
     local.exPkgSessionTM = Utils::uidToTag( wire->getExPkgSessionTM()->getUid()); //TAG_GET_ASSIGNMENT2

     return local;
}
 CE4::Soa::CadExchangeManagement::_2013_05::CadExchange::DatasetExportCheckOutInfo   CadExchangeIiopSkeleton::toLocal( Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::DatasetExportCheckOutInfo> wire )
{
     CE4::Soa::CadExchangeManagement::_2013_05::CadExchange::DatasetExportCheckOutInfo local;
     local.dsetObject = Utils::uidToTag( wire->getDsetObject()->getUid()); //TAG_GET_ASSIGNMENT2
     local.isExport = wire->getIsExport(); //SIMPLE_ASSIGNMENT2
     local.isCheckOut = wire->getIsCheckOut(); //SIMPLE_ASSIGNMENT2
     local.bomLine = Utils::uidToTag( wire->getBomLine()->getUid()); //TAG_GET_ASSIGNMENT2
     local.bomWindow = Utils::uidToTag( wire->getBomWindow()->getUid()); //TAG_GET_ASSIGNMENT2

     return local;
}
 CE4::Soa::CadExchangeManagement::_2013_05::CadExchange::ImportPackageInput   CadExchangeIiopSkeleton::toLocal( Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportPackageInput> wire )
{
     CE4::Soa::CadExchangeManagement::_2013_05::CadExchange::ImportPackageInput local;
     local.packageRev = Utils::uidToTag( wire->getPackageRev()->getUid()); //TAG_GET_ASSIGNMENT2
     local.importTM = Utils::uidToTag( wire->getImportTM()->getUid()); //TAG_GET_ASSIGNMENT2

     return local;
}


    static CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeImpl* _service;
	static Teamcenter::Soa::Server::ServicePolicy*  	 _servicePolicy;


    void CadExchangeIiopSkeleton::createExportPackage( const std::string& xmlIn, std::string& xmlOut )
    {
        Teamcenter::Soa::Internal::Common::Monitor::markStart( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::createExportPackage" );
        JOURNAL_routine_start ("CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::createExportPackage");
        int journal_skeleton = JOURNAL_ask_call_depth();
         
        // Parse the Doc/Literal input document
        Teamcenter::Soa::Internal::Common::Monitor::markStart( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::createExportPackage::parseXML" );
        JOURNAL_routine_start( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::createExportPackage::parseXML" );
        int journal_parseXml = JOURNAL_ask_call_depth();
         

        Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPackageInput> wireIn =
                                     new Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPackageInput();
        std::string contentType = Teamcenter::Soa::Common::Xml::XmlUtil::parse( xmlIn, wireIn );


        Teamcenter::Soa::Internal::Common::Monitor::markEnd( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::createExportPackage::parseXML", "Bytes", xmlIn.length() );
        JOURNAL_assert_call_depth (journal_parseXml);
        JOURNAL_return_value (0);
        JOURNAL_routine_end();


        // Extract the c++ method arguments
        Teamcenter::Soa::Internal::Common::Monitor::markStart( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::createExportPackage::wireToLocal" );
        JOURNAL_routine_start( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::createExportPackage::wireToLocal" );
        int journal_toLocal = JOURNAL_ask_call_depth();
         
        CE4::Soa::CadExchangeManagement::_2013_05::CadExchange::CreateExportPkgInput exPkgSessionInput = toLocal( wireIn->getExPkgSessionInput());  //LOCAL_ASSIGNMENT

        
        Teamcenter::Soa::Internal::Common::Monitor::markEnd( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::createExportPackage::wireToLocal" );
        JOURNAL_assert_call_depth (journal_toLocal);
        JOURNAL_return_value (0);
        JOURNAL_routine_end();

        int journal_impl = 0;

            // Execute the method
            Teamcenter::Soa::Internal::Common::Monitor::markStart( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeImpl::createExportPackage" );
            JOURNAL_routine_start( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeImpl::createExportPackage" );
            journal_impl = JOURNAL_ask_call_depth();
 
            Teamcenter::Soa::Server::ServiceData localOut = _service->createExportPackage( exPkgSessionInput );
 
            Teamcenter::Soa::Internal::Common::Monitor::markEnd( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeImpl::createExportPackage" );
            JOURNAL_assert_call_depth (journal_impl);
            JOURNAL_return_value (0);
            JOURNAL_routine_end();

        
            // Construct the Doc/Literal return object           
            Teamcenter::Soa::Internal::Common::Monitor::markStart( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::createExportPackage::localToWire" );
            JOURNAL_routine_start( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::createExportPackage::localToWire" );
            int journal_toWire = JOURNAL_ask_call_depth();
 
            Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ServiceData> wireOut;
            wireOut = new Teamcenter::Schemas::Soa::_2006_03::Base::ServiceData();
            
            localOut.getNotifications( ); //SERVICEDATA_EVENTS_ASSIGNMENT
        localOut.inflateProperties( ); //SERVICEDATA_OUT_ASSIGNMENT
        wireOut = localOut.getWireData();

 
            Teamcenter::Soa::Internal::Common::Monitor::markEnd( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::createExportPackage::localToWire" );
            JOURNAL_assert_call_depth (journal_toWire);
            JOURNAL_return_value (0);
            JOURNAL_routine_end();


            // Serialize the document
            Teamcenter::Soa::Internal::Common::Monitor::markStart( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::createExportPackage::serializeXML" );
            JOURNAL_routine_start( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::createExportPackage::serializeXML" );
            int journal_serializeXml = JOURNAL_ask_call_depth();
         
            Teamcenter::Soa::Common::Xml::XmlUtil::serialize( contentType, wireOut, xmlOut );

            
            Teamcenter::Soa::Internal::Common::Monitor::markEnd( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::createExportPackage::serializeXML", "Bytes", xmlOut.length() );
            JOURNAL_assert_call_depth (journal_serializeXml);
            JOURNAL_return_value (0);
            JOURNAL_routine_end();


            
            Teamcenter::Soa::Internal::Common::Monitor::markEnd( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::createExportPackage" );
            JOURNAL_assert_call_depth (journal_skeleton);
            JOURNAL_return_value (0);
            JOURNAL_routine_end();
            return;

    }

    void CadExchangeIiopSkeleton::importExchangePackage( const std::string& xmlIn, std::string& xmlOut )
    {
        Teamcenter::Soa::Internal::Common::Monitor::markStart( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::importExchangePackage" );
        JOURNAL_routine_start ("CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::importExchangePackage");
        int journal_skeleton = JOURNAL_ask_call_depth();
         
        // Parse the Doc/Literal input document
        Teamcenter::Soa::Internal::Common::Monitor::markStart( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::importExchangePackage::parseXML" );
        JOURNAL_routine_start( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::importExchangePackage::parseXML" );
        int journal_parseXml = JOURNAL_ask_call_depth();
         

        Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportExchangePackageInput> wireIn =
                                     new Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportExchangePackageInput();
        std::string contentType = Teamcenter::Soa::Common::Xml::XmlUtil::parse( xmlIn, wireIn );


        Teamcenter::Soa::Internal::Common::Monitor::markEnd( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::importExchangePackage::parseXML", "Bytes", xmlIn.length() );
        JOURNAL_assert_call_depth (journal_parseXml);
        JOURNAL_return_value (0);
        JOURNAL_routine_end();


        // Extract the c++ method arguments
        Teamcenter::Soa::Internal::Common::Monitor::markStart( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::importExchangePackage::wireToLocal" );
        JOURNAL_routine_start( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::importExchangePackage::wireToLocal" );
        int journal_toLocal = JOURNAL_ask_call_depth();
         
        CE4::Soa::CadExchangeManagement::_2013_05::CadExchange::ImportPackageInput importPkgInput = toLocal( wireIn->getImportPkgInput());  //LOCAL_ASSIGNMENT

        
        Teamcenter::Soa::Internal::Common::Monitor::markEnd( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::importExchangePackage::wireToLocal" );
        JOURNAL_assert_call_depth (journal_toLocal);
        JOURNAL_return_value (0);
        JOURNAL_routine_end();

        int journal_impl = 0;

            // Execute the method
            Teamcenter::Soa::Internal::Common::Monitor::markStart( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeImpl::importExchangePackage" );
            JOURNAL_routine_start( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeImpl::importExchangePackage" );
            journal_impl = JOURNAL_ask_call_depth();
 
            Teamcenter::Soa::Server::ServiceData localOut = _service->importExchangePackage( importPkgInput );
 
            Teamcenter::Soa::Internal::Common::Monitor::markEnd( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeImpl::importExchangePackage" );
            JOURNAL_assert_call_depth (journal_impl);
            JOURNAL_return_value (0);
            JOURNAL_routine_end();

        
            // Construct the Doc/Literal return object           
            Teamcenter::Soa::Internal::Common::Monitor::markStart( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::importExchangePackage::localToWire" );
            JOURNAL_routine_start( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::importExchangePackage::localToWire" );
            int journal_toWire = JOURNAL_ask_call_depth();
 
            Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ServiceData> wireOut;
            wireOut = new Teamcenter::Schemas::Soa::_2006_03::Base::ServiceData();
            
            localOut.getNotifications( ); //SERVICEDATA_EVENTS_ASSIGNMENT
        localOut.inflateProperties( ); //SERVICEDATA_OUT_ASSIGNMENT
        wireOut = localOut.getWireData();

 
            Teamcenter::Soa::Internal::Common::Monitor::markEnd( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::importExchangePackage::localToWire" );
            JOURNAL_assert_call_depth (journal_toWire);
            JOURNAL_return_value (0);
            JOURNAL_routine_end();


            // Serialize the document
            Teamcenter::Soa::Internal::Common::Monitor::markStart( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::importExchangePackage::serializeXML" );
            JOURNAL_routine_start( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::importExchangePackage::serializeXML" );
            int journal_serializeXml = JOURNAL_ask_call_depth();
         
            Teamcenter::Soa::Common::Xml::XmlUtil::serialize( contentType, wireOut, xmlOut );

            
            Teamcenter::Soa::Internal::Common::Monitor::markEnd( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::importExchangePackage::serializeXML", "Bytes", xmlOut.length() );
            JOURNAL_assert_call_depth (journal_serializeXml);
            JOURNAL_return_value (0);
            JOURNAL_routine_end();


            
            Teamcenter::Soa::Internal::Common::Monitor::markEnd( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::importExchangePackage" );
            JOURNAL_assert_call_depth (journal_skeleton);
            JOURNAL_return_value (0);
            JOURNAL_routine_end();
            return;

    }

    void CadExchangeIiopSkeleton::setExpChkOut( const std::string& xmlIn, std::string& xmlOut )
    {
        Teamcenter::Soa::Internal::Common::Monitor::markStart( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::setExpChkOut" );
        JOURNAL_routine_start ("CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::setExpChkOut");
        int journal_skeleton = JOURNAL_ask_call_depth();
         
        // Parse the Doc/Literal input document
        Teamcenter::Soa::Internal::Common::Monitor::markStart( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::setExpChkOut::parseXML" );
        JOURNAL_routine_start( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::setExpChkOut::parseXML" );
        int journal_parseXml = JOURNAL_ask_call_depth();
         

        Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::SetExpChkOutInput> wireIn =
                                     new Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::SetExpChkOutInput();
        std::string contentType = Teamcenter::Soa::Common::Xml::XmlUtil::parse( xmlIn, wireIn );


        Teamcenter::Soa::Internal::Common::Monitor::markEnd( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::setExpChkOut::parseXML", "Bytes", xmlIn.length() );
        JOURNAL_assert_call_depth (journal_parseXml);
        JOURNAL_return_value (0);
        JOURNAL_routine_end();


        // Extract the c++ method arguments
        Teamcenter::Soa::Internal::Common::Monitor::markStart( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::setExpChkOut::wireToLocal" );
        JOURNAL_routine_start( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::setExpChkOut::wireToLocal" );
        int journal_toLocal = JOURNAL_ask_call_depth();
         
       Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::DatasetExportCheckOutInfoArray wireInDatasetExportChkOutInfo = wireIn->getDatasetExportChkOutInfoArray(); //ARRAY_ASSIGNMENT
       std::vector< CE4::Soa::CadExchangeManagement::_2013_05::CadExchange::DatasetExportCheckOutInfo > datasetExportChkOutInfo;
       for(size_t i = 0; i<wireInDatasetExportChkOutInfo.size(); i++)
       {
           datasetExportChkOutInfo.push_back( toLocal( wireInDatasetExportChkOutInfo[i]) );
       }

        
        Teamcenter::Soa::Internal::Common::Monitor::markEnd( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::setExpChkOut::wireToLocal" );
        JOURNAL_assert_call_depth (journal_toLocal);
        JOURNAL_return_value (0);
        JOURNAL_routine_end();

        int journal_impl = 0;

            // Execute the method
            Teamcenter::Soa::Internal::Common::Monitor::markStart( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeImpl::setExpChkOut" );
            JOURNAL_routine_start( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeImpl::setExpChkOut" );
            journal_impl = JOURNAL_ask_call_depth();
 
            Teamcenter::Soa::Server::ServiceData localOut = _service->setExpChkOut( datasetExportChkOutInfo );
 
            Teamcenter::Soa::Internal::Common::Monitor::markEnd( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeImpl::setExpChkOut" );
            JOURNAL_assert_call_depth (journal_impl);
            JOURNAL_return_value (0);
            JOURNAL_routine_end();

        
            // Construct the Doc/Literal return object           
            Teamcenter::Soa::Internal::Common::Monitor::markStart( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::setExpChkOut::localToWire" );
            JOURNAL_routine_start( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::setExpChkOut::localToWire" );
            int journal_toWire = JOURNAL_ask_call_depth();
 
            Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ServiceData> wireOut;
            wireOut = new Teamcenter::Schemas::Soa::_2006_03::Base::ServiceData();
            
            localOut.getNotifications( ); //SERVICEDATA_EVENTS_ASSIGNMENT
        localOut.inflateProperties( ); //SERVICEDATA_OUT_ASSIGNMENT
        wireOut = localOut.getWireData();

 
            Teamcenter::Soa::Internal::Common::Monitor::markEnd( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::setExpChkOut::localToWire" );
            JOURNAL_assert_call_depth (journal_toWire);
            JOURNAL_return_value (0);
            JOURNAL_routine_end();


            // Serialize the document
            Teamcenter::Soa::Internal::Common::Monitor::markStart( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::setExpChkOut::serializeXML" );
            JOURNAL_routine_start( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::setExpChkOut::serializeXML" );
            int journal_serializeXml = JOURNAL_ask_call_depth();
         
            Teamcenter::Soa::Common::Xml::XmlUtil::serialize( contentType, wireOut, xmlOut );

            
            Teamcenter::Soa::Internal::Common::Monitor::markEnd( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::setExpChkOut::serializeXML", "Bytes", xmlOut.length() );
            JOURNAL_assert_call_depth (journal_serializeXml);
            JOURNAL_return_value (0);
            JOURNAL_routine_end();


            
            Teamcenter::Soa::Internal::Common::Monitor::markEnd( "CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeIiopSkeleton::setExpChkOut" );
            JOURNAL_assert_call_depth (journal_skeleton);
            JOURNAL_return_value (0);
            JOURNAL_routine_end();
            return;

    }




CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeImpl* CadExchangeIiopSkeleton::_service = new CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeImpl;
Teamcenter::Soa::Server::ServicePolicy*  	  CadExchangeIiopSkeleton::_servicePolicy = NULL;
static T2LService* me_CadExchange = TcServiceManager::instance().registerService( "CadExchangeManagement-2013-05-CadExchange", new CadExchangeIiopSkeleton );

 //register the service for getServiceList()
 static std::string registeredServiceOnStartup_CadExchange = ServiceManager::instance().registerService("CadExchangeManagement-2013-05-CadExchange");

}}}}    // End Namespace


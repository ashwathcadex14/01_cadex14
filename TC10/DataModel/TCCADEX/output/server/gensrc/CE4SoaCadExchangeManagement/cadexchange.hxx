/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           cadexchange.hxx          
#      Module          :           libce4tccadex_v1.output.server.gensrc.CE4SoaCadExchangeManagement          
#      Description     :           Auto Generated SOA File          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
/* 


   Auto-generated source from service interface.
                 DO NOT EDIT


*/
#ifndef TEAMCENTER_SERVICES_CADEXCHANGEMANAGEMENT_CADEXCHANGE_HXX 
#define TEAMCENTER_SERVICES_CADEXCHANGEMANAGEMENT_CADEXCHANGE_HXX


#include <cadexchange1305.hxx>


namespace CE4
{
    namespace Soa
    {
        namespace CadExchangeManagement
        {
            class CadExchange;
        }
    }
}


/**
 * Service for Cad Exchange operations of TCCADEX
 * <br>
 * <br>
 * <br>
 * <b>Library Reference:</b>
 * <ul>
 * <li type="disc">libce4soacadexchangemanagement.dll
 * </li>
 * <li type="disc">libce4soacadexchangemanagementtypes.dll (runtime dependency)
 * </li>
 * </ul>
 */

class CE4::Soa::CadExchangeManagement::CadExchange
    : public CE4::Soa::CadExchangeManagement::_2013_05::CadExchange
{};

#endif


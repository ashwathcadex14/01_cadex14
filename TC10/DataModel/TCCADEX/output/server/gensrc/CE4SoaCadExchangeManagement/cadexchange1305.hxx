/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           cadexchange1305.hxx          
#      Module          :           libce4tccadex_v1.output.server.gensrc.CE4SoaCadExchangeManagement          
#      Description     :           Auto Generated SOA File          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
/* 



   Auto-generated source from service interface.
                 DO NOT EDIT


*/

#ifndef TEAMCENTER_SERVICES_CADEXCHANGEMANAGEMENT_2013_05_CADEXCHANGE_HXX 
#define TEAMCENTER_SERVICES_CADEXCHANGEMANAGEMENT_2013_05_CADEXCHANGE_HXX


#include <ae/Dataset.hxx>
#include <bom/BOMLine.hxx>
#include <bom/BOMWindow.hxx>
#include <pie/TransferMode.hxx>
#include <tccore/ItemRevision.hxx>
#include <teamcenter/soa/server/ServiceData.hxx>

#include <teamcenter/soa/server/ServiceException.hxx>
#include <metaframework/BusinessObjectRef.hxx>

#include <CadExchangeManagement_exports.h>

namespace CE4
{
    namespace Soa
    {
        namespace CadExchangeManagement
        {
            namespace _2013_05
            {
                class CadExchange;
            }
        }
    }
}


class SOACADEXCHANGEMANAGEMENT_API CE4::Soa::CadExchangeManagement::_2013_05::CadExchange

{
public:

    struct CreateExportPkgInput;
    struct DatasetExportCheckOutInfo;
    struct ImportPackageInput;

    struct CreateExportPkgInput
    {
        /**
         * Exchange Package Revision
         */
        BusinessObjectRef<Teamcenter::ItemRevision> exPkgRevision;
        /**
         * Transfer Mode to be used during export
         */
        BusinessObjectRef<Teamcenter::TransferMode> exPkgSessionTM;
    };

    struct DatasetExportCheckOutInfo
    {
        /**
         * Dataset for setting Exported, CheckOut in Supplier CAD Exchange
         */
        BusinessObjectRef<Teamcenter::Dataset> dsetObject;
        /**
         * Need to export or not
         */
        bool isExport;
        /**
         * Need to check out or not
         */
        bool isCheckOut;
        /**
         * Line to which dataset is associated
         */
        BusinessObjectRef<Teamcenter::BOMLine> bomLine;
        /**
         * Window of the current package
         */
        BusinessObjectRef<Teamcenter::BOMWindow> bomWindow;
    };

    struct ImportPackageInput
    {
        /**
         * Package Revision to import the exchange package
         */
        BusinessObjectRef<Teamcenter::ItemRevision> packageRev;
        /**
         * Transfer mode to import the exchange package
         */
        BusinessObjectRef<Teamcenter::TransferMode> importTM;
    };




    /**
     * .
     *
     * @param exPkgSessionInput
     *        Input for export package session
     *
     * @return
     *
     */
    virtual Teamcenter::Soa::Server::ServiceData createExportPackage ( const CreateExportPkgInput& exPkgSessionInput ) = 0;

    /**
     * .
     *
     * @param importPkgInput
     *        Input details for importing the exchange package from supplier
     *
     * @return
     *
     */
    virtual Teamcenter::Soa::Server::ServiceData importExchangePackage ( const ImportPackageInput& importPkgInput ) = 0;

    /**
     * .
     *
     * @param datasetExportChkOutInfo
     *        vector of Dataset Export Check Out info
     *
     * @return
     *
     */
    virtual Teamcenter::Soa::Server::ServiceData setExpChkOut ( const std::vector< DatasetExportCheckOutInfo >& datasetExportChkOutInfo ) = 0;


};

#include <CadExchangeManagement_undef.h>
#endif


/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CE4_ExPkgBaseRevisionGenImpl.cxx          
#      Module          :           libce4tccadex_v1.output.server.gensrc.CE4tccadex          
#      Description     :           For Setter customization of ce4_status attribute          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    








/*==================================================================================================
File description:
   This file contains the implementation for the Business Object CE4_ExPkgBaseRevisionGenImpl

    Filename: CE4_ExPkgBaseRevisionGenImpl.hxx
    Module  : CE4tccadex

    @BMIDE autogenerated
==================================================================================================*/

#include <pom/pom/pom_errors.h>
#include <pom/pom/pom.h>
#include <base_utils/Mem.h>
#include <tccore/method.h>
#include <base_utils/DateTime.hxx>
#include <property/prop_msg.h>
#include <ug_va_copy.h>

#include <CE4tccadex/CE4_ExPkgBaseRevisionGenImpl.hxx>

#include <CE4tccadex/CE4_ExPkgBaseRevisionDelegate.hxx>
#include <metaframework/BusinessObjectRegistry.hxx> 
#include <metaframework/BusinessObjectRef.hxx>
#include <extensionframework/OperationDispatcherRegistry.hxx>

using  namespace  TCCADEX;

extern  "C"
{
    static  int  setCe4_status_1String( METHOD_message_t *msg, va_list args );
}

//----------------------------------------------------------------------------------
// CE4_ExPkgBaseRevisionGenImpl::CE4_ExPkgBaseRevisionGenImpl(CE4_ExPkgBaseRevision &)
// Constructor for the class
//----------------------------------------------------------------------------------
CE4_ExPkgBaseRevisionGenImpl::CE4_ExPkgBaseRevisionGenImpl( CE4_ExPkgBaseRevision &busObj )
    : m_busObj( &busObj )
{
}

//----------------------------------------------------------------------------------
// CE4_ExPkgBaseRevisionGenImpl::~CE4_ExPkgBaseRevisionGenImpl()
// Destructor for the class
//----------------------------------------------------------------------------------
CE4_ExPkgBaseRevisionGenImpl::~CE4_ExPkgBaseRevisionGenImpl()
{
}
//----------------------------------------------------------------------------------
// CE4_ExPkgBaseRevisionGenImpl::registerPropertyFunctions()
// registerPropertyFunctions
//----------------------------------------------------------------------------------
int  CE4_ExPkgBaseRevisionGenImpl::registerPropertyFunctions()
{
    int ifail = ITK_ok;
    METHOD_id_t  method;
    if ( ifail == ITK_ok)
    {
        ifail = METHOD__register_prop_operationFn("CE4_ExPkgBaseRevision", "ce4_status",
                                                     PROP_set_value_string_msg, &setCe4_status_1String, 0, &method);
    }



    return  ifail;
}
//----------------------------------------------------------------------------------
// CE4_ExPkgBaseRevisionGenImpl::getCE4_ExPkgBaseRevision()
// Accessor for the business object
//----------------------------------------------------------------------------------
CE4_ExPkgBaseRevision *
CE4_ExPkgBaseRevisionGenImpl::getCE4_ExPkgBaseRevision() const
{
    return  m_busObj;
}



//----------------------------------------------------------------------------------
// CE4_ExPkgBaseRevisionGenImpl::initializeClass()
// Initialize Class
//----------------------------------------------------------------------------------
int  CE4_ExPkgBaseRevisionGenImpl::initializeClass()
{
    int ifail = ITK_ok;
    Teamcenter::BusinessObjectRegistry::instance().initializePropertyFnRegister( "CE4_ExPkgBaseRevision", &CE4_ExPkgBaseRevisionGenImpl::registerPropertyFunctions );
    return  ifail;
}



//----------------------------------------------------------------------------------
// CE4_ExPkgBaseRevisionGenImpl::newInstance()
// Create a new instance of this Business Object
//----------------------------------------------------------------------------------
Teamcenter::RootObjectImpl * CE4_ExPkgBaseRevisionGenImpl::newInstance()
{
    CE4_ExPkgBaseRevision *busObj = dynamic_cast<CE4_ExPkgBaseRevision *>
         ( Teamcenter::BusinessObjectRegistry::instance().createBusinessObject( CE4_ExPkgBaseRevision::getInterfaceName() ) );
    if ( busObj != 0 )
    {
       return  (Teamcenter::RootObjectImpl *)(busObj->getCE4_ExPkgBaseRevisionImpl());
    }
    return  0;
}
//----------------------------------------------------------------------------------
// property Operation invocation on Business Object
//----------------------------------------------------------------------------------

static  int  setCe4_status_1String( METHOD_message_t *msg, va_list args )
{
    int ifail = ITK_ok;
    va_list largs;
	va_copy( largs, args );
	va_arg( largs, tag_t);
	const char* value = va_arg( largs, const char*);
	logical is_null = va_arg( largs, int )!=0;
	va_end( largs );
	tag_t boTag = NULLTAG;
	METHOD_PROP_MESSAGE_OBJECT( msg, boTag );
	BusinessObjectRef<CE4_ExPkgBaseRevision>boObj( boTag );
	std::string aString;
	if ( value != NULL )
	{
		aString = value;
	}
	ifail = boObj->setCe4_status( aString, is_null );
    return  ifail;
}


tag_t  TCCADEX::CE4_ExPkgBaseRevisionGenImpl::getCe4_checkedOutNodesTag() const
{
    static tag_t attr_tag = NULLTAG;
    //  statically cache the tag of the attr id. 
    if ( attr_tag == NULLTAG )
    {
        POM_attr_id_of_attr( "ce4_checkedOutNodes", "CE4_ExPkgBaseRevision", &attr_tag );
    }
    return  attr_tag;
}
tag_t  TCCADEX::CE4_ExPkgBaseRevisionGenImpl::getCe4_renderingNodesTag() const
{
    static tag_t attr_tag = NULLTAG;
    //  statically cache the tag of the attr id. 
    if ( attr_tag == NULLTAG )
    {
        POM_attr_id_of_attr( "ce4_renderingNodes", "CE4_ExPkgBaseRevision", &attr_tag );
    }
    return  attr_tag;
}
tag_t  TCCADEX::CE4_ExPkgBaseRevisionGenImpl::getCe4_ep_serial_noTag() const
{
    static tag_t attr_tag = NULLTAG;
    //  statically cache the tag of the attr id. 
    if ( attr_tag == NULLTAG )
    {
        POM_attr_id_of_attr( "ce4_ep_serial_no", "CE4_ExPkgBaseRevision", &attr_tag );
    }
    return  attr_tag;
}
tag_t  TCCADEX::CE4_ExPkgBaseRevisionGenImpl::getCe4_nativeNodesTag() const
{
    static tag_t attr_tag = NULLTAG;
    //  statically cache the tag of the attr id. 
    if ( attr_tag == NULLTAG )
    {
        POM_attr_id_of_attr( "ce4_nativeNodes", "CE4_ExPkgBaseRevision", &attr_tag );
    }
    return  attr_tag;
}
tag_t  TCCADEX::CE4_ExPkgBaseRevisionGenImpl::getCe4_doNotExportNodesTag() const
{
    static tag_t attr_tag = NULLTAG;
    //  statically cache the tag of the attr id. 
    if ( attr_tag == NULLTAG )
    {
        POM_attr_id_of_attr( "ce4_doNotExportNodes", "CE4_ExPkgBaseRevision", &attr_tag );
    }
    return  attr_tag;
}
tag_t  TCCADEX::CE4_ExPkgBaseRevisionGenImpl::getCe4_statusTag() const
{
    static tag_t attr_tag = NULLTAG;
    //  statically cache the tag of the attr id. 
    if ( attr_tag == NULLTAG )
    {
        POM_attr_id_of_attr( "ce4_status", "CE4_ExPkgBaseRevision", &attr_tag );
    }
    return  attr_tag;
}
tag_t  TCCADEX::CE4_ExPkgBaseRevisionGenImpl::getCe4_siteCheckedOutNodesTag() const
{
    static tag_t attr_tag = NULLTAG;
    //  statically cache the tag of the attr id. 
    if ( attr_tag == NULLTAG )
    {
        POM_attr_id_of_attr( "ce4_siteCheckedOutNodes", "CE4_ExPkgBaseRevision", &attr_tag );
    }
    return  attr_tag;
}
tag_t  TCCADEX::CE4_ExPkgBaseRevisionGenImpl::getCe4_epConfigurationTag() const
{
    static tag_t attr_tag = NULLTAG;
    //  statically cache the tag of the attr id. 
    if ( attr_tag == NULLTAG )
    {
        POM_attr_id_of_attr( "ce4_epConfiguration", "CE4_ExPkgBaseRevision", &attr_tag );
    }
    return  attr_tag;
}
tag_t  TCCADEX::CE4_ExPkgBaseRevisionGenImpl::getCe4_isUpdatedTag() const
{
    static tag_t attr_tag = NULLTAG;
    //  statically cache the tag of the attr id. 
    if ( attr_tag == NULLTAG )
    {
        POM_attr_id_of_attr( "ce4_isUpdated", "CE4_ExPkgBaseRevision", &attr_tag );
    }
    return  attr_tag;
}
tag_t  TCCADEX::CE4_ExPkgBaseRevisionGenImpl::getCe4_revisionRuleTag() const
{
    static tag_t attr_tag = NULLTAG;
    //  statically cache the tag of the attr id. 
    if ( attr_tag == NULLTAG )
    {
        POM_attr_id_of_attr( "ce4_revisionRule", "CE4_ExPkgBaseRevision", &attr_tag );
    }
    return  attr_tag;
}
tag_t  TCCADEX::CE4_ExPkgBaseRevisionGenImpl::getCe4_pkgHistoryTag() const
{
    static tag_t attr_tag = NULLTAG;
    //  statically cache the tag of the attr id. 
    if ( attr_tag == NULLTAG )
    {
        POM_attr_id_of_attr( "ce4_pkgHistory", "CE4_ExPkgBaseRevision", &attr_tag );
    }
    return  attr_tag;
}
tag_t  TCCADEX::CE4_ExPkgBaseRevisionGenImpl::getCe4_statusRemarksTag() const
{
    static tag_t attr_tag = NULLTAG;
    //  statically cache the tag of the attr id. 
    if ( attr_tag == NULLTAG )
    {
        POM_attr_id_of_attr( "ce4_statusRemarks", "CE4_ExPkgBaseRevision", &attr_tag );
    }
    return  attr_tag;
}


//----------------------------------------------------------------------------------
// TCCADEX::CE4_ExPkgBaseRevisionGenImpl::setCe4_statusBase
// Base for Modifier of ce4_status
//----------------------------------------------------------------------------------
int TCCADEX::CE4_ExPkgBaseRevisionGenImpl::setCe4_statusBase( const std::string& value, bool isNull )
{
    int stat = POM_ok;
    stat = AttributeAccessor::setStringValue(
                        m_busObj->getTag(),
                        getCe4_statusTag(),
                        value,
                        isNull );
    return stat;
}

//----------------------------------------------------------------------------------
// TCCADEX::CE4_ExPkgBaseRevisionGenImpl::getCe4_statusBase
// Base for Accessor of ce4_status
//----------------------------------------------------------------------------------
int TCCADEX::CE4_ExPkgBaseRevisionGenImpl::getCe4_statusBase( std::string &value, bool &isNull ) const
{
    int stat = POM_ok;
    stat = AttributeAccessor::getStringValue(
                        m_busObj->getTag(),
                        getCe4_statusTag(),
                        value,
                        isNull );
    return stat;
}

/**
 * Setter for a string Property
 * @param value - Value to be set for the parameter
 * @param isNull - If true, set the parameter value to null
 * @return - Status. 0 if successful
 */
int  CE4_ExPkgBaseRevisionGenImpl::setCe4_status( const std::string &value, bool isNull )
{
    return  getCE4_ExPkgBaseRevision()->setCe4_status( value, isNull );
}



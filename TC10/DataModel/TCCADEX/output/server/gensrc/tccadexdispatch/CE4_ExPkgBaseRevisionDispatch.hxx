/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CE4_ExPkgBaseRevisionDispatch.hxx          
#      Module          :           libce4tccadex_v1.output.server.gensrc.tccadexdispatch          
#      Description     :           Dispatch codes for re-routing setter methods to custom methods          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    


/*==================================================================================================
File description:
   This file contains the declaration for the Business Object  CE4_ExPkgBaseRevisionDispatch

    Filename: CE4_ExPkgBaseRevisionDispatch.hxx
    Module  : tccadexdispatch

    @BMIDE autogenerated
==================================================================================================*/

#ifndef TCCADEX__CE4_EXPKGBASEREVISIONDISPATCH_HXX
#define TCCADEX__CE4_EXPKGBASEREVISIONDISPATCH_HXX

#include <CE4tccadex/CE4_ExPkgBaseRevision.hxx>
#include <CE4tccadex/CE4_ExPkgBaseRevisionDelegate.hxx>
#include <foundationdispatch/ItemRevisionDispatch.hxx>
#include <tccadexdispatch/initializetccadex.hxx>


#include <tccadexdispatch/libtccadexdispatch_exports.h>

namespace TCCADEX
{
   class CE4_ExPkgBaseRevisionDispatch;
}
namespace TCCADEX
{
   class CE4_ExPkgBaseRevisionDelegate;
}
namespace Teamcenter
{
   class BusinessObjectImpl;
}

class  TCCADEXDISPATCH_API TCCADEX::CE4_ExPkgBaseRevisionDispatch 
         : public TCCADEX::CE4_ExPkgBaseRevisionDelegate
{
public:
    // Method to initialize this Class
    static int initializeClass();

    // Method to get the CE4_ExPkgBaseRevisionDispatch instance
    static CE4_ExPkgBaseRevisionDispatch& getInstance();


   /**
    * Setter for a string Property
    * @param value - Value to be set for the parameter
    * @param isNull - If true, set the parameter value to null
    * @return - Status. 0 if successful
    */
   virtual int  setCe4_status(CE4_ExPkgBaseRevision &ce4_expkgbaserevision,  const std::string &value, bool isNull );

   /**
    * Setter for a string Property
    * @param value - Value to be set for the parameter
    * @param isNull - If true, set the parameter value to null
    * @return - Status. 0 if successful
    */
   virtual int  setCe4_statusBase(CE4_ExPkgBaseRevision &ifObj,  const std::string &value, bool isNull );

// SubClasses for Operation Dispatcher

protected:

private:
    // Pointer to the dispatch instance
    static CE4_ExPkgBaseRevisionDispatch* m_dispatch;

     // Default constructor
    CE4_ExPkgBaseRevisionDispatch();

    // Default Destructor
    virtual ~CE4_ExPkgBaseRevisionDispatch();

    // Constructor Method
    static Teamcenter::RootObject * constructor();

    // Constructor for a CE4_ExPkgBaseRevisionDispatch
    CE4_ExPkgBaseRevisionDispatch( const CE4_ExPkgBaseRevisionDispatch& );

    // Copy constructor
    CE4_ExPkgBaseRevisionDispatch& operator = (const CE4_ExPkgBaseRevisionDispatch&);

    //friend
    friend void TCCADEX::initializetccadexInternal();
    friend class TCCADEX::CE4_ExPkgBaseRevisionDelegate;
};

#include <tccadexdispatch/libtccadexdispatch_undef.h>
#endif // TCCADEX__CE4_EXPKGBASEREVISIONDISPATCH_HXX

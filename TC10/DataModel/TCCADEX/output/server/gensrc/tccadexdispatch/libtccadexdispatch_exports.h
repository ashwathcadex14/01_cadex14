/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           libtccadexdispatch_exports.h          
#      Module          :           libce4tccadex_v1.output.server.gensrc.tccadexdispatch          
#      Description     :           Dispatch codes for re-routing setter methods to custom methods          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    

/** 
    @file 

    This file contains the declaration for the Dispatch Library  tccadexdispatch

*/

#include <common/library_indicators.h>

#ifdef EXPORTLIBRARY
#define EXPORTLIBRARY something else
#error ExportLibrary was already defined
#endif

#define EXPORTLIBRARY            libtccadexdispatch

#if !defined(LIBTCCADEXDISPATCH) && !defined(IPLIB)
#   error IPLIB or LIBTCCADEXDISPATCH is not defined
#endif

/* Handwritten code should use TCCADEXDISPATCH_API, not TCCADEXDISPATCHEXPORT */

#define TCCADEXDISPATCH_API TCCADEXDISPATCHEXPORT

#if IPLIB==libtccadexdispatch || defined(LIBTCCADEXDISPATCH)
#   if defined(__lint)
#       define TCCADEXDISPATCHEXPORT       __export(tccadexdispatch)
#       define TCCADEXDISPATCHGLOBAL       extern __global(tccadexdispatch)
#       define TCCADEXDISPATCHPRIVATE      extern __private(tccadexdispatch)
#   elif defined(_WIN32)
#       define TCCADEXDISPATCHEXPORT       __declspec(dllexport)
#       define TCCADEXDISPATCHGLOBAL       extern __declspec(dllexport)
#       define TCCADEXDISPATCHPRIVATE      extern
#   else
#       define TCCADEXDISPATCHEXPORT
#       define TCCADEXDISPATCHGLOBAL       extern
#       define TCCADEXDISPATCHPRIVATE      extern
#   endif
#else
#   if defined(__lint)
#       define TCCADEXDISPATCHEXPORT       __export(tccadexdispatch)
#       define TCCADEXDISPATCHGLOBAL       extern __global(tccadexdispatch)
#   elif defined(_WIN32) && !defined(WNT_STATIC_LINK)
#       define TCCADEXDISPATCHEXPORT      __declspec(dllimport)
#       define TCCADEXDISPATCHGLOBAL       extern __declspec(dllimport)
#   else
#       define TCCADEXDISPATCHEXPORT
#       define TCCADEXDISPATCHGLOBAL       extern
#   endif
#endif

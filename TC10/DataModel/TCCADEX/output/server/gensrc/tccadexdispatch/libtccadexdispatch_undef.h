/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           libtccadexdispatch_undef.h          
#      Module          :           libce4tccadex_v1.output.server.gensrc.tccadexdispatch          
#      Description     :           Dispatch codes for re-routing setter methods to custom methods          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    


#include <common/library_indicators.h>

#if !defined(EXPORTLIBRARY)
#   error EXPORTLIBRARY is not defined
#endif

#undef EXPORTLIBRARY

#if !defined(LIBTCCADEXDISPATCH) && !defined(IPLIB)
#   error IPLIB orLIBTCCADEXDISPATCH is not defined
#endif

#undef TCCADEXDISPATCH_API
#undef TCCADEXDISPATCHEXPORT
#undef TCCADEXDISPATCHGLOBAL
#undef TCCADEXDISPATCHPRIVATE

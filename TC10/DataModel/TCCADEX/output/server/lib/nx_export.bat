@echo off

echo ###################################################
echo.
echo					NX EXPORT						
echo.
echo ###################################################

echo NX - %1 %2 %3 %4 %5 %6 %7 %8

SET NX_BASE_DIR=%1

SET TC_ROOT=%2
SET TC_DATA=%3
SET FMS_HOME=%4

call %NX_BASE_DIR%\UGII\ugiicmd.bat %NX_BASE_DIR%

call %TC_DATA%\tc_profilevars.bat

for /f "delims=," %%F in (%5) DO (
	echo %%F
	call %UGII_BASE_DIR%\UGMANAGER\ugmanager_clone -pim=yes -u=infodba -p=infodba -g=dba -operation=export %%F -default_naming=autotranslate
)

echo %ERRORLEVEL%

/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           ImportExchangePackageInput.hxx          
#      Module          :           libce4tccadex_v1.output.types.gensrc.CE4SoaCadExchangeManagement.cpp.ce4.schemas.cadexchangemanagement._2013_05.cadexchange          
#      Description     :           Auto Generated SOA file for CadExchange Service          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
/* 










   Auto-generated source from service interface.
                 DO NOT EDIT


*/

#ifndef CE4__SCHEMAS__CADEXCHANGEMANAGEMENT___2013_05__CADEXCHANGE_IMPORTEXCHANGEPACKAGEINPUT_HXX
#define CE4__SCHEMAS__CADEXCHANGEMANAGEMENT___2013_05__CADEXCHANGE_IMPORTEXCHANGEPACKAGEINPUT_HXX


#include <vector>
#include <string>
#include <set>
#include <ostream>
#include <new> // for size_t

#include <teamcenter/soa/common/MemoryManager.hxx>
#include <teamcenter/soa/common/DateTime.hxx>
#include <teamcenter/soa/common/AutoPtr.hxx>
#include <teamcenter/soa/common/xml/BaseObject.hxx>
#include <teamcenter/soa/common/xml/XmlUtils.hxx>
#include <teamcenter/soa/common/xml/XmlStream.hxx>
#include <teamcenter/soa/common/xml/JsonStream.hxx>

#include <ce4/schemas/cadexchangemanagement/_2013_05/cadexchange/ImportPackageInput.hxx>


#include <ce4/schemas/cadexchangemanagement/_2013_05/cadexchange/Cadexchange_exports.h>

namespace Ce4
{
    namespace Schemas
    {
        namespace Cadexchangemanagement
        {
            namespace _2013_05
            {
                namespace Cadexchange
                {

                    class ImportExchangePackageInput;
                    typedef std::vector< Teamcenter::Soa::Common::AutoPtr<ImportExchangePackageInput> > ImportExchangePackageInputArray;
                    class ImportPackageInput;

                }
            }
        }
    }
}





class CADEXCHANGE_API Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportExchangePackageInput : 
        public Teamcenter::Soa::Common::Xml::BaseObject 
{
 public:
    
    ImportExchangePackageInput(   );


    Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportPackageInput>& getImportPkgInput() ;
    const Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportPackageInput>& getImportPkgInput() const;
    void setImportPkgInput( const Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportPackageInput>& importPackageInput );


    virtual void outputXML( Teamcenter::Soa::Common::Xml::XmlStream& out ) const { outputXML( out, "ImportExchangePackageInput" ); }
    virtual void outputXML( Teamcenter::Soa::Common::Xml::XmlStream& out, const std::string& elementName ) const;
    virtual void parse    ( const Teamcenter::Soa::Common::Xml::XMLNode& node );
    virtual void getNamespaces( std::set< std::string >& namespaces ) const;
  
    virtual void writeJSON( Teamcenter::Soa::Common::Xml::JsonStream* stream ) const;
    virtual void parseJSON( const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Soa::Internal::Json::JSONObject> node );

    
    SOA_CLASS_NEW_OPERATORS
    

protected:
    virtual ImportExchangePackageInput* reallyClone();

         Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportPackageInput>   m_importPkgInput;

};


#include <ce4/schemas/cadexchangemanagement/_2013_05/cadexchange/Cadexchange_undef.h>
#endif


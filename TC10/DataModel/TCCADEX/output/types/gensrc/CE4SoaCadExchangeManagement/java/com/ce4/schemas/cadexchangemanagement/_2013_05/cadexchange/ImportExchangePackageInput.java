/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           ImportExchangePackageInput.java          
#      Module          :           libce4tccadex_v1.output.types.gensrc.CE4SoaCadExchangeManagement.java.com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange          
#      Description     :           Auto Generated SOA file for CadExchange Service          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.09.15 at 12:30:57 PM IST 
//


package com.ce4.schemas.cadexchangemanagement._2013_05.cadexchange;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="importPkgInput" type="{http://ce4.com/Schemas/CadExchangeManagement/2013-05/CadExchange}ImportPackageInput"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "importPkgInput"
})
@XmlRootElement(name = "ImportExchangePackageInput")
public class ImportExchangePackageInput
    implements Serializable
{

    @XmlElement(required = true)
    protected ImportPackageInput importPkgInput;

    /**
     * Gets the value of the importPkgInput property.
     * 
     * @return
     *     possible object is
     *     {@link ImportPackageInput }
     *     
     */
    public ImportPackageInput getImportPkgInput() {
        return importPkgInput;
    }

    /**
     * Sets the value of the importPkgInput property.
     * 
     * @param value
     *     allowed object is
     *     {@link ImportPackageInput }
     *     
     */
    public void setImportPkgInput(ImportPackageInput value) {
        this.importPkgInput = value;
    }

    public boolean isSetImportPkgInput() {
        return (this.importPkgInput!= null);
    }

}

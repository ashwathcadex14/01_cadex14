/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CreateExportPkgInput_2013_05_Cadexchange.cs          
#      Module          :           libce4tccadex_v1.output.types.gensrc.CE4SoaCadExchangeManagement.net          
#      Description     :           Auto Generated SOA file for CadExchange Service          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
/* 










   Auto-generated source from service interface.
                 DO NOT EDIT


*/

using System.Xml.Serialization;

using Teamcenter.Schemas.Soa._2006_03.Base;


namespace Ce4.Schemas.Cadexchangemanagement._2013_05.Cadexchange 
{


[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd2csharp", "1.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://ce4.com/Schemas/CadExchangeManagement/2013-05/CadExchange", IsNullable=false)]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
  public partial class CreateExportPkgInput 
  {

         private Teamcenter.Schemas.Soa._2006_03.Base.ModelObject ExPkgRevisionField;
     ///<summary>XML Serialization Attributes</summary>
     [System.Xml.Serialization.XmlElementAttribute("exPkgRevision")]
     public Teamcenter.Schemas.Soa._2006_03.Base.ModelObject ExPkgRevision
     { 
        get { return this.ExPkgRevisionField;}
        set { this.ExPkgRevisionField = value;}
     }

     ///<summary>To support access via generated code.</summary>
     public Teamcenter.Schemas.Soa._2006_03.Base.ModelObject getExPkgRevision()
     { 
       return this.ExPkgRevisionField;
     }
     public void setExPkgRevision(Teamcenter.Schemas.Soa._2006_03.Base.ModelObject val)
     { 
       this.ExPkgRevisionField = val;
     }


     private Teamcenter.Schemas.Soa._2006_03.Base.ModelObject ExPkgSessionTMField;
     ///<summary>XML Serialization Attributes</summary>
     [System.Xml.Serialization.XmlElementAttribute("exPkgSessionTM")]
     public Teamcenter.Schemas.Soa._2006_03.Base.ModelObject ExPkgSessionTM
     { 
        get { return this.ExPkgSessionTMField;}
        set { this.ExPkgSessionTMField = value;}
     }

     ///<summary>To support access via generated code.</summary>
     public Teamcenter.Schemas.Soa._2006_03.Base.ModelObject getExPkgSessionTM()
     { 
       return this.ExPkgSessionTMField;
     }
     public void setExPkgSessionTM(Teamcenter.Schemas.Soa._2006_03.Base.ModelObject val)
     { 
       this.ExPkgSessionTMField = val;
     }



    
    


  } // type
} // ns
            






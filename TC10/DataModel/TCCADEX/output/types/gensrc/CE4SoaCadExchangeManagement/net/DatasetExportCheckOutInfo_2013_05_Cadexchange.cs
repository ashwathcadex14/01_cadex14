/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           DatasetExportCheckOutInfo_2013_05_Cadexchange.cs          
#      Module          :           libce4tccadex_v1.output.types.gensrc.CE4SoaCadExchangeManagement.net          
#      Description     :           Auto Generated SOA file for CadExchange Service          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
/* 










   Auto-generated source from service interface.
                 DO NOT EDIT


*/

using System.Xml.Serialization;

using Teamcenter.Schemas.Soa._2006_03.Base;


namespace Ce4.Schemas.Cadexchangemanagement._2013_05.Cadexchange 
{


[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd2csharp", "1.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://ce4.com/Schemas/CadExchangeManagement/2013-05/CadExchange", IsNullable=false)]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
  public partial class DatasetExportCheckOutInfo 
  {

         private bool IsExportField;
     ///<summary>XML Serialization Attributes</summary>
     [System.Xml.Serialization.XmlAttributeAttribute(AttributeName="isExport")]
     public bool IsExport
     { 
        get { return this.IsExportField;}
        set { this.IsExportField = value;}
     }

     ///<summary>To support access via generated code.</summary>
     public bool getIsExport()
     { 
       return this.IsExportField;
     }
     public void setIsExport(bool val)
     { 
       this.IsExportField = val;
     }


     private bool IsCheckOutField;
     ///<summary>XML Serialization Attributes</summary>
     [System.Xml.Serialization.XmlAttributeAttribute(AttributeName="isCheckOut")]
     public bool IsCheckOut
     { 
        get { return this.IsCheckOutField;}
        set { this.IsCheckOutField = value;}
     }

     ///<summary>To support access via generated code.</summary>
     public bool getIsCheckOut()
     { 
       return this.IsCheckOutField;
     }
     public void setIsCheckOut(bool val)
     { 
       this.IsCheckOutField = val;
     }


     private Teamcenter.Schemas.Soa._2006_03.Base.ModelObject DsetObjectField;
     ///<summary>XML Serialization Attributes</summary>
     [System.Xml.Serialization.XmlElementAttribute("dsetObject")]
     public Teamcenter.Schemas.Soa._2006_03.Base.ModelObject DsetObject
     { 
        get { return this.DsetObjectField;}
        set { this.DsetObjectField = value;}
     }

     ///<summary>To support access via generated code.</summary>
     public Teamcenter.Schemas.Soa._2006_03.Base.ModelObject getDsetObject()
     { 
       return this.DsetObjectField;
     }
     public void setDsetObject(Teamcenter.Schemas.Soa._2006_03.Base.ModelObject val)
     { 
       this.DsetObjectField = val;
     }


     private Teamcenter.Schemas.Soa._2006_03.Base.ModelObject BomLineField;
     ///<summary>XML Serialization Attributes</summary>
     [System.Xml.Serialization.XmlElementAttribute("bomLine")]
     public Teamcenter.Schemas.Soa._2006_03.Base.ModelObject BomLine
     { 
        get { return this.BomLineField;}
        set { this.BomLineField = value;}
     }

     ///<summary>To support access via generated code.</summary>
     public Teamcenter.Schemas.Soa._2006_03.Base.ModelObject getBomLine()
     { 
       return this.BomLineField;
     }
     public void setBomLine(Teamcenter.Schemas.Soa._2006_03.Base.ModelObject val)
     { 
       this.BomLineField = val;
     }


     private Teamcenter.Schemas.Soa._2006_03.Base.ModelObject BomWindowField;
     ///<summary>XML Serialization Attributes</summary>
     [System.Xml.Serialization.XmlElementAttribute("bomWindow")]
     public Teamcenter.Schemas.Soa._2006_03.Base.ModelObject BomWindow
     { 
        get { return this.BomWindowField;}
        set { this.BomWindowField = value;}
     }

     ///<summary>To support access via generated code.</summary>
     public Teamcenter.Schemas.Soa._2006_03.Base.ModelObject getBomWindow()
     { 
       return this.BomWindowField;
     }
     public void setBomWindow(Teamcenter.Schemas.Soa._2006_03.Base.ModelObject val)
     { 
       this.BomWindowField = val;
     }



    
    


  } // type
} // ns
            






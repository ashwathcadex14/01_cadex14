/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           cadexchange1305impl.cxx          
#      Module          :           libce4tccadex_v1.src.server.CE4SoaCadExchangeManagement          
#      Description     :           Main implementation file for Cad Exchange Service Operations          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
/* 







*/

#include <unidefs.h>
#if defined(SUN)
#include <unistd.h>
#endif
#include <common/CE4_Common.h>
#include <cadexchange1305impl.hxx>

using namespace CE4::Soa::CadExchangeManagement::_2013_05;
using namespace Teamcenter::Soa::Server;

Teamcenter::Soa::Server::ServiceData CadExchangeImpl::createExportPackage ( const CreateExportPkgInput& exPkgSessionInput )
{
	int iStatus = ITK_ok;
    ServiceData *data = new ServiceData();

	const char* __function__ = "Teamcenter::Soa::Server::ServiceData CadExchangeImpl::createExportPackage" ;
	CE4_TRACE_ENTER() ;

	try
	{
		CE4_TRACE_CALL(iStatus = CE4_createExportPackage(exPkgSessionInput.exPkgRevision.tag(), exPkgSessionInput.exPkgSessionTM.tag()));
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	CE4_TRACE_LEAVE() ;

	return *data;
}

Teamcenter::Soa::Server::ServiceData CadExchangeImpl::importExchangePackage ( const ImportPackageInput& importPkgInput )
{
	int iStatus = ITK_ok;
    ServiceData *data = new ServiceData();

	const char* __function__ = "Teamcenter::Soa::Server::ServiceData CadExchangeImpl::importExchangePackage" ;
	CE4_TRACE_ENTER() ;

	try
	{
		CE4_TRACE_CALL(iStatus = CE4_importPackage(importPkgInput.packageRev.tag(), importPkgInput.importTM.tag()));
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	CE4_TRACE_LEAVE() ;

	return *data;
}



Teamcenter::Soa::Server::ServiceData CadExchangeImpl::setExpChkOut ( const std::vector< DatasetExportCheckOutInfo >& datasetExportChkOutInfo )
{
	int iStatus = ITK_ok;
    ServiceData *data = new ServiceData();

	const char* __function__ = "Teamcenter::Soa::Server::ServiceData CadExchangeImpl::setExpChkOut" ;
	CE4_TRACE_ENTER() ;

	try
	{
		DatasetExportCheckOutInfo dsetInfo = datasetExportChkOutInfo.at(0);
		CE4_TRACE_CALL(iStatus = CE4_setExpCheckOut(dsetInfo.dsetObject->getTag(), dsetInfo.isExport, dsetInfo.isCheckOut, dsetInfo.bomLine->getTag(), dsetInfo.bomWindow->getTag()));
		
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
			data->addErrorStack();
	}

	CE4_TRACE_LEAVE() ;

	return *data;
}




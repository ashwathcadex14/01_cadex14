/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           cadexchange1305impl.hxx          
#      Module          :           libce4tccadex_v1.src.server.CE4SoaCadExchangeManagement          
#      Description     :           Header for Cad Exchange Service Operations          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
/* 







*/

#ifndef TEAMCENTER_SERVICES_CADEXCHANGEMANAGEMENT_2013_05_CADEXCHANGE_IMPL_HXX 
#define TEAMCENTER_SERVICES_CADEXCHANGEMANAGEMENT_2013_05_CADEXCHANGE_IMPL_HXX


#include <cadexchange1305.hxx>

#include <CadExchangeManagement_exports.h>

namespace CE4
{
    namespace Soa
    {
        namespace CadExchangeManagement
        {
            namespace _2013_05
            {
                class CadExchangeImpl;
            }
        }
    }
}


class SOACADEXCHANGEMANAGEMENT_API CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeImpl : public CE4::Soa::CadExchangeManagement::_2013_05::CadExchange

{
public:

    virtual Teamcenter::Soa::Server::ServiceData createExportPackage ( const CreateExportPkgInput& exPkgSessionInput );
    virtual Teamcenter::Soa::Server::ServiceData importExchangePackage ( const ImportPackageInput& importPkgInput );
    virtual Teamcenter::Soa::Server::ServiceData setExpChkOut ( const std::vector< DatasetExportCheckOutInfo >& datasetExportChkOutInfo );


};

#include <CadExchangeManagement_undef.h>
#endif

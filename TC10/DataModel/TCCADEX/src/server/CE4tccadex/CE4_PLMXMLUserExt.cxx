/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CE4_PLMXMLUserExt.cxx          
#      Module          :           libce4tccadex_v1.src.server.CE4tccadex          
#      Description     :           PLMXML User Exits          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    

/* 
 * @file 
 *
 *   This file contains the implementation for the Extension CE4_PLMXMLUserExt
 *
 */
#include <CE4tccadex/CE4_PLMXMLUserExt.hxx>


int CE4_PLMXMLUserExt( METHOD_message_t *msg, va_list args )
{
	int iStatus				= 0 ;
	const char* __function__ = "CE4_PLMXMLUserExt" ;
	CE4_TRACE_ENTER() ;

	try
	{	
		CE4_TRACE_CALL( iStatus = PIE_register_user_action("CADEXExportPreAction", (PIE_user_action_func_t)CE4_PLMXMLExportPreAction) );
		CE4_TRACE_CALL( iStatus = PIE_register_user_action("CADEXExportPostAction", (PIE_user_action_func_t)CE4_PLMXMLExportPostAction) );
        CE4_TRACE_CALL( iStatus = PIE_register_user_filter("CADEXExportFilter", (PIE_user_filter_func_t)CE4_PLMXMLExportFilter));
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	CE4_TRACE_LEAVE() ;

	return iStatus ;

	
 return 0;

}

int CE4_PLMXMLExportPreAction(tag_t session)
{
	int iStatus					= 0 ,
		targetCount				= 0 ;

	map<string, string> mapSessionOpts ;

	const char* __function__ = "CE4_PLMXMLExportPreAction" ;
	CE4_TRACE_ENTER() ;

	try
	{	
		CE4_TRACE_CALL( iStatus = CE4_getSessionOptions(session, mapSessionOpts));
		CE4_TRACE_CALL( iStatus = CE4_traverseStructure(mapSessionOpts, false, session));
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{

				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}

			plmxmlActionFailure = true;
	}

	CE4_TRACE_LEAVE() ;
	return iStatus ;
}

int CE4_PLMXMLExportPostAction(tag_t session)
{
	int iStatus					= 0 ,
		targetCount				= 0 ;

	map<string, string> mapSessionOpts ;

	const char* __function__ = "CE4_PLMXMLExportPostAction" ;
	CE4_TRACE_ENTER() ;

	try
	{	
		CE4_TRACE_CALL( iStatus = CE4_getSessionOptions(session, mapSessionOpts));
		CE4_TRACE_CALL( iStatus = CE4_traverseStructure(mapSessionOpts, true, session));
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{

				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}

			plmxmlActionFailure = true;
	}

	CE4_TRACE_LEAVE() ;
	return iStatus ;
}

PIE_rule_type_t CE4_PLMXMLExportFilter(void* userPath)
{
	int iStatus					= 0 ,
		primCount				= 0 ;

	tag_t dSet					= NULLTAG ,
		  *primObjs				= NULL	  ,
		  session				= NULLTAG ;

	void *prevPath ;
	PIE_rule_type_t decision	= PIE_skip ;

	map<string, string> sessionOpts	;
	
	const char* __function__ = "CE4_PLMXMLExportFilter" ;
	CE4_TRACE_ENTER() ;

	try
	{
		CE4_TRACE_CALL(iStatus = PIE_get_obj_from_callpath(userPath, &dSet));
		CE4_TRACE_CALL(iStatus = PIE_get_session(userPath, &session));
		CE4_TRACE_CALL(iStatus = CE4_getSessionOptions(session, sessionOpts));
		
		string objType			= "" ,
			   secClass			= "" ;
		
		logical isCad			= false ,
				isRendering		= false ;

		tag_t relType			= NULLTAG ;

		CE4_TRACE_CALL(iStatus = getClassForObj(dSet, secClass));

		if(secClass.compare("Dataset")==0)
		{
			CE4_TRACE_CALL(iStatus = getObjectAttribute(dSet,"object_type", objType));
			CE4_TRACE_CALL(iStatus = checkIfAvailable("CADEXTraversalCADTypes", objType, &isCad));
			CE4_TRACE_CALL(iStatus = checkIfAvailable("CADEXTraversalRenderTypes", objType, &isRendering));
			


			if(!isCad && !isRendering )
			{
				CE4_TRACE_CALL( iStatus = GRM_find_relation_type(CE4_ABS_OCC_DSET_RELATION_EXPORT, &relType));
				CE4_TRACE_CALL( iStatus = GRM_list_primary_objects_only(dSet, relType, &primCount, &primObjs)); 
				logical isExportable = false ;

				for(int i = 0 ; i < primCount ; i++)
				{
					string objType1 = "" ,
						   objName  = "" ;
					
					CE4_TRACE_CALL(iStatus = getObjectAttribute(primObjs[i],"object_type", objType1));//AbsOccGRMAnchor
					
					if(objType1.compare("AbsOccGRMAnchor")==0)
					{
						CE4_TRACE_CALL(iStatus = getObjectAttribute(primObjs[i],"absocc_rootline_str", objName));
						if(objName.compare(sessionOpts.find("CE4BomViewRevisionName")->second)==0)
						{
							decision = PIE_travers_and_process;
							break;
						}
					}
				}
			}
			else
				decision = PIE_travers_and_process ;
		}
		else
			decision = PIE_travers_and_process ;
		
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	CE4_free_memory(primObjs);

	CE4_TRACE_LEAVE() ;

	return decision ;
}
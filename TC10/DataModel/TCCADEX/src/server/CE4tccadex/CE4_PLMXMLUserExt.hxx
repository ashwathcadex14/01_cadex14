/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CE4_PLMXMLUserExt.hxx          
#      Module          :           libce4tccadex_v1.src.server.CE4tccadex          
#      Description     :           PLMXML User Exits          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    


/* 
 * @file 
 *
 *   This file contains the declaration for the Extension CE4_PLMXMLUserExt
 *
 */
 
#ifndef CE4_PLMXMLUSEREXT_HXX
#define CE4_PLMXMLUSEREXT_HXX
#include <tccore/method.h>
#include <common/CE4_Common.h>
#include <CE4tccadex/libce4tccadex_exports.h>
#ifdef __cplusplus
         extern "C"{
#endif
                 
extern CE4TCCADEX_API int CE4_PLMXMLUserExt(METHOD_message_t* msg, va_list args);
extern CE4TCCADEX_API int CE4_PLMXMLExportPreAction(tag_t session);
extern CE4TCCADEX_API int CE4_PLMXMLExportPostAction(tag_t session);
extern CE4TCCADEX_API PIE_rule_type_t CE4_PLMXMLExportFilter(void* userPath);

#ifdef __cplusplus
                   }
#endif
                
#include <CE4tccadex/libce4tccadex_undef.h>
                
#endif  // CE4_PLMXMLUSEREXT_HXX

/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CE4_setCfgParentAttr.hxx          
#      Module          :           libce4tccadex_v1.src.server.CE4tccadex          
#      Description     :           To set the CfgAttachmentLine attribute at run time          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    








/* 
 * @file 
 *
 *   This file contains the declaration for the Extension CE4_setCfgParentAttr
 *
 */
 
#ifndef CE4_SETCFGPARENTATTR_HXX
#define CE4_SETCFGPARENTATTR_HXX
#include <tccore/method.h>
#include <ug_va_copy.h>
#include <common/CE4_Common.h>
#include <CE4tccadex/libce4tccadex_exports.h>
#ifdef __cplusplus
         extern "C"{
#endif
                 
extern CE4TCCADEX_API int CE4_setCfgParentAttr(METHOD_message_t* msg, va_list args);
                 
#ifdef __cplusplus
                   }
#endif
                
#include <CE4tccadex/libce4tccadex_undef.h>
                
#endif  // CE4_SETCFGPARENTATTR_HXX

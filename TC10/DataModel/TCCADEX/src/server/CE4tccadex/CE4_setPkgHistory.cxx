/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CE4_setPkgHistory.cxx          
#      Module          :           libce4tccadex_v1.src.server.CE4tccadex          
#      Description     :           To set package history while setting ce4_status          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    








/* 
 * @file 
 *
 *   This file contains the implementation for the Extension CE4_setPkgHistory
 *
 */
#include <CE4tccadex/CE4_setPkgHistory.hxx>

int CE4_setPkgHistory( METHOD_message_t *msg, va_list args )
{
 
 return 0;

}
/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CE4_Common.h          
#      Module          :           libce4tccadex_v1.src.server.common          
#      Description     :           Main Header file for all source in CADEX          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    


#ifndef CE4_COMMON_HXX
#define CE4_COMMON_HXX


#include <sys/timeb.h>
#include <time.h>
#include <tccore/method.h>
#include <iostream>
#include <tccore/aom_prop.h>
#include <tccore/aom.h>
#include <tccore/grm.h>
#include <tccore/item.h>
#include <pie/pie.h>
#include <property/prop.h>
#include <tc/preferences.h>
#include <server_exits/user_server_exits.h>
#include <tccore/custom.h>
#include <tcinit/tcinit.h>
#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <strstream>
#include <sstream>
#include <fstream>
#include <common/CE4_Common_Constants.h>
#include <bom/bom.h>
#include <ae/ae.h>
#include <sa/tcfile.h>
#include <res/res_itk.h>
#include <tchar.h>
#include <algorithm>
#include <itk/bmf.h>
#include <cfm/cfm.h>
#include <iterator>
#include <fclasses/tc_string.h>
#include <fclasses/tc_date.h>
#include <epm/epm.h>
#include <CE4tccadex/libce4tccadex_exports.h>


using namespace std;

template<typename T>
extern CE4TCCADEX_API int convertVectorToArray(vector<T> vectorToConvert, T** resultArray, int* size);
extern CE4TCCADEX_API int getObjectAttribute ( tag_t tObj , const string& sAttr , string& sValue ) ;
extern CE4TCCADEX_API int getSecondaryObjects ( const string& sRelationName , tag_t tPrimaryObj , int* iNoOfSecObjs , tag_t** tSecObjs );
int getFilesinDir(const string &folderName, vector<string> *fileNames);

#ifdef __cplusplus
         extern "C"{
#endif

int checkIfAvailable(char *prefName, const string& value, logical *retVal);
/**Start Of Custom Handlers Section **/
extern CE4TCCADEX_API int libce4tccadex_register_callbacks();
int libce4cadex_register_custom_handlers( int *decision , va_list args);
EPM_decision_t CE4_custom_error(EPM_rule_message_t msg);
extern EPM_decision_t EPM_check_action_performer_role(EPM_rule_message_t msg);
/**End Of Custom Handlers Section **/
extern CE4TCCADEX_API char* ce4_get_local_time(char*) ;
extern CE4TCCADEX_API void ce4_change_indent (	const char	cChange ) ;
static int							ce4TraceIndention = 0			;
extern logical plmxmlActionFailure	;
extern logical isCadexCheckIn ;

extern CE4TCCADEX_API void ce4_trace(int loglevel,char* file,int line,char* format,...);
extern CE4TCCADEX_API void printIndent() ; 
extern CE4TCCADEX_API int ce4_get_error_text( int iErrorCode, char** msg ) ;
void set_output_mode( int isPrintNeeded ) ;
typedef int(* USER_function_t)(void*);
int CE4_Create_Item ( void *returnValue ) ;
int CE4_Update_Revision_Attrs ( tag_t tRevision , char** sRevMasterAttr , char** sRevMasterAttrValue , int iRevAttrLen );
extern CE4TCCADEX_API int CE4CreateExpPkgStructure(METHOD_message_t* msg, va_list args);
extern CE4TCCADEX_API int CE4_validateCheckIn(METHOD_message_t* msg, va_list args);
extern CE4TCCADEX_API int CE4_setExpCheckOut(tag_t, logical, logical, tag_t, tag_t);
extern CE4TCCADEX_API int CE4_createExportPackage(tag_t revTag, tag_t tferMode);
extern CE4TCCADEX_API int CE4_findRelationToRemove(tag_t bomLine, tag_t dataset, char* relationType, tag_t bomWindow);
extern CE4TCCADEX_API void ce4_trace(int loglevel,char* format,int line,char* file,...);
extern CE4TCCADEX_API int CE4_importPackage(tag_t revTag, tag_t tferMode);
int CE4_traverseStructure(map<string, string> sessionOptions, bool isPost, tag_t session);
logical convertStringtoLogical(const string& value);
int getClassForObj(tag_t objTag, string& nameOfClass);
int findToplineOfPkg(tag_t rev, int *n_tags, tag_t **tags);
int createDataset ( tag_t tItemRevision , const string& sDatasetName , const string& sFilePath ,  const string& sFileRef , const string& datasetFormat, const string& sDataset , const string& sRelation );
int CE4_getSessionOptions(tag_t session,map<string, string>& sessionOpts);

#ifdef __cplusplus
                   }
#endif



#define CE4_free_memory(p) {\
    if ( p != NULL ) {\
        MEM_free(p);\
        p = NULL;\
    }\
}

#define CE4_TRACE_ENTER() \
do { \
		if ( true ) \
		{ \
			ce4_trace ( 1, __FILE__, __LINE__ ,"" ) ; \
			printIndent() ; \
			fprintf(stdout , "--> %s() %d\n" , __function__ , __LINE__ ) ; \
			ce4_change_indent ( '+' ) ; \
		} \
} while(0)

#define CE4_TRACE_LEAVE() \
do { \
		if ( true ) \
		{ \
			ce4_trace ( 1, __FILE__, __LINE__ ,"" ) ; \
			ce4_change_indent ( '-' ) ; \
			printIndent() ; \
			fprintf(stdout , "<-- %s() %d\n" , __function__ , __LINE__ ) ; \
		} \
} while(0)

#define CE4_TRACE_CALL(function) \
do { \
		char * pc = NULL ; \
		pc=(char*)strchr(#function,'=');if(pc){do pc++;while(*pc==' ');}else pc=#function;\
		int stat = (function) ; \
		char *err_string = NULL ;		\
		if( stat != ITK_ok )	\
		{							\
			ce4_get_error_text (stat, &err_string);	\
			ce4_trace ( 2, __FILE__, __LINE__ ,"" ) ; \
			printIndent() ; \
			fprintf( stdout , "%s:%d:%s\n", pc , stat , err_string );	\
			MEM_free (err_string);	\
			throw iStatus ; \
		}	\
		else { \
			ce4_trace ( 1, __FILE__, __LINE__ ,"" ) ; \
			printIndent() ; \
			fprintf( stdout , "%s:Done\n" , pc );	\
		} \
} while(0)

template<typename T>
int convertVectorToArray(vector<T> vectorToConvert, T** resultArray, int* size)
{
	int iStatus = ITK_ok;

	T* check = NULL;

	const char * __function__ = "convertVectorToArray" ;
	CE4_TRACE_ENTER();

	try
	{
		int i	 = 0 ,
			vectorSize = 0 ;

		vectorSize = vectorToConvert.size();
		*resultArray = (T*)MEM_alloc(sizeof(T)*vectorSize);
		check = (T*)MEM_alloc(sizeof(T)*vectorSize);
		for(std::vector<T>::iterator it = vectorToConvert.begin();it != vectorToConvert.end() ;it++)
		{
			(*resultArray)[i] = (T)MEM_alloc(sizeof(T));
			(*resultArray)[i] = (T)*it;
			i++;
		}

		//memcpy(*resultArray, check, vectorSize);
		//*resultArray = check;
		*size = i;
	}
	catch(exception e)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s:%s Unhandled Exception.\n", __function__,e.what() );
				printf("%s:%s Unhandled Exception.\n", __function__,e.what() );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	MEM_free(check);

	CE4_TRACE_LEAVE () ;

	return iStatus;
}

#include <CE4tccadex/libce4tccadex_undef.h>

#endif    //CE4_COMMON_HXX

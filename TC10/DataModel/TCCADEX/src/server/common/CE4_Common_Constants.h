/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CE4_Common_Constants.h          
#      Module          :           libce4tccadex_v1.src.server.common          
#      Description     :           Constants used in Source          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
#ifndef CE4_COMMON_CONSTANTS_H
#define CE4_COMMON_CONSTANTS_H

#define CE4_INFO_LOG_LEVEL 1
#define CE4_ERROR_LOG_LEVEL 2
#define CE4_DEBUG_LOG_LEVEL 3
#define CE4_WARN_LOG_LEVEL 4

#define CE4_UNKNOWN_ERROR 1

#define CE4_ATTR_ITEM_ID "item_id"
#define CE4_ATTR_REV_ID "item_revision_id"
#define CE4_BOM_VIEW_DESC "Bom view for Export Pakcage Item"
#define CE4_BOM_VIEW_REV_DESC "Bom view revision for Export Pakcage Item Revision"
#define CE4_BOM_VIEW_TYPE_NAME "view"
#define CE4_TARGET_ITEM_ID_PREFIX "TARGETS"
#define CE4_REFS_ITEM_ID_PREFIX "REFERENCES"
#define CE4_TR_ITEM_ID_SEPERATOR "_"
#define CE4_TARGET_ITEM_TYPE "CE4_ExPkgTargets"
#define CE4_REFERENCES_ITEM_TYPE "CE4_ExPkgRefs"
#define CE4_ABS_OCC_DSET_RELATION_EXPORT "CE4_exportableObject"
#define CE4_ABS_OCC_DSET_RELATION_CHECKOUT "CE4_checkOutableObject"
#define CE4_ME_CL_PARENT "me_cl_parent"
#define CE4_AL_SOURCE_CLASS "al_source_class"
#define CE4_AL_SOURCE_TYPE "al_source_type"
#define CE4_ME_CL_SOURCE "me_cl_source"
#define CE4_EXPORT_NATIVE "CE4_exportNative"
#define CE4_EXPORT_RENDERING "CE4_exportRendering"
#define CE4_CL_PARENT_EXPORT_NATIVE "ce4_cl_parent_native"
#define CE4_CL_PARENT_EXPORT_RENDERING "ce4_cl_parent_rendering"
#define CE4_CL_NO_CAD_NO_RENDER "ce4_cl_no_cad_no_render"

#define CE4_CAD_CHECK_PREF_NAME "CADEXTraversalCADTypes"
#define CE4_RENDER_CHECK_PREF_NAME "CADEXTraversalRenderTypes"

#define CE4_ALREADY_CHECKED_OUT 7
#define CE4_IGNORE_CHECK_OUT 8
#define CE4_DO_CHECK_OUT 9

#define CE4_CADEX_ERROR_BASE 919000
#define CE4_SET_EXP_CHECK_OUT_ERROR (CE4_CADEX_ERROR_BASE + 2)
#define CE4_INPKG_NOT_FOUND_ERROR (CE4_CADEX_ERROR_BASE + 3)
#define CE4_MANY_FILES_ERROR (CE4_CADEX_ERROR_BASE + 4)

#endif    //CE4_COMMON_CONSTANTS_H

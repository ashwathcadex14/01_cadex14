/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           ce4_common.cpp          
#      Module          :           libce4tccadex_v1.src.server.common          
#      Description     :           Common implementations for CADEX          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
#include <common/CE4_Common.h>
#include <common\dirent.h>

char* ce4_get_local_time(char* sFormat)
{
		struct timeb timebuffer ;
		ftime( &timebuffer ) ;

		time_t tt = timebuffer.time ;
		struct tm *ptm = localtime( &tt ) ;
		date_t curDate ;
		curDate.year = ptm -> tm_year + 1900 ;
		curDate.month = ptm -> tm_mon ;
		curDate.day = ptm -> tm_mday ;
		curDate.hour = ptm -> tm_hour ;
		curDate.minute = ptm -> tm_min ;
		curDate.second = ptm -> tm_sec ;

		int msec = timebuffer.millitm ;

		char *strDate = NULL ;
		DATE_date_to_string( curDate, sFormat , &strDate ) ;

		
		return strDate;
}

void ce4_change_indent (
	const char	cChange )
{
	     if ( cChange == '+' ) ce4TraceIndention = ce4TraceIndention+3 ;
	else if ( cChange == '-' ) ce4TraceIndention = ce4TraceIndention-3 ;

	return ;
}

void printIndent()
{
			char	cIndentFmt[128+1] = ""	; 
			sprintf ( cIndentFmt , "%%%ds" , ce4TraceIndention ) ;
			fprintf ( stdout , cIndentFmt , ""                    ) ; 
}

int ce4_get_error_text( int iErrorCode, char** msg )
{
		int iStatus = ITK_ok;
		int iErrorCount;
		int* iSeverities;
		int* iFails;
		char** cTexts = NULL;

		const char * __function__ = "ce4_get_error_text" ;
		//CE4_TRACE_ENTER();

				try
				{
					//Get the errors on stack
					iStatus = EMH_ask_errors( &iErrorCount, (const int**)&iSeverities, (const int**)&iFails, (const char***)&cTexts );
					if( iErrorCount > 0 )
					{
						for( int i = (iErrorCount -1); i >= 0; i-- )
						{
								//check whether the error code matches the thrown error code
								if( iErrorCode == iFails[i] && cTexts[i] != NULL )
								{
										*msg =  (char*)MEM_alloc( (tc_strlen( cTexts[i] ) + 1 ) * sizeof(char));
										tc_strcpy( (*msg), cTexts[i] );
										break;
								}
						}
					}
				}
				catch(...)
				{

				}

	//CE4_TRACE_LEAVE () ;

	return iStatus;

}

void ce4_trace(int loglevel,char* file,int line,char* format,...)
{
	char	cBuffer[4096+1] = ""; 
	char* strDate = NULL ; 
	va_list		pArgs 	; 
	va_start ( pArgs , format ) ; 
	vsprintf ( cBuffer , format , pArgs ) ; 
	va_end(pArgs); 
			strDate = ce4_get_local_time("%Y-%m-%d %H:%M:%S") ; 
			fprintf( stdout, "%s - %s - %s", 
			(loglevel==CE4_INFO_LOG_LEVEL) ? "[INFO]" : 
			(loglevel==CE4_DEBUG_LOG_LEVEL) ? "[DEBUG]" : 
			(loglevel==CE4_ERROR_LOG_LEVEL) ? "[ERROR]" : 
			(loglevel==CE4_WARN_LOG_LEVEL) ? "[WARN]" : 
			"[INFO]", strDate ? strDate : "NULL DATE" , 
			cBuffer ); 
			CE4_free_memory(strDate); 
}

//void set_output_mode( int isPrintNeeded )
//{
//	string sFilePath = "" ;
//
//	sFilePath.append ( "DDN_GENERATE_XML_" ).append ( ce4_get_local_time ( "_%H%M%S_%d%m%Y" ) ).append(".log") ;
//
//	if ( isPrintNeeded == 0 )
//	{
//			OutputLog = stdout ;
//	}
//	else
//	{
//		fopen_s ( &OutputLog , sFilePath.c_str() , "w" ) ;
//	}
//}

int checkIfAvailable(char *prefName, const string& value, logical *retVal)
{
	int iStatus		 = ITK_ok ,
		iNoValues	 = 0 ;
		
	char **sValues	 = NULL ;

	const char * __function__ = "checkIfAvailable" ;
	CE4_TRACE_ENTER();

	try
	{
		*retVal = false;
		CE4_TRACE_CALL ( iStatus = PREF_ask_char_values  ( prefName , &iNoValues , &sValues )  ) ;

		for ( int j = 0 ; j < iNoValues ; j++ )
		{
			if(tc_strcmp(sValues[j], value.c_str())==0)
			{
				*retVal = true;
				break;
			}
		}
	}
	catch(...)
	{
		if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	CE4_free_memory(sValues);

	CE4_TRACE_LEAVE () ;

	return iStatus;
}

int getObjectAttribute ( tag_t tObj , const string& sAttr , string& sValue ) 
{
	int iStatus   =  0 ,
		num		  =  0 ;

	PROP_value_type_t eValType ; 

	char *sValTypeName = NULL ,
		 *sAttrValue   = NULL ,
		 *class_name   = NULL ;

	char** sAttrValues = NULL ;

	tag_t refAttrValue = NULL ;
	tag_t class_id     = NULL ;

	const char* __function__ = "getObjectAttribute()" ;
	CE4_TRACE_ENTER() ;
	
	try
	{
		CE4_TRACE_CALL ( iStatus = AOM_ask_value_type ( tObj , sAttr.c_str() , &eValType , &sValTypeName ) ) ;
	
		switch ( eValType )
		{
			case PROP_string :
				{
					//CE4_TRACE_CALL ( iStatus = AOM_ask_value_string ( tObj , sAttr.c_str() , &sAttrValue ) ) ;
					CE4_TRACE_CALL ( iStatus = AOM_ask_value_strings ( tObj , sAttr.c_str() , &num, &sAttrValues ) ) ;
					
					if( num == 1 )
					{
						sValue.assign( sAttrValues[0] );
					}
					else
					{
						for( int i = 0 ; i < num ; i++ )
						{
							sValue.append( sAttrValues[i] );
							if( i != (num - 1) )
							{
								sValue.append( "," );
							}
						}
					}
					//sValue.assign ( sAttrValue ) ;
					break;
				}				
			default:
				{
					CE4_TRACE_CALL ( iStatus = AOM_UIF_ask_value ( tObj , sAttr.c_str() , &sAttrValue ) ) ;
					
					sValue.assign ( sAttrValue ) ;

					break;
				}
				
		}


	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	CE4_free_memory ( sValTypeName ) ;
	CE4_free_memory ( sAttrValue ) ;

	CE4_TRACE_LEAVE () ;

	return iStatus ;

}

int getSecondaryObjects ( const string& sRelationName , tag_t tPrimaryObj , int* iNoOfSecObjs , tag_t** tSecObjs )
{
	int iStatus		 = 0 ;

	tag_t tRelationType		= NULLTAG ;

	const char* __function__ = "getSecondaryObjects()" ;
	CE4_TRACE_ENTER() ;

	try
	{
		CE4_TRACE_CALL ( iStatus = GRM_find_relation_type ( sRelationName.c_str() , &tRelationType ) ) ;

		if( tRelationType == NULLTAG )
			return iStatus;
	
		CE4_TRACE_CALL ( iStatus = GRM_list_secondary_objects_only ( tPrimaryObj , tRelationType , iNoOfSecObjs , tSecObjs ) ) ;
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	CE4_TRACE_LEAVE () ;

	return iStatus ;
}

logical convertStringtoLogical(const string& value)
{
	if(value.compare("true")==0)
		return true;
	else
		return false;
}

int getClassForObj(tag_t objTag, string& nameOfClass)
{
	int iStatus		 = 0 ;

	char* className  = NULL ;

	tag_t classId	 = NULLTAG ;

	const char* __function__ = "getClassForObj()" ;
	CE4_TRACE_ENTER() ;

	try
	{
		CE4_TRACE_CALL(iStatus = POM_class_of_instance(objTag, &classId));
		CE4_TRACE_CALL(iStatus = POM_name_of_class(classId, &className));
		nameOfClass.assign(className);
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	CE4_free_memory(className);

	CE4_TRACE_LEAVE () ;

	return iStatus ;
}

int getFilesinDir(const string &folderName, vector<string> *fileNames)
{
	int iStatus		 = 0 ;

	DIR *dir;
	struct dirent *ent;
	
	const char* __function__ = "getFilesinDir()" ;
	CE4_TRACE_ENTER() ;

	try
	{
		if ((dir = opendir ((folderName+ "\\" ).c_str())) != NULL) 
			{
			  /* print all the files and directories within directory */
			  while ((ent = readdir (dir)) != NULL) 
			  {
				  fileNames->push_back(ent->d_name);
			  }
				closedir (dir);
			}
			else 
			{
			  /* could not open directory */
			  perror ("");
			  return EXIT_FAILURE;
			}
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	CE4_TRACE_LEAVE () ;

	return iStatus ;
}
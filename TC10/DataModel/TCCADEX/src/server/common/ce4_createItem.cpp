/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           ce4_createItem.cpp          
#      Module          :           libce4tccadex_v1.src.server.common          
#      Description     :           To create item          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
#include <common/CE4_Common.h>

int CE4_Create_Item( void *returnValue )
{
	int iStatus				= 0 ,
		iRevAttrLen			= 0 ;

	char   *sItemID			= NULL ,
		   *sItemType		= NULL ,
		   *sItemDesc		= NULL ,
		   *sItemName		= NULL ,
		   **sRevFormAttrs	= NULL	 ,
		   **sRevFormAttrValues	= NULL ;
	
	tag_t	tItem  = NULLTAG ,
			tRev   = NULLTAG ;

	const char* __function__ = "CE4_Create_Item" ;
	CE4_TRACE_ENTER() ;

	try
	{	
		CE4_TRACE_CALL ( iStatus = USERARG_get_string_argument ( &sItemType ) ) ;
		CE4_TRACE_CALL ( iStatus = USERARG_get_string_argument ( &sItemID ) ) ;
		CE4_TRACE_CALL ( iStatus = USERARG_get_string_argument ( &sItemName ) ) ;
		CE4_TRACE_CALL ( iStatus = USERARG_get_string_argument ( &sItemDesc ) );
		CE4_TRACE_CALL ( iStatus = USERARG_get_string_array_argument ( &iRevAttrLen , &sRevFormAttrs ) ) ;
		CE4_TRACE_CALL ( iStatus = USERARG_get_string_array_argument ( &iRevAttrLen , &sRevFormAttrValues ) ) ;

		CE4_TRACE_CALL ( iStatus = ITEM_create_item ( sItemID , sItemName ,	sItemType , "000" , &tItem , &tRev ) ) ;
		
		CE4_TRACE_CALL ( iStatus = CE4_Update_Revision_Attrs ( tRev , sRevFormAttrs , sRevFormAttrValues , iRevAttrLen ) ) ;
		
		//CE4_TRACE_CALL ( iStatus = USERSERVICE_return_tag_array ( &tItem , 1 , (USERSERVICE_array_t*)returnValue ) ) ;
	
		*(tag_t *)returnValue = tRev ;
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	CE4_free_memory ( sItemID ) ;
	CE4_free_memory ( sItemType ) ;
	CE4_free_memory ( sItemDesc ) ;
	CE4_free_memory ( sItemName ) ;
	CE4_free_memory ( sRevFormAttrs ) ;
	CE4_free_memory ( sRevFormAttrValues ) ;

	CE4_TRACE_LEAVE() ;

	return iStatus ;

}

int CE4_Update_Revision_Attrs ( tag_t tRevision , char** sRevMasterAttr , char** sRevMasterAttrValue , int iRevAttrLen )
{
		tag_t tRelationType = NULLTAG ;

		int iMasterForms	= 0 ,
			iStatus			= 0 ;
			  
			const char* __function__ = "CE4_Update_Revision_Attrs" ;
			CE4_TRACE_ENTER() ;

		try
		{
			//CE4_TRACE_CALL ( iStatus = GRM_find_relation_type ( CE4_REV_MASTER_RELATION_TYPE , &tRelationType  ) ) ;
		
			//CE4_TRACE_CALL ( iStatus = GRM_list_secondary_objects_only ( tRevision , tRelationType , &iMasterForms , &tMasterForm ) ) ;
			
			for ( int i = 0 ; i < iRevAttrLen ; i++ )
			{
				CE4_TRACE_CALL ( iStatus = AOM_refresh ( tRevision , true ) ) ;

				//printf ( "\nsRevMasterAttr[%d] - %s  -  %s\n" , i, sRevMasterAttr[i],sRevMasterAttrValue[i] ) ;

				CE4_TRACE_CALL ( iStatus = AOM_UIF_set_value ( tRevision , sRevMasterAttr[i] , sRevMasterAttrValue[i] ) ) ;
				
				CE4_TRACE_CALL ( iStatus = AOM_save ( tRevision ) ) ;

				CE4_TRACE_CALL ( iStatus = AOM_refresh ( tRevision , false ) ) ;
			}
			
		}
		catch (...)
		{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
		}

		
		CE4_TRACE_LEAVE() ;

		return iStatus ;
}

/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           BOMTraversalEngine.h          
#      Module          :           libce4tccadex_v1.src.server.custom_classes          
#      Description     :           Interface for traversing a bom structure          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
#include <common/CE4_Common.h>

#pragma once
//Interface
class BOMTraversalEngine
{
	public:
		virtual int processBOMLine(tag_t bomLine) = 0; // There is no default implementation
		BOMTraversalEngine( void ) ;
		BOMTraversalEngine ( tag_t, const string&, logical ) ;
		BOMTraversalEngine ( tag_t, const string&, logical , int) ;
		int traverseStructure();
	private:
		BOMTraversalEngine ( tag_t ) ;
		tag_t topBomLine ;
		tag_t window	 ;
		int stopLevel;
		int traverseStructure(tag_t bomLine);
	protected:
		string pkgId ;
		string pkgRevId ;
};
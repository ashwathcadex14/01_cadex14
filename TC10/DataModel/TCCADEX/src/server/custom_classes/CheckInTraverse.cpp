/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CheckInTraverse.cpp          
#      Module          :           libce4tccadex_v1.src.server.custom_classes          
#      Description     :           Extends CheckOutTraverse to check in objects          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
#include <custom_classes/CheckInTraverse.h>


CheckInTraverse::CheckInTraverse(void)
{
}

CheckInTraverse::CheckInTraverse(tag_t topLine, const string& revRule, logical isAbsOcc) : CheckOutTraverse(topLine, revRule, isAbsOcc)
{
}

int CheckInTraverse::reserveObject(tag_t object)
{
	int iStatus				= 0 ;

	const char* __function__ = "CheckInTraverse::reserveObject" ;
	CE4_TRACE_ENTER() ;

	try
	{
		CE4_TRACE_CALL( iStatus = RES_checkin(object) ); 
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}
		
	CE4_TRACE_LEAVE() ;

	return ITK_ok;
}

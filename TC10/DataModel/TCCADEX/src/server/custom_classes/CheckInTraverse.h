/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CheckInTraverse.h          
#      Module          :           libce4tccadex_v1.src.server.custom_classes          
#      Description     :           Extends CheckOutTraverse to check in objects          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
#pragma once
#include <custom_classes/CheckOutTraverse.h>

class CheckInTraverse : public CheckOutTraverse
{
	public:
		CheckInTraverse(void);
		CheckInTraverse(tag_t, const string&, logical) ;
		virtual int reserveObject(tag_t object);
};


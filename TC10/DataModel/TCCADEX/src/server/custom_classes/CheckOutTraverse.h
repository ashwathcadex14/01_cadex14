/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CheckOutTraverse.h          
#      Module          :           libce4tccadex_v1.src.server.custom_classes          
#      Description     :           Implements BOMTraversalEngine to check out objects          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
#pragma once
#include <custom_classes/BOMTraversalEngine.h>

class CheckOutTraverse :
	public BOMTraversalEngine
{
	public:
		CheckOutTraverse(void) ;
		CheckOutTraverse(tag_t, const string&, logical) ;
		CheckOutTraverse(tag_t, const string&, logical, int) ;
		int processBOMLine(tag_t bomLine);
		virtual int reserveObject(tag_t object);
	private:
		int checkOutLine(tag_t line);
		int checkOutSecondary(tag_t object, logical exportNative, logical exportRendering, logical checkOut);
		int checkOutAttachments(tag_t object);
		int isCheckOutNeeded(tag_t obj, string pkgString, int *isCheckOutN);
		vector<string> itemDefaultRelations;
		vector<string> revDefaultRelations;
};


/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           ExportInputProvider.cpp          
#      Module          :           libce4tccadex_v1.src.server.custom_classes          
#      Description     :           Implements BOMTraversalEngine to gather 1st level NX parents objects         
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
#include <custom_classes/ExportInputProvider.h>


ExportInputProvider::ExportInputProvider(void) 
{
}

ExportInputProvider::ExportInputProvider(tag_t topLine, const string& revRule, logical isAbsOcc) : BOMTraversalEngine(topLine, revRule, isAbsOcc)
{
}

ExportInputProvider::ExportInputProvider(tag_t topLine, const string& revRule, logical isAbsOcc, int stopLvl) : BOMTraversalEngine(topLine, revRule, isAbsOcc, stopLvl)
{
}

int ExportInputProvider::processBOMLine(tag_t line)
{
	int iStatus				= 0 ,
		attId				= 0 ,
		level				= 0 ,
		attrTag				= 0 ;

	tag_t revTag			= NULLTAG ;

	const char* __function__ = "ExportInputProvider::processBOMLine" ;
	CE4_TRACE_ENTER() ;

	try
	{
		CE4_TRACE_CALL ( iStatus = BOM_line_look_up_attribute( "bl_level_starting_0", &attrTag ));

		CE4_TRACE_CALL ( iStatus = BOM_line_ask_attribute_int( line, attrTag, &level));
		
		if(level == 1)
		{
			logical isNX = false;
			CE4_TRACE_CALL(iStatus = BOM_line_look_up_attribute  ( "bl_revision", &attId )) ;  
			CE4_TRACE_CALL(iStatus = BOM_line_ask_attribute_tag (line, attId, &revTag));
			CE4_TRACE_CALL(iStatus = checkCadType(revTag, &isNX));

			if(isNX)
			{
				string itemRevId = "" ,
				itemId	 = "" ;
				tag_t itemTag	 = NULLTAG;
				CE4_TRACE_CALL(iStatus = getObjectAttribute(revTag, CE4_ATTR_REV_ID, itemRevId));
				CE4_TRACE_CALL(iStatus = ITEM_ask_item_of_rev(revTag, &itemTag ));
				CE4_TRACE_CALL(iStatus = getObjectAttribute(itemTag, CE4_ATTR_ITEM_ID, itemId));
					
				if(cloneInput.length() > 0)
					this->cloneInput.append(" ");

				this->cloneInput.append("-assembly=").append("@DB/" + itemId + "/" + itemRevId);
			}
		}
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}
		
	CE4_TRACE_LEAVE() ;

	return iStatus;
}

int ExportInputProvider::checkCadType(tag_t revTag, logical* isNX)
{
	int iStatus				= 0 ;

	tag_t classId			= NULLTAG ;

	string objClass			= "" ;

	vector<string> defaultRelations ;

	const char* __function__ = "ExportInputProvider::checkCadType" ;
	CE4_TRACE_ENTER() ;

	try
	{
		int objCount = 0 ;

		tag_t *secObjs = NULL ;

		CE4_TRACE_CALL(iStatus = AOM_ask_value_tags( revTag,"IMAN_specification", &objCount, &secObjs));
				
		if(objCount>0)
		{
			for(int j = 0 ; j < objCount ; j++ )
			{
				string objType			= "" ;
				logical isCad			= false ;
				
				CE4_TRACE_CALL(iStatus = getObjectAttribute(secObjs[j],"object_type", objType));
				CE4_TRACE_CALL(iStatus = checkIfAvailable(CE4_CAD_CHECK_PREF_NAME, objType, &isCad));

				if(isCad && objType.compare("UGMASTER") == 0)
				{
					*isNX = true;
					break;
				}
			}
		}
		CE4_free_memory(secObjs);
	
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	CE4_TRACE_LEAVE() ;

	return iStatus;
}


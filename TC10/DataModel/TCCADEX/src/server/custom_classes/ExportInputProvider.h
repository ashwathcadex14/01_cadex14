/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           ExportInputProvider.h          
#      Module          :           libce4tccadex_v1.src.server.custom_classes          
#      Description     :           Implements BOMTraversalEngine to gather 1st level NX parents objects          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
#pragma once
#include <custom_classes/BOMTraversalEngine.h>

class ExportInputProvider :
	public BOMTraversalEngine
{
	public:
		ExportInputProvider(void) ;
		ExportInputProvider(tag_t, const string&, logical) ;
		ExportInputProvider(tag_t, const string&, logical, int) ;
		int processBOMLine(tag_t bomLine);
		string cloneInput;
	private:
		int checkCadType(tag_t revTag, logical *isNX);
};
package cadexstructurecompare.delta;

import java.util.HashSet;
import java.util.List;

import cadexstructurecompare.views.structurecompare.CompareNode;

public abstract class AbstractDeltaGenerator<T> implements ICadexDeltaGenerator {

	private T thisDeltaDoc ;
	CompareNode thisCmpNode = null ;
	HashSet<String> visitedNodes = new HashSet<String>();
	
	public AbstractDeltaGenerator(T deltaDoc, CompareNode cmpNode) {
		thisDeltaDoc = deltaDoc;
		thisCmpNode = cmpNode;
	}
	
	public CompareNode getThisCmpNode() {
		return thisCmpNode;
	}

	public T getDocument()
	{
		return thisDeltaDoc;
	}

	@Override
	public void traverseStructure() throws Exception
	{
		try
		{
			traverseChildren(thisCmpNode);	
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	
	protected boolean isVisited(String id) {
		return visitedNodes.contains( id );
	}
	
	@Override
	public void traverseChildren(CompareNode node) throws Exception
	{
		try
		{
			writeDelta(node);
			
			List<CompareNode> childs = node.getChildNodes();
			
			if( childs !=null && childs.size() > 0)
			{
				for(CompareNode cmpNode : childs)
				{
					traverseChildren(cmpNode);
				}
			}
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	
	public enum DELTA_INFO
	{
		NO_CHANGE(0),
		NEW_PRODUCT (1),
		NEW_PRODUCT_REVISION (2),
		NEW_DATASET (3),
		DATASET_REMOVED ( 4),
		NEW_OCCURRENCE ( 5),
		OCCURRENCE_REMOVED ( 6),
		OCCURRENCE_MODIFIED ( 7),
		PRODUCT_MODIFIED ( 8),
		PRODUCT_REVISION_MODIFIED ( 9),
		DATASET_MODIFIED ( 10),
		NEW_FILE ( 11),
		FILE_MODIFIED ( 12),
		FILE_REMOVED(13),
		NEW(14),
		MODIFIED(15),
		REMOVED(16);
		
		private final int value ;
		
		DELTA_INFO(int value){
			this.value = value;
		}
		
		public int value(){return value;}
	}
}

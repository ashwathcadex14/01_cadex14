/**
 * 
 */
package cadexstructurecompare.delta;

import cadexstructurecompare.views.structurecompare.CompareNode;

/**
 * @author IZCHND004
 *
 */
public interface ICadexDeltaGenerator {
	
	public void traverseStructure() throws Exception;
	
	public void traverseChildren(CompareNode node) throws Exception;
	
	public void writeDelta(CompareNode node) throws Exception;
	
	
}

package cadexstructurecompare.delta;

import java.util.HashMap;

import cadexstructurecompare.model.interfaces.ICadexGenericObject;

public class IdObjectMap {

	
	HashMap<String, ICadexGenericObject> idMap = new HashMap<String, ICadexGenericObject>();
	
	public IdObjectMap() {
		// TODO Auto-generated constructor stub
	}

	public void addObj(String id, ICadexGenericObject obj)
	{
		idMap.put(id, obj);
	}
	
	public boolean containsId(String id)
	{
		return this.idMap.containsKey(id);
	}
	
	public ICadexGenericObject getObj(String id)
	{
		return this.idMap.containsKey(id) ? idMap.get( id ) : null ;
	}
}

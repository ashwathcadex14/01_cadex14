package cadexstructurecompare.delta.infoexplmxml;

import com.infoex.plmxml.delta.types.PLMXMLDelta;
import com.infoex.plmxml.generic.Document;
import com.infoex.plmxml.generic.PLMXMLHelper;

import cadexstructurecompare.views.structurecompare.CompareNode;

public class InfoExGenerateDelta {

	public InfoExGenerateDelta(String newDeltaFile, CompareNode node) throws Exception {

		Document<PLMXMLDelta> deltaDoc = PLMXMLHelper.createDeltaDocument();
		deltaDoc.getRoot().setAuthor( "CADEX Delta Generator BETA v0");
		InfoExPlmxmlDeltaGenerator deltaGen = new InfoExPlmxmlDeltaGenerator( (PLMXMLDelta) deltaDoc.getRoot(), node);
		deltaGen.traverseStructure();
		deltaDoc.save( newDeltaFile );
//		plmxmlDelta.save( newDeltaFile );
		
	}

}

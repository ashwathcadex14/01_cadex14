package cadexstructurecompare.delta.infoexplmxml;

import java.util.HashSet;
import java.util.List;

import com.infoex.plmxml.delta.types.DeltaAdd;
import com.infoex.plmxml.delta.types.DeltaDelete;
import com.infoex.plmxml.delta.types.DeltaModify;
import com.infoex.plmxml.delta.types.ExternalReference;
import com.infoex.plmxml.delta.types.PLMXMLDelta;
import com.infoex.plmxml.delta.types.DeltaModifyOp;
import com.infoex.plmxml.types.ApplicationRef;
import com.infoex.plmxml.types.AssociatedDataSet;
import com.infoex.plmxml.types.DataSet;
import com.infoex.plmxml.types.ExternalFile;
import com.infoex.plmxml.types.Occurrence;
import com.infoex.plmxml.types.Product;
import com.infoex.plmxml.types.ProductRevision;
import cadexstructurecompare.delta.AbstractDeltaGenerator;
import cadexstructurecompare.model.interfaces.ICadexComparable;
import cadexstructurecompare.model.plmxml.infoex.IPlmxmlGenericObject;
import cadexstructurecompare.model.plmxml.infoex.PlmxmlDataset;
import cadexstructurecompare.model.plmxml.infoex.PlmxmlFile;
import cadexstructurecompare.model.plmxml.infoex.PlmxmlItem;
import cadexstructurecompare.model.plmxml.infoex.PlmxmlOccurence;
import cadexstructurecompare.model.plmxml.infoex.PlmxmlRevision;
import cadexstructurecompare.views.structurecompare.CompareNode;

public class InfoExPlmxmlDeltaGenerator extends AbstractDeltaGenerator<PLMXMLDelta> {

	DeltaAdd addDelta = null;
	DeltaModify modDelta = null;
	
	public InfoExPlmxmlDeltaGenerator(PLMXMLDelta deltaDoc, CompareNode node) {
		super(deltaDoc, node);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void writeDelta(CompareNode node) throws Exception
	{
		HashSet<CompareNode> addNodes = new HashSet<CompareNode>();
		HashSet<CompareNode> modNodes = new HashSet<CompareNode>();
		HashSet<CompareNode> delNodes = new HashSet<CompareNode>();
		node.shiftHierarchy();
		addDelta = new DeltaAdd();
		modDelta = new DeltaModify();
	    
		writeDeltaForLine(node, addNodes, modNodes, delNodes);
		if(addDelta.getElements() != null && addDelta.getElements().size() > 0)
			getDocument().addAdd(addDelta);
		if(modDelta.getElements() != null && modDelta.getElements().size() > 0)
			getDocument().addModify(modDelta);
	}

	private void writeDeltaForLine(CompareNode node, HashSet<CompareNode> addNodes, HashSet<CompareNode> modNodes, HashSet<CompareNode> delNodes) throws Exception
	{	
		writeDeltaForOccurrence(node);
		
		if(node.getChildDetailNodes().size() == 1)
		{
			doDeltaForChilds(node.getChildDetailNodes().get(0));
		}
	}
	
	private void doDeltaForChilds(CompareNode node) throws Exception
	{
		ICadexComparable cNode = node.getNode();
		
		if(cNode.getType().equals( "Item" ))
		{
			writeDeltaForItem(node);
		}
		else if(cNode.getType().equals( "ItemRevision" ))
		{
			writeDeltaForRevision(node);
		}
		else if(cNode.getType().equals( "Dataset" ))
		{
			writeDeltaForDataset(node);
		}
		else if(cNode.getType().equals( "ImanFile" ))
		{
			writeDeltaForFile(node);
		}
		
		for(CompareNode detailNode : node.getChildNodes())
		{
			doDeltaForChilds(detailNode);
		}
	}

	
	private void writeDeltaForOccurrence(CompareNode node) throws Exception
	{
		Occurrence occ = ((PlmxmlOccurence)node.getNode().getObject()).getObj();
		
		Occurrence newOcc = occ;
		//newOcc.setOwner( occ );
		
		System.out.println("Preparing delta for occ " + occ.getId());
		
		if(node.getDelta() == DELTA_INFO.NEW)
		{
			if(node.getParentCompNode() == null)
			{
				addDelta.addElement( newOcc );
			}
			else
			{
				if(node.getParentCompNode().getDelta() == DELTA_INFO.NEW)
				{
					addDelta.addElement( newOcc );
				}
				else
				{
					ICadexComparable rightParent = node.getParentCompNode().getNode();
					ExternalReference newRef = new ExternalReference();
					newRef.setType( rightParent.getType());					
					ApplicationRef newAppRef = new ApplicationRef();
					newAppRef.setApplication( ((IPlmxmlGenericObject)rightParent).getAppRefName() );
					//newAppRef.setVersion( ((IPlmxmlGenericObject)rightParent).getAppRefVersion() );
					newAppRef.setLabel( ((IPlmxmlGenericObject)rightParent).getAppRefLabel() );
					newRef.addApplicationRef( newAppRef );
					getDocument().addNewElement(newRef);
					DeltaModify modNode = new DeltaModify();
					modNode.setAttributeName( "occurrenceRefs" );
					modNode.setOp( DeltaModifyOp.ADD );
					modNode.setTargetRef( "#" + newRef.getId() );
					modNode.getValueRefs().add( "#" + occ.getId() );
					getDocument().addModify(modNode);
					addDelta.addElement( newOcc );
				}
			}
		}
		else if( node.getDelta() == DELTA_INFO.MODIFIED )
		{
			modDelta.addElement( newOcc );
		}
		else if( node.getDelta() == DELTA_INFO.REMOVED )
		{
			ICadexComparable leftNode = node.getLeftNode();
			ExternalReference newRef = new ExternalReference();
			newRef.setType( leftNode.getType());					
			ApplicationRef newAppRef = new ApplicationRef();
			newAppRef.setApplication( ((IPlmxmlGenericObject)leftNode).getAppRefName() );
			//newAppRef.setVersion( ((IPlmxmlGenericObject)rightParent).getAppRefVersion() );
			newAppRef.setLabel( ((IPlmxmlGenericObject)leftNode).getAppRefLabel() );
			newRef.addApplicationRef( newAppRef );
			getDocument().addNewElement(newRef);
			DeltaDelete delNode = new DeltaDelete();
			delNode.addTargetURI( "#" + newRef.getId() );
			getDocument().addDel(delNode);
		}
	}
	
	private void writeDeltaForRevision(CompareNode node) throws Exception
	{
		ProductRevision occ = ((PlmxmlRevision)node.getNode().getObject()).getObj();
		ProductRevision newOcc = (ProductRevision) occ;
		
		System.out.println("Preparing delta for ItemRevision " + node.getNodeUid());
	
		if(node.getDelta() == DELTA_INFO.NEW)
		{
			if(node.getParentCompNode() == null)
			{
				addDelta.addElement( newOcc );
			}
			else
			{
				if(node.getParentCompNode().getDelta() == DELTA_INFO.NEW)
				{
					addDelta.addElement( newOcc );
				}
				else
				{
					ICadexComparable rightParent = node.getParentCompNode().getNode();
					ExternalReference newRef = new ExternalReference();
					newRef.setType( rightParent.getType());					
					ApplicationRef newAppRef = new ApplicationRef();
					newAppRef.setApplication( ((IPlmxmlGenericObject)rightParent).getAppRefName() );
					newAppRef.setVersion( ((IPlmxmlGenericObject)rightParent).getAppRefVersion() );
					newAppRef.setLabel( ((IPlmxmlGenericObject)rightParent).getAppRefLabel() );
					newRef.addApplicationRef( newAppRef );
					getDocument().addNewElement(newRef);
//					DeltaModify modNode = new DeltaModify();
//					modNode.setAttributeName( "occurrenceRefs" );
//					modNode.setOp( eDeltaModifyOpType.ADD );
//					modNode.setTargetURI( "#" + newRef.getId() );
//					modNode.addValueURI( "#" + occ.getId() );
//					getDocument().addElement(modNode);
					addDelta.addElement( newOcc );
				}
			}
		}
		else if( node.getDelta() == DELTA_INFO.MODIFIED )
		{
			modDelta.addElement( newOcc );
		}
		else if( node.getDelta() == DELTA_INFO.REMOVED )
		{
			ICadexComparable leftNode = node.getLeftNode();
			ExternalReference newRef = new ExternalReference();
			newRef.setType( leftNode.getType());					
			ApplicationRef newAppRef = new ApplicationRef();
			newAppRef.setApplication( ((IPlmxmlGenericObject)leftNode).getAppRefName() );
			newAppRef.setVersion( ((IPlmxmlGenericObject)leftNode).getAppRefVersion() );
			newAppRef.setLabel( ((IPlmxmlGenericObject)leftNode).getAppRefLabel() );
			newRef.addApplicationRef( newAppRef );
			getDocument().addNewElement(newRef);
			DeltaDelete delNode = new DeltaDelete();
			delNode.addTargetURI( "#" + newRef.getId() );
			getDocument().addDel(delNode);
		}
	}
	
	private void writeDeltaForItem(CompareNode node) throws Exception
	{
		
		Product occ = ((PlmxmlItem)node.getNode().getObject()).getObj();
		Product newOcc = (Product) occ;
		
		System.out.println("Preparing delta for Item " + node.getNodeUid());
		
		if(node.getDelta() == DELTA_INFO.NEW)
		{
			addDelta.addElement( newOcc );
		}
		else if( node.getDelta() == DELTA_INFO.MODIFIED )
		{
			modDelta.addElement( newOcc );
		}
		else if( node.getDelta() == DELTA_INFO.REMOVED )
		{
			ICadexComparable leftNode = node.getNode();
			ExternalReference newRef = new ExternalReference();
			newRef.setType( leftNode.getType());					
			ApplicationRef newAppRef = new ApplicationRef();
			newAppRef.setApplication( ((IPlmxmlGenericObject)leftNode).getAppRefName() );
			newAppRef.setVersion( ((IPlmxmlGenericObject)leftNode).getAppRefVersion() );
			newAppRef.setLabel( ((IPlmxmlGenericObject)leftNode).getAppRefLabel() );
			newRef.addApplicationRef( newAppRef );
			getDocument().addNewElement(newRef);
			DeltaDelete delNode = new DeltaDelete();
			delNode.addTargetURI( "#" + newRef.getId() );
			getDocument().addDel(delNode);
		}
		
	}
	
	private void writeDeltaForDataset(CompareNode node) throws Exception
	{
		DataSet occ = ((PlmxmlDataset)node.getNode().getObject()).getObj();
		DataSet newOcc = (DataSet) occ;
		AssociatedDataSet relNode = ((PlmxmlDataset)node.getNode()).getRelNode();
		AssociatedDataSet newRelNode = (AssociatedDataSet) relNode;
		
		System.out.println("Preparing delta for Dataset " + node.getNodeUid());
		
		if(node.getDelta() == DELTA_INFO.NEW)
		{
			if(node.getParentCompNode() == null)
			{
				addDelta.addElement( newOcc );
			}
			else
			{
				if(node.getParentCompNode().getDelta() == DELTA_INFO.NEW)
				{
					addDelta.addElement( newOcc );
				}
				else
				{
					ICadexComparable rightParent = node.getParentCompNode().getNode();
					ExternalReference newRef = new ExternalReference();
					newRef.setType( rightParent.getType());					
					ApplicationRef newAppRef = new ApplicationRef();
					newAppRef.setApplication( ((IPlmxmlGenericObject)rightParent).getAppRefName() );
					newAppRef.setVersion( ((IPlmxmlGenericObject)rightParent).getAppRefVersion() );
					newAppRef.setLabel( ((IPlmxmlGenericObject)rightParent).getAppRefLabel() );
					newRef.addApplicationRef( newAppRef );
					getDocument().addNewElement(newRef);
					DeltaModify modNode = new DeltaModify();
					modNode.setAttributeName( "associatedDataSetRefs" );
					modNode.setOp( DeltaModifyOp.ADD );
					modNode.setTargetRef( "#" + newRef.getId() );
					modNode.addValueURI( "#" + relNode.getId() );
					getDocument().addModify(modNode);
					addDelta.addElement( newOcc );
					addDelta.addElement( newRelNode );
				}
			}
		}
		else if( node.getDelta() == DELTA_INFO.MODIFIED )
		{
			modDelta.addElement( newOcc );
		}
		else if( node.getDelta() == DELTA_INFO.REMOVED )
		{
			ExternalReference newRef = new ExternalReference();
			newRef.setType( "AssociatedDataSet" );					
			
			List<ApplicationRef> appRefs =  relNode.getApplicationRef();
			
			if(appRefs.size() == 1)
			{
				newRef.addApplicationRef( appRefs.get(0) );
			}
			
			getDocument().addNewElement(newRef);
			DeltaDelete delNode = new DeltaDelete();
			delNode.addTargetURI( "#" + newRef.getId() );
			getDocument().addDel(delNode);
		}
	}
	
	private void writeDeltaForFile(CompareNode node) throws Exception
	{
		ExternalFile occ = ((PlmxmlFile)node.getNode().getObject()).getObj();
		ExternalFile newOcc = (ExternalFile) occ;
		
		System.out.println("Preparing delta for ImanFile " + node.getNodeUid());
		
		if(node.getDelta() == DELTA_INFO.NEW)
		{
			if(node.getParentCompNode() == null)
			{
				addDelta.addElement( newOcc );
			}
			else
			{
				if(node.getParentCompNode().getDelta() == DELTA_INFO.NEW)
				{
					addDelta.addElement( newOcc );
				}
				else
				{
					ICadexComparable rightParent = node.getParentCompNode().getNode();
					ExternalReference newRef = new ExternalReference();
					newRef.setType( rightParent.getType());					
					ApplicationRef newAppRef = new ApplicationRef();
					newAppRef.setApplication( ((IPlmxmlGenericObject)rightParent).getAppRefName() );
					newAppRef.setVersion( ((IPlmxmlGenericObject)rightParent).getAppRefVersion() );
					newAppRef.setLabel( ((IPlmxmlGenericObject)rightParent).getAppRefLabel() );
					newRef.addApplicationRef( newAppRef );
					getDocument().addNewElement(newRef);
					DeltaModify modNode = new DeltaModify();
					modNode.setAttributeName( "memberRefs" );
					modNode.setOp( DeltaModifyOp.ADD );
					modNode.setTargetRef( "#" + newRef.getId() );
					modNode.addValueURI( "#" + occ.getId() );
					getDocument().addModify(modNode);
					addDelta.addElement( newOcc );
				}
			}
		}
		else if( node.getDelta() == DELTA_INFO.MODIFIED )
		{
			modDelta.addElement( newOcc );
		}
		else if( node.getDelta() == DELTA_INFO.REMOVED )
		{
			ICadexComparable leftNode = node.getNode();
			ExternalReference newRef = new ExternalReference();
			newRef.setType( leftNode.getType());					
			ApplicationRef newAppRef = new ApplicationRef();
			newAppRef.setApplication( ((IPlmxmlGenericObject)leftNode).getAppRefName() );
			newAppRef.setVersion( ((IPlmxmlGenericObject)leftNode).getAppRefVersion() );
			newAppRef.setLabel( ((IPlmxmlGenericObject)leftNode).getAppRefLabel() );
			newRef.addApplicationRef( newAppRef );
			getDocument().addNewElement(newRef);
			DeltaDelete delNode = new DeltaDelete();
			delNode.addTargetURI( "#" + newRef.getId() );
			getDocument().addDel(delNode);
		}
		
	}
}

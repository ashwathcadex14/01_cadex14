package cadexstructurecompare.delta.plmxml;

import org.plmxml.sdk.plmxml.delta60.PLMXMLDelta;

import cadexstructurecompare.views.structurecompare.CompareNode;

public class GenerateDelta {

	public GenerateDelta(String newDeltaFile, CompareNode node) throws Exception {

		PLMXMLDelta plmxmlDelta = new PLMXMLDelta();
		plmxmlDelta.setAuthor( "CADEX Delta Generator BETA v0");
		PlmxmlDeltaGenerator deltaGen = new PlmxmlDeltaGenerator(plmxmlDelta, node);
		deltaGen.traverseStructure();
		
		plmxmlDelta.save( newDeltaFile );
		
	}

}

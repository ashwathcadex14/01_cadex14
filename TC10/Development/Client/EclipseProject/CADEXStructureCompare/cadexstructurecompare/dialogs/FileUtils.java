/**=================================================================================================                    
#                Copyright (c) 2014 BAVIS Technology Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CheckFile.java          
#      Module          :           com.teamcenter.siemens.briefcase.utils          
#      Description     :           This Class contains static methods related to File operations in this plugin          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Feb-2013                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    

package cadexstructurecompare.dialogs;

import org.apache.log4j.Logger;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class FileUtils {

	private static final Logger logger = Logger.getLogger(FileUtils.class);
	private static String MT_WILD_CHARACTER = "*" ;
	
    public static List<File> findDirectoryOrFile( String parentDirectoryPath , final String findThis , boolean firstLevel , final boolean findFile ) {
    	
    	logger
				.info(" ----- >> Entering Function findDirectoryOrFile with parameters ...."
						+ "parentDirectoryPath - "
						+ parentDirectoryPath
						+ "findThis - "
						+ findThis
						+ "firstLevel - "
						+ firstLevel + "findFile - " + findFile);
		List<File> thisFiles = new ArrayList<File>() ;
    	
    	File parentDirectory = new File( parentDirectoryPath ) ;
    	
        File[] files = parentDirectory.listFiles( new FileFilter() {
			
        	private boolean checkName(File arg0)
        	{
        		logger
						.info(" ----- >> Entering Function checkName with parameters ...."
								+ arg0);
				System.out.println( findThis );
        		
				if ( findThis.contains(MT_WILD_CHARACTER) && arg0.getName().startsWith(removeWildChars(findThis)) )
					return true;
				else if ( arg0.getName().equals( findThis ) )
					return true;
				else
					return false;
        	}
        	
			@Override
			public boolean accept(File arg0) {
				
				logger
						.info(" ----- >> Entering Function accept with parameters ...."
								+ arg0);
				if ( findFile && arg0.isFile() )
				{
					return checkName(arg0);
				}
				else if ( !findFile && arg0.isDirectory() )
				{
					return checkName(arg0);
				}
				else
					return false;
			}
		});
        	
        if ( firstLevel && files != null && files.length > 0 )
        {	
        	thisFiles =  new LinkedList<File>(Arrays.asList(files));;
        	return thisFiles;
        }
        else if ( !firstLevel && files.length == 0 )
        {
		    for ( File fil : parentDirectory.listFiles() )
		    {
		    	if ( fil.isDirectory() )
		    	{
		    		List<File> findFiles = findDirectoryOrFile( fil.getAbsolutePath() , findThis , firstLevel , findFile );
		    		
		    		if ( findFiles != null && findFiles.size() >0 )
		    			thisFiles.addAll( findFiles );
		    	}
		    		
		    }
        }
        else
        {
        	if(files != null && files.length > 0)
        		thisFiles =  Arrays.asList(files);
        	
        	return thisFiles;
        }
        
		logger.info(" << ----- Leaving Function findDirectoryOrFile ....");
		return thisFiles ;
    }
    
    public static List<File> getDesignDirs ( String parentDirectoryPath )
    {
    	logger
				.info(" ----- >> Entering Function getDesignDirs with parameters ...."
						+ parentDirectoryPath);
		List<File> thisFiles = new ArrayList<File>() ;
    	
    	File parentDirectory = new File( parentDirectoryPath ) ;
    	
        File[] files = parentDirectory.listFiles() ;
        
        for (File file : files) 
		{
			if (file.isFile() ) {
				continue;
			}

			if (file.getName().startsWith("D")) 
			{
				thisFiles.add(file);
			}
		}
	   	
    	logger.info(" << ----- Leaving Function getDesignDirs ....");
		return thisFiles ;
    }
    
    private static String removeWildChars ( String thisStr )
    {
    	logger
				.info(" ----- >> Entering Function removeWildChars with parameters ...."
						+ thisStr);
		logger.info(" << ----- Leaving Function removeWildChars ....");
		return thisStr.replace( "*" , "" ) ;
    }
    
	public static String getFileExtension(String filename)
	{
			logger
				.info(" ----- >> Entering Function getFileExtension with parameters ...."
						+ filename);
			if (filename == null) {
			            return null;
			}
			int lastUnixPos = filename.lastIndexOf('/');
			int lastWindowsPos = filename.lastIndexOf('\\');
			int indexOfLastSeparator = Math.max(lastUnixPos, lastWindowsPos);
			int extensionPos = filename.lastIndexOf('.');
			int lastSeparator = indexOfLastSeparator;
			int indexOfExtension = lastSeparator > extensionPos ? -1 : extensionPos;
			int index = indexOfExtension;
			if (index == -1) {
				logger.info(" << ----- Leaving Function getFileExtension ....");
			  return "";
			} else {
				logger.info(" << ----- Leaving Function getFileExtension ....");
			  return filename.substring(index + 1);
			}
	}
	
	public static String getFileNameWoExt( String thisFile )
	{
		logger
				.info(" ----- >> Entering Function getFileNameWoExt with parameters ...."
						+ thisFile);
		logger.info(" << ----- Leaving Function getFileNameWoExt ....");
		return thisFile.replaceFirst("[.][^.]+$", "") ;
	}
    
	public static void copyFile(File sourceFile, File destFile) throws IOException {
	    logger.info(" ----- >> Entering Function copyFile with parameters ...."
				+ "sourceFile - " + sourceFile + "destFile - " + destFile);
		if(!destFile.exists()) {
	        destFile.createNewFile();
	    }

	    FileChannel source = null;
	    FileChannel destination = null;

	    try {
	        source = new FileInputStream(sourceFile).getChannel();
	        destination = new FileOutputStream(destFile).getChannel();
	        destination.transferFrom(source, 0, source.size());
	    }
	    finally {
	        if(source != null) {
	            source.close();
	        }
	        if(destination != null) {
	            destination.close();
	        }
	    }
		logger.info(" << ----- Leaving Function copyFile ....");
	}
	
	@SuppressWarnings("resource")
	public static void lockFile( File fileToLock ) throws IOException
	{
		 logger.info(" ----- >> Entering Function lockFile with parameters ...."
				+ fileToLock);
		try {
		        // Get a file channel for the file
		       
		        FileChannel channel = new RandomAccessFile(fileToLock, "rw").getChannel();

		        // Use the file channel to create a lock on the file.
		        // This method blocks until it can retrieve the lock.
		        @SuppressWarnings("unused")
				FileLock lock = channel.lock();

		        /*
		           use channel.lock OR channel.tryLock();
		        */

		        // Try acquiring the lock without blocking. This method returns
		        // null or throws an exception if the file is already locked.
		        try {
		            lock = channel.tryLock();
		        } catch (OverlappingFileLockException e) {
		            logger.error("Following error has occured \n"
							, e);
					// File is already locked in this thread or virtual machine
		        	e.printStackTrace();
		        }

		        // Release the lock
//		        lock.release();
//
//		        // Close the file
//		        channel.close();
		    } catch (Exception e) {
		    	logger.error("Following error has occured \n" , e);
				e.printStackTrace();
		    }
		logger.info(" << ----- Leaving Function lockFile ....");
	}
	
	@SuppressWarnings("resource")
	public static void releaseLock( File releaseFile )
	{
		 logger
				.info(" ----- >> Entering Function releaseLock with parameters ...."
						+ releaseFile);
		try {
		        // Get a file channel for the file
		       
		        FileChannel channel = new RandomAccessFile(releaseFile, "rw").getChannel();

		        // Use the file channel to create a lock on the file.
		        // This method blocks until it can retrieve the lock.
		        FileLock lock = channel.lock();

		        /*
		           use channel.lock OR channel.tryLock();
		        */

		        // Try acquiring the lock without blocking. This method returns
		        // null or throws an exception if the file is already locked.
		        try {
		            lock = channel.tryLock();
		        } catch (OverlappingFileLockException e) {
		            logger.error("Following error has occured \n"
							, e);
					lock.release();
		        }

		        // Release the lock
//		        lock.release();
//
//		        // Close the file
//		        channel.close();
		    } catch (Exception e) {
				logger.error("Following error has occured \n" );
		    }
		logger.info(" << ----- Leaving Function releaseLock ....");
	}
	
	public static void cleanUp(File delFile)
	{		
		logger.info(" ----- >> Entering Function cleanUp with parameters ...."
				+ delFile);
		if(delFile.exists())
			delFile.delete();
		logger.info(" << ----- Leaving Function cleanUp ....");
	}
	
	@SuppressWarnings("resource")
	public static boolean searchFile( String filePath, String searchString )
	{
		logger
				.info(" ----- >> Entering Function searchFile with parameters ...."
						+ "filePath - "
						+ filePath
						+ "searchString - "
						+ searchString);
		File file = new File(filePath);
		boolean strFound = false;

		try {
		    Scanner scanner = new Scanner(file);
		
			//now read the file line by line...
			int lineNum = 0;
			while (scanner.hasNextLine()) {
			    String line = scanner.nextLine();
			    lineNum++;
			    if( line.contains(searchString) ) { 
			        System.out.println("ho hum, i found it on line " +lineNum);
			        strFound = true;
			        break;
			    }
			}
		} catch(FileNotFoundException e) {
			logger.error("Following error has occured \n" , e); 
		    //handle this
		}
		
		logger.info(" << ----- Leaving Function searchFile ....");
		return strFound;
	}
	
	public static void deleteFolder(File folder, boolean itself) {
	    logger
				.info(" ----- >> Entering Function deleteFolder with parameters ...."
						+ "folder - " + folder + "itself - " + itself);
		File[] files = folder.listFiles();
	    if(files!=null) { //some JVMs return null for empty dirs
	        for(File f: files) {
	            if(f.isDirectory()) {
	                deleteFolder(f,true);
	            } else {
	                f.delete();
	            }
	        }
	    }
	    if(itself)
	    	folder.delete();
		logger.info(" << ----- Leaving Function deleteFolder ....");
	}

	@Override

	public String toString() {
		String strValue = super.toString();
		return strValue;
	}
}

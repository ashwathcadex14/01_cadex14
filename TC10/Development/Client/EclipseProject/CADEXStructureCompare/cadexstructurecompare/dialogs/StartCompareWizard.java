/**=================================================================================================                    
#                Copyright (c) 2013 BAVIS Technology Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           PackageOpenWizard.java          
#      Module          :           com.teamcenter.siemens.briefcase.editors.open          
#      Description     :           Wizard for opening package in Briefcase Browser          
#      Project         :           SVAI External Engineering          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Feb-2013                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package cadexstructurecompare.dialogs;

import org.eclipse.jface.wizard.Wizard;

public class StartCompareWizard extends Wizard {

	protected StartCompareWizardPage one ;
	private String file1 = "" ;
	private String file2 = "" ;
	
	  public String getFile2() {
		return file2;
	}

	public StartCompareWizard( String sFileAbsPath ) {
		    super();
		    this.file1 = sFileAbsPath ;
		    setNeedsProgressMonitor(true);
		    
		  }

		  @Override
		  public void addPages() {
		    one = new StartCompareWizardPage( this.file1 );
		    
		    addPage(one);
		  }

		  @Override
		  public boolean performFinish() {
			
			this.file2 = this.one.getRightFilePath() ;
			  
		    return one.isPageComplete();
		  }
		  
		  @Override
		  public boolean canFinish() {
			
			return one.isPageComplete();
		  }

		public String getFile1() {
			return file1;
		}

}

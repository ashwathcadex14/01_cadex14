/**=================================================================================================                    
#                Copyright (c) 2013 BAVIS Technology Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           PackageOpenWizardDialog.java          
#      Module          :           com.teamcenter.siemens.briefcase.editors.open          
#      Description     :           Wizard Dialog for opening package in Briefcase Browser          
#      Project         :           SVAI External Engineering          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Feb-2013                         Ashwath                              Initial Creation
#  12-Feb-2014						Ashwath								Added image to title bar lines 40,41 
#  $HISTORY$                    
#  =================================================================================================*/                    
package cadexstructurecompare.dialogs;

import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.wb.swt.ResourceManager;

public class StartCompareWizardDialog extends WizardDialog {

	private String leftFile = "" ;
	private String rightFile = "" ;
	
	public StartCompareWizardDialog(Shell parentShell, IWizard newWizard) {
		super(parentShell, newWizard);
		
		setTitleImage( ResourceManager.getPluginImage("cadexstructurecompare", "icons/open_package.png") ) ;
		setTitle( "Start Compare" );
		
	}
	
	@Override
	public void create() {
		// TODO Auto-generated method stub
		super.create();
		this.getShell().setText( "Start Compare" );
		this.getShell().setImage( ResourceManager.getPluginImage("cadexstructurecompare", "icons/package_editor.PNG") );
	}
	
	@Override
	protected void finishPressed() {
	
		this.leftFile = ((StartCompareWizardPage)getCurrentPage()).getLeftFilePath();
		this.rightFile = ((StartCompareWizardPage)getCurrentPage()).getRightFilePath();
		
		super.finishPressed();
	}
	
	public String getLeftFilePath()
	{
		return this.leftFile ;
	}
	
	public String getRightFilePath()
	{
		return this.rightFile ;
	}
}

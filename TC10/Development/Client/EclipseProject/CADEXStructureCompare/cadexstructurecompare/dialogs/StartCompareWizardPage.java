/**=================================================================================================                    
#                Copyright (c) 2013 BAVIS Technology Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           PackageOpenWizardPage.java          
#      Module          :           com.teamcenter.siemens.briefcase.editors.open          
#      Description     :           Wizard Page for opening package in Briefcase Browser          
#      Project         :           SVAI External Engineering          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Feb-2013                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package cadexstructurecompare.dialogs;

import java.io.File;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.ResourceManager;

public class StartCompareWizardPage extends WizardPage {
	
	private Composite container;
	Composite composite_1 ;
	private Text text;
	private Text text_1;
	private String sFilePath = "" ;
	private String sFile1Path = "" ;

	public StartCompareWizardPage( String sPath ) {
		super("Start Compare", "Start Compare", null);
		setMessage("Select the plmxml files to compare");
		this.sFilePath = sPath ;
		setTitle("Open Plmxml File");
		// TODO Auto-generated constructor stub
		
	}

	@Override
	public void createControl(final Composite parent) {
		// TODO Auto-generated method stub
	    container = new Composite(parent, SWT.NULL);
	    
	    setControl(container);
	    container.setLayout(new GridLayout(1, false));
	    
	    Composite composite = new Composite(container, SWT.NONE);
	    
	    Label lblFileName = new Label(composite, SWT.NONE);
	    lblFileName.setBounds(10, 10, 81, 25);
	    lblFileName.setText("Left File");
	    
	    text = new Text(composite, SWT.BORDER);
	    text.setBounds(167, 10, 637, 31);
	    text.setText( this.sFilePath ) ;
	    
	    Button btnBrowse = new Button(composite, SWT.NONE);
	    btnBrowse.addSelectionListener(new SelectionAdapter() {
	    	@Override
	    	public void widgetSelected(SelectionEvent e) {
	    		
	    		String[] zipExt = { "*.xml" } ;
	    		
	    		File newFile[] = OpenFileDialog.openDialogForFile( parent.getShell() , "Select PLMXML File" , zipExt , "" , "" , false ) ;
	    		
	    		if( newFile != null  && newFile.length == 1 )
	    			sFilePath = newFile[0].getAbsoluteFile().toString() ;
	    		
	    		text.setText( sFilePath ) ;
	    		
	    	}
	    });
	    btnBrowse.setImage(ResourceManager.getPluginImage("cadexstructurecompare", "icons/browse.png"));
	    btnBrowse.setBounds(824, 10, 132, 31);
	    btnBrowse.setText("Browse");
	    
	    composite_1 = new Composite(composite, SWT.NONE);
	    composite_1.setBounds(0, 82, 966, 92);
	    
	    composite_1.setVisible( true ) ;
	    
	    Label lblRightFile = new Label(composite_1, SWT.NONE);
	    lblRightFile.setText("Right File");
	    lblRightFile.setBounds(10, 20, 156, 25);
	    
	    text_1 = new Text(composite_1, SWT.BORDER);
	    text_1.setBounds(167, 20, 637, 31);
	    
	    Button button = new Button(composite_1, SWT.CENTER);
	    button.setText("Browse");
	    button.setImage(ResourceManager.getPluginImage("cadexstructurecompare", "icons/browse.png"));
	    button.setBounds(824, 20, 132, 31);
	    
	    button.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				String[] zipExt = { "*.xml" } ;
				
				File newFile[] = OpenFileDialog.openDialogForFile( parent.getShell() , "Select PLMXML File" , zipExt , "" , "" , false ) ;
	    		
				if( newFile != null  && newFile.length == 1 )
					sFile1Path = newFile[0].getAbsoluteFile().toString() ;
	    		
	    		text_1.setText( sFile1Path ) ;
	    		
	    		
	    		setPageComplete( isPageComplete() ) ;
			
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	    
	    text_1.addModifyListener(new ModifyListener() {
			
			@Override
			public void modifyText(ModifyEvent e) {
				// TODO Auto-generated method stub
				setPageComplete( isPageComplete() ) ;
			}
		});
	    
	    setPageComplete(false);
	}
	
	@Override
	public boolean isPageComplete() {
		
		if ( text.getText().length() > 0 && composite_1.isVisible() && text_1.getText().length() == 0  )
			return false ;
		else if ( !(composite_1.isVisible()) && text.getText().length() == 0 )
			return false ;
		else
			return true ;
		
	}
	
	public String getLeftFilePath()
	{
		return this.sFilePath ;
	}
	
	public String getRightFilePath()
	{
		return this.sFile1Path ;
	}
}

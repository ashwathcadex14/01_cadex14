package cadexstructurecompare.editors;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.MultiPageEditorPart;

import cadexstructurecompare.utils.CadexUtils;
import cadexstructurecompare.views.structurecompare.CadexCompareEditorInput;

	

public class CompareStrucureMultiPageEditor extends MultiPageEditorPart {

	public static String ID = "CADEXStructureCompare.editors.CadexCompareEditor" ;
	private CompareStructureEditor newCadexEditor ;
	
	@Override
	protected void createPages() 
	{
		newCadexEditor = new CompareStructureEditor( this ) ;
		
		setPartName( ((CadexCompareEditorInput)getEditorInput()).toString() ) ;
		
		try {
			addPage( newCadexEditor , getEditorInput() ) ;
		} catch (final PartInitException e) {
			getSite().getShell().getDisplay().syncExec( new Runnable() {
				
				@Override
				public void run() {
					CadexUtils.openError(getSite().getShell(), "Cadex Editor Error", "Unable to compare." + e.getMessage() + "\n" ,e);	
				}
			});
		}
			

		
		if (getPageCount() == 1 && getContainer() instanceof CTabFolder) {
            ((CTabFolder) getContainer()).setTabHeight(0);
        }

	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		// TODO Auto-generated method stub

	}

	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isSaveAsAllowed() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	protected IEditorPart getActiveEditor() {
		// TODO Auto-generated method stub
		return super.getActiveEditor() ;
	}

	public CompareStructureEditor getCompareEditor()
	{
		return this.newCadexEditor;
	}
	
	@Override
	protected void pageChange(int newPageIndex) {
		// TODO Auto-generated method stub
		//super.pageChange(newPageIndex);
	}
	

}

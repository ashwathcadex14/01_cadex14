package cadexstructurecompare.handlers;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.progress.IProgressConstants;
import org.eclipse.wb.swt.ResourceManager;

import cadexstructurecompare.dialogs.OpenFileDialog;
import cadexstructurecompare.dialogs.StartCompareWizard;
import cadexstructurecompare.dialogs.StartCompareWizardDialog;
import cadexstructurecompare.utils.CadexUtils;
import cadexstructurecompare.views.StructureCompare;

public class StartCompareHandler extends AbstractHandler {

	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		final IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindow(event);
//		final IWorkbenchPage page = window.getActivePage() ;
		String[] zipExt = { "*.xml" } ;
	    
		File packagefile[] = OpenFileDialog.openDialogForFile( window.getShell() , "Select PLMXML File" , zipExt , "" ,"", false) ;
		
		    		
		String leftFilePath = "" ;
		
		if( packagefile != null && packagefile.length == 1 )
			leftFilePath = packagefile[0].getAbsolutePath() ;
		else
			return null ;
		
		String rightFilePath = "" ;
		
		StartCompareWizard tWiz = new StartCompareWizard( packagefile[0].getAbsolutePath() ) ;
		
		StartCompareWizardDialog p = new StartCompareWizardDialog( window.getShell() , tWiz ) ;
		
		int isDone = p.open() ;
		
		if ( isDone == 0 )
		 {
			leftFilePath = p.getLeftFilePath();
			
			rightFilePath = p.getRightFilePath();
				
			final String tempPackageFilePath = leftFilePath ;
			
			final String tempworkingDirectory = rightFilePath ;
			
//			final String tempPackageFilePath = "C:\\apps\\plmxml_rnd\\large_assy\\000062_A_1-TopCheck.xml" ;
//			
//			final String tempworkingDirectory = "C:\\apps\\plmxml_rnd\\large_assy\\000062_A_1-TopCheck_samey.xml" ;

			final Job engJob = new Job( "Comparing Structures...." ) {
				
				@Override
				protected IStatus run(final IProgressMonitor monitor) 
				{
					try 
					{
						//final CadexCompareEditorInput newInput = new CadexCompareEditorInput(tempPackageFilePath, tempworkingDirectory, monitor);
						window.getShell().getDisplay().syncExec( new Runnable() {
							
							@Override
							public void run() {
								try 
								{
									StructureCompare compareView = (StructureCompare) window.getActivePage().findViewReference( StructureCompare.ID ).getView(true);
//									page.openEditor( newInput , CompareStructureEditor.ID );
									PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView( StructureCompare.ID );
									compareView.startCompare(tempPackageFilePath, tempworkingDirectory);
								}
								catch (final Exception e) {

									window.getShell().getDisplay().syncExec( new Runnable() {
										
										@Override
										public void run() {
											CadexUtils.openError( window.getShell() , "Compare Problem", "Problem occured while comparing two files." , e);	
										}
									} );
								}	
							}
						} );
						
					} 
					catch (final Exception e)
					{
						window.getShell().getDisplay().syncExec( new Runnable() {
							
							@Override
							public void run() {
								CadexUtils.openError( window.getShell() , "Compare Problem", "Problem occured while comparing two files." , e);	
							}
						} );
					} 
					
					return Status.OK_STATUS ;
				}
			};
			
			
			engJob.setProperty(IProgressConstants.KEEP_PROPERTY, Boolean.TRUE);
			engJob.setProperty(IProgressConstants.ICON_PROPERTY, ResourceManager.getPluginImageDescriptor( "com.teamcenter.siemens.briefcase" , "icons/package_editor.PNG" ));
			engJob.schedule() ;
			
		 }
		
		return null;
	}

}

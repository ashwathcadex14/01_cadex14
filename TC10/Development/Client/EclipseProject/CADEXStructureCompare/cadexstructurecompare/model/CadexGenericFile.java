package cadexstructurecompare.model;

import cadexstructurecompare.model.interfaces.ICadexGenericFile;

public abstract class CadexGenericFile<T> extends CadexGenericObject<T> implements
		ICadexGenericFile {

	public CadexGenericFile(T obj) throws Exception {
		super(obj);
	}

	public abstract void setProperties() throws Exception;

}

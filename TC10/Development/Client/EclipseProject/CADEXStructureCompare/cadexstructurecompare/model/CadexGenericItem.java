package cadexstructurecompare.model;

import cadexstructurecompare.model.interfaces.ICadexGenericItem;

public abstract class CadexGenericItem<T> extends CadexGenericObject<T> implements ICadexGenericItem{
	
	public CadexGenericItem(T item) throws Exception {
		super(item);
	}

}
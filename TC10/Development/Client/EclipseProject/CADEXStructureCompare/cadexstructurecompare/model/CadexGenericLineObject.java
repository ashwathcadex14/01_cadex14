package cadexstructurecompare.model;

import java.util.ArrayList;

import cadexstructurecompare.delta.IdObjectMap;
import cadexstructurecompare.model.interfaces.ICadexGenericDataset;
import cadexstructurecompare.model.interfaces.ICadexGenericFile;
import cadexstructurecompare.model.interfaces.ICadexGenericItem;
import cadexstructurecompare.model.interfaces.ICadexGenericLineObject;
import cadexstructurecompare.model.interfaces.ICadexGenericRevision;


public abstract class CadexGenericLineObject<T> extends CadexGenericObject<T> implements ICadexGenericLineObject {
	
	private ICadexGenericRevision lineItem ;
	private ArrayList<ICadexGenericLineObject> children = new ArrayList<ICadexGenericLineObject>();
	
	public CadexGenericLineObject(T lineObj) throws Exception {
		super(lineObj);
		setLineRev();
		setChildren();
	}
	
	public abstract ICadexGenericRevision getItemRevision() throws Exception;
	public abstract ArrayList<ICadexGenericLineObject> getChildLines() throws Exception;

	public ICadexGenericRevision getLineRev() {
		return lineItem;
	}

	public void setLineRev() throws Exception {
		this.lineItem = getItemRevision();
	}

	public ArrayList<ICadexGenericLineObject> returnChildren() {
		return children;
	}

	private void setChildren() throws Exception {
		this.children.addAll( getChildLines() );
	}
	

	@Override
	public void addUniqueObjs(IdObjectMap map) {
		
		if( !map.containsId( this.getUid() ) )
			map.addObj(this.getUid(), this);
		
		ICadexGenericRevision rev = this.getLineRev();
		
		if( !map.containsId( rev.getUid() ) )
		{
			map.addObj( rev.getUid() , rev);
			
			ICadexGenericItem item = rev.getRevItem();
			
			if( !map.containsId( item.getUid() ) )
				map.addObj( item.getUid() , item);
			
			ArrayList<ICadexGenericDataset> dsetS = rev.getAttaches();
			
			for(ICadexGenericDataset dset : dsetS)
			{
				if( !map.containsId( dset.getUid() ) )
					map.addObj(dset.getUid(), dset);
				
				ArrayList<ICadexGenericFile> fileS = dset.getFiles();
				
				for(ICadexGenericFile file : fileS)
				{
					if( !map.containsId( file.getUid() ) )
						map.addObj(file.getUid(), file);
				}
			}
		}
	}
}

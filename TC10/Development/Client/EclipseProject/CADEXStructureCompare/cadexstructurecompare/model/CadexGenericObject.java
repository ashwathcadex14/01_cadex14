package cadexstructurecompare.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

import org.eclipse.compare.ITypedElement;

import cadexstructurecompare.delta.AbstractDeltaGenerator.DELTA_INFO;
import cadexstructurecompare.model.interfaces.ICadexComparable;
import cadexstructurecompare.model.interfaces.ICadexGenericObject;
import cadexstructurecompare.views.structurecompare.AttributeNode;

public abstract class CadexGenericObject<T> implements ICadexGenericObject{

	private T obj ;
	private HashMap<String, String> attrMap = new HashMap<String,String>();
	private cadexstructurecompare.delta.AbstractDeltaGenerator.DELTA_INFO changeInfo = DELTA_INFO.NO_CHANGE;
	private HashMap<String, AttributeNode> attrNodes = new HashMap<String, AttributeNode>();
	
	public CadexGenericObject(T obj) throws Exception {
		this.obj = obj;
		setProperties();
	}
	
	@Override
	public boolean equals(Object other) {
		if (other instanceof ITypedElement)
			return getName().equals(((ITypedElement) other).getName());
		return super.equals(other);
	}
	
	
	public int hashCode() {
		return getName().hashCode();
	}

	@Override
	public String toString() {
		return getName();
				
	}
	
	public abstract void setProperties() throws Exception;
	
	
	public T getObj() {
		return obj;
	}

	public String getAttr(String attrName) {
		return attrMap.get( attrName );
	}

	public void addAttr(String attrName, String attrValue) {
		this.attrMap.put(attrName, attrValue);
	}
	
	@Override
	public HashSet<String> getAttrNames()
	{
		HashSet<String> attrNames = new HashSet<String>();
		attrNames.addAll( this.attrMap.keySet() );
		return attrNames;
	}
	
	@Override
	public DELTA_INFO changeInfo() {
		// TODO Auto-generated method stub
		return changeInfo;
	}
	
	@Override
	public void setChangeInfo(DELTA_INFO delta) {
		this.changeInfo = delta;
	}
	
	@Override
	public String getObjectString() {
		return getAttr( "object_string" );
	}
	
	@Override
	public boolean doDeepCompare(ICadexComparable oldObject) {
		
//		boolean isSame = true;
//		
//		for(Entry<String, String> entry : this.attrMap.entrySet())
//		{
//			
//			if(entry.getKey().equals( "last_mod_date" ))
//			{
//				
//				String oldAttr = oldObject.getObject().getAttr( entry.getKey() );
//				String newAttr = entry.getValue();
//				
//				SimpleDateFormat frmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//				try 
//				{
//					Date oldDate = frmt.parse( oldAttr );
//					Date newDate = frmt.parse( newAttr );
//					
//					if(newDate.compareTo(oldDate) < 0)
//						isSame = false;
//					
//				} catch (ParseException e) {
//					isSame = false;
//				}
//				
//				
//				break;
//			}
//
//		}
//		
//		return !isSame;
		
		
		boolean isSame = true;

        HashSet<String> thisAttrs = this.getAttrNames();
        HashSet<String> oldObjAttrs = oldObject.getAttrNames();

        thisAttrs.addAll(oldObjAttrs);

        for (String entry : thisAttrs)
	    {
            String oldAttr = getAttr(entry);
            String newAttr = oldObject.getObject().getAttr(entry);
            if(entry.equals( "last_mod_date" ))
		    {
				SimpleDateFormat frmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				try 
				{
					Date oldDate = frmt.parse( oldAttr );
					Date newDate = frmt.parse( newAttr );
					
					if(oldDate.compareTo(newDate) < 0)
						isSame = false;
					
				} catch (ParseException e) {
					isSame = false;
				}
		    }
	    }
	
	    return !isSame;
	}
	
	@Override
	public ICadexGenericObject getObject() {
		// TODO Auto-generated method stub
		return this;
	}

	@Override
	public String getDeltaInfo() {
		// TODO Auto-generated method stub
		return this.changeInfo().name();
	}
	
	@Override
	public HashMap<String, AttributeNode> getAttrNodes() {
		// TODO Auto-generated method stub
		return attrNodes;
	}
	
    public void addAttrNode(String attrName, String leftValue, String rightValue, String ancestorValue)
    {
        AttributeNode node = new AttributeNode(attrName, leftValue, rightValue, ancestorValue);
        attrNodes.put(attrName, node);
    }
}

package cadexstructurecompare.model;

import java.util.ArrayList;

import cadexstructurecompare.model.interfaces.ICadexGenericDataset;
import cadexstructurecompare.model.interfaces.ICadexGenericItem;
import cadexstructurecompare.model.interfaces.ICadexGenericRevision;

public abstract class CadexGenericRevision<T> extends CadexGenericObject<T> implements ICadexGenericRevision{

	private ICadexGenericItem revItem ;
	private ArrayList<ICadexGenericDataset> attaches = new ArrayList<ICadexGenericDataset>();
	
	public CadexGenericRevision(T obj) throws Exception {
		super(obj);
		setRevItem();
		setAttaches();
	}

	public abstract ArrayList<ICadexGenericDataset> getAttachments() throws Exception;
	public abstract ICadexGenericItem getItem() throws Exception;
	
	public ArrayList<ICadexGenericDataset> getAttaches() {
		return attaches;
	}

	public void setAttaches() throws Exception {
		this.attaches.addAll( getAttachments() );
	}

	public ICadexGenericItem getRevItem() {
		return revItem;
	}

	private void setRevItem() throws Exception {
		this.revItem = getItem();
	}
	
	

}

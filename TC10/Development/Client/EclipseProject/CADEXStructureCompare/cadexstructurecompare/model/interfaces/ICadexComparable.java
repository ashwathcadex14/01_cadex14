package cadexstructurecompare.model.interfaces;

import java.util.HashMap;
import java.util.HashSet;

import org.eclipse.compare.ITypedElement;
import org.eclipse.compare.structuremergeviewer.IStructureComparator;

import cadexstructurecompare.delta.AbstractDeltaGenerator.DELTA_INFO;
import cadexstructurecompare.views.structurecompare.AttributeNode;

public interface ICadexComparable extends IStructureComparator, ITypedElement 
{
	public ICadexGenericObject getObject();
	
	public int getChildrenCount();
	
	public String getObjectString();
	
	public String getDeltaInfo();
	
	public DELTA_INFO changeInfo();
	
	public void generateDelta();
	
	public void setChangeInfo(DELTA_INFO delta);
	
	public boolean doDeepCompare(ICadexComparable oldObject);
	
    HashMap<String, AttributeNode> getAttrNodes();

    HashSet<String> getAttrNames();
	
}

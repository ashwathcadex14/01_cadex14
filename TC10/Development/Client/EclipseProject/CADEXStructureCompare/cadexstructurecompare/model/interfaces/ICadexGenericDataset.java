package cadexstructurecompare.model.interfaces;

import java.util.ArrayList;

public interface ICadexGenericDataset extends ICadexGenericObject{

	public ArrayList<ICadexGenericFile> getFiles();
	public String getType();
	public String getRelation();
	
}

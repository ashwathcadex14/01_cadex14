package cadexstructurecompare.model.interfaces;

public interface ICadexGenericFile extends ICadexGenericObject {

	public String getOriginalFileName();
}

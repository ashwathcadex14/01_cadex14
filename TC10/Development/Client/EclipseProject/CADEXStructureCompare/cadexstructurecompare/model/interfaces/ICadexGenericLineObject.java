package cadexstructurecompare.model.interfaces;

import java.util.ArrayList;

import cadexstructurecompare.delta.IdObjectMap;

public interface ICadexGenericLineObject extends ICadexGenericObject {

	public ICadexGenericRevision getLineRev();

	public ArrayList<ICadexGenericLineObject> returnChildren();
	
	public void addUniqueObjs(IdObjectMap map);
	
}

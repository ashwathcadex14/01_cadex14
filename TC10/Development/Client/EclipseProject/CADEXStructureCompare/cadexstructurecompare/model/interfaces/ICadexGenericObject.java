package cadexstructurecompare.model.interfaces;


public interface ICadexGenericObject extends ICadexComparable {
	
	public Object getObj();

	public String getAttr(String attrName);

	public void addAttr(String attrName, String attrValue);

	public String getUid();
	
	public String getTagName();
	
	public boolean isNew();
	
}
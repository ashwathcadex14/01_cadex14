package cadexstructurecompare.model.plmxml;

import org.plmxml.sdk.plmxml.plmxml60.DescriptionObject;

public interface IPlmxmlGenericObject 
{

	public String getAppRefLabel();
	public String getAppRefVersion();
	public String getAppRefName();
	public void setAppRefLabel(String a);
	public void setAppRefVersion(String a);
	public void setAppRefName(String a);
	public DescriptionObject getDescObj();
	
}

package cadexstructurecompare.model.plmxml;

import java.util.ArrayList;

import org.eclipse.swt.graphics.Image;
import org.plmxml.sdk.plmxml.plmxml60.AssociatedDataSet;
import org.plmxml.sdk.plmxml.plmxml60.DataSet;
import org.plmxml.sdk.plmxml.plmxml60.DescriptionObject;
import org.plmxml.sdk.plmxml.plmxml60.ExternalFile;

import cadexstructurecompare.model.CadexGenericDataset;
import cadexstructurecompare.model.interfaces.ICadexComparable;
import cadexstructurecompare.model.interfaces.ICadexGenericFile;

public class PlmxmlDataset extends CadexGenericDataset<DataSet> implements IPlmxmlGenericObject {

	protected String applicationName = "";
	protected String appRefLabel = "";
	protected String appRefVersion = "";
	protected AssociatedDataSet relNode = null;
	
	public PlmxmlDataset(DataSet obj, String relation) throws Exception {
		super(obj,relation);
		PlmxmlUtil.populateAppRef(this);
	}

	@Override
	public void setProperties() throws Exception
	{
		this.addAttr( "object_name" , (String) this.getObj().getAttributeValue( "name" ) );
		this.addAttr( "object_type" , (String) this.getObj().getAttributeValue( "type" ) );
		this.addAttr( "version" , this.getObj().getAttributeValue( "version" ).toString() );
		PlmxmlUtil.addUserData(this, this.getObj());
		this.addAttr( "object_string" , this.getAttr("object_name")  );
	}

	@Override
	public ArrayList<ICadexGenericFile> populateFiles() throws Exception{
		
		ArrayList<ICadexGenericFile> files = new ArrayList<ICadexGenericFile>();
		String[] baseObj = this.getObj().getMemberURIs();
		
		if (baseObj.length > 0)
		{			
			for(int i = 0; i < baseObj.length; i++) 
			{
				ExternalFile file = (ExternalFile) this.getObj().getMemberHandle( i ).getObject();
				files.add( new PlmxmlFile( file ) ); 
			}
		}
		
		return files;
		
	}

	@Override
	public String getType() {
		return "Dataset";
	}

	@Override
	public boolean doDeepCompare(ICadexComparable obj) {
		// TODO Auto-generated method stub
		return super.doDeepCompare(obj);
	}

	@Override
	public boolean isNew() {
		// TODO Auto-generated method stub
		return !applicationName.equals( PlmxmlUtil.TEAMCENTER_APP_NAME ) ? true : false;
	}

	@Override
	public String getUid() {
		// TODO Auto-generated method stub
		return getAppRefLabel();
	}

	@Override
	public String getTagName() {
		// TODO Auto-generated method stub
		return PlmxmlUtil.PLMXML_DATASET;
	}

	@Override
	public String getAppRefLabel() {
		// TODO Auto-generated method stub
		return appRefLabel;
	}

	@Override
	public String getAppRefVersion() {
		// TODO Auto-generated method stub
		return appRefVersion;
	}

	@Override
	public String getAppRefName() {
		// TODO Auto-generated method stub
		return applicationName;
	}

	@Override
	public void setAppRefLabel(String a) {
		this.appRefLabel = a;
		
	}

	@Override
	public void setAppRefVersion(String a) {
		this.appRefVersion = a;
	}

	@Override
	public void setAppRefName(String a) {
		this.applicationName = a;
	}

	@Override
	public DescriptionObject getDescObj() {
		// TODO Auto-generated method stub
		return getObj();
	}

	@Override
	public Image getImage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return getUid();
	}

	@Override
	public Object[] getChildren() 
	{
		ArrayList<ICadexComparable> allChilds = new ArrayList<ICadexComparable>();
		
		for(ICadexGenericFile line : this.getFiles())
		{
			allChilds.add( line );
		}
		
		return allChilds.toArray();
	}

	@Override
	public int getChildrenCount() {
		Object[] objs = this.getChildren();
		return objs != null ? objs.length : 0 ;
	}


	@Override
	public void generateDelta() {
		// TODO Auto-generated method stub
		
	}

	public AssociatedDataSet getRelNode() {
		return relNode;
	}

	public void setRelNode(AssociatedDataSet relNode) {
		this.relNode = relNode;
	}

}

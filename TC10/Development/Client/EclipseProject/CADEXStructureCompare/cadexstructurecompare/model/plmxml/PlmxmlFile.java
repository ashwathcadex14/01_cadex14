package cadexstructurecompare.model.plmxml;

import org.eclipse.swt.graphics.Image;
import org.plmxml.sdk.plmxml.plmxml60.DescriptionObject;
import org.plmxml.sdk.plmxml.plmxml60.ExternalFile;

import cadexstructurecompare.model.CadexGenericFile;
import cadexstructurecompare.model.interfaces.ICadexComparable;

public class PlmxmlFile extends CadexGenericFile<ExternalFile> implements IPlmxmlGenericObject {

	protected String applicationName = "";
	protected String appRefLabel = "";
	protected String appRefVersion = "";
	
	public PlmxmlFile(ExternalFile obj) throws Exception {
		super(obj);
		PlmxmlUtil.populateAppRef(this);
	}

	@Override
	public void setProperties() throws Exception {
		this.addAttr( "format" , (String) this.getObj().getAttributeValue( "format" ) );
		String locationRef = (String) this.getObj().getAttributeValueAsString( "locationRef" );
		String orgFileName = "";
		if(locationRef.contains( "/" ))
			orgFileName = locationRef.substring( locationRef.lastIndexOf( "/" )+1, locationRef.length());
		else
			orgFileName = locationRef.substring( locationRef.lastIndexOf( "\\" )+1, locationRef.length());
		
		this.addAttr( "location" , locationRef  );
		this.addAttr( "original_file_name" , orgFileName  );
		this.addAttr( "object_string" , orgFileName  );
		PlmxmlUtil.addUserData(this, this.getObj());
	}

	@Override
	public String getOriginalFileName() {
		return this.getAttr( "original_file_name" );
	}

	@Override
	public boolean doDeepCompare(ICadexComparable obj) {
		// TODO Auto-generated method stub
		return super.doDeepCompare(obj);
	}

	@Override
	public boolean isNew() {
		// TODO Auto-generated method stub
		return !applicationName.equals( PlmxmlUtil.TEAMCENTER_APP_NAME ) ? true : false;
	}

	@Override
	public String getUid() {
		// TODO Auto-generated method stub
		return getAppRefLabel();
	}

	@Override
	public String getTagName() {
		// TODO Auto-generated method stub
		return PlmxmlUtil.PLMXML_FILE;
	}

	@Override
	public String getAppRefLabel() {
		// TODO Auto-generated method stub
		return appRefLabel;
	}

	@Override
	public String getAppRefVersion() {
		// TODO Auto-generated method stub
		return appRefVersion;
	}

	@Override
	public String getAppRefName() {
		// TODO Auto-generated method stub
		return applicationName;
	}

	@Override
	public void setAppRefLabel(String a) {
		this.appRefLabel = a;
		
	}

	@Override
	public void setAppRefVersion(String a) {
		this.appRefVersion = a;
	}

	@Override
	public void setAppRefName(String a) {
		this.applicationName = a;
	}

	@Override
	public DescriptionObject getDescObj() {
		// TODO Auto-generated method stub
		return getObj();
	}

	@Override
	public Image getImage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return getUid();
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "ImanFile";
	}

	@Override
	public Object[] getChildren() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getChildrenCount() {
		Object[] objs = this.getChildren();
		return objs != null ? objs.length : 0 ;
	}
	
	@Override
	public void generateDelta() {
		// TODO Auto-generated method stub
		
	}
}

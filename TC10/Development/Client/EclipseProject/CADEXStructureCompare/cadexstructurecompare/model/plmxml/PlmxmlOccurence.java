package cadexstructurecompare.model.plmxml;

import java.util.ArrayList;

import org.eclipse.swt.graphics.Image;
import org.plmxml.sdk.plmxml.plmxml60.DescriptionObject;
import org.plmxml.sdk.plmxml.plmxml60.IdObject;
import org.plmxml.sdk.plmxml.plmxml60.Occurrence;
import org.plmxml.sdk.plmxml.plmxml60.ProductRevision;

import cadexstructurecompare.model.CadexGenericLineObject;
import cadexstructurecompare.model.interfaces.ICadexComparable;
import cadexstructurecompare.model.interfaces.ICadexGenericLineObject;
import cadexstructurecompare.model.interfaces.ICadexGenericObject;
import cadexstructurecompare.model.interfaces.ICadexGenericRevision;

public class PlmxmlOccurence extends CadexGenericLineObject<Occurrence> implements IPlmxmlGenericObject {

	
	protected String applicationName = "";
	protected String appRefLabel = "";
	protected String appRefVersion = "";
	
	public PlmxmlOccurence(Occurrence lineObj) throws Exception {
		super(lineObj);
		PlmxmlUtil.populateAppRef(this);
		addObjString();
	}

	@Override
	public void setProperties() throws Exception 
	{
		PlmxmlUtil.addUserData(this, this.getObj());
	}

	@Override
	public ArrayList<ICadexGenericLineObject> getChildLines() throws Exception {
		
		ArrayList<ICadexGenericLineObject> childList = new ArrayList<ICadexGenericLineObject>();
		
		IdObject[] objs = this.getObj().resolveOccurrenceRefs();
		
		for (int i = 0; i < objs.length; i++) {
			 childList.add( (ICadexGenericLineObject) new PlmxmlOccurence( (Occurrence)objs[i] )) ;
		}
		
		return childList;
	}

	@Override
	public ICadexGenericRevision getItemRevision() throws Exception {
		
		ProductRevision rev = null;
		
		rev = (ProductRevision) this.getObj().getInstancedHandle().getObject();
		
		return (ICadexGenericRevision) new PlmxmlRevision(rev);
	}

	@Override
	public Object[] getChildren() 
	{
		ArrayList<ICadexComparable> allChilds = new ArrayList<ICadexComparable>();
		
		for(ICadexGenericLineObject line : this.returnChildren())
		{
			allChilds.add( line );
		}
		
		try {
			allChilds.add( getLineRev() );
		} catch (Exception e) {
			// Needs Proper Handling
			e.printStackTrace();
		}
		
		return allChilds.toArray();
	}

	@Override
	public String getName() {

		return getUid();
	}
	
	private void addObjString()
	{
		ICadexGenericObject lineRev = ((ICadexGenericObject) getLineRev());
		if(lineRev!=null)
		{
			String revId = lineRev.getAttr( "item_revision_id" ) ;
			String objName = lineRev.getAttr( "object_name" ) ;
			String itemId = ((ICadexGenericObject) getLineRev().getRevItem()).getAttr( "item_id" ) ;
			addAttr("object_string", itemId + "/" + revId + ";" + objName);
		}
	}

	@Override
	public Image getImage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "Occurrence";
	}

	@Override
	public boolean doDeepCompare(ICadexComparable obj) {
		// TODO Auto-generated method stub
		return super.doDeepCompare(obj);
	}
	
	@Override
	public boolean isNew() {
		// TODO Auto-generated method stub
		return !applicationName.equals( PlmxmlUtil.TEAMCENTER_APP_NAME ) ? true : false;
	}

	@Override
	public String getUid() {
		// TODO Auto-generated method stub
		return getAppRefLabel();
	}

	@Override
	public String getTagName() {
		// TODO Auto-generated method stub
		return PlmxmlUtil.PLMXML_OCCURRENCE;
	}

	@Override
	public String getAppRefLabel() {
		// TODO Auto-generated method stub
		return appRefLabel;
	}

	@Override
	public String getAppRefVersion() {
		// TODO Auto-generated method stub
		return appRefVersion;
	}

	@Override
	public String getAppRefName() {
		// TODO Auto-generated method stub
		return applicationName;
	}

	@Override
	public void setAppRefLabel(String a) {
		this.appRefLabel = a;
		
	}

	@Override
	public void setAppRefVersion(String a) {
		this.appRefVersion = a;
	}

	@Override
	public void setAppRefName(String a) {
		this.applicationName = a;
	}

	@Override
	public DescriptionObject getDescObj() {
		// TODO Auto-generated method stub
		return getObj();
	}

	@Override
	public int getChildrenCount() {
		Object[] objs = this.getChildren();
		return objs != null ? objs.length : 0 ;
	}


	@Override
	public void generateDelta() {
		// TODO Auto-generated method stub
		
	}

}

package cadexstructurecompare.model.plmxml;

import java.util.ArrayList;

import org.eclipse.swt.graphics.Image;
import org.plmxml.sdk.plmxml.ObjectBase;
import org.plmxml.sdk.plmxml.plmxml60.AssociatedDataSet;
import org.plmxml.sdk.plmxml.plmxml60.DataSet;
import org.plmxml.sdk.plmxml.plmxml60.DescriptionObject;
import org.plmxml.sdk.plmxml.plmxml60.Product;
import org.plmxml.sdk.plmxml.plmxml60.ProductRevision;

import cadexstructurecompare.model.CadexGenericRevision;
import cadexstructurecompare.model.interfaces.ICadexComparable;
import cadexstructurecompare.model.interfaces.ICadexGenericDataset;
import cadexstructurecompare.model.interfaces.ICadexGenericItem;
import cadexstructurecompare.model.interfaces.ICadexGenericObject;

public class PlmxmlRevision extends CadexGenericRevision<ProductRevision> implements IPlmxmlGenericObject{

	protected String applicationName = "";
	protected String appRefLabel = "";
	protected String appRefVersion = "";
	
	public PlmxmlRevision(ProductRevision obj) throws Exception {
		super(obj);
		PlmxmlUtil.populateAppRef(this);
		addObjString();
	}

	@Override
	public ArrayList<ICadexGenericDataset> getAttachments() throws Exception
	{
			return getDatasets();
	}

	@Override
	public void setProperties() throws Exception
	{
		this.addAttr( "item_revision_id", (String) this.getObj().getAttributeValue( "revision" ));
		this.addAttr( "object_name", (String) this.getObj().getAttributeValue( "name" ));
	}
	
	private void addObjString()
	{
		ICadexGenericObject lineItem = ((ICadexGenericObject) getRevItem());
		if(lineItem!=null)
		{
			String revId = this.getAttr( "item_revision_id" ) ;
			String objName = lineItem.getAttr( "object_name" ) ;
			String itemId = lineItem.getAttr( "item_id" ) ;
			addAttr("object_string", itemId + "/" + revId + ";" + objName);
		}
	}
	
	@Override
	public ICadexGenericItem getItem() throws Exception
	{
		return (ICadexGenericItem) new PlmxmlItem((Product) this.getObj().getMasterHandle().getObject());
	}
	
	public ArrayList<ICadexGenericDataset> getDatasets() throws Exception
	{
		ArrayList<ICadexGenericDataset> datasets = new ArrayList<ICadexGenericDataset>();
		ObjectBase[] baseObj = this.getObj().getElementsByClass("AssociatedDataSet");
		
		if (baseObj.length > 0)
		{			
			for(int i = 0; i < baseObj.length; i++) 
			{
				AssociatedDataSet assDset = (AssociatedDataSet)baseObj[i];
				ObjectBase objBase = assDset.getDataSetHandle().getObject();
				if (objBase != null)
				{
					PlmxmlDataset dset = new PlmxmlDataset( (DataSet) objBase, assDset.getAttributeValueAsString( "role" ) ) ;
					dset.setRelNode(assDset);
					datasets.add( dset ); 
				}
			}
		}
		
		return datasets;
	}

	@Override
	public boolean doDeepCompare(ICadexComparable obj) {
		// TODO Auto-generated method stub
		return super.doDeepCompare(obj);
	}

	@Override
	public boolean isNew() {
		// TODO Auto-generated method stub
		return !applicationName.equals( PlmxmlUtil.TEAMCENTER_APP_NAME ) ? true : false;
	}

	@Override
	public String getUid() {
		// TODO Auto-generated method stub
		return getAppRefVersion();
	}

	@Override
	public String getTagName() {
		// TODO Auto-generated method stub
		return PlmxmlUtil.PLMXML_PRODUCT_REVISION;
	}

	@Override
	public String getAppRefLabel() {
		// TODO Auto-generated method stub
		return appRefLabel;
	}

	@Override
	public String getAppRefVersion() {
		// TODO Auto-generated method stub
		return appRefVersion;
	}

	@Override
	public String getAppRefName() {
		// TODO Auto-generated method stub
		return applicationName;
	}

	@Override
	public void setAppRefLabel(String a) {
		this.appRefLabel = a;
		
	}

	@Override
	public void setAppRefVersion(String a) {
		this.appRefVersion = a;
	}

	@Override
	public void setAppRefName(String a) {
		this.applicationName = a;
	}

	@Override
	public DescriptionObject getDescObj() {
		// TODO Auto-generated method stub
		return getObj();
	}

	@Override
	public Object[] getChildren() {
		ArrayList<ICadexComparable> allChilds = new ArrayList<ICadexComparable>();
		
		for(ICadexGenericDataset line : this.getAttaches())
		{
			allChilds.add( line );
		}
		
		try {
			allChilds.add( getRevItem() );
		} catch (Exception e) {
			// Needs Proper Handling
			e.printStackTrace();
		}
		
		return allChilds.toArray();
	}

	@Override
	public Image getImage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return getUid();
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "ItemRevision";
	}

	@Override
	public int getChildrenCount() {
		Object[] objs = this.getChildren();
		return objs != null ? objs.length : 0 ;
	}

	@Override
	public void generateDelta() {
		// TODO Auto-generated method stub
		
	}

}

package cadexstructurecompare.model.plmxml;

import org.plmxml.sdk.plmxml.ObjectBase;
import org.plmxml.sdk.plmxml.PLMXMLException;
import org.plmxml.sdk.plmxml.plmxml60.ApplicationRef;
import org.plmxml.sdk.plmxml.plmxml60.UserData;
import org.plmxml.sdk.plmxml.plmxml60.UserValue;

import cadexstructurecompare.model.interfaces.ICadexGenericObject;

public class PlmxmlUtil {
	
	public static String TEAMCENTER_APP_NAME = "Teamcenter";
	public static String THIRD_PARTY_APP = "Cadex";
	public static String PLMXML_PRODUCT_REVISION = "ProductRevision";
	public static String PLMXML_PRODUCT = "Product";
	public static String PLMXML_DATASET = "DataSet";
	public static String PLMXML_FILE = "ExternalFile";
	public static String PLMXML_OCCURRENCE = "Occurrence";
	public static boolean CONSIDER_MISSING_ATTRIBUTES_AS_CHANGE = false;
	
	public static void addUserData( ICadexGenericObject obj , ObjectBase tag ) throws Exception
	{
		ObjectBase[] userDatas = tag.getElementsByClass( "UserData" );
		for (int i = 0; i < userDatas.length; i++) 
		{
			UserValue[] userValues = ((UserData)userDatas[i]).getUserValues();
			
			for (int j = 0; j < userValues.length; j++) {
				obj.addAttr(userValues[j].getTitle(), userValues[j].getValue());
			}
			
		}
	}
	
	public static void populateAppRef(IPlmxmlGenericObject plmxmlObject) throws NoSuchFieldException, IllegalArgumentException, ClassCastException, PLMXMLException
	{
		ApplicationRef[] appRef =  plmxmlObject.getDescObj().getApplicationRefs();
		
		if(appRef.length == 1)
		{
			plmxmlObject.setAppRefLabel(appRef[0].getLabel());
			plmxmlObject.setAppRefVersion(appRef[0].getVersion());
			plmxmlObject.setAppRefName(appRef[0].getApplication());
		}
	}
	
	
}

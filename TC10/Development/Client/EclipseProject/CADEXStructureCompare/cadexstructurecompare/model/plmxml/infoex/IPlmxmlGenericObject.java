package cadexstructurecompare.model.plmxml.infoex;

import com.infoex.plmxml.types.DescriptionBase;

public interface IPlmxmlGenericObject 
{

	public String getAppRefLabel();
	public String getAppRefVersion();
	public String getAppRefName();
	public void setAppRefLabel(String a);
	public void setAppRefVersion(String a);
	public void setAppRefName(String a);
	public DescriptionBase getDescObj();
	
}

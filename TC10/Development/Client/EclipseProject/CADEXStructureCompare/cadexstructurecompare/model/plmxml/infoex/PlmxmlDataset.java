package cadexstructurecompare.model.plmxml.infoex;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.swt.graphics.Image;
import cadexstructurecompare.model.CadexGenericDataset;
import cadexstructurecompare.model.interfaces.ICadexComparable;
import cadexstructurecompare.model.interfaces.ICadexGenericFile;

import com.infoex.plmxml.generic.IdBase;
import com.infoex.plmxml.generic.PLMXMLHelper;
import com.infoex.plmxml.types.AssociatedDataSet;
import com.infoex.plmxml.types.DataSet;
import com.infoex.plmxml.types.ExternalFile;
import com.infoex.plmxml.types.DescriptionBase;

public class PlmxmlDataset extends CadexGenericDataset<DataSet> implements IPlmxmlGenericObject {

	protected String applicationName = "";
	protected String appRefLabel = "";
	protected String appRefVersion = "";
	protected AssociatedDataSet relNode = null;
	
	public PlmxmlDataset(DataSet obj, String relation) throws Exception {
		super(obj,relation);
		PlmxmlUtil.populateAppRef(this);
	}

	@Override
	public void setProperties() throws Exception
	{
		this.addAttr( "object_name" , (String) this.getObj().getName() );
		this.addAttr( "object_type" , (String) this.getObj().getType() );
		this.addAttr( "version" , this.getObj().getVersion().toString() );
		PlmxmlUtil.addUserData(this, this.getObj());
		this.addAttr( "object_string" , this.getAttr("object_name")  );
	}

	@Override
	public ArrayList<ICadexGenericFile> populateFiles() throws Exception{
		
		ArrayList<ICadexGenericFile> files = new ArrayList<ICadexGenericFile>();
		List<IdBase> fileElements = PLMXMLHelper.getHandles(this.getObj(), this.getObj().getMemberRefs());
		
		if (fileElements != null && fileElements.size() > 0)
		{			
			for(IdBase fileElement : fileElements) 
			{
				files.add( new PlmxmlFile( (ExternalFile) fileElement ) ); 
			}
		}
		
		return files;
		
	}

	@Override
	public String getType() {
		return "Dataset";
	}

	@Override
	public boolean doDeepCompare(ICadexComparable obj) {
		// TODO Auto-generated method stub
		return super.doDeepCompare(obj);
	}

	@Override
	public boolean isNew() {
		// TODO Auto-generated method stub
		return !applicationName.equals( PlmxmlUtil.TEAMCENTER_APP_NAME ) ? true : false;
	}

	@Override
	public String getUid() {
		// TODO Auto-generated method stub
		return getAppRefLabel();
	}

	@Override
	public String getTagName() {
		// TODO Auto-generated method stub
		return PlmxmlUtil.PLMXML_DATASET;
	}

	@Override
	public String getAppRefLabel() {
		// TODO Auto-generated method stub
		return appRefLabel;
	}

	@Override
	public String getAppRefVersion() {
		// TODO Auto-generated method stub
		return appRefVersion;
	}

	@Override
	public String getAppRefName() {
		// TODO Auto-generated method stub
		return applicationName;
	}

	@Override
	public void setAppRefLabel(String a) {
		this.appRefLabel = a;
		
	}

	@Override
	public void setAppRefVersion(String a) {
		this.appRefVersion = a;
	}

	@Override
	public void setAppRefName(String a) {
		this.applicationName = a;
	}

	@Override
	public DescriptionBase getDescObj() {
		// TODO Auto-generated method stub
		return getObj();
	}

	@Override
	public Image getImage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return getUid();
	}

	@Override
	public Object[] getChildren() 
	{
		ArrayList<ICadexComparable> allChilds = new ArrayList<ICadexComparable>();
		
		for(ICadexGenericFile line : this.getFiles())
		{
			allChilds.add( line );
		}
		
		return allChilds.toArray();
	}

	@Override
	public int getChildrenCount() {
		Object[] objs = this.getChildren();
		return objs != null ? objs.length : 0 ;
	}


	@Override
	public void generateDelta() {
		// TODO Auto-generated method stub
		
	}

	public AssociatedDataSet getRelNode() {
		return relNode;
	}

	public void setRelNode(AssociatedDataSet relNode) {
		this.relNode = relNode;
	}

}

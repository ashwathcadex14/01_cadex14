package cadexstructurecompare.model.plmxml.infoex;

import org.eclipse.swt.graphics.Image;
import cadexstructurecompare.model.CadexGenericItem;
import cadexstructurecompare.model.interfaces.ICadexComparable;
import com.infoex.plmxml.types.DescriptionBase;
import com.infoex.plmxml.types.Product;

public class PlmxmlItem extends CadexGenericItem<Product> implements IPlmxmlGenericObject {

	protected String applicationName = "";
	protected String appRefLabel = "";
	protected String appRefVersion = "";
	
	public PlmxmlItem(Product item) throws Exception {
		super(item);
		PlmxmlUtil.populateAppRef(this);
		addObjString();
	}

	@Override
	public void setProperties() throws Exception{
		this.addAttr( "item_id", (String) this.getObj().getProductId());
		this.addAttr( "object_name", (String) this.getObj().getName());
	}

	@Override
	public boolean doDeepCompare(ICadexComparable obj) {
		// TODO Auto-generated method stub
		return super.doDeepCompare(obj);
	}
	
	private void addObjString()
	{
		String objName = this.getAttr( "object_name" ) ;
		addAttr("object_string", getAttr( "item_id" )+ ";" + objName);
	}
	
	@Override
	public boolean isNew() {
		// TODO Auto-generated method stub
		return !applicationName.equals( PlmxmlUtil.TEAMCENTER_APP_NAME ) ? true : false;
	}

	@Override
	public String getUid() {
		// TODO Auto-generated method stub
		return getAppRefLabel();
	}

	@Override
	public String getTagName() {
		// TODO Auto-generated method stub
		return PlmxmlUtil.PLMXML_PRODUCT;
	}

	@Override
	public String getAppRefLabel() {
		// TODO Auto-generated method stub
		return appRefLabel;
	}

	@Override
	public String getAppRefVersion() {
		// TODO Auto-generated method stub
		return appRefVersion;
	}

	@Override
	public String getAppRefName() {
		// TODO Auto-generated method stub
		return applicationName;
	}

	@Override
	public void setAppRefLabel(String a) {
		this.appRefLabel = a;
		
	}

	@Override
	public void setAppRefVersion(String a) {
		this.appRefVersion = a;
	}

	@Override
	public void setAppRefName(String a) {
		this.applicationName = a;
	}

	@Override
	public DescriptionBase getDescObj() {
		// TODO Auto-generated method stub
		return getObj();
	}

	@Override
	public Image getImage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return getUid();
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "Item";
	}

	@Override
	public Object[] getChildren() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getChildrenCount() {
		Object[] objs = this.getChildren();
		return objs != null ? objs.length : 0 ;
	}


	@Override
	public void generateDelta() {
		// TODO Auto-generated method stub
		
	}

}

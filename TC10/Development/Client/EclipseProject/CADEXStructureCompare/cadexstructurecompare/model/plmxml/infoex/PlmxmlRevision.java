package cadexstructurecompare.model.plmxml.infoex;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.swt.graphics.Image;
import cadexstructurecompare.model.CadexGenericRevision;
import cadexstructurecompare.model.interfaces.ICadexComparable;
import cadexstructurecompare.model.interfaces.ICadexGenericDataset;
import cadexstructurecompare.model.interfaces.ICadexGenericItem;
import cadexstructurecompare.model.interfaces.ICadexGenericObject;
import com.infoex.plmxml.generic.PLMXMLHelper;
import com.infoex.plmxml.types.AssociatedDataSet;
import com.infoex.plmxml.types.DataSet;
import com.infoex.plmxml.types.DescriptionBase;
import com.infoex.plmxml.types.ProductRevision;
import com.infoex.plmxml.types.Product;

public class PlmxmlRevision extends CadexGenericRevision<ProductRevision> implements IPlmxmlGenericObject{

	protected String applicationName = "";
	protected String appRefLabel = "";
	protected String appRefVersion = "";
	
	public PlmxmlRevision(ProductRevision obj) throws Exception {
		super(obj);
		PlmxmlUtil.populateAppRef(this);
		addObjString();
	}

	@Override
	public ArrayList<ICadexGenericDataset> getAttachments() throws Exception
	{
			return getDatasets();
	}

	@Override
	public void setProperties() throws Exception
	{
		this.addAttr( "item_revision_id", (String) this.getObj().getRevision());
		this.addAttr( "object_name", (String) this.getObj().getName());
	}
	
	private void addObjString()
	{
		ICadexGenericObject lineItem = ((ICadexGenericObject) getRevItem());
		if(lineItem!=null)
		{
			String revId = this.getAttr( "item_revision_id" ) ;
			String objName = lineItem.getAttr( "object_name" ) ;
			String itemId = lineItem.getAttr( "item_id" ) ;
			addAttr("object_string", itemId + "/" + revId + ";" + objName);
		}
	}
	
	@Override
	public ICadexGenericItem getItem() throws Exception
	{
		return (ICadexGenericItem) new PlmxmlItem( (Product) PLMXMLHelper.resolveId(this.getObj(), this.getObj().getMasterRef()) );
	}
	
	public ArrayList<ICadexGenericDataset> getDatasets() throws Exception
	{
		ArrayList<ICadexGenericDataset> datasets = new ArrayList<ICadexGenericDataset>();
		List<AssociatedDataSet> relations = PLMXMLHelper.getAssociatedDatasets( this.getObj() );	
		if (relations!=null && relations.size()>0)
		{			
			for(AssociatedDataSet relation : relations) 
			{
				DataSet dataset = (DataSet) PLMXMLHelper.resolveId(this.getObj(), relation.getDataSetRef());
				if (dataset != null)
				{
					PlmxmlDataset dset = new PlmxmlDataset( dataset, relation.getRole() ) ;
					dset.setRelNode(relation);
					datasets.add( dset ); 
				}
			}
		}
		
		return datasets;
	}

	@Override
	public boolean doDeepCompare(ICadexComparable obj) {
		// TODO Auto-generated method stub
		return super.doDeepCompare(obj);
	}

	@Override
	public boolean isNew() {
		// TODO Auto-generated method stub
		return !applicationName.equals( PlmxmlUtil.TEAMCENTER_APP_NAME ) ? true : false;
	}

	@Override
	public String getUid() {
		// TODO Auto-generated method stub
		return getAppRefVersion();
	}

	@Override
	public String getTagName() {
		// TODO Auto-generated method stub
		return PlmxmlUtil.PLMXML_PRODUCT_REVISION;
	}

	@Override
	public String getAppRefLabel() {
		// TODO Auto-generated method stub
		return appRefLabel;
	}

	@Override
	public String getAppRefVersion() {
		// TODO Auto-generated method stub
		return appRefVersion;
	}

	@Override
	public String getAppRefName() {
		// TODO Auto-generated method stub
		return applicationName;
	}

	@Override
	public void setAppRefLabel(String a) {
		this.appRefLabel = a;
		
	}

	@Override
	public void setAppRefVersion(String a) {
		this.appRefVersion = a;
	}

	@Override
	public void setAppRefName(String a) {
		this.applicationName = a;
	}

	@Override
	public DescriptionBase getDescObj() {
		// TODO Auto-generated method stub
		return getObj();
	}

	@Override
	public Object[] getChildren() {
		ArrayList<ICadexComparable> allChilds = new ArrayList<ICadexComparable>();
		
		for(ICadexGenericDataset line : this.getAttaches())
		{
			allChilds.add( line );
		}
		
		try {
			allChilds.add( getRevItem() );
		} catch (Exception e) {
			// Needs Proper Handling
			e.printStackTrace();
		}
		
		return allChilds.toArray();
	}

	@Override
	public Image getImage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return getUid();
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "ItemRevision";
	}

	@Override
	public int getChildrenCount() {
		Object[] objs = this.getChildren();
		return objs != null ? objs.length : 0 ;
	}

	@Override
	public void generateDelta() {
		// TODO Auto-generated method stub
		
	}

}

package cadexstructurecompare.model.plmxml.infoex;

import java.util.List;
import javax.xml.bind.JAXBElement;
import org.plmxml.sdk.plmxml.PLMXMLException;
import cadexstructurecompare.model.interfaces.ICadexGenericObject;

import com.infoex.plmxml.types.ApplicationRef;
import com.infoex.plmxml.types.AttribOwnerBase;
import com.infoex.plmxml.types.AttributeBase;
import com.infoex.plmxml.types.UserData;
import com.infoex.plmxml.types.UserDataElement;

public class PlmxmlUtil {
	
	public static String TEAMCENTER_APP_NAME = "Teamcenter";
	public static String THIRD_PARTY_APP = "Cadex";
	public static String PLMXML_PRODUCT_REVISION = "ProductRevision";
	public static String PLMXML_PRODUCT = "Product";
	public static String PLMXML_DATASET = "DataSet";
	public static String PLMXML_FILE = "ExternalFile";
	public static String PLMXML_OCCURRENCE = "Occurrence";
	public static boolean CONSIDER_MISSING_ATTRIBUTES_AS_CHANGE = false;
	
	public static void addUserData( ICadexGenericObject obj , AttribOwnerBase tag ) throws Exception
	{
		List<JAXBElement<? extends AttributeBase>> userDatas = tag.getAttribute();
		
		for (JAXBElement<? extends AttributeBase> attrNode : userDatas) 
		{
			if( attrNode.getValue() instanceof UserData )
			{
				UserData userData = (UserData) attrNode.getValue();
			
				List<UserDataElement> userValues = userData.getUserValue();
				
				for (UserDataElement userValue : userValues) {
					obj.addAttr(userValue.getTitle(), userValue.getValue());
				}
			}
		}
	}
	
	public static void populateAppRef(IPlmxmlGenericObject plmxmlObject) throws NoSuchFieldException, IllegalArgumentException, ClassCastException, PLMXMLException
	{
		List<ApplicationRef> appRefs = plmxmlObject.getDescObj().getApplicationRef();
		
		if(appRefs.size() == 1)
		{
			ApplicationRef appRef = appRefs.get(0);
			plmxmlObject.setAppRefLabel(appRef.getLabel());
			plmxmlObject.setAppRefVersion(appRef.getVersion());
			plmxmlObject.setAppRefName(appRef.getApplication());
		}
	}
	
	
}

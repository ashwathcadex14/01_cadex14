package cadexstructurecompare.views;

import java.util.List;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableColorProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.wb.swt.ResourceManager;

import cadexstructurecompare.model.interfaces.ICadexComparable;
import cadexstructurecompare.views.structurecompare.CompareNode;

public class DatasetsView extends Composite implements ISelectionChangedListener{
	
	public DatasetsView(Composite parent, int style) {
		super(parent, style);
		createPartControl(this);
	}

	TreeViewer treeViewer;
	
	
	private static Image ITEM = ResourceManager.getPluginImage("CADEXStructureCompare", "cadexstructurecompare/images/Item.png") ;
	private static Image ITEM_NEW = ResourceManager.getPluginImage("CADEXStructureCompare", "cadexstructurecompare/images/Item_New.png") ;
	private static Image ITEM_DELETED = ResourceManager.getPluginImage("CADEXStructureCompare", "cadexstructurecompare/images/Item_Deleted.png") ;
	private static Image ITEM_MODIFIED = ResourceManager.getPluginImage("CADEXStructureCompare", "cadexstructurecompare/images/Item_Modified.png") ;
	public static String ID = "CADEXStructureCompare.views.datasetsView" ;
	
	public void createPartControl(Composite parent) {
		parent.setLayout(new GridLayout(1, false));
		
		treeViewer = new TreeViewer(parent, SWT.BORDER);
		Tree tree = treeViewer.getTree();
		tree.setLinesVisible(true);
		tree.setHeaderVisible(true);
		tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		TreeViewerColumn treeViewerColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		TreeColumn trclmnObject = treeViewerColumn.getColumn();
		trclmnObject.setWidth(242);
		trclmnObject.setText("Object");
		
		TreeViewerColumn treeViewerColumn_1 = new TreeViewerColumn(treeViewer, SWT.NONE);
		TreeColumn trclmnType = treeViewerColumn_1.getColumn();
		trclmnType.setWidth(171);
		trclmnType.setText("Type");
		
		TreeViewerColumn treeViewerColumn_2 = new TreeViewerColumn(treeViewer, SWT.NONE);
		TreeColumn trclmnRelation = treeViewerColumn_2.getColumn();
		trclmnRelation.setWidth(167);
		trclmnRelation.setText("Change Info");

		treeViewer.setContentProvider( new DatasetsViewContentProvider() );
		treeViewer.setLabelProvider( new DatasetsViewLabelProvider() );
		treeViewer.setAutoExpandLevel( 4 );
	}
	@Override
	public boolean setFocus() {
		return false;
		// TODO Auto-generated method stub

	}
	
	private class DatasetsViewContentProvider implements ITreeContentProvider
	{
		@SuppressWarnings("unused")
		CompareNode selectNode = null;
		
		public DatasetsViewContentProvider() {
			// TODO Auto-generated constructor stub
		}
		
		@Override
		public void dispose() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
					this.selectNode = (CompareNode) newInput;
		}

		@Override
		public Object[] getElements(Object inputElement) {
//			return ((CompareNode)inputElement).getDatasetNodes().toArray();
			List<CompareNode> list = ((CompareNode)inputElement).getChildDetailNodes();
			return list != null ? list.toArray() : new Object[0];
		}

		@Override
		public Object[] getChildren(Object parentElement) {
//			if(parentElement instanceof DatasetNode)
//				return ((DatasetNode) parentElement).getDsets().toArray();
//			else if(parentElement instanceof ICadexGenericDataset)
//				return ((ICadexGenericDataset) parentElement).getFiles().toArray();
			
			return ((CompareNode)parentElement).getChildNodes().toArray();
		}

		@Override
		public Object getParent(Object element) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean hasChildren(Object element) {
//			if(element instanceof DatasetNode)
//				return ((DatasetNode) element).getDsets().size() > 0;
//			else if(element instanceof ICadexGenericDataset)
//				return ((ICadexGenericDataset) element).getFiles().size() > 0;
//			else
//				return false;
			
			return ((CompareNode)element).getChildNodes().size() > 0;
		}
		
	}
	
	private class DatasetsViewLabelProvider implements ITableLabelProvider, ITableColorProvider
	{
		
		public DatasetsViewLabelProvider() {
			// TODO Auto-generated constructor stub
		}
		
		@Override
		public void addListener(ILabelProviderListener listener) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void dispose() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean isLabelProperty(Object element, String property) {
			// TODO Auto-generated method stub
			return true;
		}

		@Override
		public void removeListener(ILabelProviderListener listener) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			if (columnIndex==0) 
			{
				switch (((CompareNode) element).getDelta()) 
				{
					case NEW:
						return ITEM_NEW;
					case REMOVED:
						return ITEM_DELETED;
					case MODIFIED:
						return ITEM_MODIFIED;
					default:
						return ITEM;
				}
			}
			else
				return null;
		}

		@Override
		public String getColumnText(Object element, int columnIndex) {
			
//			if(element instanceof DatasetNode)
//				return columnIndex == 0 ? ((DatasetNode) element).getSideName() : "";
//			else if(element instanceof ICadexGenericDataset)
//			{
//				ICadexGenericDataset dset = ((ICadexGenericDataset) element);
//				return columnIndex == 0 ? dset.getAttr( "object_name" ) : columnIndex == 1 ? dset.getAttr( "object_type" ) : dset.getRelation() ;
//			}
//			else
//				return columnIndex == 0 ? ((ICadexGenericFile) element).getOriginalFileName() : "";
		
			CompareNode cmpNode = (CompareNode) element;
			
			ICadexComparable cmp = null;
			
			if(cmpNode != null)
			{
				if(cmpNode.getLeftNode() != null)
				{
					cmp = cmpNode.getLeftNode();
				}
				else if( cmpNode.getRightNode() != null )
				{
					cmp = cmpNode.getRightNode();
				}
			}
			
			if(columnIndex == 0)
				return  cmp.getObjectString();
			else
				return "";
		}

		@Override
		public Color getBackground(Object arg0, int arg1) 
		{
//			switch (((CompareNode)arg0).getDelta()) 
//			{
//			case NEW:
//				return new Color(Display.getCurrent(), 51,153, 0);
//			case REMOVED:
//				return new Color(Display.getCurrent(), 255, 51, 0);
//			case MODIFIED:
//				return new Color(Display.getCurrent(), 255, 255, 0);
//			default:
//				return null ;
//			}
			return null;
		}

		@Override
		public Color getForeground(Object arg0, int arg1) {
			// TODO Auto-generated method stub
			return null;
		}
		
	}
	
	@Override
	public void selectionChanged(SelectionChangedEvent event) {
			IStructuredSelection selection = (IStructuredSelection) event.getSelection();
			CompareNode line = (CompareNode)selection.getFirstElement();
			treeViewer.setInput( line );
		}

}

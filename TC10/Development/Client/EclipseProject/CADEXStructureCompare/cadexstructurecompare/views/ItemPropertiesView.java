package cadexstructurecompare.views;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;

import cadexstructurecompare.views.structurecompare.AttributeNode;
import cadexstructurecompare.views.structurecompare.CompareNode;

public class ItemPropertiesView extends Composite implements ISelectionChangedListener {
	
	public ItemPropertiesView(Composite parent, int style) {
		super(parent, style);
		createPartControl(this);
	}


	TreeViewer treeViewer;


	public static String ID = "CADEXStructureCompare.views.itemProps" ;
	
	public void createPartControl(Composite parent) {
		parent.setLayout(new GridLayout(1, false));
		
		treeViewer = new TreeViewer(parent, SWT.BORDER);
		Tree tree = treeViewer.getTree();
		tree.setLinesVisible(true);
		tree.setHeaderVisible(true);
		tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		TreeViewerColumn treeViewerColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		TreeColumn trclmnAttribute = treeViewerColumn.getColumn();
		trclmnAttribute.setWidth(158);
		trclmnAttribute.setText("Attribute");
		
		TreeViewerColumn treeViewerColumn_2 = new TreeViewerColumn(treeViewer, SWT.NONE);
		TreeColumn trclmnLeft = treeViewerColumn_2.getColumn();
		trclmnLeft.setWidth(155);
		trclmnLeft.setText("Left");
		
		TreeViewerColumn treeViewerColumn_3 = new TreeViewerColumn(treeViewer, SWT.NONE);
		TreeColumn trclmnRight = treeViewerColumn_3.getColumn();
		trclmnRight.setWidth(179);
		trclmnRight.setText("Right");
		
		TreeViewerColumn treeViewerColumn_1 = new TreeViewerColumn(treeViewer, SWT.NONE);
		TreeColumn trclmnAncestor = treeViewerColumn_1.getColumn();
		trclmnAncestor.setWidth(152);
		trclmnAncestor.setText("Ancestor");
		
		treeViewer.setContentProvider( new ItemPropertiesContentProvider() );
		treeViewer.setLabelProvider( new ItemPropertiesLabelProvider() );

		treeViewer.setAutoExpandLevel( 2 );
	}

	@Override
	public boolean setFocus() {
		return false;
		// TODO Auto-generated method stub

	}
	
	private class ItemPropertiesContentProvider implements ITreeContentProvider
	{
		@SuppressWarnings("unused")
		CompareNode selectNode = null;
		
		public ItemPropertiesContentProvider() {
			// TODO Auto-generated constructor stub
		}
		
		@Override
		public void dispose() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
					this.selectNode = (CompareNode) newInput;
		}

		@Override
		public Object[] getElements(Object inputElement) {
			return ((CompareNode)inputElement).getAttrNodes().toArray();
		}

		@Override
		public Object[] getChildren(Object parentElement) {
//			if(parentElement instanceof AttributeNode)
//				return ((AttributeNode) parentElement).getChildNodes().toArray();
			
			return null;
		}

		@Override
		public Object getParent(Object element) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean hasChildren(Object element) {
			return false;
		}
		
	}
	
	private class ItemPropertiesLabelProvider implements ITableLabelProvider
	{

		public ItemPropertiesLabelProvider() {
			// TODO Auto-generated constructor stub
		}
		
		@Override
		public void addListener(ILabelProviderListener listener) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void dispose() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean isLabelProperty(Object element, String property) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void removeListener(ILabelProviderListener listener) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getColumnText(Object element, int columnIndex) {
			
			if(element instanceof AttributeNode)
			{
				switch (columnIndex) 
				{
					case 0:
						return ((AttributeNode)element).getAttrName();
					case 1:
						return ((AttributeNode)element).getAttrValueL();
					case 2:
						return ((AttributeNode)element).getAttrValueR();
					default:
						return ((AttributeNode)element).getAttrValueA();
				}
				
			}
				
			return null;
		}
		
	}


	@Override
	public void selectionChanged(SelectionChangedEvent event) {
			IStructuredSelection selection = (IStructuredSelection) event.getSelection();
			CompareNode line = (CompareNode)selection.getFirstElement();
			treeViewer.setInput( line );
		}

}

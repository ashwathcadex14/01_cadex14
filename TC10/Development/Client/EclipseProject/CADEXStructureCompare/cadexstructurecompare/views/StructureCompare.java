package cadexstructurecompare.views;
import java.util.ArrayList;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.progress.IProgressConstants;
import org.eclipse.wb.swt.ResourceManager;

import cadexstructurecompare.ui.CELoadingComposite;
import cadexstructurecompare.utils.CadexUtils;
import cadexstructurecompare.views.structurecompare.CadexCompareContentProvider;
import cadexstructurecompare.views.structurecompare.CadexCompareEditorInput;
import cadexstructurecompare.views.structurecompare.CadexCompareLabelProvider;
import cadexstructurecompare.views.structurecompare.plmxml.InfoExPlmxmlCompareEditorInput;
//import cadexstructurecompare.views.structurecompare.plmxml.PlmxmlCompareEditorInput;


public class StructureCompare extends ViewPart implements SelectionListener{
	
	public static String ID = "CADEXStructureCompare.views.cadexCompare";
	CELoadingComposite loadMidComposite;
	TreeViewer treeViewerLeft;
	TreeViewer treeViewerRight;
	Composite window = null;
	CadexCompareEditorInput newInput;
	
	public StructureCompare() {
	}

	@Override
	public void createPartControl(Composite parent) {
		
		window = parent;
		parent.setLayout(new GridLayout(1, false));
		
		loadMidComposite = new CELoadingComposite(parent, SWT.NONE);
		
		SashForm sashForm = new SashForm(loadMidComposite.otherComposite, SWT.NONE);
		sashForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		Composite compositeAncestor = new Composite(sashForm, SWT.NONE);
		compositeAncestor.setLayout(new GridLayout(1, false));
		compositeAncestor.setVisible( false );
		
		TreeViewer treeViewerAncestor = new TreeViewer(compositeAncestor, SWT.BORDER);
		Tree treeAncestor = treeViewerAncestor.getTree();
		treeAncestor.setSize(322, 681);
		treeAncestor.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		treeAncestor.setLinesVisible(true);
		treeAncestor.setHeaderVisible(true);
		treeAncestor.setEnabled(false);
		
		TreeViewerColumn treeViewerColumn = new TreeViewerColumn(treeViewerAncestor, SWT.NONE);
		TreeColumn trclmnObject = treeViewerColumn.getColumn();
		trclmnObject.setWidth(249);
		trclmnObject.setText("Object");
		
		Composite compositeLeft = new Composite(sashForm, SWT.NONE);
		compositeLeft.setLayout(new GridLayout(1, false));
		compositeLeft.setVisible(true);
		
		treeViewerLeft = new TreeViewer(compositeLeft, SWT.BORDER);
		Tree treeLeft = treeViewerLeft.getTree();
		treeLeft.setSize(92, 55);
		GridData gd_treeLeft = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_treeLeft.heightHint = 158;
		treeLeft.setLayoutData(gd_treeLeft);
		treeLeft.setLinesVisible(true);
		treeLeft.setHeaderVisible(true);
		
		
		TreeViewerColumn treeViewerColumn_1 = new TreeViewerColumn(treeViewerLeft, SWT.NONE);
		TreeColumn trclmnObject_1 = treeViewerColumn_1.getColumn();
		trclmnObject_1.setWidth(235);
		trclmnObject_1.setText("Object");
		
				treeViewerLeft.getTree().getVerticalBar().addSelectionListener(this);
				
				treeViewerLeft.getTree().addSelectionListener( this );
				
				treeViewerLeft.getTree().addListener( SWT.Expand , new Listener() {
					
					@Override
					public void handleEvent(Event event) {
						TreeItem expandedItem = (TreeItem) event.item;
						
						treeViewerRight.expandToLevel( expandedItem.getData() , 1);
					}
				});
				
				treeViewerLeft.getTree().addListener( SWT.Collapse , new Listener() {
					
					@Override
					public void handleEvent(Event event) {
						TreeItem expandedItem = (TreeItem) event.item;
						
						treeViewerRight.collapseToLevel( expandedItem.getData() , 1);
					}
				});
				
				
		
		Composite compositeRight = new Composite(sashForm, SWT.NONE);
		compositeRight.setLayout(new GridLayout(1, false));
		
		treeViewerRight = new TreeViewer(compositeRight, SWT.BORDER);
		Tree treeRight = treeViewerRight.getTree();
		GridData gd_treeRight = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_treeRight.heightHint = 158;
		treeRight.setLayoutData(gd_treeRight);
		treeRight.setSize(92, 55);
		treeRight.setLinesVisible(true);
		treeRight.setHeaderVisible(true);
		
		TreeViewerColumn treeViewerColumn_2 = new TreeViewerColumn(treeViewerRight, SWT.NONE);
		TreeColumn trclmnObject_2 = treeViewerColumn_2.getColumn();
		trclmnObject_2.setWidth(259);
		trclmnObject_2.setText("Object");
		sashForm.setWeights(new int[] {1, 1, 1});
		GridData gd_loadMidComposite = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_loadMidComposite.heightHint = 172;
		loadMidComposite.setLayoutData(gd_loadMidComposite);
		treeViewerRight.getTree().getVerticalBar().addSelectionListener(this);
		treeViewerRight.getTree().addSelectionListener( this );
		
		treeViewerRight.getTree().addListener( SWT.Expand , new Listener() {
			
			@Override
			public void handleEvent(Event event) {
				TreeItem expandedItem = (TreeItem) event.item;
				
				treeViewerLeft.expandToLevel( expandedItem.getData() , 1);
			}
		});
		
		treeViewerRight.getTree().addListener( SWT.Collapse , new Listener() {
			
			@Override
			public void handleEvent(Event event) {
				TreeItem expandedItem = (TreeItem) event.item;
				
				treeViewerLeft.collapseToLevel( expandedItem.getData() , 1);
			}
		});
		
		CadexCompareContentProvider contentProvider = new CadexCompareContentProvider();
		
		
		
		
		treeViewerLeft.setContentProvider( contentProvider );
		treeViewerLeft.setLabelProvider( new CadexCompareLabelProvider(1) );
		
		treeViewerRight.setContentProvider( contentProvider );
		treeViewerRight.setLabelProvider( new CadexCompareLabelProvider(2) );
		
		
		//ItemPropertiesView propsView = (ItemPropertiesView) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findViewReference( ItemPropertiesView.ID ).getView(true);
		//DatasetsView datasetsView = (DatasetsView) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findViewReference( DatasetsView.ID ).getView(true);
		
		Composite composite = new Composite(loadMidComposite.otherComposite, SWT.NONE);
		composite.setLayout(new GridLayout(2, false));
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		DatasetsView datasetsView = new DatasetsView(composite, SWT.NONE);
		datasetsView.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		ItemPropertiesView propsView = new ItemPropertiesView(composite, SWT.NONE);
		propsView.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		
		
		treeViewerLeft.addSelectionChangedListener( propsView );
		treeViewerRight.addSelectionChangedListener( propsView );
		
		getSite().setSelectionProvider(treeViewerLeft);
		getSite().setSelectionProvider(treeViewerRight);
		treeViewerLeft.addSelectionChangedListener( datasetsView );
		treeViewerRight.addSelectionChangedListener( datasetsView );
	}

	@Override
	public void widgetSelected(SelectionEvent e) {
		
		if( e.getSource().equals(treeViewerLeft.getTree().getVerticalBar()) )
			onTreeVerticalScrollSelected(e, treeViewerLeft, treeViewerRight);
		else if( e.getSource().equals(treeViewerRight.getTree().getVerticalBar()) )
			onTreeVerticalScrollSelected(e, treeViewerRight, treeViewerLeft);
		else if( e.getSource().equals( treeViewerLeft.getTree() ))
			treeViewerRight.setSelection( treeViewerLeft.getSelection() );
		else if( e.getSource().equals( treeViewerRight.getTree() ))
			treeViewerLeft.setSelection( treeViewerRight.getSelection() );	
			
	}
	
	private void onTreeVerticalScrollSelected(SelectionEvent e, TreeViewer target, TreeViewer other)
	{			
		TreeItem targetItem = target.getTree().getTopItem();				
		TreeItem otherItem = getOtherItem(targetItem, target, other);
		
		if( otherItem != null )
			other.getTree().setTopItem(otherItem);
	}
	
	private TreeItem getOtherItem(TreeItem targetItem, TreeViewer target, TreeViewer other)
	{	
		ArrayList<TreeItem> targetVisibleItems = getAllVisibleItems(target);
		ArrayList<TreeItem> otherVisibleItems = getAllVisibleItems(other);
		
		int targetIndex = targetVisibleItems.indexOf(targetItem);
		if( targetIndex != -1 && otherVisibleItems.size() > targetIndex )
			return (TreeItem)otherVisibleItems.get(targetIndex);
		
		return (TreeItem)otherVisibleItems.get(otherVisibleItems.size() - 1);
	}
	
	public ArrayList<TreeItem> getAllVisibleItems(TreeViewer treeViewer)
	{
		ArrayList<TreeItem> result = new ArrayList<TreeItem>();
		getVisibleItems(result, treeViewer.getControl());
		return result;
	}
	
	private void getVisibleItems(ArrayList<TreeItem> result, Widget widget)
	{
		TreeItem[] items = getChildren(widget);
	    
		for(int i=0; i<items.length; i++)
	    {
			TreeItem item = items[i];
			result.add(item);          
			if( item.getExpanded() )
				getVisibleItems(result, item);
	    }
	}
	
	private TreeItem[] getChildren(Widget o)
	{
		if( o instanceof TreeItem )
			return ((TreeItem)o).getItems();
		if( o instanceof Tree )
			return ((Tree)o).getItems();
		return null;
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
		
	}
	
	public void startCompare(final String file1, final String file2)
	{
		final Job engJob = new Job( "Comparing Structures...." ) {
			
			@Override
			protected IStatus run(final IProgressMonitor monitor) 
			{
				try 
				{
//					newInput = new PlmxmlCompareEditorInput(file1, file2);
//					newInput.initializeInput(monitor);
					
					newInput = new InfoExPlmxmlCompareEditorInput(file1, file2);
					newInput.initializeInput(monitor);
					
					Display.getDefault().syncExec( new Runnable() {
						
						@Override
						public void run() {
							treeViewerLeft.setInput( newInput );
							treeViewerRight.setInput( newInput );
						}
					} );		
				} 
				catch (final Exception e)
				{
					Display.getDefault().syncExec( new Runnable() {
						
						@Override
						public void run() {
							CadexUtils.openError( window.getShell() , "Compare Problem", "Problem occured while comparing two files." , e);	
						}
					} );
				} 
				
				return Status.OK_STATUS ;
			}
		};
		
		
		engJob.setProperty(IProgressConstants.KEEP_PROPERTY, Boolean.TRUE);
		engJob.setProperty(IProgressConstants.ICON_PROPERTY, ResourceManager.getPluginImageDescriptor( "com.teamcenter.siemens.briefcase" , "icons/package_editor.PNG" ));
		engJob.schedule() ;
	}

	public CadexCompareEditorInput getNewInput() {
		return newInput;
	}

}

package cadexstructurecompare.views.structurecompare;

import cadexstructurecompare.model.interfaces.ICadexComparable;
import cadexstructurecompare.model.interfaces.ICadexGenericObject;

/**
 * Attribute Node representing the attributes of the comparable node. It holds attribute
 * of both left and right node.
 * @author Ashwath
 *
 */
public class AttributeNode
{
	String attrName = "" ;
	String attrValueL = "" ;
	String attrValueR = "" ;
	String attrValueA = "" ;
	int attrType ;
	private boolean isSame = false;
	public static final int BOM_ATTR_TYPE = 1;
	public static final int ITEM_ATTR_TYPE = 2;
	public static final int REV_ATTR_TYPE = 3;
//	List<AttributeNode> childNodes = new ArrayList<AttributeNode>();
	
	public AttributeNode(String attrName, CompareNode obj, int type) {
		this(attrName,obj.getLeftNode(),obj.getRightNode(),obj.getAncestorNode());
		this.attrType = type;			
//		setAttributes(obj);
	}
	
//	private void setAttributes(CompareNode obj)
//	{
//		switch(this.attrType)
//		{
//			case BOM_ATTR_TYPE:
//				//setAttribute( obj.getLeftNode() , obj.getRightNode(), obj.getAncestorNode());
//				break;
//			case ITEM_ATTR_TYPE:
//			{
//				//setAttribute( getLineItem(obj.getLeftNode()) , getLineItem(obj.getRightNode()), getLineItem(obj.getAncestorNode()));
//				break;
//			}
//			case REV_ATTR_TYPE:
//			{
//				//setAttribute( getLineRev(obj.getLeftNode()) , getLineRev(obj.getRightNode()), getLineRev(obj.getAncestorNode()));
//				break;
//			}
//			default:
//				break;
//		}
//	}
	
//	private void setAttribute(ICadexComparable iCadexGenericObject, ICadexComparable iCadexGenericObject2, ICadexComparable iCadexGenericObject3 )
//	{
//		Set<String> globNames = new HashSet<String>(); 
//		
//		if(iCadexGenericObject != null)
//			globNames.addAll(((ICadexGenericObject) iCadexGenericObject).getAttrNames());
//		if(iCadexGenericObject2 != null)
//			globNames.addAll(((ICadexGenericObject) iCadexGenericObject2).getAttrNames());
//		if(iCadexGenericObject3 != null)
//			globNames.addAll(((ICadexGenericObject) iCadexGenericObject3).getAttrNames());
//		
//		for(String globString : globNames)
//		{
//			this.childNodes.add( new AttributeNode(globString, iCadexGenericObject, iCadexGenericObject2,iCadexGenericObject3) );
//		}
//	}
	
	public AttributeNode(String attrName) {
		this.attrName = attrName;
	}
	
    public AttributeNode(String attrName, String leftValue, String rightValue, String ancestorValue)
    {
    	this(attrName);
        if (leftValue.equals(rightValue))
            isSame = true;

        attrValueL = leftValue;
        attrValueR = rightValue;
        attrValueA = ancestorValue;
    }
	
	/**
	 * @return the isSame
	 */
	public boolean isSame() {
		return isSame;
	}

	public AttributeNode(String attrName, ICadexComparable iCadexGenericObject, ICadexComparable iCadexGenericObject2, ICadexComparable iCadexGenericObject3) {
		this(attrName);
		this.attrValueL = iCadexGenericObject != null ? ((ICadexGenericObject) iCadexGenericObject).getAttr(attrName) : "" ;
		this.attrValueR = iCadexGenericObject2 != null ? ((ICadexGenericObject) iCadexGenericObject2).getAttr(attrName) : "" ;
		this.attrValueA = iCadexGenericObject3 != null ? ((ICadexGenericObject) iCadexGenericObject3).getAttr(attrName) : "" ;
	}

	public String getAttrName() {
		return attrName;
	}

	public String getAttrValueL() {
		return attrValueL;
	}

	public String getAttrValueR() {
		return attrValueR;
	}

	public String getAttrValueA() {
		return attrValueA;
	}
	
	@Override
	public boolean equals(Object arg0) {
		if( arg0 != null && arg0 instanceof AttributeNode )
			return ((AttributeNode)arg0).getAttrName().equals( this.attrName ); 
		else
			return false ;
	}

//	public List<AttributeNode> getChildNodes() {
//		return childNodes;
//	}
}

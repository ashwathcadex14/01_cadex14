package cadexstructurecompare.views.structurecompare;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

/**
 * Content provider for Cadex Compare tree structure.
 * @author Ashwath
 *
 */
public class CadexCompareContentProvider implements ITreeContentProvider {

	CadexCompareEditorInput compModel ;
	
	
	public CadexCompareContentProvider() {
		
	}
	
	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		this.compModel = (CadexCompareEditorInput) newInput;
	}

	@Override
	public Object[] getElements(Object inputElement) {
		
		Object[] obj = new Object[1];
		obj[0] = this.compModel.getCompareNode();
		return obj;
	}

	@Override
	public Object[] getChildren(Object parentElement) 
	{		
		return ((CompareNode)parentElement).getChildNodes().toArray();
	}

	@Override
	public Object getParent(Object element) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean hasChildren(Object element) {
		// TODO Auto-generated method stub
		return ((CompareNode)element).getChildNodes().size() > 0;
	}
}

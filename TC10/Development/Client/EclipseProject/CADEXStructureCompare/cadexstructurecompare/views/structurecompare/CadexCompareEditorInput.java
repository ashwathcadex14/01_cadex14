package cadexstructurecompare.views.structurecompare;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.eclipse.compare.ITypedElement;
import org.eclipse.compare.structuremergeviewer.DiffNode;
import org.eclipse.compare.structuremergeviewer.Differencer;
import org.eclipse.compare.structuremergeviewer.IDiffContainer;
import org.eclipse.core.runtime.IProgressMonitor;
import cadexstructurecompare.model.interfaces.ICadexComparable;
import cadexstructurecompare.model.interfaces.ICadexGenericDataset;
import cadexstructurecompare.model.interfaces.ICadexGenericLineObject;

public abstract class CadexCompareEditorInput {
	
	private ICadexGenericLineObject root1 ;
	private ICadexGenericLineObject root2 ;
	private DiffNode diffNode ;
	private CompareNode compareNode = null ;
	private int seqNo = 0 ;
	public static int LEFT_SIDE = 1;
	public static int RIGHT_SIDE = 2;
	//private IdObjectCluster idMap = null;
	
	 

	public DiffNode getDiffNode() {
		return diffNode;
	}

	public CadexCompareEditorInput() {
		//idMap = new IdObjectCluster();
	}
	
	/**
	 * <pre>Initializes the compare engine. Call this after constructing the object.
	 * NOTE: Until this method is called compare will not start.</pre>
	 * 
	 * @param monitor
	 * @throws Exception 
	 */
	public final void initializeInput(IProgressMonitor monitor) throws Exception
	{
		root1 = initializeRoot1();
		root2 = initializeRoot2();
		prepareInput(monitor);
	}
	
	/**
	 * Gets the Left Root object for comparison
	 * @return
	 * @throws Exception
	 */
	public abstract ICadexGenericLineObject initializeRoot1() throws Exception ;
	
	/**
	 * Gets the Right Root object for comparison
	 * @return
	 * @throws Exception
	 */
	public abstract ICadexGenericLineObject initializeRoot2() throws Exception ;

	/**
	 * Get Left root object
	 * @return
	 */
	public ICadexGenericLineObject getRoot1() {
		return root1;
	}
	
	/**
	 * Get right root object
	 * @return
	 */
	public ICadexGenericLineObject getRoot2() {
		return root2;
	}
	
	/**
	 * Traverse root structure
	 * @throws Exception
	 */
	public void traverseStructure() throws Exception
	{
		try
		{
			traverseChildren(compareNode);	
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	
	/**
	 * Recursive traverse logic for traversing the tree for comparator
	 * @param node
	 * @throws Exception
	 */
	public void traverseChildren(CompareNode node) throws Exception
	{
		try
		{
			node.shiftHierarchy();
			
			List<CompareNode> childs = node.getChildNodes();
			
			if( childs !=null && childs.size() > 0)
			{
				for(CompareNode cmpNode : childs)
				{
					traverseChildren(cmpNode);
				}
			}
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	
	/**
	 * Main traversal logic
	 * @param monitor
	 * @throws Exception
	 */
	protected void prepareInput(IProgressMonitor monitor)	throws Exception 
	{
		
		Differencer d = new Differencer() 
		{
			int nodeTraverse = 0 ;
			CompareNode currentNode ;
			CompareNode parentNode ;
			HashMap<String, CompareNode> compareNodes = new HashMap<String, CompareNode>();
			
			@Override
			protected Object visit(Object parent, int description, Object ancestor, Object left, Object right) {
				MyDiffNode diff = new MyDiffNode((IDiffContainer) parent, description, (ITypedElement) ancestor, (ITypedElement)left, (ITypedElement)right);
				String nodeUid = getNodeUid(ancestor, left, right);
				
				for(Entry<String, CompareNode> entry : compareNodes.entrySet())
				{
					if( entry.getKey().startsWith( nodeUid ) && ((entry.getValue().getLeftNode() ==null) || (entry.getValue().getRightNode() ==null)))
					{
						entry.getValue().setDifNode(diff);
					}
				}
				
				return diff;
			}
			
			@Override
			protected boolean contentsEqual(Object input1, Object input2) {
				boolean compareResult = super.contentsEqual(input1, input2);
				return compareResult;
			}
			
			@Override
			protected Object[] getChildren(Object input) 
			{
				Object[] objs = super.getChildren(input);
				
				System.out.println( "Getting Children for Tree " + nodeTraverse + " - - " + input + " -- " + (input !=null ?  input.getClass() : "nullparent"));

				if(nodeTraverse==0)
				{
					
					currentNode = new CompareNode((ICadexComparable) input, null, null,++seqNo);  
					
					if(compareNode == null)
						compareNode = this.currentNode;
					
					nodeTraverse++;
				}
				else if(nodeTraverse==1)
				{
					if(currentNode != null)
						currentNode.setRightNode((ICadexComparable) input);
					
					nodeTraverse++;
				}
				else
				{
					if(currentNode != null)
						currentNode.setLeftNode((ICadexComparable) input);
					
					if(parentNode != null && !currentNode.getNodeUid().equals( parentNode.getNodeUid() ))	
					{
						parentNode.addChildNode( currentNode );
					}
					
					if(currentNode.childAvailable && currentNode.getLeftNode() != null && currentNode.getRightNode() != null)
						parentNode = currentNode;
					else
						revertParent();
					
					currentNode.setAttributes();
					compareNodes.put( currentNode.getNodeUid() + "|" + currentNode.getSeqNo() , currentNode);
					
					if(currentNode.getLeftNode() == null || currentNode.getRightNode() == null  )
					{
						createDifferenceCompNodes();
					}
					
					currentNode.performDeepCompare();
					nodeTraverse-=2; 
				}
				
				return objs;
			}
			
			private void revertParent()
			{
				if (parentNode != null) {
					ICadexComparable ancestorNode = parentNode
							.getAncestorNode();
					ICadexComparable leftNode = parentNode.getLeftNode();
					ICadexComparable rightNode = parentNode
							.getRightNode();
					int leftSize = leftNode != null ? leftNode.getChildrenCount() : 0;
					int rightSize = rightNode != null ? rightNode.getChildrenCount() : 0;
					int ancestorSize = ancestorNode != null ? ancestorNode.getChildrenCount() : 0;
					int allSize = leftSize > rightSize ? leftSize > ancestorSize ? leftSize
							: ancestorSize > rightSize ? ancestorSize
									: rightSize
							: rightSize > ancestorSize ? rightSize
									: ancestorSize;
					if (((parentNode.getChildCount() - parentNode.deletions) == allSize)) {
						parentNode = parentNode.getParentCompNode();
						revertParent();
					}
				}
			}
			
			private void createDifferenceCompNodes()
			{
				if(currentNode.getLeftNode() != null)
					addCompareChild(currentNode, LEFT_SIDE);
				else
					addCompareChild(currentNode, RIGHT_SIDE);
			}
			
			private void addCompareChild(CompareNode curNode, int side)
			{
				Object[] lines = side == LEFT_SIDE ? curNode.getLeftNode().getChildren() : curNode.getRightNode().getChildren();
				
				if(lines!=null)
				{
					for(Object line :  lines)
					{
						CompareNode newNode = new CompareNode( null , side == LEFT_SIDE ? (ICadexComparable)line : null, side == RIGHT_SIDE ? (ICadexComparable)line : null,++seqNo);
						newNode.setNodeType();
						newNode.performDeepCompare();
						curNode.addChildNode( newNode );
						compareNodes.put( newNode.getNodeUid() + "|" + newNode.getSeqNo() , newNode);
						addCompareChild(newNode, side);
					}
				}
			}
			
			private String getNodeUid(Object ancestor, Object left, Object right)
			{
				String retUid = "" ;
				
				if( ancestor != null && ancestor instanceof ITypedElement  )
					retUid = ((ITypedElement)ancestor).getName();
				
				if( retUid.length() == 0 && left != null && left instanceof ITypedElement  )
					retUid = ((ITypedElement)left).getName();
				
				if( retUid.length() == 0 && right != null && right instanceof ITypedElement  )
					retUid = ((ITypedElement)right).getName();
				
				return retUid;
			}
			
			
		};
			
		diffNode = (DiffNode) d.findDifferences(false, monitor, null, null, root1, root2);
		
		if(compareNode!=null)
			traverseStructure();
		
	}
	
	/**
	 * Class representing the Difference node in the compare tree
	 * @author Ashwath
	 *
	 */
	public class MyDiffNode extends DiffNode 
	{
		private boolean fDirty= false;
		private ITypedElement fLastId;
		private String fLastName;
		
		
		public MyDiffNode(IDiffContainer parent, int description, ITypedElement ancestor, ITypedElement left, ITypedElement right) {
			super(parent, description, ancestor, left, right);
			
		}
	
		void clearDirty() {
			fDirty= false;
		}
		public String getName() {
			if (fLastName == null)
				fLastName= super.getName();
			if (fDirty)
				return '<' + fLastName + '>';
			return fLastName;
		}
		
		public ITypedElement getId() {
			ITypedElement id= super.getId();
			if (id == null)
				return fLastId;
			fLastId= id;
			return id;
		}
	}
	
	
	public class DatasetNode
	{
		String sideName = "";
		ArrayList<ICadexGenericDataset> dsets = new ArrayList<ICadexGenericDataset>();
		public DatasetNode(String name) {
			this.sideName = name;
		}
		public void setDsets(ArrayList<ICadexGenericDataset> dsets) {
			if(dsets !=null)
				this.dsets.addAll(dsets);
		}
		public ArrayList<ICadexGenericDataset> getDsets() {
			return dsets;
		}
		public String getSideName() {
			return sideName;
		}
	}

	/**
	 * Gets the root compare node
	 * @return
	 */
	public CompareNode getCompareNode() {
		return compareNode;
	}
	
}

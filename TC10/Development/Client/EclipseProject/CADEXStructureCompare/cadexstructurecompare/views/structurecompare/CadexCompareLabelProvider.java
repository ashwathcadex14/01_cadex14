package cadexstructurecompare.views.structurecompare;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableColorProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.wb.swt.ResourceManager;

import cadexstructurecompare.model.interfaces.ICadexComparable;

/**
 * This provides the display components for the compare tree structure
 * for both left and right trees.
 * This is a common provider for both trees as the data is same (Compare Node) for both trees.
 * @author Ashwath
 *
 */
public class CadexCompareLabelProvider implements ITableLabelProvider, ITableColorProvider {

	public static int CADEX_PROVIDE_LEFT = 1;
	public static int CADEX_PROVIDE_RIGHT = 2;
	public static int CADEX_PROVIDE_ANCESTOR = 3;
	private int whatToProvide = 0 ;
	private static Image ITEM = ResourceManager.getPluginImage("CADEXStructureCompare", "cadexstructurecompare/images/Item.png") ;
	private static Image ITEM_NEW = ResourceManager.getPluginImage("CADEXStructureCompare", "cadexstructurecompare/images/Item_New.png") ;
	private static Image ITEM_DELETED = ResourceManager.getPluginImage("CADEXStructureCompare", "cadexstructurecompare/images/Item_Deleted.png") ;
	private static Image ITEM_MODIFIED = ResourceManager.getPluginImage("CADEXStructureCompare", "cadexstructurecompare/images/Item_Modified.png") ;
	
	public CadexCompareLabelProvider(int provideWhat) {
		this.whatToProvide = provideWhat;
	}
	
	@Override
	public void addListener(ILabelProviderListener listener) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {
		// TODO Auto-generated method stub

	}

	@Override
	public Image getColumnImage(Object element, int columnIndex) 
	{
		if(this.whatToProvide==CADEX_PROVIDE_RIGHT)
		{
			switch (((CompareNode)element).getDelta()) 
			{
				case NEW:
					return ITEM_NEW;
				case REMOVED:
					return ITEM_DELETED;
				case MODIFIED:
					return ITEM_MODIFIED;
				default:
				{
					if(((CompareNode)element).isRelationModfied())
						return ITEM_MODIFIED;
					else
						return ITEM;
				}
			}
		}
		else
		{
			switch (((CompareNode)element).getDelta()) 
			{
				case REMOVED:
					return ITEM_DELETED;
				default:
					return ITEM ;
			}
		}
	}

	@Override
	public String getColumnText(Object element, int columnIndex) 
	{
		ICadexComparable obj = getObject(element);
		if(obj!=null)
			return (obj).getObjectString();
		else
			return "";
	}
	
	private ICadexComparable getObject(Object element)
	{	
		 CompareNode currentNode = ((CompareNode)element);
		
		switch (this.whatToProvide) 
		{
			case 1:
				return currentNode.getLeftNode();
			case 2:
				return currentNode.getRightNode();
			default:
				return currentNode.getAncestorNode();
		}
	}

	@Override
	public Color getForeground(Object element, int columnIndex) {
		return null;
	}

	@Override
	public Color getBackground(Object element, int columnIndex) {

//		if(this.whatToProvide==CADEX_PROVIDE_RIGHT)
//		{
//			switch (((CompareNode)element).getDelta()) 
//			{
//				case NEW:
//					return new Color(Display.getCurrent(), 51,153, 0);
//				case REMOVED:
//					return new Color(Display.getCurrent(), 255, 51, 0);
//				case MODIFIED:
//					return new Color(Display.getCurrent(), 255, 255, 0);
//				default:
//					return null ;
//			}
//		}
//		else
//		{
//			switch (((CompareNode)element).getDelta()) 
//			{
//				case REMOVED:
//					return new Color(Display.getCurrent(), 255, 51, 0);
//				default:
//					return null ;
//			}
//		}
		
		return null;
	}

	
	
}

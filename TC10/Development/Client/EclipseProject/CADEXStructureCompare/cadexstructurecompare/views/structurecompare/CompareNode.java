package cadexstructurecompare.views.structurecompare;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.eclipse.compare.structuremergeviewer.IStructureComparator;

import cadexstructurecompare.delta.AbstractDeltaGenerator.DELTA_INFO;
import cadexstructurecompare.model.interfaces.ICadexComparable;
import cadexstructurecompare.model.interfaces.ICadexGenericLineObject;
import cadexstructurecompare.model.interfaces.ICadexGenericObject;
import cadexstructurecompare.views.structurecompare.CadexCompareEditorInput.DatasetNode;
import cadexstructurecompare.views.structurecompare.CadexCompareEditorInput.MyDiffNode;

/**
 * Core data node for the compare tree structure. It holds the data of both left and right
 * object of a compare node.
 * @author Ashwath
 *
 */
public class CompareNode 
{
	private ICadexComparable ancestorNode ;
	private ICadexComparable leftNode ; 
	private ICadexComparable rightNode ;
	private MyDiffNode difNode ;
	private boolean isDiff ;
	private String nodeUid = "" ;
	private int seqNo  = 0 ;
	protected int additions = 0 ;
	protected int deletions = 0 ;
	private int nodeType = 0;
	public static int COMPARE_LINE_NODE = 1;
	public static int COMPARE_OTHER_NODE = 2;
	private CompareNode parentCompNode = null ;
	private List<CompareNode> childNodes = new ArrayList<CompareNode>();
	private List<AttributeNode> attrNodes = new ArrayList<AttributeNode>();
	private List<DatasetNode> datasetNodes = new ArrayList<DatasetNode>();
	protected boolean childAvailable = false ;
	
	public CompareNode(ICadexComparable ancestorNode, ICadexComparable leftNode, ICadexComparable rightNode, int seqNo) {
		this.leftNode = leftNode;
		this.rightNode = rightNode;
		this.setAncestorNode(ancestorNode);
		this.seqNo = seqNo;
		

		if(this.ancestorNode != null)
			this.nodeUid = this.ancestorNode.toString();
	}

	public MyDiffNode getDifNode() {
		return difNode;
	}
	
	public void performDeepCompare() {
		
		if(leftNode == null && rightNode != null)
		{
			rightNode.setChangeInfo( DELTA_INFO.NEW );
		}
		else if( leftNode != null && rightNode == null)
		{
			leftNode.setChangeInfo( DELTA_INFO.REMOVED );
		}
		else
		{
		   rightNode.setChangeInfo( leftNode.doDeepCompare(rightNode) ? DELTA_INFO.MODIFIED : DELTA_INFO.NO_CHANGE );
		}
	}
	
	public boolean isRelationModfied()
	{
		return isModified( COMPARE_LINE_NODE );
	}
	
    private boolean isModified(int nodeType)
    {
        for (CompareNode node : (nodeType == CompareNode.COMPARE_LINE_NODE ? getChildDetailNodes() : getChildNodes()))
        {
            if(node.getDelta() != DELTA_INFO.NO_CHANGE)
            {
                return true;
            }
            else
            {
                return node.isModified(CompareNode.COMPARE_OTHER_NODE);
            }
        }

        return false;
    }
	
	public DELTA_INFO getDelta()
	{
		if(leftNode == null && rightNode != null)
		{
			return rightNode.changeInfo();
		}
		else if( leftNode != null && rightNode == null)
		{
			return leftNode.changeInfo();
		}
		else
		{
			return rightNode.changeInfo();
		}
	}

	public void setDifNode(MyDiffNode difNode) {

		this.difNode = difNode;
		if(this.difNode != null)
		{
			this.isDiff = true;
			
		}
	}

	public ICadexComparable getLeftNode() {
		return leftNode;
	}
	
	public ICadexComparable getRightNode() {
		return rightNode;
	}

	public List<CompareNode> getChildNodes() {

		if(this.nodeType == COMPARE_LINE_NODE)
		{
			ArrayList<CompareNode> childNodes = new ArrayList<CompareNode>();
			for(CompareNode node : this.childNodes)
			{
				if(node.nodeType == COMPARE_LINE_NODE)
					childNodes.add( node );
			}
			return childNodes;
		}
		else
			return childNodes;
		
	}
	
	public List<CompareNode> getChildDetailNodes() {

		if(this.nodeType == COMPARE_LINE_NODE)
		{
			ArrayList<CompareNode> childNodes = new ArrayList<CompareNode>();
			for(CompareNode node : this.childNodes)
			{
				if(node.nodeType == COMPARE_OTHER_NODE)
					childNodes.add( node );
			}
			return childNodes;
		}
		else
			return null;
		
	}
	
	public void shiftHierarchy()
	{
		CompareNode itemNode = null;
		ArrayList<CompareNode> detailChilds = (ArrayList<CompareNode>) this.getChildDetailNodes();
		if(detailChilds.size() == 1)
		{
			for(CompareNode revChild : detailChilds.get(0).childNodes )
			{
				if(revChild.getNode().getType().equals( "Item" ))
				{
					itemNode = revChild;
					break;
				}
			}
			
			if(itemNode!=null)
			{
				detailChilds.get(0).childNodes.remove( itemNode );
				itemNode.childNodes.add( detailChilds.get(0) );
				this.childNodes.remove( detailChilds.get(0) );
				this.childNodes.add( itemNode );
			}
		}
	}
	
	public int getChildCount() {
			return childNodes.size();
	}

	public ICadexComparable getAncestorNode() {
		return ancestorNode;
	}

	public void addChildNode(CompareNode childNode) {
		this.childNodes.add( childNode );
		
		if(childNode.leftNode == null)
		{
			this.additions++;
		}
		else if(childNode.rightNode == null)
		{
			this.deletions++;
		}
		
		childNode.setParentCompNode(this);
	}
	
//	@Override
//	public String toString() {
//		return ancestorNode + " <--> " + leftNode + " <--> " + rightNode ;
//	}

	public void setLeftNode(ICadexComparable leftNode) 
	{
		this.leftNode = leftNode;
		this.setChildAvailable(this.leftNode);
		
		if(this.leftNode != null)
		{
			if( this.nodeUid.length() == 0 )
			{
				this.nodeUid = this.leftNode.toString();
			}
			
			setNodeType();
		}
		
		
	}

	public void setRightNode(ICadexComparable rightNode) 
	{
		this.rightNode = rightNode;
		this.setChildAvailable(this.rightNode);
		
		if(this.rightNode != null)
		{
			if( this.nodeUid.length() == 0)
			{
				this.nodeUid = this.rightNode.toString();
			}
			
			setNodeType();
		}
	}

	public boolean isChildAvailable() {
		return childAvailable;
	}

	public void setChildAvailable(Object input) {
		
		Object[] objs = this.getChildren(input);
		
		if(!this.childAvailable)
			this.childAvailable =  objs !=null && objs.length > 0 ? true : false ;
	}

	public CompareNode getParentCompNode() {
		return parentCompNode;
	}

	private void setParentCompNode(CompareNode parentCompNode) {
		this.parentCompNode = parentCompNode;
	}
	
	protected Object[] getChildren(Object input) {
		if (input instanceof IStructureComparator)
			return ((IStructureComparator)input).getChildren();
		return null;
	}

	public void setAncestorNode(ICadexComparable ancestorNode2) {
		this.ancestorNode = ancestorNode2;
		this.setChildAvailable(this.ancestorNode);
	}
	
	public void setAttributes()
	{
        HashSet<String> leftAttrs = new HashSet<String>();
        HashSet<String> rightAttrs = new HashSet<String>();

        if (leftNode != null)
            leftAttrs = leftNode.getAttrNames();

        if (rightNode != null)
            rightAttrs = rightNode.getAttrNames();

        leftAttrs.addAll(rightAttrs);

        for (String entry : leftAttrs)
	    {
            String oldAttr = leftNode != null ? ((ICadexGenericObject)leftNode).getAttr(entry) : "NA";
            String newAttr = rightNode != null ? ((ICadexGenericObject)rightNode).getAttr(entry) : "NA";
            addAttrNode(entry, oldAttr, newAttr, "");
	    }
		
//		AttributeNode bomAttr = new AttributeNode( "BOM" , this , 1);
//		AttributeNode itemAttr = new AttributeNode( "Item" , this , 2);
//		AttributeNode revAttr = new AttributeNode( "Revision" , this , 3);
//		this.attrNodes.add(bomAttr);
//		this.attrNodes.add(itemAttr);
//		this.attrNodes.add(revAttr);
	}
	
    public void addAttrNode(String attrName, String leftValue, String rightValue, String ancestorValue)
    {
        AttributeNode node = new AttributeNode(attrName, leftValue, rightValue, ancestorValue);
        attrNodes.add(node);
    }

	public List<AttributeNode> getAttrNodes() {
		return attrNodes;
	}

	public List<DatasetNode> getDatasetNodes() {
		return datasetNodes;
	}

	public boolean isDiff() {
		return isDiff;
	}

	public int getSeqNo() {
		return seqNo;
	}

	public int getNodeType() {
		return nodeType;
	}

	public void setNodeType() 
	{
		if(nodeType == 0)
			nodeType = ((this.leftNode != null ? this.leftNode : this.rightNode) instanceof ICadexGenericLineObject) ? CompareNode.COMPARE_LINE_NODE : CompareNode.COMPARE_OTHER_NODE ;
	}
	
	public ICadexComparable getNode()
	{
		if(leftNode==null)
			return rightNode;
		else if(rightNode == null)
			return leftNode;
		else
			return rightNode;
	}

	public String getNodeUid() {
		return nodeUid;
	}
}


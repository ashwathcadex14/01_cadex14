/**=================================================================================================
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  ================================================================================================= 
#      Filename        :           InfoExPlmxmlCompareEditorInput.java          
#      Module          :           cadexstructurecompare.views.structurecompare.plmxml          
#      Description     :           TODO          
#      Project         :           CADEXStructureCompare          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                         Name           Description of Change
#  09-Oct-2015		              Ashwath	         Initial Creation
#  $HISTORY$                  
#  =================================================================================================*/
package cadexstructurecompare.views.structurecompare.plmxml;

import java.util.List;

import cadexstructurecompare.model.interfaces.ICadexGenericLineObject;
import cadexstructurecompare.model.plmxml.infoex.PlmxmlOccurence;
import cadexstructurecompare.views.structurecompare.CadexCompareEditorInput;

import com.infoex.plmxml.generic.Document;
import com.infoex.plmxml.generic.PLMXMLParser;
import com.infoex.plmxml.types.Occurrence;
import com.infoex.plmxml.types.PLMXML;
import com.infoex.plmxml.types.ProductView;

/**
 * Editor Input consisting of the plmxml file paths.
 * @author Ashwath
 */
public class InfoExPlmxmlCompareEditorInput extends CadexCompareEditorInput {

	private String file1 ;
	private String file2 ;
	
	/**
	 * Constructor
	 */
	public InfoExPlmxmlCompareEditorInput(String file1, String file2) {
		this.file1 = file1;
		this.file2 = file2;
	}

	/* (non-Javadoc)
	 * @see cadexstructurecompare.views.structurecompare.CadexCompareEditorInput#initializeRoot1()
	 */
	@Override
	public ICadexGenericLineObject initializeRoot1() throws Exception {
		// TODO Auto-generated method stub
		return initializeRoot(this.file1);
	}

	/* (non-Javadoc)
	 * @see cadexstructurecompare.views.structurecompare.CadexCompareEditorInput#initializeRoot2()
	 */
	@Override
	public ICadexGenericLineObject initializeRoot2() throws Exception {
		// TODO Auto-generated method stub
		return initializeRoot(this.file2);
	}
	
	/**
	 * Returns the root occurrence of the plmxml file.
	 * @param inputPLMXMLPath
	 * @return
	 * @throws Exception
	 */
	private PlmxmlOccurence initializeRoot(String inputPLMXMLPath) throws Exception
	{
		List<com.infoex.plmxml.generic.IdBase> prodViewList = null;
		Document<PLMXML> m_xmlDoc = readDocument(inputPLMXMLPath);
		ProductView mainProdcutView ;
		prodViewList = m_xmlDoc.getElementsByClass("ProductView");			
		
		mainProdcutView = (ProductView) prodViewList.get(0);
		
		List<Object> occList = mainProdcutView.getRootRefs();
		
		if(occList!=null && occList.size()>0)
		{
			return new PlmxmlOccurence((Occurrence) occList.get(0));
		}		
		else
			throw new Exception("No root occurrence found for the file " + inputPLMXMLPath);
	}

	/**
	 * Reads the plmxml document using InfoExPLMXMLSDK
	 * @param xmlFileUri
	 * @return
	 * @throws Exception
	 */
	public static Document<PLMXML> readDocument(final String xmlFileUri) throws Exception 
	{
	  Document<PLMXML> docElement = null;
	  docElement = (Document<PLMXML>) PLMXMLParser.readDocument(xmlFileUri);
      return docElement;
	}
}

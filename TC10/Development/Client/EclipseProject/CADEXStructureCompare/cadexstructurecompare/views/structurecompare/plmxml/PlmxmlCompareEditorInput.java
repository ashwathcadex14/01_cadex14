/**=================================================================================================
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  ================================================================================================= 
#      Filename        :           PlmxmlCompareEditorInput.java          
#      Module          :           cadexstructurecompare.views.structurecompare.plmxml          
#      Description     :           TODO          
#      Project         :           CADEXStructureCompare          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                         Name           Description of Change
#  09-Oct-2015		              Ashwath	         Initial Creation
#  $HISTORY$                  
#  =================================================================================================*/
package cadexstructurecompare.views.structurecompare.plmxml;

import org.plmxml.sdk.plmxml.Handle;
import org.plmxml.sdk.plmxml.ObjectBase;
import org.plmxml.sdk.plmxml.PLMXMLException;
import org.plmxml.sdk.plmxml.plmxml60.Document;
import org.plmxml.sdk.plmxml.plmxml60.Occurrence;
import org.plmxml.sdk.plmxml.plmxml60.ProductView;

import cadexstructurecompare.model.interfaces.ICadexGenericLineObject;
import cadexstructurecompare.model.plmxml.PlmxmlOccurence;
import cadexstructurecompare.views.structurecompare.CadexCompareEditorInput;

/**
 * Editor Input for CADEX Compare based on the siemens plmxml sdk.
 * @author Ashwath
 * @deprecated
 */
public class PlmxmlCompareEditorInput extends CadexCompareEditorInput {

	private String file1 ;
	private String file2 ;
	
	/**
	 * Constructor
	 */
	public PlmxmlCompareEditorInput(String file1, String file2) {
		this.file1 = file1;
		this.file2 = file2;
	}

	/* (non-Javadoc)
	 * @see cadexstructurecompare.views.structurecompare.CadexCompareEditorInput#initializeRoot1()
	 */
	@Override
	public ICadexGenericLineObject initializeRoot1() throws Exception {
		return initializeRoot(file1);
	}

	/* (non-Javadoc)
	 * @see cadexstructurecompare.views.structurecompare.CadexCompareEditorInput#initializeRoot2()
	 */
	@Override
	public ICadexGenericLineObject initializeRoot2() throws Exception {
		return initializeRoot(file2);
	}

	/**
	 * Returns the root occurrence of the plmxml file.
	 * @param inputPLMXMLPath
	 * @return
	 * @throws Exception
	 */
	private PlmxmlOccurence initializeRoot(String inputPLMXMLPath) throws Exception
	{
		String caeDeckStructId = null;
		ObjectBase[] prodViewList = null;
		Document m_xmlDoc = readDocument(inputPLMXMLPath);
		ProductView mainProdcutView ;
	    
		prodViewList = (ObjectBase[]) m_xmlDoc.getElementsByClass("ProductView");			
		
		mainProdcutView = (ProductView) prodViewList[0];
		
		caeDeckStructId = mainProdcutView.getAttributeValueAsString("rootRefs");
		
		Occurrence m_caeDeckOcc = (Occurrence) m_xmlDoc.resolveId(caeDeckStructId);	
				
		
		return new PlmxmlOccurence(m_caeDeckOcc);
	}
	
	/**
	 * Reads the plmxml document using Siemens PLMXML SDK
	 * @param xmlFileUri
	 * @return
	 * @throws Exception
	 */
	private Document readDocument(final String xmlFileUri) throws IllegalArgumentException, PLMXMLException 
	{
	      Document docElement = null;

	      Handle[] hdl = ObjectBase.load(xmlFileUri);

          if ((hdl != null) && (hdl.length > 0)) {
              docElement = (Document) hdl[0].getObject();
          }

	      return docElement;
	}
}

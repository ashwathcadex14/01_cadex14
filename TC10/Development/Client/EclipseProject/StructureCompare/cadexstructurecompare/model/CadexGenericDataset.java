package cadexstructurecompare.model;

import java.util.ArrayList;

import cadexstructurecompare.model.interfaces.ICadexGenericDataset;
import cadexstructurecompare.model.interfaces.ICadexGenericFile;

public abstract class CadexGenericDataset<T> extends CadexGenericObject<T> implements ICadexGenericDataset{

	ArrayList<ICadexGenericFile> datasetFiles = new ArrayList<ICadexGenericFile>();
	String relationName = "" ;
	
	public CadexGenericDataset(T obj, String relation) throws Exception {
		super(obj);
		this.relationName = relation;
		setFiles();
	}

	public abstract ArrayList<ICadexGenericFile> populateFiles() throws Exception;
	
	@Override
	public ArrayList<ICadexGenericFile> getFiles() {
		return datasetFiles;
	}
	
	private void setFiles() throws Exception
	{
		ArrayList<ICadexGenericFile> files =  populateFiles();
		if(files != null)
			this.datasetFiles.addAll( files );
	}

	public String getRelation() {
		// TODO Auto-generated method stub
		return this.relationName;
	}
	
}

package cadexstructurecompare.model;

import java.util.ArrayList;

import org.eclipse.compare.ITypedElement;
import org.eclipse.compare.structuremergeviewer.IStructureComparator;

import cadexstructurecompare.model.interfaces.ICadexGenericLineObject;
import cadexstructurecompare.model.interfaces.ICadexGenericRevision;


public abstract class CadexGenericLineObject<T> extends CadexGenericObject<T> implements IStructureComparator, ITypedElement, ICadexGenericLineObject {
	
	private ICadexGenericRevision lineItem ;
	private ArrayList<ICadexGenericLineObject> children = new ArrayList<ICadexGenericLineObject>();
	
	public CadexGenericLineObject(T lineObj) throws Exception {
		super(lineObj);
		setLineRev();
		setChildren();
	}
	
	public abstract ICadexGenericRevision getItemRevision() throws Exception;
	public abstract ArrayList<ICadexGenericLineObject> getChildLines() throws Exception;

	public ICadexGenericRevision getLineRev() {
		return lineItem;
	}

	public void setLineRev() throws Exception {
		this.lineItem = getItemRevision();
	}

	public ArrayList<ICadexGenericLineObject> returnChildren() {
		return children;
	}

	private void setChildren() throws Exception {
		this.children.addAll( getChildLines() );
	}
	
}

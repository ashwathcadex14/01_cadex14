package cadexstructurecompare.model;

import java.util.HashMap;
import java.util.Set;

import cadexstructurecompare.model.interfaces.ICadexGenericObject;

public abstract class CadexGenericObject<T> implements ICadexGenericObject{

	private T obj ;
	private HashMap<String, String> attrMap = new HashMap<String,String>();
	
	public CadexGenericObject(T obj) throws Exception {
		this.obj = obj;
		setProperties();
	}
	
	public abstract void setProperties() throws Exception;
	
	
	public T getObj() {
		return obj;
	}

	public String getAttr(String attrName) {
		return attrMap.get( attrName );
	}

	public void addAttr(String attrName, String attrValue) {
		this.attrMap.put(attrName, attrValue);
	}
	
	public Set<String> getAttrNames()
	{
		return this.attrMap.keySet();
	}
}

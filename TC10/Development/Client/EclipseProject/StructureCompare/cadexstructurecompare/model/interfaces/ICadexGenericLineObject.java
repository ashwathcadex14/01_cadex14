package cadexstructurecompare.model.interfaces;

import java.util.ArrayList;

public interface ICadexGenericLineObject extends ICadexGenericObject {

	public ICadexGenericRevision getLineRev();

	public ArrayList<ICadexGenericLineObject> returnChildren();
}

package cadexstructurecompare.model.interfaces;

import java.util.Set;

public interface ICadexGenericObject {
	
	public Object getObj();

	public String getAttr(String attrName);

	public void addAttr(String attrName, String attrValue);
	
	public Set<String> getAttrNames();
}
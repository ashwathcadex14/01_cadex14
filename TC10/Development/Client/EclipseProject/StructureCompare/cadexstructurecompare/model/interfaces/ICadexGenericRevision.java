package cadexstructurecompare.model.interfaces;

import java.util.ArrayList;

public interface ICadexGenericRevision extends ICadexGenericObject{
	
	public ArrayList<ICadexGenericDataset> getAttaches();

	public ICadexGenericItem getRevItem();

}

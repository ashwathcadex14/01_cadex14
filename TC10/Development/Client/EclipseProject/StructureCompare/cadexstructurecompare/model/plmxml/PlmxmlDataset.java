package cadexstructurecompare.model.plmxml;

import java.util.ArrayList;

import org.plmxml.sdk.plmxml.plmxml60.DataSet;
import org.plmxml.sdk.plmxml.plmxml60.ExternalFile;

import cadexstructurecompare.model.CadexGenericDataset;
import cadexstructurecompare.model.interfaces.ICadexGenericFile;

public class PlmxmlDataset extends CadexGenericDataset<DataSet> {

	public PlmxmlDataset(DataSet obj, String relation) throws Exception {
		super(obj,relation);
	}

	@Override
	public void setProperties() throws Exception
	{
		this.addAttr( "object_name" , (String) this.getObj().getAttributeValue( "name" ) );
		this.addAttr( "object_type" , (String) this.getObj().getAttributeValue( "type" ) );
		this.addAttr( "version" , this.getObj().getAttributeValue( "version" ).toString() );
		PlmxmlUtil.addUserData(this, this.getObj());
	}
	
	

	@Override
	public ArrayList<ICadexGenericFile> populateFiles() throws Exception{
		
		ArrayList<ICadexGenericFile> files = new ArrayList<ICadexGenericFile>();
		String[] baseObj = this.getObj().getMemberURIs();
		
		if (baseObj.length > 0)
		{			
			for(int i = 0; i < baseObj.length; i++) 
			{
				ExternalFile file = (ExternalFile) this.getObj().getMemberHandle( i ).getObject();
				files.add( new PlmxmlFile( file ) ); 
			}
		}
		
		return files;
		
	}

	@Override
	public String getType() {
		return this.getAttr( "object_type" );
	}

}

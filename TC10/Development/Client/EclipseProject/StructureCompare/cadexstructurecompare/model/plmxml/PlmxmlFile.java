package cadexstructurecompare.model.plmxml;

import org.plmxml.sdk.plmxml.plmxml60.ExternalFile;

import cadexstructurecompare.model.CadexGenericFile;

public class PlmxmlFile extends CadexGenericFile<ExternalFile> {

	public PlmxmlFile(ExternalFile obj) throws Exception {
		super(obj);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void setProperties() throws Exception {
		this.addAttr( "format" , (String) this.getObj().getAttributeValue( "format" ) );
		String locationRef = (String) this.getObj().getAttributeValueAsString( "locationRef" );
		String orgFileName = "";
		if(locationRef.contains( "\\" ))
			orgFileName = locationRef.substring( locationRef.indexOf( "\\" )+1, locationRef.length());
		
		this.addAttr( "location" , locationRef  );
		this.addAttr( "original_file_name" , orgFileName  );
		PlmxmlUtil.addUserData(this, this.getObj());
	}

	@Override
	public String getOriginalFileName() {
		return this.getAttr( "original_file_name" );
	}

}

package cadexstructurecompare.model.plmxml;

import org.plmxml.sdk.plmxml.plmxml60.Product;

import cadexstructurecompare.model.CadexGenericItem;

public class PlmxmlItem extends CadexGenericItem<Product> {

	public PlmxmlItem(Product item) throws Exception {
		super(item);
	}

	@Override
	public void setProperties() throws Exception{
		this.addAttr( "item_id", (String) this.getObj().getAttributeValue( "productId" ));
	}

}

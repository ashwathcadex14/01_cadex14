package cadexstructurecompare.model.plmxml;

import java.util.ArrayList;

import org.eclipse.compare.ITypedElement;
import org.eclipse.swt.graphics.Image;
import org.plmxml.sdk.plmxml.plmxml60.IdObject;
import org.plmxml.sdk.plmxml.plmxml60.Occurrence;
import org.plmxml.sdk.plmxml.plmxml60.ProductRevision;

import cadexstructurecompare.model.CadexGenericLineObject;
import cadexstructurecompare.model.interfaces.ICadexGenericLineObject;
import cadexstructurecompare.model.interfaces.ICadexGenericObject;
import cadexstructurecompare.model.interfaces.ICadexGenericRevision;

public class PlmxmlOccurence extends CadexGenericLineObject<Occurrence> {

	public PlmxmlOccurence(Occurrence lineObj) throws Exception {
		super(lineObj);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void setProperties() throws Exception 
	{
		PlmxmlUtil.addUserData(this, this.getObj());
	}

	@Override
	public ArrayList<ICadexGenericLineObject> getChildLines() throws Exception {
		
		ArrayList<ICadexGenericLineObject> childList = new ArrayList<ICadexGenericLineObject>();
		
		IdObject[] objs = this.getObj().resolveOccurrenceRefs();
		
		for (int i = 0; i < objs.length; i++) {
			 childList.add( (ICadexGenericLineObject) new PlmxmlOccurence( (Occurrence)objs[i] )) ;
		}
		
		return childList;
	}

	@Override
	public ICadexGenericRevision getItemRevision() throws Exception {
		
		ProductRevision rev = null;
		
		rev = (ProductRevision) this.getObj().getInstancedHandle().getObject();
		
		return (ICadexGenericRevision) new PlmxmlRevision(rev);
	}

	@Override
	public Object[] getChildren() {
		// TODO Auto-generated method stub
		return this.returnChildren().toArray();
	}

	@Override
	public String getName() {
		
		ICadexGenericObject lineRev = ((ICadexGenericObject) getLineRev());
		String revId = lineRev.getAttr( "item_revision_id" ) ;
		String objName = lineRev.getAttr( "object_name" ) ;
		String itemId = ((ICadexGenericObject) getLineRev().getRevItem()).getAttr( "item_id" ) ;
		return itemId + "/" + revId + ";" + objName;
	}

	@Override
	public Image getImage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean equals(Object other) {
		if (other instanceof ITypedElement)
			return getName().equals(((ITypedElement) other).getName());
		return super.equals(other);
	}
	
	
	
	public int hashCode() {
		return getName().hashCode();
	}

	@Override
	public String toString() {
		return getName();
				
	}
}

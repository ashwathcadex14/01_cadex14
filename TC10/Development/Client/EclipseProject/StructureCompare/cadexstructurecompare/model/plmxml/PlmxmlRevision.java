package cadexstructurecompare.model.plmxml;

import java.util.ArrayList;

import org.plmxml.sdk.plmxml.ObjectBase;
import org.plmxml.sdk.plmxml.plmxml60.AssociatedDataSet;
import org.plmxml.sdk.plmxml.plmxml60.DataSet;
import org.plmxml.sdk.plmxml.plmxml60.Product;
import org.plmxml.sdk.plmxml.plmxml60.ProductRevision;

import cadexstructurecompare.model.CadexGenericRevision;
import cadexstructurecompare.model.interfaces.ICadexGenericDataset;
import cadexstructurecompare.model.interfaces.ICadexGenericItem;

public class PlmxmlRevision extends CadexGenericRevision<ProductRevision> {

	public PlmxmlRevision(ProductRevision obj) throws Exception {
		super(obj);
	}

	@Override
	public ArrayList<ICadexGenericDataset> getAttachments() throws Exception
	{
			return getDatasets();
	}

	@Override
	public void setProperties() throws Exception
	{
		this.addAttr( "item_revision_id", (String) this.getObj().getAttributeValue( "revision" ));
		this.addAttr( "object_name", (String) this.getObj().getAttributeValue( "name" ));
	}
	
	@Override
	public ICadexGenericItem getItem() throws Exception
	{
		return (ICadexGenericItem) new PlmxmlItem((Product) this.getObj().getMasterHandle().getObject());
	}
	
	public ArrayList<ICadexGenericDataset> getDatasets() throws Exception
	{
		ArrayList<ICadexGenericDataset> datasets = new ArrayList<ICadexGenericDataset>();
		ObjectBase[] baseObj = this.getObj().getElementsByClass("AssociatedDataSet");
		
		if (baseObj.length > 0)
		{			
			for(int i = 0; i < baseObj.length; i++) 
			{
				AssociatedDataSet assDset = (AssociatedDataSet)baseObj[i];
				ObjectBase objBase = assDset.getDataSetHandle().getObject();
				if (objBase != null)
				{
					datasets.add( new PlmxmlDataset( (DataSet) objBase, assDset.getAttributeValueAsString( "role" ) ) ); 
				}
			}
		}
		
		return datasets;
	}

}

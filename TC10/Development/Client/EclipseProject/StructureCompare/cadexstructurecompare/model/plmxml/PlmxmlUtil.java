package cadexstructurecompare.model.plmxml;

import org.plmxml.sdk.plmxml.ObjectBase;
import org.plmxml.sdk.plmxml.plmxml60.UserData;
import org.plmxml.sdk.plmxml.plmxml60.UserValue;

import cadexstructurecompare.model.interfaces.ICadexGenericObject;

public class PlmxmlUtil {
	
	public static void addUserData( ICadexGenericObject obj , ObjectBase tag ) throws Exception
	{
		ObjectBase[] userDatas = tag.getElementsByClass( "UserData" );
		for (int i = 0; i < userDatas.length; i++) 
		{
			UserValue[] userValues = ((UserData)userDatas[i]).getUserValues();
			
			for (int j = 0; j < userValues.length; j++) {
				obj.addAttr(userValues[j].getTitle(), userValues[j].getValue());
			}
			
		}
	}
}

package com.structurecompare.core;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.compare.ITypedElement;
import org.eclipse.compare.structuremergeviewer.DiffNode;
import org.eclipse.compare.structuremergeviewer.Differencer;
import org.eclipse.compare.structuremergeviewer.IDiffContainer;
import org.eclipse.compare.structuremergeviewer.IStructureComparator;
import org.plmxml.sdk.plmxml.Handle;
import org.plmxml.sdk.plmxml.ObjectBase;
import org.plmxml.sdk.plmxml.PLMXMLException;
import org.plmxml.sdk.plmxml.plmxml60.Document;
import org.plmxml.sdk.plmxml.plmxml60.Occurrence;
import org.plmxml.sdk.plmxml.plmxml60.ProductView;

import cadexstructurecompare.model.interfaces.ICadexGenericDataset;
import cadexstructurecompare.model.interfaces.ICadexGenericLineObject;
import cadexstructurecompare.model.interfaces.ICadexGenericObject;
import cadexstructurecompare.model.interfaces.ICadexGenericRevision;
import cadexstructurecompare.model.plmxml.PlmxmlOccurence;

import com.structurecompare.core.CadexCompareEditorInput.AttributeNode;
import com.structurecompare.core.CadexCompareEditorInput.CompareNode;
import com.structurecompare.core.CadexCompareEditorInput.DatasetNode;
import com.structurecompare.core.CadexCompareEditorInput.MyDiffNode;

public class CadexCompareEditorInput {
	
	private PlmxmlOccurence root1 ;
	private PlmxmlOccurence root2 ;
	private DiffNode diffNode ;
	private CompareNode compareNode = null ;
	private int seqNo = 0 ;
	public static int LEFT_SIDE = 1;
	public static int RIGHT_SIDE = 2;
	 

	public DiffNode getDiffNode() {
		return diffNode;
	}

	public CadexCompareEditorInput(String file1, String file2) throws Exception {
		
		root1 = initializeFromInputPLMXML(file1);
		root2 = initializeFromInputPLMXML(file2);
		prepareInput();
	}
	
	public final PlmxmlOccurence initializeFromInputPLMXML(String inputPLMXMLPath) throws Exception 
	{ 
		String caeDeckStructId = null;
		ObjectBase[] prodViewList = null;
		Document m_xmlDoc = readDocument(inputPLMXMLPath);
		ProductView mainProdcutView ;
	    
		prodViewList = (ObjectBase[]) m_xmlDoc.getElementsByClass("ProductView");			
		
		mainProdcutView = (ProductView) prodViewList[0];
		
		caeDeckStructId = mainProdcutView.getAttributeValueAsString("rootRefs");
		
		Occurrence m_caeDeckOcc = (Occurrence) m_xmlDoc.resolveId(caeDeckStructId);		
		
		return new PlmxmlOccurence(m_caeDeckOcc);
	}
	
	public static Document readDocument(final String xmlFileUri) throws IllegalArgumentException, PLMXMLException 
	{
	      Document docElement = null;

	      Handle[] hdl = ObjectBase.load(xmlFileUri);

          if ((hdl != null) && (hdl.length > 0)) {
              docElement = (Document) hdl[0].getObject();
          }

	      return docElement;
	    }

	public PlmxmlOccurence getRoot1() {
		return root1;
	}
	
	public PlmxmlOccurence getRoot2() {
		return root2;
	}
	
	protected void prepareInput()	throws InvocationTargetException, InterruptedException 
	{
		
		Differencer d = new Differencer() 
		{
			int nodeTraverse = 0 ;
			CompareNode currentNode ;
			CompareNode parentNode ;
			HashMap<String, CompareNode> compareNodes = new HashMap<String, CompareNode>();
			
			@Override
			protected Object visit(Object parent, int description, Object ancestor, Object left, Object right) {
				MyDiffNode diff = new MyDiffNode((IDiffContainer) parent, description, (ITypedElement) ancestor, (ITypedElement)left, (ITypedElement)right);
				String nodeUid = getNodeUid(ancestor, left, right);
				
				for(Entry<String, CompareNode> entry : compareNodes.entrySet())
				{
					if( entry.getKey().startsWith( nodeUid ) && ((entry.getValue().leftNode ==null) || (entry.getValue().rightNode ==null)))
					{
						entry.getValue().setDifNode(diff);
					}
				}
				
				return diff;
			}
			
			@Override
			protected boolean contentsEqual(Object input1, Object input2) {
				boolean compareResult = super.contentsEqual(input1, input2);
				return compareResult;
			}
			
			@Override
			protected Object[] getChildren(Object input) 
			{
				Object[] objs = super.getChildren(input);
				
				System.out.println( "Getting Children for Tree " + nodeTraverse + " - - " + input);

				if(nodeTraverse==0)
				{
					currentNode = new CompareNode((ICadexGenericLineObject) input, null, null);  
					
					if(compareNode == null)
						compareNode = this.currentNode;
					
					nodeTraverse++;
				}
				else if(nodeTraverse==1)
				{
					if(currentNode != null)
						currentNode.setRightNode((ICadexGenericLineObject) input);
					
					nodeTraverse++;
				}
				else
				{
					if(currentNode != null)
						currentNode.setLeftNode((ICadexGenericLineObject) input);
					
					if(parentNode != null && !currentNode.nodeUid.equals( parentNode.nodeUid ))	
					{
						parentNode.addChildNode( currentNode );
					}
					
					if(currentNode.childAvailable && currentNode.leftNode != null && currentNode.rightNode != null)
						parentNode = currentNode;
					else
						revertParent();
					
					currentNode.setAttributes();
					compareNodes.put( currentNode.nodeUid + "|" + currentNode.seqNo , currentNode);
					
					if((currentNode.leftNode == null || currentNode.rightNode == null ) && currentNode.childAvailable )
					{
						createDifferenceCompNodes();
					}
					
					nodeTraverse-=2; 
				}
				
				return objs;
			}
			
			private void revertParent()
			{
				if (parentNode != null) {
					ICadexGenericLineObject ancestorNode = parentNode
							.getAncestorNode();
					ICadexGenericLineObject leftNode = parentNode.getLeftNode();
					ICadexGenericLineObject rightNode = parentNode
							.getRightNode();
					int leftSize = leftNode != null ? leftNode.returnChildren() != null ? leftNode
							.returnChildren().size() : 0
							: 0;
					int rightSize = rightNode != null ? leftNode
							.returnChildren() != null ? rightNode
							.returnChildren().size() : 0 : 0;
					int ancestorSize = ancestorNode != null ? ancestorNode
							.returnChildren() != null ? ancestorNode
							.returnChildren().size() : 0 : 0;
					int allSize = leftSize > rightSize ? leftSize > ancestorSize ? leftSize
							: ancestorSize > rightSize ? ancestorSize
									: rightSize
							: rightSize > ancestorSize ? rightSize
									: ancestorSize;
					if ((parentNode.getChildNodes().size() == allSize)) {
						parentNode = parentNode.getParentCompNode();
						revertParent();
					}
				}
			}
			
			private void createDifferenceCompNodes()
			{
				if(currentNode.leftNode != null)
					addCompareChild(currentNode, LEFT_SIDE);
				else
					addCompareChild(currentNode, RIGHT_SIDE);
			}
			
			private void addCompareChild(CompareNode curNode, int side)
			{
				ArrayList<ICadexGenericLineObject> lines = side == LEFT_SIDE ? curNode.leftNode.returnChildren() : curNode.rightNode.returnChildren();
				
				for(ICadexGenericLineObject line :  lines)
				{
					CompareNode newNode = new CompareNode( null , side == LEFT_SIDE ? line : null, side == RIGHT_SIDE ? line : null);
					curNode.addChildNode( newNode );
					compareNodes.put( newNode.nodeUid + "|" + newNode.seqNo , newNode);
					addCompareChild(newNode, side);
				}
			}
			
			private String getNodeUid(Object ancestor, Object left, Object right)
			{
				String retUid = "" ;
				
				if( ancestor != null && ancestor instanceof ITypedElement  )
					retUid = ((ITypedElement)ancestor).getName();
				
				if( retUid.length() == 0 && left != null && left instanceof ITypedElement  )
					retUid = ((ITypedElement)left).getName();
				
				if( retUid.length() == 0 && right != null && right instanceof ITypedElement  )
					retUid = ((ITypedElement)right).getName();
				
				return retUid;
			}
			
			
		};
			
		diffNode = (DiffNode) d.findDifferences(false, monitor, null, null, root1, root2);
		System.out.println("");
	}
	
	class MyDiffNode extends DiffNode 
	{
		private boolean fDirty= false;
		private ITypedElement fLastId;
		private String fLastName;
		
		
		public MyDiffNode(IDiffContainer parent, int description, ITypedElement ancestor, ITypedElement left, ITypedElement right) {
			super(parent, description, ancestor, left, right);
		}
	
		void clearDirty() {
			fDirty= false;
		}
		public String getName() {
			if (fLastName == null)
				fLastName= super.getName();
			if (fDirty)
				return '<' + fLastName + '>';
			return fLastName;
		}
		
		public ITypedElement getId() {
			ITypedElement id= super.getId();
			if (id == null)
				return fLastId;
			fLastId= id;
			return id;
		}
	}
	
	public class CompareNode 
	{
		private ICadexGenericLineObject ancestorNode ;
		private ICadexGenericLineObject leftNode ; 
		private ICadexGenericLineObject rightNode ;
		private MyDiffNode difNode ;
		private boolean isDiff ;
		private String nodeUid = "" ;
		private int seqNo  = 0 ;
		private CompareNode parentCompNode = null ;
		private List<CompareNode> childNodes = new ArrayList<CompareNode>();
		private List<AttributeNode> attrNodes = new ArrayList<AttributeNode>();
		private List<DatasetNode> datasetNodes = new ArrayList<DatasetNode>();
		private boolean childAvailable = false ;
		
		public CompareNode(ICadexGenericLineObject ancestorNode, ICadexGenericLineObject leftNode, ICadexGenericLineObject rightNode) {
			this.leftNode = leftNode;
			this.rightNode = rightNode;
			this.setAncestorNode(ancestorNode);
			this.seqNo = ++CadexCompareEditorInput.this.seqNo;
			
			if(this.ancestorNode != null)
				this.nodeUid = this.ancestorNode.toString();
		}

		public MyDiffNode getDifNode() {
			return difNode;
		}

		public void setDifNode(MyDiffNode difNode) {
			this.difNode = difNode;
			if(this.difNode != null)
				this.isDiff = true;
		}

		public ICadexGenericLineObject getLeftNode() {
			return leftNode;
		}
		
		public ICadexGenericLineObject getRightNode() {
			return rightNode;
		}

		public List<CompareNode> getChildNodes() {
			return childNodes;
		}

		public ICadexGenericLineObject getAncestorNode() {
			return ancestorNode;
		}

		public void addChildNode(CompareNode childNode) {
			this.childNodes.add( childNode );
			childNode.setParentCompNode(this);
		}
		
//		@Override
//		public String toString() {
//			return ancestorNode + " <--> " + leftNode + " <--> " + rightNode ;
//		}

		public void setLeftNode(ICadexGenericLineObject leftNode) 
		{
			this.leftNode = leftNode;
			this.setChildAvailable(this.leftNode);
			
			if(this.leftNode != null)
			{
				if( this.nodeUid.length() == 0 )
				{
					this.nodeUid = this.leftNode.toString();
				}
				
				populateDatasets(this.leftNode, LEFT_SIDE);
			}
		}

		private void populateDatasets(ICadexGenericLineObject leftNode2, int side) {
			ICadexGenericRevision rev = leftNode2.getLineRev();
			if(rev!=null)
			{
				DatasetNode node = new DatasetNode( side==LEFT_SIDE ? "LEFT" : side==RIGHT_SIDE ? "RIGHT" : "ANCESTOR" );
				node.setDsets( rev.getAttaches() );
				this.datasetNodes.add( node );
			}
		}

		public void setRightNode(ICadexGenericLineObject rightNode) 
		{
			this.rightNode = rightNode;
			this.setChildAvailable(this.rightNode);
			
			if(this.rightNode != null)
			{
				if( this.nodeUid.length() == 0)
				{
					this.nodeUid = this.rightNode.toString();
				}
				
				populateDatasets(this.rightNode, RIGHT_SIDE);
			}
		}

		public boolean isChildAvailable() {
			return childAvailable;
		}

		public void setChildAvailable(Object input) {
			
			Object[] objs = this.getChildren(input);
			
			if(!this.childAvailable)
				this.childAvailable =  objs !=null && objs.length > 0 ? true : false ;
		}

		public CompareNode getParentCompNode() {
			return parentCompNode;
		}

		private void setParentCompNode(CompareNode parentCompNode) {
			this.parentCompNode = parentCompNode;
		}
		
		protected Object[] getChildren(Object input) {
			if (input instanceof IStructureComparator)
				return ((IStructureComparator)input).getChildren();
			return null;
		}

		public void setAncestorNode(ICadexGenericLineObject ancestorNode2) {
			this.ancestorNode = ancestorNode2;
			this.setChildAvailable(this.ancestorNode);
		}
		
		public void setAttributes()
		{
			AttributeNode bomAttr = new AttributeNode( "BOM" , this , 1);
			AttributeNode itemAttr = new AttributeNode( "Item" , this , 2);
			AttributeNode revAttr = new AttributeNode( "Revision" , this , 3);
			this.attrNodes.add(bomAttr);
			this.attrNodes.add(itemAttr);
			this.attrNodes.add(revAttr);
		}

		public List<AttributeNode> getAttrNodes() {
			return attrNodes;
		}

		public List<DatasetNode> getDatasetNodes() {
			return datasetNodes;
		}

		public boolean isDiff() {
			return isDiff;
		}
	}
	
	public class DatasetNode
	{
		String sideName = "";
		ArrayList<ICadexGenericDataset> dsets = new ArrayList<ICadexGenericDataset>();
		public DatasetNode(String name) {
			this.sideName = name;
		}
		public void setDsets(ArrayList<ICadexGenericDataset> dsets) {
			if(dsets !=null)
				this.dsets.addAll(dsets);
		}
		public ArrayList<ICadexGenericDataset> getDsets() {
			return dsets;
		}
		public String getSideName() {
			return sideName;
		}
	}
	
	public class AttributeNode
	{
		String attrName = "" ;
		String attrValueL = "" ;
		String attrValueR = "" ;
		String attrValueA = "" ;
		int attrType ;
		public static final int BOM_ATTR_TYPE = 1;
		public static final int ITEM_ATTR_TYPE = 2;
		public static final int REV_ATTR_TYPE = 3;
		List<AttributeNode> childNodes = new ArrayList<CadexCompareEditorInput.AttributeNode>();
		
		public AttributeNode(String attrName, CompareNode obj, int type) {
			this(attrName,obj.getLeftNode(),obj.getRightNode(),obj.getAncestorNode());
			this.attrType = type;			
			setAttributes(obj);
		}
		
		private void setAttributes(CompareNode obj)
		{
			switch(this.attrType)
			{
				case BOM_ATTR_TYPE:
					setAttribute( obj.getLeftNode() , obj.getRightNode(), obj.getAncestorNode());
					break;
				case ITEM_ATTR_TYPE:
				{
					setAttribute( getLineItem(obj.getLeftNode()) , getLineItem(obj.getRightNode()), getLineItem(obj.getAncestorNode()));
					break;
				}
				case REV_ATTR_TYPE:
				{
					setAttribute( getLineRev(obj.getLeftNode()) , getLineRev(obj.getRightNode()), getLineRev(obj.getAncestorNode()));
					break;
				}
				default:
					break;
			}
		}
		
		private void setAttribute(ICadexGenericObject iCadexGenericObject, ICadexGenericObject iCadexGenericObject2, ICadexGenericObject iCadexGenericObject3 )
		{
			Set<String> globNames = new HashSet<String>(); 
			
			if(iCadexGenericObject != null)
				globNames.addAll(((ICadexGenericObject) iCadexGenericObject).getAttrNames());
			if(iCadexGenericObject2 != null)
				globNames.addAll(((ICadexGenericObject) iCadexGenericObject2).getAttrNames());
			if(iCadexGenericObject3 != null)
				globNames.addAll(((ICadexGenericObject) iCadexGenericObject3).getAttrNames());
			
			for(String globString : globNames)
			{
				this.childNodes.add( new AttributeNode(globString, iCadexGenericObject, iCadexGenericObject2,iCadexGenericObject3) );
			}
		}
		
		public AttributeNode(String attrName) {
			this.attrName = attrName;
		}
		
		public AttributeNode(String attrName, ICadexGenericObject iCadexGenericObject, ICadexGenericObject iCadexGenericObject2, ICadexGenericObject iCadexGenericObject3) {
			this(attrName);
			this.attrValueL = iCadexGenericObject != null ? ((ICadexGenericObject) iCadexGenericObject).getAttr(attrName) : "" ;
			this.attrValueR = iCadexGenericObject2 != null ? ((ICadexGenericObject) iCadexGenericObject2).getAttr(attrName) : "" ;
			this.attrValueA = iCadexGenericObject3 != null ? ((ICadexGenericObject) iCadexGenericObject3).getAttr(attrName) : "" ;
		}

		public String getAttrName() {
			return attrName;
		}

		public String getAttrValueL() {
			return attrValueL;
		}

		public String getAttrValueR() {
			return attrValueR;
		}

		public String getAttrValueA() {
			return attrValueA;
		}
		
		@Override
		public boolean equals(Object arg0) {
			if( arg0 != null && arg0 instanceof AttributeNode )
				return ((AttributeNode)arg0).getAttrName().equals( this.attrName ); 
			else
				return false ;
		}
		
		private ICadexGenericObject getLineItem(ICadexGenericLineObject line)
		{
			if( line != null && line.getLineRev() != null )
			{
				return (ICadexGenericObject) line.getLineRev().getRevItem();
			}
			else
				return null;	
		}
		
		private ICadexGenericObject getLineRev(ICadexGenericLineObject line)
		{
			if( line != null )
			{
				return (ICadexGenericObject) line.getLineRev();
			}
			else
				return null;	
		}

		public List<AttributeNode> getChildNodes() {
			return childNodes;
		}
	}

	public CompareNode getCompareNode() {
		return compareNode;
	}
	
}

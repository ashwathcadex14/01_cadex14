/*=================================================================================================                    
#                Copyright (c) 2014 BAVIS Technology Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           Activator.java          
#      Module          :           com.teamcenter.rac.tccadex          
#      Description     :           Activator for the RAC plugin          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex;

import org.osgi.framework.BundleContext;

import com.teamcenter.rac.kernel.AbstractRACPlugin;
import com.teamcenter.rac.services.IAspectService;
import com.teamcenter.rac.services.IAspectUIService;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractRACPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "com.teamcenter.rac.tccadex"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;
	
	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
//		setupServices(context);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	@Override
	public IAspectService getLogicService() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IAspectUIService getUIService() {
		// TODO Auto-generated method stub
		return null;
	}
	
//	protected void setupServices(BundleContext context) 
//	{
//		// TODO Auto-generated method stub
//	    Dictionary<String, String> properties = new java.util.Hashtable<String, String>();
//	    // Register the open service
//	    properties.put( IServiceConstants.PERSPECTIVE,
//	    		"com.teamcenter.rac.tccadex.perspectives.cadexPerspective" );
//	    PackageOpenService customOpenService = new PackageOpenService();
//	    context.registerService( IOpenService.class.getName(),
//	            customOpenService, properties );
//	
//	}
	
}

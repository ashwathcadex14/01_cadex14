/*=================================================================================================                    
#                Copyright (c) 2014 Some Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           EarlyStartup.java          
#      Module          :           com.teamcenter.rac.tccadex          
#      Description     :           Early startup mechanism for overriding Eclipse cut, copy, paste, delete commands          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex;

import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IExecutionListener;
import org.eclipse.core.commands.NotHandledException;
import org.eclipse.ui.IStartup;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.ICommandService;

import com.teamcenter.rac.tccadex.handlers.GlobalOverrideHandler;

public class EarlyStartup implements IStartup {
	
	public static final String DELETE_COMMAND_ID 	= "org.eclipse.ui.edit.delete";
	public static final String CUT_COMMAND_ID 		= "org.eclipse.ui.edit.cut";
	public static final String PASTE_COMMAND_ID 	= "org.eclipse.ui.edit.paste";
	public static final String CADEX_VIEW_ID       = "com.teamcenter.rac.tccadex.views.cadexView";
	public static final String DATASET_VIEW_ID    = "com.teamcenter.rac.tccadex.views.datasetView";
	public static final String HISTORY_VIEW_ID    = "com.teamcenter.rac.tccadex.views.pkgHistory";
	public static final String PROBLEMS_VIEW_ID    = "com.teamcenter.rac.tccadex.views.packageProblemsView";
	public static final String ADD_TARGET_COMMAND_ID       = "com.teamcenter.rac.tccadex.commands.addTarget";
	public static final String ADD_REFERENCE_COMMAND_ID       = "com.teamcenter.rac.tccadex.commands.addReference";
	
	@Override
	public void earlyStartup() 
	{
		//final IHandlerService service = (IHandlerService) PlatformUI.getWorkbench().getService(IHandlerService.class);

		final ICommandService commandService = (ICommandService) PlatformUI.getWorkbench().getService(ICommandService.class);

		//service.activateHandler(DELETE_COMMAND_ID, new GlobalOverrideHandler() , Expression.TRUE);
		
		commandService.addExecutionListener(new IExecutionListener(){

			@Override
			public void notHandled(String arg0, NotHandledException arg1) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void postExecuteFailure(String arg0, ExecutionException arg1) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void postExecuteSuccess(String arg0, Object arg1) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void preExecute(String id, ExecutionEvent arg1) 
			{
				
				if ( PASTE_COMMAND_ID.equals(id) && isCadexView() ) 
				{
					overrideHandlers(id);
				}
				else if( DELETE_COMMAND_ID.equals(id) && isCadexView() )
				{
					overrideHandlers(id);
				}
				else if( CUT_COMMAND_ID.equals(id) && isCadexView() )
				{
					overrideHandlers(id);
				}
					
					
			}
			
			private boolean isCadexView()
			{
				IWorkbenchPartReference currPart = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActivePartReference();
				
				if( currPart.getId().equals(CADEX_VIEW_ID) )
				{
					return true;
				}
				else
					return false;
			}
			
			private void overrideHandlers(String id)
			{
				GlobalOverrideHandler handler = new GlobalOverrideHandler();
				Command command = commandService.getCommand(id);
				command.setHandler(handler);
			}
						
		});

	}

}

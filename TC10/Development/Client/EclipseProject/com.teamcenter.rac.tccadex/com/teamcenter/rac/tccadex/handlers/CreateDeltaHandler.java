package com.teamcenter.rac.tccadex.handlers;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import cadexstructurecompare.delta.infoexplmxml.InfoExGenerateDelta;
import cadexstructurecompare.delta.plmxml.GenerateDelta;
import cadexstructurecompare.dialogs.CadexErrorDialog;
import cadexstructurecompare.views.StructureCompare;
import cadexstructurecompare.views.structurecompare.CompareNode;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentTcFile;
import com.teamcenter.rac.tccadex.EarlyStartup;
import com.teamcenter.rac.tccadex.utils.CadexUtils;
import com.teamcenter.rac.tccadex.utils.FileUtils;
import com.teamcenter.rac.tccadex.views.CadexView;

public class CreateDeltaHandler extends AbstractHandler {

	public CreateDeltaHandler() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindow(event);
		CadexView cadexView = (CadexView) window.getActivePage().findViewReference( EarlyStartup.CADEX_VIEW_ID ).getView(true);
		StructureCompare compareView = (StructureCompare) window.getActivePage().findViewReference( StructureCompare.ID ).getView(true);
		
		try 
		{
				TCComponentItemRevision pkgRev = cadexView.getTargetRev();
			
				if(pkgRev!=null)
				{
					CompareNode cmpNode = compareView.getNewInput().getCompareNode();
					
					File compareDir = new File(System.getProperty("java.io.tmpdir") + "\\cxdelta");
					if(compareDir.exists())
					{
						FileUtils.deleteFolder(compareDir, true);
						compareDir.mkdir();
					}
					else
						compareDir.mkdir();
					
					String fil1 = compareDir.getAbsolutePath() + "\\delta.xml";
					
//					new GenerateDelta( fil1 , cmpNode);
					new InfoExGenerateDelta(fil1, cmpNode);
					
//					ArrayList<File> allFiles = new ArrayList<File>();
//					
//					CadexUtils.getAllFiles( compareDir , allFiles);
//					
//					for (File file : allFiles) 
//					{
//						if(file.getAbsolutePath().endsWith( ".plmxml" ))
//						{
//							CadexUtils.replaceText(fil1, file.getName() , "");
//							break;
//						}
//					}
					
					
					TCComponentDataset inPkg = (TCComponentDataset) pkgRev.getRelatedComponent("CE4_ExPkgIncomingPackage");
					
					if(inPkg!=null)
					{
						
						TCComponent[] refs1 = inPkg.getNamedReferences();
						if(refs1.length==1)
						{
							String zipExtract = compareDir + "\\" + refs1[0].getStringProperty( "original_file_name" );
							((TCComponentTcFile)refs1[0]).getFile(compareDir.getAbsolutePath(),refs1[0].getStringProperty( "original_file_name" ));
							
							File[] files = new File[1];
							files[0] = new File(fil1);
							CadexUtils.addFilesToExistingZip(new File(zipExtract), files);
							inPkg.removeNamedReference( "ZIPFILE" );
							String[] filePaths = { zipExtract };
							String[] fileTypes = { "ImanFile" };
							String[] fileSubTypes = { "" };
							String[] namedRefs = { "ZIPFILE" };
							inPkg.setFiles(filePaths, fileTypes, fileSubTypes, namedRefs);
							MessageDialog.openInformation(window.getShell(), "Validation Completed", "Validation completed and delta created sucessfully");
						}
					}			
			}
					
			
			
			
		} catch (Exception e) {
			CadexUtils.openError(window.getShell(), "Error", "Error ocurred during operation.\n" , e);
		}	
		
		return null;
	}

}
/*=================================================================================================                    
#                Copyright (c) 2014 Some Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           ExportPackageHandler.java          
#      Module          :           com.teamcenter.rac.tccadex.handlers          
#      Description     :           To expor the exchange package          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import com.teamcenter.rac.tccadex.EarlyStartup;
import com.teamcenter.rac.tccadex.utils.CadexUtils;
import com.teamcenter.rac.tccadex.views.CadexView;

public class ExportPackageHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event)	throws ExecutionException {
		
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindow(event);
		CadexView cadexView = (CadexView) window.getActivePage().findViewReference( EarlyStartup.CADEX_VIEW_ID ).getView(true);
		
		try 
		{
			cadexView.exportPackage();
		} catch (Exception e) {
			CadexUtils.openError(window.getShell(), "Error", "Error ocurred during operation.\n" , e);
		}	
		
		return null;
	}



}

/*=================================================================================================                    
#                Copyright (c) 2014 Some Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           GlobalOverrideHandler.java          
#      Module          :           com.teamcenter.rac.tccadex.handlers          
#      Description     :           Global handler for all cut, copy, paste operations in CADEX View          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponentBOMLine;
import com.teamcenter.rac.tccadex.EarlyStartup;
import com.teamcenter.rac.tccadex.utils.CadexUtils;
import com.teamcenter.rac.tccadex.views.CadexView;

public class GlobalOverrideHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindow(event);
		String commandId = event.getCommand().getId();
		
		CadexView cadexView = (CadexView) window.getActivePage().findViewReference( EarlyStartup.CADEX_VIEW_ID ).getView(true);
		
		try 
		{
			TCComponentBOMLine currentLine = getTargetObject();
			
			if(commandId.equals( EarlyStartup.PASTE_COMMAND_ID ))
			{
				if(currentLine.parent() == null)
				{
					cadexView.pasteObjects(null);
				}
				else
					throw new Exception("Cannot paste under the selected object. Individual assemblies are not editable in the Cadex View. Please use Structure Manager.");
			}
			else if( commandId.equals( EarlyStartup.CUT_COMMAND_ID ) )
			{
				if(currentLine.parent().getStringProperty( "bl_item_object_type" ).equals( "CE4_ExPkg" ))
				{
					cadexView.removeObjects();
				}
				else
					throw new Exception("Cannot cut the selected object. Individual assemblies are not editable in the Cadex View. Please use Structure Manager.");
			}
			else if( commandId.equals( EarlyStartup.ADD_TARGET_COMMAND_ID ) )
			{
				cadexView.pasteObjects("Target");
			}
			else if( commandId.equals( EarlyStartup.ADD_REFERENCE_COMMAND_ID ) )
			{
				cadexView.pasteObjects("Reference");
			}
			else
				throw new Exception( "A Object cannot be deleted from Cadex View. Please use My Teamcenter or Structure Manager.");
				
		} catch (Exception e) {
			CadexUtils.openError(window.getShell(), "Error", "Error ocurred during operation.\n" , e);
		}	
		
		return null;
	}
	
	private TCComponentBOMLine getTargetObject()
	{
		return (TCComponentBOMLine) AIFUtility.getCurrentApplication().getTargetComponent();
	}
	


}

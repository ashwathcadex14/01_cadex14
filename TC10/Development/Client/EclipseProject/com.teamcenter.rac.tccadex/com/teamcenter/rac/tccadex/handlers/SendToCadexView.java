/*=================================================================================================                    
#                Copyright (c) 2014 Some Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           SendToCadexView.java          
#      Module          :           com.teamcenter.rac.tccadex.handlers          
#      Description     :           Send to Handler for opening Exchange package in CADEX View          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;

import com.teamcenter.rac.tccadex.utils.CadexUtils;

public class SendToCadexView extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event)
			throws org.eclipse.core.commands.ExecutionException {
		IWorkbench workbench = PlatformUI.getWorkbench();
        IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();
        try {
                    workbench.showPerspective( "com.teamcenter.rac.tccadex.perspectives.cadexPerspective", window, null );
           } catch (WorkbenchException e) {
                    CadexUtils.openError(window.getShell(), "Open Exchange Package error", "Failed to open the Exchange Package.", e);
           }
		return null;
	}


}

package com.teamcenter.rac.tccadex.handlers;

import java.io.File;
import java.io.InputStream;
import java.util.zip.ZipFile;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import cadexstructurecompare.views.StructureCompare;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentTcFile;
import com.teamcenter.rac.tccadex.EarlyStartup;
import com.teamcenter.rac.tccadex.utils.CadexUtils;
import com.teamcenter.rac.tccadex.utils.FileUtils;
import com.teamcenter.rac.tccadex.views.CadexView;

public class StartCompareHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindow(event);
		CadexView cadexView = (CadexView) window.getActivePage().findViewReference( EarlyStartup.CADEX_VIEW_ID ).getView(true);
		StructureCompare compareView = (StructureCompare) window.getActivePage().findViewReference( StructureCompare.ID ).getView(true);
		
		try 
		{
			TCComponentItemRevision pkgRev = cadexView.getTargetRev();
			
			if(pkgRev!=null)
			{
				TCComponentDataset outPkg = (TCComponentDataset) pkgRev.getRelatedComponent("CE4_ExPkgOutgoingPackage");
				TCComponentDataset inPkg = (TCComponentDataset) pkgRev.getRelatedComponent("CE4_ExPkgIncomingPackage");
				
				if(outPkg!=null && inPkg!=null)
				{
					TCComponent[] refs1 = outPkg.getNamedReferences();
					TCComponent[] refs2 = inPkg.getNamedReferences();
					File zip1 = null;
					if(refs1.length==1)
					{
						zip1 = ((TCComponentTcFile)refs1[0]).getFmsFile();
					}
					File zip2 = null;
					if(refs2.length==1)
					{
						zip2 = ((TCComponentTcFile)refs2[0]).getFmsFile();
					}
					
					if(zip1.exists() && zip2.exists())
					{
						ZipFile zipFile1 = new ZipFile(zip1.getAbsolutePath());
						
						File compareDir = new File(System.getProperty("java.io.tmpdir") + "\\cxcompare");
						if(compareDir.exists())
							compareDir.delete();
						else
							compareDir.mkdir();
						
						String fil1 = compareDir.getAbsolutePath() + "\\file1.xml";
						String fil2 = compareDir.getAbsolutePath() + "\\file2.xml";
						
						if(zipFile1!=null)
						{
							InputStream stream1 = FileUtils.getFileFromZip(zipFile1);
							FileUtils.writeInputStream( stream1 , fil1);
							stream1.close();
						}
						
						ZipFile zipFile2 = new ZipFile(zip2.getAbsolutePath());
						
						if(zipFile2!=null)
						{
							InputStream stream1 = FileUtils.getFileFromZip(zipFile2);
							FileUtils.writeInputStream( stream1 , fil2);
							stream1.close();
						}
						
						if(new File(fil1).exists() && new File(fil2).exists())
						{
							PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView( StructureCompare.ID );
							compareView.startCompare(fil1, fil2);
						}
					}
					
				}
			
			}
			
			
			
		} catch (Exception e) {
			CadexUtils.openError(window.getShell(), "Error", "Error ocurred during operation.\n" , e);
		}	
		
		return null;
	}

}

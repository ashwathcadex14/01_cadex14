/*=================================================================================================                    
#                Copyright (c) 2014 Some Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           PackageOpenService.java          
#      Module          :           com.teamcenter.rac.tccadex.services          
#      Description     :           For opening of Exchange Package in CADEX View (Obselete)          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.services;

import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.services.IOpenService;

public class PackageOpenService implements IOpenService {

	@Override
	@Deprecated
	public boolean close() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean open(InterfaceAIFComponent arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean open(InterfaceAIFComponent[] arg0) {
		// TODO Auto-generated method stub
		return false;
	}

}

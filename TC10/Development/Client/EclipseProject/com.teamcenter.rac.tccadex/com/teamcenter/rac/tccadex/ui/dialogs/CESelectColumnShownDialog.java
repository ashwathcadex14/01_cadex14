/*=================================================================================================                    
#                Copyright (c) 2014 Some Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CESelectColumnShownDialog.java          
#      Module          :           com.teamcenter.rac.tccadex.ui.dialogs          
#      Description     :           OOTB Overriden dialog to show column configuration in CADEX View(s)          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.ui.dialogs;

import java.awt.Frame;

import com.teamcenter.rac.common.table.InsertColumnDialog;
import com.teamcenter.rac.kernel.TCSession;

public class CESelectColumnShownDialog extends InsertColumnDialog {

	public CESelectColumnShownDialog(Frame frame,TCSession arg0, String type, String pref) {
		super(frame, arg0, type,pref,false,true,null);
		this.saveButton.setVisible(false);
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8244059048256698381L;

}

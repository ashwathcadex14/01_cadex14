/**=================================================================================================                    
#                Copyright (c) 2014 BAVIS Technology Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CadexErrorDialog.java          
#      Module          :           com.teamcenter.siemens.briefcase.utils          
#      Description     :           Customized Error dialog to show errors related to this plugin          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Feb-2013                         Ashwath                              Initial Creation
#  07-Mar-2014                       Ashwath                            Added Send Error Report support
#  $HISTORY$                    
#  =================================================================================================*/                    

package com.teamcenter.rac.tccadex.ui.dialogs;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;

import com.teamcenter.rac.tccadex.utils.CadexUtils;
import com.teamcenter.rac.tccadex.utils.FileUtils;

/**
 * @author tvcpt3
 *
 */
public class CadexErrorDialog extends ErrorDialog {

	public static int SEND_ERROR_REPORT = 99;
	
	/**
	 * @param parentShell
	 * @param dialogTitle
	 * @param message
	 * @param status
	 * @param displayMask
	 */
	private IStatus status;
    
    /**
     * Constructor
     */
    public CadexErrorDialog(Shell parentShell, String dialogTitle,
                    String message, IStatus status, int displayMask) {
            super(parentShell, dialogTitle, message,status, displayMask);
            this.status = status;
    }
   
    /**
     * Opens the ErrorWithExceptionDialog - Maintain same API as ErrorDialog
     */
    public static int openError(Shell parentShell, String title,
                    String message, IStatus status) {
    	CadexErrorDialog dialog = new CadexErrorDialog(
                            parentShell, title, message, status,status.getSeverity());
            return dialog.open();
    }

    @Override
    protected List createDropDownList(Composite parent) {
            List rtc = super.createDropDownList(parent);
            if ( status != null && status.getException() != null ) {
                    for (StackTraceElement ste : status.getException().getStackTrace()) {
                            rtc.add( "    " + ste.toString() );
                    }
            }
            return rtc;
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.jface.dialogs.ErrorDialog#createButtonsForButtonBar(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected void createButtonsForButtonBar(Composite parent) {
    	// TODO Auto-generated method stub
    	super.createButtonsForButtonBar(parent);
		createButton(parent, SEND_ERROR_REPORT,
				"Send Error Report", false);
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.jface.dialogs.ErrorDialog#buttonPressed(int)
     */
    @Override
    protected void buttonPressed(int id) {
    	if(id != SEND_ERROR_REPORT)
    		super.buttonPressed(id);
    	else
    		sendErrorReport();
    }
    
    private void sendErrorReport()
    {
    	try {
			
			String[] zipExt = { "*.zip" } ;
			SimpleDateFormat newDateFormat = new SimpleDateFormat( "ddMMyyyy_HHmmss" ) ;
			String timeStamp = newDateFormat.format( Calendar.getInstance().getTime() ) ;
			File logFile = OpenFileDialog.openDialogForSave(this.getParentShell(), "Save Logs" , zipExt ,  "bb_logs_" + timeStamp + ".zip" , "");
			CadexUtils.createZip( new File("./logs" ));
			File genZipFile = new File( "./logs.zip" );
			if( genZipFile.exists() )
			{
				FileUtils.copyFile( genZipFile , logFile );
				genZipFile.delete();
		        Clipboard clipboard = new Clipboard(this.getShell().getDisplay());
		        TextTransfer textTransfer = TextTransfer.getInstance();
		        clipboard.setContents(new String[]{logFile.getParent()}, new Transfer[]{textTransfer});
		        clipboard.dispose();
				MessageDialog.openInformation( this.getParentShell() , "Logs", "Log file archived to " + logFile.getAbsolutePath() + "\nLocation copied to clipboard." );
			}
			else
				throw new Exception( "Cannot create the logs zip file." ); 
    	} catch (Exception e) {
    		String log = System.getProperty( "user.dir" ) + "\\logs";
			Clipboard clipboard = new Clipboard(this.getShell().getDisplay());
	        TextTransfer textTransfer = TextTransfer.getInstance();
	        clipboard.setContents(new String[]{log}, new Transfer[]{textTransfer});
	        clipboard.dispose();
	        MessageDialog.openError( this.getParentShell() , "Log Error", "Could not export the log files. Alternatively you can zip the directory " +  log + "\nLocation is copied to clipboard."  );
    	}    	
    }
}

/**=================================================================================================                    
#                Copyright (c) 2014 BAVIS Technology Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           OpenFileDialog.java          
#      Module          :           com.teamcenter.siemens.briefcase.editors.open          
#      Description     :           Default File Open Dialog for this plugin          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Feb-2013                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.ui.dialogs;

import org.apache.log4j.Logger;
import java.io.File;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

@SuppressWarnings("unused")
public class OpenFileDialog {
	
	private static final Logger logger = Logger.getLogger(OpenFileDialog.class);

	public static File[] openDialogForFile( Shell parent , String dialogTitle , String[] extensions , String defaultFile , String filterPath, boolean multiSelect )
	{
	    logger
				.info(" ----- >> Entering Function openDialogForFile with parameters ...."
						+ "parent - "
						+ parent
						+ "dialogTitle - "
						+ dialogTitle
						+ "extensions - "
						+ extensions
						+ "defaultFile - "
						+ defaultFile
						+ "filterPath - "
						+ filterPath
						+ "multiSelect - " + multiSelect);
		FileDialog dialog = new FileDialog( parent , multiSelect ? SWT.MULTI : SWT.NULL ) ;
	    dialog.setText( dialogTitle ) ;
	    dialog.setFilterExtensions( extensions ) ;
	    dialog.setFileName( defaultFile ) ;
	    dialog.setFilterPath(filterPath);
		String path = dialog.open();
	    String names[] = dialog.getFileNames();
	    if (names != null && names.length > 0) 
	    {

	    	File file[] = new File[names.length] ;
		    
	    	for (int j = 0 ; j < names.length ; j++)
	    		file[j] = new File( dialog.getFilterPath() + "\\" + names[j] );
		    	
	    	return file ;
		   
	    }
	    
	  
	    
	    logger.info(" << ----- Leaving Function openDialogForFile ....");
		return null ;
	}
	
	public static String openDialogForDirectory( Shell parent )
	{
	    
	    logger
				.info(" ----- >> Entering Function openDialogForDirectory with parameters ...."
						+ parent);
		DirectoryDialog directoryDialog = new DirectoryDialog( parent );
        
        //directoryDialog.setFilterPath( selectedDir );
        directoryDialog.setMessage( "Please select a directory and click OK" );
        
        String dir = directoryDialog.open();
        
        if(dir != null) 
        {
        	return dir ;
        }
	    
	    logger.info(" << ----- Leaving Function openDialogForDirectory ....");
		return null ;
	}
	
	//fix for issue 8
	public static File openDialogForSave( Shell parent , String dialogTitle , String[] extensions , String defaultFile , String filterPath)
	{
	    logger
				.info(" ----- >> Entering Function openDialogForSave with parameters ...."
						+ "parent - "
						+ parent
						+ "dialogTitle - "
						+ dialogTitle
						+ "extensions - "
						+ extensions
						+ "defaultFile - "
						+ defaultFile + "filterPath - " + filterPath);
		FileDialog dialog = new FileDialog( parent , SWT.SAVE ) ;
	    dialog.setText( dialogTitle ) ;
	    dialog.setFilterExtensions( extensions ) ;
	    dialog.setFileName( defaultFile ) ;
	    dialog.setFilterPath(filterPath);
	    String path = dialog.open();
	    if (path != null) 
	    {

	    	File file = new File( path );
		    
		    	return file ;
		   
	    }
		
		logger.info(" << ----- Leaving Function openDialogForSave ....");
		return null;
	}

	@Override

	public String toString() {
		String strValue = super.toString();
		return strValue;
	}
	

}

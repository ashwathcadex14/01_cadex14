package com.teamcenter.rac.tccadex.utils;

import java.util.HashMap;

import org.eclipse.swt.widgets.Shell;

import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponentBOMLine;
import com.teamcenter.rac.kernel.TCComponentItemRevision;

public abstract class BOMTraversalEngine<T> 
{
	TCComponentBOMLine topLine ; 
	Shell currentShell;
	HashMap<TCComponentItemRevision, T> bomMap = new HashMap<TCComponentItemRevision, T>();
	
	public BOMTraversalEngine(TCComponentBOMLine topLine, Shell shell)
	{
		this.topLine = topLine;
		this.currentShell = shell;
	}
	
	public void initiateTraversal()
	{
		this.startTraversing(this.topLine);
	}
	
	private void startTraversing(TCComponentBOMLine line)
	{
		
		try 
		{
			processLine( line );
			
			AIFComponentContext[] lineChilds = line.getChildren();
			
			if(lineChilds!=null && lineChilds.length>0)
			{
				for(int i=0;i<lineChilds.length;i++)
				{
					TCComponentBOMLine currentLine = (TCComponentBOMLine) lineChilds[i].getComponent();
					
					startTraversing(currentLine);
				}
			}
		} catch (Exception e) {
			CadexUtils.openError(this.currentShell, "Traversing error" , "Error occured during traversal." , e);
		}
	}
	
	public abstract void processLine(TCComponentBOMLine line) throws Exception;
	
	protected void addToMap(TCComponentItemRevision line, T line2) throws Exception
	{
		this.bomMap.put(line,line2);
	}
	
	public T getLineFromMap(TCComponentItemRevision line)
	{
		return this.bomMap.get( line );
	}
	
	public boolean checkIfInMap(TCComponentItemRevision line)
	{
		return this.bomMap.containsKey( line );
	}
}

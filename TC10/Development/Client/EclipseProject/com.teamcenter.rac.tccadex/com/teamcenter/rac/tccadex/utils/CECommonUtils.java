/*=================================================================================================                    
#                Copyright (c) 2014 Some Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CECommonUtils.java          
#      Module          :           com.teamcenter.rac.tccadex.utils          
#      Description     :           Common methods for CADEX related operations          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.utils;

import java.util.HashMap;
import java.util.List;

import com.ce4.services.rac.cadexchangemanagement.CadExchangeService;
import com.ce4.services.rac.cadexchangemanagement._2013_05.CadExchange.DatasetExportCheckOutInfo;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.ServiceData;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentBOMLine;
import com.teamcenter.rac.kernel.TCComponentBOMWindow;
import com.teamcenter.rac.kernel.TCComponentBOMWindowType;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentRevisionRule;
import com.teamcenter.rac.kernel.TCComponentTransferMode;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPropertyDescriptor;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.schemas.soa._2006_03.exceptions.ServiceException;
import com.teamcenter.services.rac.globalmultisite.ImportExportService;
import com.teamcenter.services.rac.globalmultisite._2007_06.ImportExport.GetPLMXMLRuleInputData;
import com.teamcenter.services.rac.globalmultisite._2007_06.ImportExport.GetTransferModesResponse;

public class CECommonUtils 
{	
	public static TCSession tSes = (TCSession) AIFUtility.getActiveDesktop().getCurrentApplication().getSession() ;
	public static CadExchangeService exServ = CadExchangeService.getService(tSes);
	
	/*
     * Create the BOM window.
     */
	public static TCComponentBOMLine createBOMWindow( TCComponent tcComponent ) throws Exception
	{
		TCComponentBOMWindow bomwindow = null;
		TCComponentRevisionRule revisionRule = null;

		TCComponentBOMLine bomline = null;                
		try
		{
			revisionRule = getRevisionRule(null);

			TCComponentBOMWindowType bomWindowType = (TCComponentBOMWindowType)tSes.getTypeComponent( "BOMWindow" );                        
			bomwindow = bomWindowType.create( revisionRule );                        
			bomline = bomwindow.setWindowTopLine(null, (TCComponentItemRevision)tcComponent, null, null);
			
		}
		catch (TCException e)
		{
			throw new Exception( "Failed to create BOM Window." , e);
		}
		return bomline;
	}

	public static TCComponentRevisionRule getRevisionRule(String revRule) throws Exception
	{
    	TCComponentRevisionRule revisionRule = null;
    	String revRuleName = revRule != null && revRule.length() > 0 ? revRule : getCERevisionRuleName();
    	try
    	{
    		TCComponentRevisionRule[] revisionRules = TCComponentRevisionRule.listAllRules( tSes );
    		for (int i = 0; i < revisionRules.length; i++)
    		{
    			if( revisionRules[i].getStringProperty( "object_name" ).equalsIgnoreCase( revRuleName ) )
    			{
    				revisionRule = revisionRules[i];
    				break;
    			}
    		}
    	}
    	catch( TCException e )
    	{
    		throw new Exception( "Failed to fetch revision rules." , e);
    	}
        
        return revisionRule;
    }
	
	public static String getCERevisionRuleName()
	{
		return "Latest Working";
	}
    
	
	public static HashMap<String,String> getDisplayName(String typeName, List<String> realNames) throws Exception
	{
		HashMap<String,String> result = new HashMap<String,String>();
		try
		{
			TCComponentType neededType = tSes.getTypeComponent( typeName );
			TCPropertyDescriptor[] tcProps = neededType.getPropertyDescriptors();
			for( int i=0;i<tcProps.length;i++ )
			{
				for(String realName : realNames)
				{
					if(tcProps[i].getName().equals( realName.substring( realName.indexOf( "." )+1) ))
					{
						result.put( realName, tcProps[i].getDisplayName() );
						break;
					}
				}
			}
		}
		catch( Exception e )
		{
			throw new Exception( "Failed to fetch display names." , e);
		}
		
		return result;
	}
	
	public static TCComponentRevisionRule[] getAllRevisionRules() throws TCException
	{
		return TCComponentRevisionRule.listAllRules( tSes );
	}
	
	public static boolean setExpCheckOutDset(TCComponentBOMLine bomLine, TCComponentBOMWindow window, boolean isExport, boolean isChkOut, TCComponentDataset dset)
	{
		DatasetExportCheckOutInfo[] dsetExpChkOutInfos = new DatasetExportCheckOutInfo[1];
		dsetExpChkOutInfos[0] = new DatasetExportCheckOutInfo();
		dsetExpChkOutInfos[0].bomLine = bomLine;
		dsetExpChkOutInfos[0].bomWindow = window;
		dsetExpChkOutInfos[0].dsetObject = dset;
		dsetExpChkOutInfos[0].isExport = isExport;
		dsetExpChkOutInfos[0].isCheckOut = isChkOut;
		ServiceData data = exServ.setExpChkOut(dsetExpChkOutInfos);
		return false;
	}
	
	public static TCComponentTransferMode getTransferMode(String transferModeName, String scope) throws ServiceException, TCException
	{
		ImportExportService importexportservice = ImportExportService.getService( tSes );
		GetPLMXMLRuleInputData ruleInput = new GetPLMXMLRuleInputData();
		ruleInput.schemaFormat = "PLMXML";
		ruleInput.scope = scope;
		GetTransferModesResponse tmResp = importexportservice.getTransferModes(ruleInput);
		TCComponentTransferMode[] allTms = tmResp.transferModeObjects;
		
		if(allTms != null && allTms.length > 0)
		{
			for (int i = 0; i < allTms.length; i++) 
			{
				if(allTms[i].getStringProperty( "object_name" ).equalsIgnoreCase( transferModeName ))
					return allTms[i];
			}
		}
		
		return null;
	}
}

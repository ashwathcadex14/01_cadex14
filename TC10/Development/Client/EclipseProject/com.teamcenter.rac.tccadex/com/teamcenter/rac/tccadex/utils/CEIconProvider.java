/*=================================================================================================                    
#                Copyright (c) 2014 Some Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CEIconProvider.java          
#      Module          :           com.teamcenter.rac.tccadex.utils          
#      Description     :           Type Icon Provider for CADEX View(s)          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.utils;

import org.eclipse.swt.graphics.Image;

import com.teamcenter.rac.common.TCTypeRenderer;

public class CEIconProvider 
{
	private static String FORM_TYPE = "Form" ;
	private static String ITEM_TYPE = "Item" ;
	private static String DATASET_TYPE = "Dataset" ;
	private static String DESIGN_TYPE = "Design" ;
	private static String DRAWING_TYPE = "Drawing" ;
		
	public static Image getTCTypeIcon( String tCompType )
	{		
		
		if ( tCompType.contains( FORM_TYPE ) )
		{		
			return TCTypeRenderer.getTypeImage(tCompType, FORM_TYPE );			
		}
		else if ( tCompType.contains( DESIGN_TYPE ) ) 
		{
			return TCTypeRenderer.getTypeImage(tCompType, DESIGN_TYPE );
		}
		else if ( tCompType.contains( ITEM_TYPE ) ) 
		{
			return TCTypeRenderer.getTypeImage(tCompType, ITEM_TYPE );
		}
		else if ( tCompType.contains( DATASET_TYPE ) ) 
		{
			return TCTypeRenderer.getTypeImage(tCompType, DATASET_TYPE );
		}
		else if ( tCompType.contains( DRAWING_TYPE ) ) 
		{
			return TCTypeRenderer.getTypeImage(tCompType, DRAWING_TYPE );
		}
		else
		{
			return TCTypeRenderer.getTypeImage(tCompType, ITEM_TYPE );
		}		
	}
	@Override

	public String toString() {
		String strValue = super.toString();
		return strValue;
	}
}

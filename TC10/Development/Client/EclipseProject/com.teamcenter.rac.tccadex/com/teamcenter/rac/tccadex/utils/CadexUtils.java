/**=================================================================================================                    
#                Copyright (c) 2014 BAVIS Technology Services                   
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           PackageUtils.java          
#      Module          :           com.teamcenter.siemens.briefcase.utils          
#      Description     :           Contains Utilities for zip and Unzip activties.          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Feb-2013                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.widgets.Shell;

import com.teamcenter.rac.tccadex.ui.dialogs.CadexErrorDialog;

public class CadexUtils {
	
	 private static final Logger logger = Logger.getLogger(CadexUtils.class);
	private final static int BUFFER_SIZE = 2048;
	 private final static String ZIP_EXTENSION = ".zip";

	 public static void createZip( File zipThis ) throws IOException {
			logger
				.info(" ----- >> Entering Function createZip with parameters ...."
						+ zipThis);
			File directoryToZip = zipThis ;

			List<File> fileList = new ArrayList<File>();
			System.out.println("---Getting references to all files in: " + directoryToZip.getCanonicalPath());
			getAllFiles(directoryToZip, fileList);
			System.out.println("---Creating zip file");
			writeZipFile(directoryToZip, fileList);
			System.out.println("---Done");
			logger.info(" << ----- Leaving Function createZip ....");
		}

		public static void getAllFiles(File dir, List<File> fileList) throws IOException {
				logger
					.info(" ----- >> Entering Function getAllFiles with parameters ...."
							+ "dir - " + dir + "fileList - " + fileList);
				File[] files = dir.listFiles();
				for (File file : files) {
					fileList.add(file);
					if (file.isDirectory()) {
						System.out.println("directory:" + file.getCanonicalPath());
						getAllFiles(file, fileList);
					} else {
						System.out.println("     file:" + file.getCanonicalPath());
					}
				}
				logger.info(" << ----- Leaving Function getAllFiles ....");

		}

		public static void writeZipFile(File directoryToZip, List<File> fileList) throws IOException {

				logger
					.info(" ----- >> Entering Function writeZipFile with parameters ...."
							+ "directoryToZip - "
							+ directoryToZip
							+ "fileList - " + fileList);
				FileOutputStream fos = new FileOutputStream( directoryToZip.getParent() + "\\" + directoryToZip.getName() + ".zip");
				ZipOutputStream zos = new ZipOutputStream(fos);

				for (File file : fileList) {
					if (!file.isDirectory()) { // we only zip files, not directories
						addToZip(directoryToZip, file, zos);
					}
				}

				zos.close();
				fos.close();
				logger.info(" << ----- Leaving Function writeZipFile ....");
		}

		public static void addToZip(File directoryToZip, File file, ZipOutputStream zos) throws FileNotFoundException,
				IOException {

			logger
							.info(" ----- >> Entering Function addToZip with parameters ...."
									+ "directoryToZip - "
									+ directoryToZip
									+ "file - " + file + "zos - " + zos);
			FileInputStream fis = new FileInputStream(file);

			// we want the zipEntry's path to be a relative path that is relative
			// to the directory being zipped, so chop off the rest of the path
			String zipFilePath = file.getCanonicalPath().substring(directoryToZip.getCanonicalPath().length() + 1,
					file.getCanonicalPath().length());
			System.out.println("Writing '" + zipFilePath + "' to zip file");
			ZipEntry zipEntry = new ZipEntry(zipFilePath);
			zos.putNextEntry(zipEntry);

			byte[] bytes = new byte[1024];
			int length;
			while ((length = fis.read(bytes)) >= 0) {
				zos.write(bytes, 0, length);
			}

			zos.closeEntry();
			fis.close();
			logger.info(" << ----- Leaving Function addToZip ....");
		}
	
	public static boolean unzipToFile(String srcZipFileName,
 		   String destDirectoryName) throws IOException {
 		   logger
					.info(" ----- >> Entering Function unzipToFile with parameters ...."
							+ "srcZipFileName - "
							+ srcZipFileName
							+ "destDirectoryName - " + destDirectoryName);
		BufferedInputStream bufIS = null;
 		   // create the destination directory structure (if needed)
 		   File destDirectory = new File(destDirectoryName);
 		   
 		   if(!destDirectory.exists())
 			   destDirectory.mkdirs();
 		 
 		   // open archive for reading
 		   File file = new File(srcZipFileName);
 		   ZipFile zipFile = new ZipFile(file, ZipFile.OPEN_READ);
 		 
 		   //for every zip archive entry do
 		   Enumeration<? extends ZipEntry> zipFileEntries = zipFile.entries();
 		   while (zipFileEntries.hasMoreElements()) {
 		    ZipEntry entry = (ZipEntry) zipFileEntries.nextElement();
 		    System.out.println("\tExtracting entry: " + entry);
 		 
 		    //create destination file
 		    File destFile = new File(destDirectory, entry.getName());
 		 
 		    //create parent directories if needed
 		    File parentDestFile = destFile.getParentFile(); 
 		    
 		    if( !parentDestFile.exists() ) 
 		    	parentDestFile.mkdirs();    
 		     
 		    if (!entry.isDirectory() && !destFile.exists()) {
 		     bufIS = new BufferedInputStream(
 		       zipFile.getInputStream(entry));
 		     int currentByte;
 		 
 		     // buffer for writing file
 		     byte data[] = new byte[BUFFER_SIZE];
 		 
 		     // write the current file to disk
 		     FileOutputStream fOS = new FileOutputStream(destFile);
 		     BufferedOutputStream bufOS = new BufferedOutputStream(fOS, BUFFER_SIZE);
 		 
 		     while ((currentByte = bufIS.read(data, 0, BUFFER_SIZE)) != -1) {
 		      bufOS.write(data, 0, currentByte);
 		     }
 		 
 		     // close BufferedOutputStream
 		     bufOS.flush();
 		     bufOS.close();
 		 
 		     // recursively unzip files
 		     if (entry.getName().toLowerCase().endsWith(ZIP_EXTENSION)) {
 		      String zipFilePath = destDirectory.getPath() + File.separatorChar + entry.getName();
 		 
 		      unzipToFile(zipFilePath, zipFilePath.substring(0, 
 		              zipFilePath.length() - ZIP_EXTENSION.length()));
 		     }
 		    zipFile.close();
 		    }
 		   }
 		   bufIS.close();
 		   logger.info(" << ----- Leaving Function unzipToFile ....");
		return true;
 		 }
	
	public static String stackTraceToString(Throwable e) {
	    logger
				.info(" ----- >> Entering Function stackTraceToString with parameters ...."
						+ e);
		StringBuilder sb = new StringBuilder();
	    for (StackTraceElement element : e.getStackTrace()) {
	        sb.append(element.toString());
	        sb.append("\n");
	    }
	    logger.info(" << ----- Leaving Function stackTraceToString ....");
		return sb.toString();
	}
	
	public static void openError(Shell shell , String title, String message , Exception e )
	{
		logger
				.info(" ----- >> Entering Function openError with parameters ...."
						+ "shell - "
						+ shell
						+ "title - "
						+ title
						+ "message - " + message + "e - " + e);
		IStatus status=new Status(IStatus.ERROR,"com.teamcenter.siemens.briefcase",message,e);
		CadexErrorDialog.openError(shell, title, message,status );
		logger.info(" << ----- Leaving Function openError ....");
	}

	@Override

	public String toString() {
		String strValue = super.toString();
		return strValue;
	}
	
    public static String formatMessage(String message, Exception ex)
    {
        return message + "\n" + ex.getMessage() + "\n" + stackTraceToString(ex);
    }
    
    public static void replaceText(String file, String toReplace, String withThis) throws Exception
    {
            File f1=null;
            FileReader fr=null;
            BufferedReader br=null;
            FileWriter fw=null;
            BufferedWriter out=null;
            ArrayList<String> lines = new ArrayList<String>();
            String line = null;
            try {
                f1 = new File(file);
                fr = new FileReader(f1);
                br = new BufferedReader(fr);
                while ((line = br.readLine()) != null) {
                    if (line.contains(toReplace))
                        line = line.replace(toReplace, withThis);
                    lines.add(line + "\r\n");
                }
                
                fr.close();
                br.close();
                fw = new FileWriter(f1);
                out = new BufferedWriter(fw);
                for (String s : lines)
                    out.write(s);
                out.flush();

            } catch (Exception ex) {
            	throw new Exception("Unable to create delta");
            } finally {
                try
                {
	                fr.close();
	                br.close();
	                out.close();
                }catch(IOException ioe)
                {
                    throw new Exception("Unable to create delta");
                }
            }

    }
    
    public static void addFilesToExistingZip(File zipFile,
            File[] files) throws IOException {
           // get a temp file
       File tempFile = File.createTempFile(zipFile.getName(), null);
           // delete it, otherwise you cannot rename your existing zip to it.
       tempFile.delete();

       boolean renameOk=zipFile.renameTo(tempFile);
       if (!renameOk)
       {
           throw new RuntimeException("could not rename the file "+zipFile.getAbsolutePath()+" to "+tempFile.getAbsolutePath());
       }
       byte[] buf = new byte[1024];

       ZipInputStream zin = new ZipInputStream(new FileInputStream(tempFile));
       ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFile));

       ZipEntry entry = zin.getNextEntry();
       while (entry != null) {
           String name = entry.getName();
           boolean notInFiles = true;
           for (File f : files) {
               if (f.getName().equals(name)) {
                   notInFiles = false;
                   break;
               }
           }
           if (notInFiles) {
               // Add ZIP entry to output stream.
               out.putNextEntry(new ZipEntry(name));
               // Transfer bytes from the ZIP file to the output file
               int len;
               while ((len = zin.read(buf)) > 0) {
                   out.write(buf, 0, len);
               }
           }
           entry = zin.getNextEntry();
       }
       // Close the streams        
       zin.close();
       // Compress the files
       for (int i = 0; i < files.length; i++) {
           InputStream in = new FileInputStream(files[i]);
           // Add ZIP entry to output stream.
           out.putNextEntry(new ZipEntry(files[i].getName()));
           // Transfer bytes from the file to the ZIP file
           int len;
           while ((len = in.read(buf)) > 0) {
               out.write(buf, 0, len);
           }
           // Complete the entry
           out.closeEntry();
           in.close();
       }
       // Complete the ZIP file
       out.close();
       tempFile.delete();
   }

}

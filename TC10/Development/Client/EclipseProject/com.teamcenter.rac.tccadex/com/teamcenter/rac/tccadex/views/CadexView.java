/*=================================================================================================                    
#                Copyright (c) 2014 Some Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CadexView.java          
#      Module          :           com.teamcenter.rac.tccadex.views          
#      Description     :           Core View of Supplier CAD Exchange application          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.views;

import java.awt.datatransfer.DataFlavor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CheckboxCellEditor;
import org.eclipse.jface.viewers.ColumnViewerEditor;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationEvent;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationStrategy;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.TreeViewerEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.wb.swt.SWTResourceManager;

import com.ce4.services.rac.cadexchangemanagement.CadExchangeService;
import com.ce4.services.rac.cadexchangemanagement._2013_05.CadExchange.CreateExportPkgInput;
import com.ce4.services.rac.cadexchangemanagement._2013_05.CadExchange.ImportPackageInput;
import com.teamcenter.rac.aif.AIFClipboard;
import com.teamcenter.rac.aif.AIFPortal;
import com.teamcenter.rac.aif.AIFTransferable;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentBOMLine;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentRevisionRule;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.tccadex.EarlyStartup;
import com.teamcenter.rac.tccadex.ui.CELoadingComposite;
import com.teamcenter.rac.tccadex.utils.CECommonUtils;
import com.teamcenter.rac.tccadex.utils.CadexUtils;
import com.teamcenter.rac.tccadex.views.actions.CustomAction;
import com.teamcenter.rac.tccadex.views.cadexview.CadexViewContentProvider;
import com.teamcenter.rac.tccadex.views.cadexview.CadexViewEditingSupport;
import com.teamcenter.rac.tccadex.views.cadexview.CadexViewLabelProvider;
import com.teamcenter.rac.tccadex.views.cadexview.CadexViewModel;
import com.teamcenter.rac.tdv.views.Thumbnail3DView;
import com.teamcenter.rac.ui.views.TCComponentView;
import com.teamcenter.rac.util.Registry;

public class CadexView extends ViewPart implements ISelectionChangedListener {
	public CadexView() {
		super();
		TCComponentView homeView = (TCComponentView) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findViewReference( "com.teamcenter.rac.ui.views.TCComponentView" ).getView(true);
		homeView.getViewer().addSelectionChangedListener( this );
	}
	
	CELoadingComposite loadMidComposite;
	Registry reg = Registry.getRegistry(this);
	Job startLoadingJob;
	Job revRuleRefreshJob;
	Job validateJob;
	Job exportJob;
	Job checkInJob;
	Composite cParent ;
	TreeViewer ceTreeViewer;
	CadexViewModel cadexModel = null;
	TCComponentItemRevision targetRev;
	TCSession m_tcSession=null;
	HashMap<Integer, String> columnMapping = new HashMap<Integer,String>();
	int columnCount = -1;
	CheckboxCellEditor checkboxCellEditor;
	String packageString;
	private Menu menu ;
	private Tree tree;
	ComboViewer comboViewer;
	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());
	ScrolledForm scrldfrmNewScrolledform;
	Label label_3;
	boolean isExportable = false;
	
	@Override
	public void dispose() 
	{
		if(startLoadingJob !=null && startLoadingJob.getState() == Job.RUNNING )
			startLoadingJob.done(Status.CANCEL_STATUS);
		
		if(revRuleRefreshJob !=null && revRuleRefreshJob.getState() == Job.RUNNING )
			revRuleRefreshJob.done(Status.CANCEL_STATUS);
		
		if(validateJob !=null && validateJob.getState() == Job.RUNNING )
			validateJob.done(Status.CANCEL_STATUS);
		
		if(exportJob !=null && exportJob.getState() == Job.RUNNING )
			exportJob.done(Status.CANCEL_STATUS);
		
		if(checkInJob !=null && checkInJob.getState() == Job.RUNNING )
			checkInJob.done(Status.CANCEL_STATUS);
		
		Job.getJobManager().cancel( "TCCADEX family" );
		
		super.dispose();
		
		try {
			getCadexModel().closeWindow();
		} catch (TCException e) {
			CadexUtils.openError(cParent.getShell(), "Close Error", e.getMessage(),e);
		}
	}
	
	
	
	@Override
	public void createPartControl(Composite composite) {
		cParent = composite ;
		
		try 
		{	
			TCComponent targetComp = (TCComponent) AIFUtility.getCurrentApplication().getTargetComponent();
			
			if(targetComp==null)
			{
				return;
			}
			else if( targetComp != null && targetComp.getStringProperty( "object_type").equals( "CE4_ExPkgRevision" ) )
			{
				targetRev = (TCComponentItemRevision) targetComp;
				m_tcSession = targetRev.getSession();
				packageString = targetRev.getStringProperty( "object_string" );
				initUI();
			}
//			else
//			{
//				throw new Exception("Only CE_ExchangePackageRevision is allwoed to open in Supplier CAD Exchange View.");
//			}
			
		} catch (Exception e) {
			CadexUtils.openError(cParent.getShell(), "Open Error", e.getMessage(),e);
			return;
		}
		
		
	}
	
	private void initUI()
	{
		cParent.setLayout(new GridLayout(1, false));
		columnMapping.clear();
		columnCount = -1;
		scrldfrmNewScrolledform = formToolkit.createScrolledForm(cParent);
		scrldfrmNewScrolledform.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		scrldfrmNewScrolledform.setText(packageString);
		GridData gd_scrldfrmNewScrolledform = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		gd_scrldfrmNewScrolledform.heightHint = 110;
		scrldfrmNewScrolledform.setLayoutData(gd_scrldfrmNewScrolledform);
		formToolkit.paintBordersFor(scrldfrmNewScrolledform);
		scrldfrmNewScrolledform.getBody().setLayout(new GridLayout(1, false));
		
		
		Composite composite_1 = new Composite(scrldfrmNewScrolledform.getBody(), SWT.NONE);
		GridData gd_composite_1 = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		gd_composite_1.heightHint = 47;
		gd_composite_1.widthHint = 1046;
		composite_1.setLayoutData(gd_composite_1);
		composite_1.setBounds(0, 0, 771, 108);
		formToolkit.adapt(composite_1);
		formToolkit.paintBordersFor(composite_1);
		
		Label label = new Label(composite_1, SWT.NONE);
		label.setText("Revision Rule :");
		label.setFont(SWTResourceManager.getFont("Segoe UI", 8, SWT.BOLD));
		label.setBounds(10, 10, 145, 26);
		formToolkit.adapt(label, true, true);
		
		comboViewer = new ComboViewer(composite_1, SWT.READ_ONLY);
		Combo combo = comboViewer.getCombo();
		combo.setBounds(161, 7, 286, 21);
		formToolkit.paintBordersFor(combo);
		comboViewer.setContentProvider( ArrayContentProvider.getInstance() );
		
		Label label_2 = new Label(composite_1, SWT.NONE);
		label_2.setText("Status :");
		label_2.setFont(SWTResourceManager.getFont("Segoe UI", 8, SWT.BOLD));
		label_2.setBounds(463, 10, 79, 26);
		formToolkit.adapt(label_2, true, true);
		
		label_3 = new Label(composite_1, SWT.NONE);
		label_3.setFont(SWTResourceManager.getFont("Tahoma", 8, SWT.NORMAL));
		label_3.setTouchEnabled(true);
		label_3.setToolTipText("Indicates the current status of the package");
		label_3.setAlignment(SWT.CENTER);
		label_3.setBounds(533, 7, 255, 29);
		formToolkit.adapt(label_3, true, true);
		
		
		// TODO Auto-generated method stub
		loadMidComposite = new CELoadingComposite(cParent, SWT.NONE);
		GridData gd_loadMidComposite = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_loadMidComposite.heightHint = 461;
		loadMidComposite.setLayoutData(gd_loadMidComposite);
		GridLayout gridLayout = (GridLayout) loadMidComposite.otherComposite.getLayout();
		gridLayout.makeColumnsEqualWidth = true;				  

		ceTreeViewer = new TreeViewer(loadMidComposite.otherComposite, SWT.BORDER | SWT.FULL_SELECTION | SWT.H_SCROLL | SWT.V_SCROLL);
		tree = ceTreeViewer.getTree();
		tree.setLinesVisible(true);
		tree.setHeaderVisible(true);
		tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		//final TreeViewerFocusCellManager mgr = new TreeViewerFocusCellManager(ceTreeViewer,new FocusCellOwnerDrawHighlighter(ceTreeViewer));
		ColumnViewerEditorActivationStrategy actSupport = new ColumnViewerEditorActivationStrategy(ceTreeViewer) {
			protected boolean isEditorActivationEvent(
					ColumnViewerEditorActivationEvent event) {
				return event.eventType == ColumnViewerEditorActivationEvent.TRAVERSAL
				|| event.eventType == ColumnViewerEditorActivationEvent.MOUSE_CLICK_SELECTION
				|| (event.eventType == ColumnViewerEditorActivationEvent.KEY_PRESSED && ( event.keyCode == SWT.CR || event.character == ' ' ))
				|| event.eventType == ColumnViewerEditorActivationEvent.PROGRAMMATIC;
			}
		};
		
		TreeViewerEditor.create(ceTreeViewer, null, actSupport, ColumnViewerEditor.TABBING_HORIZONTAL
				| ColumnViewerEditor.TABBING_MOVE_TO_ROW_NEIGHBOR
				| ColumnViewerEditor.TABBING_VERTICAL | ColumnViewerEditor.KEYBOARD_ACTIVATION);
		
		checkboxCellEditor = new CheckboxCellEditor(ceTreeViewer.getTree());
		
		TreeViewerColumn treeViewerColumn = new TreeViewerColumn(ceTreeViewer, SWT.NONE);
		TreeColumn trclmnBomline = treeViewerColumn.getColumn();
		trclmnBomline.setAlignment(SWT.CENTER);
		trclmnBomline.setWidth(344);
		trclmnBomline.setText(reg.getString( "columnPackage.title" ));
		columnMapping.put( ++columnCount , reg.getString( "columnPackage.real" ));
		
		TreeViewerColumn treeViewerColumnType = new TreeViewerColumn(ceTreeViewer, SWT.CHECK);
		TreeColumn trclmnType = treeViewerColumnType.getColumn();
		trclmnType.setMoveable(true);
		trclmnType.setAlignment(SWT.CENTER);
		trclmnType.setWidth(125);
		trclmnType.setText(reg.getString( "columnType.title" ));
		columnMapping.put( ++columnCount , reg.getString( "columnType.real" ));
		
		TreeViewerColumn treeViewerColumn_2 = new TreeViewerColumn(ceTreeViewer, SWT.CHECK);
		TreeColumn trclmnExport = treeViewerColumn_2.getColumn();
		trclmnExport.setAlignment(SWT.CENTER);
		trclmnExport.setMoveable(true);
		trclmnExport.setWidth(111);
		trclmnExport.setText(reg.getString( "columnExport.title" ));
		columnMapping.put( ++columnCount , reg.getString( "columnExport.real" ));
		treeViewerColumn_2.setEditingSupport( new CadexViewEditingSupport(ceTreeViewer, columnCount, this) );
		
		TreeViewerColumn treeViewerColumn_1 = new TreeViewerColumn(ceTreeViewer, SWT.CHECK);
		TreeColumn trclmnCheckOut = treeViewerColumn_1.getColumn();
		trclmnCheckOut.setMoveable(true);
		trclmnCheckOut.setAlignment(SWT.CENTER);
		trclmnCheckOut.setWidth(125);
		trclmnCheckOut.setText(reg.getString( "columnCheckOut.title" ));
		columnMapping.put( ++columnCount , reg.getString( "columnCheckOut.real" ));
		treeViewerColumn_1.setEditingSupport( new CadexViewEditingSupport(ceTreeViewer, columnCount, this) );
		
		TreeViewerColumn treeViewerCheckedOut = new TreeViewerColumn(ceTreeViewer, SWT.CHECK);
		TreeColumn trclmnCheckedOut = treeViewerCheckedOut.getColumn();
		trclmnCheckedOut.setMoveable(true);
		trclmnCheckedOut.setAlignment(SWT.CENTER);
		trclmnCheckedOut.setWidth(125);
		trclmnCheckedOut.setText(reg.getString( "columnCheckedOut.title" ));
		columnMapping.put( ++columnCount , reg.getString( "columnCheckedOut.real" ));
		treeViewerCheckedOut.setEditingSupport( new CadexViewEditingSupport(ceTreeViewer, columnCount, this) );
		
		TreeViewerColumn treeViewerCoBy = new TreeViewerColumn(ceTreeViewer, SWT.CHECK);
		TreeColumn trclmnCoBy = treeViewerCoBy.getColumn();
		trclmnCoBy.setMoveable(true);
		trclmnCoBy.setAlignment(SWT.CENTER);
		trclmnCoBy.setWidth(125);
		trclmnCoBy.setText(reg.getString( "columnCoBy.title" ));
		columnMapping.put( ++columnCount , reg.getString( "columnCoBy.real" ));
		treeViewerCoBy.setEditingSupport( new CadexViewEditingSupport(ceTreeViewer, columnCount, this) );
		
		TreeViewerColumn treeViewerColumn_3 = new TreeViewerColumn(ceTreeViewer, SWT.CHECK);
		TreeColumn trclmnExportRendering = treeViewerColumn_3.getColumn();
		trclmnExportRendering.setWidth(167);
		trclmnExportRendering.setText(reg.getString( "columnExpRendering.title" ));
		columnMapping.put( ++columnCount , reg.getString( "columnExpRendering.real" ));
		treeViewerColumn_3.setEditingSupport( new CadexViewEditingSupport(ceTreeViewer, columnCount, this) );
		
		TreeViewerColumn treeViewerColumn_4 = new TreeViewerColumn(ceTreeViewer, SWT.CHECK);
		TreeColumn trclmnExportNative = treeViewerColumn_4.getColumn();
		trclmnExportNative.setWidth(138);
		trclmnExportNative.setText(reg.getString( "columnExpNative.title" ));
		columnMapping.put( ++columnCount , reg.getString( "columnExpNative.real" ));
		treeViewerColumn_4.setEditingSupport( new CadexViewEditingSupport(ceTreeViewer, columnCount, this) );
		
		Action lCustomAction = new CustomAction("BOMLine","CADEXShownColumnsPref");
		lCustomAction.setText(reg.getString( "columnConfiguration.title" ));
		getViewSite().getActionBars().getMenuManager().removeAll();
		getViewSite().getActionBars().getMenuManager().add(lCustomAction);
		getViewSite().getActionBars().getMenuManager().add(new Separator());
		
		
		
		DatasetsView dsetView = (DatasetsView) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findViewReference( EarlyStartup.DATASET_VIEW_ID ).getView(true);
		PackageHistoryView pkgHistoryView = (PackageHistoryView) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findViewReference( EarlyStartup.HISTORY_VIEW_ID ).getView(true);
		
		
		try {
			createColumns();
			runLoadJob();
			setTreeData();
			populateRevRules();
			setStatus();
		} catch (Exception e) {
			CadexUtils.openError(cParent.getShell(), "Columns Error", e.getMessage(),e);
		}
		
		

		
		getSite().setSelectionProvider(ceTreeViewer);
		getSite().getPage().addPartListener( pkgHistoryView );
		
		ceTreeViewer.addSelectionChangedListener( new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent arg0) {
				try {
					TCComponentBOMLine bomLine = (TCComponentBOMLine) ((ITreeSelection)arg0.getSelection()).getFirstElement();
					
					IWorkbenchPage page =  PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
					IViewReference[] graphicsView = page.getViewReferences();
					for (int i = 0; i < graphicsView.length; i++) {
						if(graphicsView[i].getId().equals( "com.teamcenter.rac.tdv.views.ThumbnailView" ))
						{
							Thumbnail3DView jtPreView = (Thumbnail3DView) graphicsView[i].getView(true);
//						jtPreView.selectionChanged(CadexView.this, arg0.getSelection());
							if(bomLine!=null && bomLine.getChildrenCount()>0)
							{
								AIFComponentContext[] childs = bomLine.getChildren();
								List<Object> objsList = new ArrayList<Object>();
								for (int j = 0; j < childs.length; j++) {
									objsList.add( childs[j].getComponent() );
								}
								jtPreView.setInputRootObjects(CadexView.this,objsList);
							}
							else
								jtPreView.setInputRootObject(CadexView.this,bomLine);
							
							break;
						}

					}
				} catch (TCException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		} );
		
		ceTreeViewer.addSelectionChangedListener( dsetView );
		ceTreeViewer.addSelectionChangedListener( new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent arg0) {
				TCComponentBOMLine line = (TCComponentBOMLine) ((IStructuredSelection)arg0.getSelection()).getFirstElement();
				String message = CadexView.this.cadexModel.getMessage(line);
				if(message.length()==0)
					{
						scrldfrmNewScrolledform.setMessage( "",  IMessageProvider.NONE  );
					}
				else
					scrldfrmNewScrolledform.setMessage( message	,  IMessageProvider.ERROR  );
			}
		} );
		
		setMenu();
	}
	
	public void setStatus() throws TCException
	{		
		label_3.setText( targetRev.getStringProperty( "ce4_status" ) );
	}
	
	public void updateStatus() throws TCException
	{
		if(!targetRev.getStringProperty( "ce4_status" ).equals( "Preparation" ))
		{
			cadexModel.setPkgStatusProperty( "The package has new items to be validated. Changing status to Prepartion." , "Preparation");
		}
		
		setStatus();
		getCadexModel().saveWindow();
	}
	
	private void setMenu() {
		MenuManager menuManager = new MenuManager();
		menu = menuManager.createContextMenu( ceTreeViewer.getTree() );
		ceTreeViewer.getTree().setMenu( menu );
		getSite().registerContextMenu( menuManager , ceTreeViewer);
		getSite().setSelectionProvider( ceTreeViewer );
		
	}

	public CheckboxCellEditor getCheckboxCellEditor() {
		return checkboxCellEditor;
	}
	
	private void populateRevRules() throws Exception
	{
		comboViewer.setInput( CECommonUtils.getAllRevisionRules() );
		
		String revRule = targetRev.getStringProperty( "ce4_revisionRule" );
		final ISelection sel = new StructuredSelection(CECommonUtils.getRevisionRule( revRule ));
		comboViewer.setSelection( sel );
		
		comboViewer.addSelectionChangedListener(new ISelectionChangedListener() {
	        @Override
	        public void selectionChanged(SelectionChangedEvent event) {
	        	
	            try {
					IStructuredSelection selection = (IStructuredSelection) event.getSelection();
					TCComponentRevisionRule rule = (TCComponentRevisionRule)selection.getFirstElement();
					comboViewer.refresh();
					updateStatus();
					refreshJob(rule);
				} catch (Exception e) {
					CadexUtils.openError(cParent.getShell(),"Revision Rule Error", "Error occured while setting revision rules.\n" , e);
				}
	            
	        }
	    });
		
		comboViewer.setLabelProvider(new LabelProvider() {
	        @Override
	        public String getText(Object element) {
	            try {
					if (element instanceof TCComponentRevisionRule) {
						TCComponentRevisionRule current = (TCComponentRevisionRule) element;
						return current.getStringProperty( "object_name" );
					}
				} catch (TCException e) {
					CadexUtils.openError(cParent.getShell(),"Revision Rule Error", "Error occured while loading revision rules.\n" , e);
				}
	            return super.getText(element);
	        }
	    });
		
	}

	private void createColumns() throws Exception
	{
		
		TCPreferenceService prefService = m_tcSession.getPreferenceService();
		String[] shownProps = prefService.getStringValues( "CADEXShownColumnsPref" );
		if(shownProps!=null && shownProps.length > 0 )
		{
			HashMap<String, String> realDispMap = CECommonUtils.getDisplayName("BOMLine", Arrays.asList( shownProps ));
			
			for(int i=0; i<shownProps.length; i++)
			{
				TreeViewerColumn propViewerColumn = new TreeViewerColumn(ceTreeViewer, SWT.NONE);
				TreeColumn propColumn = propViewerColumn.getColumn();
				propColumn.setWidth(138);
				propColumn.setText(realDispMap.get( shownProps[i] ));
				columnMapping.put( ++columnCount ,shownProps[i].substring(shownProps[i].indexOf( "." )+1) );
			}
		}
	}
	
	public void setTreeData()
	{
		ceTreeViewer.setContentProvider( new CadexViewContentProvider(cParent.getShell()) ) ;	
		ceTreeViewer.setLabelProvider( new CadexViewLabelProvider(this) ) ;
		ceTreeViewer.setAutoExpandLevel(2);
		
	}

	@Override
	public void setFocus() {
		System.out.println();
	}
	
	private void refreshJob(final TCComponentRevisionRule rule)
	{
		revRuleRefreshJob = new CadexJob("Applying Revision Rule ...."){
			
			@Override
			public void parallelActivity() throws Exception {
				cadexModel.setRevisionRule( rule );
			}
		};
		
		revRuleRefreshJob.schedule();
	}
	
	private void runLoadJob()
	{
		
		startLoadingJob = new CadexJob("Loading Exchange Package details....") {
			
			@Override
			public void parallelActivity() throws Exception {
				
				//if(cadexModel==null)
					cadexModel = new CadexViewModel(targetRev);
				
			}
		};
		
		startLoadingJob.schedule();
	}

	
	public String getColumn(int index)
	{
		return this.columnMapping.get(index);
	}
	
	public void refreshTreeNode(Object arg0) throws TCException
	{
		((TCComponentBOMLine)arg0).refresh();
		this.ceTreeViewer.update(arg0,null);
	}
	
	@SuppressWarnings("unchecked")
	public void pasteObjects(String type) throws Exception
	{
		AIFClipboard tClipboard = AIFPortal.getClipboard() ;

		AIFTransferable tTrans = (AIFTransferable) tClipboard.getContents(this) ;
		
		boolean pasteAsTarget = (type == null || type.length() == 0) ? MessageDialog.open(MessageDialog.QUESTION_WITH_CANCEL, this.getSite().getShell(), "Target or Reference" , "Do you want to paste the object as Target or Refernce? If Target, Click Yes or No.", SWT.NONE) : (type.equals( "Target" ) ? true : false);
		
		System.out.println( pasteAsTarget );
		
		Vector<TCComponent> vector = new Vector<TCComponent>();
		try
		{
			vector = (Vector<TCComponent>)tTrans.getTransferData(new DataFlavor(Vector.class, "AIF Vector"));
			TCComponentBOMLine currentBomLine = (TCComponentBOMLine) ((ITreeSelection)ceTreeViewer.getSelection()).getFirstElement();
			Iterator<?> tIter = vector.iterator() ;
			
			for (; tIter.hasNext();)
			{
				TCComponent tComp =  (TCComponent) tIter.next() ;
				validatePasteObject(currentBomLine, tComp);
				TCComponentBOMLine pastedLine = currentBomLine.addBOMLine(currentBomLine, tComp, "");
				setAllChilds(pastedLine, "CE4_attachType", pasteAsTarget ? "Target" : "Reference",true);
			}

			cadexModel.saveWindow();
			refreshTreeNode(currentBomLine);
			updateStatus();
			runLoadJob();
		}
		catch( Exception exception1 )
		{
			throw exception1;
		}
	}
	
	public void removeObjects() throws Exception
	{
		try
		{
			TCComponentBOMLine currentBomLine = (TCComponentBOMLine) ((ITreeSelection)ceTreeViewer.getSelection()).getFirstElement();
			TCComponentBOMLine parentLine = currentBomLine.parent();
			currentBomLine.cut();

			cadexModel.saveWindow();
			refreshTreeNode(parentLine);
			updateStatus();
			runLoadJob();
		}
		catch( Exception exception1 )
		{
			throw exception1;
		}
	}
	
	public IWorkbenchPage getPage()
	{
		return getSite().getPage();
	}

	//@Override
	protected void createContent(Composite arg0) {
		// TODO Auto-generated method stub
		System.out.println();
	}
	
	
	private void setAllChilds(TCComponentBOMLine line, String prop, String value, boolean setChilds) throws Exception
	{
		try 
		{
			line.setProperty("CE4_attachType", value);
			
			if(setChilds)
			{
				AIFComponentContext[] lineChilds = line.getChildren();
			
				if(lineChilds!=null && lineChilds.length>0)
				{
					for(int i=0;i<lineChilds.length;i++)
					{
						TCComponentBOMLine currentLine = (TCComponentBOMLine) lineChilds[i].getComponent();
						setAllChilds(currentLine,prop, value, setChilds);
					}
				}
			}
			
		} catch (TCException e) {
			throw new Exception("Problem occured while setting attribute.",e);
		}
	}
	
	public void setMessage(String message, int type)
	{
		scrldfrmNewScrolledform.setMessage(message, type);
	}
		
	public CadexViewModel getCadexModel() {
		return cadexModel;
	}
	
	public void validatePackage()
	{
		try 
		{
			final TCComponentBOMLine currentLine = this.cadexModel.getTopBomLine();
			String pkgStatus = targetRev.getStringProperty("ce4_status");
			if(!pkgStatus.equalsIgnoreCase( "Preparation" ) && !pkgStatus.equalsIgnoreCase( "Export Validation Failed" ))
				throw new Exception("The package is already validated.");
			
			validateJob = new CadexJob( "Validating package..." ) {
				
				@Override
				public void parallelActivity() throws Exception 
				{
					
					CadexView.this.getCadexModel().preValidate();
					
					if(!CadexView.this.cadexModel.isExportable())
					{
						String msg = "The package has no objects selected for export.";
						CadexView.this.setMessage(msg, IMessageProvider.ERROR);
						getCadexModel().updateProblemLines( currentLine , msg);
					}
					
					CadexView.this.cadexModel.setPkgStatus();
					
					if(!getCadexModel().isProblemExists())
					{
						CadexView.this.setMessage( "The package is validated successfully and ready for export." , IMessageProvider.INFORMATION);
					}
				}
			};
			
			validateJob.schedule();
			
		} catch (Exception e) {
			CadexUtils.openError(this.getSite().getShell(), "Error", "Error ocurred during operation.\n" , e);
		}	
	}
	
	public void exportPackage()
	{
		try 
		{
			final TCComponentBOMLine currentLine = this.cadexModel.getTopBomLine();
			String pkgStatus = targetRev.getStringProperty("ce4_status");
			if(!pkgStatus.equalsIgnoreCase( "Ready For Export" ) && !pkgStatus.equalsIgnoreCase( "Export Failed" ))
				throw new Exception("The package is not validated.");
			
			exportJob = new CadexJob( "Exporting package..." ) {
				
				@Override
				public void parallelActivity() throws Exception {
					
					CadExchangeService exServ = CadExchangeService.getService( m_tcSession );
					CreateExportPkgInput exPkgSessionInput = new CreateExportPkgInput();
					exPkgSessionInput.exPkgRevision = currentLine.getItemRevision();
					exPkgSessionInput.exPkgSessionTM = CECommonUtils.getTransferMode( "CE4_SupplierCadExchangeTM" , "EXPORT");
					if(exPkgSessionInput.exPkgSessionTM==null)
						throw new Exception("The Supplier CAD Exchange Transfer Mode CE4_SupplierCadExchangeTM is not available in Teamcenter. Please contact your system administrator."); 
					
					exServ.createExportPackage(exPkgSessionInput);
					
				}
			};
			
			exportJob.schedule();
		} catch (Exception e) {
			CadexUtils.openError(this.getSite().getShell(), "Error", "Error ocurred during operation.\n" , e);
		}	
	}
	
	public void checkInPackage()
	{
		try 
		{
			final TCComponentBOMLine currentLine = this.cadexModel.getTopBomLine();
//			String pkgStatus = targetRev.getStringProperty("ce4_status");
			/*if(!pkgStatus.equalsIgnoreCase( "Ready For Export" ))
				throw new Exception("The package is not validated.");*/
			
			checkInJob = new CadexJob( "Importing package..." ) {
				
				@Override
				public void parallelActivity() throws Exception {
					
					CadExchangeService exServ = CadExchangeService.getService( m_tcSession );
					ImportPackageInput imPkgSessionInput = new ImportPackageInput();
					imPkgSessionInput.packageRev = currentLine.getItemRevision();
					imPkgSessionInput.importTM = CECommonUtils.getTransferMode( "incremental_import", "IMPORT" );
					if(imPkgSessionInput.importTM==null)
						throw new Exception("The Supplier CAD Exchange Transfer Mode incremental_import is not available in Teamcenter. Please contact your system administrator."); 
					
					exServ.importExchangePackage(imPkgSessionInput);
					
				}
			};
			
			checkInJob.schedule();
			
		} catch (Exception e) {
			CadexUtils.openError(this.getSite().getShell(), "Error", "Error ocurred during operation.\n" , e);
		}	
	}
	
	private void validatePasteObject(TCComponentBOMLine line, TCComponent pasteComp) throws Exception
	{
		AIFComponentContext[] childs = line.getChildren();		

		
		for (int i = 0; i < childs.length; i++) {
			
			TCComponentBOMLine currentLine = (TCComponentBOMLine) childs[i].getComponent();
			
			if(pasteComp instanceof TCComponentItem)
			{
				if(currentLine.getItem().equals( pasteComp ))
						throw new Exception( "The Object " + pasteComp.getStringProperty( "object_string" ) + " is already in the Package."); 
			}
			else if(pasteComp instanceof TCComponentItemRevision)
			{
				if(currentLine.getItemRevision().equals( pasteComp ))
						throw new Exception( "The Object " + pasteComp.getStringProperty( "object_string" ) + " is already in the Package."); 
			}
			else if(pasteComp instanceof TCComponentBOMLine)
			{
				if(currentLine.getItemRevision().equals( ((TCComponentBOMLine) pasteComp).getItemRevision() ))
					throw new Exception( "The Object " + pasteComp.getStringProperty( "object_string" ) + " is already in the Package."); 
			}
			else
			{
				throw new Exception("Only Objects of Type Item, ItemRevision, BOMLine are allowed to paste in a package.");
			}
		}
	}
	
	
	public TCComponentItemRevision getTargetRev() {
		return targetRev;
	}

	private abstract class CadexJob extends Job {

		public CadexJob(String name) {
			super(name);
			// TODO Auto-generated constructor stub
		}

		@Override
		protected IStatus run(final IProgressMonitor arg0)
		{
			CadexView.this.loadMidComposite.startLoading(this.getName());
			arg0.beginTask( this.getName() , -1);
			
			try 
			{
				Display.getDefault().syncExec(new Runnable() {
					
					@Override
					public void run() {
						try {
							parallelActivity();
						} catch (Exception e) {
							CadexUtils.openError( cParent.getShell() , "Operation Error", "Problem occured during " + getName() , e );
						}
					}
				});
									
			} 
			catch (final Exception e) {
				Display.getDefault().syncExec(new Runnable() {
					
					@Override
					public void run() {
						CadexUtils.openError( cParent.getShell() , "Refresh Error", "Problem occured during refreshing." , e );
					}
				});
				e.printStackTrace();
			} 
				
			arg0.beginTask( "Refreshing packages list...." , -1);
			
			Display.getDefault().syncExec(new Runnable() {
				
				@Override
				public void run() {
					
//					if (!CadexView.this.isDisposed()) 
//					{
						try 
						{
							arg0.beginTask( "Refreshing package tree information...." , -1);
							arg0.beginTask("Setting Tree Information....", -1);
							CadexView.this.loadMidComposite.finishLoading();
							ceTreeViewer.setInput( cadexModel ) ;
//							IWorkbench workbench = PlatformUI.getWorkbench();
//					        IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();
//							IViewPart viewPart = 
//									window.getActivePage().showView("com.teamcenter.rac.tccadex.views.cadexView");
//							CadexView.this.setTitleToolTip("Supplier Info Exchange");
							cParent.layout();
							setStatus();
						} 
						catch (final Exception e) {
							Display.getDefault().syncExec(new Runnable() {
								
								@Override
								public void run() {
									CadexUtils.openError( cParent.getShell() , "Refresh Error", "Problem occured during refreshing." , e );
								}
							});
							e.printStackTrace();
						} 
//					}
				}
			});

			
			return Status.OK_STATUS;
			
		}
		
		public abstract void parallelActivity() throws Exception;

	}
	

	@Override
	public void selectionChanged(SelectionChangedEvent event) {
		IStructuredSelection selection = (IStructuredSelection) event.getSelection();
		IWorkbench workbench = PlatformUI.getWorkbench();
        IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();
		try {
			if( selection.getFirstElement() !=null )
			{
				AIFComponentContext context = (AIFComponentContext) selection.getFirstElement();
				String objType = context.getComponent().getProperty( "object_type" );
				System.out.println(objType);
				if(objType.equals("CE_ExchangePackageRevision"))
				{
					TCComponentItemRevision revision = (TCComponentItemRevision) context.getComponent();
					targetRev = revision;
					m_tcSession = targetRev.getSession();
					packageString = targetRev.getStringProperty( "object_string" );
			        //workbench.showPerspective( "com.teamcenter.rac.tccadex.perspectives.cadexPerspective", window, null );
//					viewPart.init( window.getActivePage().getActivePart(). )
					Control[] children = cParent.getChildren();
				    for (int i = 0 ; i < children.length; i++) {
				        children[i].dispose();
				    }
				    initUI();
				    
				}
			}
		} catch (Exception e) {
			CadexUtils.openError(window.getShell(), "Open Exchange Package error", "Failed to open the Exchange Package.", e);
		}
	}
}

/*=================================================================================================                    
#                Copyright (c) 2014 Some Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           DatasetsView.java          
#      Module          :           com.teamcenter.rac.tccadex.views          
#      Description     :           Core Datasets view in Supplier CAD Exchange application          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.views;

import java.util.Arrays;
import java.util.HashMap;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.viewers.CheckboxCellEditor;
import org.eclipse.jface.viewers.ColumnViewerEditor;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationEvent;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationStrategy;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TableViewerEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.part.ViewPart;

import com.teamcenter.rac.kernel.TCComponentBOMLine;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.tccadex.utils.CECommonUtils;
import com.teamcenter.rac.tccadex.utils.CadexUtils;
import com.teamcenter.rac.tccadex.views.actions.CustomAction;
import com.teamcenter.rac.tccadex.views.datasetsview.CadexTCDatasetComponent;
import com.teamcenter.rac.tccadex.views.datasetsview.DatasetViewContentProvider;
import com.teamcenter.rac.tccadex.views.datasetsview.DatasetViewEditingSupport;
import com.teamcenter.rac.tccadex.views.datasetsview.DatasetViewLabelProvider;
import com.teamcenter.rac.util.Registry;

public class DatasetsView extends ViewPart implements ISelectionChangedListener {
	private Table table;
	Registry reg = Registry.getRegistry(this);
	TCSession m_tcSession=CECommonUtils.tSes;
	TableViewer tableViewer;
	Composite cParent ;
	int columnCount = -1;
	HashMap<Integer, String> columnMapping = new HashMap<Integer,String>();
	CheckboxCellEditor checkboxCellEditor;
	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());
	Section sctnNewSection;
	
	public DatasetsView() {
	}

	@Override
	public void createPartControl(Composite parent) {
		
		cParent = parent ;
		parent.setLayout(new GridLayout(1, false));
		
		sctnNewSection = formToolkit.createSection(parent, Section.TITLE_BAR);
		GridData gd_sctnNewSection = new GridData(SWT.FILL, SWT.BOTTOM, false, false, 1, 1);
		gd_sctnNewSection.heightHint = 40;
		sctnNewSection.setLayoutData(gd_sctnNewSection);
		formToolkit.paintBordersFor(sctnNewSection);
		sctnNewSection.setText("");
		
		tableViewer = new TableViewer(parent, SWT.BORDER | SWT.FULL_SELECTION);
		table = tableViewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		ColumnViewerEditorActivationStrategy actSupport = new ColumnViewerEditorActivationStrategy(tableViewer) {
			protected boolean isEditorActivationEvent(
					ColumnViewerEditorActivationEvent event) {
				return event.eventType == ColumnViewerEditorActivationEvent.TRAVERSAL
				|| event.eventType == ColumnViewerEditorActivationEvent.MOUSE_CLICK_SELECTION
				|| (event.eventType == ColumnViewerEditorActivationEvent.KEY_PRESSED && ( event.keyCode == SWT.CR || event.character == ' ' ))
				|| event.eventType == ColumnViewerEditorActivationEvent.PROGRAMMATIC;
			}
		};
		
		TableViewerEditor.create(tableViewer, null, actSupport, ColumnViewerEditor.TABBING_HORIZONTAL
				| ColumnViewerEditor.TABBING_MOVE_TO_ROW_NEIGHBOR
				| ColumnViewerEditor.TABBING_VERTICAL | ColumnViewerEditor.KEYBOARD_ACTIVATION);
		
		TableViewerColumn tableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		TableColumn tblclmnObject = tableViewerColumn.getColumn();
		tblclmnObject.setAlignment(SWT.CENTER);
		tblclmnObject.setWidth(135);
		tblclmnObject.setText(reg.getString( "columnObject.title" ));
		columnMapping.put( ++columnCount , reg.getString( "columnObject.real" ));
		
		TableViewerColumn treeViewerColumn_2 = new TableViewerColumn(tableViewer, SWT.CHECK);
		TableColumn trclmnExport = treeViewerColumn_2.getColumn();
		trclmnExport.setAlignment(SWT.CENTER);
		trclmnExport.setMoveable(true);
		trclmnExport.setWidth(111);
		trclmnExport.setText(reg.getString( "columnExport.title" ));
		columnMapping.put( ++columnCount , reg.getString( "columnExport.real" ));
		treeViewerColumn_2.setEditingSupport( new DatasetViewEditingSupport(tableViewer, columnCount, this) );
		
		TableViewerColumn treeViewerColumn_1 = new TableViewerColumn(tableViewer, SWT.CHECK);
		TableColumn trclmnCheckOut = treeViewerColumn_1.getColumn();
		trclmnCheckOut.setMoveable(true);
		trclmnCheckOut.setAlignment(SWT.CENTER);
		trclmnCheckOut.setWidth(125);
		trclmnCheckOut.setText(reg.getString( "columnCheckOut.title" ));
		columnMapping.put( ++columnCount , reg.getString( "columnCheckOut.real" ));
		treeViewerColumn_1.setEditingSupport( new DatasetViewEditingSupport(tableViewer, columnCount, this) );
		
		Action lCustomAction = new CustomAction("Dataset","CADEXDsetClmnShownPref");
		lCustomAction.setText(reg.getString( "columnConfiguration.title" ));
		getViewSite().getActionBars().getMenuManager().add(lCustomAction);
		getViewSite().getActionBars().getMenuManager().add(new Separator());
		
		checkboxCellEditor = new CheckboxCellEditor(tableViewer.getTable());
		
		TableViewerColumn tableViewerColumn_1 = new TableViewerColumn(tableViewer, SWT.NONE);
		TableColumn tblclmnCheckedOut = tableViewerColumn_1.getColumn();
		tblclmnCheckedOut.setWidth(100);
		tblclmnCheckedOut.setText(reg.getString( "columnCheckedOut.title" ));
		columnMapping.put( ++columnCount , reg.getString( "columnCheckedOut.real" ));
		tableViewerColumn_1.setEditingSupport( new DatasetViewEditingSupport(tableViewer, columnCount, this) );
		
		TableViewerColumn tableViewerColumn_2 = new TableViewerColumn(tableViewer, SWT.NONE);
		TableColumn tblclmnCheckedOutBy = tableViewerColumn_2.getColumn();
		tblclmnCheckedOutBy.setWidth(100);
		tblclmnCheckedOutBy.setText(reg.getString( "columnCoBy.title" ));
		columnMapping.put( ++columnCount , reg.getString( "columnCoBy.real" ));
		tableViewerColumn_2.setEditingSupport( new DatasetViewEditingSupport(tableViewer, columnCount, this) );
		
		setTableData();
		try {
			createColumns();
		} catch (Exception e) {
			CadexUtils.openError(cParent.getShell(), "Columns Error", e.getMessage(), e);
		}
		
		tableViewer.addSelectionChangedListener( new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent arg0) {
				CadexTCDatasetComponent dsetComp = (CadexTCDatasetComponent) ((IStructuredSelection)arg0.getSelection()).getFirstElement();
				sctnNewSection.setText( dsetComp !=null ? dsetComp.getProblemFound() : "" );
			}
		} );
		
	}
	
	private void setTableData()
	{
		tableViewer.setContentProvider( new DatasetViewContentProvider(cParent.getShell()) );
		tableViewer.setLabelProvider( new DatasetViewLabelProvider(this) );
	}
	
	private void createColumns() throws Exception
	{
		TCPreferenceService prefService = m_tcSession.getPreferenceService();
		String[] shownProps = prefService.getStringValues( "CADEXDsetClmnShownPref" );
		if(shownProps!=null && shownProps.length > 0 )
		{
			HashMap<String, String> realDispMap = CECommonUtils.getDisplayName("Dataset", Arrays.asList( shownProps ));
			
			for(int i=0; i<shownProps.length; i++)
			{
				TableViewerColumn propViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
				TableColumn propColumn = propViewerColumn.getColumn();
				propColumn.setWidth(138);
				propColumn.setText(realDispMap.get( shownProps[i] ));
				columnMapping.put( ++columnCount ,shownProps[i].substring(shownProps[i].indexOf( "." )+1) );
			}
		}
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}

	@Override
	public void selectionChanged(SelectionChangedEvent event) {
		IStructuredSelection selection = (IStructuredSelection) event.getSelection();
		TCComponentBOMLine line = (TCComponentBOMLine)selection.getFirstElement();
		tableViewer.setInput( line );
	}
	
	public String getColumn(int index)
	{
		return this.columnMapping.get(index);
	}
	
	public CheckboxCellEditor getCheckboxCellEditor() {
		return checkboxCellEditor;
	}
	
	public void refreshTreeNode(Object arg0) throws TCException
	{
		((CadexTCDatasetComponent)arg0).refresh();
		this.tableViewer.update(arg0,null);
	}

}

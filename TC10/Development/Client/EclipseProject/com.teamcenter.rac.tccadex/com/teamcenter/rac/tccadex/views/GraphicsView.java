/*=================================================================================================                    
#                Copyright (c) 2014 Some Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           GraphicsView.java          
#      Module          :           com.teamcenter.rac.tccadex.views          
#      Description     :           OOTB View to show JT models of the assemblies in Supplier CAD Exchange application          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.views;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.Panel;

import javax.swing.JRootPane;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.common.tcviewer.DirectModelViewer;
import com.teamcenter.rac.common.tcviewer.JTViewer;
import com.teamcenter.rac.common.tcviewer.TCComponentDatasetViewerInput;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentBOMLine;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCException;

public class GraphicsView extends ViewPart implements ISelectionChangedListener {
	TCComponent comp = null;
	JTViewer newJTViewer;
	public GraphicsView() {
		comp = null;
	}

	protected void createContent(Composite arg0) {
		
	}

	@Override
	public void createPartControl(Composite arg0) {
		arg0.setLayout(new GridLayout(1, false));
		Composite composite = new Composite(arg0, SWT.EMBEDDED);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		composite.setLayout(new GridLayout(1, false));
		
		Frame frame = SWT_AWT.new_Frame(composite);
		
		Panel panel = new Panel();
		frame.add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		JRootPane rootPane = new JRootPane();
		panel.add(rootPane);
		// TODO Auto-generated method stub
		//newJTViewer = new JTViewer();
		//rootPane.getContentPane().add( newJTViewer );
		
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void selectionChanged(SelectionChangedEvent arg0) {
		
		TCComponentBOMLine bomLine = (TCComponentBOMLine) ((ITreeSelection)arg0.getSelection()).getFirstElement();
		TCComponent needed = null;
//		try {
//			
//		
//	
//		
//		TCComponentDataset dataset = (TCComponentDataset) getAttachmentLine(bomLine).getReferenceProperty( "al_object" );
//			
//		DataManagement.ViewerData viewerdata = new ViewerData();
//		viewerdata.inputObj = dataset;
//		viewerdata.viewableObj = dataset;
//		TypeToNamedRefPair[] pairs = new TypeToNamedRefPair[1];
//		
//		pairs[0] = new TypeToNamedRefPair();
//		String[] tickets = {"someticket"};
//		pairs[0].namedRefType = "JTPART";
//		pairs[0].fileTickets = tickets;
//		
//		viewerdata.viewableNamedRefs = pairs;
//		
//		AIFComponentContext[] childs = new AIFComponentContext[bomLine.getChildrenCount()];
//		
//		childs = bomLine.getChildren();
//		viewerdata.traversal = new TCComponent[bomLine.getChildrenCount()];
//		for (int i = 0; i < bomLine.getChildrenCount(); i++) {
//			
//			TCComponentBOMLine childLine = (TCComponentBOMLine)childs[i].getComponent();
//			viewerdata.traversal[i] = (TCComponentDataset) getAttachmentLine(childLine).getReferenceProperty( "al_object" );
//		}
//		
//		TCComponentDatasetViewerInput dsetViewerInput = new TCComponentDatasetViewerInput( viewerdata );
//		
//		System.out.println( dsetViewerInput );
//		newJTViewer.setInput(dsetViewerInput);
//		newJTViewer.repaint();
//		} catch (TCException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
	private TCComponent getAttachmentLine(TCComponentBOMLine bomLine)
	{
		try {
			TCComponent[] comps = bomLine.getReferenceListProperty( "bl_dataset_attachments" );
			
			for (int i = 0; i < comps.length; i++) {
				if(comps[i].getStringProperty( "al_source_type" ).equals( "DirectModel" ))
					{
						return comps[i];
					}
			}
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

}

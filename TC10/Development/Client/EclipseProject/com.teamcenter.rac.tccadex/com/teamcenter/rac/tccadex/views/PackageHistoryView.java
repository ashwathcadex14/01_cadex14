/*=================================================================================================                    
#                Copyright (c) 2014 Some Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           PackageHistoryView.java          
#      Module          :           com.teamcenter.rac.tccadex.views          
#      Description     :           View to display the package stages          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.views;

import java.util.HashMap;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IPartListener2;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import com.teamcenter.rac.tccadex.EarlyStartup;
import com.teamcenter.rac.tccadex.views.packagehistoryview.PkgHistoryContentProvider;
import com.teamcenter.rac.tccadex.views.packagehistoryview.PkgHistoryLabelProvider;
import com.teamcenter.rac.util.Registry;

public class PackageHistoryView extends ViewPart implements IPartListener2{
	private Table table;
	TableViewer tableViewer;
	Composite cParent ;
	int columnCount = -1;
	HashMap<Integer, String> columnMapping = new HashMap<Integer,String>();
	Registry reg = Registry.getRegistry(this);
	public PackageHistoryView() {
	}

	@Override
	public void createPartControl(Composite arg0) {
		cParent = arg0 ;
		arg0.setLayout(new GridLayout(1, false));
		
		tableViewer = new TableViewer(arg0, SWT.BORDER | SWT.FULL_SELECTION);
		table = tableViewer.getTable();
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		TableViewerColumn tableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		TableColumn tblclmnStatus = tableViewerColumn.getColumn();
		tblclmnStatus.setAlignment(SWT.CENTER);
		tblclmnStatus.setWidth(138);
		tblclmnStatus.setText(reg.getString( "columnStatus.title" ));
		columnMapping.put( ++columnCount , reg.getString( "columnStatus.real" ));
		
		TableViewerColumn tableViewerColumn_1 = new TableViewerColumn(tableViewer, SWT.NONE);
		TableColumn tblclmnDate = tableViewerColumn_1.getColumn();
		tblclmnDate.setAlignment(SWT.CENTER);
		tblclmnDate.setWidth(158);
		tblclmnDate.setText(reg.getString( "columnDate.title" ));
		columnMapping.put( ++columnCount , reg.getString( "columnDate.real" ));
		
		TableViewerColumn tableViewerColumn_2 = new TableViewerColumn(tableViewer, SWT.NONE);
		TableColumn tblclmnDescription = tableViewerColumn_2.getColumn();
		tblclmnDescription.setAlignment(SWT.CENTER);
		tblclmnDescription.setWidth(279);
		tblclmnDescription.setText(reg.getString( "columnDesc.title" ));
		columnMapping.put( ++columnCount , reg.getString( "columnDesc.real" ));
		
		tableViewer.setLabelProvider(new PkgHistoryLabelProvider(this));
		tableViewer.setContentProvider(new PkgHistoryContentProvider(cParent.getShell()));
		

	}

	@Override
	public void setFocus() {
		updateView();
	}
	
	public String getColumn(int index)
	{
		return this.columnMapping.get(index);
	}

	@Override
	public void partActivated(IWorkbenchPartReference iworkbenchpartreference) {
		updateView();
	}

	@Override
	public void partBroughtToTop(IWorkbenchPartReference iworkbenchpartreference) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void partClosed(IWorkbenchPartReference iworkbenchpartreference) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void partDeactivated(IWorkbenchPartReference iworkbenchpartreference) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void partOpened(IWorkbenchPartReference iworkbenchpartreference) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void partHidden(IWorkbenchPartReference iworkbenchpartreference) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void partVisible(IWorkbenchPartReference iworkbenchpartreference) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void partInputChanged(IWorkbenchPartReference iworkbenchpartreference) {
		// TODO Auto-generated method stub
		
	}
	
	private void updateView()
	{
		IViewReference viewRef = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findViewReference( EarlyStartup.CADEX_VIEW_ID );
		if(viewRef!=null)
		{
			try {
				CadexView cadexView = (CadexView) viewRef.getView(true);		
				tableViewer.setInput( cadexView.targetRev ) ;
				tableViewer.refresh();
			} catch (Exception e) {
				//swallowing !!!!!!
			}
		}
	}

}

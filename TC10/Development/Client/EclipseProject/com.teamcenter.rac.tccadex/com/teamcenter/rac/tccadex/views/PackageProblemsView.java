/*=================================================================================================                    
#                Copyright (c) 2014 Some Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           PackageProblemsView.java          
#      Module          :           com.teamcenter.rac.tccadex.views          
#      Description     :           View to display the package validation errors          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.views;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.ui.IPartListener2;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.wb.swt.ResourceManager;

import com.teamcenter.rac.tccadex.EarlyStartup;
import com.teamcenter.rac.tccadex.views.cadexview.CadexViewModel;
import com.teamcenter.rac.tccadex.views.cadexview.CadexViewModel.ErrorMessage;
import com.teamcenter.rac.tccadex.views.datasetsview.CadexTCDatasetComponent;

public class PackageProblemsView extends ViewPart implements IPartListener2
{
	private CadexView cadexView;
	TreeViewer treeViewer;
	public PackageProblemsView() {
		this.cadexView = (CadexView) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findViewReference( EarlyStartup.CADEX_VIEW_ID ).getView(true);
		this.cadexView.getSite().getPage().addPartListener(this);
	}

	@Override
	public void createPartControl(Composite arg0) {
		arg0.setLayout(new GridLayout(1, false));
		
		treeViewer = new TreeViewer(arg0, SWT.BORDER);
		Tree tree = treeViewer.getTree();
		tree.setLinesVisible(true);
		tree.setHeaderVisible(true);
		tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		TreeViewerColumn treeViewerColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		TreeColumn trclmnError = treeViewerColumn.getColumn();
		trclmnError.setWidth(456);
		trclmnError.setText("Error");
		
		TreeViewerColumn treeViewerColumn_1 = new TreeViewerColumn(treeViewer, SWT.NONE);
		TreeColumn trclmnObject = treeViewerColumn_1.getColumn();
		trclmnObject.setWidth(168);
		trclmnObject.setText("Object");
		
		treeViewer.setContentProvider( new ProblemsContentProvider() );
		treeViewer.setLabelProvider( new ProblemsLabelProvider() );
		treeViewer.setInput( this.cadexView.getCadexModel() );
		
	}

	@Override
	public void setFocus() {
		updateView();
	}
	
	private class ProblemsLabelProvider implements ITableLabelProvider
	{

		private Image ERROR = ResourceManager.getPluginImage("com.teamcenter.rac.tccadex", "com/teamcenter/rac/tccadex/images/error.jpg") ;
		
		@Override
		public void addListener(ILabelProviderListener arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void dispose() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean isLabelProperty(Object arg0, String arg1) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void removeListener(ILabelProviderListener arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public Image getColumnImage(Object arg0, int arg1) {
			if(arg1==0)
			{
				return ERROR;
			}
			else
				return null;
		}

		@Override
		public String getColumnText(Object arg0, int arg1) 
		{
			
			if(arg0 instanceof ErrorMessage)
			{
				switch (arg1) 
				{
				case 0:
					return ((ErrorMessage)arg0).getLineMessage();
				case 1:
					return ((ErrorMessage)arg0).getLineObj();
				default:
					return null;
				}
			}
			else if( arg0 instanceof CadexTCDatasetComponent )
			{
				switch (arg1) 
				{
					case 0:
						return ((CadexTCDatasetComponent)arg0).getProblemFound();
					case 1:
						return ((CadexTCDatasetComponent)arg0).getObjString();
					default:
						return null;
				}
			}
			else
				return null;
				
		}
		
	}
	
	private class ProblemsContentProvider implements ITreeContentProvider
	{

		CadexViewModel model ;
		@Override
		public void dispose() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void inputChanged(Viewer arg0, Object arg1, Object arg2) {
			model=(CadexViewModel) arg2;
		}

		@Override
		public Object[] getChildren(Object arg0) {
			if(arg0 instanceof ErrorMessage)
				return ((ErrorMessage)arg0).getDsetMessage().toArray();
			else
				return null;
		}

		@Override
		public Object[] getElements(Object arg0) {
			return model.getAllProblems().toArray();
		}

		@Override
		public Object getParent(Object arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean hasChildren(Object arg0) {
			if(arg0 instanceof ErrorMessage)
				return ((ErrorMessage)arg0).getDsetMessage().size()>0;
			else
				return false;
		}
		
	}

	@Override
	public void partActivated(IWorkbenchPartReference arg0) {
		updateView();
	}

	@Override
	public void partBroughtToTop(IWorkbenchPartReference arg0) {
		// TODO Auto-generated method stub
		System.out.println();
	}

	@Override
	public void partClosed(IWorkbenchPartReference arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void partDeactivated(IWorkbenchPartReference arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void partHidden(IWorkbenchPartReference arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void partInputChanged(IWorkbenchPartReference arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void partOpened(IWorkbenchPartReference arg0) {
		// TODO Auto-generated method stub
		System.out.println();
	}

	@Override
	public void partVisible(IWorkbenchPartReference arg0) {
		// TODO Auto-generated method stub
		System.out.println();
	}
	
	private void updateView()
	{
			try 
			{
				treeViewer.refresh();
			} catch (Exception e) {
				//swallowing !!!!!!
			}
	}

}

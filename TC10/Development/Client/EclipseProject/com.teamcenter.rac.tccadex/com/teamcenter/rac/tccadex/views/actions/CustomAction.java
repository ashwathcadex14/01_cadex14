/*=================================================================================================                    
#                Copyright (c) 2014 Some Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CustomAction.java          
#      Module          :           com.teamcenter.rac.tccadex.views.actions          
#      Description     :           Action for triggering Column Configuration options          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.views.actions;

import java.awt.EventQueue;
import java.awt.Frame;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.eclipse.jface.action.Action;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.tccadex.ui.dialogs.CESelectColumnShownDialog;

public class CustomAction extends Action {

	String pref;
	String type;
	
	public CustomAction(String type, String pref) {
		this.type = type;
		this.pref = pref;
	}
	
	@Override
	public void run() 
	{
		popUpSwingDialog();
	}
	
	public void popUpSwingDialog() 
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run() {
				try 
				{
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					
					TCSession tcSession = (TCSession) AIFUtility.getCurrentApplication().getSession();
					Frame parentFrame = AIFUtility.getCurrentApplication().getDesktop().getFrame();
					CESelectColumnShownDialog dialog = new CESelectColumnShownDialog(parentFrame, tcSession,type,pref);
					dialog.showDialog();
				}
				catch (ClassNotFoundException localClassNotFoundException)
				{
				}
				catch (InstantiationException localInstantiationException)
				{
				}
				catch (IllegalAccessException localIllegalAccessException)
				{
				}
				catch (UnsupportedLookAndFeelException localUnsupportedLookAndFeelException) {
				}
			}
		});
	}
}

/*=================================================================================================                    
#                Copyright (c) 2014 Some Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CadexViewContentProvider.java          
#      Module          :           com.teamcenter.rac.tccadex.views.cadexview          
#      Description     :           Content Provider for CADEX Tree viewer          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.views.cadexview;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.widgets.Shell;

import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponentBOMLine;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.tccadex.utils.CadexUtils;

public class CadexViewContentProvider implements ITreeContentProvider {
	
	private CadexViewModel model;
	private Shell currentShell;
	
	@Override
	public Object[] getChildren(Object parentElement) {
		try 
		{
			AIFComponentContext[] childs = ((TCComponentBOMLine)parentElement).getChildren();
			TCComponentBOMLine[] childLines = new TCComponentBOMLine[childs.length];
			for(int i=0; i<childs.length ; i++)
			{
				childLines[i]=(TCComponentBOMLine) childs[i].getComponent();				
			}
			return childLines;
		} catch (Exception e) {
			CadexUtils.openError(currentShell, "Load Error" ,"Unable to get children of Bom line.",e);
		}
		return null;
	}

	@Override
	public Object getParent(Object element) {
		
		return null;
	}

	@Override
	public boolean hasChildren(Object element) {
		
		try {
			return ((TCComponentBOMLine)element).getChildrenCount() > 0;
		} catch (TCException e) {
			CadexUtils.openError(currentShell, "Load Error" ,"Unable to get children of Bom line.",e);
		}
		return false;
	}

	@Override
	public Object[] getElements(Object inputElement) {
		Object[] objs = new Object[1];
		objs[0] = model.getTopBomLine(); 
		return objs;
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// TODO Auto-generated method stub
		this.model = (CadexViewModel) newInput;
	}

	public CadexViewContentProvider(Shell parent) {
		this.currentShell = parent;
	}
}

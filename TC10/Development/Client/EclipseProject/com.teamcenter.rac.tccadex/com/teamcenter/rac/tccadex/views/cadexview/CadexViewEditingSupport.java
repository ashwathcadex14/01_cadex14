/*=================================================================================================                    
#                Copyright (c) 2014 Some Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CadexViewEditingSupport.java          
#      Module          :           com.teamcenter.rac.tccadex.views.cadexview          
#      Description     :           Editing Support for CADEX Tree Viewer columns          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.views.cadexview;

import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.EditingSupport;

import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponentBOMLine;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.tccadex.utils.CadexUtils;
import com.teamcenter.rac.tccadex.views.CadexView;
import com.teamcenter.rac.util.Registry;

public class CadexViewEditingSupport extends EditingSupport {

	int columnIndex ;
	Registry reg = Registry.getRegistry(this);
	List<String> editableColumns = Arrays.asList( reg.getStringArray( "cadexViewEditableColumns" ) );
	CadexView mainView ;
	String propName ;
	
	public CadexViewEditingSupport(ColumnViewer viewer, int index, CadexView cadexView) {
		super(viewer);
		this.columnIndex = index;
		this.mainView = cadexView;
		propName = this.mainView.getColumn( this.columnIndex );
	}

	@Override
	protected boolean canEdit(Object arg0) {
		try {
			return checkEdit(arg0);
		} catch (Exception e) {
			CadexUtils.openError(this.mainView.getSite().getShell(), "Cannot Edit", e.getMessage(),e);
		}
		return false;
	}

	@Override
	protected CellEditor getCellEditor(Object arg0) {
		// TODO Auto-generated method stub
		return this.mainView.getCheckboxCellEditor();
	}

	@Override
	protected Object getValue(Object arg0) {
		try 
		{
			String propValue = ((TCComponentBOMLine)arg0).getStringProperty( propName );
			return propValue == null || propValue.length() == 0 ? false : Boolean.valueOf(propValue);
		} catch (TCException e) {
			CadexUtils.openError(this.mainView.getSite().getShell(), "Value Error", "Unable to get property " + propName, e );
		}
		
		return null;
	}

	@Override
	protected void setValue(Object arg0, Object arg1) 
	{
		TCComponentBOMLine thisLine = (TCComponentBOMLine) arg0;
		boolean propChilds = false;
		
		try 
		{
			if( thisLine.getChildrenCount() > 0 )
			{
				if( MessageDialog.openQuestion( this.mainView.getSite().getShell(), "Apply changes", "Should the changes be applied to all child lines?" ) )
				{
					propChilds = true;
				}
			}
			
			setAllChilds(thisLine,String.valueOf( arg1 ), propChilds);
			
			this.mainView.refreshTreeNode(arg0);
			this.mainView.updateStatus();
			
		} catch ( Exception e) {
			CadexUtils.openError(this.mainView.getSite().getShell(), "Set Error", "Unable to set property " + propName, e );
		}
		
		
		
	}
	
	private boolean checkEdit(Object arg0) throws Exception
	{
		if( editableColumns.contains( String.valueOf( this.columnIndex ) ) )		
		{
			switch(this.columnIndex)
			{
				case 2:
					return true;//Export attribute is editable always.need to check
				case 3: 
					return validateCheckOut(arg0);
				case 6:
					return true;//Rendering attribute is editable always.need to check
				case 7:
					return true;//Native attribute is editable always. need to check
				default:
					return false;
			}
		}
		else
			return false;
	}

	private boolean validateCheckOut(Object arg0) throws Exception
	{
		TCComponentBOMLine thisLine = (TCComponentBOMLine) arg0;
		
		return isTarget( thisLine );
	}
	
	private boolean isTarget(TCComponentBOMLine line) throws Exception
	{
		try 
		{
			String type = line.getStringProperty("CE4_attachType");
			
			if( type.equals( "Target" ) )
			{
				return true;
			}
			else
				return false;
			
		} catch (TCException e) {
			throw new Exception("Problem occured while evaluating line for target or reference.",e);
		}
	}
	
	private void setAllChilds(TCComponentBOMLine line, String value, boolean setChilds) throws Exception
	{
		try 
		{
			setDependentProperty(line,value);
			
			if(setChilds)
			{
				AIFComponentContext[] lineChilds = line.getChildren();
			
				if(lineChilds!=null && lineChilds.length>0)
				{
					for(int i=0;i<lineChilds.length;i++)
					{
						TCComponentBOMLine currentLine = (TCComponentBOMLine) lineChilds[i].getComponent();
						setAllChilds(currentLine, value, setChilds);
					}
				}
			}
			
		} catch (TCException e) {
			throw new Exception("Problem occured while setting attribute.",e);
		}
	}
	
	private void setDependentProperty(TCComponentBOMLine line, String value) throws TCException
	{
		line.setProperty(propName, value);
		
		if(( propName.equals( "CE4_checkOutNode" )  || propName.equals( "CE4_exportRendering" ) || propName.equals( "CE4_exportNative" )) && value.equals( "true" ))
		{
			line.setProperty("CE4_exportNode", value);
		}
		else if( propName.equals( "CE4_exportNode" ) && value.equals( "false" ) )
		{
			line.setProperty("CE4_checkOutNode", value);
			line.setProperty("CE4_exportRendering", value);
			line.setProperty("CE4_exportNative", value);
			line.setProperty( "CE4_exportedDsets" , "");
			line.setProperty( "CE4_checkedOutDsets" , "");
		}
		this.mainView.refreshTreeNode(line);
	}

}

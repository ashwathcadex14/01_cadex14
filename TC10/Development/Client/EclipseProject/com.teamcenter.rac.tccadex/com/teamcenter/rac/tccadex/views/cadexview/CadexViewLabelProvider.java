/*=================================================================================================                    
#                Copyright (c) 2014 Some Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CadexViewLabelProvider.java          
#      Module          :           com.teamcenter.rac.tccadex.views.cadexview          
#      Description     :           Label Provider for CADEX View Tree Viewer          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.views.cadexview;

import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableColorProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.wb.swt.ResourceManager;

import com.teamcenter.rac.kernel.TCComponentBOMLine;
import com.teamcenter.rac.tccadex.utils.CEIconProvider;
import com.teamcenter.rac.tccadex.utils.CadexUtils;
import com.teamcenter.rac.tccadex.views.CadexView;

public class CadexViewLabelProvider implements ITableLabelProvider,ITableColorProvider {
	private CadexView mainView;
	private Shell currentShell;
	private static Image CHECKED = ResourceManager.getPluginImage("com.teamcenter.rac.tccadex", "com/teamcenter/rac/tccadex/images/LargeCheck.png") ;
	private static Image CHECKED_OUT = ResourceManager.getPluginImage("com.teamcenter.rac.tccadex", "com/teamcenter/rac/tccadex/images/Checkout.png") ;

	  public CadexViewLabelProvider(CadexView parent) {
		  this.mainView = parent;
		  this.currentShell=parent.getSite().getShell();
	  }

	@Override
	public void addListener(ILabelProviderListener arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isLabelProperty(Object arg0, String arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Image getColumnImage(Object arg0, int arg1) {
		  
		try
		{
			if( arg1 == 0 )
			{
				String tObj = ((TCComponentBOMLine)arg0).getStringProperty( "bl_item_object_type" );
				return CEIconProvider.getTCTypeIcon( tObj ) ;
			}
			else if( arg1 == 2 || arg1 == 3 || arg1 == 6 || arg1 == 7 )
			{
				return getColumnText(arg0, arg1) != null && getColumnText(arg0, arg1).equals("true") ? CHECKED : null;
			}
			else if(arg1==4)
			{
				return isCheckedOut((TCComponentBOMLine)arg0) ? CHECKED_OUT : null ;
			}
			else
			{
//				TCComponentBOMLine line = (TCComponentBOMLine) arg0;
//				return line.getStringProperty( this.mainView.getColumn(index) );
				return null;
			}
				
		} catch (Exception e) {
			CadexUtils.openError(currentShell, "Load Error" ,"Unable to get properties for bomline.",e );
		}
		return null;
	}
	
	public static boolean isCheckedOut(TCComponentBOMLine line) throws Exception
	{
		return retCo(line).isCo;
	}

	@Override
	public String getColumnText(Object arg0, int arg1) 
	{
		try 
		{
			if(arg1==5)
			{
				return retCo(arg0).checkedOutBy;
			}
			else
			{
				String value = ((TCComponentBOMLine) arg0).getStringProperty( this.mainView.getColumn(arg1) );
			
				return value!=null && ( value.equals("true") || value.equals( "false" )) ? value.equals( "true" ) ? "true" : "" : value ;
			}
		} 
		catch (Exception e) 
		{
			CadexUtils.openError(currentShell, "Load Error" ,"Unable to get properties for bomline.",e);
		}
		return "Error";
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITableColorProvider#getBackground(java.lang.Object, int)
	 */
	@Override
	public Color getBackground(Object arg0, int arg1)
	{
			if(this.mainView.getCadexModel().getMessage((TCComponentBOMLine)arg0).length()>0)
			{
				mainView.setMessage( "Please resolve the problems in the package. Click on individual objects for the problem details." , IMessageProvider.ERROR);
				return new Color(Display.getCurrent(),253, 97, 97);
			}
			else
				mainView.setMessage("",IMessageProvider.NONE);
		
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITableColorProvider#getForeground(java.lang.Object, int)
	 */
	@Override
	public Color getForeground(Object arg0, int arg1) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private static CheckOutDetail retCo(Object arg0) throws Exception
	{
		return new CheckOutDetail(arg0);
	}
	  
}

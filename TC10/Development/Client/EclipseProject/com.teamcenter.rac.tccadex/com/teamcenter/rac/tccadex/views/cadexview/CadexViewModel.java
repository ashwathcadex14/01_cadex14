/*=================================================================================================                    
#                Copyright (c) 2014 BAVIS Technology Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CadexViewModel.java          
#      Module          :           com.teamcenter.rac.tccadex.views.cadexview          
#      Description     :           Model of CADEX View          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.views.cadexview;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.eclipse.swt.widgets.Shell;

import com.teamcenter.rac.kernel.TCComponentBOMLine;
import com.teamcenter.rac.kernel.TCComponentBOMWindow;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentRevisionRule;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.tccadex.utils.CECommonUtils;
import com.teamcenter.rac.tccadex.views.datasetsview.CadexTCDatasetComponent;
import com.teamcenter.schemas.soa._2006_03.exceptions.ServiceException;

public class CadexViewModel {
	
	private TCComponentItemRevision targetPackage = null;
	private TCComponentBOMLine currentPkgLine = null;
	private TCComponentBOMWindow currentWindow = null;
	private HashMap<TCComponentBOMLine, ErrorMessage> problemLines = new HashMap<TCComponentBOMLine, ErrorMessage>();
	private boolean problemExists = false;
	private boolean isExportable = false;
	
	public CadexViewModel(TCComponentItemRevision targetRev) throws Exception {
		this.targetPackage = targetRev;
		expandBomLines();
		setInitialStatus();
		//preValidate();
	}
	
	private void expandBomLines() throws Exception
	{
		this.currentPkgLine = CECommonUtils.createBOMWindow( this.targetPackage );
		this.currentWindow = this.currentPkgLine.window();
		this.currentWindow.setAbsoccEditMode(true);
		this.currentWindow.setAbsoccContextBOMLine( this.currentPkgLine );
		//unpackBomStructure();
		String currentRevRule = this.targetPackage.getStringProperty( "ce4_revisionRule");
		if( currentRevRule != null && currentRevRule.length() > 0 )
		{
			TCComponentRevisionRule revRule = CECommonUtils.getRevisionRule(currentRevRule);
			if( revRule == null )
				throw new Exception("Revision Rule specified in the package is invalid.");
			else
				setRevisionRule(revRule);
		}
	}
	
	public void setRevisionRule(TCComponentRevisionRule rule) throws TCException
	{
		this.currentWindow.setRevisionRule(rule);
		this.currentWindow.refresh();
		this.targetPackage.setStringProperty( "ce4_revisionRule" , rule.getStringProperty("object_name"));
	}
	
	public TCComponentBOMLine getTopBomLine()
	{
		return this.currentPkgLine;
	}
	
	public void saveWindow() throws TCException {
		currentWindow.save();
	}
	
	public void closeWindow() throws TCException {
		currentWindow.close();
	}
	
	public void setPkgStatus() throws TCException
	{
		if(this.problemExists)
		{ 
			setPkgStatusProperty("The package had validation issues.", "Export Validation Failed");
		}
		else
		{
			setPkgStatusProperty("The package has passed validation.", "Ready For Export");
		}
	}
	
	public void setPkgStatusProperty(String remarks, String status) throws TCException
	{
		this.targetPackage.setStringProperty( "ce4_statusRemarks" ,  remarks );
		this.targetPackage.setStringProperty( "ce4_status" ,  status) ;
	}
	
	private void setInitialStatus() throws TCException, ServiceException
	{
		String status = this.targetPackage.getStringProperty( "ce4_status" );
		if( status == null || status.length() == 0 )
		{
			this.targetPackage.setStringProperty("ce4_statusRemarks", "Initial preparation stage.");
			this.targetPackage.setStringProperty( "ce4_status" , "Preparation");
		}
	}
	
//	private void unpackBomStructure() throws Exception
//	{
//		List<TCComponentBOMLine> lines = new ArrayList<TCComponentBOMLine>();
//		getAllPackedLines(this.currentPkgLine, lines);
//		TCComponentBOMLine[] packedLines = new TCComponentBOMLine[lines.size()];
//		lines.toArray(packedLines);
//		StructureService localStructureService = StructureService.getService(this.currentPkgLine.getSession());
//	    ServiceData localServiceData = localStructureService.packOrUnpack(packedLines, 1);
//	    SoaUtil.handlePartialErrors(localServiceData, this);
//	}
	
//	private void getAllPackedLines(TCComponentBOMLine line, List<TCComponentBOMLine> packedLines) throws Exception
//	{
//		try 
//		{
//				AIFComponentContext[] lineChilds = line.getChildren();
//			
//				if(lineChilds!=null && lineChilds.length>0)
//				{
//					for(int i=0;i<lineChilds.length;i++)
//					{
//						TCComponentBOMLine currentLine = (TCComponentBOMLine) lineChilds[i].getComponent();
//						if(currentLine.isPacked())
//							packedLines.add( currentLine );
//						getAllPackedLines(currentLine, packedLines);
//					}
//				}			
//		} catch (TCException e) {
//			throw new Exception("Problem occured while setting attribute.",e);
//		}
//	}

	
	public void updateProblemLines(TCComponentBOMLine line, String message) throws TCException
	{
		if(message.length()>0)
		{
			if(problemLines.containsKey(line))
				problemLines.get(line).setLineMessage(message);
		else
				problemLines.put(line, new ErrorMessage(message,line.getStringProperty( "object_string" )));
		}
		else
		{
			if(problemLines.containsKey( line ))
			{
				if(problemLines.get(line).getDsetMessage().size()==0)
					problemLines.remove(line);
			}
		}
		
		setProblemExists();
	}
	
	public void updateProblemDsetLines(TCComponentBOMLine line, CadexTCDatasetComponent comp) throws TCException
	{
		if( problemLines.containsKey( line ) )
		{
			problemLines.get(line).addMsgLines(comp);
		}
		else
		{
			ErrorMessage errMsg = new ErrorMessage("",line.getStringProperty("object_string"));
			errMsg.addMsgLines(comp);
			problemLines.put(line, errMsg);
		}
		setProblemExists();
		
	}
	
	public void removeProblemLine(TCComponentBOMLine line, CadexTCDatasetComponent comp) throws TCException
	{
		if( problemLines.containsKey( line ) )
		{
			ErrorMessage errMsg = problemLines.get(line);
			errMsg.dsetMessage.remove(comp);
			
			if(errMsg.dsetMessage.size()==0 && errMsg.lineMessage.length() == 0)
				problemLines.remove(line);
		}
		
		setProblemExists();
	}
	
	public String getMessage(TCComponentBOMLine line)
	{
		if(problemLines.containsKey( line ))
			return problemLines.get(line).getLineMessage();
		else
			return "";
	}
	
	public Collection<ErrorMessage> getAllProblems()
	{
		return (Collection<ErrorMessage>) this.problemLines.values();
	}

	public void clearProblems()
	{
		this.problemLines.clear();
	}
	
	public boolean isProblemExists() {
		return problemExists;
	}

	public void setProblemExists() {
		this.problemExists = problemLines.size() > 0 ? true : false;
	}
	
	public void preValidate()
	{
		clearProblems();
		ValidationTraversal<TCComponentBOMLine> validate = new ValidationTraversal<TCComponentBOMLine>(this.currentPkgLine, new Shell(),this);
		validate.initiateTraversal();
	}

	public boolean isExportable() {
		return isExportable;
	}

	public void setExportable(boolean isExportable) {
		this.isExportable = isExportable;
	}

	public class ErrorMessage
	{
		String lineMessage = "";
		String lineObj = "";
		ArrayList<CadexTCDatasetComponent> dsetMessage = new ArrayList<CadexTCDatasetComponent>();
		
		public ErrorMessage(String lineMsg, String obj) {
			this.lineMessage=lineMsg;
			this.lineObj=obj;
		}
		
		public void addMsgLines(CadexTCDatasetComponent comp)
		{
			if(this.dsetMessage.contains(comp))
			{
				if(comp.isFoundProblem())
					this.dsetMessage.remove(comp);
				else
				{
					this.dsetMessage.remove( comp );
					this.dsetMessage.add( comp );
				}
			}
			else
			{
				this.dsetMessage.add(comp);
			}
		}

		public String getLineMessage() {
			return lineMessage;
		}

		public void setLineMessage(String lineMessage) {
			this.lineMessage = lineMessage;
		}

		public ArrayList<CadexTCDatasetComponent> getDsetMessage() {
			return dsetMessage;
		}

		public String getLineObj() {
			return lineObj;
		}
	}

}

/*=================================================================================================                    
#                Copyright (c) 2014 Some Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CheckOutDetail.java          
#      Module          :           com.teamcenter.rac.tccadex.views.cadexview          
#      Description     :           Stores the Check out detail of a Dataset or Item Revision          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.views.cadexview;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentBOMLine;

public class CheckOutDetail
{
	public boolean isCo = false;
	public String checkedOutBy = "";
	
	public CheckOutDetail(Object arg0) throws Exception {
		TCComponentBOMLine bomLine = ((TCComponentBOMLine)arg0);
		String itemCo = bomLine.getStringProperty( "bl_item_checked_out" );
		String itemRevCo = bomLine.getStringProperty( "bl_rev_checked_out" );
		String itemRevMasterCo = bomLine.getStringProperty( "ce4_bl_irm_co" );
		String bvCo = bomLine.getStringProperty( "ce4_bl_bv_co" );
		String bvrCo = bomLine.getStringProperty( "ce4_bl_bvr_co" );
		
		if( itemCo!=null && itemCo.equalsIgnoreCase( "Y" ) )
		{
			this.isCo = true;
			this.checkedOutBy = bomLine.getItem().getPropertyDisplayableValue( "checked_out_user" );
		}
		else if( itemRevCo!=null && itemRevCo.equalsIgnoreCase( "Y" ))
		{
			this.isCo = true;
			this.checkedOutBy = bomLine.getItemRevision().getPropertyDisplayableValue( "checked_out_user" );
		}
		else if(itemRevMasterCo!=null && itemRevMasterCo.equalsIgnoreCase( "Y" ))
		{
			this.isCo = true;
			TCComponent[] irmForm = bomLine.getItemRevision().getReferenceListProperty( "IMAN_master_form_rev" );

			this.checkedOutBy = irmForm!=null && irmForm.length == 1 ? irmForm[0].getPropertyDisplayableValue( "checked_out_user" ) : "";
		}
		else if(bvCo!=null && bvCo.equalsIgnoreCase( "Y" ) )
		{
			this.isCo = true;
			TCComponent bv = bomLine.getBOMView();
			this.checkedOutBy = bv!=null ? bv.getPropertyDisplayableValue( "checked_out_user" ) : "";
		}
		else if(bvrCo!=null && bvrCo.equalsIgnoreCase( "Y" ) )
		{
			this.isCo = true;
			TCComponent bvr = bomLine.getBOMViewRevision();
			this.checkedOutBy = bvr!=null ? bvr.getPropertyDisplayableValue( "checked_out_user" ) : "";
		}
	}
}

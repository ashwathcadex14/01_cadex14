package com.teamcenter.rac.tccadex.views.cadexview;

import java.util.ArrayList;

import org.eclipse.swt.widgets.Shell;

import com.teamcenter.rac.kernel.TCComponentBOMLine;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.tccadex.utils.BOMTraversalEngine;
import com.teamcenter.rac.tccadex.views.datasetsview.CadexTCDatasetComponent;
import com.teamcenter.rac.tccadex.views.datasetsview.DatasetViewContentProvider;
import com.teamcenter.rac.util.Registry;

public class ValidationTraversal<T> extends BOMTraversalEngine<TCComponentBOMLine> {

	CadexViewModel cadexModel ;
	Registry reg = Registry.getRegistry(this);
	public ValidationTraversal(TCComponentBOMLine topLine, Shell shell) {
		super(topLine, shell);
		// TODO Auto-generated constructor stub
	}
	
	public ValidationTraversal(TCComponentBOMLine topLine, Shell shell, CadexViewModel model) {
		super(topLine, shell);
		this.cadexModel = model;
	}

	@Override
	public void processLine(TCComponentBOMLine line) throws Exception {
		validateLine(line, this);
		addToMap(line.getItemRevision(), line);
	}

	private String validateLine(TCComponentBOMLine line, ValidationTraversal<T> validationTraversal) throws Exception
	{
			String retString = "";
			
			ArrayList<CadexTCDatasetComponent> list = DatasetViewContentProvider.getAllRelatedDatasets( line );
			
			
			
			retString = line.getStringProperty( "CE4_attachType" );
			String tObj = line.getStringProperty( "bl_item_object_type" );
			TCComponentBOMLine parent = line.parent();
			if( parent != null && !parent.getStringProperty( "bl_item_object_type" ).equals( "CE4_ExPkg" )){
				line.setStringProperty( "CE4_attachType" , parent.getStringProperty( "CE4_attachType" ) );
			}
			
			
			String export = line.getStringProperty( "CE4_exportNode" );
			String foundProblem = "";
			if( !tObj.equals( "CE4_ExPkg" ) )
			{
				boolean check = CadexViewLabelProvider.isCheckedOut(line);
				if((retString==null||retString.length()==0) || (check && retString.equals( "Target" ) && export != null && export.equals("true")))
				{
					foundProblem = "The selected object is selected as Target, but it is already checked out in teamcenter.";
					this.cadexModel.updateProblemLines(line, foundProblem);
				}
				
				if(!this.cadexModel.isExportable() && line.getStringProperty( "CE4_exportNode" ).equalsIgnoreCase( "true" ))
					this.cadexModel.setExportable(true);
			}
			
			String ipClass = line.getStringProperty( "bl_item_ip_classification" );
			String expNative = line.getStringProperty( "CE4_exportNative" );
			boolean isNativeExp = expNative!=null && expNative.equalsIgnoreCase( "true" ) ? true : false; 
			if( line.getStringProperty( "bl_config_string" ).equalsIgnoreCase( "No Configured Revision" ) )
			{
				this.cadexModel.updateProblemLines(line, "This line does not have a configured revision. Please check the revision rule. Skipped Dataset validation.");
			}
			else if(ipClass!=null && (ipClass.equals( "PIIX" ) || ipClass.equals( "PII" ) ) && isNativeExp )
			{
				this.cadexModel.updateProblemLines(line, "This item is classfied as " + ipClass + ". Native CAD files cannot be sent to Supplier.");
			}
			else if(ipClass!=null && (ipClass.equals( "PI" ) ) && CadexViewLabelProvider.isCheckedOut(line) )
			{
				this.cadexModel.updateProblemLines(line, "This item is classfied as " + ipClass + ". It cannot be checked out.");
			}
			else if( validationTraversal.checkIfInMap(line.getItemRevision()) )
			{
				TCComponentBOMLine attachLine = validationTraversal.getLineFromMap(line.getItemRevision());
				
				if( attachLine.getStringProperty( "CE4_attachType" ).equals( retString ) )
				{
					alignOccurrences(line, attachLine);
				}
				else
				{
					this.cadexModel.updateProblemLines(line, "This line is attached both as Target and Reference. Please check attachments under the package. Skipped Dataset validation.");
				}
			}
			else
			{
				for(CadexTCDatasetComponent dset : list)
				{
					if(dset.isFoundProblem())
						this.cadexModel.updateProblemDsetLines(line, dset);
					else
						this.cadexModel.removeProblemLine(line,dset);
				}
			}
			
			return retString;
	}
	
	private void alignOccurrences(TCComponentBOMLine occ1, TCComponentBOMLine occ2) throws TCException
	{
		String[] alignAttrs = this.reg.getStringArray( "alignAttributes" );
		for (int i = 0; i < alignAttrs.length; i++) {
			occ2.setStringProperty( alignAttrs[i] , occ1.getStringProperty( alignAttrs[i] ));
		}
		occ1.window().save();
	}
}

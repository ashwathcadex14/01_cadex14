/*=================================================================================================                    
#                Copyright (c) 2014 Some Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CadexTCDatasetComponent.java          
#      Module          :           com.teamcenter.rac.tccadex.views.datasetsview          
#      Description     :           Component representing the model for the Dataset View in Supplier CAD Exchange Application          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.views.datasetsview;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentBOMLine;
import com.teamcenter.rac.kernel.TCComponentCfgAttachmentLine;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentPseudoFolder;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.tccadex.utils.CECommonUtils;

public class CadexTCDatasetComponent {
	
	TCComponentCfgAttachmentLine dsetLine ;
	TCComponentDataset dset ;
	TCComponentBOMLine bomLine;
	String objString;
	boolean isExported = false;
	boolean isCheckOut = false;
	boolean isExportable = false;
	boolean isCheckOutable = false;
	String objectType ;
	boolean foundProblem = false;
	String problemFound = "";
	boolean checkedOut = false;
	String uid ;
	
	public CadexTCDatasetComponent(TCComponentCfgAttachmentLine line) throws TCException {
		this.dsetLine = line;
		try {
			setBomLine();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setDataset();
		setCadexProps();
		checkForProblems();
	}
	
	private void setDataset() throws TCException
	{
		this.dset = (TCComponentDataset) this.dsetLine.getReferenceProperty( "me_cl_source" );
		this.objString = this.dset.getStringProperty( "object_string" );
		this.objectType = dset.getStringProperty("object_type");
		this.checkedOut = this.dset.getStringProperty( "checked_out" ).equals( "Y" ) ? true : false;
		this.uid = this.dset.getUid();
	}
	
	public void setExportedProp(boolean bool) throws TCException
	{
		this.isExported = bool;
		setUnsetValue(this.isExported,"CE4_exportedDsets");
		if(!this.isExported && this.isCheckOut)
			setCheckedOutProp(this.isExported);
		
		CECommonUtils.setExpCheckOutDset(this.bomLine, this.bomLine.window(), bool, bool ? this.isCheckOut : bool, this.dset);
		
		bomLine.window().save();
		checkForProblems();
		
		
	}
	
	public void setCheckedOutProp(boolean bool) throws TCException
	{
		this.isCheckOut = bool;
		setUnsetValue(this.isCheckOut,"CE4_checkedOutDsets");
		if( this.isCheckOut && !this.isExported  )
			setExportedProp( bool );
		
		CECommonUtils.setExpCheckOutDset(this.bomLine, this.bomLine.window(), bool ? (isExported ? isExported : bool) : isExported, bool , this.dset);
		
		bomLine.window().save();
		checkForProblems();
	}
	
	private void setCadexProps() throws TCException
	{
		if( checkAvail(this.bomLine.getStringProperty( "CE4_checkedOutDsets" ), "\\|", dset.getUid() ) )
		{
			this.isCheckOut = true;
		}
		
		if( checkAvail(this.bomLine.getStringProperty( "CE4_exportedDsets" ), "\\|", dset.getUid() ) )
		{
			this.isExported = true;
		}
		
		this.isExportable = this.bomLine.getStringProperty( "CE4_exportNode" ).equals( "true" ) ? true : false;
		this.isCheckOutable = this.bomLine.getStringProperty( "CE4_attachType" ).equals( "Target" ) ? true : false;
	}
	
	private boolean checkAvail(String uidS, String delim, String findThis)
	{
		if(uidS.contains( "|" ))
			return Arrays.asList( uidS.split( delim ) ).contains( findThis );
		else
			return uidS.equals( findThis );
	}
	
	private void setUnsetValue(boolean bool, String attr) throws TCException
	{
		String attrValue = this.bomLine.getStringProperty( attr );
		String dsetUid = dset.getUid();
		
		if( attrValue != null && attrValue.length() > 0)
		{
			boolean uidAvail = checkAvail(attrValue, "\\|", dsetUid);
			
			if(bool)
			{
				if(!uidAvail)
				{
					attrValue += "|" + dsetUid;
				}
			}
			else
			{
				if(uidAvail)
				{
					attrValue = removeFromAttr(attrValue, "|", dsetUid);
				}
			}	
		}
		else
			attrValue = dsetUid;
		
		this.bomLine.setStringProperty( attr , attrValue);
	}
	
	private String removeFromAttr(String attr, String delim, String remThis)
	{
		String[] splitted ;
		if( attr.contains("|") )
			splitted = attr.split("\\|");
		else
		{	
			splitted = new String[1];
			splitted[0] = attr;
		}
		
		List<String> remList = new ArrayList<String>();
		
		for (int i = 0; i < splitted.length; i++) {
			remList.add( splitted[i].equals( remThis ) ? "" : splitted[i] );
		}
		
		return remList.toString().replace("[","").replace("]", "").replace( "," , "|");
		
	}

	public TCComponentDataset getDset() {
		return dset;
	}
	
	public String getStringProperty(String propName)
	{
		if(propName.equals( "CE4_exportNode" ))
			return String.valueOf(isExported);
		else if(propName.equals( "object_type" ))
			return this.objectType;
		else
			return String.valueOf(isCheckOut);
	}
	
	public void refresh() throws TCException
	{
		this.bomLine.refresh();
	}
	
	private void checkForProblems()
	{
		if( isExported && checkedOut )
		{
			problemFound = "The selected object is exported and also checked out.";
			foundProblem = true;
		}
		else
		{
			problemFound = "";
			foundProblem = false;
		}
	}

	public String getProblemFound() {
		return problemFound;
	}

	public boolean isFoundProblem() {
		return foundProblem;
	}

	public String getObjString() {
		return objString;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return obj instanceof CadexTCDatasetComponent ? this.uid.equals( ((CadexTCDatasetComponent)obj).uid ) : false;
	}

	public String getUid() {
		return uid;
	}
	
	private void setBomLine() throws TCException
	{
		TCComponent parent = this.dsetLine.getReferenceProperty( "me_cl_parent" );
		TCComponent parentComp = parent.getReferenceProperty( "me_cl_source" );
		if(parentComp instanceof TCComponentBOMLine)
			this.bomLine = (TCComponentBOMLine)parentComp;
		else if (parentComp instanceof TCComponentPseudoFolder) {
			TCComponent parentLine = parent.getReferenceProperty( "me_cl_parent" );
			TCComponent parentLineComp = parentLine.getReferenceProperty( "me_cl_source" );
			
			if(parentLineComp instanceof TCComponentBOMLine)
				this.bomLine = (TCComponentBOMLine) parentLineComp;
		}
	}
}

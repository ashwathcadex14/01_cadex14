/*=================================================================================================                    
#                Copyright (c) 2014 Some Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           DatasetViewContentProvider.java          
#      Module          :           com.teamcenter.rac.tccadex.views.datasetsview          
#      Description     :           Content Provider for Dataset Tree viewer          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.views.datasetsview;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.widgets.Shell;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentBOMLine;
import com.teamcenter.rac.kernel.TCComponentCfgAbsOccAttachmentLine;
import com.teamcenter.rac.kernel.TCComponentCfgAttachmentLine;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentPseudoFolder;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.tccadex.utils.CadexUtils;
import com.teamcenter.rac.util.Registry;

public class DatasetViewContentProvider implements IStructuredContentProvider {
	
	private Shell currentShell;
	private TCComponentBOMLine selectedBomLine;
	private static Registry mReg = Registry.getRegistry( DatasetViewContentProvider.class );
	private static List<String> skipDatasets = Arrays.asList( mReg.getStringArray( "skipDatasets" ) );

	public DatasetViewContentProvider(Shell parent) {
		this.currentShell = parent;
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void inputChanged(Viewer arg0, Object arg1, Object arg2) {
		selectedBomLine = (TCComponentBOMLine) arg2;
	}

	@Override
	public Object[] getElements(Object arg0) {		
		
		try 
		{
			return getAllRelatedDatasets(selectedBomLine).toArray();
		} catch (Exception e) {
			CadexUtils.openError(currentShell, "Fetch Error", "Error occured while getting datasets for the selected line.\n" , e);
			return new ArrayList<TCComponent>().toArray();
		}
	}
	
	public static ArrayList<CadexTCDatasetComponent> getAllRelatedDatasets(TCComponentBOMLine line) throws Exception
	{
		ArrayList<CadexTCDatasetComponent> listComponents = new ArrayList<CadexTCDatasetComponent>();
		
		if(line!=null)
		{
			try 
			{
				TCComponent[] attachments = line.getReferenceListProperty( "bl_dataset_attachments" );
				if(attachments!=null && attachments.length > 0)
				{
					for (int i = 0; i < attachments.length; i++) 
					{
						TCComponent comp = attachments[i].getReferenceProperty( "me_cl_source" );					
						
						if( comp instanceof TCComponentPseudoFolder )
						{
							TCComponent[] attachmentChilds = attachments[i].getReferenceListProperty( "me_cl_child_lines" );
							for (int j = 0; j < attachmentChilds.length; j++) 
							{
								getDatasetAttachments(attachmentChilds[j], listComponents);
							}
						}
						else
						{
							getDatasetAttachments(attachments[i], listComponents);
						}
					}
				}

				
			} catch (TCException e) {
				throw new Exception("Exception occured while fetching the datasets of the selected line.",e);
			}
			
		}
		
		return listComponents;
	}
	
	private static void getDatasetAttachments( TCComponent attachment, ArrayList<CadexTCDatasetComponent> list ) throws TCException
	{
			TCComponent comp = attachment.getReferenceProperty( "me_cl_source" );
			String objType = comp.getStringProperty("object_type");
			System.out.println("ce4_cl_parent_export - " + attachment.getStringProperty( "ce4_cl_parent_export" ));
			if( !(attachment instanceof TCComponentCfgAbsOccAttachmentLine) && comp instanceof TCComponentDataset && !skipDatasets.contains( objType ))
			{
				list.add( new CadexTCDatasetComponent( (TCComponentCfgAttachmentLine) attachment ) );
			}		
	}
	
	
}

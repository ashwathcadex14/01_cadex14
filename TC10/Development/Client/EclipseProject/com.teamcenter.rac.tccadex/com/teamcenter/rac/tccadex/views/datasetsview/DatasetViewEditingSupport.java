/*=================================================================================================                    
#                Copyright (c) 2014 Some Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           DatasetViewEditingSupport.java          
#      Module          :           com.teamcenter.rac.tccadex.views.datasetsview          
#      Description     :           Editing Support for Dataset Tree Viewer columns          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.views.datasetsview;

import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.EditingSupport;

import com.teamcenter.rac.tccadex.utils.CadexUtils;
import com.teamcenter.rac.tccadex.views.DatasetsView;
import com.teamcenter.rac.util.Registry;

public class DatasetViewEditingSupport extends EditingSupport {

	int columnIndex ;
	Registry reg = Registry.getRegistry(this);
	List<String> editableColumns = Arrays.asList( reg.getStringArray( "datasetViewEditableColumns" ) );
	DatasetsView mainView ;
	String propName ;
	
	public DatasetViewEditingSupport(ColumnViewer viewer, int index, DatasetsView dsetView) {
		super(viewer);
		this.columnIndex = index;
		this.mainView = dsetView;
		propName = this.mainView.getColumn( this.columnIndex );
	}

	@Override
	protected boolean canEdit(Object arg0) {
		try {
			return checkEdit(arg0);
		} catch (Exception e) {
			CadexUtils.openError(this.mainView.getSite().getShell(), "Cannot Edit", e.getMessage(),e);
		}
		return false;
	}

	@Override
	protected CellEditor getCellEditor(Object arg0) {
		// TODO Auto-generated method stub
		return this.mainView.getCheckboxCellEditor();
	}

	@Override
	protected Object getValue(Object arg0) {
		String propValue = ((CadexTCDatasetComponent)arg0).getStringProperty( propName );
		return propValue == null || propValue.length() == 0 ? false : Boolean.valueOf(propValue);
	}

	@Override
	protected void setValue(Object arg0, Object arg1) 
	{
		CadexTCDatasetComponent thisLine = (CadexTCDatasetComponent) arg0;
		
		try 
		{
			if(this.propName.equals( "CE4_exportNode" ))
				thisLine.setExportedProp( (boolean) arg1 );
			else
				thisLine.setCheckedOutProp( (boolean) arg1 );
			
			thisLine.refresh();
			this.mainView.refreshTreeNode( arg0 );
			
			
		} catch ( Exception e) {
			CadexUtils.openError(this.mainView.getSite().getShell(), "Set Error", "Unable to set property " + propName, e);
		}
		
		
		
	}
	
	private boolean checkEdit(Object arg0) throws Exception
	{
		if( editableColumns.contains( String.valueOf( this.columnIndex ) ) )		
		{	
			CadexTCDatasetComponent comp = (CadexTCDatasetComponent)arg0;
			switch(this.columnIndex)
			{
				case 1:
					return comp.isExportable;
				case 2: 
					return comp.isExportable && comp.isCheckOutable;
				default:
					return false;
			}
		}
		else
			return false;
	}
}

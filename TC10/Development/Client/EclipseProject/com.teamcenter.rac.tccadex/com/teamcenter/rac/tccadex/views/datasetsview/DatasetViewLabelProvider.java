/*=================================================================================================                    
#                Copyright (c) 2014 Some Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           DatasetViewLabelProvider.java          
#      Module          :           com.teamcenter.rac.tccadex.views.datasetsview          
#      Description     :           Label Provider for Dataset View Tree Viewer          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.views.datasetsview;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableColorProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.wb.swt.ResourceManager;

import com.teamcenter.rac.tccadex.utils.CEIconProvider;
import com.teamcenter.rac.tccadex.utils.CadexUtils;
import com.teamcenter.rac.tccadex.views.DatasetsView;

public class DatasetViewLabelProvider implements ITableLabelProvider,ITableColorProvider {
	private DatasetsView mainView;
	private Shell currentShell;
	private static Image CHECKED = ResourceManager.getPluginImage("com.teamcenter.rac.tccadex", "com/teamcenter/rac/tccadex/images/LargeCheck.png") ;
	private static Image CHECKED_OUT = ResourceManager.getPluginImage("com.teamcenter.rac.tccadex", "com/teamcenter/rac/tccadex/images/Checkout.png") ;

	  public DatasetViewLabelProvider(DatasetsView parent) {
		  this.mainView = parent;
		  this.currentShell=parent.getSite().getShell();
	  }

	@Override
	public void addListener(ILabelProviderListener arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isLabelProperty(Object arg0, String arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Image getColumnImage(Object arg0, int arg1) {
		  
		if( arg1 == 0 )
		{
			String tObj = ((CadexTCDatasetComponent)arg0).getStringProperty( "object_type" );
			return CEIconProvider.getTCTypeIcon( tObj ) ;
		}
		else if( arg1 == 1 || arg1 == 2 )
		{
			return getColumnText(arg0, arg1).equals("true") ? CHECKED : null;
		}
		else if(arg1==3)
		{
			return getColumnText(arg0, arg1).equals("Y") ? CHECKED_OUT : null;
		}
		else
			return null;
	}

	@Override
	public String getColumnText(Object arg0, int arg1) 
	{
		CadexTCDatasetComponent comp = (CadexTCDatasetComponent) arg0;
		try 
		{
			if( arg1 == 1 || arg1 == 2 )
			{
				return comp.getStringProperty( this.mainView.getColumn(arg1) );
			}
			else
				return comp.getDset().getPropertyDisplayableValue( this.mainView.getColumn(arg1) );
		} 
		catch (Exception e) 
		{
			CadexUtils.openError(currentShell, "Load Error" ,"Unable to get properties for dataset.",e );
		}
		return "Error";
	}

	@Override
	public Color getBackground(Object arg0, int arg1) {

		if( ((CadexTCDatasetComponent)arg0).foundProblem )
			return new Color(Display.getCurrent(),253, 97, 97);
		else
			return null;
	}

	@Override
	public Color getForeground(Object arg0, int arg1) {
		// TODO Auto-generated method stub
		return null;
	}
	
	  
	}

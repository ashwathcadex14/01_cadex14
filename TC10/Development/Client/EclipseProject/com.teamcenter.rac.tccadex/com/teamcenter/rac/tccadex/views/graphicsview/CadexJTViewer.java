/*=================================================================================================                    
#                Copyright (c) 2014 Some Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CadexJTViewer.java          
#      Module          :           com.teamcenter.rac.tccadex.views.graphicsview          
#      Description     :           Class for evaluating OOTB JT viewer behaviour          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.views.graphicsview;

import com.teamcenter.rac.common.tcviewer.DirectModelViewer;

public class CadexJTViewer extends DirectModelViewer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1062656921629813564L;

	public CadexJTViewer() {
		super();
	}
	
}

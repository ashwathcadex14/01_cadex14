/*=================================================================================================                    
#                Copyright (c) 2014 Some Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CadexPSEAppPanel.java          
#      Module          :           com.teamcenter.rac.tccadex.views.graphicsview          
#      Description     :           Class for evaluating OOTB PSE Panel behaviour          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.views.graphicsview;

import java.beans.PropertyChangeEvent;

import javax.swing.JPanel;

import com.teamcenter.rac.aif.AbstractAIFOperation;
import com.teamcenter.rac.cm.CMListSupercedures;
import com.teamcenter.rac.common.genericselection.SelectComponentEvent;
import com.teamcenter.rac.ecmanagement.ECMListSupercedures;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentBOMLine;
import com.teamcenter.rac.kernel.TCComponentBOMWindow;
import com.teamcenter.rac.kernel.TCComponentRevisionRule;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.officeliveservices.ExcelExportOption;
import com.teamcenter.rac.pse.AbstractPSEApplication;
import com.teamcenter.rac.pse.AbstractPSEApplicationPanel;
import com.teamcenter.rac.pse.common.BOMPanel;
import com.teamcenter.rac.pse.common.BOMTreeTable;
import com.teamcenter.rac.pse.dialogs.BOMCompareReportTable;
import com.teamcenter.rac.pse.search.PSESearchResultRetriever;
import com.teamcenter.rac.psebase.AbstractBOMLineViewerApplication;
import com.teamcenter.rac.psebase.common.AbstractBOMTreeViewPanel;

public class CadexPSEAppPanel extends AbstractPSEApplicationPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -495229877760079185L;

	@Override
	public void processSelectComponent(SelectComponentEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void closeSignaled() {
		// TODO Auto-generated method stub

	}

	@Override
	public void propertyChange(PropertyChangeEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void searchResultRetriever(PSESearchResultRetriever arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateQSearchResultsToTable(Object[] arg0, int arg1,
			TCComponentBOMWindow arg2, TCComponentBOMLine[] arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean canViewBOMLine() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void createOpenWindow(String arg0, TCComponentRevisionRule arg1,
			String arg2, String[] arg3, String[] arg4) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createWorkingPanel() {
		// TODO Auto-generated method stub

	}

	@Override
	public AbstractBOMTreeViewPanel getAbstractViewer() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BOMTreeTable getActiveBOMTreeTable() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JPanel getActivePanel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractBOMLineViewerApplication getBOMLineViewerApplication() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getIndexActiveBOMTreeTable() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setPortalViewerStatus(boolean arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public TCComponent[] getComponentsToExport(ExcelExportOption arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] getDisplayedPropertyNames() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractAIFOperation activeBOMSupercedureOp(BOMPanel arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void closeBOMPanel(BOMPanel arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public BOMPanel createBasedPanel() throws TCException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteSuperceduresPanel() {
		// TODO Auto-generated method stub

	}

	@Override
	public void fireApplicationOpen(TCComponent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public BOMPanel[] getAllBOMPanels() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BOMPanel[] getAllBOMPanelsReally() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TCComponentBOMWindow[] getAllBOMWindows() throws TCException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractPSEApplication getApplication() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CMListSupercedures getCMListSuperceduresPanel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BOMPanel getCurrentBOMPanel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ECMListSupercedures getECMListSuperceduresPanel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BOMPanel getRightBOMPanel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean getSearchResultPanelShown() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void newWindow() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean open(TCComponent arg0, TCComponentRevisionRule arg1,
			TCComponent arg2, boolean arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void openRemoteBOMLine(TCComponent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void refreshBOM() {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeCurrentCompareReport() {
		// TODO Auto-generated method stub

	}

	@Override
	public void selectBOM(BOMPanel arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void showCompareReport(BOMPanel[] arg0, BOMCompareReportTable arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void showSearchResultDialog(boolean arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void unsplitPanels() {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateBOMPanels() {
		// TODO Auto-generated method stub

	}

	@Override
	public void zoomBOMPanel(BOMPanel arg0) {
		// TODO Auto-generated method stub

	}

}

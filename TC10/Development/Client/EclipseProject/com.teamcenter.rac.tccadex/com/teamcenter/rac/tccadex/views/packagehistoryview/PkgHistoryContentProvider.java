/*=================================================================================================                    
#                Copyright (c) 2014 Some Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           PkgHistoryContentProvider.java          
#      Module          :           com.teamcenter.rac.tccadex.views.packagehistoryview          
#      Description     :           Content Provider for Package History viewer          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.views.packagehistoryview;

import java.util.ArrayList;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.widgets.Shell;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.tccadex.utils.CadexUtils;

public class PkgHistoryContentProvider implements IStructuredContentProvider {

	private Shell currentShell;
	private TCComponentItemRevision selectedPkg;
	
	public PkgHistoryContentProvider(Shell parent) {
		this.currentShell = parent;
	}
	
	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void inputChanged(Viewer arg0, Object arg1, Object arg2) {
		selectedPkg=(TCComponentItemRevision) arg2;
		
	}

	@Override
	public Object[] getElements(Object arg0) {
		try {
			// TODO Auto-generated method stub
			return selectedPkg.getReferenceListProperty( "ce4_pkgHistory" );
		} catch (TCException e) {
			CadexUtils.openError(currentShell, "Fetch Error", "Error occured while getting history for the package\n" , e);
			return new ArrayList<TCComponent>().toArray();
		}
	}
}

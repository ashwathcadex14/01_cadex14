/*=================================================================================================                    
#                Copyright (c) 2014 Some Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           PkgHistoryLabelProvider.java          
#      Module          :           com.teamcenter.rac.tccadex.views.packagehistoryview          
#      Description     :           Label Provider for Package History View Tree Viewer          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
package com.teamcenter.rac.tccadex.views.packagehistoryview;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Shell;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.tccadex.utils.CadexUtils;
import com.teamcenter.rac.tccadex.views.PackageHistoryView;

public class PkgHistoryLabelProvider implements ITableLabelProvider {

	private PackageHistoryView mainView;
	private Shell currentShell;
	
	public PkgHistoryLabelProvider(PackageHistoryView parent) {
		  this.mainView = parent;
		  this.currentShell=parent.getSite().getShell();
	  }
	
	@Override
	public void addListener(ILabelProviderListener arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isLabelProperty(Object arg0, String arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public Image getColumnImage(Object arg0, int arg1) {
			return null;
	}

	@Override
	public String getColumnText(Object arg0, int arg1) 
	{
		try 
		{
				return ((TCComponent)arg0).getPropertyDisplayableValue( this.mainView.getColumn(arg1) );
		} 
		catch (Exception e) 
		{
			CadexUtils.openError(currentShell, "Load Error" ,"Unable to get properties for package status.", e);
		}
		return "Error";
	}

}

package com.teamcenter.tccadex.compare.application;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.wb.swt.SWTResourceManager;

import com.teamcenter.tccadex.compare.ui.CELoadingComposite;

public class StructureCompare {

	protected Shell shlCompareStructure;
	CELoadingComposite loadMidComposite;
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			StructureCompare window = new StructureCompare();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shlCompareStructure.open();
		shlCompareStructure.layout();
		while (!shlCompareStructure.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlCompareStructure = new Shell();
		shlCompareStructure.setImage(SWTResourceManager.getImage(StructureCompare.class, "/icons/cadexApplication_32.png"));
		shlCompareStructure.setSize(1153, 746);
		shlCompareStructure.setText("Compare Structure");
		shlCompareStructure.setLayout(new GridLayout(1, false));
		
		loadMidComposite = new CELoadingComposite(shlCompareStructure, SWT.NONE);
		GridData gd_loadMidComposite = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_loadMidComposite.heightHint = 172;
		loadMidComposite.setLayoutData(gd_loadMidComposite);
		
		
		
	}

}

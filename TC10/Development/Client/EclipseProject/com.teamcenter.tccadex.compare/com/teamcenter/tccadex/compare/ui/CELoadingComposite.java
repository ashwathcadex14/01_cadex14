/*=================================================================================================                    
#                Copyright (c) 2014 Some Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CELoadingComposite.java          
#      Module          :           com.teamcenter.rac.tccadex.ui          
#      Description     :           Composite to implement loading screen mechanism during CADEX View operations          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
/**=================================================================================================
#                Copyright (c) 2013 Intelizign Engineering Services                    
#                  Unpublished - All Rights Reserved                    
#  ================================================================================================= 
#      Filename        :           MTLoadingComposite.java          
#      Module          :           com.siemens.is.mt.teamcenter.ee.views.searchandadd          
#      Description     :           <DESCRIPTION>          
#      Project         :           SVAI External Engineering          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                         Name           Description of Change
#  13-Mar-2014				       Ashwath			Initial Creation
#  $HISTORY$                  
#  =================================================================================================*/
package com.teamcenter.tccadex.compare.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.SWTResourceManager;

/**
 * @author tvcpt3
 *
 */
public class CELoadingComposite extends Composite
{
	Thread newthread ;
	Thread gearThread ;
	StackLayout loadStackLayout = new StackLayout();
	Composite loadingComposite;
	public Composite otherComposite;
	Canvas canvas;
	ImageLoader imageLoader = new ImageLoader();
    ImageData[] imageDatas = imageLoader.load(getClass().getResourceAsStream( "/com/teamcenter/tccadex/compare/images/loadingbar.gif" ) );
    ImageData[] imageDatasGear = imageLoader.load(getClass().getResourceAsStream( "/com/teamcenter/tccadex/compare/images/194_Moving_gears.gif" ) );
    GC gc;
    GC gcGear;
    Label lblRefreshingInformation;
    private Composite infoComposite;
    Label lblinfoLabel;
    private Canvas canvas_1;
    private boolean stopGear = true;
	/**
	 * @param parent
	 * @param style
	 */
	public CELoadingComposite(final Composite parent, int style) 
	{
		super(parent, style);
		setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		setLayout(loadStackLayout);
		setBackgroundMode(SWT.INHERIT_DEFAULT);
		
		loadingComposite = new Composite(this, SWT.TRANSPARENT);
		loadingComposite.setBackgroundMode( SWT.INHERIT_DEFAULT);
		loadingComposite.setLayout(new GridLayout(1, false));
		
	    
	 
	    final Image image = new Image(parent.getDisplay(), imageDatas[0].width, imageDatas[0].height);
	    gc = new GC(image);
		
		canvas = new Canvas(loadingComposite, SWT.NONE);
		canvas.setBackgroundMode( SWT.INHERIT_DEFAULT );
		canvas.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		canvas.setLayout(new GridLayout(1, false));
		GridData gd_canvas = new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1);
		gd_canvas.heightHint = 126;
		gd_canvas.widthHint = 172;
		canvas.setLayoutData(gd_canvas);
		
		lblRefreshingInformation = new Label(loadingComposite, SWT.NONE);
		lblRefreshingInformation.setAlignment(SWT.CENTER);
		GridData gd_lblRefreshingInformation = new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1);
		gd_lblRefreshingInformation.widthHint = 439;
		lblRefreshingInformation.setLayoutData(gd_lblRefreshingInformation);
        canvas.addPaintListener(new PaintListener() {
	         public void paintControl(PaintEvent e) {
	           e.gc.drawImage(image, 0, 0);
	         }
	    });
        
        otherComposite = new Composite(this, SWT.NONE);
        otherComposite.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		GridLayout gl_otherComposite = new GridLayout(1, false);
		gl_otherComposite.verticalSpacing = 1;
		gl_otherComposite.marginWidth = 1;
		gl_otherComposite.marginHeight = 1;
		otherComposite.setLayout(gl_otherComposite);
		
		
		
		infoComposite = new Composite(this, SWT.NONE);
		infoComposite.setLayout(new GridLayout(1, true));
		
		canvas_1 = new Canvas(infoComposite, SWT.NONE);
		canvas_1.setLayout(null);
		GridData gd_canvas_1 = new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1);
		gd_canvas_1.widthHint = 303;
		gd_canvas_1.heightHint = 236;
		canvas_1.setLayoutData(gd_canvas_1);
		
		lblinfoLabel = new Label(infoComposite, SWT.NONE);
		lblinfoLabel.setAlignment(SWT.CENTER);
		GridData gd_lblinfoLabel = new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1);
		gd_lblinfoLabel.heightHint = 107;
		gd_lblinfoLabel.widthHint = 498;
		lblinfoLabel.setLayoutData(gd_lblinfoLabel);
		lblinfoLabel.setBounds(0, 0, 70, 20);
		
	    final Image imageGear = new Image(parent.getDisplay(), imageDatasGear[0].width, imageDatasGear[0].height);
	    gcGear = new GC(imageGear);
	    
        canvas_1.addPaintListener(new PaintListener() {
	         public void paintControl(PaintEvent e) {
	           e.gc.drawImage(imageGear, 0, 0);
	         }
	    });
		
		loadStackLayout.topControl = otherComposite;
		
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.swt.widgets.Composite#checkSubclass()
	 */
	@Override
	protected void checkSubclass() {
		// This will bypass sub class checking error
	}
	
	public void swapComposites(final String message)
	{
		this.getDisplay().syncExec( new Runnable() {
			
			@Override
			public void run() {
				lblRefreshingInformation.setText(message);
				lblRefreshingInformation.redraw();
				if( loadStackLayout.topControl.equals(loadingComposite) )
				{
					loadStackLayout.topControl = otherComposite;
				}
				else
					loadStackLayout.topControl = loadingComposite;
				
				CELoadingComposite.this.layout();
			}
		});
		
		
		
		
	}
	
	public void finishLoading()
	{
		swapComposites("");
		if(!newthread.isInterrupted())
			newthread.interrupt();
		newthread = null;
		lblRefreshingInformation.setText("");
	}
	
	private void createThread()
	{
	    newthread = new Thread() {
		      int frameIndex = 0;
		      public void run() {
				        while (!isInterrupted() && !CELoadingComposite.this.isDisposed()) {
					          frameIndex %= imageDatas.length;
					 
					          final ImageData frameData = imageDatas[frameIndex];
					          CELoadingComposite.this.getDisplay().syncExec(new Runnable() {
					            public void run() {
					              Image frame =
					                new Image(Display.getDefault(), frameData);
					              gc.drawImage(frame, frameData.x, frameData.y);
					              frame.dispose();
					              if(!canvas.isDisposed())
					            	  canvas.redraw();
					            }
					          });
					          try {
					              int ms = frameData.delayTime * 50;
					              if (ms < 20)
					                ms += 10;
					              if (ms < 30)
					                ms += 10;
					              Thread.sleep(ms);
					          } catch (InterruptedException e) {
					            return;
					          }
					          frameIndex += 1;
					        }
		      }
		    };
	}

	public void startLoading(String message) {
		swapComposites(message);
		createThread();
		newthread.start();
	}
	
	private void startGear() {
		runGearThread();
		stopGear = false;
		gearThread.start();
	}
	
	private void stopGear()
	{		
		stopGear = true;
		if( gearThread != null && !gearThread.isInterrupted())
			gearThread.interrupt();
		gearThread = null;
	}
	
	public void showInfo(final String message, final boolean show , final boolean showRefresh)
	{
		
		this.getDisplay().asyncExec( new Runnable() {
			
			@Override
			public void run() {
				
				if(show)
				{
					lblinfoLabel.setText(message);
					lblinfoLabel.redraw();
					loadStackLayout.topControl = infoComposite;
					
					if( !showRefresh )
					{	
						startGear();
					}
					else
					{
						stopGear();
					}
				}
				else
				{
					loadStackLayout.topControl = otherComposite; 
					stopGear();
				}
				
				CELoadingComposite.this.layout();
			}
		});
	}
	
	private void runGearThread()
	{
		gearThread = new Thread() {
		      int frameIndex = 0;
		      public void run() {
				        while ( !stopGear && !CELoadingComposite.this.isDisposed()) 
				        {
					          frameIndex %= imageDatasGear.length;
					 
					          final ImageData frameData = imageDatasGear[frameIndex];
					          CELoadingComposite.this.getDisplay().syncExec(new Runnable() {
					            public void run() {
					              Image frame =
					                new Image(Display.getDefault(), frameData);
					              gcGear.drawImage(frame, frameData.x, frameData.y);
					              frame.dispose();
					              if(!canvas_1.isDisposed())
					            	  canvas_1.redraw();
					            }
					          });
					          try {
					              int ms = frameData.delayTime * 100;
					              if (ms < 20)
					                ms += 10;
					              if (ms < 30)
					                ms += 10;
					              Thread.sleep(ms);
					          } catch (InterruptedException e) {
					        	  
					            System.out.println( "thread interrupted" );
					          }
					          finally
					          {
					        	  frameIndex += 1;
					          }
					        }
		      }
		    };
	}
}

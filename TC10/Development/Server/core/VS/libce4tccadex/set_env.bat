rem ##################################################################################
rem # set env
rem # do not put  " " around LIB and INCLUDE - PATH, cause this is made in the project-file
rem #
rem # please create YOUR OWN SECTION for host specific settings - e.g. :CFR
rem ##################################################################################

GOTO SETENV
:SETENV
echo "Please set up your enviroment variables!"

:CHA
SET CADEX_DEV=C:\gitrepo\INFOEX_WORKSPACE\TC10
set BMIDE_FOLDER=%CADEX_DEV%\DataModel\TCCADEX
set SOA_DEFAULT_LIBS=%BMIDE_FOLDER%\output\types\lib;%BMIDE_FOLDER%\output\client\lib
set SOURCE_REFERENCE=%CADEX_DEV%\Development\Server\core\build\libce4tccadex_v1
set SOURCE_SRC=%SOURCE_REFERENCE%\src\server
set SOURCE_GENSRC=%SOURCE_REFERENCE%\output\server\gensrc
set SERVER_INCLUDE=%SOURCE_SRC%;%SOURCE_GENSRC%
set SOA_CLIENT_PATH=C:\apps\Teamcenter\TC10\soa_client
set TC_ROOT=C:\apps\Teamcenter\TC10\tcroot
set DEV_TOOL=C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\devenv.exe
set TC_DATA=C:\apps\Teamcenter\TC10\tcdata
call %TC_DATA%/tc_profilevars.bat
set path=%SOA_CLIENT_PATH%\libs\wntx64;%path%
GOTO CONTINUE

GOTO CONTINUE

:CONTINUE


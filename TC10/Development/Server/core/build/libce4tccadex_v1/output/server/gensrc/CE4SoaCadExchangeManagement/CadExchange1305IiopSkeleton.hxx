/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CadExchange1305IiopSkeleton.hxx          
#      Module          :           libce4tccadex_v1.output.server.gensrc.CE4SoaCadExchangeManagement          
#      Description     :           Auto Generated SOA File          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
/* 


   Auto-generated source from service interface.
                 DO NOT EDIT


*/

//#ifndef TEAMCENTER_SERVICES_CadExchangeManagement__2013_05_CadExchange_HXX 
//#define TEAMCENTER_SERVICES_CadExchangeManagement__2013_05_CadExchange_HXX

#include <unidefs.h>
#if defined(SUN)
#    include <unistd.h>
#endif

#include <string>
#include <iostream>
#include <sstream>

#include <tcgateway/tcsvcmgr.hxx>

#include <teamcenter/soa/server/ServiceData.hxx>
#include <teamcenter/soa/server/ServiceException.hxx>
#include <teamcenter/soa/server/PartialErrors.hxx>
#include <teamcenter/soa/server/Preferences.hxx>
#include <teamcenter/soa/server/ServicePolicy.hxx> 
#include <teamcenter/soa/internal/server/IiopSkeleton.hxx>
#include <teamcenter/soa/internal/server/Utils.hxx>
#include <teamcenter/soa/internal/server/ServiceManager.hxx>
#include <teamcenter/soa/common/xml/SaxToNodeParser.hxx>
#include <teamcenter/soa/common/xml/XmlUtils.hxx>
#include <teamcenter/soa/common/exceptions/ExceptionMapper.hxx>
#include <teamcenter/soa/common/exceptions/DomException.hxx>
#include <teamcenter/schemas/soa/_2006_03/exceptions/ServiceException.hxx>

#include <cadexchange1305impl.hxx>
#include <ce4/schemas/cadexchangemanagement/_2013_05/cadexchange/CreateExportPackageInput.hxx>
#include <teamcenter/schemas/soa/_2006_03/base/ServiceData.hxx>
#include <ce4/schemas/cadexchangemanagement/_2013_05/cadexchange/ImportExchangePackageInput.hxx>
#include <ce4/schemas/cadexchangemanagement/_2013_05/cadexchange/SetExpChkOutInput.hxx>


#include <CadExchangeManagement_exports.h>  
namespace Teamcenter
{
    namespace Services
    {
    
        namespace CadExchangeManagement
        {
             namespace _2013_05
             {


class SOACADEXCHANGEMANAGEMENT_API CadExchangeIiopSkeleton : public Teamcenter::Soa::Internal::Server::IiopSkeleton
{

public:

    CadExchangeIiopSkeleton();
 
    ~CadExchangeIiopSkeleton();
    virtual void initialize();
   

static CE4::Soa::CadExchangeManagement::_2013_05::CadExchange::CreateExportPkgInput  toLocal( Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPkgInput> wire );
static CE4::Soa::CadExchangeManagement::_2013_05::CadExchange::DatasetExportCheckOutInfo  toLocal( Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::DatasetExportCheckOutInfo> wire );
static CE4::Soa::CadExchangeManagement::_2013_05::CadExchange::ImportPackageInput  toLocal( Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportPackageInput> wire );

private:

    static CE4::Soa::CadExchangeManagement::_2013_05::CadExchangeImpl* _service;
	static Teamcenter::Soa::Server::ServicePolicy*  	 _servicePolicy;


    static void createExportPackage( const std::string& xmlIn, std::string& xmlOut );
    
    static void importExchangePackage( const std::string& xmlIn, std::string& xmlOut );
    
    static void setExpChkOut( const std::string& xmlIn, std::string& xmlOut );
    


};    // End Class



}}}}    // End Namespace
#include <CadExchangeManagement_undef.h>
//#endif   


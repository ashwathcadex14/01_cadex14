/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           libce4tccadex_exports.h          
#      Module          :           libce4tccadex_v1.output.server.gensrc.CE4tccadex          
#      Description     :           Exports definition file          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    

/** 
    @file 

    This file contains the declaration for the Dispatch Library  CE4tccadex

*/

#include <common/library_indicators.h>

#ifdef EXPORTLIBRARY
#define EXPORTLIBRARY something else
#error ExportLibrary was already defined
#endif

#define EXPORTLIBRARY            libCE4tccadex

#if !defined(LIBCE4TCCADEX) && !defined(IPLIB)
#   error IPLIB or LIBCE4TCCADEX is not defined
#endif

/* Handwritten code should use CE4TCCADEX_API, not CE4TCCADEXEXPORT */

#define CE4TCCADEX_API CE4TCCADEXEXPORT

#if IPLIB==libCE4tccadex || defined(LIBCE4TCCADEX)
#   if defined(__lint)
#       define CE4TCCADEXEXPORT       __export(CE4tccadex)
#       define CE4TCCADEXGLOBAL       extern __global(CE4tccadex)
#       define CE4TCCADEXPRIVATE      extern __private(CE4tccadex)
#   elif defined(_WIN32)
#       define CE4TCCADEXEXPORT       __declspec(dllexport)
#       define CE4TCCADEXGLOBAL       extern __declspec(dllexport)
#       define CE4TCCADEXPRIVATE      extern
#   else
#       define CE4TCCADEXEXPORT
#       define CE4TCCADEXGLOBAL       extern
#       define CE4TCCADEXPRIVATE      extern
#   endif
#else
#   if defined(__lint)
#       define CE4TCCADEXEXPORT       __export(CE4tccadex)
#       define CE4TCCADEXGLOBAL       extern __global(CE4tccadex)
#   elif defined(_WIN32) && !defined(WNT_STATIC_LINK)
#       define CE4TCCADEXEXPORT      __declspec(dllimport)
#       define CE4TCCADEXGLOBAL       extern __declspec(dllimport)
#   else
#       define CE4TCCADEXEXPORT
#       define CE4TCCADEXGLOBAL       extern
#   endif
#endif

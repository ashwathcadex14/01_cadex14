/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           Ce4_Schemas_Cadexchangemanagement_2013_05_Cadexchange.cxx          
#      Module          :           libce4tccadex_v1.output.types.gensrc.CE4SoaCadExchangeManagement.cpp          
#      Description     :           Auto Generated SOA file for CadExchange Service          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
/* 










   Auto-generated source from service interface.
                 DO NOT EDIT


*/

#include <ce4/schemas/cadexchangemanagement/_2013_05/cadexchange/CreateExportPackageInput.hxx>
#include <ce4/schemas/cadexchangemanagement/_2013_05/cadexchange/DatasetExportCheckOutInfo.hxx>
#include <ce4/schemas/cadexchangemanagement/_2013_05/cadexchange/ImportPackageInput.hxx>
#include <ce4/schemas/cadexchangemanagement/_2013_05/cadexchange/ImportExchangePackageInput.hxx>
#include <ce4/schemas/cadexchangemanagement/_2013_05/cadexchange/SetExpChkOutInput.hxx>
#include <ce4/schemas/cadexchangemanagement/_2013_05/cadexchange/CreateExportPkgInput.hxx>


#include <teamcenter/soa/common/xml/XmlUtils.hxx>
#include <teamcenter/soa/common/exceptions/DomException.hxx>
#include <teamcenter/soa/internal/common/Monitor.hxx>
#include <teamcenter/soa/common/xml/SaxToNodeParser.hxx>
#include <teamcenter/soa/internal/json/Value.hxx>
#include <teamcenter/soa/internal/json/JSONArray.hxx>
#include <teamcenter/soa/internal/json/JSONObject.hxx>
#include <teamcenter/soa/internal/json/BooleanValue.hxx>
#include <teamcenter/soa/internal/json/DateValue.hxx>
#include <teamcenter/soa/internal/json/DoubleValue.hxx>
#include <teamcenter/soa/internal/json/IntegerValue.hxx>
#include <teamcenter/soa/internal/json/StringValue.hxx>


using namespace Teamcenter::Soa::Common;
using namespace Teamcenter::Soa::Common::Xml;
using namespace Teamcenter::Soa::Common::Exceptions;
using namespace Teamcenter::Soa::Internal::Common;
using namespace Teamcenter::Soa::Internal::Json;

namespace Ce4
{

namespace Schemas
{

namespace Cadexchangemanagement
{

namespace _2013_05
{

namespace Cadexchange
{

CreateExportPackageInput::CreateExportPackageInput(   ):
    BaseObject( "http://ce4.com/Schemas/CadExchangeManagement/2013-05/CadExchange", "CreateExportPackageInput" ),
    m_exPkgSessionInput ( new Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPkgInput() ){
}

CreateExportPackageInput* CreateExportPackageInput::reallyClone()
{
    return new CreateExportPackageInput( *this );
}

Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPkgInput>& CreateExportPackageInput::getExPkgSessionInput() 
{
   return m_exPkgSessionInput;
}

const Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPkgInput>& CreateExportPackageInput::getExPkgSessionInput() const
{
   return m_exPkgSessionInput;
}

void CreateExportPackageInput::setExPkgSessionInput( const Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPkgInput>& createExportPkgInput )
{
   m_exPkgSessionInput = createExportPkgInput;
}

void CreateExportPackageInput::outputXML( XmlStream& out, const std::string& elementName ) const
{
    bool root = Teamcenter::Soa::Common::Xml::BaseObject::outputXMLBase(out, elementName, "Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPackageInput::outputXML");
    

    
    out.writeOpenElementClose( m_xsdNamespace, false );//XML_ELEMENT_SERIALIZATION_IMPL
    std::string prefix = out.getNamespacePrefix( m_xsdNamespace ) + ":";
   m_exPkgSessionInput->outputXML( out, prefix+"exPkgSessionInput" ); //SERIALIZE_REQUIRED_COMPLEX_ELEMENT

    out.writeCloseElement( m_xsdNamespace, elementName  );


    if (root)
        Monitor::markEnd("Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPackageInput::outputXML");
}

void CreateExportPackageInput::writeJSON( Teamcenter::Soa::Common::Xml::JsonStream* stream ) const
{
    stream->writeObjectOpen( getQaulifiedName() );
    stream->writeKey( "exPkgSessionInput" ); //SERIALIZE_REQUIRED_COMPLEX_JSON_ELEMENT
    m_exPkgSessionInput->writeJSON( stream );
    stream->writeObjectClose();
}

void CreateExportPackageInput::parse( const XMLNode& node )
{
    if(node.parent == NULL)
        Monitor::markStart("Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPackageInput::parse");

    

        //XML_ELEMENT_PARSING_IMPL 
    for ( XMLNodeIterator it = node.children.begin(); it != node.children.end(); ++it )
    {
        const XMLNode& childNode = **it;
        if ( childNode.name == "exPkgSessionInput" ) //PARSE_COMPLEX_ELEMENT
       {
           m_exPkgSessionInput->parse( childNode );
       }
       else 
        {
            // Ignore these elements, may be for the parent type
        }
    }


    if (node.parent == NULL)
        Monitor::markEnd("Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPackageInput::parse");
}

void CreateExportPackageInput::parseJSON( const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Soa::Internal::Json::JSONObject> node )
{



     m_exPkgSessionInput->parseJSON( node->getJSONObject( "exPkgSessionInput" ) ); //PARSE_COMPLEX_JSON_ELEMENT


}

void CreateExportPackageInput::getNamespaces( std::set< std::string >& namespaces ) const
{
    namespaces.insert(m_xsdNamespace);

       m_exPkgSessionInput->getNamespaces( namespaces ); //GETNAMESPACE_REQUIRED_COMPLEX_ELEMENT

}


SOA_CLASS_NEW_OPERATORS_IMPL(CreateExportPackageInput, "Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPackageInput")

DatasetExportCheckOutInfo::DatasetExportCheckOutInfo(   ):
    BaseObject( "http://ce4.com/Schemas/CadExchangeManagement/2013-05/CadExchange", "DatasetExportCheckOutInfo" ),
    m_dsetObject ( new Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject() ),
    m_bomLine ( new Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject() ),
    m_bomWindow ( new Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject() ){
    setIsExport( false );
    setIsCheckOut( false );
}

DatasetExportCheckOutInfo* DatasetExportCheckOutInfo::reallyClone()
{
    return new DatasetExportCheckOutInfo( *this );
}

bool DatasetExportCheckOutInfo::getIsExport() const
{
   return m_isExport;
}

void DatasetExportCheckOutInfo::setIsExport( bool isExport )
{
   m_isExport = isExport;
}

bool DatasetExportCheckOutInfo::getIsCheckOut() const
{
   return m_isCheckOut;
}

void DatasetExportCheckOutInfo::setIsCheckOut( bool isCheckOut )
{
   m_isCheckOut = isCheckOut;
}

Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& DatasetExportCheckOutInfo::getDsetObject() 
{
   return m_dsetObject;
}

const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& DatasetExportCheckOutInfo::getDsetObject() const
{
   return m_dsetObject;
}

void DatasetExportCheckOutInfo::setDsetObject( const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& modelObject )
{
   m_dsetObject = modelObject;
}

Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& DatasetExportCheckOutInfo::getBomLine() 
{
   return m_bomLine;
}

const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& DatasetExportCheckOutInfo::getBomLine() const
{
   return m_bomLine;
}

void DatasetExportCheckOutInfo::setBomLine( const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& modelObject )
{
   m_bomLine = modelObject;
}

Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& DatasetExportCheckOutInfo::getBomWindow() 
{
   return m_bomWindow;
}

const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& DatasetExportCheckOutInfo::getBomWindow() const
{
   return m_bomWindow;
}

void DatasetExportCheckOutInfo::setBomWindow( const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& modelObject )
{
   m_bomWindow = modelObject;
}

void DatasetExportCheckOutInfo::outputXML( XmlStream& out, const std::string& elementName ) const
{
    bool root = Teamcenter::Soa::Common::Xml::BaseObject::outputXMLBase(out, elementName, "Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::DatasetExportCheckOutInfo::outputXML");
        out.writeAttribute  ( "isExport", lexical_cast< std::string, bool >( m_isExport ) ); //SERIALIZE_REQUIRED_ATTR
    out.writeAttribute  ( "isCheckOut", lexical_cast< std::string, bool >( m_isCheckOut ) ); //SERIALIZE_REQUIRED_ATTR


    
    out.writeOpenElementClose( m_xsdNamespace, false );//XML_ELEMENT_SERIALIZATION_IMPL
    std::string prefix = out.getNamespacePrefix( m_xsdNamespace ) + ":";
   m_dsetObject->outputXML( out, prefix+"dsetObject" ); //SERIALIZE_REQUIRED_COMPLEX_ELEMENT
   m_bomLine->outputXML( out, prefix+"bomLine" ); //SERIALIZE_REQUIRED_COMPLEX_ELEMENT
   m_bomWindow->outputXML( out, prefix+"bomWindow" ); //SERIALIZE_REQUIRED_COMPLEX_ELEMENT

    out.writeCloseElement( m_xsdNamespace, elementName  );


    if (root)
        Monitor::markEnd("Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::DatasetExportCheckOutInfo::outputXML");
}

void DatasetExportCheckOutInfo::writeJSON( Teamcenter::Soa::Common::Xml::JsonStream* stream ) const
{
    stream->writeObjectOpen( getQaulifiedName() );
    stream->writeAttribute( "isExport", m_isExport, true ); // SERIALIZE_REQUIRED_JSON_ATTR
    stream->writeAttribute( "isCheckOut", m_isCheckOut, true ); // SERIALIZE_REQUIRED_JSON_ATTR
    stream->writeKey( "dsetObject" ); //SERIALIZE_REQUIRED_COMPLEX_JSON_ELEMENT
    m_dsetObject->writeJSON( stream );
    stream->writeKey( "bomLine" ); //SERIALIZE_REQUIRED_COMPLEX_JSON_ELEMENT
    m_bomLine->writeJSON( stream );
    stream->writeKey( "bomWindow" ); //SERIALIZE_REQUIRED_COMPLEX_JSON_ELEMENT
    m_bomWindow->writeJSON( stream );
    stream->writeObjectClose();
}

void DatasetExportCheckOutInfo::parse( const XMLNode& node )
{
    if(node.parent == NULL)
        Monitor::markStart("Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::DatasetExportCheckOutInfo::parse");

        setIsExport( lexical_cast< bool, std::string >( node.getAttr("isExport") ) ); //PARSE_REQUIRED_ATTR
    setIsCheckOut( lexical_cast< bool, std::string >( node.getAttr("isCheckOut") ) ); //PARSE_REQUIRED_ATTR


        //XML_ELEMENT_PARSING_IMPL 
    for ( XMLNodeIterator it = node.children.begin(); it != node.children.end(); ++it )
    {
        const XMLNode& childNode = **it;
        if ( childNode.name == "dsetObject" ) //PARSE_COMPLEX_ELEMENT
       {
           m_dsetObject->parse( childNode );
       }
       else if ( childNode.name == "bomLine" ) //PARSE_COMPLEX_ELEMENT
       {
           m_bomLine->parse( childNode );
       }
       else if ( childNode.name == "bomWindow" ) //PARSE_COMPLEX_ELEMENT
       {
           m_bomWindow->parse( childNode );
       }
       else 
        {
            // Ignore these elements, may be for the parent type
        }
    }


    if (node.parent == NULL)
        Monitor::markEnd("Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::DatasetExportCheckOutInfo::parse");
}

void DatasetExportCheckOutInfo::parseJSON( const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Soa::Internal::Json::JSONObject> node )
{

    setIsExport( node->getBoolean( "isExport" ) ); //PARSE_REQUIRED_JSON_ATTR
    setIsCheckOut( node->getBoolean( "isCheckOut" ) ); //PARSE_REQUIRED_JSON_ATTR


     if(  node->getValueType( "dsetObject" ) == Teamcenter::Soa::Internal::Json::Value::StringValueType ) //PARSE_MODELOBJECT_JSON_ELEMENT
     {
         m_dsetObject->setUid( node->getString( "dsetObject" ) );
     }
     else
     {
          m_dsetObject->parseJSON( node->getJSONObject( "dsetObject" ) );
     }
     if(  node->getValueType( "bomLine" ) == Teamcenter::Soa::Internal::Json::Value::StringValueType ) //PARSE_MODELOBJECT_JSON_ELEMENT
     {
         m_bomLine->setUid( node->getString( "bomLine" ) );
     }
     else
     {
          m_bomLine->parseJSON( node->getJSONObject( "bomLine" ) );
     }
     if(  node->getValueType( "bomWindow" ) == Teamcenter::Soa::Internal::Json::Value::StringValueType ) //PARSE_MODELOBJECT_JSON_ELEMENT
     {
         m_bomWindow->setUid( node->getString( "bomWindow" ) );
     }
     else
     {
          m_bomWindow->parseJSON( node->getJSONObject( "bomWindow" ) );
     }


}

void DatasetExportCheckOutInfo::getNamespaces( std::set< std::string >& namespaces ) const
{
    namespaces.insert(m_xsdNamespace);

       m_dsetObject->getNamespaces( namespaces ); //GETNAMESPACE_REQUIRED_COMPLEX_ELEMENT
   m_bomLine->getNamespaces( namespaces ); //GETNAMESPACE_REQUIRED_COMPLEX_ELEMENT
   m_bomWindow->getNamespaces( namespaces ); //GETNAMESPACE_REQUIRED_COMPLEX_ELEMENT

}


SOA_CLASS_NEW_OPERATORS_IMPL(DatasetExportCheckOutInfo, "Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::DatasetExportCheckOutInfo")

ImportPackageInput::ImportPackageInput(   ):
    BaseObject( "http://ce4.com/Schemas/CadExchangeManagement/2013-05/CadExchange", "ImportPackageInput" ),
    m_packageRev ( new Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject() ),
    m_importTM ( new Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject() ){
}

ImportPackageInput* ImportPackageInput::reallyClone()
{
    return new ImportPackageInput( *this );
}

Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& ImportPackageInput::getPackageRev() 
{
   return m_packageRev;
}

const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& ImportPackageInput::getPackageRev() const
{
   return m_packageRev;
}

void ImportPackageInput::setPackageRev( const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& modelObject )
{
   m_packageRev = modelObject;
}

Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& ImportPackageInput::getImportTM() 
{
   return m_importTM;
}

const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& ImportPackageInput::getImportTM() const
{
   return m_importTM;
}

void ImportPackageInput::setImportTM( const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& modelObject )
{
   m_importTM = modelObject;
}

void ImportPackageInput::outputXML( XmlStream& out, const std::string& elementName ) const
{
    bool root = Teamcenter::Soa::Common::Xml::BaseObject::outputXMLBase(out, elementName, "Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportPackageInput::outputXML");
    

    
    out.writeOpenElementClose( m_xsdNamespace, false );//XML_ELEMENT_SERIALIZATION_IMPL
    std::string prefix = out.getNamespacePrefix( m_xsdNamespace ) + ":";
   m_packageRev->outputXML( out, prefix+"packageRev" ); //SERIALIZE_REQUIRED_COMPLEX_ELEMENT
   m_importTM->outputXML( out, prefix+"importTM" ); //SERIALIZE_REQUIRED_COMPLEX_ELEMENT

    out.writeCloseElement( m_xsdNamespace, elementName  );


    if (root)
        Monitor::markEnd("Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportPackageInput::outputXML");
}

void ImportPackageInput::writeJSON( Teamcenter::Soa::Common::Xml::JsonStream* stream ) const
{
    stream->writeObjectOpen( getQaulifiedName() );
    stream->writeKey( "packageRev" ); //SERIALIZE_REQUIRED_COMPLEX_JSON_ELEMENT
    m_packageRev->writeJSON( stream );
    stream->writeKey( "importTM" ); //SERIALIZE_REQUIRED_COMPLEX_JSON_ELEMENT
    m_importTM->writeJSON( stream );
    stream->writeObjectClose();
}

void ImportPackageInput::parse( const XMLNode& node )
{
    if(node.parent == NULL)
        Monitor::markStart("Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportPackageInput::parse");

    

        //XML_ELEMENT_PARSING_IMPL 
    for ( XMLNodeIterator it = node.children.begin(); it != node.children.end(); ++it )
    {
        const XMLNode& childNode = **it;
        if ( childNode.name == "packageRev" ) //PARSE_COMPLEX_ELEMENT
       {
           m_packageRev->parse( childNode );
       }
       else if ( childNode.name == "importTM" ) //PARSE_COMPLEX_ELEMENT
       {
           m_importTM->parse( childNode );
       }
       else 
        {
            // Ignore these elements, may be for the parent type
        }
    }


    if (node.parent == NULL)
        Monitor::markEnd("Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportPackageInput::parse");
}

void ImportPackageInput::parseJSON( const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Soa::Internal::Json::JSONObject> node )
{



     if(  node->getValueType( "packageRev" ) == Teamcenter::Soa::Internal::Json::Value::StringValueType ) //PARSE_MODELOBJECT_JSON_ELEMENT
     {
         m_packageRev->setUid( node->getString( "packageRev" ) );
     }
     else
     {
          m_packageRev->parseJSON( node->getJSONObject( "packageRev" ) );
     }
     if(  node->getValueType( "importTM" ) == Teamcenter::Soa::Internal::Json::Value::StringValueType ) //PARSE_MODELOBJECT_JSON_ELEMENT
     {
         m_importTM->setUid( node->getString( "importTM" ) );
     }
     else
     {
          m_importTM->parseJSON( node->getJSONObject( "importTM" ) );
     }


}

void ImportPackageInput::getNamespaces( std::set< std::string >& namespaces ) const
{
    namespaces.insert(m_xsdNamespace);

       m_packageRev->getNamespaces( namespaces ); //GETNAMESPACE_REQUIRED_COMPLEX_ELEMENT
   m_importTM->getNamespaces( namespaces ); //GETNAMESPACE_REQUIRED_COMPLEX_ELEMENT

}


SOA_CLASS_NEW_OPERATORS_IMPL(ImportPackageInput, "Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportPackageInput")

ImportExchangePackageInput::ImportExchangePackageInput(   ):
    BaseObject( "http://ce4.com/Schemas/CadExchangeManagement/2013-05/CadExchange", "ImportExchangePackageInput" ),
    m_importPkgInput ( new Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportPackageInput() ){
}

ImportExchangePackageInput* ImportExchangePackageInput::reallyClone()
{
    return new ImportExchangePackageInput( *this );
}

Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportPackageInput>& ImportExchangePackageInput::getImportPkgInput() 
{
   return m_importPkgInput;
}

const Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportPackageInput>& ImportExchangePackageInput::getImportPkgInput() const
{
   return m_importPkgInput;
}

void ImportExchangePackageInput::setImportPkgInput( const Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportPackageInput>& importPackageInput )
{
   m_importPkgInput = importPackageInput;
}

void ImportExchangePackageInput::outputXML( XmlStream& out, const std::string& elementName ) const
{
    bool root = Teamcenter::Soa::Common::Xml::BaseObject::outputXMLBase(out, elementName, "Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportExchangePackageInput::outputXML");
    

    
    out.writeOpenElementClose( m_xsdNamespace, false );//XML_ELEMENT_SERIALIZATION_IMPL
    std::string prefix = out.getNamespacePrefix( m_xsdNamespace ) + ":";
   m_importPkgInput->outputXML( out, prefix+"importPkgInput" ); //SERIALIZE_REQUIRED_COMPLEX_ELEMENT

    out.writeCloseElement( m_xsdNamespace, elementName  );


    if (root)
        Monitor::markEnd("Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportExchangePackageInput::outputXML");
}

void ImportExchangePackageInput::writeJSON( Teamcenter::Soa::Common::Xml::JsonStream* stream ) const
{
    stream->writeObjectOpen( getQaulifiedName() );
    stream->writeKey( "importPkgInput" ); //SERIALIZE_REQUIRED_COMPLEX_JSON_ELEMENT
    m_importPkgInput->writeJSON( stream );
    stream->writeObjectClose();
}

void ImportExchangePackageInput::parse( const XMLNode& node )
{
    if(node.parent == NULL)
        Monitor::markStart("Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportExchangePackageInput::parse");

    

        //XML_ELEMENT_PARSING_IMPL 
    for ( XMLNodeIterator it = node.children.begin(); it != node.children.end(); ++it )
    {
        const XMLNode& childNode = **it;
        if ( childNode.name == "importPkgInput" ) //PARSE_COMPLEX_ELEMENT
       {
           m_importPkgInput->parse( childNode );
       }
       else 
        {
            // Ignore these elements, may be for the parent type
        }
    }


    if (node.parent == NULL)
        Monitor::markEnd("Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportExchangePackageInput::parse");
}

void ImportExchangePackageInput::parseJSON( const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Soa::Internal::Json::JSONObject> node )
{



     m_importPkgInput->parseJSON( node->getJSONObject( "importPkgInput" ) ); //PARSE_COMPLEX_JSON_ELEMENT


}

void ImportExchangePackageInput::getNamespaces( std::set< std::string >& namespaces ) const
{
    namespaces.insert(m_xsdNamespace);

       m_importPkgInput->getNamespaces( namespaces ); //GETNAMESPACE_REQUIRED_COMPLEX_ELEMENT

}


SOA_CLASS_NEW_OPERATORS_IMPL(ImportExchangePackageInput, "Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::ImportExchangePackageInput")

SetExpChkOutInput::SetExpChkOutInput(   ):
    BaseObject( "http://ce4.com/Schemas/CadExchangeManagement/2013-05/CadExchange", "SetExpChkOutInput" ) {
}

SetExpChkOutInput* SetExpChkOutInput::reallyClone()
{
    return new SetExpChkOutInput( *this );
}

const std::vector< Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::DatasetExportCheckOutInfo> >& SetExpChkOutInput::getDatasetExportChkOutInfoArray() const
{
   return m_datasetExportChkOutInfo;
}

void SetExpChkOutInput::setDatasetExportChkOutInfoArray( const std::vector< Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::DatasetExportCheckOutInfo> >& datasetExportChkOutInfo )
{
   m_datasetExportChkOutInfo = datasetExportChkOutInfo;
}

void SetExpChkOutInput::addDatasetExportChkOutInfo( const Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::DatasetExportCheckOutInfo>& datasetExportChkOutInfo )
{
    m_datasetExportChkOutInfo.push_back( datasetExportChkOutInfo );
}

void SetExpChkOutInput::outputXML( XmlStream& out, const std::string& elementName ) const
{
    bool root = Teamcenter::Soa::Common::Xml::BaseObject::outputXMLBase(out, elementName, "Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::SetExpChkOutInput::outputXML");
    

    
    out.writeOpenElementClose( m_xsdNamespace, false );//XML_ELEMENT_SERIALIZATION_IMPL
    std::string prefix = out.getNamespacePrefix( m_xsdNamespace ) + ":";
    for ( Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::DatasetExportCheckOutInfoArray::const_iterator it = m_datasetExportChkOutInfo.begin(); it != m_datasetExportChkOutInfo.end(); ++it ) //SERIALIZE_COMPLEX_ARRAY
    {
        it->getPtr()->outputXML( out, prefix+"datasetExportChkOutInfo" );
    }

    out.writeCloseElement( m_xsdNamespace, elementName  );


    if (root)
        Monitor::markEnd("Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::SetExpChkOutInput::outputXML");
}

void SetExpChkOutInput::writeJSON( Teamcenter::Soa::Common::Xml::JsonStream* stream ) const
{
    stream->writeObjectOpen( getQaulifiedName() );
    JsonStream::writeVectorIfJsonSerializeable( m_datasetExportChkOutInfo, "datasetExportChkOutInfo",   stream ); // SERIALIZE_COMPLEX_JSON_ARRAY
    stream->writeObjectClose();
}

void SetExpChkOutInput::parse( const XMLNode& node )
{
    if(node.parent == NULL)
        Monitor::markStart("Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::SetExpChkOutInput::parse");

    

        //XML_ELEMENT_PARSING_IMPL 
    m_datasetExportChkOutInfo.reserve( node.children.size() );
    for ( XMLNodeIterator it = node.children.begin(); it != node.children.end(); ++it )
    {
        const XMLNode& childNode = **it;
        if ( childNode.name == "datasetExportChkOutInfo" ) //PARSE_COMPLEX_ARRAY
       {
           Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::DatasetExportCheckOutInfo> newObj;
           newObj = new Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::DatasetExportCheckOutInfo();

           newObj->parse( childNode );
           addDatasetExportChkOutInfo( newObj );
       }
       else 
        {
            // Ignore these elements, may be for the parent type
        }
    }


    if (node.parent == NULL)
        Monitor::markEnd("Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::SetExpChkOutInput::parse");
}

void SetExpChkOutInput::parseJSON( const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Soa::Internal::Json::JSONObject> node )
{



    //PARSE_COMPLEX_JSON_ARRAY
    if ( node->containsKey( "datasetExportChkOutInfo" ) )
    {
        AutoPtr<JSONArray> childNode = node->getJSONArray( "datasetExportChkOutInfo" );
        m_datasetExportChkOutInfo.reserve( childNode->size() );
        for( size_t ii = 0; ii < childNode->size(); ++ii )
        {
            Teamcenter::Soa::Common::AutoPtr<Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::DatasetExportCheckOutInfo> newObj =
                                         new Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::DatasetExportCheckOutInfo();
            newObj->parseJSON( childNode->getJSONObject( ii ) );
            addDatasetExportChkOutInfo( newObj );
        }
    }


}

void SetExpChkOutInput::getNamespaces( std::set< std::string >& namespaces ) const
{
    namespaces.insert(m_xsdNamespace);

        for( size_t i=0; i<m_datasetExportChkOutInfo.size(); ++i ) //GETNAMESPACE_COMPLEX_ARRAY
    {
        m_datasetExportChkOutInfo[i]->getNamespaces( namespaces );
    }

}


SOA_CLASS_NEW_OPERATORS_IMPL(SetExpChkOutInput, "Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::SetExpChkOutInput")

CreateExportPkgInput::CreateExportPkgInput(   ):
    BaseObject( "http://ce4.com/Schemas/CadExchangeManagement/2013-05/CadExchange", "CreateExportPkgInput" ),
    m_exPkgRevision ( new Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject() ),
    m_exPkgSessionTM ( new Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject() ){
}

CreateExportPkgInput* CreateExportPkgInput::reallyClone()
{
    return new CreateExportPkgInput( *this );
}

Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& CreateExportPkgInput::getExPkgRevision() 
{
   return m_exPkgRevision;
}

const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& CreateExportPkgInput::getExPkgRevision() const
{
   return m_exPkgRevision;
}

void CreateExportPkgInput::setExPkgRevision( const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& modelObject )
{
   m_exPkgRevision = modelObject;
}

Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& CreateExportPkgInput::getExPkgSessionTM() 
{
   return m_exPkgSessionTM;
}

const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& CreateExportPkgInput::getExPkgSessionTM() const
{
   return m_exPkgSessionTM;
}

void CreateExportPkgInput::setExPkgSessionTM( const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& modelObject )
{
   m_exPkgSessionTM = modelObject;
}

void CreateExportPkgInput::outputXML( XmlStream& out, const std::string& elementName ) const
{
    bool root = Teamcenter::Soa::Common::Xml::BaseObject::outputXMLBase(out, elementName, "Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPkgInput::outputXML");
    

    
    out.writeOpenElementClose( m_xsdNamespace, false );//XML_ELEMENT_SERIALIZATION_IMPL
    std::string prefix = out.getNamespacePrefix( m_xsdNamespace ) + ":";
   m_exPkgRevision->outputXML( out, prefix+"exPkgRevision" ); //SERIALIZE_REQUIRED_COMPLEX_ELEMENT
   m_exPkgSessionTM->outputXML( out, prefix+"exPkgSessionTM" ); //SERIALIZE_REQUIRED_COMPLEX_ELEMENT

    out.writeCloseElement( m_xsdNamespace, elementName  );


    if (root)
        Monitor::markEnd("Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPkgInput::outputXML");
}

void CreateExportPkgInput::writeJSON( Teamcenter::Soa::Common::Xml::JsonStream* stream ) const
{
    stream->writeObjectOpen( getQaulifiedName() );
    stream->writeKey( "exPkgRevision" ); //SERIALIZE_REQUIRED_COMPLEX_JSON_ELEMENT
    m_exPkgRevision->writeJSON( stream );
    stream->writeKey( "exPkgSessionTM" ); //SERIALIZE_REQUIRED_COMPLEX_JSON_ELEMENT
    m_exPkgSessionTM->writeJSON( stream );
    stream->writeObjectClose();
}

void CreateExportPkgInput::parse( const XMLNode& node )
{
    if(node.parent == NULL)
        Monitor::markStart("Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPkgInput::parse");

    

        //XML_ELEMENT_PARSING_IMPL 
    for ( XMLNodeIterator it = node.children.begin(); it != node.children.end(); ++it )
    {
        const XMLNode& childNode = **it;
        if ( childNode.name == "exPkgRevision" ) //PARSE_COMPLEX_ELEMENT
       {
           m_exPkgRevision->parse( childNode );
       }
       else if ( childNode.name == "exPkgSessionTM" ) //PARSE_COMPLEX_ELEMENT
       {
           m_exPkgSessionTM->parse( childNode );
       }
       else 
        {
            // Ignore these elements, may be for the parent type
        }
    }


    if (node.parent == NULL)
        Monitor::markEnd("Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPkgInput::parse");
}

void CreateExportPkgInput::parseJSON( const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Soa::Internal::Json::JSONObject> node )
{



     if(  node->getValueType( "exPkgRevision" ) == Teamcenter::Soa::Internal::Json::Value::StringValueType ) //PARSE_MODELOBJECT_JSON_ELEMENT
     {
         m_exPkgRevision->setUid( node->getString( "exPkgRevision" ) );
     }
     else
     {
          m_exPkgRevision->parseJSON( node->getJSONObject( "exPkgRevision" ) );
     }
     if(  node->getValueType( "exPkgSessionTM" ) == Teamcenter::Soa::Internal::Json::Value::StringValueType ) //PARSE_MODELOBJECT_JSON_ELEMENT
     {
         m_exPkgSessionTM->setUid( node->getString( "exPkgSessionTM" ) );
     }
     else
     {
          m_exPkgSessionTM->parseJSON( node->getJSONObject( "exPkgSessionTM" ) );
     }


}

void CreateExportPkgInput::getNamespaces( std::set< std::string >& namespaces ) const
{
    namespaces.insert(m_xsdNamespace);

       m_exPkgRevision->getNamespaces( namespaces ); //GETNAMESPACE_REQUIRED_COMPLEX_ELEMENT
   m_exPkgSessionTM->getNamespaces( namespaces ); //GETNAMESPACE_REQUIRED_COMPLEX_ELEMENT

}


SOA_CLASS_NEW_OPERATORS_IMPL(CreateExportPkgInput, "Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::CreateExportPkgInput")

}

}

}

}

}


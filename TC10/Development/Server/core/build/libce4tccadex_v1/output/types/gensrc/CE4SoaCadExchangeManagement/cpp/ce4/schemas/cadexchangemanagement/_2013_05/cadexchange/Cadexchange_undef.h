/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           Cadexchange_undef.h          
#      Module          :           libce4tccadex_v1.output.types.gensrc.CE4SoaCadExchangeManagement.cpp.ce4.schemas.cadexchangemanagement._2013_05.cadexchange          
#      Description     :           Auto Generated SOA file for CadExchange Service          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
/* 










   Auto-generated source from service interface.
                 DO NOT EDIT


*/
#include <common/library_indicators.h>


#if !defined(EXPORTLIBRARY)
#   error EXPORTLIBRARY is not defined
#endif

#undef EXPORTLIBRARY

#if !defined(IPLIB)
#   error IPLIB is not defined
#endif

#undef CADEXCHANGE_API
#undef CADEXCHANGEEXPORT
#undef CADEXCHANGEGLOBAL
#undef CADEXCHANGEPRIVATE


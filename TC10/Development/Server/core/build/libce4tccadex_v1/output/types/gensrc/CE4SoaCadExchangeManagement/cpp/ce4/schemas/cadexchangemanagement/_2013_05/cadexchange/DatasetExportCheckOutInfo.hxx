/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           DatasetExportCheckOutInfo.hxx          
#      Module          :           libce4tccadex_v1.output.types.gensrc.CE4SoaCadExchangeManagement.cpp.ce4.schemas.cadexchangemanagement._2013_05.cadexchange          
#      Description     :           Auto Generated SOA file for CadExchange Service          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
/* 










   Auto-generated source from service interface.
                 DO NOT EDIT


*/

#ifndef CE4__SCHEMAS__CADEXCHANGEMANAGEMENT___2013_05__CADEXCHANGE_DATASETEXPORTCHECKOUTINFO_HXX
#define CE4__SCHEMAS__CADEXCHANGEMANAGEMENT___2013_05__CADEXCHANGE_DATASETEXPORTCHECKOUTINFO_HXX


#include <vector>
#include <string>
#include <set>
#include <ostream>
#include <new> // for size_t

#include <teamcenter/soa/common/MemoryManager.hxx>
#include <teamcenter/soa/common/DateTime.hxx>
#include <teamcenter/soa/common/AutoPtr.hxx>
#include <teamcenter/soa/common/xml/BaseObject.hxx>
#include <teamcenter/soa/common/xml/XmlUtils.hxx>
#include <teamcenter/soa/common/xml/XmlStream.hxx>
#include <teamcenter/soa/common/xml/JsonStream.hxx>

#include <teamcenter/schemas/soa/_2006_03/base/ModelObject.hxx>


#include <ce4/schemas/cadexchangemanagement/_2013_05/cadexchange/Cadexchange_exports.h>

namespace Ce4
{
    namespace Schemas
    {
        namespace Cadexchangemanagement
        {
            namespace _2013_05
            {
                namespace Cadexchange
                {

                    class DatasetExportCheckOutInfo;
                    typedef std::vector< Teamcenter::Soa::Common::AutoPtr<DatasetExportCheckOutInfo> > DatasetExportCheckOutInfoArray;
                    class ModelObject;

                }
            }
        }
    }
}





class CADEXCHANGE_API Ce4::Schemas::Cadexchangemanagement::_2013_05::Cadexchange::DatasetExportCheckOutInfo : 
        public Teamcenter::Soa::Common::Xml::BaseObject 
{
 public:
    
    DatasetExportCheckOutInfo(   );


    bool getIsExport() const;
    void setIsExport( bool isExport );
    bool getIsCheckOut() const;
    void setIsCheckOut( bool isCheckOut );
    Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& getDsetObject() ;
    const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& getDsetObject() const;
    void setDsetObject( const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& modelObject );
    Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& getBomLine() ;
    const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& getBomLine() const;
    void setBomLine( const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& modelObject );
    Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& getBomWindow() ;
    const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& getBomWindow() const;
    void setBomWindow( const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>& modelObject );


    virtual void outputXML( Teamcenter::Soa::Common::Xml::XmlStream& out ) const { outputXML( out, "DatasetExportCheckOutInfo" ); }
    virtual void outputXML( Teamcenter::Soa::Common::Xml::XmlStream& out, const std::string& elementName ) const;
    virtual void parse    ( const Teamcenter::Soa::Common::Xml::XMLNode& node );
    virtual void getNamespaces( std::set< std::string >& namespaces ) const;
  
    virtual void writeJSON( Teamcenter::Soa::Common::Xml::JsonStream* stream ) const;
    virtual void parseJSON( const Teamcenter::Soa::Common::AutoPtr<Teamcenter::Soa::Internal::Json::JSONObject> node );

    
    SOA_CLASS_NEW_OPERATORS
    

protected:
    virtual DatasetExportCheckOutInfo* reallyClone();

         bool   m_isExport;
    bool   m_isCheckOut;
    Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>   m_dsetObject;
    Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>   m_bomLine;
    Teamcenter::Soa::Common::AutoPtr<Teamcenter::Schemas::Soa::_2006_03::Base::ModelObject>   m_bomWindow;

};


#include <ce4/schemas/cadexchangemanagement/_2013_05/cadexchange/Cadexchange_undef.h>
#endif


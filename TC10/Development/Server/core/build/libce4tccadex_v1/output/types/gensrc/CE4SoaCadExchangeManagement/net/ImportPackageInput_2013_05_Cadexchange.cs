/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           ImportPackageInput_2013_05_Cadexchange.cs          
#      Module          :           libce4tccadex_v1.output.types.gensrc.CE4SoaCadExchangeManagement.net          
#      Description     :           Auto Generated SOA file for CadExchange Service          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
/* 










   Auto-generated source from service interface.
                 DO NOT EDIT


*/

using System.Xml.Serialization;

using Teamcenter.Schemas.Soa._2006_03.Base;


namespace Ce4.Schemas.Cadexchangemanagement._2013_05.Cadexchange 
{


[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd2csharp", "1.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://ce4.com/Schemas/CadExchangeManagement/2013-05/CadExchange", IsNullable=false)]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
  public partial class ImportPackageInput 
  {

         private Teamcenter.Schemas.Soa._2006_03.Base.ModelObject PackageRevField;
     ///<summary>XML Serialization Attributes</summary>
     [System.Xml.Serialization.XmlElementAttribute("packageRev")]
     public Teamcenter.Schemas.Soa._2006_03.Base.ModelObject PackageRev
     { 
        get { return this.PackageRevField;}
        set { this.PackageRevField = value;}
     }

     ///<summary>To support access via generated code.</summary>
     public Teamcenter.Schemas.Soa._2006_03.Base.ModelObject getPackageRev()
     { 
       return this.PackageRevField;
     }
     public void setPackageRev(Teamcenter.Schemas.Soa._2006_03.Base.ModelObject val)
     { 
       this.PackageRevField = val;
     }


     private Teamcenter.Schemas.Soa._2006_03.Base.ModelObject ImportTMField;
     ///<summary>XML Serialization Attributes</summary>
     [System.Xml.Serialization.XmlElementAttribute("importTM")]
     public Teamcenter.Schemas.Soa._2006_03.Base.ModelObject ImportTM
     { 
        get { return this.ImportTMField;}
        set { this.ImportTMField = value;}
     }

     ///<summary>To support access via generated code.</summary>
     public Teamcenter.Schemas.Soa._2006_03.Base.ModelObject getImportTM()
     { 
       return this.ImportTMField;
     }
     public void setImportTM(Teamcenter.Schemas.Soa._2006_03.Base.ModelObject val)
     { 
       this.ImportTMField = val;
     }



    
    


  } // type
} // ns
            






/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           SetExpChkOutInput_2013_05_Cadexchange.cs          
#      Module          :           libce4tccadex_v1.output.types.gensrc.CE4SoaCadExchangeManagement.net          
#      Description     :           Auto Generated SOA file for CadExchange Service          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
/* 










   Auto-generated source from service interface.
                 DO NOT EDIT


*/

using System.Xml.Serialization;



namespace Ce4.Schemas.Cadexchangemanagement._2013_05.Cadexchange 
{


[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd2csharp", "1.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://ce4.com/Schemas/CadExchangeManagement/2013-05/CadExchange", IsNullable=false)]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
  public partial class SetExpChkOutInput 
  {

         private Ce4.Schemas.Cadexchangemanagement._2013_05.Cadexchange.DatasetExportCheckOutInfo[] DatasetExportChkOutInfoField;
     ///<summary>XML Serialization Attributes</summary>
     [System.Xml.Serialization.XmlElementAttribute("datasetExportChkOutInfo")]
     public Ce4.Schemas.Cadexchangemanagement._2013_05.Cadexchange.DatasetExportCheckOutInfo[] DatasetExportChkOutInfo
     { 
        get { return this.DatasetExportChkOutInfoField;}
        set { this.DatasetExportChkOutInfoField = value;}
     }

     ///<summary>To support access via generated code.</summary>
     public System.Collections.ArrayList getDatasetExportChkOutInfo()
     { 
         if(this.DatasetExportChkOutInfoField==null)
         { 
             return new System.Collections.ArrayList();
         }
             return new System.Collections.ArrayList(this.DatasetExportChkOutInfoField);
     } 
     ///<summary>Set the vaule of variable </summary> 
     public void setDatasetExportChkOutInfo(System.Collections.ArrayList val)
     { 
       this.DatasetExportChkOutInfoField = new Ce4.Schemas.Cadexchangemanagement._2013_05.Cadexchange.DatasetExportCheckOutInfo[val.Count];
       val.CopyTo(this.DatasetExportChkOutInfoField);
     }



    
    


  } // type
} // ns
            






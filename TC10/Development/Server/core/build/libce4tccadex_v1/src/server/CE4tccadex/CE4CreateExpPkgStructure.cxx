/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CE4CreateExpPkgStructure.cxx          
#      Module          :           libce4tccadex_v1.src.server.CE4tccadex          
#      Description     :           To create BOM structure during creation of Exchange Package          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    








/* 
 * @file 
 *
 *   This file contains the implementation for the Extension CE4CreateExpPkgStructure
 *
 */
#include <CE4tccadex/CE4CreateExpPkgStructure.hxx>

int CE4CreateExpPkgStructure( METHOD_message_t *msg, va_list args )
{
 
	int iStatus				= 0 ,
		iRevAttrLen			= 0 ,
		iParamCount			= 0 ;

	char   *sRevID			= NULL ,
		   *sItemId			= NULL ;
	
	tag_t	tItem				= NULLTAG ,
			tRev				= NULLTAG ,
			tViewType			= NULLTAG ,
			tBomView			= NULLTAG ,
			tBomViewRevision	= NULLTAG ,
			tTargetItem			= NULLTAG ,
			tTargetRev			= NULLTAG ,
			tRefstItem			= NULLTAG ,
			tRefsRev			= NULLTAG ,
			tBomWindow			= NULLTAG ,
			tTopBomLine			= NULLTAG ,
			tNewLine			= NULLTAG ,
			tNewLine1			= NULLTAG ;

	string bomViewName		= "" ,
		   targetItemId		= "" ,
		   refereceItemId	= "" ,
		   operationType    = "" ;

	BMF_extension_arguments_t * extensionArgs = NULL;

	logical isNew			= false;

	const char* __function__ = "CE4CreateExpPkgStructure" ;
	CE4_TRACE_ENTER() ;

	try
	{	
		
		CE4_TRACE_CALL( iStatus = BMF_get_user_params(msg,&iParamCount,&extensionArgs));


		for(int iparamIndex=0;iparamIndex<iParamCount;iparamIndex++)
		{
			if(tc_strcmp(extensionArgs[iparamIndex].paramName,"operationType")==0)
			{
				operationType.append(extensionArgs[iparamIndex].arg_val.str_value);
			}
		}

		printf(" Operation Type is %s ", operationType.c_str() );
		//ce4_trace(1, " Operation Type is %s ", __LINE__, __FUNCTION__,operationType.c_str());

		if(operationType.compare("Save")==0)
		{
			tRev   = va_arg(args, tag_t);
			isNew = va_arg(args, logical);
		}

		return iStatus;

		//tItem    = va_arg(args, tag_t);
		//sRevID = va_arg(args, char*);
		

		if(isNew)
		{
			CE4_TRACE_CALL( iStatus = ITEM_ask_item_of_rev( tRev, &tItem));

			CE4_TRACE_CALL( iStatus = AOM_ask_value_string( tItem,	CE4_ATTR_ITEM_ID, &sItemId ) ) ;
			
			CE4_TRACE_CALL( iStatus = AOM_ask_value_string( tRev,	CE4_ATTR_REV_ID, &sRevID ) ) ;

			bomViewName.append(sItemId).append("/").append(sRevID);

			CE4_TRACE_CALL( iStatus = PS_find_view_type	( CE4_BOM_VIEW_TYPE_NAME , &tViewType ));	

			CE4_TRACE_CALL( iStatus = PS_create_bom_view ( tViewType , (const char*)sItemId , CE4_BOM_VIEW_DESC , tItem , &tBomView));	

			CE4_TRACE_CALL ( iStatus = AOM_save(tBomView));
			CE4_TRACE_CALL ( iStatus = AOM_save(tItem));
			CE4_TRACE_CALL ( iStatus = AOM_save(tRev));

			CE4_TRACE_CALL( iStatus = PS_create_bvr	( tBomView , bomViewName.c_str() , CE4_BOM_VIEW_REV_DESC, false,tRev, &tBomViewRevision));
	
			CE4_TRACE_CALL ( iStatus = AOM_save(tBomViewRevision));
			CE4_TRACE_CALL ( iStatus = AOM_save(tItem));
			CE4_TRACE_CALL ( iStatus = AOM_save(tRev));

			targetItemId.append ( CE4_TARGET_ITEM_ID_PREFIX ).append(CE4_TR_ITEM_ID_SEPERATOR).append(sItemId);

			CE4_TRACE_CALL ( iStatus = ITEM_create_item ( targetItemId.c_str() , CE4_TARGET_ITEM_ID_PREFIX ,	CE4_TARGET_ITEM_TYPE , (const char*)sRevID , &tTargetItem , &tTargetRev ) ) ;
		
			CE4_TRACE_CALL ( iStatus = AOM_save(tTargetItem));
			CE4_TRACE_CALL ( iStatus = AOM_save(tTargetRev));

			refereceItemId.append ( CE4_REFS_ITEM_ID_PREFIX ).append(CE4_TR_ITEM_ID_SEPERATOR).append(sItemId).append(CE4_TR_ITEM_ID_SEPERATOR);

			CE4_TRACE_CALL ( iStatus = ITEM_create_item ( refereceItemId.c_str() , CE4_REFS_ITEM_ID_PREFIX ,	CE4_REFERENCES_ITEM_TYPE , (const char*)sRevID , &tRefstItem , &tRefsRev ) ) ;
	
			CE4_TRACE_CALL ( iStatus = AOM_save(tRefstItem));
			CE4_TRACE_CALL ( iStatus = AOM_save(tRefsRev));

			CE4_TRACE_CALL ( iStatus = BOM_create_window ( &tBomWindow ) );
					
			CE4_TRACE_CALL ( iStatus = BOM_set_window_top_line ( tBomWindow , tItem , tRev , tBomViewRevision , &tTopBomLine ) ) ; 
	
			CE4_TRACE_CALL ( iStatus = BOM_line_add ( tTopBomLine, NULLTAG, tTargetRev, NULLTAG, &tNewLine));

			CE4_TRACE_CALL ( iStatus = BOM_line_add ( tTopBomLine, NULLTAG, tRefsRev, NULLTAG, &tNewLine1));

			

			CE4_TRACE_CALL ( iStatus = BOM_save_window ( tBomWindow ) );

			CE4_TRACE_CALL ( iStatus = BOM_close_window ( tBomWindow ) );
		}
		else
		{
			//Skipping
		}
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	CE4_free_memory ( sItemId ) ;
	CE4_free_memory ( sRevID ) ;

	CE4_TRACE_LEAVE() ;

	return iStatus ;

}

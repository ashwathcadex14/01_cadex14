/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CE4CreateExpPkgStructure.hxx          
#      Module          :           libce4tccadex_v1.src.server.CE4tccadex          
#      Description     :           To create BOM structure during creation of Exchange Package          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    








/* 
 * @file 
 *
 *   This file contains the declaration for the Extension CE4CreateExpPkgStructure
 *
 */
 
#ifndef CE4CREATEEXPPKGSTRUCTURE_HXX
#define CE4CREATEEXPPKGSTRUCTURE_HXX
#include <tccore/method.h>
#include <common/CE4_Common.h>
#include <CE4tccadex/libce4tccadex_exports.h>
#ifdef __cplusplus
         extern "C"{
#endif
                 
extern CE4TCCADEX_API int CE4CreateExpPkgStructure(METHOD_message_t* msg, va_list args);
                 
#ifdef __cplusplus
                   }
#endif
                
#include <CE4tccadex/libce4tccadex_undef.h>
                
#endif  // CE4CREATEEXPPKGSTRUCTURE_HXX

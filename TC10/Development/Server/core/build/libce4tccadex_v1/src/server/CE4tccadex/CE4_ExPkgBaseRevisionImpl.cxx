/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CE4_ExPkgBaseRevisionImpl.cxx          
#      Module          :           libce4tccadex_v1.src.server.CE4tccadex          
#      Description     :           Main implementation file for setter method of ce4_status          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    








/** 
    @file 

    This file contains the implementation for the Business Object CE4_ExPkgBaseRevision

*/

#include <CE4tccadex/CE4_ExPkgBaseRevision.hxx>
#include <CE4tccadex/CE4_ExPkgBaseRevisionImpl.hxx>

#include <fclasses/tc_string.h>
#include <tc/tc.h>

using namespace TCCADEX; 

//----------------------------------------------------------------------------------
// CE4_ExPkgBaseRevisionImpl::CE4_ExPkgBaseRevisionImpl(CE4_ExPkgBaseRevision& busObj)
// Constructor for the class
//---------------------------------------------------------------------------------- 
CE4_ExPkgBaseRevisionImpl::CE4_ExPkgBaseRevisionImpl( CE4_ExPkgBaseRevision& busObj )
   : CE4_ExPkgBaseRevisionGenImpl( busObj ) 
{
}

//----------------------------------------------------------------------------------
// CE4_ExPkgBaseRevisionImpl::~CE4_ExPkgBaseRevisionImpl()
// Destructor for the class
//----------------------------------------------------------------------------------
CE4_ExPkgBaseRevisionImpl::~CE4_ExPkgBaseRevisionImpl()
{
}
 
//----------------------------------------------------------------------------------
// CE4_ExPkgBaseRevisionImpl::initializeClass
// This method is used to initialize this Class
//----------------------------------------------------------------------------------
int CE4_ExPkgBaseRevisionImpl::initializeClass()
{
    int ifail = ITK_ok;
    static bool initialized = false;

    if( !initialized )
    {
        ifail = CE4_ExPkgBaseRevisionGenImpl::initializeClass( );
        if ( ifail == ITK_ok )
        {
            initialized = true;
        }
    }
    return ifail;
}


/**
 * Setter for a string Property
 * @param value - Value to be set for the parameter
 * @param isNull - If true, set the parameter value to null
 * @return - Status. 0 if successful
 */
int  CE4_ExPkgBaseRevisionImpl::setCe4_statusBase( const std::string &value, bool isNull )
{
	int iStatus				= 0 ;

	const char* __function__ = "CE4_ExPkgBaseRevisionImpl::setCe4_statusBase" ;
	CE4_TRACE_ENTER() ;

	try
	{
		//iStatus = getCE4_ExPkgBaseRevision()->setString("ce4_status", value, false);
		
		CE4_TRACE_CALL( iStatus = CE4_ExPkgBaseRevisionGenImpl::setCe4_statusBase(value,false) );

		//this->setCe4_statusBase(value,isNull);
		setPkgHistory(value);
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	CE4_TRACE_LEAVE() ;
    
    return iStatus;
}

int CE4_ExPkgBaseRevisionImpl::setPkgHistory(const string& value)
{
	int iStatus				= 0 ;

	string remarks = "";
	bool isNull = false;

	const char* __function__ = "CE4_ExPkgBaseRevisionImpl::setCe4_statusBase" ;
	CE4_TRACE_ENTER() ;

	try
	{
		Teamcenter::CreateInput* pPkgStatusCreI = dynamic_cast<Teamcenter::CreateInput*>(Teamcenter::BusinessObjectRegistry::instance().createInputObject("CE4_PackageStatus", OPERATIONINPUT_CREATE)); 
		CE4_TRACE_CALL( iStatus = pPkgStatusCreI->setString("object_name",value,false));
		getCE4_ExPkgBaseRevision()->getString("ce4_statusRemarks",remarks,isNull);
		CE4_TRACE_CALL( iStatus = pPkgStatusCreI->setString("object_desc",remarks,isNull));
		Teamcenter::WorkspaceObject* pStatus = dynamic_cast<Teamcenter::WorkspaceObject*>(Teamcenter::BusinessObjectRegistry::instance().createBusinessObject(pPkgStatusCreI));
		if(pStatus!=NULL)
		{
			CE4_TRACE_CALL( iStatus = AOM_save(pStatus->getTag()) );
			vector<tag_t> vectorTagValues;
			vector<int> vectorNullValues;
			tag_t* resultTagValues = NULL;
			int resultSize = 0;
			CE4_TRACE_CALL( iStatus = getCE4_ExPkgBaseRevision()->getTagArray("ce4_pkgHistory", vectorTagValues,vectorNullValues));
			vectorTagValues.push_back(pStatus->getTag());
			tag_t revTag = getCE4_ExPkgBaseRevision()->getTag();
			CE4_TRACE_CALL( iStatus = convertVectorToArray(vectorTagValues, &resultTagValues, &resultSize));
			CE4_TRACE_CALL( iStatus = AOM_set_value_tags(revTag, "ce4_pkgHistory", resultSize , resultTagValues));
			MEM_free(resultTagValues);
		}
	}
	catch(exception e)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s:%s Unhandled Exception.\n", __function__,e.what() );
				printf("%s:%s Unhandled Exception.\n", __function__,e.what() );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	CE4_TRACE_LEAVE() ;
    
    return iStatus;
}


/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CE4_ExPkgBaseRevisionImpl.hxx          
#      Module          :           libce4tccadex_v1.src.server.CE4tccadex          
#      Description     :           Main implementation file for setter method of ce4_status          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    









/** 
    @file 

    This file contains the declaration for the Business Object CE4_ExPkgBaseRevisionImpl

*/

#ifndef TCCADEX__CE4_EXPKGBASEREVISIONIMPL_HXX
#define TCCADEX__CE4_EXPKGBASEREVISIONIMPL_HXX

#include <CE4tccadex/CE4_ExPkgBaseRevisionGenImpl.hxx>
#include <CE4tccadex/libce4tccadex_exports.h>

#define CE4_ExPkgBaseRevisionPomClassName "CE4_ExPkgBaseRevision"

namespace TCCADEX
{
   class CE4_ExPkgBaseRevisionImpl; 
   class CE4_ExPkgBaseRevisionDelegate;
}
 
class  CE4TCCADEX_API TCCADEX::CE4_ExPkgBaseRevisionImpl
           : public TCCADEX::CE4_ExPkgBaseRevisionGenImpl 
{
public:    

    // find method
    // static status_t find();  


   /**
    * Setter for a string Property
    * @param value - Value to be set for the parameter
    * @param isNull - If true, set the parameter value to null
    * @return - Status. 0 if successful
    */
    int  setCe4_statusBase( const std::string &value, bool isNull );


protected:
    // Constructor for a CE4_ExPkgBaseRevision
    explicit CE4_ExPkgBaseRevisionImpl( CE4_ExPkgBaseRevision& busObj );

    // Destructor
    ~CE4_ExPkgBaseRevisionImpl();


private:
    // Default Constructor for the class
    CE4_ExPkgBaseRevisionImpl();
    
    // Private default constructor. We do not want this class instantiated without the business object passed in.
    CE4_ExPkgBaseRevisionImpl( const CE4_ExPkgBaseRevisionImpl& );

    // Copy constructor
    CE4_ExPkgBaseRevisionImpl& operator=( const CE4_ExPkgBaseRevisionImpl& );

    // Method to initialize this Class
    static int initializeClass();

    //static data
    friend class TCCADEX::CE4_ExPkgBaseRevisionDelegate;

	//to set the package history while setting the status property
	int setPkgHistory(const string&);

};

#include <CE4tccadex/libce4tccadex_undef.h>
#endif // TCCADEX__CE4_EXPKGBASEREVISIONIMPL_HXX

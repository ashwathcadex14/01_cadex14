/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CE4_setCfgParentAttr.cxx          
#      Module          :           libce4tccadex_v1.src.server.CE4tccadex          
#      Description     :           To set the CfgAttachmentLine attribute at run time          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    








/* 
 * @file 
 *
 *   This file contains the implementation for the Extension CE4_setCfgParentAttr
 *
 */
#include <CE4tccadex/CE4_setCfgParentAttr.hxx>

int evaluateAttribute(tag_t parentTag, tag_t objTag, char *propName, logical *retValue);

int CE4_setCfgParentAttr( METHOD_message_t *msg, va_list args )
{
	int iStatus				= 0 ;

	tag_t propTag			= NULLTAG ,
		  objTag			= NULLTAG ,
		  parentTag			= NULLTAG ;

	char *propName			= NULL ,
		 *objClass			= NULL ,
		 *parentClass		= NULL ;

	logical *retValue       = false; 
	const char* __function__ = "CE4_setCfgParentAttr" ;
	CE4_TRACE_ENTER() ;

	try
	{	
		va_list largs;
		va_copy(largs, args);
		propTag = va_arg(largs, tag_t);
		retValue = va_arg(largs, logical*);
		*retValue = false; // As suggested by TCHelp, nullifying it
		CE4_TRACE_CALL( iStatus = PROP_ask_owning_object(propTag, &objTag));
		CE4_TRACE_CALL( iStatus = PROP_ask_name(propTag, &propName));
		CE4_TRACE_CALL( iStatus = AOM_ask_value_tag(objTag, CE4_ME_CL_PARENT, &parentTag));
		CE4_TRACE_CALL( iStatus = AOM_ask_value_string(objTag, CE4_AL_SOURCE_CLASS, &objClass));
		

		if(tc_strcmp(objClass, "Dataset")==0)
		{
			CE4_TRACE_CALL( iStatus = AOM_ask_value_string(parentTag, CE4_AL_SOURCE_CLASS, &parentClass));

			if(tc_strcmp(parentClass, "ItemRevision")==0)
			{
				CE4_TRACE_CALL( iStatus = evaluateAttribute(parentTag, objTag, propName, &(*retValue)));
			}
			else if(tc_strcmp(parentClass, "PseudoFolder")==0)
			{
				tag_t revParent = NULLTAG;

				CE4_TRACE_CALL( iStatus = AOM_ask_value_tag(parentTag, CE4_ME_CL_PARENT, &revParent));
				CE4_TRACE_CALL( iStatus = evaluateAttribute(revParent, objTag, propName, &(*retValue))); // Assuming it shud be the revision cfg attachment line
			}
		}
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	CE4_free_memory(propName);
	CE4_free_memory(objClass);
	CE4_free_memory(parentClass);

	CE4_TRACE_LEAVE() ;

	return iStatus ;

}

int evaluateAttribute(tag_t parentTag, tag_t objTag, char *propName, logical *retValue)
{
	int iStatus				= 0 ;

	tag_t bomLine = NULLTAG ;

	char *exportValue = NULL ;

	string bomPropName = "" ;
	const char* __function__ = "evaluateAttribute" ;
	CE4_TRACE_ENTER() ;

	try
	{
		CE4_TRACE_CALL( iStatus = AOM_ask_value_tag(parentTag, CE4_ME_CL_SOURCE, &bomLine));

		if(tc_strcmp(propName, CE4_CL_PARENT_EXPORT_NATIVE)==0)
		{
			bomPropName.assign(CE4_EXPORT_NATIVE);
			CE4_TRACE_CALL( iStatus = AOM_ask_value_string(bomLine, bomPropName.c_str(), &exportValue));
		}
		else if(tc_strcmp(propName, CE4_CL_PARENT_EXPORT_RENDERING)==0)
		{
			bomPropName.assign(CE4_EXPORT_RENDERING);
			CE4_TRACE_CALL( iStatus = AOM_ask_value_string(bomLine, bomPropName.c_str(), &exportValue));
		}
		else if(tc_strcmp(propName, CE4_CL_NO_CAD_NO_RENDER)==0)
		{
			logical isCad	 = false ,
				    isRender = false ;

			char *typeName   = NULL ;
			CE4_TRACE_CALL( iStatus = AOM_ask_value_string(objTag, CE4_AL_SOURCE_TYPE, &typeName));

			CE4_TRACE_CALL( iStatus = checkIfAvailable(CE4_CAD_CHECK_PREF_NAME, typeName , &isCad));
			CE4_TRACE_CALL( iStatus = checkIfAvailable(CE4_RENDER_CHECK_PREF_NAME, typeName , &isRender));

			if(isCad || isRender)
			{
				exportValue = (char*)MEM_alloc(4*sizeof(char));
				tc_strcpy(exportValue, "true");
			}

			CE4_free_memory(typeName);
		}

		

		if(tc_strcmp(exportValue, "true")==0)
		{
			*retValue = true;
		}
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	CE4_free_memory(exportValue);

	CE4_TRACE_LEAVE() ;

	return iStatus ;
}


/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CE4_validateCheckIn.cxx          
#      Module          :           libce4tccadex_v1.src.server.CE4tccadex          
#      Description     :           To validate the WSO during check in and throw error if checked out for a package          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
/* 
 * @file 
 *
 *   This file contains the implementation for the Extension CE4_validateCheckIn
 *
 */
#include <CE4tccadex/CE4_validateCheckIn.hxx>

logical isCadexCheckIn = false ;

int CE4_validateCheckIn( METHOD_message_t *msg, va_list args )
{
 
	int iStatus				= 0 ;

	tag_t resObj			= NULLTAG ,
		  primObj			= NULLTAG ,
		  reservTag			= NULLTAG ;
	
	char *reason			= NULL ,
		 *changeId			= NULL ;

	logical hasByPass		= false ;

	string objString		= ""   ,
		   pkgStr			= ""   ;

	const char* __function__ = "CE4_validateCheckIn" ;
	CE4_TRACE_ENTER() ;

	try
	{
		resObj = va_arg(args, tag_t);

		CE4_TRACE_CALL(iStatus = GRM_ask_primary(resObj, &primObj));
		CE4_TRACE_CALL(iStatus = RES_ask_reservation_object(primObj, &reservTag));
		CE4_TRACE_CALL(iStatus = RES_ask_reason2(reservTag, &reason));
		CE4_TRACE_CALL(iStatus = RES_ask_change_id2(reservTag, &changeId));
		CE4_TRACE_CALL(iStatus = getObjectAttribute(primObj, "object_string", objString));
		pkgStr.assign(changeId);
		CE4_TRACE_CALL( iStatus = ITK_ask_bypass( &hasByPass ) );
		//pkgStr.replace(pkgStr.begin(), pkgStr.end(), '_', '/');
		if(tc_strcmp(reason, "Checked out for Supplier")==0 && !isCadexCheckIn && !hasByPass)
		{
			iStatus = EMH_store_initial_error_s2(EMH_severity_error, CE4_SET_EXP_CHECK_OUT_ERROR, objString.c_str() , pkgStr.c_str() );
			iStatus = CE4_SET_EXP_CHECK_OUT_ERROR;
			throw iStatus;
		}
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	CE4_free_memory(reason);
	CE4_free_memory(changeId);

	CE4_TRACE_LEAVE() ;

	return iStatus ;

}
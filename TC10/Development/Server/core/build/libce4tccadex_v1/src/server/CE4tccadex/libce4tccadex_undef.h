/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           libce4tccadex_undef.h          
#      Module          :           libce4tccadex_v1.src.server.CE4tccadex          
#      Description     :                     
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    









#include <common/library_indicators.h>

#if !defined(EXPORTLIBRARY)
#   error EXPORTLIBRARY is not defined
#endif

#undef EXPORTLIBRARY

#if !defined(LIBCE4TCCADEX) && !defined(IPLIB)
#   error IPLIB orLIBCE4TCCADEX is not defined
#endif

#undef CE4TCCADEX_API
#undef CE4TCCADEXEXPORT
#undef CE4TCCADEXGLOBAL
#undef CE4TCCADEXPRIVATE

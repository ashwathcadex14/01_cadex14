/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           BOMTraversalEngine.cpp          
#      Module          :           libce4tccadex_v1.src.server.custom_classes          
#      Description     :           Interface for traversing a bom structure          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
#include <custom_classes/BOMTraversalEngine.h>

BOMTraversalEngine::BOMTraversalEngine( void )
{
}

BOMTraversalEngine::BOMTraversalEngine( tag_t topLine )
{
	int iStatus				= 0 ;

	const char* __function__ = "BOMTraversalEngine::BOMTraversalEngine" ;
	CE4_TRACE_ENTER() ;

	try
	{
		this->topBomLine = topLine;
		this->stopLevel = -1;
		CE4_TRACE_CALL( iStatus = BOM_line_ask_window(this->topBomLine, &(this->window)));
		CE4_TRACE_CALL( iStatus = getObjectAttribute(this->topBomLine, "bl_item_item_id", this->pkgId));
		CE4_TRACE_CALL( iStatus = getObjectAttribute(this->topBomLine, "bl_rev_item_revision_id", this->pkgRevId));
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}
		
	CE4_TRACE_LEAVE() ;
	
}

BOMTraversalEngine::BOMTraversalEngine( tag_t topLine, const string& revRule, logical isAbsOcc )
{
	int iStatus				= 0 ;

	tag_t revRuleTag		= NULLTAG ;
	const char* __function__ = "BOMTraversalEngine::BOMTraversalEngine" ;
	CE4_TRACE_ENTER() ;

	try
	{
		this->BOMTraversalEngine::BOMTraversalEngine(topLine);
		CE4_TRACE_CALL(iStatus = CFM_find(revRule.c_str(), &revRuleTag));
		CE4_TRACE_CALL(iStatus = BOM_set_window_config_rule(this->window,revRuleTag));
		CE4_TRACE_CALL(iStatus = BOM_refresh_window(this->window));
		CE4_TRACE_CALL(iStatus = BOM_window_set_absocc_edit_mode(this->window, isAbsOcc));
		CE4_TRACE_CALL(iStatus = BOM_refresh_window(this->window));
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}
		
	CE4_TRACE_LEAVE() ;
	
}

BOMTraversalEngine::BOMTraversalEngine( tag_t topLine, const string& revRule, logical isAbsOcc, int stopLvl )
{
	int iStatus				= 0 ;

	tag_t revRuleTag		= NULLTAG ;
	const char* __function__ = "BOMTraversalEngine::BOMTraversalEngine" ;
	CE4_TRACE_ENTER() ;

	try
	{
		this->BOMTraversalEngine::BOMTraversalEngine(topLine, revRule, isAbsOcc);
		
		if(stopLvl >= 0)
			this->stopLevel = stopLvl;
			
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}
		
	CE4_TRACE_LEAVE() ;
}

int BOMTraversalEngine::traverseStructure(tag_t bomLine)
{
	int iStatus				= 0 ,
		childCount			= 0 , 
		attrTag				= 0 ,
		level				= 0 ;

	tag_t *childLines		= NULL ;

	const char* __function__ = "BOMTraversalEngine::traverseStructure" ;
	CE4_TRACE_ENTER() ;

	try
	{
		
	
		CE4_TRACE_CALL ( iStatus = BOM_line_look_up_attribute( "bl_level_starting_0", &attrTag ));

		CE4_TRACE_CALL ( iStatus = BOM_line_ask_attribute_int( bomLine, attrTag, &level));

		if(this->stopLevel == -1 || level < this->stopLevel)
		{
			CE4_TRACE_CALL( iStatus = BOM_line_ask_child_lines( bomLine, &childCount, &childLines));

			if( childCount > 0 )
			{
				for (int i = 0 ; i < childCount ; i++ )
				{
					CE4_TRACE_CALL( iStatus = processBOMLine(childLines[i]));
					CE4_TRACE_CALL( iStatus = traverseStructure(childLines[i]));
				}
			}
		}
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	CE4_free_memory(childLines);
		
	CE4_TRACE_LEAVE() ;

	return iStatus;
}

int BOMTraversalEngine::traverseStructure()
{
	int iStatus				= 0 ,
		childCount			= 0 ;

	tag_t *childLines		= NULL ;

	const char* __function__ = "BOMTraversalEngine::traverseStructure" ;
	CE4_TRACE_ENTER() ;

	try
	{
		CE4_TRACE_CALL( iStatus = traverseStructure( this->topBomLine) );
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}
		
	CE4_TRACE_LEAVE() ;

	return iStatus;
}
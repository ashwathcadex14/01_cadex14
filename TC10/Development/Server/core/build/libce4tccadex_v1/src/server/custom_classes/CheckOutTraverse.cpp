/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           CheckOutTraverse.cpp          
#      Module          :           libce4tccadex_v1.src.server.custom_classes          
#      Description     :           Implements BOMTraversalEngine to check out objects          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    
#include <custom_classes/CheckOutTraverse.h>


CheckOutTraverse::CheckOutTraverse(void) 
{
	
}

CheckOutTraverse::CheckOutTraverse(tag_t topLine, const string& revRule, logical isAbsOcc) : BOMTraversalEngine(topLine, revRule, isAbsOcc)
{
	itemDefaultRelations.push_back("IMAN_master_form");
	itemDefaultRelations.push_back("bom_view_tags");
	revDefaultRelations.push_back("IMAN_master_form_rev");
	revDefaultRelations.push_back("structure_revisions");
	revDefaultRelations.push_back("IMAN_specification");
	revDefaultRelations.push_back("IMAN_Rendering");
}

CheckOutTraverse::CheckOutTraverse(tag_t topLine, const string& revRule, logical isAbsOcc, int stopLvl) : BOMTraversalEngine(topLine, revRule, isAbsOcc, stopLvl)
{
}

int CheckOutTraverse::processBOMLine(tag_t line)
{
	int iStatus				= 0 ;

	const char* __function__ = "CheckOutTraverse::processBOMLine" ;
	CE4_TRACE_ENTER() ;

	try
	{
		CE4_TRACE_CALL( iStatus = checkOutLine(line));
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}
		
	CE4_TRACE_LEAVE() ;

	return iStatus;
}

int CheckOutTraverse::checkOutLine(tag_t line)
{
	int iStatus				= 0 ,
		attId				= 0 ;

	tag_t revTag			= NULLTAG ,
		  itemTag			= NULLTAG ;
	const char* __function__ = "CheckOutTraverse::checkOutLine" ;
	CE4_TRACE_ENTER() ;

	try
	{
		string checkOut = "" ;
		CE4_TRACE_CALL(iStatus = getObjectAttribute(line, "CE4_checkOutNode", checkOut));
		
		if(checkOut.compare("true")==0)
		{	
			string exportN			   = "" ,
				   exportR			   = "" ;
			logical isCheckOut		   = false ,
				    exportNative	   = false ,
					exportRendering	   = false ;
			
			CE4_TRACE_CALL(iStatus = BOM_line_look_up_attribute  ( "bl_revision", &attId )) ;  
			CE4_TRACE_CALL(iStatus = BOM_line_ask_attribute_tag(line, attId, &revTag));
			CE4_TRACE_CALL(iStatus = reserveObject(revTag));
			CE4_TRACE_CALL(iStatus = getObjectAttribute(line, "CE4_exportNative", exportN));
			CE4_TRACE_CALL(iStatus = getObjectAttribute(line, "CE4_exportRendering", exportR));
			CE4_TRACE_CALL(iStatus = checkOutSecondary(revTag,convertStringtoLogical(exportN),convertStringtoLogical(exportR),true));
			CE4_TRACE_CALL(iStatus = ITEM_ask_item_of_rev(revTag, &itemTag));
			CE4_TRACE_CALL(iStatus = reserveObject(itemTag));
			CE4_TRACE_CALL(iStatus = checkOutSecondary(itemTag,false,false,false));
			CE4_TRACE_CALL(iStatus = checkOutAttachments(line));
		}
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}
		
	CE4_TRACE_LEAVE() ;

	return iStatus;
}

int CheckOutTraverse::reserveObject(tag_t object)
{
	int iStatus				= 0 ,
		checkOutStatus		= 0 ;

	const char* __function__ = "CheckOutTraverse::reserveObject" ;
	CE4_TRACE_ENTER() ;

	try
	{
		string pkgString = this->pkgId + "_" + this->pkgRevId; // Logic should be changed

		CE4_TRACE_CALL(iStatus = isCheckOutNeeded(object, pkgString, &checkOutStatus));
		if(checkOutStatus == CE4_DO_CHECK_OUT)
			CE4_TRACE_CALL( iStatus = RES_checkout2(object, "Checked out for Supplier", pkgString.c_str(), "", RES_EXCLUSIVE_RESERVE)); 
		else if( checkOutStatus == CE4_ALREADY_CHECKED_OUT)
			throw 1;

	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}
		
	CE4_TRACE_LEAVE() ;

	return iStatus;
}

int CheckOutTraverse::isCheckOutNeeded(tag_t obj, string pkgString, int *isCheckOutN)
{
	int iStatus				= 0 ;

	tag_t reservTag			= NULLTAG ;

	char *changeId			= NULL ,
		 *reason			= NULL ;

	const char* __function__ = "CheckOutTraverse::isCheckOutNeeded" ;
	CE4_TRACE_ENTER() ;

	try
	{
		string   checkedOut = ""  ; 

		CE4_TRACE_CALL(iStatus = getObjectAttribute(obj, "checked_out", checkedOut));
		
		if(checkedOut.compare("Y")==0)
		{
			CE4_TRACE_CALL(iStatus = RES_ask_reservation_object(obj, &reservTag));
			CE4_TRACE_CALL(iStatus = RES_ask_change_id2(reservTag, &changeId));
			CE4_TRACE_CALL(iStatus = RES_ask_reason2(reservTag, &reason));
			if(tc_strcmp(changeId, pkgString.c_str())==0 && tc_strcmp(reason, "Checked out for Supplier")==0)
			{
				*isCheckOutN = CE4_IGNORE_CHECK_OUT;
			}
			else
			{
				*isCheckOutN = CE4_ALREADY_CHECKED_OUT;
			}
		}
		else
		{
			*isCheckOutN = CE4_DO_CHECK_OUT;
		}	
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	CE4_free_memory(changeId);
	CE4_free_memory(reason);
		
	CE4_TRACE_LEAVE() ;

	return iStatus;
}

int CheckOutTraverse::checkOutSecondary(tag_t object, logical exportNative, logical exportRendering, logical checkOut)
{
	int iStatus				= 0 ;

	tag_t classId			= NULLTAG ;

	string objClass			= "" ;

	vector<string> defaultRelations ;

	const char* __function__ = "CheckOutTraverse::checkOutSecondary" ;
	CE4_TRACE_ENTER() ;

	try
	{
		CE4_TRACE_CALL(iStatus = getClassForObj(object, objClass));

		if(objClass.compare("Item")==0)
			defaultRelations = itemDefaultRelations;
		else if(objClass.compare("ItemRevision")==0)
			defaultRelations = revDefaultRelations;

		for(std::vector<string>::iterator it = defaultRelations.begin();it != defaultRelations.end() ;it++)
		{
				int objCount = 0 ;

				tag_t *secObjs = NULL ;

				CE4_TRACE_CALL(iStatus = AOM_ask_value_tags( object, (*it).c_str(), &objCount, &secObjs));
				
				if(objCount>0)
				{
					for(int j = 0 ; j < objCount ; j++ )
					{
						string objType			= "" ,
							   secClass			= "" ;
						logical isCad			= false ,
							    isRendering		= false ;

						CE4_TRACE_CALL(iStatus = getClassForObj(secObjs[j], secClass));

						if(secClass.compare("Dataset")!=0)
						{
							CE4_TRACE_CALL(iStatus = reserveObject(secObjs[j]));
						}
						else
						{
							CE4_TRACE_CALL(iStatus = getObjectAttribute(secObjs[j],"object_type", objType));
							CE4_TRACE_CALL(iStatus = checkIfAvailable("CADEXTraversalCADTypes", objType, &isCad));
							CE4_TRACE_CALL(iStatus = checkIfAvailable("CADEXTraversalRenderTypes", objType, &isRendering));

							if(((isCad && exportNative) || (isRendering && exportRendering)) && checkOut)
							{
								CE4_TRACE_CALL(iStatus = reserveObject(secObjs[j]));
							}
						}
					}
				}
				CE4_free_memory(secObjs);
		}
	
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	CE4_TRACE_LEAVE() ;

	return iStatus;
}

int CheckOutTraverse::checkOutAttachments(tag_t lineObject)
{
	int iStatus				= 0 ,
		relCount			= 0 ;

	tag_t *relTags			= NULL ;

	const char* __function__ = "CheckOutTraverse::checkOutAttachments" ;
	CE4_TRACE_ENTER() ;

	try
	{
		CE4_TRACE_CALL(iStatus = BOM_line_ask_absocc_relation(lineObject, NULLTAG, CE4_ABS_OCC_DSET_RELATION_CHECKOUT, true, &relCount, &relTags));
	
		for(int i = 0 ; i < relCount ; i++)
		{
			tag_t secObject = NULLTAG ;
			CE4_TRACE_CALL(iStatus = GRM_ask_secondary(relTags[i], &secObject));
			CE4_TRACE_CALL(iStatus = reserveObject(secObject));
		}
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	CE4_free_memory(relTags);
	
	CE4_TRACE_LEAVE() ;

	return iStatus;
}

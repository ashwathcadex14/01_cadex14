/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           ce4_createExportPackage.cpp          
#      Module          :           libce4tccadex_v1.src.server.custom_functions          
#      Description     :           To create export package in Exchange Package          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    

/* 
 * @file 
 *
 *   This file contains the implementation for the Extension CE4CreateExpPkgStructure
 *
 */
#include <common/CE4_Common.h>

int zipExportContents(string exportDir, string zipName, logical encrypt);
logical plmxmlActionFailure	= false ;

int CE4_createExportPackage(tag_t revTag, tag_t tferMode)
{
 
	int iStatus				= 0 ,
		exportCount			= 0 ,
		infoCount			= 5 ,
		markPoint			= 0 ;

	tag_t session			= NULLTAG ,
		  *exportObjs		= NULL	  ;

	char **sessionTitles	= NULL	  ,
		 **sessionValues	= NULL    ,
		 *bomLineString		= NULL    ,
		 *rootDir			= NULL    ,
		 *dateTime			= NULL    ,
		 *pieDir			= NULL    ;

	const char* __function__ = "CE4_createExportPackage" ;
	CE4_TRACE_ENTER() ;

	try
	{
		string itemId		= "" ,
			   revId		= "" ,
			   xmlFile		= "" ,
			   logFile		= "" ,
			   pkgIdStr		= "CE4PackageID" ,
			   pkgRevStr	= "CE4PackageRevID",
			   bomLineUid	= "CE4TopBomLineUID",
			   bomRevRule	= "CE4BomRevisionRule",
			   bomViewName  = "CE4BomViewRevisionName",
			   revRule		= "" , 
			   packageId	= "" ,
			   dirPath		= "" ,
			   encrypt      = "" ;

		CE4_TRACE_CALL(iStatus = POM_place_markpoint(&markPoint));

		sessionTitles = (char**)MEM_alloc(sizeof(char*)*infoCount);
		sessionValues = (char**)MEM_alloc(sizeof(char*)*infoCount);

		CE4_TRACE_CALL( iStatus = getObjectAttribute(revTag, "item_id", itemId));
		CE4_TRACE_CALL( iStatus = getObjectAttribute(revTag, "item_revision_id", revId));
		CE4_TRACE_CALL( iStatus = getObjectAttribute(revTag, "ce4_revisionRule", revRule));
		CE4_TRACE_CALL( iStatus = IMF_get_transient_volume_root_dir(2, &rootDir));
		dateTime = ce4_get_local_time("_%Y%m%d_%H%M%S") ;
		packageId.assign("Infoex_ExPkg_" + itemId + "_" + revId );
		CE4_TRACE_CALL(iStatus = PIE_create_working_dir(&pieDir));
		dirPath.append(pieDir);
		xmlFile.append(dirPath + "\\" + packageId +  ".xml");
		logFile.append(dirPath + "\\" + packageId +  ".log");
		sessionTitles[0] = (char*)MEM_alloc(sizeof(char)*pkgIdStr.length());
		sessionValues[0] = (char*)MEM_alloc(sizeof(char)*itemId.length());
		sessionTitles[1] = (char*)MEM_alloc(sizeof(char)*pkgRevStr.length());
		sessionValues[1] = (char*)MEM_alloc(sizeof(char)*revId.length());
		sessionTitles[2] = (char*)MEM_alloc(sizeof(char)*bomLineUid.length());
		sessionTitles[3] = (char*)MEM_alloc(sizeof(char)*bomRevRule.length());
		sessionValues[3] = (char*)MEM_alloc(sizeof(char)*revRule.length());
		sessionTitles[4] = (char*)MEM_alloc(sizeof(char)*bomViewName.length());

		tc_strcpy(sessionTitles[0], pkgIdStr.c_str());
		tc_strcpy(sessionValues[0], itemId.c_str());
		tc_strcpy(sessionTitles[1], pkgRevStr.c_str());
		tc_strcpy(sessionValues[1], revId.c_str());
		tc_strcpy(sessionTitles[2], bomLineUid.c_str());
		tc_strcpy(sessionTitles[3], bomRevRule.c_str());
		tc_strcpy(sessionValues[3], revRule.c_str());
		tc_strcpy(sessionTitles[4], bomViewName.c_str());

		CE4_TRACE_CALL( iStatus = PIE_create_session(&session));
		CE4_TRACE_CALL( iStatus = PIE_session_set_file(session, xmlFile.c_str()));
		CE4_TRACE_CALL( iStatus = PIE_session_set_log_file(session, logFile.c_str()));
		CE4_TRACE_CALL( iStatus = PIE_session_set_transfer_mode(session, tferMode));
		CE4_TRACE_CALL( iStatus = findToplineOfPkg(revTag, &exportCount, &exportObjs) ) ;

		if(exportObjs[1]!=NULLTAG)
		{
			//Should be Bomline
			ITK__convert_tag_to_uid(exportObjs[1], &bomLineString);
			sessionValues[2] =  (char*)MEM_alloc(sizeof(char)*tc_strlen(bomLineString));
			tc_strcpy(sessionValues[2], bomLineString);
			string bvrName = "" ;
			//Should be BOMView Revision
			CE4_TRACE_CALL(iStatus = getObjectAttribute(exportObjs[2], "object_name", bvrName));
			sessionValues[4] =  (char*)MEM_alloc(sizeof(char)*bvrName.length());
			tc_strcpy(sessionValues[4], bvrName.c_str());
		}

		
		CE4_TRACE_CALL( iStatus = PIE_session_add_info(session, infoCount, sessionTitles, sessionValues));
		CE4_TRACE_CALL( iStatus = PIE_session_export_objects(session, exportCount, exportObjs) );
		CE4_TRACE_CALL( iStatus = PIE_delete_session(session));
		if(plmxmlActionFailure == true)
		{
			ce4_trace(CE4_ERROR_LOG_LEVEL, __FILE__, __LINE__, "%s", "Problem occured during the Pre-Action of PLMXML export.");
			throw 1;
		}
		CE4_TRACE_CALL(iStatus = AOM_refresh(revTag, TRUE));
		CE4_TRACE_CALL(iStatus = AOM_set_value_string(revTag, "ce4_statusRemarks", "Package Exported successfully."));
		CE4_TRACE_CALL(iStatus = AOM_save(revTag));
		CE4_TRACE_CALL(iStatus = AOM_set_value_string(revTag, "ce4_status", "Exported"));
		CE4_TRACE_CALL(iStatus = AOM_save(revTag));
		CE4_TRACE_CALL(iStatus = AOM_refresh(revTag, FALSE));
		CE4_TRACE_CALL(iStatus = getObjectAttribute(revTag, "ce4_ep_serial_no", encrypt));
		
		CE4_TRACE_CALL(iStatus = zipExportContents(dirPath, packageId, (encrypt.compare("true")==0 ? true : false)));
		CE4_TRACE_CALL(iStatus = createDataset(revTag, packageId, dirPath + "\\" + packageId + ".zip", "ZIPFILE", "BINARY_REF", "Zip", "CE4_ExPkgOutgoingPackage"));
		
		if(exportObjs[0]!=NULLTAG)
		{
			CE4_TRACE_CALL(iStatus = BOM_close_window(exportObjs[0]));
		}
		CE4_TRACE_CALL(iStatus = POM_forget_markpoint(markPoint));
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
		
		logical stateChanged	= false ;
		CE4_TRACE_CALL(iStatus = POM_roll_to_markpoint(markPoint, &stateChanged));
		plmxmlActionFailure = false ;
		CE4_TRACE_CALL(iStatus = AOM_refresh(revTag, TRUE));
		CE4_TRACE_CALL(iStatus = AOM_set_value_string(revTag, "ce4_statusRemarks", "Package Export Failed"));
		CE4_TRACE_CALL(iStatus = AOM_save(revTag));
		CE4_TRACE_CALL(iStatus = AOM_set_value_string(revTag, "ce4_status", "Export Failed"));
		CE4_TRACE_CALL(iStatus = AOM_save(revTag));
		CE4_TRACE_CALL(iStatus = AOM_refresh(revTag, FALSE));
	}

	CE4_free_memory(sessionTitles);
	CE4_free_memory(sessionValues);
	CE4_free_memory(exportObjs);
	CE4_free_memory(rootDir);
	CE4_free_memory(dateTime);
	CE4_free_memory(pieDir);

	CE4_TRACE_LEAVE() ;

	return iStatus ;
}

int findToplineOfPkg(tag_t rev, int *n_tags, tag_t **tags)
{
	int iStatus				= 0 ,
		iViewCount			= 0	,
		localTagsCount		= 0 ;

	tag_t itemTag			= NULLTAG ,
		  tNewWindow		= NULLTAG ,
		  *tBvrs			= NULL	  ,
		  tTopBomLine		= NULLTAG ,
		  *localTags		= NULL    ;

	char* sBvrType			= NULL	 ;
	
	const char* __function__ = "findToplineOfPkg" ;
	CE4_TRACE_ENTER() ;

	try
	{
		localTagsCount = 3;
		localTags = (tag_t*)MEM_alloc(sizeof(tag_t)*localTagsCount);

		CE4_TRACE_CALL ( iStatus = ITEM_rev_list_all_bom_view_revs( rev , &iViewCount , &tBvrs ) ) ;
		
				for( int j = 0 ; j < iViewCount ; j++ )
				{
					CE4_TRACE_CALL ( iStatus =  WSOM_ask_object_type2 ( tBvrs[j], &sBvrType ) ) ;

					if( !tc_strcmp( sBvrType , "BOMView Revision" ) )
					{
						CE4_TRACE_CALL ( iStatus = BOM_create_window ( &tNewWindow ) );

						CE4_TRACE_CALL ( iStatus = ITEM_ask_item_of_rev ( rev , &itemTag ) ) ;
						
						CE4_TRACE_CALL ( iStatus = BOM_set_window_top_line ( tNewWindow , itemTag , rev , tBvrs[j] , &tTopBomLine ) ) ;  
						
						localTags[0] = tNewWindow;
						localTags[1] = tTopBomLine;
						localTags[2] = tBvrs[j];
						*n_tags = localTagsCount;
						*tags = localTags;
					}
				}
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	CE4_free_memory(tBvrs);
	CE4_free_memory(sBvrType);

	CE4_TRACE_LEAVE() ;

	return iStatus ;
}

int zipExportContents(string exportDir, string zipName, logical encrypt)
{
	int iStatus				= 0 ;
	
	string zipContentsCmd	= "" ;
	const char* __function__ = "zipExportContents" ;
	CE4_TRACE_ENTER() ;

	try
	{
		if(encrypt)
			zipContentsCmd.append("cd /d " + exportDir + " && " + getenv("TC_ROOT") + "\\bin\\7za a " + zipName + ".zip" + " -pINFOEX");
		else
			zipContentsCmd.append("cd /d " + exportDir + " && " + getenv("TC_ROOT") + "\\bin\\zip -R " + zipName + " *");

		iStatus = system( zipContentsCmd.c_str()); 
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	CE4_TRACE_LEAVE() ;

	return iStatus ;
}
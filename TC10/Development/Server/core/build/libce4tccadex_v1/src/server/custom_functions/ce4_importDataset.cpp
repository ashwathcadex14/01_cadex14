/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           ce4_importDataset.cpp          
#      Module          :           libce4tccadex_v1.src.server.custom_functions          
#      Description     :           To import a dataset into Teamcenter          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    

#include <common/CE4_Common.h>

int createDataset ( tag_t tItemRevision , const string& sDatasetName , const string& sFilePath ,  const string& sFileRef , const string& datasetFormat, const string& sDataset , const string& sRelation )
{
		int iStatus		= ITK_ok ,
			iRefCount	= 0		 ,
			iSecObjs	= 0		 ;
		
		tag_t tDatasetType	= NULLTAG ,
			  tNewDataset	= NULLTAG ,
			  tTool			= NULLTAG ,
			  tFileTag		= NULLTAG ,
			  tRelationType = NULLTAG ,
			  tRelation		= NULLTAG ,
			  *tXmls		= NULL	  ;	

		 IMF_file_t fileDescriptor ;

		 AE_reference_type_t tagRefType ;

		
		const char* __function__ = "createDataset" ;
		CE4_TRACE_ENTER() ;

		try
		{
					tagRefType = AE_PART_OF ;

					CE4_TRACE_CALL ( iStatus = getSecondaryObjects ( sRelation ,  tItemRevision , &iSecObjs , &tXmls ) ) ;
					
					CE4_TRACE_CALL ( iStatus = IMF_import_file ( sFilePath.c_str() , NULL , SS_BINARY , &tFileTag , &fileDescriptor ) ) ;
					
					CE4_TRACE_CALL ( iStatus = AOM_save ( tFileTag ) ) ;

					if ( iSecObjs == 0 )
					{
						
						CE4_TRACE_CALL ( iStatus = AE_find_datasettype ( sDataset.c_str() , &tDatasetType ) ) ;

						CE4_TRACE_CALL ( iStatus = AE_create_dataset_with_id ( tDatasetType , sDatasetName.c_str() , NULL , NULL , NULL , &tNewDataset ) ) ;
					

						CE4_TRACE_CALL ( iStatus = AE_set_dataset_format ( tNewDataset , datasetFormat.c_str() ) ) ;
						
						CE4_TRACE_CALL ( iStatus = AE_ask_datasettype_def_tool ( tDatasetType , &tTool ) ) ;
						
						CE4_TRACE_CALL ( iStatus = AE_set_dataset_tool ( tNewDataset , tTool ) );
												
						CE4_TRACE_CALL ( iStatus = AE_add_dataset_named_ref ( tNewDataset , sFileRef.c_str() , tagRefType , tFileTag ) ) ;
						
						CE4_TRACE_CALL ( iStatus = AOM_save ( tNewDataset ) ) ;
						
						CE4_TRACE_CALL ( iStatus = GRM_find_relation_type ( sRelation.c_str() , &tRelationType ) ) ;
						

						CE4_TRACE_CALL ( iStatus = GRM_create_relation( tItemRevision , tNewDataset , tRelationType , NULL , &tRelation ) ) ;
						

						CE4_TRACE_CALL ( iStatus = GRM_save_relation( tRelation ) ) ;
					}
					else
					{
						CE4_TRACE_CALL ( iStatus = AOM_refresh ( tXmls[0] , true ) ) ;

						CE4_TRACE_CALL ( iStatus = AE_purge_dataset_revs  (  tXmls[0]  ) )  ;

						CE4_TRACE_CALL ( iStatus = AE_remove_dataset_named_ref ( tXmls[0] , sFileRef.c_str() ) ) ;
						
						CE4_TRACE_CALL ( iStatus = AE_add_dataset_named_ref ( tXmls[0] , sFileRef.c_str() , tagRefType , tFileTag ) ) ;
						
						CE4_TRACE_CALL ( iStatus = AOM_save (  tXmls[0] ) ) ;

						CE4_TRACE_CALL ( iStatus = AOM_refresh ( tXmls[0] , false ) ) ;

					}
		
		}
		catch(...)
		{
				if ( iStatus == ITK_ok )
				{
					TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
					iStatus = CE4_UNKNOWN_ERROR;
				}
		}

		CE4_free_memory ( tXmls ) ;

		CE4_TRACE_LEAVE () ;

		return iStatus;
}

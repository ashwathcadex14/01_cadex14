/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           ce4_importPackage.cxx          
#      Module          :           libce4tccadex_v1.src.server.custom_functions          
#      Description     :           To import incoming package to Teamcenter          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    

/* 
 * @file 
 *
 *   This file contains the implementation for the Extension CE4_importPackage
 *
 */
#include <custom_classes/CheckInTraverse.h>
#include <common/CE4_Common.h>

int unzipExportContents(string exportDir, string zipName, logical encrypt);
string findXmlFile(vector<string> fileNames);
bool hasEnding (std::string const &fullString, std::string const &ending);

int CE4_importPackage(tag_t revTag, tag_t tferMode)
{
 
	int iStatus				= 0 ,
		tagCount			= 0 ,
		iSecObjs			= 0 ,
		iRefs				= 0 ,
		iImportObjs			= 0 ,
		markPoint			= 0 ;

	tag_t session			= NULLTAG ,
	      *bomTags			= NULL ,
		  *secObjs			= NULL ,
		  *refTags			= NULL ,
		  *importObjs		= NULL ;

	char *pieDir			= NULL    ;

	const char* __function__ = "CE4_importPackage" ;
	CE4_TRACE_ENTER() ;

	try
	{
		string revRule	= "" ;
		isCadexCheckIn = true;
		CE4_TRACE_CALL(iStatus = POM_place_markpoint(&markPoint));
		CE4_TRACE_CALL( iStatus = PIE_create_working_dir(&pieDir));
		CE4_TRACE_CALL( iStatus = getSecondaryObjects("CE4_ExPkgIncomingPackage", revTag, &iSecObjs, &secObjs));
		if(iSecObjs==1)
		{

			CE4_TRACE_CALL( iStatus = AE_ask_all_dataset_named_refs2(secObjs[0], "ZIPFILE", &iRefs, &refTags));
			
			if(iRefs == 1)
			{
				string orgFileName = "" ,
					   exportPath  = "" ,
					   xmlFile	   = "" ,
					   logFile	   = "" ,
					   encrypt     = "" ;

				vector<string> fileNames ;

				CE4_TRACE_CALL( iStatus = getObjectAttribute(refTags[0], "original_file_name", orgFileName));

				exportPath.assign(pieDir).append("\\").append(orgFileName);
				CE4_TRACE_CALL( iStatus = IMF_export_file( refTags[0], exportPath.c_str()));
				CE4_TRACE_CALL(iStatus = getObjectAttribute(revTag, "ce4_ep_serial_no", encrypt));
				iStatus = unzipExportContents( pieDir, orgFileName, encrypt.compare("true")==0 ? true : false);
				CE4_TRACE_CALL( iStatus = getFilesinDir(pieDir, &fileNames));
				string plmxmlFile = findXmlFile(fileNames);
				xmlFile.assign(pieDir).append( "\\" + plmxmlFile);
				logFile.assign(pieDir).append( "\\" + plmxmlFile +  ".log");
				CE4_TRACE_CALL( iStatus = PIE_create_session(&session));
				CE4_TRACE_CALL( iStatus = PIE_session_set_file(session, xmlFile.c_str()));
				CE4_TRACE_CALL( iStatus = PIE_session_set_log_file(session, logFile.c_str()));
				CE4_TRACE_CALL( iStatus = PIE_session_set_transfer_mode(session, tferMode));
				CE4_TRACE_CALL( iStatus = PIE_session_import_objects(session, false, &iImportObjs, &importObjs));
				CE4_TRACE_CALL( iStatus = PIE_delete_session(session));
		/*		if(plmxmlActionFailure == true)
				{
					ce4_trace(CE4_ERROR_LOG_LEVEL, __FILE__, __LINE__, "%s", "Problem occured during the Pre-Action of PLMXML export.");
					throw 1;
				}*/
				CE4_TRACE_CALL(iStatus = AOM_refresh(revTag, TRUE));
				CE4_TRACE_CALL(iStatus = AOM_set_value_string(revTag, "ce4_statusRemarks", "Package Imported successfully."));
				CE4_TRACE_CALL(iStatus = AOM_save(revTag));
				CE4_TRACE_CALL(iStatus = AOM_set_value_string(revTag, "ce4_status", "Imported"));
				CE4_TRACE_CALL(iStatus = AOM_save(revTag));
				CE4_TRACE_CALL(iStatus = AOM_refresh(revTag, FALSE));
								
				CE4_TRACE_CALL( iStatus = getObjectAttribute(revTag, "ce4_revisionRule", revRule));
				CE4_TRACE_CALL(iStatus = findToplineOfPkg(revTag, &tagCount, &bomTags));
				CheckInTraverse *traverseCheckIn = new CheckInTraverse(bomTags[1], revRule, true);
				traverseCheckIn->traverseStructure();
				isCadexCheckIn = false;

				CE4_TRACE_CALL(iStatus = BOM_close_window(bomTags[0]));
				CE4_TRACE_CALL(iStatus = POM_forget_markpoint(markPoint));
			}
			else
			{ 
				iStatus = CE4_MANY_FILES_ERROR;
				throw iStatus;
			}
		}
		else
		{ 
			iStatus = CE4_INPKG_NOT_FOUND_ERROR;
	        throw iStatus;
		}
	}
	catch(...)
	{
		if(bomTags[0] != NULLTAG)
		    CE4_TRACE_CALL(iStatus = BOM_close_window(bomTags[0]));

			isCadexCheckIn = false;
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
			logical stateChanged	= false ;
			CE4_TRACE_CALL(iStatus = POM_roll_to_markpoint(markPoint, &stateChanged));
			plmxmlActionFailure = false ;
			CE4_TRACE_CALL(iStatus = AOM_refresh(revTag, TRUE));
			CE4_TRACE_CALL(iStatus = AOM_set_value_string(revTag, "ce4_statusRemarks", "Package Import completed with failures."));
			CE4_TRACE_CALL(iStatus = AOM_save(revTag));
			CE4_TRACE_CALL(iStatus = AOM_set_value_string(revTag, "ce4_status", "Imported"));
			CE4_TRACE_CALL(iStatus = AOM_save(revTag));
			CE4_TRACE_CALL(iStatus = AOM_refresh(revTag, FALSE));
			
	}
	
		
	CE4_free_memory(bomTags);
	CE4_free_memory(secObjs);
	CE4_free_memory(refTags);
	CE4_free_memory(pieDir);
	CE4_free_memory(importObjs);

	CE4_TRACE_LEAVE() ;

	return iStatus ;
}

int unzipExportContents(string exportDir, string zipName, logical encrypt)
{
	int iStatus				= 0 ;
	
	string zipContentsCmd	= "" ;
	const char* __function__ = "unzipExportContents" ;
	CE4_TRACE_ENTER() ;

	try
	{
		if(encrypt)
			zipContentsCmd.append("cd /d " + exportDir + " && " + getenv("TC_ROOT") + "\\bin\\7za e " + zipName + " -pINFOEX");
		else
			zipContentsCmd.append("cd /d " + exportDir + " && " + getenv("TC_ROOT") + "\\bin\\unzip " + zipName );
		
		iStatus = system( zipContentsCmd.c_str()); 
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	CE4_TRACE_LEAVE() ;

	return iStatus ;
}

string findXmlFile(vector<string> fileNames)
{
	for(vector<string> ::iterator it = fileNames.begin(); it != fileNames.end(); ++it)
	{
		if(hasEnding(*it, "delta.xml"))
			return *it;
	}
}

bool hasEnding (std::string const &fullString, std::string const &ending)
{
    if (fullString.length() >= ending.length()) {
        return (0 == fullString.compare (fullString.length() - ending.length(), ending.length(), ending));
    } else {
        return false;
    }
}


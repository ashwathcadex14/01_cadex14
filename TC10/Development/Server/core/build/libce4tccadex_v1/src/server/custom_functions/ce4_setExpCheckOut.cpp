/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           ce4_setExpCheckOut.cpp          
#      Module          :           libce4tccadex_v1.src.server.custom_functions          
#      Description     :           To attach objects to AbsOCC          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    

/* 
 * @file 
 *
 *   This file contains the implementation for the Extension CE4CreateExpPkgStructure
 *
 */
#include <common/CE4_Common.h>



int CE4_setExpCheckOut(tag_t dataset, logical isExport, logical isCheckOut, tag_t bomLine, tag_t bomWindow)
{
 
	int iStatus				= 0 ;

	tag_t exportRelType     = NULLTAG ,
		  checkOutRelType   = NULLTAG ,
		  newRelation		= NULLTAG ;

	const char* __function__ = "CE4_setExpCheckOut" ;
	CE4_TRACE_ENTER() ;

	try
	{
		if(dataset == NULLTAG || bomLine == NULLTAG || bomWindow == NULLTAG)
			throw CE4_SET_EXP_CHECK_OUT_ERROR;
		
		
		if(isExport)
		{
			CE4_TRACE_CALL( iStatus = GRM_find_relation_type( CE4_ABS_OCC_DSET_RELATION_EXPORT, &exportRelType));
			CE4_TRACE_CALL( iStatus = BOM_line_add_absocc_relation(bomLine, exportRelType, dataset, &newRelation));
		}
		else
		{
			CE4_TRACE_CALL( iStatus = CE4_findRelationToRemove(bomLine, dataset, CE4_ABS_OCC_DSET_RELATION_EXPORT, bomWindow));
		}

		if(isCheckOut)
		{
			CE4_TRACE_CALL( iStatus = GRM_find_relation_type( CE4_ABS_OCC_DSET_RELATION_CHECKOUT, &checkOutRelType));
			CE4_TRACE_CALL( iStatus = BOM_line_add_absocc_relation(bomLine, checkOutRelType, dataset, &newRelation));
		}
		else
		{
			CE4_TRACE_CALL( iStatus = CE4_findRelationToRemove(bomLine, dataset, CE4_ABS_OCC_DSET_RELATION_CHECKOUT, bomWindow));
		}
			
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	CE4_TRACE_LEAVE() ;

	return iStatus ;
}

int CE4_findRelationToRemove(tag_t bomLine, tag_t dataset, char* relationType, tag_t bomWindow)
{

	int iStatus    = ITK_ok ,
		relCount   = 0      ,
		remCount   = 0      ;

	tag_t* relObjects = NULL ;

	const char* __function__ = "CE4_findRelationToRemove" ;
	CE4_TRACE_ENTER() ;

	try
	{
		CE4_TRACE_CALL( iStatus = BOM_line_ask_absocc_relation(bomLine, NULLTAG, relationType, true, &relCount, &relObjects));

		if(relCount>0)
		{
			for(int i=0; i<relCount; i++)
			{
				tag_t relDataset = NULLTAG;
				CE4_TRACE_CALL( iStatus = GRM_ask_secondary(relObjects[i], &relDataset));
				if(dataset==relDataset)
				{
					CE4_TRACE_CALL( iStatus = BOM_line_remove_absocc_relation(bomLine, NULLTAG, relObjects[i]));
					CE4_TRACE_CALL( iStatus = BOM_save_window(bomWindow));
					CE4_TRACE_CALL( iStatus = BOM_refresh_window(bomWindow));
					remCount++;
					break;
				}
			}
		}
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	if(relCount>remCount)
		CE4_free_memory(relObjects);
	CE4_TRACE_LEAVE() ;

	return iStatus ;
}
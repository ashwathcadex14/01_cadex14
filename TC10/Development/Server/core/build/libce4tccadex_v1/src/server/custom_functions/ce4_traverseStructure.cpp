/*=================================================================================================                    
#                Copyright (c) 2015 BAVIS TECHNOLOGIES PRIVATE LTD.                    
#                  Unpublished - All Rights Reserved                    
#  =================================================================================================                    
#      Filename        :           ce4_traverseStructure.cpp          
#      Module          :           libce4tccadex_v1.src.server.custom_functions          
#      Description     :           To traverse structure during PreAction          
#      Project         :           Supplier CAD Exchange (CADEX)          
#      Author          :           Ashwath          
#  =================================================================================================                    
#  Date                              Name                               Description of Change
#  10-Aug-2014                         Ashwath                              Initial Creation
#  $HISTORY$                    
#  =================================================================================================*/                    

/* 
 * @file 
 *
 *   This file contains the implementation for the Extension CE4CreateExpPkgStructure
 *
 */

#include <custom_classes/BOMTraversalEngine.h>
#include <custom_classes/CheckOutTraverse.h>
#include <common/CE4_Common.h>
#include <custom_classes/ExportInputProvider.h>

int CE4_traverse(tag_t bomLine, string revRule, bool isPost, tag_t session);


int CE4_traverseStructure(map<string, string> sessionOptions, bool isPost, tag_t session)
{
 
	int iStatus				= 0 ;

	tag_t bomLineTag		= NULLTAG ;
	const char* __function__ = "CE4_traverseStructure" ;
	CE4_TRACE_ENTER() ;

	try
	{
		ITK__convert_uid_to_tag(sessionOptions.find("CE4TopBomLineUID")->second.c_str(), &bomLineTag);
		
		CE4_TRACE_CALL(iStatus = CE4_traverse(bomLineTag, sessionOptions.find("CE4BomRevisionRule")->second.c_str(), isPost, session));
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}
		
	CE4_TRACE_LEAVE() ;

	return iStatus ;
}

int CE4_traverse(tag_t bomLine, string revRule, bool isPost, tag_t session)
{
 
	int iStatus				= 0 ;

	const char *plxmlFileName		= NULL ;

	const char* __function__ = "CE4_traverse" ;
	CE4_TRACE_ENTER() ;

	try
	{
		if(isPost)
		{

			ExportInputProvider *inputProvider = new ExportInputProvider(bomLine, revRule, true, 1);
			CE4_TRACE_CALL(iStatus = inputProvider->traverseStructure());

			string xmlFilePath = "";
			CE4_TRACE_CALL(iStatus = PIE_session_ask_file(session, &plxmlFileName));

			xmlFilePath.append(plxmlFileName);
			string directory, 
			       fileName ;
			const size_t last_slash_idx = xmlFilePath.rfind('\\');
			if (std::string::npos != last_slash_idx)
			{
				directory = xmlFilePath.substr(0, last_slash_idx);
				fileName  = xmlFilePath.substr(last_slash_idx+1);
			}

			
			string rawname;
			size_t dot = fileName.find_last_of(".");
			if (dot != std::string::npos)
			{
				rawname = fileName.substr(0, dot);
			}

			

			printf("PLMXML File - %s\n", rawname.c_str());
			printf("Physical Directory - %s\n", (directory + "\\" + rawname).c_str());

			ofstream myfile;
			myfile.open (directory + "\\" + rawname + "\\clone.input");
			myfile << inputProvider->cloneInput.c_str();
			myfile.close();

			string expCommand = "";

			expCommand.append("cd /d " + directory + "\\" + rawname + " && " + getenv("TC_ROOT") + "\\bin\\nx_export " + directory + "\\" + rawname + "\\clone.input"); 

			printf("Export command - %s\n", expCommand.c_str());

			CE4_TRACE_CALL(iStatus = system( expCommand.c_str()));
		}
		else
		{
			CheckOutTraverse *traverser = new CheckOutTraverse(bomLine, revRule, true);
			CE4_TRACE_CALL(iStatus = traverser->traverseStructure());
		}
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}
		
	CE4_TRACE_LEAVE() ;

	return iStatus ;
}

int CE4_getSessionOptions(tag_t session, map<string, string>& sessionOpts)
{
	int iStatus				= 0 ,
		targetCount			= 0 ;

	char **sessionTitles		= NULL ,
		 **sessionValues		= NULL ;

	const char* __function__ = "CE4_getSessionOptions" ;
	CE4_TRACE_ENTER() ;

	try
	{
		CE4_TRACE_CALL( iStatus = PIE_session_ask_info(session, &targetCount, &sessionTitles, &sessionValues));
		
		if(targetCount>0)
		{
			for ( int i = 0 ; i < targetCount ; i++)
			{
				sessionOpts.insert(make_pair( sessionTitles[i], sessionValues[i]));
			}
		}
	}
	catch(...)
	{
			if ( iStatus == ITK_ok )
			{
				TC_write_syslog("%s: Unhandled Exception.\n", __function__ );
				iStatus = CE4_UNKNOWN_ERROR;
			}
	}

	CE4_free_memory(sessionTitles);
	CE4_free_memory(sessionValues);

	CE4_TRACE_LEAVE() ;

	return iStatus ;
}